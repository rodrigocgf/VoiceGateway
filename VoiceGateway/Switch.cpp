#include "Switch.h"
#include <typeinfo>
#include <string>
#include "Dialogic.h"
#include "version.h"

namespace fs = boost::filesystem;
namespace ip = boost::asio::ip;

namespace fs = boost::filesystem;

Switch::Switch(boost::asio::io_service & p_ioService ,fs::path p_fsCurrentPath) :   m_IoService(p_ioService), 
                                                                                    m_fsCurrentPath(p_fsCurrentPath),
                                                                                    m_stop(false)
{  

    InitializeCriticalSection(&m_CritDB);    
    InitializeCriticalSection(&m_CritStatus);

    NumBoardsInDataBase = 0;
    NumTrunksInDataBase = 0;
    NumPortsInDataBase = 0;
    IndexScreen = 0;
    IdAudio = 1;    

    ReadRegister();
    
    //m_pLog4cpp.reset(new Logger(m_StRegister.BasePathLog,"Log_System") );
    m_pLog4cpp.reset(new Logger(m_StRegister.BasePathLog , m_StRegister.LogMaxFileSize , "Log_System",0,true) );

    PrintRegisterGeneral();
    Start();    
}

Switch::~Switch(void)
{   
    Stop();

    m_pLog4cpp->debug("[%s]",__FUNCTION__);

    log4cpp::Category::shutdown();

    DeleteCriticalSection(&m_CritStatus);
    DeleteCriticalSection(&m_CritDB);    
}



void Switch::Start()
{   
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    m_pLog4cpp->debug("================================= START ===================================", __FUNCTION__ );
    m_pLog4cpp->debug("%s", GetVersion().c_str() );


    //m_GarbageCollector.reset(new GarbageCollector(m_pLog4cpp));
	GetMachineNameIp();
    
    m_Tapi.reset(new Tapi(m_pLog4cpp) );

    fs::path pathLog(m_StRegister.BasePathLog);
    if (! (fs::exists(pathLog) && fs::is_directory(pathLog) ) )
    {
        // TODO !!!
        // EXIT
    }

    fs::path pathScripts(m_StRegister.PathScripts);
    if (! (fs::exists(pathScripts) && fs::is_directory(pathScripts) ) )
    {
        // TODO !!!
        // EXIT
    }

    //m_dbPtr.reset(new DBAccess(GetBaseLogPath()) );
    //m_dbPtr.reset(new DBAccess(m_pLog4cpp) );
    m_dbPtr.reset(new DBAccess() );
        
    m_pLog4cpp->debug("[%s] Going to open database...",__FUNCTION__);    
    if(OpenDatabase())
    {
        m_pLog4cpp->debug("DBAccess::OpenDatabase() Error: %s.", m_dbPtr->GetLastError());        
		return;
	}

    if ( !m_StRegister.NoRouter )
    {
        // Le a configuracao do Ip Router no banco de dados
	    if(DbReadConfigIpRouter()){
		    m_pLog4cpp->debug("DbReadConfigIpRouter() Error!");
		    return;
	    }
	    m_pLog4cpp->debug("Ip Router Config OK");
    }

	// Le as placas do banco de dados
	if(DbReadConfig())
    {
        m_pLog4cpp->debug("DbReadConfig() Error!");
        return;
	}
	m_pLog4cpp->debug("Ports in Database OK");

    StartThreadListen_StatusScreenNet();
    
    StartThreadListen_Telnet();    
    
    m_ConnectionPtr.reset(new ConnectionEvt(this, m_pLog4cpp) );

    // Inicia os canais de telefonia
	StartPortThreads();

    
}

void Switch::Stop()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    StopThreadListen_Telnet();

    StopThreadListen_StatusScreenNet();
    
    StopPortThreads();

    m_pLog4cpp->debug("================================= STOP ===================================", __FUNCTION__ );
    
    
}

void Switch::ReadRegister()
{
    ReadRegisterODBC();
    ReadRegisterPaths();
    ReadRegisterGeneral();
    ReadRegisterASR();
    ReadRegisterCTI();

    
    /*
		
	//--------------------------------------------------------------------------------------------------------------------

	#if defined _BRT || _ATENTO

	// Le os dados para as funcoes da lib AVAYA
	strcpy(RegPath, "Software\\SupportComm\\VoiceGateway\\Avaya");
	Config.SetPath(RegPath);

	if(Config.GetKey("PathLogFile", string, sizeof(string)) < 0){
		Config.SetKey("PathLogFile", "c:\\VoiceGateway\\log\\Avaya.txt");
		strcpy(StRegister.sAvayaLogFile, "c:\\VoiceGateway\\log\\Avaya.txt");
	}
	else{
		strcpy(StRegister.sAvayaLogFile, string);
	}

	if(Config.GetKey("Port", string, sizeof(string)) < 0){
		char sAux[40];
		sprintf(sAux, "%d", 810);
		Config.SetKey("Port", sAux);
		StRegister.iAvayaLocalPort = 810;
	}
	else{
		StRegister.iAvayaLocalPort = atoi(string);
	}

	if(Config.GetKey("IP", string, sizeof(string)) < 0){
		Config.SetKey("IP", "127.0.0.1");
		strcpy(StRegister.sAvayaLocalIP, "127.0.0.1"); 
	}
	else{
		strcpy(StRegister.sAvayaLocalIP, string);
	}

	#endif // _BRT || _ATENTO

	//--------------------------------------------------------------------------------------------------------------------    
	// Le os dados para o Logar o CDR o inicio da chamada e atualizar o CDR no fim da chamada
    strcpy(RegPath, "Software\\SupportComm\\VoiceGateway\\General");
	Config.SetPath(RegPath);
		
		

    // MULTICHASSIS
    strcpy(RegPath, "Software\\SupportComm\\VoiceGateway\\Multichassis");
	Config.SetPath(RegPath);
    
    if(Config.GetKey("MC_Multichassis_Enabled", string, sizeof(string)) < 0)
    {
		Config.SetKey("MC_Multichassis_Enabled", "NO");
		StRegister.MC_Multichassis_Enabled = 0;
	}
	else
    {			
		// Converte a string para maiusculo
		StringValue = _strupr( string );

		if( StringValue == "YES")
		{
			Config.SetKey("MC_Multichassis_Enabled", "YES");
			StRegister.MC_Multichassis_Enabled = 1;
		}
		else
		{
			Config.SetKey("MC_Multichassis_Enabled", "NO");
			StRegister.MC_Multichassis_Enabled = 0;
		}
	}

#ifndef _MULTICHASSIS
	// Forca o desligamento do Multichassis quando nao compilado o codigo de Multichassis.
	StRegister.MC_Multichassis_Enabled = 0;
#endif // _MULTICHASSIS

	// Essas informa��es eram configuradas no Banco de Dados
	StRegister.IdApp = StRegister.Machine;
	StRegister.IdTypeApp = 4;  //Switch

#ifdef _VXML
	// VXMLBrowser
    strcpy(RegPath, "Software\\SupportComm\\VoiceGateway\\VXML");
	Config.SetPath(RegPath);

	if(Config.GetKey("VXML_Url_App_Server", string, sizeof(string)) < 0)
	{
		Config.SetKey("VXML_Url_App_Server", "http://localhost:8080/app/filename.vxml");
		strcpy(StRegister.VXML_Url_App_Server, "http://localhost:8080/app/filename.vxml");
	}
	else
	{
		strcpy(StRegister.VXML_Url_App_Server, string);
	}

	if(Config.GetKey("VXML_Url_Ssml_Server", string, sizeof(string)) < 0)
	{
		Config.SetKey("VXML_Url_Ssml_Server", "http://localhost:8080/SSMLServer/servlet/SSMLServerServlet?operacao=recuperarArquivo");
		strcpy(StRegister.VXML_Url_Ssml_Server, "http://localhost:8080/SSMLServer/servlet/SSMLServerServlet?operacao=recuperarArquivo");
	}
	else
	{
		strcpy(StRegister.VXML_Url_Ssml_Server, string);
	}
#endif
    */

}

void Switch::StartThreadListen_StatusScreenNet()
{
    m_pThrListen = new ThrListen( this , m_pLog4cpp );
    m_pThrListen->Start();
}

void Switch::StopThreadListen_StatusScreenNet()
{
    m_pThrListen->Stop();    
}

void Switch::StartThreadListen_Telnet()
{
    m_pThrTelnetListen = new ThrTelnetListen( m_IoService, this , m_pLog4cpp , m_StRegister.sTelnetPassword );
    m_pThrTelnetListen->Start();    
}

void Switch::StopThreadListen_Telnet()
{
    m_pThrTelnetListen->Stop();    
}

// CALLED BY CONTROLLER
void Switch::ThreadInitSystem()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    

    m_ExecuteThreadPtr.reset ( new boost::thread ( boost::bind (&Switch::Run, this )));	
    m_ExecuteThreadPtr->detach();
}

// CALLED BY CONTROLLER
void Switch::ThreadEndSystem()
{
    m_stop = true;
    m_cond.notify_one();
	m_ExecuteThreadPtr->join();
}


void Switch::Run()
{
    while( !m_stop )
	{
					
		boost::mutex::scoped_lock lk(  m_Mutex );
        m_cond.wait( lk );
		//while ( !m_stop )
			
    }
}

void Switch::GetMachineNameIp()
{
	try {
        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
        ip::tcp::resolver resolver(m_IoService);
 
        std::string h = ip::host_name();
        std::cout << "Host name is " << h << '\n';
        m_pLog4cpp->debug("Host name is %s\r\n", h.c_str() );
        
        std::cout << "IP addresses are: \n";
        m_pLog4cpp->debug("IP addresses are : \r\n");

        ip::tcp::resolver::query q(h,"");
        boost::asio::ip::tcp::resolver::iterator itResolver = resolver.resolve(q);
        boost::asio::ip::tcp::resolver::iterator itEnd;

        for ( ; itResolver != itEnd; *itResolver++ )
        {
            boost::asio::ip::tcp::endpoint ep = itResolver->endpoint();
            std::cout << "\t" << ep.address() << "\r\n";
            m_pLog4cpp->debug("\t %s \r\n", ep.address().to_string().c_str() );
        }      

	}
	catch(...)
    {
        m_pLog4cpp->debug("EXCEPTION: Switch::GetMachineNameIp()");
		//Log::LogSys(LOG_ERROR, "EXCEPTION: Switch::GetMachineNameIp()");
	}
}

void Switch::ReadRegisterODBC()
{
    char lszValue[1500];
    TCHAR buffer[1024];
    HKEY hKey;
    LONG returnStatus;
    DWORD dwType=REG_SZ;
    DWORD dwSize=255;
       

    try
    {

        returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\ODBC"), NULL,  KEY_READ|KEY_WOW64_64KEY, &hKey);

        if (returnStatus == ERROR_SUCCESS)
        {
            
            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.OdbcDataBase,0x00,sizeof(m_StRegister.OdbcDataBase));
            returnStatus = RegQueryValueEx(hKey, TEXT("Database"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            strcpy( &m_StRegister.OdbcDataBase[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.OdbcDataSourceName,0x00,sizeof(m_StRegister.OdbcDataSourceName));
            returnStatus = RegQueryValueEx(hKey, TEXT("DataSourceName"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );        
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            strcpy( &m_StRegister.OdbcDataSourceName[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.OdbcPassWord,0x00,sizeof(m_StRegister.OdbcPassWord));
            returnStatus = RegQueryValueEx(hKey, TEXT("Password"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );        
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            strcpy( &m_StRegister.OdbcPassWord[0] , lszValue );

            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.OdbcUser,0x00,sizeof(m_StRegister.OdbcUser));
            returnStatus = RegQueryValueEx(hKey, TEXT("User"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );        
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            strcpy( &m_StRegister.OdbcUser[0] , lszValue );
            
        }

        RegCloseKey (hKey);
    }
    catch(...)
    {
        std::cout << "Error at Switch::ReadRegisterODBC()." << std::endl;		
        // LOG ERROR 
	}
}

void Switch::ReadRegisterPaths()
{
    char lszValue[1500];
    TCHAR buffer[1024];
    HKEY hKey;
    LONG returnStatus;
    DWORD dwType=REG_SZ;
    DWORD dwSize=255;
        

    try
    {
        returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\Paths"), NULL,  KEY_READ|KEY_WOW64_64KEY, &hKey);

        if (returnStatus == ERROR_SUCCESS)
        {
            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.BasePathLog,0x00,sizeof(m_StRegister.BasePathLog));
            returnStatus = RegQueryValueEx(hKey, TEXT("BasePathLog"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
            {
                wcstombs(lszValue, buffer, sizeof(lszValue));                
            } else
                strcpy(lszValue,"c:\\VoiceGateway\\log");

            strcpy( &m_StRegister.BasePathLog[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.PathPrompts,0x00,sizeof(m_StRegister.PathPrompts));
            returnStatus = RegQueryValueEx(hKey, TEXT("PathPrompts"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else
                strcpy(lszValue,"c:\\VoiceGateway\\prompts");

            strcpy( &m_StRegister.PathPrompts[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.PathScripts,0x00,sizeof(m_StRegister.PathScripts));
            returnStatus = RegQueryValueEx(hKey, TEXT("PathScripts"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else
                strcpy(lszValue,"c:\\VoiceGateway\\scripts");

            strcpy( &m_StRegister.PathScripts[0] , lszValue );

            
        }
        RegCloseKey (hKey);
    }
    catch(...)
    {
		std::cout << "Error at Switch::ReadRegisterPaths()." << std::endl;		
	}
}

void Switch::ReadRegisterGeneral()
{
    char lszValue[1500];
    TCHAR buffer[1024];
    //char buffer[1024];
    HKEY hKey;
    LONG returnStatus;
    DWORD dwType=REG_SZ;
    DWORD dwValue = 0;
    DWORD dwSize=sizeof(buffer);

    TCHAR szValue[1024];
    DWORD cbValueLength = sizeof(szValue);

    try
    {
        //returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\General"), NULL,  KEY_READ|KEY_WOW64_64KEY, &hKey);
        returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\General"), 0,  KEY_QUERY_VALUE, &hKey);

        if (returnStatus == ERROR_SUCCESS)
        {   

            memset(lszValue,0x00,sizeof(lszValue));
            memset(buffer,0x00,sizeof(buffer));            
            memset(m_StRegister.AppName,0x00,sizeof(m_StRegister.AppName));
            returnStatus = RegQueryValueEx(hKey, TEXT("AppName"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"Switch");                

            strcpy( &m_StRegister.AppName[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(buffer,0x00,sizeof(buffer));            
            memset(m_StRegister.sTelnetPassword,0x00,sizeof(m_StRegister.sTelnetPassword));
            returnStatus = RegQueryValueEx(hKey, TEXT("TelnetPassword"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"multimidia");                

            strcpy( &m_StRegister.sTelnetPassword[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));  
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("Site"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"1");                

            m_StRegister.Switch = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("Machine"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"1");                

            m_StRegister.Machine = atoi(lszValue);

            
            memset(lszValue,0x00,sizeof(lszValue));            
            dwSize = sizeof(DWORD);            
            returnStatus = RegQueryValueEx(hKey, TEXT("LogMaxFileSize"), NULL , &dwType, (LPBYTE)&dwValue, &dwSize) ;            
            if (returnStatus == ERROR_SUCCESS)            
                m_StRegister.LogMaxFileSize = (int)(dwValue);                
            else                
                m_StRegister.LogMaxFileSize = 10000000;
                        

            memset(lszValue,0x00,sizeof(lszValue));     
            memset(buffer,0x00,sizeof(buffer));    
            dwType = REG_SZ;
            returnStatus = RegQueryValueEx(hKey, TEXT("LogLevel"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"0");                

            m_StRegister.LogLevel = atoi(lszValue);
            

            memset(lszValue,0x00,sizeof(lszValue));
            memset(buffer,0x00,sizeof(buffer));            
            memset(m_StRegister.LogMask,0x00,sizeof(m_StRegister.LogMask));
            returnStatus = RegQueryValueEx(hKey, TEXT("LogMask"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"1111111111111111");                

            strcpy( &m_StRegister.LogMask[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("TestMode"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
            {
                wcstombs(lszValue, buffer, sizeof(lszValue));
                //strncpy(lszValue, buffer, sizeof(lszValue));
                if ( atoi(lszValue) )
                    m_StRegister.FlagTestMode = true;
                else
                    m_StRegister.FlagTestMode = false;
            }
            else            
                m_StRegister.FlagTestMode = true;


            memset(lszValue,0x00,sizeof(lszValue));    
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("NoRouter"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
            {
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
                if ( atoi(lszValue) )
                    m_StRegister.NoRouter = true;
                else
                    m_StRegister.NoRouter = false;
            }
            else            
                m_StRegister.NoRouter = true;


            memset(lszValue,0x00,sizeof(lszValue));     
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("LogScreen"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
                //strncpy(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"2");                

            m_StRegister.FlagLogScreen = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));
            memset(buffer,0x00,sizeof(buffer));            
            memset(m_StRegister.ScriptName,0x00,sizeof(m_StRegister.ScriptName));
            returnStatus = RegQueryValueEx(hKey, TEXT("ScriptName"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"IVR.lua");                

            strcpy( &m_StRegister.ScriptName[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("TcpIpSendBufferSize"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"256000");                

            m_StRegister.TcpIpSendBufferSize = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("TcpIpRecvBufferSize"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"256000");                

            m_StRegister.TcpIpRecvBufferSize = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));
            memset(buffer,0x00,sizeof(buffer));            
            memset(m_StRegister.TcpIpHost,0x00,sizeof(m_StRegister.TcpIpHost));
            returnStatus = RegQueryValueEx(hKey, TEXT("TcpIpHost"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"127.0.0.1");                

            strcpy( &m_StRegister.TcpIpHost[0] , lszValue );
         
            /*
            
            memset(lszValue,0x00,sizeof(lszValue));            
            dwSize = sizeof(DWORD);            
            returnStatus = RegQueryValueEx(hKey, TEXT("TcpIpPort"), NULL , &dwType, (LPBYTE)&dwValue, &dwSize) ;            
            if (returnStatus == ERROR_SUCCESS)            
                m_StRegister.TcpIpPort = (int)(dwValue);                
            else                
                m_StRegister.TcpIpPort = 13160;

            */

            
            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("TcpIpPort"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"13160");                

            m_StRegister.TcpIpPort = atoi(lszValue);            


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("MaxIvrDbConnections"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"5");                

            m_StRegister.iMaxIvrDbConnection = atoi(lszValue);



            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("ImportCdrDelay"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"15");                

            m_StRegister.ImportCdrDelay = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));            
            memset(buffer,0x00,sizeof(buffer));            
            returnStatus = RegQueryValueEx(hKey, TEXT("LogCDR"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                //strncpy(lszValue, buffer, sizeof(lszValue));
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"1");                

            m_StRegister.LogCDR = atoi(lszValue);
        }        

        RegCloseKey (hKey);
    }
    catch(...)
    {
		std::cout << "Error at Switch::ReadRegisterGeneral()." << std::endl;		
	}
}


void Switch::PrintRegisterGeneral()
{
    std::stringstream ss;

    ss << "\r\n";
    ss << "=====================================================================\r\n";
    ss << boost::format(" AppName                  : %s \r\n") % m_StRegister.AppName;
    ss << boost::format(" Switch                   : %d \r\n") % m_StRegister.Switch;
    ss << boost::format(" Machine                  : %d \r\n") % m_StRegister.Machine;
    ss << boost::format(" Log level                : %d \r\n") % m_StRegister.LogLevel;
    ss << boost::format(" Log Mask                 : %s \r\n") % m_StRegister.LogMask;
    ss << boost::format(" Flag test mode           : %s \r\n") % (m_StRegister.FlagTestMode?"true":"false");
    ss << boost::format(" No Router                : %s \r\n") % (m_StRegister.NoRouter?"true":"false");
    ss << boost::format(" Flag Log Screen          : %d \r\n") % m_StRegister.FlagLogScreen;
    ss << boost::format(" Script Name              : %s \r\n") % m_StRegister.ScriptName;
    ss << boost::format(" Tcp Ip Send Buffer Size  : %d \r\n") % m_StRegister.TcpIpSendBufferSize;
    ss << boost::format(" Tcp Ip Host Ip           : %s \r\n") % m_StRegister.TcpIpHost;
    ss << boost::format(" Tcp Ip Port              : %d \r\n") % m_StRegister.TcpIpPort;
    ss << boost::format(" Max Ivr Db Connections   : %d \r\n") % m_StRegister.iMaxIvrDbConnection;
    ss << boost::format(" Import Cdr Delay         : %d \r\n") % m_StRegister.ImportCdrDelay;
    ss << boost::format(" Log CDR                  : %d \r\n") % m_StRegister.LogCDR;
    ss << boost::format(" Log Max File Size        : %d \r\n") % m_StRegister.LogMaxFileSize;
    ss << boost::format(" Telnet password          : %s \r\n") % m_StRegister.sTelnetPassword;
    ss << "=====================================================================\r\n";
    m_pLog4cpp->debug("[%p][%s] REGISTER GENERAL %s ", this, __FUNCTION__ , ss.str().c_str() );
}

void Switch::ReadRegisterASR()
{
    char lszValue[1500];
    TCHAR buffer[1024];
    HKEY hKey;
    LONG returnStatus;
    DWORD dwType=REG_SZ;
    DWORD dwSize=255;


    try
    {
        returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\ASR"), NULL,  KEY_READ|KEY_WOW64_64KEY, &hKey);

        if (returnStatus == ERROR_SUCCESS)
        {
            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_GrammarPackage,0x00,sizeof(m_StRegister.ASR_GrammarPackage));
            returnStatus = RegQueryValueEx(hKey, TEXT("GrammarPackage"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"c:\\VoiceGateway\\Grammar\\Grammar");                

            strcpy( &m_StRegister.ASR_GrammarPackage[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_DatabaseType,0x00,sizeof(m_StRegister.ASR_DatabaseType));
            returnStatus = RegQueryValueEx(hKey, TEXT("DatabaseType"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"odbc");                

            strcpy( &m_StRegister.ASR_DatabaseType[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_DatabaseServer,0x00,sizeof(m_StRegister.ASR_DatabaseServer));
            returnStatus = RegQueryValueEx(hKey, TEXT("DatabaseServer"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"   ");                

            strcpy( &m_StRegister.ASR_DatabaseServer[0] , lszValue );



            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_DatabaseName,0x00,sizeof(m_StRegister.ASR_DatabaseName));
            returnStatus = RegQueryValueEx(hKey, TEXT("DatabaseName"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"   ");                

            strcpy( &m_StRegister.ASR_DatabaseName[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_DatabaseUser,0x00,sizeof(m_StRegister.ASR_DatabaseUser));
            returnStatus = RegQueryValueEx(hKey, TEXT("DatabaseUser"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"nuance");                

            strcpy( &m_StRegister.ASR_DatabaseUser[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.ASR_DatabasePassword,0x00,sizeof(m_StRegister.ASR_DatabasePassword));
            returnStatus = RegQueryValueEx(hKey, TEXT("DatabasePassword"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"nuance");                

            strcpy( &m_StRegister.ASR_DatabasePassword[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));
            memset(m_StRegister.TTS_Server_Host,0x00,sizeof(m_StRegister.TTS_Server_Host));
            returnStatus = RegQueryValueEx(hKey, TEXT("TTS_Server_Host"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"localhost");                

            strcpy( &m_StRegister.TTS_Server_Host[0] , lszValue );


            memset(lszValue,0x00,sizeof(lszValue));            
            returnStatus = RegQueryValueEx(hKey, TEXT("PollingTime"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"100");                

            m_StRegister.ASR_PollingTime = atoi(lszValue);

        }

        RegCloseKey (hKey);
    }
    catch(...)
    {
		std::cout << "Error at Switch::ReadRegisterASR()." << std::endl;
	}
}


void Switch::ReadRegisterCTI()
{
    char lszValue[1500];
    TCHAR buffer[1024];
    HKEY hKey;
    LONG returnStatus;
    DWORD dwType=REG_SZ;
    DWORD dwSize=255;
        

    try
    {
        returnStatus = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\SupportComm\\VoiceGateway\\CTI"), NULL,  KEY_READ|KEY_WOW64_64KEY, &hKey);

        if (returnStatus == ERROR_SUCCESS)
        {
            memset(lszValue,0x00,sizeof(lszValue));            
            returnStatus = RegQueryValueEx(hKey, TEXT("TimeOutDial"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"60");                

            m_StRegister.TimeOutDial = atoi(lszValue);



            memset(lszValue,0x00,sizeof(lszValue));            
            returnStatus = RegQueryValueEx(hKey, TEXT("R2MinDnis"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"4");                

            m_StRegister.iR2MinDnis = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));            
            returnStatus = RegQueryValueEx(hKey, TEXT("R2MaxDnis"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
                wcstombs(lszValue, buffer, sizeof(lszValue));
            else            
                strcpy(lszValue,"4");                

            m_StRegister.iR2MaxDnis = atoi(lszValue);


            memset(lszValue,0x00,sizeof(lszValue));            
            returnStatus = RegQueryValueEx(hKey, TEXT("DetectMessageBox"), NULL , &dwType, (LPBYTE)&buffer, &dwSize );
            if (returnStatus == ERROR_SUCCESS)
            {
                wcstombs(lszValue, buffer, sizeof(lszValue));
                if ( atoi(lszValue) )
                    m_StRegister.DetectMessageBox = true;
                else
                    m_StRegister.DetectMessageBox = true;
            }
            else            
                m_StRegister.DetectMessageBox = true;
        }

        RegCloseKey (hKey);
    }
    catch(...)
    {
		std::cout << "Error at Switch::ReadRegisterCTI()." << std::endl;
	}
}

std::string Switch::GetBaseLogPath() const
{
    std::string sPath;
    sPath.assign(m_StRegister.BasePathLog);
    return sPath;
}



int Switch::OpenDatabase()
{
    m_pLog4cpp->debug("[%s]",__FUNCTION__);    

    try 
	{	
		int rc = 0;
        std::stringstream ss;        

		//ClassDB = new Class_ADO;
                
        std::string sUdlFile = GetFullUdlPath();
        ss << "File Name=" << sUdlFile << ";";
        m_pLog4cpp->debug("[%s] Going to open Database %s", __FUNCTION__ , ss.str().c_str() );

        rc = m_dbPtr->Open(ss.str().c_str());

		if( rc )
		{
            m_pLog4cpp->debug("%s", m_dbPtr->GetLastError());
			return -1;
		}
		m_pLog4cpp->debug("Database OK");

		return 0;
	}
	catch(...){
		m_pLog4cpp->debug("EXCEPTION: Switch::OpenDatabase()");
		return -1;
	}
}

int Switch::DbReadConfigIpRouter(void)
{
	try
	{
		char sQuery[256];

        m_pLog4cpp->debug("-----------------------------------------------------");
		m_pLog4cpp->debug("Switch::DbReadConfigIpRouter()");

		sprintf(sQuery, "SELECT IP AS IP, PORT AS Port FROM TSysProcessor");

		m_pLog4cpp->debug("Query: %s", sQuery);
        EnterCriticalSection(&m_CritDB);		

		// Executa a procedure
		if( m_dbPtr->Execute(sQuery) )
		{
			m_pLog4cpp->debug(m_dbPtr->GetLastError());
			m_dbPtr->Close();
			if( m_dbPtr->Open() < 0 )
			{
				m_pLog4cpp->debug(m_dbPtr->GetLastError());
			}
            LeaveCriticalSection(&m_CritDB);
			//CritDB.Leave();
			return -1;
		}

		if(m_StRegister.FlagTestMode)
		{
            m_pLog4cpp->debug("Using FLAG TEST MODE");
			strcpy(m_StRegister.IpRouter, "127.0.0.1");
			m_StRegister.PortRouter = 34444;
		}
		else
		{
			if(m_dbPtr->GetRowCount() > 0)
			{
				strcpy(m_StRegister.IpRouter, m_dbPtr->GetItem("IP"));
				//StripSpace(m_StRegister.IpRouter);
                //boost::algorithm::trim_all(m_StRegister.IpRouter);
				m_StRegister.PortRouter = atoi(m_dbPtr->GetItem("Port"));

                m_pLog4cpp->debug("IP , PORT FROM DB : %s , %d", m_StRegister.IpRouter , m_StRegister.PortRouter );
                m_pLog4cpp->debug("-----------------------------------------------------");
			}
		}
        		
        LeaveCriticalSection(&m_CritDB);
		
		return 0;
	}
	catch(...)
	{
        LeaveCriticalSection(&m_CritDB);
		
        m_pLog4cpp->debug("EXCEPTION: Switch::DbReadConfigIpRouter()");
		return -1;
	}
}														

int Switch::DbReadConfig(void)
{
	try
	{
		int boardID, trunkID, trunkRef;
        m_pLog4cpp->debug("[%s]",__FUNCTION__);    

        m_pLog4cpp->debug("-----------------------------------------------------");        

		// Zera o contador global de placas.
		NumBoardsInDataBase = 0;
		// Zera o contador global de troncos.
		NumTrunksInDataBase = 0;
		// Zera o contador global de canais.
		NumPortsInDataBase = 0;
		// Inicializa o id do tronco.
		trunkID = 1;
		// Le as placas do banco de dados
		if(DbReadBoards())
		{
			m_pLog4cpp->debug( "DbReadBoards() Error!");
			return -1;
		}

        m_pLog4cpp->debug("[%s] NumBoardsInDataBase : %d",__FUNCTION__, NumBoardsInDataBase);    
        

		for(boardID = 1; boardID <= NumBoardsInDataBase; boardID++)
		{
            m_pLog4cpp->debug("[%s] boardID : %d",__FUNCTION__, boardID);  

            DbBoards[boardID].TrunkCount = 0;
            DbBoards[boardID].ChannelCount = 0;
            DbBoards[boardID].ResourceCount  = 0;

			// Le as informacoes da placas no banco de dados
			if(DbReadBoardInfo(boardID))
			{
				m_pLog4cpp->debug("DbReadBoardInfo() Error!");
				return -1;
			}

			// Le os troncos do banco de dados
			if(DbReadTrunks(boardID))
			{
				m_pLog4cpp->debug("DbReadTrunks() Error!");
				return -1;
			}

            m_pLog4cpp->debug("[%s] NumTrunksInDataBase : %d",__FUNCTION__, NumTrunksInDataBase);    

			trunkRef = 1;

			for(; trunkID <= NumTrunksInDataBase; trunkID++)
			{
				// Le os canais do banco de dados
				if(DbReadChannels(boardID, trunkID, trunkRef++))
				{
					m_pLog4cpp->debug("DbReadChannels() Error!");
					return -1;
				}
			}

			// Le os recursos do banco de dados
			if(DbReadResources(boardID))
			{
				m_pLog4cpp->debug("DbReadResources() Error!");
				return -1;
			}
		}
		
		return 0;
	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
		m_pLog4cpp->debug("EXCEPTION: Switch::DbReadConfig()");
		return -1;
	}

}



int Switch::DbReadBoards()
{
    m_pLog4cpp->debug("[%s]",__FUNCTION__);    

	try
	{
		int rc, boardRef, boardID;
		char sQuery[256];		

		sprintf(sQuery, "EXEC SP_VG_GetBoard");

        m_pLog4cpp->debug("[%s] Query : %s",__FUNCTION__, sQuery);    		

		EnterCriticalSection(&m_CritDB);

		rc = m_dbPtr->Execute(sQuery);

		if( rc < 0 )
		{
			m_pLog4cpp->debug(m_dbPtr->GetLastError());
			m_dbPtr->Close();
			if( m_dbPtr->Open() < 0 )
			{
                m_pLog4cpp->debug("[%s] Error in Query : %s",__FUNCTION__, m_dbPtr->GetLastError() );    						
			}
			LeaveCriticalSection(&m_CritDB);
			return -1;
		}			
        m_pLog4cpp->debug("[%s] NumBoardsInDataBase : %d",__FUNCTION__, m_dbPtr->GetRowCount());    		
		NumBoardsInDataBase = m_dbPtr->GetRowCount();

		if(NumBoardsInDataBase <= 0)
		{
			m_pLog4cpp->debug("Switch::DbReadBoards(): There is no boards in Database");
			return -1;
		}

		for(boardRef = 0; boardRef < NumBoardsInDataBase; boardRef++)
		{
			boardID = atoi(	m_dbPtr->GetItem("BoardID") );

			DbBoards[boardID].BoardTypeID						= atoi(	m_dbPtr->GetItem("BoardTypeID") );
			DbBoards[boardID].BoardName							= m_dbPtr->GetItem("BoardName");
			DbBoards[boardID].ManufacturerName			        = m_dbPtr->GetItem("ManufacturerName");
			DbBoards[boardID].FamilyName						= m_dbPtr->GetItem("FamilyName");
			
			m_dbPtr->MoveNext();
		}

		LeaveCriticalSection(&m_CritDB);

		return 0;
	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
		m_pLog4cpp->debug("EXCEPTION: Switch::DbReadBoards()");
		return -1;
	}
}

//---------------------------------------------------------------------------------------------

int Switch::DbReadBoardInfo(int boardID)
{
	try
	{
		int rc;
		std::string StringValue;
		char sQuery[256];
		int propertyCount;
		int propertyID;
		std::string key, value;


        m_pLog4cpp->debug("-----------------------------------------------------");
        m_pLog4cpp->debug("Switch::DbReadBoardInfo()");

        EnterCriticalSection(&m_CritDB);

		sprintf(sQuery, "EXEC SP_VG_GetProperty %d", boardID);

        m_pLog4cpp->debug("Query: %s", sQuery);

		rc = m_dbPtr->Execute(sQuery);

		if( rc < 0 )
		{
		    m_pLog4cpp->debug( m_dbPtr->GetLastError() );
			m_dbPtr->Close();
				
            if( m_dbPtr->Open() < 0 )
			{
                m_pLog4cpp->error( m_dbPtr->GetLastError() );
			}
            LeaveCriticalSection(&m_CritDB);
			return -1;
		}

		propertyCount = m_dbPtr->GetRowCount();

		for(propertyID = 1; propertyID <= propertyCount; propertyID++)
		{
			key = m_dbPtr->GetItem("PropertyName");
			value = m_dbPtr->GetItem("PropertyValue");

            m_pLog4cpp->debug("[PROPERTY | VALUE] : %s | %s", key.c_str(), value.c_str() );

			DbBoards[boardID].Properties[key] = value;

			// Procura por chave IPProtocol
			// Tentar tirar posteriormente essa forma de configuracao IP.
			if(key.compare("IPProtocol") == 0)
			{
				DbBoards[boardID].IPProtocol = value;
			}

			m_dbPtr->MoveNext();
		}

#ifdef _MULTICHASSIS
		if(DbBoards[boardID].ManufacturerName == "AMTELCO")
		{
				if(StRegister.MC_Multichassis_Enabled)
				{
					// Numero da placa da Amtelco, default 16 quando for a primeira LiteBoard
				Switch::StMultichassis.MC_Board_Number = atoi( DbBoards[boardID].Properties["AmtelcoBoardPhysicalID"].c_str() );
    
					// Max Range Local Timeslot
				Switch::StMultichassis.MC_Local_Ts_Max = atoi( DbBoards[boardID].Properties["LocalTsMax"].c_str() );

					// Min Range Local Timeslot
				Switch::StMultichassis.MC_Local_Ts_Min = atoi( DbBoards[boardID].Properties["LocalTsMin"].c_str() );

					// Max Range Global Timeslot
				Switch::StMultichassis.MC_Global_Ts_Max = atoi( DbBoards[boardID].Properties["GlobalTsMax"].c_str() );

					// Min Range Global Timeslot
				Switch::StMultichassis.MC_Global_Ts_Min = atoi( DbBoards[boardID].Properties["GlobalTsMin"].c_str() );
    
					// Modo de operacao do ring (fibra otica)
				if( DbBoards[boardID].Properties["RingMode"].compare("REDUNDANT") == 0 )
					{
						Switch::StMultichassis.MC_Ring_Mode = 1;
					}
					else	// EXTENDED
					{
						Switch::StMultichassis.MC_Ring_Mode = 0;
					}
    
					// Taxa de operacao do clock no bus local
				if( DbBoards[boardID].Properties["ClockRate"].compare("2MHZ") == 0 )
					{
						Switch::StMultichassis.MC_Clock_Rate = 0;
					}
				else if( DbBoards[boardID].Properties["ClockRate"].compare("8MHZ") == 0 )
					{
						Switch::StMultichassis.MC_Clock_Rate = 2;
					}
					else
					{
						Switch::StMultichassis.MC_Clock_Rate = 1;
					}

					// Modo de operacao do clock no bus local
				if( DbBoards[boardID].Properties["ClockMode"].compare("SLAVE") == 0 )
					{
						Switch::StMultichassis.MC_Clock_Mode = 1;
					}
					else
					{
						Switch::StMultichassis.MC_Clock_Mode = 2;
					}

					// Tipo do bus local
				if( DbBoards[boardID].Properties["BusType"].compare("SCBUS") == 0 )
					{
						strcpy( Switch::StMultichassis.MC_Bus_Type, "SCBUS" );
					}
					else
					{
						strcpy( Switch::StMultichassis.MC_Bus_Type, "CTBUS" );
					}

					// Tipo de codificacao de audio (ALAW / MULAW)
				if( DbBoards[boardID].Properties["Encoding"].compare("ALAW") == 0 )
					{
						Switch::StMultichassis.MC_Encoding = 'A';
					}
					else  // "MULAW"
					{
						Switch::StMultichassis.MC_Encoding = 'M';
					}					
				}
		}
#endif // _MULTICHASSIS

#ifdef _KHOMP
		if(DbBoards[boardID].ManufacturerName == "KHOMP")
		{
			if(DbBoards[boardID].Properties["BusType"].compare("SCBUS") == 0)
			{
				Switch::khompConfig.BusType = KHOMP_SCBUS;
			}
			else if(DbBoards[boardID].Properties["BusType"].compare("MVIP") == 0)
			{
				Switch::khompConfig.BusType = KHOMP_MVIP;
		}
			else if(DbBoards[boardID].Properties["BusType"].compare("H100") == 0)
			{
				Switch::khompConfig.BusType = KHOMP_H100;
			}
			else
			{
				Switch::khompConfig.BusType = KHOMP_H100;
			}

			if(DbBoards[boardID].Properties["ClockMode"].compare("MASTER") == 0)
			{
				Switch::khompConfig.ClockMode = KHOMP_MASTER;
			}
			else if(DbBoards[boardID].Properties["ClockMode"].compare("SLAVE") == 0)
			{
				Switch::khompConfig.ClockMode = KHOMP_SLAVE;
			}
			else
			{
				Switch::khompConfig.ClockMode = KHOMP_SLAVE;
			}

			if(DbBoards[boardID].Properties["ClockRate"].compare("2MHZ") == 0)
			{
				Switch::khompConfig.ClockRate = KHOMP_2MHZ;
			}
			else if(DbBoards[boardID].Properties["ClockRate"].compare("4MHZ") == 0)
			{
				Switch::khompConfig.ClockRate = KHOMP_4MHZ;
			}
			else if(DbBoards[boardID].Properties["ClockRate"].compare("8MHZ") == 0)
			{
				Switch::khompConfig.ClockRate = KHOMP_8MHZ;
			}
			else
			{
				Switch::khompConfig.ClockMode = KHOMP_8MHZ;
			}

			if(DbBoards[boardID].Properties["Encoding"].compare("PCM") == 0)
			{
				Switch::khompConfig.Encoding = KHOMP_PCM;
			}
			else if(DbBoards[boardID].Properties["Encoding"].compare("ALAW") == 0)
			{
				Switch::khompConfig.Encoding = KHOMP_ALAW;
			}
			else if(DbBoards[boardID].Properties["Encoding"].compare("MULAW") == 0)
			{
				Switch::khompConfig.Encoding = KHOMP_MULAW;
			}
			else
			{
				Switch::khompConfig.Encoding = KHOMP_ALAW;
			}

			if(DbBoards[boardID].Properties["BaseTimeslot"].empty())
			{
				Switch::khompConfig.BaseTimeslot = 0;
			}
			else
			{
				// Timeslot base p/ alocacao das placas da khomp.
				Switch::khompConfig.BaseTimeslot = atoi( DbBoards[boardID].Properties["BaseTimeslot"].c_str() );
			}
		}
#endif // _KHOMP
        LeaveCriticalSection(&m_CritDB);
		return 0;
	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
		m_pLog4cpp->error("EXCEPTION: Switch::DbReadBoardInfo()");
		return -1;
	}

}
//---------------------------------------------------------------------------------------------

int Switch::DbReadTrunks(int boardID)
{
	try
	{
		int rc, trunkRef, trunkID;
		char sQuery[256];

		m_pLog4cpp->debug("Switch::DbReadTrunks()");

        m_pLog4cpp->debug("-----------------------------------------------------");
		sprintf(sQuery, "EXEC SP_VG_GetTrunk %d", boardID);

		m_pLog4cpp->debug("Query: %s", sQuery);

        EnterCriticalSection(&m_CritDB);

		rc = m_dbPtr->Execute(sQuery);

		if( rc < 0 )
		{
			m_pLog4cpp->error(m_dbPtr->GetLastError());
			m_dbPtr->Close();
			if( m_dbPtr->Open() < 0 )
			{
				m_pLog4cpp->error(m_dbPtr->GetLastError());
			}
            LeaveCriticalSection(&m_CritDB);
			return -1;
		}

		DbBoards[boardID].TrunkCount = m_dbPtr->GetRowCount();

		NumTrunksInDataBase += DbBoards[boardID].TrunkCount;

        m_pLog4cpp->debug("BOARD ID | TRUNK COUNT : %d | %d " , boardID , NumTrunksInDataBase );
		
		//DbTrunks[trunkID].TrunkRef = 0;
		for(trunkRef = 1; trunkRef <= m_dbPtr->GetRowCount(); trunkRef++)
		{
			trunkID = atoi( m_dbPtr->GetItem("TrunkID") );
			DbTrunks[trunkID].TrunkRef = trunkRef;
			DbTrunks[trunkID].BoardID = boardID;
			DbTrunks[trunkID].TrunkTypeName = m_dbPtr->GetItem("TrunkTypeName");
			DbTrunks[trunkID].TrunkSignalingName = m_dbPtr->GetItem("TrunkSignalingName");

			if (DbTrunks[trunkID].TrunkTypeName == "ANALOG")
				DbTrunks[trunkID].TrunkType = LSI;
			else if (DbTrunks[trunkID].TrunkTypeName == "E1")
				DbTrunks[trunkID].TrunkType = E1;
			else if (DbTrunks[trunkID].TrunkTypeName == "E1 SS7")
				DbTrunks[trunkID].TrunkType = E1;
			else if (DbTrunks[trunkID].TrunkTypeName == "T1")
				DbTrunks[trunkID].TrunkType = T1;
			else if (DbTrunks[trunkID].TrunkTypeName == "T1")
				DbTrunks[trunkID].TrunkType = T1;
			else if (DbTrunks[trunkID].TrunkTypeName == "IP")
				DbTrunks[trunkID].TrunkType = IP;
			else if (DbTrunks[trunkID].TrunkTypeName == "FAX")
				DbTrunks[trunkID].TrunkType = FAX;

			if(DbBoards[boardID].ManufacturerName == "DIALOGIC")
			{
				// DM3
				if(DbBoards[boardID].FamilyName == "DM3")
				{
					// LOOP START
					if(DbTrunks[trunkID].TrunkSignalingName == "LOOP START")
					{
						DbTrunks[trunkID].TrunkSignaling = LOOP_START;
						DbTrunks[trunkID].TrunkProtocol = "LOOP START";
					}
					// LOOP START TRAP
					else if(DbTrunks[trunkID].TrunkSignalingName == "LOOP START TRAP")
					{
						DbTrunks[trunkID].TrunkSignaling = LOOP_START_TRAP;
						DbTrunks[trunkID].TrunkProtocol = "LOOP START TRAP";
					}
					// SS7
					else if(DbTrunks[trunkID].TrunkSignalingName == "SS7")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_DM3;
						if (DbTrunks[trunkID].TrunkTypeName == "E1 SS7")
							DbTrunks[trunkID].TrunkProtocol = "SS7_dk";
						else
							DbTrunks[trunkID].TrunkProtocol = "SS7_dti";
					}
					// LINE SIDE
					else if(DbTrunks[trunkID].TrunkSignalingName == "LINE SIDE")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_DM3;
						DbTrunks[trunkID].TrunkProtocol = "pdk_sw_e1_luls_io";
					}
					// R2MF
					else if(DbTrunks[trunkID].TrunkSignalingName == "R2MF")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_DM3;
						DbTrunks[trunkID].TrunkProtocol = "pdk_br_r2_io";
					}
					// ISDN
					else if(DbTrunks[trunkID].TrunkSignalingName == "ISDN")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_DM3;
						DbTrunks[trunkID].TrunkProtocol = "isdn";
					}
					// SIP
					else if(DbTrunks[trunkID].TrunkSignalingName == "SIP")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_IP;
						DbTrunks[trunkID].TrunkProtocol = "SIP";
					}
					// H323
					else if(DbTrunks[trunkID].TrunkSignalingName == "H323")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_IP;
						DbTrunks[trunkID].TrunkProtocol = "H323";
					}
					// OUTROS
					else
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_DM3;
						DbTrunks[trunkID].TrunkProtocol = DbTrunks[trunkID].TrunkSignalingName;
					}
				}
				// SPRINGWARE
				else
				{

					// LOOP START
					if(DbTrunks[trunkID].TrunkSignalingName == "LOOP START")
					{
						DbTrunks[trunkID].TrunkSignaling = LOOP_START;
						DbTrunks[trunkID].TrunkProtocol = "LOOP START";
					}
					// LOOP START TRAP
					else if(DbTrunks[trunkID].TrunkSignalingName == "LOOP START TRAP")
					{
						DbTrunks[trunkID].TrunkSignaling = LOOP_START_TRAP;
						DbTrunks[trunkID].TrunkProtocol = "LOOP START TRAP";
					}
					// SS7
					else if(DbTrunks[trunkID].TrunkSignalingName == "SS7")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_SS7;
						if (DbTrunks[trunkID].TrunkTypeName == "E1 SS7")
							DbTrunks[trunkID].TrunkProtocol = "SS7_dk";
						else
							DbTrunks[trunkID].TrunkProtocol = "SS7_dti";
					}
					// LINE SIDE
					else if(DbTrunks[trunkID].TrunkSignalingName == "LINE SIDE")
					{
						DbTrunks[trunkID].TrunkSignaling = LINE_SIDE;
						DbTrunks[trunkID].TrunkProtocol = "pdk_sw_e1_luls_io";
					}
					// R2MF
					else if(DbTrunks[trunkID].TrunkSignalingName == "R2MF")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL;
						DbTrunks[trunkID].TrunkProtocol = "pdk_br_r2_io";
					}
					// ISDN
					else if(DbTrunks[trunkID].TrunkSignalingName == "ISDN")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL;
						DbTrunks[trunkID].TrunkProtocol = "ISDN";

                        m_pLog4cpp->debug("Board: %s (ID:%02d), BoardFamily: %s, TrunkSignaling: GLOBAL_CALL", 
                            DbBoards[boardID].BoardName.c_str(), 
                            DbTrunks[trunkID].BoardID, 
                            DbBoards[boardID].FamilyName.c_str() );
					}
					// SIP
					else if(DbTrunks[trunkID].TrunkSignalingName == "SIP")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_IP;
						DbTrunks[trunkID].TrunkProtocol = "SIP";
					}
					// H323
					else if(DbTrunks[trunkID].TrunkSignalingName == "H323")
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL_IP;
						DbTrunks[trunkID].TrunkProtocol = "H323";
					}
					// OUTROS
					else
					{
						DbTrunks[trunkID].TrunkSignaling = GLOBAL_CALL;
						DbTrunks[trunkID].TrunkProtocol = DbTrunks[trunkID].TrunkSignalingName;
					}
				}
			}
#ifdef _KHOMP
			else if(DbBoards[boardID].ManufacturerName == "KHOMP")
			{
				// SIP
				if(DbTrunks[trunkID].TrunkSignalingName == "SIP")
				{
					DbTrunks[trunkID].TrunkSignaling = KHOMP_SIP;
					DbTrunks[trunkID].TrunkProtocol = "SIP";
				}
			}
#endif // _KHOMP
			else if(DbBoards[boardID].ManufacturerName == "AMTELCO")
			{
                LeaveCriticalSection(&m_CritDB);
				m_pLog4cpp->error("ERROR: Invalid configuration for Amtelco board");
				return -1;
			}

            m_pLog4cpp->debug("Board: %s (ID:%02d), BoardFamily: %s, TrunkType: %s, TrunkSignalingName: %s", 
                DbBoards[boardID].BoardName.c_str(), DbTrunks[trunkID].BoardID, DbBoards[boardID].FamilyName.c_str(), 
                DbTrunks[trunkID].TrunkTypeName.c_str(), DbTrunks[trunkID].TrunkProtocol.c_str());

			m_dbPtr->MoveNext();
		}

        LeaveCriticalSection(&m_CritDB);

		return 0;

	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
        m_pLog4cpp->error("EXCEPTION: Switch::DbReadTrunks()");
		return -1;
	}

}

//---------------------------------------------------------------------------------------------

int Switch::DbReadChannels(int boardID, int trunkID, int trunkRef)
{
	try
	{
		int rc, index, ChannelID;
		int board = 0;
		char sQuery[256];

		m_pLog4cpp->debug("[%s]",__FUNCTION__);    

		sprintf(sQuery, "EXEC SP_VG_GetChannel %d, %d", boardID, trunkID);

		m_pLog4cpp->debug("Query: %s", sQuery);

		EnterCriticalSection(&m_CritDB);

        DbBoards[boardID].ChannelCount = 0;
		rc = m_dbPtr->Execute(sQuery);

		if( rc < 0 )
		{
			m_pLog4cpp->error( m_dbPtr->GetLastError());
			m_dbPtr->Close();
			if( m_dbPtr->Open() < 0 )
			{
				m_pLog4cpp->error(m_dbPtr->GetLastError());
			}
			LeaveCriticalSection(&m_CritDB);
			return -1;
		}

		NumPortsInDataBase += m_dbPtr->GetRowCount();
		DbBoards[boardID].ChannelCount += m_dbPtr->GetRowCount();
		
		for(index = 0; index < m_dbPtr->GetRowCount(); index++)
		{
			ChannelID	= atoi(	m_dbPtr->GetItem("ChannelID") );
			DbChannels[ChannelID].BoardID = atoi(	m_dbPtr->GetItem("BoardID") );
			DbChannels[ChannelID].TrunkID	= atoi(	m_dbPtr->GetItem("TrunkID") );
			DbChannels[ChannelID].BoardTrunkID	= trunkRef;
			DbChannels[ChannelID].TrunkChannelID = atoi(	m_dbPtr->GetItem("TrunkChannelID") );
		    strcpy(&DbChannels[ChannelID].Direction, m_dbPtr->GetItem("Direction"));
			DbChannels[ChannelID].Reserved = atoi(	m_dbPtr->GetItem("Reserved") ) ? true : false;
			DbChannels[ChannelID].AsrEnabled = atoi(	m_dbPtr->GetItem("AsrEnabled") ) ? true : false;
			DbChannels[ChannelID].Enabled = atoi( m_dbPtr->GetItem("Enabled") ) ? true : false;

            m_pLog4cpp->debug("ChannelID : %d", ChannelID);
            m_pLog4cpp->debug("\tBoardID          : %d", DbChannels[ChannelID].BoardID);
            m_pLog4cpp->debug("\tTrunkID          : %d", DbChannels[ChannelID].TrunkID);
            m_pLog4cpp->debug("\tTrunkChannelID   : %d", DbChannels[ChannelID].TrunkChannelID);            
            m_pLog4cpp->debug("\tDirection        : %c", DbChannels[ChannelID].Direction);
            m_pLog4cpp->debug("\r\n");


			m_dbPtr->MoveNext();
		}

		LeaveCriticalSection(&m_CritDB);

		return 0;

	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
		m_pLog4cpp->error("EXCEPTION: Switch::DbReadTrunks()");
		return -1;
	}

}

//---------------------------------------------------------------------------------------------

int Switch::DbReadResources(int boardID)
{
	try
	{
		int rc, ResourceIndex;
		char sQuery[256];
		DB_RESOURCE DbResources;

		m_pLog4cpp->debug("Switch::DbReadResources()");

		sprintf(sQuery, "EXEC SP_VG_GetResource %d", boardID);

        m_pLog4cpp->debug("Query: %s", sQuery);

        EnterCriticalSection(&m_CritDB);

		rc = m_dbPtr->Execute(sQuery);

		if( rc < 0 )
		{
			m_pLog4cpp->error(m_dbPtr->GetLastError());
			m_dbPtr->Close();
			if( m_dbPtr->Open() < 0 )
			{
				m_pLog4cpp->error(m_dbPtr->GetLastError());
			}
			LeaveCriticalSection(&m_CritDB);
			return -1;
		}

		for(ResourceIndex = 0; ResourceIndex < m_dbPtr->GetRowCount(); ResourceIndex++)
		{
			DbResources.ResourceID			=	atoi(	m_dbPtr->GetItem("ResourceID")	);
			DbResources.ResourceTypeID		=	atoi(	m_dbPtr->GetItem("ResourceTypeID")	);
			DbResources.ResourceTypeName	=	m_dbPtr->GetItem("ResourceTypeName");
			DbResources.ResourceCount		=   atoi(	m_dbPtr->GetItem("ResourceCount")	);
			DbResources.Enabled				=   atoi(	m_dbPtr->GetItem("Enabled")	) ? true : false;

		    DbBoards[boardID].Resources.push_back(DbResources);

			m_dbPtr->MoveNext();

		}

		DbBoards[boardID].ResourceCount = m_dbPtr->GetRowCount();

        LeaveCriticalSection(&m_CritDB);

		return 0;

	}
	catch(...)
	{
		LeaveCriticalSection(&m_CritDB);
        m_pLog4cpp->error("EXCEPTION: Switch::DbReadResources()");
		return -1;
	}

}

void Switch::StopPortThreads()
{
    boost::shared_ptr<Ivr> ivrPtr;
    _ThreadParam ThreadParam;
    int FakePort = 0;
	int MsiBoardCount = 1;
	int MsiPortCount = 1;
	int Dialogic_FaxPortCount		= 0;
	int ValidPortsCountToAnswer	= 1;
	int FaxCount = 1;
	int board, resource, _channel;
	bool bAsr;
	int trunkID;

    try 
	{
        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

        if ( m_Cdr.get() != NULL )
            m_Cdr->ThreadEndSystem();

        for(_channel = 1; _channel <= NumPortsInDataBase; _channel++)
		{
			board = DbChannels[_channel].BoardID;
			trunkID = DbChannels[_channel].TrunkID;


			// Inicializa o status dos canais
			if( DbChannels[_channel].Enabled == false )
				DbChannels[_channel].Status = PORT_INACTIVE;
			else
			if( DbChannels[_channel].Reserved == true )
				DbChannels[_channel].Status = PORT_RESERVED;
			else
				DbChannels[_channel].Status = PORT_NOT_INIT;
			
			if( DbChannels[_channel].Enabled )			
            {
                ThreadParam.Board = board;
				ThreadParam.Port = _channel;
				
                Ivr * ivrPtr = m_MapChanIvr[_channel];                

                if ( ivrPtr != NULL ) 
                {
                    m_pLog4cpp->debug("Going to Stop Ivr instance for Channel %d", _channel);
                    ivrPtr->Stop();
                    //m_pLog4cpp->debug("Going to delete Ivr instance for Channel %d", _channel);
                    //delete ivrPtr;
                }
			}
		}
    }
    catch(...)
	{
		m_pLog4cpp->debug("EXCEPTION: Switch::StartPortThreads()");
	}
}

void Switch::StartPortThreads()
{
    boost::shared_ptr<Ivr> ivrPtr;
    _ThreadParam ThreadParam;
    int FakePort = 0;
	int MsiBoardCount = 1;
	int MsiPortCount = 1;
	int Dialogic_FaxPortCount		= 0;
	int ValidPortsCountToAnswer	= 1;
	int FaxCount = 1;
	int board, resource, channel;
	bool bAsr;
	int trunkID;

    _BoardInfo BoardInfo;
	vector< _BoardInfo > ConferenceBoards;
	int	DCBBoardCount = 0;

    try 
	{
        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    
        // cria evento de inicializacao das threads, cada thread criada,
	    // seta este evento para que a proxima thread seja criada
	    ThreadParam.EventCreate = CreateEvent (0, FALSE, FALSE, NULL);
		
	    // Reseta evento de criacao de threads
	    ResetEvent(ThreadParam.EventCreate);

	    // Cria a thread que controla a grava��o dos arquivos de CDR's
        m_Cdr.reset(new Cdr(m_pLog4cpp, this) );        

        m_Cdr->ThreadInitSystem();        
                

        //	    Switch::Thread(CdrThread, &ThreadParam);
	    // espera pelo termino da criacao da thread para criar outra
//	    WaitForSingleObject(ThreadParam.EventCreate, INFINITE);


#if _CONFERENCE || _MULTICHASSIS
	    // Define a faixa de timeslots Globais
	    TS_Manager.SetGlobalTsRange( StMultichassis.MC_Global_Ts_Min, StMultichassis.MC_Global_Ts_Max);
	    // Define a faixa de timeslots Locais
	    TS_Manager.SetLocalTsRange( StMultichassis.MC_Local_Ts_Min, StMultichassis.MC_Local_Ts_Max);
#endif // _CONFERENCE || _MULTICHASSIS

        // Varre a estrutura de placas
	    for(board = 1; board <= NumBoardsInDataBase; board++)
	    {
            m_pLog4cpp->debug("[%p][%s] Opening %s", this, __FUNCTION__ , DbBoards[board].BoardName.c_str());
		    
							
		    // Varre a estrutura de recursos
		    DbBoards[board].IPResource = false;
		    for(resource = 0; resource < DbBoards[board].ResourceCount; resource++)
		    {
			    if(DbBoards[board].Resources[resource].ResourceTypeName == "FAX")
			    {	
				    if(DbBoards[board].Resources[resource].Enabled)
				    {
					    if(DbBoards[board].ManufacturerName == "DIALOGIC")
						    Dialogic_FaxPortCount += DbBoards[board].Resources[resource].ResourceCount;
				    }
			    }
			    else if(DbBoards[board].Resources[resource].ResourceTypeName == "CONF")
			    {
				    if(DbBoards[board].Resources[resource].Enabled)
				    {
					    if(DbBoards[board].ManufacturerName == "DIALOGIC")
					    {
						    DCBBoardCount++;
						    BoardInfo.Id = DCBBoardCount;
					    }
					    else
						    BoardInfo.Id = atoi(DbBoards[board].Properties["AmtelcoBoardPhysicalID"].c_str());

					    BoardInfo.BoardName = DbBoards[board].BoardName;
					    ConferenceBoards.push_back( BoardInfo );						
				    }
			    }
			    else if(DbBoards[board].Resources[resource].ResourceTypeName == "IP")
				    DbBoards[board].IPResource = true;
		    }
	    }

		// Cria as threads de conferencia
		if( ConferenceBoards.size() > 0 )
		{
			ThreadParam.BoardInfo = ConferenceBoards;
			// Existe placa DCB no sistema, cria thread para controle
			// Reseta evento de criacao de threads
			ResetEvent(ThreadParam.EventCreate);


			// Cria thread para controlar canal analogico ou digital
			//Switch::Thread(Thread_Conference, &ThreadParam);

            m_pConferenceManager = new ConferenceManagerClass(&ThreadParam, this , m_pLog4cpp );

			// espera pelo termino da criacao da thread para criar outra
			WaitForSingleObject(ThreadParam.EventCreate, INFINITE);
		}

		memset(ThreadParam.FaxPorts, 0, sizeof(ThreadParam.FaxPorts));

		// Cria as threads dos canais
		for(channel = 1; channel <= NumPortsInDataBase; channel++)
		{
			board = DbChannels[channel].BoardID;
			trunkID = DbChannels[channel].TrunkID;

			if( (DbBoards[board].BoardName == "D600JCT") && (DbChannels[channel].TrunkChannelID == 1) )
			{
				bAsr = false;
				for(resource = 0; resource < DbBoards[board].ResourceCount; resource++)
				{
					if(DbBoards[board].Resources[resource].ResourceTypeName == "ASR")
						bAsr = true;
				}

				if( bAsr )
				{
					// Aloca os recursos dos 30 primeiros canais da D600JCT para reconhecimento de voz
					for(FakePort = channel; FakePort < (channel + 32); FakePort++)
					{	
                        m_Tapi->InitSystem();
                        std::string sTypeClass = typeid(m_Tapi->GetCTI()).name();
                        if ( !sTypeClass.compare("Dialogic") )
						    dynamic_cast<Dialogic *>(m_Tapi->GetCTI())->FakeOpenChannel(FakePort, VOX);
						
					}
				}
			}

			// Inicializa o status dos canais
			if( DbChannels[channel].Enabled == false )
				DbChannels[channel].Status = PORT_INACTIVE;
			else
			if( DbChannels[channel].Reserved == true )
				DbChannels[channel].Status = PORT_RESERVED;
			else
				DbChannels[channel].Status = PORT_NOT_INIT;

            m_pLog4cpp->debug("[%p][%s] Going to check DbChannels[%d].Enabled", this, __FUNCTION__, channel);

			if( !DbChannels[channel].Enabled )
			{
				if(	DbBoards[board].FamilyName == "DM3" )		// DM3 (DTI + VOX/CSP)
				{					
                    m_Tapi->InitSystem();
                    std::string sTypeClass = typeid(m_Tapi->GetCTI()).name();
                    if ( !sTypeClass.compare("Dialogic") )
					    dynamic_cast<Dialogic *>(m_Tapi->GetCTI())->FakeOpenChannel(channel, DbTrunks[trunkID].TrunkType, GLOBAL_CALL_DM3);
					
				}
				else
				{
					
                    m_Tapi->InitSystem();
                    std::string sTypeClass = typeid(m_Tapi->GetCTI()).name();
                    if ( !sTypeClass.compare("Dialogic") )
                    {
                        m_pLog4cpp->debug("Going to FakeOpen %s",DbBoards[board].FamilyName.c_str() );

					    dynamic_cast<Dialogic *>(m_Tapi->GetCTI())->FakeOpenChannel(channel, DbTrunks[trunkID].TrunkType);
                    }
					
				}
			}
			else
			{
                m_pLog4cpp->debug("[%p][%s] DbChannels[channel].Enabled TRUE", this, __FUNCTION__);
#ifdef _FAX
				if( DbTrunks[trunkID].TrunkType == FAX)
				{
					// Guarda o numero do canal de FAX em relacao ao banco de dados
					ThreadParam.FaxPorts[FaxCount++] = channel;
					DbChannels[channel].FaxResource	= true;				
					if(DbBoards[board].ManufacturerName == "DIALOGIC")
					{
						if( FaxCount > DbBoards[board].TrunkCount * DbBoards[board].ChannelCount )
						{
							// Existe placa de FAX Dialogic no sistema, cria thread para controle
							ThreadParam.FaxIni			= 1;
							ThreadParam.FaxEnd			= (DbBoards[board].TrunkCount * DbBoards[board].ChannelCount);
							ThreadParam.Tecnologia	= FAX_DIALOGIC;
							// Reseta evento de criacao de threads
							ResetEvent(ThreadParam.EventCreate);
							// Cria thread para controlar canal analogico ou digital
							Switch::Thread(Thread_Fax, &ThreadParam);
							// espera pelo termino da criacao da thread para criar outra
							WaitForSingleObject(ThreadParam.EventCreate, INFINITE);
						}
					}
				}
				else
#endif // _FAX

                /*
				{
					ThreadParam.Board = board;

					//Cria as threads dos canais para todas as placas
					ThreadParam.Port = channel;
					// Reseta evento de criacao de threads
					ResetEvent(ThreadParam.EventCreate);
					// Cria thread para controlar canal analogico ou digital
					Thread(Thread_Ivr, &ThreadParam);
					// espera pelo termino da criacao da thread para criar outra
					WaitForSingleObject(ThreadParam.EventCreate, INFINITE);
					// Incrementa o contador de portas validas para atendimento
					ValidPortsCountToAnswer++;
				}
                */

                m_pLog4cpp->debug("Going to instantiate Ivr()...");

                ThreadParam.Board = board;

				//Cria as threads dos canais para todas as placas
				ThreadParam.Port = channel;

				// Reseta evento de criacao de threads
                m_pLog4cpp->debug("[%p][%s] RESET EventCreate ", this, __FUNCTION__);
				ResetEvent(ThreadParam.EventCreate);                

                Ivr * ivrPtr = new Ivr(this, &ThreadParam, m_StRegister.BasePathLog , m_StRegister.LogMaxFileSize);

                m_MapChanIvr.insert(std::make_pair(channel, ivrPtr) );

                // espera pelo termino da criacao da thread para criar outra
				WaitForSingleObject(ThreadParam.EventCreate, INFINITE);
                m_pLog4cpp->debug("[%p][%s] NEXT IVR ", this, __FUNCTION__);
                m_pLog4cpp->debug("========================");

                // Incrementa o contador de portas validas para atendimento
				ValidPortsCountToAnswer++;
			}
		}

#ifdef _FAX
		if(Dialogic_FaxPortCount)
		{
			for(channel = NumPortsInDataBase + 1; channel <= NumPortsInDataBase + Dialogic_FaxPortCount; channel++)
			{
				// Guarda o numero do canal de FAX em relacao ao banco de dados
				ThreadParam.FaxPorts[FaxCount++] = channel;
				DbChannels[channel].Enabled = true;
				DbChannels[channel].Status = PORT_NOT_INIT;
				DbChannels[channel].FaxResource	= true;				
			}
			// Existe placa de FAX Dialogic no sistema, cria thread para controle
			ThreadParam.FaxIni			= 1;
			ThreadParam.FaxEnd			= Dialogic_FaxPortCount;
			ThreadParam.Tecnologia	= FAX_DIALOGIC;
			// Reseta evento de criacao de threads
			ResetEvent(ThreadParam.EventCreate);
			// Cria thread para controlar canal analogico ou digital
			Switch::Thread(Thread_Fax, &ThreadParam);
			// espera pelo termino da criacao da thread para criar outra
			WaitForSingleObject(ThreadParam.EventCreate, INFINITE);
		}
#endif // _FAX

        m_pLog4cpp->debug("NumPortsInDataBase : %d ", NumPortsInDataBase);		
    }
    catch(...)
	{
		m_pLog4cpp->debug("EXCEPTION: Switch::StartPortThreads()");
	}
}

int Switch::GetBoardId(int p_Channel) const
{
    return DbChannels[p_Channel].BoardID;
}

int Switch::GetTrunkId(int p_Channel) const
{
    return DbChannels[p_Channel].TrunkID;
}

int Switch::GetTrunkChannelID(int p_Channel) const
{
    return DbChannels[p_Channel].TrunkChannelID;
}

int Switch::GetBoardTrunkID(int p_Channel) const
{
    return DbChannels[p_Channel].BoardTrunkID;
}

const std::string & Switch::GetFullLogPathName() const
{
    return m_sFullLogPathName;
}

int Switch::GetSwitch() const
{
    return m_StRegister.Switch;
}

int Switch::GetMachine() const
{
    return m_StRegister.Machine;
}

int Switch::GetIndexScreen() const
{
    return IndexScreen;
}

void Switch::SetIndexScreen(int value)
{
    IndexScreen = value;
}

int Switch::GetNumPortsInDataBase()
{
    return NumPortsInDataBase;
}

std::string Switch::GetManufacturerName(int p_Channel) const
{
    return DbBoards[DbChannels[p_Channel].BoardID].ManufacturerName;
}

std::string Switch::GetTrunkSignalingName(int p_TrunkId) const
{
    return DbTrunks[p_TrunkId].TrunkSignalingName;
}

int Switch::GetTrunkSignaling(int p_TrunkId) const
{
    return DbTrunks[p_TrunkId].TrunkSignaling;
}

std::string Switch::GetTrunkProtocol(int p_TrunkId) const
{
    return DbTrunks[p_TrunkId].TrunkProtocol;
}

std::string Switch::GetBasePathLog() const
{
    std::string sRet;

    sRet.assign(m_StRegister.BasePathLog);
    return sRet;
}

int Switch::GetIdAudio() const
{
    return IdAudio;
}

void Switch::SetAudioTimeslots_Channel(const int & p_IdAudio , const int & p_iChannel)
{
    AudioTimeslots[p_IdAudio].Channel = p_iChannel;
}

void Switch::SetAudioTimeslots_LocalTimeSlot(const int & p_IdAudio , const int & p_iLocalTimeSlot)
{
    AudioTimeslots[p_IdAudio].LocalTimeslot = p_iLocalTimeSlot;
}

void Switch::SetAudioTimeslots_GlobalTimeSlot(const int & p_IdAudio , const int & p_iGlobalTimeSlot)
{
    AudioTimeslots[p_IdAudio].GlobalTimeslot = p_iGlobalTimeSlot;
}

int Switch::GetChannelsReserverd(int p_Channel) const
{
    return DbChannels[p_Channel].Reserved;
}

char Switch::GetChannelsDirection(int index)
{
    return DbChannels[index].Direction;
}

int Switch::GetChannelsStatus(int p_Channel) 
{
    int i = 0;
    EnterCriticalSection(&m_CritStatus);		
    i = DbChannels[p_Channel].Status;
    LeaveCriticalSection(&m_CritStatus);
    return i;    
}

void Switch::SetChannelsStatus(int p_Channel, const int & p_Status)
{
    EnterCriticalSection(&m_CritStatus);		
    DbChannels[p_Channel].Status = p_Status;
    LeaveCriticalSection(&m_CritStatus);
}

int Switch::GetFlagLogScreen() const
{
    return m_StRegister.FlagLogScreen;
}

bool Switch::GetFlagEndProgram() const
{
    return m_stop;
}

std::string Switch::GetPathPrompts() const
{
    return m_StRegister.PathPrompts;
}

int Switch::GetR2MinDnis() const
{
    return m_StRegister.iR2MinDnis;
}

int Switch::GetR2MaxDnis() const
{
    return m_StRegister.iR2MaxDnis;
}

int Switch::GetLogCDR() const
{
    return m_StRegister.LogCDR;
}

std::string Switch::DbGetCallId(std::string sQuery)
{
    char CallId[40];

    memset(CallId,0x00,sizeof(CallId) );
    EnterCriticalSection(&m_CritDB);

		// Executa a procedure
    if( m_dbPtr->Execute(sQuery.c_str()) )
	{
        m_pLog4cpp->error( m_dbPtr->GetLastError());
        m_dbPtr->Close();
		if( m_dbPtr->Open() < 0 )
		{
			m_pLog4cpp->error( m_dbPtr->GetLastError());
		}
        LeaveCriticalSection(&m_CritDB);
		//Switch::CritDB.Leave();
		return CallId;
	}

	if(m_dbPtr->GetRowCount() <= 0)
	{
		strcpy(CallId, "0");
	}
	else
	{
		strcpy(CallId, m_dbPtr->GetItem(0));
        //boost::algorithm::trim(CallId);
		//StripSpace(CallId);
	}


    LeaveCriticalSection(&m_CritDB);
	//Switch::CritDB.Leave();
    return CallId;
}

std::string Switch::GetFullUdlPath()
{
    boost::filesystem::path fsFullUdlFilePath;
    boost::filesystem::path  fsLogName("VoiceGateway.udl");
    fsFullUdlFilePath = m_fsCurrentPath / fsLogName;

    return fsFullUdlFilePath.string();
}


std::string Switch::GetPathScripts() const
{
    return m_StRegister.PathScripts;
}

int Switch::GetTimeoutDial()
{
    return m_StRegister.TimeOutDial;
}

void Switch::SetFaxCharSize(int value)
{
    iFaxCharSize = value;
}

void Switch::SetFaxLineWidth(int value)
{
    iFaxLineWidth = value;
}

int Switch::GetMaxIvrDbConnection()
{
    return m_StRegister.iMaxIvrDbConnection;
}

WORD Switch::GetIdTypeApp()
{
    return m_StRegister.IdTypeApp;
}

int Switch::GetImportCdrDelay()
{
    return m_StRegister.ImportCdrDelay;
}

boost::shared_ptr<Cdr> Switch::GetCdr()
{
    return m_Cdr;
}

std::string Switch::GetLocalIpAddress()
{
    std::string sMachineName;
    char sLocalIpAddress[256];

    try {

		HOSTENT *hostent;
		SOCKADDR_IN sockaddr_in;
        memset(sLocalIpAddress,0x00,sizeof(sLocalIpAddress));

		hostent = gethostbyname("localhost");

		sMachineName = hostent->h_name;

		hostent = gethostbyname(sMachineName.c_str());

		sockaddr_in.sin_addr = *((LPIN_ADDR)*hostent->h_addr_list);

		sprintf(sLocalIpAddress, "%d.%d.%d.%d", 
															sockaddr_in.sin_addr.S_un.S_un_b.s_b1,
															sockaddr_in.sin_addr.S_un.S_un_b.s_b2,
															sockaddr_in.sin_addr.S_un.S_un_b.s_b3,
															sockaddr_in.sin_addr.S_un.S_un_b.s_b4);
        m_sLocalIpAddress.assign(sLocalIpAddress);
        //m_pLog4cpp->debug("[%s] LOCAL IP ADDRESS : %s" , __FUNCTION__ , sLocalIpAddress );
	}
	catch(...)
    {
		m_pLog4cpp->debug("EXCEPTION: Switch::GetMachineNameIp()");
	}

    return m_sLocalIpAddress;
}

std::string Switch::GetOdbcDataSourceName() const
{
    std::string str;

    str.assign(m_StRegister.OdbcDataSourceName);
    return str;
}

std::string Switch::GetOdbcDataBase() const
{
    std::string str;

    str.assign(m_StRegister.OdbcDataBase);
    return str;
}

std::string Switch::GetOdbcUser() const
{
    std::string str;

    str.assign( m_StRegister.OdbcUser);
    return str;
}

std::string Switch::GetOdbcPassWord() const
{
    std::string str;

    str.assign( m_StRegister.OdbcPassWord );
    return str;
}

int Switch::GetNumBoardsInDataBase()
{
    return NumBoardsInDataBase;
}

std::string Switch::GetBoardResourceTypeName(int p_board , int p_resource)
{
    return DbBoards[p_board].Resources[p_resource].ResourceTypeName;
}

int Switch::GetBoardResourceCount(int p_board , int p_resource)
{
    return DbBoards[p_board].Resources[p_resource].ResourceCount;
}

int Switch::GetResourceCount(int p_board)
{
    return DbBoards[p_board].ResourceCount;
}

int Switch::GetTrunkCount(int p_board)
{
    return DbBoards[p_board].TrunkCount;
}

std::string Switch::GetBoardName(int p_board)
{
    return DbBoards[p_board].BoardName;
}

int Switch::GetChannelCount(int p_board)
{
    return DbBoards[p_board].ChannelCount;
}

int Switch::GetTrunksRef(int p_trunkId)
{
    return DbTrunks[p_trunkId].TrunkRef;
}

int Switch::GetTrunkType(int p_trunkId)
{
    return DbTrunks[p_trunkId].TrunkType;
}

bool Switch::GetIPResource(int p_boardId)
{
    return DbBoards[p_boardId].IPResource;
}

std::string Switch::GetIPProtocol(int p_boardId)
{
    return DbBoards[p_boardId].IPProtocol;
}

bool Switch::IsChannelEnabled(int p_channel)
{
    return DbChannels[p_channel].Enabled;
}

int Switch::WinStatusChannel( int Channel, std::string& ani, std::string& dnis )
{

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
	int iBoardID = DbChannels[Channel].BoardID;
	CTIBase* cti = NULL;
    Ivr * p_Ivr = NULL;
	if(DbBoards[iBoardID].ManufacturerName == "DIALOGIC")	
	{
        m_pLog4cpp->debug("[%p][%s] Going To Get Ivr for Channel %d", this, __FUNCTION__, Channel);

        p_Ivr = GetIvr(Channel);
        if ( p_Ivr != NULL )
        {
            cti = p_Ivr->GetTapi()->GetCTI()->OBJ(Channel);
        }
	}
	
    if ( cti != NULL )
    {
	    switch( cti->GetChannelStatus() )
	    {
		    case CHANNEL_IDLE:
                m_pLog4cpp->debug("[%p][%s] return ICON_IDLEIN", this, __FUNCTION__);
			    return ICON_IDLEIN;
		    case CHANNEL_BLOCKED:
                m_pLog4cpp->debug("[%p][%s] return ICON_BLOCKED", this, __FUNCTION__);
			    return ICON_BLOCKED;
		    case CHANNEL_DISABLED:
                m_pLog4cpp->debug("[%p][%s] return ICON_INACTIVE", this, __FUNCTION__);
			    return ICON_INACTIVE;
		    case CHANNEL_OFFERED:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_OFFERED", this, __FUNCTION__);
			    return ICON_OFFERED;
		    case CHANNEL_CONNECTED:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_CONNECTED", this, __FUNCTION__);
			    return ICON_CONNECTED;
		    case CHANNEL_ACCEPTED:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_OFFERED", this, __FUNCTION__);
			    return ICON_OFFERED;
		    case CHANNEL_ANSWERED:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_ANSWERED", this, __FUNCTION__);
			    return ICON_ANSWERED;
		    case CHANNEL_ALERTING:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_ALERTING", this, __FUNCTION__);
			    return ICON_ALERTING;
		    case CHANNEL_DISCONNECTED:
                m_pLog4cpp->debug("[%p][%s] return ICON_HANGUP", this, __FUNCTION__);
			    return ICON_HANGUP;
		    case CHANNEL_CALLSTATUS:
                m_pLog4cpp->debug("[%p][%s] return ICON_IDLEIN", this, __FUNCTION__);
			    return ICON_IDLEIN;
		    case CHANNEL_DIALING:
			    ani = cti->GetAni();
			    dnis = cti->GetDnis();
                m_pLog4cpp->debug("[%p][%s] return ICON_ALERTING", this, __FUNCTION__);
			    return ICON_ALERTING;
			    //return BLOCKED;
	    }
    }

	return ICON_IDLEIN;
}

const char* Switch::StatusChannel(int Status)
{
	try
	{
        //m_pLog4cpp->debug("[%p][%s] Status = %d", this, __FUNCTION__, Status);
		switch(Status)
		{
			case ICON_IDLEIN:
				return "IDLE IN";
			case ICON_IDLEOUT:
				return "IDLE OUT";
			case ICON_INACTIVE:
				return "INACTIVE";
			case ICON_OFFERED:
				return "OFFERED";
			case ICON_CONNECTED:
				return "CONNECTED";
			case ICON_ANSWERED:
				return "ANSWERED";			
			case ICON_HANGUP:
				return "HANGUP";
			case ICON_DROPCALL:
				return "DROPCALL";
			case ICON_BLOCKED:
				return "BLOCKED";
			case ICON_ALERTING:
				return "ALERTING";
			case ICON_DIALING:
				return "DIALING";
			case ICON_RECORDING:
				return "RECORDING";
			case ICON_RESERVED:
				return "RESERVED";
			case ICON_LOCKED:
				return "LOCKED";
			default:
				return "";
		}
	}
	catch(...){
	}
}


//boost::shared_ptr<ThrListen> Switch::GetThrListen() const
ThrListen * Switch::GetThrListen() const
{
    return m_pThrListen;
}

boost::shared_ptr<Tapi> Switch::GetTapi()
{
    return m_Tapi;
}

Ivr * Switch::GetIvr(int p_channel)
{
    Ivr * p_Ivr = NULL;
    std::map<int, Ivr *>::iterator it = m_MapChanIvr.find(p_channel);
    if ( it != m_MapChanIvr.end() )
    {
        p_Ivr = (Ivr *)(it->second);
    }

    return p_Ivr;
}

bool Switch::ValidCircuit( const int ichannel )
{
	try 
    {        		
		if( !DbChannels[ ichannel ].Enabled )
		{
			return false;
		}
	
		return true;
	}
	catch(...)
    {
		m_pLog4cpp->debug("EXCEPTION: ValidCircuit()");
		return false;
	}
}

boost::shared_ptr<IConnection> Switch::GetConnection()
{
    return m_ConnectionPtr;
}


std::string Switch::GetAppName()
{
    std::string sAppName;

    sAppName.assign(m_StRegister.AppName);    
      
    return sAppName;
}

Class_TS_Manager * Switch::GetTS_Manager()
{
    return &TS_Manager;
}

ConferenceManagerClass * Switch::GetConferenceManager()
{
    return m_pConferenceManager;
}

std::string Switch::GetRegistry(const std::string & sSubKey,LPCTSTR  pszValueName)
{    
    HKEY hKey = NULL;    
    std::wstring stemp = std::wstring(sSubKey.begin(), sSubKey.end());
    LPCTSTR pszSubkey = stemp.c_str();
    if ( RegOpenKey(HKEY_LOCAL_MACHINE, pszSubkey, &hKey) != ERROR_SUCCESS )
    {
        // Error:
        // throw an exception or something...
        //
        // (In production code a custom C++ exception 
        // derived from std::runtime_error could be used)
        //AtlThrowLastWin32();
        return " ";
    }

    // Buffer to store string read from registry
    TCHAR szValue[1024];
    DWORD cbValueLength = sizeof(szValue);

    // Query string value
    if ( RegQueryValueEx(
            hKey,
            pszValueName, 
            NULL, 
            NULL, 
            reinterpret_cast<LPBYTE>(&szValue), 
            &cbValueLength) 
         != ERROR_SUCCESS )
    {
        // Error
        // throw an exception or something...
        //AtlThrowLastWin32();
        return " ";
    }

    // Create a CString from the value buffer
    std::wstring string_string(szValue);
    std::stringstream ss;
    ss << string_string.c_str();
    return ss.str();
}

std::string Switch::GetVersion()
{
    std::string sInfo;
    std::stringstream ss;

    ss << " \r\n\r\n";
    ss << "                                                                            \r\n";
    ss << "    __     ______           ____  _   _  ___  _____ _   _ _____  __         \r\n";
    ss << "    \\ \\   / / ___|         |  _ \\| | | |/ _ \\| ____| \\ | |_ _\\ \\/ /  \r\n";
    ss << "     \\ \\ / / |  _   _____  | |_) | |_| | | | |  _| |  \\| || | \\  /      \r\n";
    ss << "      \\ V /| |_| | |_____| |  __/|  _  | |_| | |___| |\\  || | /  \\       \r\n";
    ss << "       \\_/  \\____|         |_|   |_| |_|\\___/|_____|_| \\_|___/_/\\_\\   \r\n";
    ss << "            ____  _____ ____   ___  ____  _   _                             \r\n";
    ss << "           |  _ \\| ____| __ ) / _ \\|  _ \\| \\ | |                        \r\n";
    ss << "           | |_) |  _| |  _ \\| | | | |_) |  \\| |                          \r\n";
    ss << "           |  _ <| |___| |_) | |_| |  _ <| |\\  |                           \r\n";
    ss << "           |_| \\_\\_____|____/ \\___/|_| \\_\\_| \\_|                      \r\n";
    ss << "                                                                            \r\n";
    ss << "                                                                            \r\n";
    ss << boost::format("                      VERSION : %d.%d.%d ") % VERSION_MAJOR_NUMBER % VERSION_MINOR_NUMBER % VERSION_BUILD_NUMBER;
    ss << "                                                                            \r\n";
    ss << "                                                                            \r\n";
        
    return ss.str();
}
#pragma once

#include "Tapi.h"
#include "DialogicDefines.h"
#include "SwitchDefines.h"

// dialogic
#include <srllib.h>
#include <dxxxlib.h>
#include <faxlib.h>
#include <dtilib.h>
#include <msilib.h>
#include <dcblib.h> 
#include <eclib.h>
#include <ctime>
#include <time.h>

// global call
#include <gclib.h>
#include <gcerr.h>
#include <libgcs7.h>

#include <gcip.h>
#include <gcip_defs.h>
#include <gccfgparm.h>
#include <gcipmlib.h>
#include <ipmlib.h>

// isdn
#include <cclib.h>
#include <isdncmd.h>

#include <sctools.h>

using namespace std;

// desvio de frequencia pra tons R2 em hertz
#define	R2_DEVFREQ										30

// Amplitude do sinal R2 em dB		
#define	R2_AMPLITUDE									-10

// Tempo de duracao de um tom R2 em segundos
#define	R2_TIME_TONE									20

// Tempo de espera entre o envio do fim de selecao e a atendimento para nao confundir a CENTRAL em (ms)
#define R2_TIME_TO_SEND_AOFF					300

// Tempo maximo para esperar pelo proximo numero de B antes de continurar a sinalizacao R2
#define R2_TIME_OUT_NO_MORE_DNIS			5

// Tamanho defaulr de DNIS
#define DNIS_SIZE_DEFAULT							4

// Numero base de inicio dos tons R2
#define	SIG_BASE											100

// Mascara que habilita todos os digitos
#define DM_ALL												(DM_0|DM_1|DM_2|DM_3|DM_4|DM_5|DM_6|DM_7|DM_8|DM_9|DM_P|DM_S|DM_A|DM_B|DM_C|DM_D)

// Mascara que habilita eventos de interface analogica
#define	LSI_MASK											(DM_RINGS)
//#define	LSI_MASK											(DM_RINGS|DM_LCOFF)

// Bits a serem monitorados para protocolo R2 digital
#define R2_BITMASK										(DTMM_AON|DTMM_AOFF|DTMM_BON|DTMM_BOFF)

// Numero maximo de recursos de conferencia por DSP na placa MSI
#define MAX_MSI_DSP										16

// Numero maximo de recursos Dialogic alocaveis
#define MAX_OBJ												512

// Valores Default para ToneDetection
#define 	DEFAULT_HANGUP1                     "1,425,50,0,0,25,5,25,5,2"
#define	    DEFAULT_HANGUP2                     "2,425,50,0,0,25,5,25,5,2"
#define	    DEFAULT_FAX1                        "3,2150,150,0,0,25,25,0,0,1"
#define	    DEFAULT_FAX2                        "4,1100,50,0,0,25,25,0,0,1"
#define	    DEFAULT_MBOX1                       "0,0,0"
#define	    DEFAULT_MBOX2                       "0,0,0,0,0"

// Valores Default para CallProgress
#define	    DEFAULT_LOCAL_DIAL_TONE             "TID_DIAL_LCL,400,125,400,125,0,0,0,0,0"
#define	    DEFAULT_INTER_DIAL_TONE				"TID_DIAL_INTL,402,125,402,125,0,0,0,0,0"
#define	    DEFAULT_EXTRA_DIAL_TONE				"TID_DIAL_XTRA,401,125,401,125,0,0,0,0,0"
#define	    DEFAULT_BUSY1_TONE					"TID_BUSY1,500,200,0,0,55,40,55,40,4"
#define    DEFAULT_BUSY2_TONE					"TID_BUSY2,500,200,500,200,55,40,55,40,4"
#define	    DEFAULT_RING_BACK1_TONE				"TID_RNGBK1,450,150,0,0,130,105,580,415,0"
#define 	DEFAULT_RING_BACK2_TONE				"TID_RNGBK2,450,150,450,150,130,105,580,415,0"
#define	    DEFAULT_FAX1_TONE					"TID_FAX1,2150,150,0,0,25,-25,0,0,0"
#define	    DEFAULT_FAX2_TONE					"TID_FAX2,1100,50,0,0,25,-25,0,0,0"

// Valores minimos e maximos para o volume da estacao da atendente
#define MINIMUM_STATION_LEVEL					0
#define MAXIMUM_STATION_LEVEL					12

// Tamanho maximo de um IE (information element)
#define MAX_INFO_ELEMENT_SIZE					512

//TTS
#define TTS_FILE_DESCRIPTOR						0x1234

enum R2BitsStates 
{
	AON,
	AOFF,
	BON,
	BOFF,
	CON,
	COFF,
	DON,
	DOFF
};

enum R2States 
{
	R2_IDLE,					// O canal esta livre
	R2_GET_DNIS,			// O canal esta pegando o numero de			B (ligacao de entrada)
	R2_GET_ANI,				// O canal esta pegando o numero de			A (ligacao de entrada)
	R2_GET_CAT,				// O canal esta pegando a categoria de	A (ligacao de entrada)
	R2_SENT_A3,				// O canal enviou o sinal A3 (prepara para mudar para o GRUPO B)
	R2_SEND_GROUP_B,	// O canal precisa envia o sinal do GRUPO B indicando se a ligacao sera cobrada ou nao
	R2_SEND_AOFF,			// O canal enviou o sinal do grupo B e esta esperando a aplicacao setar o bit AOFF para atender
	R2_DISCONNECT,		// Usuario desconectou
	R2_BLOCKED,				// O canal esta bloqueado
};

enum R2Signals
{
	SIG_1 = SIG_BASE+1,
	SIG_2,
	SIG_3,
	SIG_4,
	SIG_5,
	SIG_6,
	SIG_7,
	SIG_8,
	SIG_9,
	SIG_10,
	SIG_11,
	SIG_12,
	SIG_13,
	SIG_14,
	SIG_15
};

enum ResourceType 
{
	VOX_RESOURCE,				// Recurso de voz para ser alocado com PopObj
	LINE_RESOURCE				// Recurso de linha para ser alocado com PopObj
};

// Estrutura para criacao dos tons para deteccao e CallProgress
struct TONE_DEF 
{
	int id_tone;
	int freq_1;
	int desv_1;
	int freq_2;
	int desv_2;
	int ontime;
	int ontdev;
	int offtime;
	int offtdev;
	int repet_cont;
};

// Estrutura para criacao dos tons para deteccao
struct SINGLE_TONE_DEF 
{
	int id_tone;
	int freq;
	int desv;
};

// Estrutura para criacao dos tons para deteccao
struct DOUBLE_TONE_DEF 
{
	int id_tone;
	int freq_1;
	int desv_1;
	int freq_2;
	int desv_2;
};


// Parametros analogicos por placa
struct DXBD 
{
	int flashtm;
	int pausetm;
	int digrate;
	int sch_tm;
	int p_bk;
	int p_mk;
	int p_idd;
	int t_idd;
	int oh_dly;
	int r_on;
	int r_off;
	int r_ird;
	int s_bnc;
	int	ttdata;
	int minpdon;
	int minpdoff;
	int minipd;
	int minlcoff;
	int redge;
	int maxpdoff;
};

typedef union
{
	unsigned short value;
	struct
	{
		unsigned int bitA : 1;	// Indicador de chamada 0 = Nacional / 1 = Internacional
		unsigned int bitB : 1;	// Nao utilizado
		unsigned int bitC : 1;	// Nao utilizado
		unsigned int bitD : 1;	// Indicador de Interfuncionamento 0 = nao encontrado, 1 = encontrado
		unsigned int bitE : 1;	// Nao utilizado
		unsigned int bitF : 1;	// Indicador de uso da ISUP 0 = nao usado, 1 = usado
		unsigned int bitG : 1;	// Par HG - Indicador de preferencia de ISUP
		unsigned int bitH : 1;	// 00 preferida, 01 nao requerida, 10 requerida, 11 reservado
		unsigned int bitI : 1;	// Indicador de acesso ISDN 0 = nao, 1 = sim
		unsigned int bitJ : 1;	// Nao utilizado
		unsigned int bitK : 1;	// Nao utilizado
		unsigned int bitL : 1;	// Nao utilizado
		unsigned int bitM : 1;	// Indicador de chamada DIC/DLC 0 = normal, 1 = a cobrar
	} fci_bits;
} FCI_FIELD; // Forward call indicators (GlobalCall SS7)

typedef union
{
	unsigned short value;
	struct
	{
		unsigned int bitA : 1;	// Par BA - Indicador de tarifa
		unsigned int bitB : 1;	// 00 s/ indicacao, 01 s/ tarifacao, 10 c/ tarifacao, 11 reserva
		unsigned int bitC : 1;	// Par DC - Indicador de estado do chamador
		unsigned int bitD : 1;	// 00 s/ indicacao, 01 assinante livre, 10 nao usado, 11 reserva
		unsigned int bitE : 1;	// Par FE - Indicador de categoria do chamador
		unsigned int bitF : 1;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
		unsigned int bitG : 1;	// Nao utilizado
		unsigned int bitH : 1;	// Nao utilizado
		unsigned int bitI : 1;	// Indicador de Interfuncionamento 0 = nao encontrado, 1 = encontrado
		unsigned int bitJ : 1;	// Nao utilizado
		unsigned int bitK : 1;	// Indicador de uso da ISUP 0 = nao usado, 1 = usado
		unsigned int bitL : 1;	// Indicador de retencao 0 = nao solicitada, 1 = solicitada
		unsigned int bitM : 1;	// Indicador de acesso ISDN 0 = nao, 1 = sim
		unsigned int bitN : 1;	// Indicador de disp. supressor de eco 0 = nao incluido, 1 = incluido
		unsigned int bitO : 1;	// Nao utilizado
		unsigned int bitP : 1;	// Nao utilizado
	} bci_bits;
} BCI_FIELD; // Backward call indicators (GlobalCall SS7)

// Calling party's category
enum SS7_CPC
{
    SS7_CPC_TELEFONISTA					= 0x09,
    SS7_CPC_ASSINANTE_COMUM			= 0x0A,
    SS7_CPC_CHAMADA_DADOS				= 0x0C,
    SS7_CPC_CHAMADA_TESTES			= 0x0D,
    SS7_CPC_TELEFONE_PUBLICO		=	0xFF,			 
    SS7_CPC_ASSINANTE_ESPECIAL	=	0xE0,
    SS7_CPC_TP_INTERURBANO			=	0xE2
};

class Dialogic : public CTIBase
{
private:
    // LOG4CPP
	//log4cpp::Category * 					m_pLog4cpp;	
    //log4cpp::Layout *                       m_layout;
    //log4cpp::Appender *                     m_rfileAppender;
    //log4cpp::Category *                     m_category;

    boost::shared_ptr<Logger>  m_pLog4cpp;
        
public:

    char * pLogBuffer;
    char sPar[1024];
    char NomeDir[_MAX_PATH+1];
    char NomeArq[_MAX_PATH+1];
    char Buffer[1024];
    char path_dir[MAX_PATH+1];

    //struct _timeb TimeBuffer;
    time_t TimeBuffer;
    struct tm *PTimeStruct;
    
    //LogInterface *pLogInterface;	// Ponteiro para o objeto de log
    FILE *Pfile;
    va_list ArgPtrLog;

    SC_TSINFO sc_tsinfo_rec;			// Estrutura para informacao de TimeSlot para gravacao simultanea de dois timeslots
    SC_TSINFO sc_tsinfo;					// Estrutura para informacao de TimeSlot
    MS_CDT		ms_cdt[5];					// Estrutura para informacao de conferencia

    DXBD dxbd;										// Parametros analogicos por placa

    TONE_DEF DefHangup1;					// Definicao dos tons para Hangup
    TONE_DEF DefHangup2;					// Definicao dos tons para Hangup
    TONE_DEF DefFax1;							// Definicao dos tons para Fax
    TONE_DEF DefFax2;							// Definicao dos tons para Fax
    SINGLE_TONE_DEF DefC5;				// Definicao do tom C5 (do)
    SINGLE_TONE_DEF DefD5;				// Definicao do tom D5 (re)
    SINGLE_TONE_DEF DefE5;				// Definicao do tom E5 (mi)
    SINGLE_TONE_DEF DefF5;				// Definicao do tom F5 (fa)
    SINGLE_TONE_DEF DefG5;				// Definicao do tom G5 (sol)
    SINGLE_TONE_DEF DefFS5;				// Definicao do tom F5 sustenido (Beep da BCP e TIM)
    DOUBLE_TONE_DEF DefC6;				// Definicao do tom C5 e C6 (do harmonicos)
    DOUBLE_TONE_DEF DefD6;				// Definicao do tom D5 e D6 (re harmonicos)
    DOUBLE_TONE_DEF DefE6;				// Definicao do tom E5 e E6 (mi harmonicos)
    DOUBLE_TONE_DEF DefF6;				// Definicao do tom F5 e F6 (fa harmonicos)
    DOUBLE_TONE_DEF DefG6;				// Definicao do tom G5 e G6 (sol harmonicos)

    TONE_DEF DefCpBusyTone1;			// Primeiro tom de ocupado para Call Progress
    TONE_DEF DefCpBusyTone2;			// Segundo tom de ocupado para Call Progress
    TONE_DEF DefCpFaxTone1;				// Primeiro tom de FAX para Call Progress
    TONE_DEF DefCpFaxTone2;				// Segundo tom de FAX para Call Progress
    TONE_DEF DefCpLocalDialTone;	// Tom de linha local para Call Progress
    TONE_DEF DefCpInterDialTone;	// Tom de linha internacional para Call Progress
    TONE_DEF DefCpExtraDialTone;	// Tom de linha extra para Call Progress
    TONE_DEF DefCpRingBackTone1;	// Primeiro tom de ringback para Call Progress
    TONE_DEF DefCpRingBackTone2;	// Segundo tom de ringback para Call Progress

    bool FlagIPChannelInit; // Indica se o canal ja foi inicializado pronto para atender ligacoes
    bool FlagChannelInit; // Indica se o canal ja foi inicializado pronto para atender ligacoes
    bool FlagDisconnected;// Flag indicando que ocorreu uma disconnexao do usuario
    bool FlagUserBlock;		// Flag indicando que ocorreu uma requisicao de bloqueio do canal
    bool FlagCallIP;			// Flag indicando se a chamada � IP.
		
    bool R2_AckCall;			// Flag indicando se sera necessario pegar mais numero de B
    bool R2_Accept;				// Flag indicando se a ligacao ja foi aceita em R2 digital
		
		
		
    bool R2ForwardTones;	// Flag indicando que os tons para frente ja foram criados
    bool R2BackwardTones;	// Flag indicando que os tons para tras estao deletados
    bool FlagMsiRing;			// Indica se a placa MSI na qual a estacao (Canal) esta conectada possui ring
											    // esse flag e resetado quando a ligacao e atendida	

    int iCtiLogLevel;			// Valor do nivel de log para CTI
    int DisconnectReasonCode; // Codigo da razao da desconexao
    int ChannelStatus;		// Status do canal
		
    int iListIndex;				// Indice para a lista da tela para os bits CAS
    int iBoardTrunk;		  // Item do indice para a lista da tela para os bits CAS
    int iBoardPort;					// Item da tela  para os bits CAS
    int R2DnisMinimumSize;// Tamanho minimo de dnis para pegar em protocolo R2 Digital
		

    int R2_State;					// Estado do canal em protocolo
    int R2TimeOutNoMoreDnis; // Variavel de controle usada para interpretacao de DNIS variavel
    int DnisMinimumSize;	// Numero minimo de DNIS a ser recebido em R2 antes de voltar T_CALL
    int NumMoreDnis;			// Numero de algarismos do DNIS para pegar depois da chamada a funcao CallAck()
    bool DnisVariavel;		// Se true o DNIS sera variavel no protocolo R2

		

    int MessageBoxMelody;	// Verificador das notas da melodia da caixa de msgs.
    int iMelody;					// Tipo da melodia: 1 para VIVO/OI e 2 para BCP/TIM 
    int CountAni;					// Contador do numero de algarismos de ANI recebidos
    int CountDnis;				// Contador do numero de algarismos de DNIS recebidos
    int CauseDropCall;		// Causa da disconexao quando o sistema desliga a ligacao
    int PcmEnconding;			// Tipo de PCMEncondig para arquivo PCM, ULAW ou ALAW
    int Canal;						// Canal que esta sendo manipulado
    int Site;							// Canal que esta sendo manipulado
    int Machine;					// Canal que esta sendo manipulado
    int TipoInterface;		// Tipo de interface LSI, E1, T1, MSI, VOX ou FAX
    int TipoSinalizacao;	// Tipo de sinalizacao. R2, ISDN, GlobalCall, etc...
    string TipoProtocolo;	// Tipo de protocolo de Sinalizacao GlobalCall. "br_r2_io", "isdn", "SS7", etc...
    string ProtocoloIP;		// Protocolo IP :"H323", "SIP", etc...
    int Dev;							// Variavel de uso geral para manipulacao de devices
    int Vox;							// Device de voz
    int Dti;							// Device de interface de linha digital
    int Msi;							// Device de interface de linha analogica
    int MsiBoardDev;			// Device da placa MSI na qual a estacao (Canal) esta conectada
    int MsiBoardNumber;		// Numero da placa MSI
    int MsiRealStation;		// Numero real da estacao na placa MSI
    int MsiIdExtConnection;// Identificador de conexoes extendidas
    int Fax;							// Device de fax
    int TimeslotLine;			// Timeslot local para a interface de linha
    int TimeslotLineIP;		// Timeslot local para o device de voz ip
    int TimeslotVox;			// Timeslot local para o recurso de vox
    int Event;						// Evento propriamemte dito
    int EventType;				// Tipo de evento
    int Data;							// Dado do evento
    int IDTone;						// Identficador unico de cada tom que sera adicionado
    int NumHangupTones;		// Numero de tons de hangup configurados
    int CpTerm;						// Causa do evento TDX_CALLP para Call Progress
    int TimeToWaitEvent;	// Tempo para esperar eventos de telefonia
    int TypePlayFormat;		// Formato de play setado
    int TypeRecordFormat; // Formato de record setado
    int ContDisconnected;	// Contador para numero de desconexoes;
    int LocalParam;				// Variavel para armazenar o parametro enviado por SendEvent
											    // para um outro canal e para receber esse mesmo parametro
											    // e recebido o evento por outro canal

    long TimeSlotArrayRec[5];	// Array de timeslots usado para gravacao de dois timeslots ao mesmo tempo
    long Devs[64];				// Guarda os devices que nos quais seram esperados eventos
    long EventHandle;			// Handle para eventos
    long TimeRecord;			// Guarda o tempo de gravacao do ultimo arquivo gravado
    long UserTimerSet;		// Guarda o tempo limite que o usuario programou para o timer
    long UserTimerInit;		// Guarda o tempo inicial do timer do usuario

    unsigned short DigMask;		// Mascara de digitos para interromper Play e Record
		
    unsigned short Bit;				// Ponteiro para bits de eventos
		
    bool FlagGlobalCallAlerting;	// Flag indicando que o evento ALERTING ja veio
    bool DialOut;									// = true		ligacao de saida
    int iDirection;								// Indica a direcao da chamada sainte/entrante.

    unsigned short PlayFileFormat;			// Formato do arquivo para Play
    unsigned short PlayDataFormat;			// Formato de dados para Play
    unsigned long  PlaySamples;					// Numero de amostras para Play	
    unsigned short PlayBitsPerSample;		// Bits por amostra para Play

    unsigned short RecordFileFormat;		// Formato do arquivo para Record
    unsigned short RecordDataFormat;		// Formato de dados para Record
    unsigned long  RecordSamples;				// Numero de amostras para Record
    unsigned short RecordBitsPerSample;	// Bits por amostra para Record

    unsigned long PlayBufferSize;				// Tamanho do buffer de reproducao p/ a Dialogic
    unsigned long PlayTTSPosition;			// Posicao da reproducao do TTS.

    METAEVENT	metaevent;				// Variavel para coleta de eventos em GLOBAL CALL
    LINEDEV		ldev;							// Device do canal para a API GLOBAL CALL
    LINEDEV		dtidev;						// Device do canal E1 para a API GLOBAL CALL
    LINEDEV		ipdev;						// Device do canal ip para a API GLOBAL CALL
    CRN				crn;							// Handle da chamada corrente
    //CRN				crn1;							// Handle da primeira chamada (inbound/outbound)
    //CRN				crn2;							// Handle da segunda chamada (outbound no mesmo canal)
    //CRN				release_crn;			// Handle da chamada que est� sendo desligada


    map<CRN, long> mapCrnToStatus;

    DV_DIGIT	dv_digit;					// Variavel para coleta de digitos no evento TDX_GETDIGIT
    DV_TPT		dv_tpt[5];				// Define as condicoes de termino para funcoes de voz
    DX_IOTT		dx_iott;					// Variavel utilizada pelas funcoes PlayFile e RecordFile
    DX_UIO		uioblk;						// Variavel de registro de funcoes de callback p/ play/record.
    DX_XPB		dx_xpb;						// Define os tipos de arquivos de voz (PCM, WAV, etc...)
    TN_GEN		tngen;						// Define as caracteristicas dos tons
    TN_GENCAD	tngencad;					// Define as caracteristicas dos tons com cadencia
    DX_CAP		dx_cap;						// Guarda os parametros analogicos para cada canal
    DX_CST		*cstp;						// Ponteiro para estrutura que guarda o resultado dos eventos
    MS_NCB		ms_zip_tone;			// Estrutura de caracteristicas do ziptone da placa MSI

    MAKECALL_BLK makecall_blk;					// Estrutura para parametros de discagem em ISDN
    GC_MAKECALL_BLK gc_makecall_blk;		// Estrutura para parametros de discagem em GlobalCall
    S7_MAKECALL_BLK gc_s7_makecall_blk;	// Estrutura para parametros de discagem em GlobalCall SS7
    S7_IE_BLK	ie_blk;						// Estrutura p/ troca de msgs. c/ GlobalCall SS7
    GC_IE_BLK	gc_ie_blk;				// Estrutura p/ troca de msgs. c/ GlobalCall
    FCI_FIELD	FCI;							// Estrutura p/ manipular o campo FCI da msg. IAM do SS7
    BCI_FIELD	BCI;							// Estrutura p/ manipular o campo BCI da msg. ACM do SS7

    void	*datap;							// Ponteiro para estrutura que guarda o resultado dos eventos

    char sEventString[256];		// Armazena a mensagem enviada pela funcao SendEvent()
    char sReasonDisconnect[256];	// Razao da desconexao
    char NomeArquivoPar[_MAX_PATH+1]; // caminho para o arquivo PAR.INI
    char Dnis[128];						// Numero chamado
    char Ani[128];							// Numero do chamador
    char CallInfo[256];				// Dados da chamada: categoria do chamador, ANI, DNIS, etc
    string CallId;						// ID da chamada
    char DigType;							// Tipo do de digito PULSO ou TOM
    char BufLastError[1024];	// Guarda a string que descreve o ultimo erro ocorrido
    char gc_last_error[1024];	// Guarda a string que descreve o ultimo erro ocorrido em GlobalCall
    char sCasBits[32];				// guarda os estados dos bits CAS para colocar na tela

    char LastDigit;						// Utimo digito que veio atraves da fila

    jmp_buf *JumpBuffer;			// Ponteiro para o ponto de desconexao
    va_list ArgPtr;						// Variavel do tipo de argumento similar a printf

    GC_CALLACK_BLK gc_callack;										// Estrutura para pegar DNIS variavel

    static GC_START_STRUCT	startp;								// Estrutura de inicio da biblioteca GloballCall
    static GC_CCLIB_STATUS	cclib_status;					// Estrutura de status da biblioteca GloballCall
		
    
    static bool FlagInitDialogic;									// Indica se a Dialogic foi inicializada ou nao
    static bool FlagEndObject;										// Indica se os objetos devem ser terminados
    static bool FlagLastOpenChannel_E1;						// Flag indicando se o ultimo canal aberto era de interface E1
    static bool FlagFirstAnalogPort;							// Flag de controle para a primeira porta analogica do sistema
    static bool FlagIpTelephonyInitiate;					// Flag indicando que o sistema de IP Telephony foi iniciado
    static DWORD dwThreadIdGlobalCallStart;				// ThreadID indicando qual a thread que inicializou o Globalcall
    

    	
    static CRITICAL_SECTION CritSecR2Tones;				// Garante que os tons R2 sejam criados
    static CRITICAL_SECTION CritMsiZipTone;				// Garante o ziptone das estacoes serializado

    
    static int OFFSET_VOX;												// OFFSET para pular os devices de voz nao usados em ambientes digitais
    static int OFFSET_DTI;												// OFFSET para pular os devices de rede usados em ambientes digitais
    static int OFFSET_IPT;												// OFFSET para contar o numero de placas VOIP ja abertas
    

    

#ifdef _ASR

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ASR - Automatic Speech Recognition
    // ==================================
    // Funcoes exclusivas de reconhecimento de voz.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Instancia da interface de reconhecimento de voz ASR.
    ClassASR* ASR;
    // Flag indicando que o reconhecimento de voz esta habilitado.
    bool bAsrEnabled;
    // String indicando o pacote de gramatica de reconhecimento de voz que sera usado.
    string ASR_GrammarPackage;
    // String indicando o tipo do servidor c/ a base de dados para uso de gramatica dinamica c/ reconhecimento de voz.
    string ASR_DatabaseType;
    // String indicando o servidor c/ a base de dados para uso de gramatica dinamica c/ reconhecimento de voz.
    string ASR_DatabaseServer;
    // String indicando o nome da base de dados para uso de gramatica dinamica c/ reconhecimento de voz.
    string ASR_DatabaseName;
    // String indicando o usuario da base de dados para uso de gramatica dinamica c/ reconhecimento de voz.
    string ASR_DatabaseUser;
    // String indicando a senha do usuario da base de dados para uso de gramatica dinamica c/ reconhecimento de voz.
    string ASR_DatabasePassword;
    // Flag indicando que as utterances devem ser salvas.
    bool ASR_Client_WriteWaveforms;
    // Flag indicando que o calllog esta ativo.
    bool ASR_Client_Behaviors_Calllog;
    // Tempo maximo no polling de eventos da dialogic na implementacao de ASR.
    int ASR_PollingTime;
    // Reconhece o que o usuario falou pela gramatica especificada e considerando o limite de silencio se especificado.
    int ASRRecognize( const char* lpcszGrammar = NULL, const double& dNoSpeechTimeout = 2.0, const char* lpcszFilename = NULL );
    // Obtem o numero de respostas no reconhecimento.
    int ASRGetNumberOfAnswers();
    // Obtem o numero de interpretacoes p/ cada resposta no reconhecimento.
    int ASRGetNumberOfInterpretations( const int& iAnswerIndex = 0 );
    // Obtem a interpretacao do que o usuario falou (resposta) no slot especificado e conforme o indice da resposta se especificado.
    const char* ASRGetAnswer( const char* lpcszSlotName, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 );
    // Obtem a confiabilidade do reconhecimento de 0 a 100.
    int ASRGetScore( const char* lpcszSlotName = NULL, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 );
    // Obtem o numero de slots preenchidos p/ cada resposta no reconhecimento.
    int ASRGetNumberOfFilledSlots( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 );
    // Obtem o nome do slot preenchido na interpretacao do que o usuario falou (resposta) nos indices especificados.
    const char* ASRGetSlotName( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0, const int& iSlotIndex = 0 );
    // Inicializa o sistema de reconhecimento de voz.
    int ASRInitialize();
    // Encerra o sistema de reconhecimento de voz.
    int ASRTerminate();
    // Reinicializa o sistema de reconhecimento de voz.
    int ASRRestart();
    // Seta o inicio/termino de uma chamada p/ o engine de reconhecimento.
    int ASRSetCallConnected( const bool& bCallConnected );

    // Prepares a dynamic grammar to be used.
    int ASRAddVocabulary( const char* lpcszGrammarId, const char* lpcszGrammarLabel );
    // Adds a phrase to a dynamic grammar.
    int ASRAddPhrase( const char* lpcszGrammarId, const char* lpcszPhraseId, const char* lpcszPhraseText, const char* lpcszPhraseNL );
    // Removes a phrase from a dynamic grammar.
    int ASRRemovePhrase( const char* lpcszGrammarId, const char* lpcszPhraseId );
    // Starts an enrollment session.
    int ASRStartEnrollmentSession( const char* lpcszGrammarId, const char* lpcszPhraseId, const char* lpcszPhraseNL, const char* lpcszGrammar );
    // Stops an enrollment session and saves enrolled phrase when required.
    int ASRStopEnrollmentSession( const bool& bAbort = false, const char * lpcszFileName = NULL, string& bsPhraseIdInClash = string("") );
    // Returns the number of repetitions still needed to conclude an enrollment session.
    int ASRGetEnrollmentStatus();

    // Gets an ASR configuration parameter.
    const char *ASRGetParameter( const char* lpcszKey );
    // Sets an ASR configuration parameter.
    int ASRSetParameter( const char* lpcszKey, const char* lpcszValue, const int& iValueType = pvString );
    // Gets the safe index of the start of speech.
    int ASRGetStartSafeIndex();
    // Gets the actual index of the start of speech.
    int ASRGetStartActualIndex();
    // Gets the safe index of the end of speech.
    int ASRGetEndSafeIndex();
    // Gets the actual index of the end of speech.
    int ASRGetEndActualIndex();
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // _ASR

public:
    
    Dialogic(boost::shared_ptr<Logger>  pLog4cpp , TAPI_PARAM *pTapiParam);// construtor de inicializacao
    Dialogic(boost::shared_ptr<Logger>  pLog4cpp);	// construtor sem inicializacao
    
    ~Dialogic();	//destrutor
    void EndObj(void);

    const char *GetLastError(void);
    const char *GetEventString(void);

    void JumpExit(void);
    void FakeOpenChannel(int Channel, int InterfaceType, int Signaling = GLOBAL_CALL);
    void OffSetVoxIncrement(int InterfaceType, int Signaling );
    void ResetUserTimer(void);
    void SetUserTimer(long TimeLimit);
    void SetJump(jmp_buf *pJmp);
    void SetPCM(int TypePcm);
    void SetPlayFormat(int Format);
    void SetRecordFormat(int Format);
    void SetDigMask(const char *Mask);
    void SetDevs(int *iArrayDevs, int iNumDevs);
    const char *GetAni();
    const char *GetDnis();
    const char *GetCallInfo();
    int SetCallId( const char* lpcszCallId );
    const char* GetCallId();
    int IPSetCapabilities(void);
    void PutError(char *fmt, ...);
    void GetLastError(char *Buffer, int Size);
    void GetMsiInfo(int BoadNumber, int &MsiBoardDev);
    void InitSystem(void);
    void EndSystem(void);
    void SetTimeToWaitEvent(int Time);
    void SetPauseTime(int iPauseTime);
    void SetFlashTime(int iFlashTime);
    void CloseFile(void);
    void RegObjetoTomOcupado(void);
    void RegObjetoMusicaEspera(void);
    void CloseChannel(void);
    void GetDigMask(char *Destino);
    void BlockObj(void);
    void ISDN_BuildMakecallBlk(char *OriginationPhoneNumber);
    void ISDN_ReasonDisconnect(void);
    void GC_ReasonDisconnect(void);
    void CasSetBit(int bit);
    void R2_CreateBackwardTones(void);
    void R2_CreateForwardTones(void);

    void ClearEventQueue(void);
    void CtiLog(const char *sformat, ...);
    void CtiLog_Original(const char *sformat, ...);
    void LogCasBits(void);
    void PrintCasBits(void);
    void R2Answer(bool bDoubleAnswer, bool bBilling);
    void R2Accept(bool bDoubleAnswer, bool bBilling);

    bool AlreadyDisconnected(void);

    int	AT(void);
    int	AR(void);
    int	BT(void);
    int	BR(void);
    int	CT(void);
    int	CR(void);
    int	DT(void);
    int	DR(void);
    int R2ChangeGroupII(void);
    int R2SendAni(char &Sig);
    int R2Inbound(void);
    int R2Outbound(void);
    int R2GetMinimumDnis(void);
    int R2GetCompleteDnis(int iNumberDnis);
    int R2GetAni(void);
    int R2WaitEvent( int Time = -1 );
    int R2RecvToneON(char &Tone);
    int R2RecvToneOFF(void);
    int R2_PlayForward(int Tone);
    int R2_PlayBackward(int Tone);
    int TryBlockObj(void);
    int RetDigMaskTerm(const char *DigitsTerm);
    int SendEvent(int Channel, int Event, int Param);
    int SendEvent(int Channel, int Event, char *sMsg);
    int SendEvent(int Channel, int Event);
    int SetCallProgress(void);
    int	SetDXBD(void);
    int	SetDXCAP(void);
    int SetHANGUP(void);
    int SetFAX(void);
    int SetMESSAGEBOX(void);
    int SetPlayBufferSize( const enum BulkQueueBufferSizeType& BulkQueueBufferSize );
    int GetVoxTermCause(void);
    int GetRecordFormat(void);
    int GetPlayFormat(void);
    int VoxState(void);
    int GetPCM(void);
    int GetPCM(int TypePcm);
    int GetPlayFormat(int Format);
    int GetRecordFormat(int Format);
    int GetChannelStatus( int Channel = -1 );    
    int CasBitState(int Bit);
    int OuveTomOcupado(void);
    int OuveMusicaEspera(void);
    int StopCh(int iTimeOut = 1000);
    int StopChAsync(void);
    int GetChannel(void);
    int GetSite(void);
    int GetMachine(void);
    int GetDirection(void);
    int GetTimeToWaitEvent(void);
    int DigitQueue(bool bFlag);
    int Disconnect(int CauseDropCall = 0 /* (DROP_NORMAL) */ );
    int Disconnect2(void);
    int	SetChannelState(int State);
    int Answer(bool bDoubleAnswer = false, bool bBilling = true);
    int Accept(bool bDoubleAnswer = false, bool bBilling = true);
    int CallAck(int NumOfDnis);
    int PlayToneSync(int Time, int Freq1, int Freq2);
    int Volume(int Level);
    int Speed(int Level);

    int AddTTS( const char *lpcszText );
    int AddFile( const char *lpcszFilename );
    int AddBuffer( char *buffer, const unsigned long buffersize );
    int ClearPrompts();
    int PlayAsync(unsigned short Terminator, const long lOffset = 0);
    int Play(const long lOffset = 0);
    int PlayTTS(const char *lpcszText);
    int PlayTTSAsync(const char *lpcszText);
		
    int PlayFileEvent(const char *Arquivo);
    int PlayFile(const char *lpcszFilename, const char *lpcszTermDigit = NULL, const long lOffset = 0);

    int PlayFileSync(const char *Arquivo);

    int PlayFileAsync(const char *lpcszFilename, const char *lpcszTermDigit = NULL, const long lOffset = 0);

    int RecordFile(const char *Arquivo, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar);
    int RecordFileFrom2Src(const char *Arquivo, const long& SecondarySrc, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar);
    int RecordBuffer(std::string** buffer, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar);

    int GetDigits(int NumDigitos, int TempoInterdigitos, char *BufferDigitos, const char *DigitosTerminacao);
		
    int ClearDigits(void);
    int LineTone(int Time);
    int RingTone(int Time);
    int BusyTone(int Time);
    int CheckMESSAGEBOXTone(const unsigned int ToneId);
    int DetectTones( const int type );
    int PulseDetection(bool flag);
    int HangupToneDetetion(bool bFlag);
    int MsgBoxBeepDetection(bool bFlag);
    int ScUnlisten(void);
    int ScListen(long Timeslot);
    int ScConnect(int mode);
    int InitChannel(void);
    int OpenChannelIP(bool VoiceResource, const char *Protocolo = "H323");
    int OpenChannel(int Interface, int Sinalizacao = 0, const char *Protocolo = "br_r2_io");
    int OpenChannelE1(int Sinalizacao, const char *Protocolo);
    int OpenChannelT1(int Sinalizacao, const char *Protocolo);
    int OpenChannelLSI(int Sinalizacao);
    int OpenStation(int IdStation, int BoardNumber);
    int OpenChannelVOX(void);
    int OpenChannelFAX(void);
    int OpenChannelFAX(int &iFaxChannel, long &lTxTimeSlot, int &iDevice);
    int GetTimeSlot(void);
    int GetTimeSlotVox(void);    
    int GetEvent( int Time = -1 );
    int GetInterface(void);
    int GetDevice(int Interface);
    int GetLocalParam(void);
    int	GetEventDev(void);
    int	GetEventHandle(void);
    int GetLineTxTimeSlot(int DeviceType);
		
    int ProcLineSide( int Time = -1 );
    int ProcR2( int Time = -1 );
    int ProcR2digital( int Time = -1 );
    int ProcISDN( int Time = -1 );
    int ProcGC( int Time = -1 );
    int ProcGCSS7( int Time = -1 );
    int ProcGCGeneric( int Time = -1 );
    int ProcLSI( int Time = -1 );
    int ProcLSITrap( int Time = -1 );
    int ProcVOX( int Time = -1 );
    int ProcFAX( int Time = -1 );
    int ProcMSI( int Time = -1 );
    int ProcMSI( long EventHandle );
		
    int DialR2Async(void);
    int DialVox(char *Number, bool CallProgress);
    int DialLSI(char *Number, bool CallProgress);
    int DialLSIAsync(char *Number, bool CallProcess);
    int Dial(char *Dnis, char *Ani, int TimeConnect = 60, bool CallProgress = false);
    int DialAsync(char *Dnis, char *Ani, bool CallProgress = false);
    int DialISDNAsync(void);
    int DialGCAsync(void);
    int DialGCSS7Async(void);
    int IPDialAsync(char *Dnis, char *Ani);
    int SetDialOutGCSS7(bool bDialOut);
    int BuildMakeCallBlkGCSS7();
    int GetCallInfoGCSS7();
    int MakeDoubleAnswerGCSS7();
    int DialLineSide(char *sNumber);
    int LineSideTransfer(char *sNumber);
    int BlindTransfer( const char *lpcszNumber, int iTime = 10 );
    int BlindTransferGC( char *lpcszNumber, int iTime = 10 );
    int BlindTransferIP( char *lpcszNumber );
    int HoldCall(void);
    int HoldCallAck(void);
    int RetrieveCall(void);
    int RetrieveCallAck(void);
    int OnHook(void);
    int OffHook(void);
		
    bool MsiHasRing(void);

    int MsiGenRing(int NumRings);
    int MsiStopRing(void);
    int MsiGenZipTone(void);
    int MsiHookStatus(void);
    int MsiInitBoard(void);
    int MsiDelExtConnection(void);
    int MsiExtConnectionTs(long TimeSlot, int Type, long &TimeSlotConf, int &IdConnection);
    int MsiExtConnectionSt(int Station, int Type, long &TimeSlotConf, int &IdConnection);
    int MsiChangeExtConnectionTypeTs(long Tslot, int IdConnection, int Type);
    int MsiChangeExtConnectionTypeSt(int Station, int IdConnection, int Type);
    int WaitEvent(void);
    int WaitEventEx(int iNumDevices, int iMilliseconds);
    int WaitForDisconnect(const int TimeToWait);
    int WaitForPlayStopped();
    int RegObj(void);
    int GetDisconnectReasonCode(void);
		
    char GetLastDigit(void);

    long GetTimeRecord(void);
    long GetBytesIO(void);

    unsigned short MsiSigEvt(unsigned short Bit);

    char *gc_GetStringError(void);
    char *GetReasonDisconnect(void);

    CTIBase *OBJ(int Canal);
    //Jairo
    //UDPStreamClass *pUDPStream;    


    //FUNCOES QUE SERAO EXCLUSIVAS DA CTI
    int InitEngine(void *) {return 0;};
    int SetParamEngine(void *) {return 0;};
    void SetCallerPort(long TxTimeSlot){};
    void SetPromptPort(long TxTimeSlot){};
    void ExternalCallConnected(int Flag){};
    void SetExternalVox(int Voxh){};
    void GetPromptPort(long & TxTimeSlot){}
    void AbortAll(void){};
    void WaitCall(void){};
    void ReleaseChannel(void){};
    int PlaceCall(char const * number){return 0;};
    int AddPrompt( string &prompt ){return 0;};
    int AddPrompt( const char *prompt ){return 0;};
    int AddErrorPrompt( const char *prompt ){return 0;};
    int AddErrorPrompt( string &prompt ){return 0;};
    int AddTTSPrompt( const char *prompt ){return 0;};
    int AddTTSPrompt( string &prompt ){return 0;};
    int RestorePromptList(){return 0;};
    int DropConference( void ){return 0;};
    int GetTransferStatus( void ){return 0;};
    int PlayDigits(char *pDigits, int tam) {return 0;};
    string sReasonTransfRet(void) {return NULL;};
				
    int GetEnableTelephony(void){return 0;};
    int GetEnablePlayBack(void){return 0;};
    void GetProtocol(string &sProtocol, int &iSinalizacao){};

    int StopRecordFile(void);

    int VoxScListen(long timeslot);
    int ScRouteCallIP();

    std::string GetExePath();
};

#define MAX_CHAR_DETAILS				15
#define MAX_CHAR_TYPE						3
#define MAX_CHAR_ORIGIN					3
#define MAX_CHAR_MESSAGE_TEXT		255

struct MSI_INFO {
	int NumStations;		// Numero de estacoes da placa MSI
	int MsiBoardDevice;	// Device da placa MSI
	int Ring;						// Indica se a placa MSI possui ring
};

static MSI_INFO MsiInfo[40];	// guarda informacoes sobre as placas MSI no sistema

static int NumBoardsMsi; // Numero de placas MSI no sistema

// Niveis de volume para estacao MSI em dB
static int TableStationLevel[] = { -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3 };


// endereco dos objetos de recursos para serem usados
static Dialogic *ObjectTapi[MAX_OBJ];
// endereco do objeto do canal que esta tocando tom de ocupado constantemente
static Dialogic *ObjetoTomOcupado;
// endereco do objeto do canal que esta tocando musica de espera constantemente
static Dialogic *ObjetoMusicaEspera;

/*
 -----------------------------------------
|	RX			TX															|
|	----------------------------------------|
|	A	|	B	|	A	|	B	|	CONDICAO DA LINHA				|
|	----------------------------------------|
|	1	|	0	|	1	|	0	|	LIVRE										|
|	0	|	0	|	1	|	0	|	OCUPACAO								|
|	0	|	0	|	1	|	1	|	CONFIRMACAO DE OCUPACAO	|
|	0	|	0	|	0	|	1	|	ATENDIMENTO							|
|	0	|	0	|	1	|	1	|	DESCONEXAO PARA TRAS		|
|	1	|	0	|	1	|	1	|	DESCONEXAO PARA FRENTE	|
|	1	|	0	|	1	|	0	|	DESCONEXAO/LIVRE				|
|	1	|	0	|	1	|	1	|	BLOQUEADO								|
 -----------------------------------------
*/

// frequencias dos tons r2 para frente
static const unsigned int forward_sig[16][2] = {
  0,    0,
  1380, 1500, //  SIG_I1  SIG_II1   101
  1380, 1620, //  SIG_I2  SIG_II2   102
  1500, 1620, //  SIG_I3  SIG_II3   103
  1380, 1740, //  SIG_I4  SIG_II4   104
  1500, 1740, //  SIG_I5  SIG_II5   105
  1620, 1740, //  SIG_I6  SIG_II6   106
  1380, 1860, //  SIG_I7  SIG_II7   107
  1500, 1860, //  SIG_I8  SIG_II8   108
  1620, 1860, //  SIG_I9  SIG_II9   109
  1740, 1860, //  SIG_I0  SIG_II0   110
  1380, 1980, //  SIG_I11 SIG_II11  111
  1500, 1980, //  SIG_I12 SIG_II12  112
  1620, 1980, //  SIG_I13 SIG_II13  113
  1740, 1980, //  SIG_I14 SIG_II14  114
  1860, 1980  //  SIG_I15 SIG_II15  115
};

// frequencias dos tons r2 para tras
static const unsigned int backward_sig[16][2] = { 
	0,		0,				  
	1140,	1020,	//	SIG_A1	SIG_B1		101
	1140,	900,	//	SIG_A2	SIG_B2		102
	1020,	900,	//	SIG_A3	SIG_B3		103
	1140,	780,	//	SIG_A4	SIG_B4		104
	1020,	780,	//	SIG_A5	SIG_B5		105
	900,	780,	//	SIG_A6	SIG_B6		106
	1140,	660,	//	SIG_A7	SIG_B7		107
	1020,	660,	//	SIG_A8	SIG_B8		108
	900,	660,	//	SIG_A9	SIG_B9		109
	780,	660,	//	SIG_A0	SIG_B0		110
	1140,	540,	//	SIG_A11	SIG_B11		111
	1020,	540,	//	SIG_A12	SIG_B12		112
	900,	540,	//	SIG_A13	SIG_B13		113
	780,	540,	//	SIG_A14	SIG_B14		114
	660,	540		//	SIG_A15	SIG_B15		115
};

// Converte numero para sinal R2
#define SigChar(Data)								((char)(Data % 10) + 48)

// Converte sinal R2 para numero
#define CharSig(Data)								((Data - 48) + SIG_BASE)

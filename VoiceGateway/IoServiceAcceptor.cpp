#include "IoServiceAcceptor.h"

/*
IoServiceAcceptor::IoServiceAcceptor(ISwitch * parent, log4cpp::Category * pLog4cpp , boost::shared_ptr<GarbageCollector> p_GarbageCollector) :      
    m_pLog4cppStart(pLog4cpp),
    m_bStop(false),
    m_GarbageCollector(p_GarbageCollector),
    m_Switch(parent) , 
    m_Acceptor( m_IoService, boost::asio::ip::tcp::endpoint( boost::asio::ip::tcp::v4(), IoServiceAcceptor::uLocalScreenPort ) ) 
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
}
*/

IoServiceAcceptor::IoServiceAcceptor(ISwitch * parent, log4cpp::Category * pLog4cpp ) :      
    m_pLog4cppStart(pLog4cpp),
    m_bStop(false),    
    m_Switch(parent) , 
    m_Acceptor( m_IoService, boost::asio::ip::tcp::endpoint( boost::asio::ip::tcp::v4(), IoServiceAcceptor::uLocalScreenPort ) ) 
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
}



IoServiceAcceptor::~IoServiceAcceptor(void)
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
}


void IoServiceAcceptor::Start()
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);

    m_ioServiceThrPtr.reset ( new boost::thread ( boost::bind (&IoServiceAcceptor::IoServiceThread, this)));
    m_ioServiceThrPtr->detach();
    
}

void IoServiceAcceptor::Stop()
{
    std::stringstream ss;
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
    m_bStop = true;
    m_IoService.stop();
	m_Acceptor.close();    

    /*
    std::list< StatusScreenNet * >::const_iterator iterator;
    for (iterator = m_StatusScreenList.begin(); iterator != m_StatusScreenList.end(); ++iterator) 
    {
        StatusScreenNet * pStatusScreenNet = (*iterator);
        ss.str(std::string());
		ss << boost::format("Deleting StatusScreenNet : %p ") % pStatusScreenNet;
		m_pLog4cppStart->debug( ss.str().c_str() );
        
        delete pStatusScreenNet;        
    }
    */
}
/*
void IoServiceAcceptor::AcceptSync()
{
    try
    {
        m_pLog4cppStart->debug("[%s]",__FUNCTION__);
        StatusScreenNet * p_StatusScreen;
        
        while(!m_bStop)
        {
            boost::asio::ip::tcp::socket socket_(m_IoService);
            m_Acceptor.accept(socket_);

            {
                boost::mutex::scoped_lock lk(  m_ListMutex );
                p_StatusScreen = new StatusScreenNet(m_IoService, m_Switch, this, m_pLog4cppStart, std::move(socket_));
                m_StatusScreenList.push_back(p_StatusScreen);
            }
        }
    }
    catch(...)
    {
        m_pLog4cppStart->debug("[%s] Accept() exception.",__FUNCTION__);
    }
}
*/
void IoServiceAcceptor::AcceptAsync()
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);

    //boost::shared_ptr<StatusScreenNetAsync>      sptr_StatusScreen;
    StatusScreenNetAsync *      sptr_StatusScreen;

    //sptr_StatusScreen.reset(new StatusScreenNetAsync(m_IoService, m_Switch, m_pLog4cppStart) );
    sptr_StatusScreen = new StatusScreenNetAsync(m_IoService, m_Switch, this , m_pLog4cppStart);
	
    //m_StatusScreenList.push_back(sptr_StatusScreen);

    m_Acceptor.async_accept( sptr_StatusScreen->GetSocket(), boost::bind( &IoServiceAcceptor::HandleAccept, this, sptr_StatusScreen, m_Switch, m_pLog4cppStart, _1 ) ); // OK
}

//void IoServiceAcceptor::HandleAccept( boost::shared_ptr<StatusScreenNetAsync> paramPtr, ISwitch * p_Switch,  log4cpp::Category * pLog4cpp, boost::system::error_code const& Error )
void IoServiceAcceptor::HandleAccept( StatusScreenNetAsync * paramPtr, ISwitch * p_Switch,  log4cpp::Category * pLog4cpp, boost::system::error_code const& Error )
{
    std::stringstream ss;    
    //boost::shared_ptr<StatusScreenNetAsync> statusScreenPtr;
    StatusScreenNetAsync * statusScreenPtr;

    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
    //if ( paramPtr.get() != NULL ) 
    if ( paramPtr != NULL ) 
    {
        paramPtr->Start();                

        //statusScreenPtr.reset(new StatusScreenNetAsync(m_IoService, p_Switch, pLog4cpp));        
        statusScreenPtr = new StatusScreenNetAsync(m_IoService, p_Switch, this , pLog4cpp);
		
        m_StatusScreenList.push_back(statusScreenPtr);
        m_Acceptor.async_accept( statusScreenPtr->GetSocket(), boost::bind( &IoServiceAcceptor::HandleAccept, this, statusScreenPtr, p_Switch, pLog4cpp, _1 ) );
    }

}

/*
void IoServiceAcceptor::AddToGarbage(StatusScreenNet * p_Obj)
{
    m_GarbageCollector->AddToGarbage(p_Obj);
}
*/


void IoServiceAcceptor::IoServiceThread()
{
    std::stringstream ss;    

    try 
    {
        AcceptAsync();        
        m_IoService.run();
        //AcceptSync();
    }
    catch( boost::system::system_error& err )
    {
        ss.str(std::string());
        ss << boost::format("ERROR: Exception on IO SERVICE ( IoServiceAcceptor ) run(): %s. \r\n") % err.what();
		
		std::cerr << ss.str().c_str();
        //Logger.LOG( SCLogger::SCLOG_ERROR, "Exception on Service.run(): %s\n", err.what() );
    }
    catch( ... )
    {
		ss.str(std::string());
        ss << boost::format("ERROR: Exception on IO SERVICE ( IoServiceAcceptor ) run(). \r\n");
		
		std::cerr << ss.str().c_str();
        //Logger.LOG( SCLogger::SCLOG_ERROR, "Unknown exception on Conference Service thread!\n" );
    }
}

void IoServiceAcceptor::SendMessageToAllScreens( std::string sMessage)
{
    boost::mutex::scoped_lock lk(  m_ListMutex );

    //std::list<StatusScreenNet *>::iterator it;
    std::list<StatusScreenNetAsync *>::iterator it;
    for (it = m_StatusScreenList.begin(); it != m_StatusScreenList.end(); ++it)
    {
        StatusScreenNetAsync * pStatusScreen = (*it);
        
        if ( (pStatusScreen != NULL) && (pStatusScreen->m_bStop == false ) )
            pStatusScreen->DoWrite(sMessage);
            //pStatusScreen->EnqueueWrite(sMessage);
    
    }
}

/*
void IoServiceAcceptor::SendMessageToAllScreens( std::string sMessage)
{
    boost::mutex::scoped_lock lk(  m_ListMutex );

    std::list<StatusScreenNet *>::iterator it;
    for (it = m_StatusScreenList.begin(); it != m_StatusScreenList.end(); ++it)
    {
        StatusScreenNet * pStatusScreen = (*it);
        
        if ( (pStatusScreen != NULL) && (pStatusScreen->m_bStop == false ) )
            pStatusScreen->EnqueueWrite(sMessage);
    
    }
}
*/

void IoServiceAcceptor::LogScreen(int ListIndex, int Item, char *s)
{
    std::stringstream ss;

	try
	{
        if ( m_Switch->GetFlagLogScreen() < 2 )        		
		{
			return;
		}

        ss << boost::format( "SS_LOG_BOARD?ID=%d&CHANNEL=%d&TEXT=%s\r\n") % ListIndex % Item % s;
		SendMessageToAllScreens( ss.str() );
	}
	catch(...)
	{
	}
}

void IoServiceAcceptor::SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg)
{
    std::stringstream ss;

    try
	{

        if ( m_Switch->GetFlagLogScreen() <= 0 )        		
		{
			return;
		}

		switch(Grid)
		{
			case 1:
				//Win::SendMessageToScreen( "SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n", ListIndex, Trunk, Item, sMsg );
				break;
			case 2:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&TYPE=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 3:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&DNIS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 4:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&ANI=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 5:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&INFO=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 6:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&BITS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
		}
	}
	catch(...)
	{
	}
}

ISwitch * IoServiceAcceptor::GetSwitch() const
{
    return m_Switch;
}
/************************************************************************************************/
/* Support Comm Teleinformatica S/A                                                             */
/*                                                                                              */
/* Modulo: DCB.h																																							  */
/* Funcao: Funcoes de baixo n�vel para uso da placa DCB																					*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato - Criacao do Modulo                                               */
/*                                                                                              */
/************************************************************************************************/
#ifndef DCB_H
#define DCB_H

// Disables the STL warning message.
#ifdef _DEBUG
	#pragma warning(disable:4786) 
#endif

//#include "log4cpp/Category.hh"
//#include "log4cpp/RollingFileAppender.hh"
//#include "log4cpp/OstreamAppender.hh"
//#include "log4cpp/Layout.hh"
//#include "log4cpp/PatternLayout.hh"
#include "Logger.h"

//#include "log4cpp/Priority.hh"
// User includes
#include "ConferenceInterface.h"
#include "ConferenceDefines.h"

// System includes
#include <iostream>
#include <strstream>
#include <vector>
#include <map>
#include <string>
#include <limits.h>
#include <fstream>

#include <time.h>
#include <sys\timeb.h>
#include <windows.h>

// Dialogic includes
#include <srllib.h>
#include <dxxxlib.h>
#include <msilib.h>
#include <dcblib.h> 
#include <errno.h>
//#include <sctools.h>
//#include <dtilib.h>

class DCBClass : public ConferenceInterfaceClass
{
	// Description:
	//	An structure containing a set of conferee informations.
	typedef struct _conferee_info
	{
		// Description:
		// User TimeSlot (Tx).
		long Timeslot;

        // Description:
		//	Conference TimeSlot (Tx).
		long ConferenceTimeslot;

		// Description:
		//	Conferee ANI.
		std::string ANI;

		// Parameters:
		//	None.
		// Return Value:
		//	None.
		// Description:
		//	Constructor.
		_conferee_info();
	} conferee_info;

	// Description:
	//	An structure containing a set of private conference informations.
	typedef struct _private_conference_info
	{

		// Description:
		//	Conference identifier.
		int Id;

		// Description:
		//	The board identifier from the board where is the conference.
		int BoardId;

		// Description:
		//	DSP device handle which the conference is.
		int DSPDeviceHandle;

		// Description:
		//	The number of conferees in the conference.
		int NumberOfConferees;

		// Description:
		//	Indicates the use of a notify tone in the conference.
		bool NotifyTone;

		// Description:
		//	A map of the conferees indexed by the conferee id.
		std::map < long, conferee_info > Conferees;

		// Parameters:
		//	None.
		// Return Value:
		//	None.
		// Description:
		//	Constructor.
		_private_conference_info();
	} private_conference_info;

	// Description:
	//	An structure containing a set of conference informations.
	typedef struct _conference_info
	{

		// Description:
		//	Conference identifier.
		int Id;

		// Description:
		//	The board identifier from the board where is the conference.
		int BoardId;

		// Description:
		//	The DSP identifier from the board where is the conference.
		int DspId;

		// Description:
		//	DSP device handle which the conference is.
		int DSPDeviceHandle;

        // Description:
		//	ChairPerson TimeSlot (Tx).
		long ChairPersonTimeslot;		

		// Description:
		//	Gost GhostTimeslot (Tx).
		long GhostTimeslot;

		// Description:
		//	The number of conferees in the conference.
		int NumberOfConferees;

		// Description:
		//	Indicates the use of a notify tone in the conference.
		bool NotifyTone;

		// Description:
		//	A map of the conferees indexed by the conferee id.
		std::map < long, conferee_info > Conferees;

		// Description:
		//	A map of the private conferences indexed by the conferee 4-last ANI digits.
		std::map < std::string, private_conference_info > PrivateConferences;

		// Parameters:
		//	None.
		// Return Value:
		//	None.
		// Description:
		//	Constructor.
		_conference_info();
	} conference_info;

	typedef struct _dsp_info
	{
		// Description:
		//	Device handle for the DSP.
		int DeviceHandle;

		// Description:
		//	Flag indicating the DSP use.
		int ResourcesCount;

		// Description:
		// Board name in use
		std::string BoardName;

		// Parameters:
		//	None.
		// Return Value:
		//	None.
		// Description:
		//	Constructor.
		_dsp_info();
	} dsp_info;
	
	typedef struct _resource_info
	{

		// Description:
		//	The number of board that has the best DSP chosen
		int BoardId;

		// Description:
		//	The number of DSP that has the best DSP chosen.
		int DspId;

		// Description:
		//	The handle of best DSP chosen
		int DSPHandle;

		// Parameters:
		//	None.
		// Return Value:
		//	None.
		// Description:
		//	Constructor.
		_resource_info();
	} resource_info;

	typedef struct _chair_person
	{

        // Description:
		//	ChairPerson TimeSlot (Tx).
		long Timeslot;		

		// Description:
		//	The number of participates 
		int Contador;

		_chair_person();
	} chair_person;

    // ponteiro para a classe log
    //log4cpp::Category * m_pLog4cpp;	
    boost::shared_ptr<Logger> m_pLog4cpp;

	// Description:
	//	Not defined.
	typedef std::map < int, dsp_info > dsp_device_handles;
	typedef std::map < int, dsp_device_handles > boards;
	boards Boards;

    // Description:
	//	A map of the conferences indexed by the conference id.
    std::map < std::string, conference_info > map_Conferences;

	// Description:
	//	A map of the chair person indexed by the conference id.
    std::map < std::string, chair_person > ChairPerson;

    std::string m_RoomId;

	// Description:
	//	Conference descriptor table.
	MS_CDT MsCdt[32];

	// Description:
	//	An integer value specifying the return code for function calls.
	int ReturnCode;

	// Description:
	//	An string stream specifying the last error message.
	std::strstream LastErrorMessage;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the board and DSP number if the function succeeds or CONF_ERROR in both if the function fails.
	// Description:
	//	This function returns the best DSP to create a conference in the board device.
	int ChooseBestDSP(resource_info &info);

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	This function clears the last error message buffer.
	void ClearLastErrorMessage();

  // Parameters:
	//	lpcszConferenceId  	- [in] An char point value identifying the conference created.
	//	bNotifyTone					- [in] An boolean value indicating if a notification will be generated when a conferee joins/leaves the conference. The default value is bNotifyTone = true.
	// Return Value:
	//	The return value is zero if the function succeeds or CONF_ERROR if the function fails.
	// Description:
	//	This function creates a new conference.
	// See Also:
	//	Destroy.
	int Create( const char* lpcszConferenceId, const bool& bNotifyTone = true );

  // Parameters:
	//	lpcszConferenceId		- [in] An char point value identifying the conference.
	// Return Value:
	//	The return value is zero if the function succeeds or CONF_ERROR if the function fails.
	// Description:
	//	This function destroys a previously created conference.
	// See Also:
	//	Create.
	int Destroy( const char* lpcszConferenceId );

  // Parameters:
	//	lpcszConferenceId		- [in] An char point value identifying the conference.
	// Return Value:
	//	The return value is the number of users from a conference
	//  Returns CONF_ERROR if the respective conference does not exist or the number of conferees
	// Description:
	//	This function returns the number of users from a conference.
	// See Also:
	//	GetCapacity.
	int GetNumberOfConferees( const char* lpcszConferenceId );

  // Parameters:
	//	iDSPHandle			- [in] An integer value identifying the DSP device handle.
	//	iConferenceId		- [in] An integer value identifying the conference.
	//	lTimeSlot				- [in] An long value identifying the timeslot for conferee in the conference.
	// Return Value:
	//	The return value is true if in the given conferee is present in the given conference or false if it is not.
	// Description:
	//	This function verifies if a given conferee is present in the given conference.
	bool VerifyConfereePresence( const int& iDSPHandle, const int& iConferenceId, long& lTimeSlot );

  // Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	This function logs the conference status from the DCB board.
	void Debug();

public:

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Constructor.
	DCBClass(boost::shared_ptr<Logger> pLog4cpp);

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Destructor.
	~DCBClass();

	// Parameters:
	//	iBoardId	- [in] An integer value specifying the board identifier.
	//	cbtBoardType	- [in] An enumerated type value specifying the board type.
	// Return Value:
	//	The return value is zero if the function succeeds or CONF_ERROR if the function fails.
	// Description:
	//	This function opens a board device given its identifier.
	// See Also:
	//	CloseBoard.
	virtual int OpenBoard( const int& iBoardId, const char* sBoardName );

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or CONF_ERROR if the function fails.
	// Description:
	//	This function closes a board device previously openned.
	// See Also:
	//	OpenBoard.
	virtual int CloseBoard( const int& iBoardId = 0 );

    // Parameters:
	//	lpcszConferenceId	- [in] An char point value identifying the conference.  
	//	lTimeSlot				  - [in] An long value identifying the timeslot for conferee in the conference.
	//	iConferenceSize 	- [in] An integer value specifying the number of conferees (parties) of the conference. The default value is iNumberOfConferees = 4.
    //	cctConfereeType	  - [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach).
	//	bNotifyTone			  - [in] A boolean value specifying if the conferee is a supervisor or not. The default value is bNotifyTone = false.
	// Return Value:
	//	The return value is zero if the function succeeds, 1 if the conference has already reached the full capacity, CONF_ERROR if the function fails.
	// Description:
	//	This function adds an user to a previously created conference.
	// See Also:
	//	Remove, cctConfereeType.
    virtual int Add( const char * lpcszConferenceId, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot );	

    // Parameters:
	//	lpcszConferenceId	- [in] An char point value identifying the conference.
	//	lTimeSlot					-	[in] An long value identifying the timeslot for conferee in the conference.
    //	cctConfereeType	  - [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach).
	// Return Value:
	//	The return value is zero if the function succeeds, 1 if conference is full or CONF_ERROR if the function fails.
	// Description:
	//	This function removes an user from a conference.
	// See Also:
	//	Add.
	virtual int Remove( const char * lpcszConferenceId, long& lTimeSlot, conference_conferee_type cctConfereeType);	

    // Parameters:
	//	lpcszConferenceIdSource				- [in] An char point value identifying the conference where the conferee is.
	//	lpcszConferenceIdDestination	- [in] An char point value identifying the conference where the conferee is going.
	//	lTimeSlot											- [in] An long value identifying the timeslot for conferee in the conference.
	//	iConferenceSize 							- [in] An integer value specifying the number of conferees (parties) of the conference. The default value is iNumberOfConferees = 4.
    //	cctConfereeType								- [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach).
	//	bNotifyTone										- [in] A boolean value specifying if the conferee is a supervisor or not. The default value is bNotifyTone = false.
	// Return Value:
	//	The return value is zero if the function succeeds, 1 if the conference has already reached the full capacity, CONF_ERROR if the function fails.
	// Description:
	//	This function moves an user from a given conference to another previously created conference.
	// See Also:
	//	Add, Remove, cctConfereeType.
    virtual int Change( const char * lpcszConferenceIdSource, const char * lpcszConferenceIdDestination, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot );

	// Parameters:
	//	lpcszConferenceId	- [in] An char point value identifying the conference.
	//	lTimeSlot			- [in] An long value identifying the timeslot for conferee in the conference.
	// Return Value:
	//	The return value is zero if the function succeeds or negative if the function fails.
	// Description:
	//	This function sets the conferee attributes in a given conference.
	// See Also:
	//	Add, Remove.
	virtual int SetConfereeAttribute( const char* lpcszConferenceId, const long& lTimeSlot, conference_conferee_type cctConfereeType = DUPLEX );  
  
    // Parameters:
	//	lpcszConferenceId	- [in] An char point value identifying the conference.
	//	lTimeSlot			- [in] An long value identifying the timeslot for conferee in the conference.
	// Return Value:
	//	The return value is timeslot if the function succeeds or negative if the function fails.
	// Description:
	//	This function returns the output timeslot for conferee in the conference.
    virtual int GetConferenceTimeSlot( const char * lpcszConferenceId, long& lTimeSlot );

    // Parameters:
	//	lpcszConferenceId			- [in] An char point value identifying the conference.
	//	lChairPersonTimeSlot	- [in] An long value will receive the timeslot for chair person in the conference.
	// Return Value:
	//	The return value is timeslot if the function succeeds or negative if the function fails.
	// Description:
	//	This function returns the output timeslot for conferee in the conference.
    virtual int GetChairPersonTimeSlot( const char * lpcszConferenceId, long& lChairPersonTimeSlot, conference_conferee_type cctConfereeType = DUPLEX );

    // Verifica se o esse timeslot n�o foi removido Para o Chat-Conferencia
	virtual int VerifyConfereeExistAndRemove( const long& lTimeSlot );

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is pointer to a null-terminated string specifying the last error message.
	// Description:
	//	This function returns the last error message.
	virtual const char* GetLastError();
};

#endif // DCB_H

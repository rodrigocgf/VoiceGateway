/************************************************************************************************/
/* Support Comm Teleinformatica S/A																*/
/*                                                                                              */
/* Modulo: LuaConference.cpp																	*/
/* Funcao: Funcoes relativas ao controle da confer�ncia	para a camada LUA						*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato/Jairo Rosa - Criacao do Modulo                                    */
/*               Ricardo Hayama Reis                                                            */
/* 16/JUL/2019 - Rodrigo C. G. Fran�a                                                           */
/*                                                                                              */
/* OBS: M�dulo pertencente ao "Chat Novo" utilizado em algumas plataformas (ex: Oi)				*/
/************************************************************************************************/
#include "Ivr.h"
#include "Switch.h"


//* Declara��o das Fun��es */
int LuaConferenceAdd( lua_State *L );
int LuaConferenceRemove( lua_State *L );
int LuaConferenceListen( lua_State *L );
int LuaConferenceUnlisten( lua_State *L );
int LuaConferenceChangeStatus( lua_State *L );


/* Estrutura que guarda o nome das fun��es no LUA */
static const struct luaL_reg ConferenceLibFunctions[] = 
{
	{"ConferenceAdd",											LuaConferenceAdd },
	{"ConferenceRemove",									LuaConferenceRemove },
	{"ConferenceListen",									LuaConferenceListen },
	{"ConferenceUnlisten",								LuaConferenceUnlisten },
    {"ConferenceChangeStatus",						LuaConferenceChangeStatus }
};

Ivr * GetIvrConference( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );

    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );

    lua_remove( L, -1 );
    return pIvr;
}


void Lua_ConferenceLibOpen( lua_State *L )
{
    //luaL_openlib( L, "Conference", ConferenceLibFunctions, 0 );
    luaL_openl( L,  ConferenceLibFunctions );
}


//--------------------------------------------------------------------------------------------
// LuaConferenceAdd(const char* conferenceId, const char* ani, int* confType, int* confType, int* notifytone)
// Coloca o usu�rio na sala de confer�ncia
// Retorno:	1 se sucesso, 0 se erro, -1 se usu�rio desligou

int LuaConferenceAdd( lua_State *L )
{
    //LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
    Ivr * pIvr = NULL;
    pIvr = GetIvrConference(L);

	try
	{
		int rc, iNumArgs;
        const char *lpcszConferenceId;
        int iConferenceSize;		
		const char *sAni;
        int iConfType = 0;
        bool bSupervisor = false;
		bool bNotifyTone = true;

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

		if( iNumArgs == 3 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );
            iConferenceSize = luaL_check_int( L, 2 );
			sAni = (char *)luaL_check_string( L, 3 );
            iConfType = 0;
            bSupervisor = false;
			bNotifyTone = true;
		}
		else if( iNumArgs == 4 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );			
            iConferenceSize = luaL_check_int( L, 2 );
			sAni = (char *)luaL_check_string( L, 3 );
			iConfType = luaL_check_int( L, 4 );
            bSupervisor = false;
			bNotifyTone = true;
		}
        else if( iNumArgs == 5 )
		{
			lpcszConferenceId =  (char *)luaL_check_string( L, 1 );			
            iConferenceSize = luaL_check_int( L, 2 );
			sAni = (char *)luaL_check_string( L, 3 );
			iConfType = luaL_check_int( L, 4 );
            bSupervisor = ( luaL_check_int( L, 5 ) ? true : false );
			bNotifyTone = true;
		}
            else if( iNumArgs == 6 )
		{
			lpcszConferenceId =  (char *)luaL_check_string( L, 1 );			
            iConferenceSize = luaL_check_int( L, 2 );
			sAni = (char *)luaL_check_string( L, 3 );
			iConfType = luaL_check_int( L, 4 );
            bSupervisor = ( luaL_check_int( L, 5 ) ? true : false );
			bNotifyTone = ( luaL_check_int( L, 6 ) ? true : false );
		}
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaConferenceAdd()." );
			return 0;
		}
		
		rc = pIvr->ConferenceAdd(lpcszConferenceId, iConferenceSize, iConfType, bNotifyTone);
		lua_pushnumber( L, rc );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaConferenceAdd()" );
		return 0;
	}
}


//--------------------------------------------------------------------------------------------
// LuaConferenceRemove(const char* conferenceId, int* confType)
// Retira o usu�rio da sala de confer�ncia
// Retorno:	1 se sucesso, 0 se erro, -1 se usu�rio desligou

int LuaConferenceRemove( lua_State *L )
{
    //LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
    Ivr * pIvr = NULL;
    pIvr = GetIvrConference(L);

	try
	{
		int rc, iNumArgs;
		const char *lpcszConferenceId;
        int iConfType = 0;		

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

		if( iNumArgs == 1 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );
            iConfType = 0;
		}
		else if( iNumArgs == 2 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );
			iConfType = luaL_check_int( L, 2 );
		}
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaConferenceRemove()." );
			return 0;
		}

		rc = pIvr->ConferenceRemove(lpcszConferenceId, iConfType);
		lua_pushnumber( L, rc );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaConferenceRemove()" );
		return 0;
	}
}


//--------------------------------------------------------------------------------------------
// LuaConferenceListen(const char* conferenceId)
// Conecta o usu�rio na sala de confer�ncia
// Retorno:	1 se sucesso, 0 se erro, -1 se usu�rio desligou

int LuaConferenceListen( lua_State *L )
{
    //LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
    Ivr * pIvr = NULL;
    pIvr = GetIvrConference(L);

	try
	{
		int rc, iNumArgs;
		const char *lpcszConferenceId;

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

		if( iNumArgs == 1 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );
		}
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaConferenceListen()." );
			return 0;
		}

		rc = pIvr->ConferenceListen(lpcszConferenceId);
		lua_pushnumber( L, rc );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaConferenceListen()" );
		return 0;
	}
}


//--------------------------------------------------------------------------------------------
// LuaConferenceUnlisten(const char* conferenceId)
// Desconecta o usu�rio da sala de confer�ncia para poder ouvir o prompt
// Retorno:	1 se sucesso, 0 se erro, -1 se usu�rio desligou

int LuaConferenceUnlisten( lua_State *L )
{
    //LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
    Ivr * pIvr = NULL;
    pIvr = GetIvrConference(L);

	try
	{
		int rc, iNumArgs;
		const char *lpcszConferenceId;

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

		if( iNumArgs == 1 )
		{
			lpcszConferenceId = (char *)luaL_check_string( L, 1 );
		}
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaConferenceUnlisten()." );
			return 0;
		}

		rc = pIvr->ConferenceUnlisten(lpcszConferenceId);
		lua_pushnumber( L, rc );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaConferenceUnlisten()" );
		return 0;
	}
}


//--------------------------------------------------------------------------------------------
// LuaConferenceChangeStatus(const char* conferenceId, const int& iConfereeType, const int& iUnListen)
// Muda o status do usuario na conferencia. E pode fazer ele ficar sem ouvir a converencia.
// Retorno:	1 se sucesso, 0 se erro, -1 se usu�rio desligou

int LuaConferenceChangeStatus( lua_State *L )
{
    //LUA_USERDATA *Lua_UserData = (LUA_USERDATA *) LuaGetUserData( L );
    Ivr * pIvr = NULL;
    pIvr = GetIvrConference(L);

	try
	{
		int rc, iNumArgs;
		const char *lpcszConferenceId;
        int iConfereeType = 0, iUnListen = 0;		

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

		if( iNumArgs < 2 )
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaConferenceUnlisten()." );
			return 0;      
        }
        else if( iNumArgs == 2 )
		{
            lpcszConferenceId = (char *)luaL_check_string( L, 1 );
            iConfereeType     = luaL_check_int( L, 2 );
            iUnListen         = 0;
        }
        else if( iNumArgs == 3 )
		{
            lpcszConferenceId = (char *)luaL_check_string( L, 1 );
            iConfereeType     = luaL_check_int( L, 2 );
            iUnListen         = luaL_check_int( L, 3 );
        }
		
        rc = pIvr->ConferenceChangeStatus(lpcszConferenceId, iConfereeType, iUnListen);
		lua_pushnumber( L, rc );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaConferenceUnlisten()" );
		return 0;
	}
}

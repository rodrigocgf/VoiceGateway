/************************************************************************************************/
/* SupportComm S.A.																												                			*/
/*                                                                                              */
/* Modulo: Lua_Audio.cpp																																	    */
/* Funcao: Funcoes relativas ao compartilhamento do audio da MSI           												*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 25/05/2004 - Hilda Matsumura - Criacao do Modulo                                             */
/*                                                                                              */
/************************************************************************************************/

#include "Ivr.h"
#include "Switch.h"

// RETORNO DAS FUNCOES DE TELEFONIA CASO O USUARIO DESLIGUE
#define R_CALL_DISCONNECTED					-1

// Declara��o das Fun��es
int LuaGetTimeSlot          (lua_State *L);
int LuaGetTimeSlotVox       (lua_State *L);
int LuaGetGlobalTimeSlot    (lua_State *L);
int LuaAudioLocalListen			(lua_State *L);
int LuaAudioLocalUnlisten		(lua_State *L);
int LuaAudioGlobalListen		(lua_State *L);
int LuaAudioGlobalUnlisten	(lua_State *L);
int LuaStopPlay							(lua_State *L);
int LuaSetVolume						(lua_State *L);
int LuaSetSpeed							(lua_State *L);
int LuaSetPlayFormat				(lua_State *L);
int LuaSetRecordFormat			(lua_State *L);
int LuaSetPCM								(lua_State *L);
int LuaPlayFile							(lua_State *L);
int LuaPlayFileAsync				(lua_State *L);
int LuaRemotePlayFileAsync	(lua_State *L);
int LuaWaitForPlayStopped		(lua_State *L);
int LuaForcedPlayFile				(lua_State *L);
int LuaRecordFile						(lua_State *L);
int LuaRemoteRecordFile			(lua_State *L);
int LuaGetDigits						(lua_State *L);
int LuaSetDigMask						(lua_State *L);
int LuaClearDigits					(lua_State *L);
int LuaAddTTS								(lua_State *L);
int LuaPlayTTS							(lua_State *L);
int LuaPlay									(lua_State *L);
int LuaClearPrompts					(lua_State *L);
int LuaAddFile							(lua_State *L);
int LuaAddRemoteFile				(lua_State *L);
int LuaRemotePlayFile				(lua_State *L);
int LuaPlayFileUntil				(lua_State *L);
int LuaStopRecordFile				(lua_State *L);
int LuaPlayFileUntilEvent		(lua_State *L);
int LuaRecordFileWithMusic	(lua_State *L);

// Estrutura que guarda o nome das fun��es no LUA
static const struct luaL_reg AudioLibFunctions[] = 
{
  {"GetTimeSlot",										LuaGetTimeSlot					},
  {"GetTimeSlotVox",								LuaGetTimeSlotVox				},
  {"GetGlobalTimeSlot",							LuaGetGlobalTimeSlot		},
	{"AudioLocalListen",							LuaAudioLocalListen			},
	{"AudioLocalUnlisten",	  				LuaAudioLocalUnlisten		},
	{"AudioGlobalListen",							LuaAudioGlobalListen		},
	{"AudioGlobalUnlisten",	  				LuaAudioGlobalUnlisten	},
	{"StopPlay",											LuaStopPlay							},
	{"SetVolume",											LuaSetVolume						},
	{"SetSpeed",											LuaSetSpeed							},
	{"SetPlayFormat",									LuaSetPlayFormat				},
	{"SetRecordFormat",								LuaSetRecordFormat			},
	{"SetPCM",												LuaSetPCM								},
	{"PlayFile",											LuaPlayFile							},
	{"PlayFileAsync",									LuaPlayFileAsync				},
	{"RemotePlayFileAsync",						LuaRemotePlayFileAsync	},
	{"WaitForPlayStopped",						LuaWaitForPlayStopped		},
	{"ForcedPlayFile",								LuaForcedPlayFile				},
	{"RecordFile",										LuaRecordFile						},
	{"RemoteRecordFile",							LuaRemoteRecordFile			},
	{"GetDigits",											LuaGetDigits						},
	{"SetDigMask",										LuaSetDigMask						},
	{"ClearDigits",										LuaClearDigits					},
	{"AddTTS",												LuaAddTTS								},
	{"PlayTTS",												LuaPlayTTS							},
	{"Play",													LuaPlay									},
	{"LuaClearPrompts",								LuaClearPrompts					},
	{"AddFile",												LuaAddFile							},
	{"AddRemoteFile",									LuaAddRemoteFile				},
	{"RemotePlayFile",								LuaRemotePlayFile				},
	{"PlayFileUntil",									LuaPlayFileUntil				},
  {"StopRecordFile",								LuaStopRecordFile				},
	{"PlayFileUntilEvent",						LuaPlayFileUntilEvent		},
	{"RecordFileWithMusic",						LuaRecordFileWithMusic	}
};

Ivr * GetIvrAudio( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );
    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return pIvr;
}

void Lua_AudioLibOpen( lua_State *L )
{
    //luaL_openlib( L, "Audio", AudioLibFunctions, 0 );
    luaL_openl( L, AudioLibFunctions );
}


//-----------------------------------------------------------------------------------------------------------------------

int LuaGetTimeSlot(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    try
	{
        int iRet;		
    
        // Recupera o TimeSlot do canal
        iRet = pIvr->GetCTI()->GetTimeSlot();
        lua_pushnumber (L, iRet);
	    return 1;

    }
    catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetTimeSlot()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetTimeSlotVox(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    try
	{
        int iRet;		
    
        // Recupera o TimeSlotVox do canal
        iRet = pIvr->GetCTI()->GetTimeSlotVox();
        lua_pushnumber (L, iRet);
		return 1;

    }
    catch(...)
    {		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetTimeSlotVox()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetGlobalTimeSlot(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    try
	{
        int iRet;		
    
        // Recupera o TimeSlot Global do canal
        iRet = pIvr->GetGlobalTimeSlot();
        lua_pushnumber (L, iRet);
		return 1;

    }
    catch(...)
	{
		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetTimeSlotVox()" );
		return 0;
	}
}

//--------------------------------------------------------------------------------------------
// LuaAudioLocalListen( long* lAudioTimeSlot)
// Coloca o usu�rio para ouvir o audio de uma linha na mesma m�quina
// Retorno:	0 se sucesso, 1 se erro, -1 se usu�rio desligou

int LuaAudioLocalListen( lua_State *L )
{	
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    try
	{
		int rc, iNumArgs;
        long lAudioTimeSlot;
		

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

        if( iNumArgs == 1 )
        {
            lAudioTimeSlot = luaL_check_int( L, 1 );
        }
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaAudioLocalListen()." );
			return 0;
		}
		
        // Retorna zero se for ok
		rc = pIvr->GetCTI()->ScListen(lAudioTimeSlot);
		if( rc < 0 )
			lua_pushnumber( L, 1 );
		else
			lua_pushnumber( L, 0 );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaAudioLocalListen()" );
		return 0;
	}
}


//--------------------------------------------------------------------------------------------
// LuaAudioLocalUnlisten()
// Retira o audio do usuario que est� na mesma m�quina
// Retorno:	0 se sucesso, 1 se erro, -1 se usu�rio desligou

int LuaAudioLocalUnlisten( lua_State *L )
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try
	{
		int rc;
        long lTimeSlot;		

        lTimeSlot = pIvr->GetCTI()->GetTimeSlotVox();

        // Retorna zero se for ok
		rc = pIvr->GetCTI()->ScListen(lTimeSlot);
		if( rc < 0 )
			lua_pushnumber( L, 1 );
		else
			lua_pushnumber( L, 0 );

		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaAudioLocalUnlisten()" );
		return 0;
	}
}

//--------------------------------------------------------------------------------------------
// LuaAudioGlobalListen( long* lAudioTimeSlot)
// Coloca o usu�rio para ouvir o audio de uma linha remota
// Retorno:	0 se sucesso, 1 se erro, -1 se usu�rio desligou

int LuaAudioGlobalListen( lua_State *L )
{	
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    try
	{
		int rc, iNumArgs;
        long lTimeSlotRing;		

		// Pega o numero de argumentos
		iNumArgs = lua_gettop( L );

        if( iNumArgs == 1 )
        {
            lTimeSlotRing = luaL_check_int( L, 1 );
        }
		else
		{
			pIvr->Log( LOG_SCRIPT, "Invalid number of arguments in a call to LuaAudioGlobalListen()." );
			return 0;
		}		

		rc = pIvr->ListenGlobalAudio(lTimeSlotRing);
		if( rc < 0 )
			lua_pushnumber( L, 1 );
		else
			lua_pushnumber( L, 0 );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaAudioGlobalListen()" );
		return 0;
	}
}

//--------------------------------------------------------------------------------------------
// LuaAudioGlobalUnlisten()
// Retira o audio do usuario que se encontra em uma m�quina remota
// Retorno:	0 se sucesso, 1 se erro, -1 se usu�rio desligou

int LuaAudioGlobalUnlisten( lua_State *L )
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try
	{
		int rc;		

        // Retorna zero se for ok
		rc = pIvr->UnListenGlobalAudio();
		if( rc < 0 )
			lua_pushnumber( L, 1 );
		else
			lua_pushnumber( L, 0 );

		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaAudioGlobalUnlisten()" );
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaStopPlay(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {		
		pIvr->Log(LOG_SCRIPT, "StopPlay()");

		iRet = pIvr->GetCTI()->StopCh();

		if(iRet == T_DISCONNECT)
        {
			pIvr->Log(LOG_SCRIPT, "Call disconnected");
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber (L, R_CALL_DISCONNECTED);
			return 1;
		}

		if(iRet == -1)
        {
		    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
			// Quando der erro retorna 1
			lua_pushnumber (L, 1);
			return 1;
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaStopPlay()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetVolume(lua_State *L)
{
	int iNumArgs, iLevel;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0)
        {
			return 0;
		}

		iLevel = luaL_check_int(L, 1);
		pIvr->Log(LOG_SCRIPT, "SetVolume(%d)", iLevel);

		//	1		Increase Play-Back Volume by 2db
		//	2		Increase Play-Back Volume by 4db
		//	3		Increase Play-Back Volume by 6db
		//	4		Increase Play-Back Volume by 8db
		//	0		Normal Volume
		//	-1	Decrease Play-Back Volume by 2db
		//	-2	Decrease Play-Back Volume by 4db
		//	-3	Decrease Play-Back Volume by 6db
		//	-4	Decrease Play-Back Volume by 8db

		pIvr->GetCTI()->Volume(iLevel);

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetVolume()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetSpeed(lua_State *L)
{
	int iNumArgs, iLevel;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);
		if(iNumArgs <= 0)
        {
			return 0;
		}

		iLevel = luaL_check_int(L, 1);

		pIvr->Log(LOG_SCRIPT, "SetSpeed(%d)", iLevel);

		//	1		Speed up Play-Back 10 Percent
		//	2		Speed up Play-Back 20 Percent
		//	3		Speed up Play-Back 30 Percent
		//	4		Speed up Play-Back 40 Percent
		//	5		Speed up Play-Back 50 Percent
		//	0		Normal Speed
		//	-1	Slow Down Play-Back 10 Percent
		//	-2	Slow Down Play-Back 20 Percent
		//	-3	Slow Down Play-Back 30 Percent
		//	-4	Slow Down Play-Back 40 Percent
		//	-5	Slow Down Play-Back 50 Percent

		pIvr->GetCTI()->Speed(iLevel);

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetSpeed()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetPlayFormat(lua_State *L)
{
	int iNumArgs, iFormat;
	unsigned int iSize;
	char sPar[20];

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);
	
	try 
    {
		pIvr->Log(LOG_SCRIPT, "SetPlayFormat()");

		/*
		
		Parametros possiveis

		VOX24,				Formato de arquivo de voz ADCPCM			  6KHZ	4 BITS
		VOX32,				Formato de arquivo de voz ADCPCM				8KHZ	4 BITS
		PCM48,				Formato de arquivo de voz PCM					  6KHZ	8 BITS
		PCM64,				Formato de arquivo de voz PCM						8KHZ	8 BITS
		WAV,					Formato de arquivo de voz WAV						11KHZ 8	BITS
		WAV_G711_ALAW,Formato de arquivo de voz WAV_G711_ALAW	8KHZ	8	BITS (DM3)
		PCM_G711_ALAW,Formato de arquivo de voz PCM_G711_ALAW	8KHZ	8	BITS (DM3)
        WAV_PCM			 ,Formato de arquivo de voz WAV_PCM				8KHZ	8	BITS (DM3)

		*/

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}
		
		memset(sPar, 0, sizeof(sPar));
		iSize = sizeof(sPar) -1;
		
		strcpy(sPar, luaL_check_lstr(L, 1, &iSize));

		if( strcmp(sPar, "VOX24") == 0 )
			iFormat = 0;
		else
			if( strcmp(sPar, "VOX32") == 0 )
				iFormat = 1;
		else
			if( strcmp(sPar, "PCM48") == 0 )
				iFormat = 2;
		else
			if( strcmp(sPar, "PCM64") == 0 )
				iFormat = 3;
		else
			if( strcmp(sPar, "WAV")		== 0 )
				iFormat = 4;
		else
			if( strcmp(sPar, "WAV_G711_ALAW")		== 0 )
				iFormat = 5;
		else
			if( strcmp(sPar, "PCM_G711_ALAW")		== 0 )
				iFormat = 6;
    else
			if( strcmp(sPar, "WAV_PCM")		== 0 )
				iFormat = 7;
			else
				return 0; // parametro invalido

		pIvr->GetCTI()->SetPlayFormat(iFormat);

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetPlayFormat()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetRecordFormat(lua_State *L)
{
	int iNumArgs, iFormat;
	unsigned int iSize;
	char sPar[20];

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);
	
	try 
    {
		pIvr->Log(LOG_SCRIPT, "SetRecordFormat()");

		/*
		
		Parametros possiveis

		VOX24,				Formato de arquivo de voz ADCPCM			  6KHZ	4 BITS
		VOX32,				Formato de arquivo de voz ADCPCM				8KHZ	4 BITS
		PCM48,				Formato de arquivo de voz PCM					  6KHZ	8 BITS
		PCM64,				Formato de arquivo de voz PCM						8KHZ	8 BITS
		WAV,					Formato de arquivo de voz WAV						11KHZ 8	BITS
		WAV_G711_ALAW,Formato de arquivo de voz WAV_G711_ALAW	8KHZ	8	BITS (DM3)
		PCM_G711_ALAW,Formato de arquivo de voz PCM_G711_ALAW	8KHZ	8	BITS (DM3)
    WAV_PCM			 ,Formato de arquivo de voz WAV_PCM				8KHZ	8	BITS (DM3)
		
		*/

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}
		
		memset(sPar, 0, sizeof(sPar));
		iSize = sizeof(sPar) -1;
		
		strcpy(sPar, luaL_check_lstr(L, 1, &iSize));

		if( strcmp(sPar, "VOX24") == 0 )
			iFormat = 0;
		else
			if( strcmp(sPar, "VOX32") == 0 )
				iFormat = 1;
		else
			if( strcmp(sPar, "PCM48") == 0 )
				iFormat = 2;
		else
			if( strcmp(sPar, "PCM64") == 0 )
				iFormat = 3;
		else
			if( strcmp(sPar, "WAV")		== 0 )
				iFormat = 4;
		else
			if( strcmp(sPar, "WAV_G711_ALAW")		== 0 )
				iFormat = 5;
		else
			if( strcmp(sPar, "PCM_G711_ALAW")		== 0 )
				iFormat = 6;
    else
			if( strcmp(sPar, "WAV_PCM")		== 0 )
				iFormat = 7;
			else
				return 0; // parametro invalido

		pIvr->GetCTI()->SetRecordFormat(iFormat);

		return 0;
	}
	catch(...){		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetRecordFormat()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetPCM(lua_State *L)
{
	int iNumArgs, iFormat;
	unsigned int iSize;
	char sPar[20];

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);
	
	try 
    {
		pIvr->Log(LOG_SCRIPT, "SetPCM()");

		/* Definido em Dialogic.h
		enum PCMEnconding {
			ULAW,								// Formato ULAW de PCMEnconding para arquivo de voz do tipo PCM
			ALAW								// Formato ALAW de PCMEnconding para arquivo de voz do tipo PCM
		};*/

		// Parametros possiveis
		// ULAW Formato ULAW de PCMEnconding para arquivo de voz do tipo PCM
		// ALAW Formato ALAW de PCMEnconding para arquivo de voz do tipo PCM

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}
		
		memset(sPar, 0, sizeof(sPar));
		iSize = sizeof(sPar) -1;
		
		strcpy(sPar, luaL_check_lstr(L, 1, &iSize));

		if( strcmp(sPar, "ULAW") == 0 )
			iFormat = 0;
		else
			if( strcmp(sPar, "ALAW") == 0 )
				iFormat = 1;
		else
			return 0; // parametro invalido

		pIvr->GetCTI()->SetPCM(iFormat);

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetPCM()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaPlayFile(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs, i, iRet;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "PlayFile()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		for(i = 1; i <= iNumArgs; i++){

			// monta o caminho para o arquivo de voz a ser reproduzido
            sprintf(sNomeArq, "%s\\%s", pIvr->GetSwitch()->GetPathScripts().c_str(), luaL_check_string(L, i));
			
			pIvr->Log(LOG_SCRIPT, sNomeArq);

			iRet = pIvr->GetCTI()->PlayFile(sNomeArq);

			if(iRet == T_DISCONNECT){
				pIvr->Log(LOG_SCRIPT, "Call disconnected");
				pIvr->SetScriptHangupFlag(true);
				lua_pushnumber (L, R_CALL_DISCONNECTED);
				return 1;
			}

			if(iRet == -1)
            {
				pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
				// Quando der erro retorna 1
				lua_pushnumber (L, 1);
				return 1;
			}
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPlayFile()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaPlayFileAsync(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "PlayFileAsync()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
        sprintf(sNomeArq, "%s\\%s", pIvr->GetSwitch()->GetPathScripts().c_str(), luaL_check_string(L, 1));

		pIvr->GetCTI()->ClearDigits();
		
		if( pIvr->GetCTI()->PlayFileAsync(sNomeArq) ){
			pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
			// Quando der erro retorna 1
			lua_pushnumber (L, 1);
			return 1;
		}

		//  Pega eventos por pelo menos 1 segundo para dar change a placa de iniciar o PlayFile,
		// porem devemos monitorar para ver se o usuario desligou nesse momento
		if( pIvr->GetCTI()->WaitForDisconnect(1000) ){
			pIvr->Log(LOG_SCRIPT, "Call disconnected");
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber (L, R_CALL_DISCONNECTED);
			return 1;
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPlayFileAsync()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaRemotePlayFileAsync(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "RemotePlayFileAsync()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0)
        {
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
		sprintf(sNomeArq, "%s", luaL_check_string(L, 1));

		pIvr->GetCTI()->ClearDigits();
		
		if( pIvr->GetCTI()->PlayFileAsync(luaL_check_string(L, 1)) )
        {
			pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
			// Quando der erro retorna 1
			lua_pushnumber (L, 1);
			return 1;
		}

		//  Pega eventos por pelo menos 1 segundo para dar chance a placa de iniciar o PlayFile,
		// porem devemos monitorar para ver se o usuario desligou nesse momento
		if( pIvr->GetCTI()->WaitForDisconnect(1000) )
        {
			pIvr->Log(LOG_SCRIPT, "Call disconnected");
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber (L, R_CALL_DISCONNECTED);
			return 1;
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRemotePlayFileAsync()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaWaitForPlayStopped(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try
	{
		pIvr->Log( LOG_SCRIPT, "WaitForPlayStopped()" );

		if( pIvr->GetCTI()->WaitForPlayStopped() )
		{
			pIvr->Log(LOG_SCRIPT, "Call disconnected.");
			// Houve desconexao. Retorna p/ o script.
			lua_pushnumber( L, R_CALL_DISCONNECTED );
			return 1;
		}

		// Quando for sucesso retorna 0
		lua_pushnumber( L, 0 );
		return 1;
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaWaitForPlayStopped()" );
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaForcedPlayFile(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs, i, iRet;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "ForcedPlayFile()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		for(i = 1; i <= iNumArgs; i++){

			// monta o caminho para o arquivo de voz a ser reproduzido
            sprintf(sNomeArq, "%s\\%s", pIvr->GetSwitch()->GetPathScripts().c_str(), luaL_check_string(L, i));

			pIvr->Log(LOG_SCRIPT, sNomeArq);
			
			// Nao interrompe o arquivo com senhum digito
			iRet = pIvr->GetCTI()->PlayFile(sNomeArq, "");

			if(iRet == T_DISCONNECT)
            {
				pIvr->Log(LOG_SCRIPT, "Call disconnected");
				pIvr->SetScriptHangupFlag(true);
				lua_pushnumber (L, R_CALL_DISCONNECTED);
				return 1;
			}

			if(iRet == -1)
            {
				pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
				// Quando der erro retorna 1
				lua_pushnumber (L, 1);
				return 1;
			}

			// limpa os digitos da fila para impedir que os digitos nao interfiram com
			// as funcoes posteriores
			pIvr->GetCTI()->ClearDigits();
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaForcedPlayFile()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaRecordFile(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	char sKillString[50];
	int iNumArgs, iRet, iRecordTime, iSilenceTime, iBip = 0;
	int iTimeRecorded = 0;
	bool FlagBip;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 2){
			pIvr->Log(LOG_SCRIPT, "RecordFile()");
			// E necessario pelo menos dois parametros, nome do arquivo
			// e o tempo de gravacao, o resto sera o default
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
        sprintf(sNomeArq, "%s\\%s", pIvr->GetSwitch()->GetPathScripts().c_str(), luaL_check_string(L, 1));

		pIvr->Log(LOG_SCRIPT, "RecordFile( %s )", sNomeArq);

		// tempo de gravacao em segundos
		iRecordTime = luaL_check_int(L, 2);

		if(iNumArgs >= 3)
			iSilenceTime = luaL_check_int(L, 3);
		else
			iSilenceTime = 5;

		if(iNumArgs >= 4)
			iBip = luaL_check_int(L, 4);

		if(iBip)
			FlagBip = true;
		else
			FlagBip = false;

		if(iNumArgs >= 5)
			sprintf(sKillString, "%s", luaL_check_string(L, 5));
		else
			sprintf(sKillString, "@");

		// Inicia a gravacao
		iRet = pIvr->GetCTI()->RecordFile(sNomeArq, iRecordTime, iSilenceTime, FlagBip, sKillString);

		// Obtem o tempo gravado em segundos
		iTimeRecorded = pIvr->GetCTI()->GetTimeRecord();

		if(strlen(sKillString) == 0){
			// Caso foi passado uma string vazia indicando que a grava��o n�o dever� ser
			// interrompida por d�gito nenhum, limpa os digitos da fila para impedir que
			// os digitos nao interfiram com as funcoes posteriores
			pIvr->GetCTI()->ClearDigits();
		}

		if(iRet == T_DISCONNECT){
			pIvr->Log(LOG_SCRIPT, "Call disconnected");
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber (L, R_CALL_DISCONNECTED);
			lua_pushnumber (L, iTimeRecorded);
			return 2;
		}

		if(iRet == -1){
			pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
			lua_pushnumber (L, 1); // Erro
			lua_pushnumber (L, 0); // Sem tempo de gravacao
			return 2;
		}

		lua_pushnumber (L, 0); // OK
		lua_pushnumber (L, iTimeRecorded);
		
		return 2;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRecordFile()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaRemoteRecordFile(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	char sKillString[50];
	int iNumArgs, iRet, iRecordTime, iSilenceTime, iBip = 0;
	int iTimeRecorded = 0;
	bool FlagBip;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 2){
			pIvr->Log(LOG_SCRIPT, "RemoteRecordFile()");
			// E necessario pelo menos dois parametros, nome do arquivo
			// e o tempo de gravacao, o resto sera o default
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
		sprintf(sNomeArq, "%s", luaL_check_string(L, 1));

		pIvr->Log(LOG_SCRIPT, "RemoteRecordFile( %s )", sNomeArq);

		// tempo de gravacao em segundos
		iRecordTime = luaL_check_int(L, 2);

		if(iNumArgs >= 3)
			iSilenceTime = luaL_check_int(L, 3);
		else
			iSilenceTime = 0;

		if(iNumArgs >= 4)
			iBip = luaL_check_int(L, 4);

		if(iBip)
			FlagBip = true;
		else
			FlagBip = false;

		if(iNumArgs >= 5)
			sprintf(sKillString, "%s", luaL_check_string(L, 5));
		else
			sprintf(sKillString, "@");

        for(;;) 
        {

		    // Inicia a gravacao
		    iRet = pIvr->GetCTI()->RecordFile(sNomeArq, iRecordTime, iSilenceTime, FlagBip, sKillString);

		    // Obtem o tempo gravado em segundos
		    iTimeRecorded = pIvr->GetCTI()->GetTimeRecord();

		    if(strlen(sKillString) == 0)
            {
			    // Caso foi passado uma string vazia indicando que a grava��o n�o dever� ser
			    // interrompida por d�gito nenhum, limpa os digitos da fila para impedir que
			    // os digitos nao interfiram com as funcoes posteriores
                pIvr->GetCTI()->ClearDigits();
		  }

		  if(iRet == T_DISCONNECT)
          {
			  pIvr->Log(LOG_SCRIPT, "Call disconnected");
			  pIvr->SetScriptHangupFlag(true);
			  lua_pushnumber (L, R_CALL_DISCONNECTED);
			  lua_pushnumber (L, iTimeRecorded);
			  return 2;
		  }

		  if(iRet == T_SILENCE)
          {
			  pIvr->Log(LOG_SCRIPT, "Silence Timeout");
			  lua_pushnumber (L, 0);  // OK
			  lua_pushnumber (L, iTimeRecorded);
			  lua_pushnumber (L, 1);  // timeout de silencio
			  return 3;
		  }

		  if(iRet == -1)
          {
			  pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
			  lua_pushnumber (L, 1); // Erro
			  lua_pushnumber (L, 0); // Sem tempo de gravacao
			  return 2;
		  }

      // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
      if(iRet == T_SW_CONF_MONITOR_REQUEST)
      {
        string sMessage;
        iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
        if( iRet <= 0 )
        {
          lua_pushnumber (L, iRet);
          lua_pushstring(L, sMessage.c_str());
				  return 2;
        }
        continue;
      }
#endif // _APPLICATION_SOCKET

		  lua_pushnumber (L, 0); // OK
		  lua_pushnumber (L, iTimeRecorded);
		  
 		  return 2;
    }
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRecordFile()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

// int GetDigits(NumDigits, InterDigitTime, StringKill, eco, idioma)

int LuaGetDigits(lua_State *L)
{
	int iNumArgs, iRet, iNumDigits, iInterDigitTime, iPlayFormat;
	char sDigBuffer[64];
	char sDigit[8];
	const char *sTermDigits;
	const char *Eco_Bip;
	bool FlagEco;
	bool FlagBip;
	char sNomeArq[_MAX_PATH+1];
	char path_values[_MAX_PATH+1];
    bool bCofMonitor = false;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "GetDigits()");

		// Pega o numerod e argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 2)
        {
			return 0;
		}

		// Numero de digitos
		iNumDigits			= luaL_check_int(L, 1);
		// Intervalo de tempo entre os digitos
		iInterDigitTime	= luaL_check_int(L, 2);

		memset(sDigBuffer, 0, sizeof(sDigBuffer));

		switch(iNumArgs)
        {

			case 2:
				FlagEco = false;	// Nao toca eco dos digitos
				FlagBip = false;	// Nao toca bip nos digitos colhidos

				for(;;) 
                {

                    // Pega os digitos
				    iRet = pIvr->GetCTI()->GetDigits(iNumDigits, iInterDigitTime, sDigBuffer, NULL);

				    if(iRet == T_DISCONNECT)
                    {
					    pIvr->Log(LOG_SCRIPT, "Call disconnected");
					    pIvr->SetScriptHangupFlag(true);
					    lua_pushnumber (L, R_CALL_DISCONNECTED);
					    return 1;
				    }

				    if(iRet == -1)
                    {
					    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
				    }

                    if(iRet == T_MESSAGEINI)
                    {
	                    lua_pushstring (L, "MSGBOX_BEEP");
	                    return 1;
                    }

                    // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
                    if(iRet == T_SW_CONF_MONITOR_REQUEST)
                    {
                        string sMessage;
                        iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
                        if( iRet <= 0 )
                        {
                            lua_pushnumber (L, iRet);
                            lua_pushstring(L, sMessage.c_str());
				            return 2;
                        }
                        continue;
                    }
#endif // _APPLICATION_SOCKET

				    if(strlen(sDigBuffer) == 0){
					    // Se nao veio nada, retorna nil
					    return 0;
				    }

                    lua_pushstring(L, sDigBuffer);
			        return 1;
                }

				  

			case 3:
				// string de terminacao
				sTermDigits	= luaL_check_string(L, 3);

				FlagEco = false;	// Nao toca eco dos digitos
				FlagBip = false;	// Nao toca bip nos digitos colhidos

				for(;;) 
                {
        
                    // Pega os digitos
				    iRet = pIvr->GetCTI()->GetDigits(iNumDigits, iInterDigitTime, sDigBuffer, (char *)sTermDigits);

				    if(iRet == T_DISCONNECT)
                    {
					    pIvr->Log(LOG_SCRIPT, "Call disconnected");
					    pIvr->SetScriptHangupFlag(true);
					    lua_pushnumber (L, R_CALL_DISCONNECTED);
					    return 1;
				    }

				    if(iRet == -1)
                    {
					    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
				    }

				    if(iRet == T_MESSAGEINI){
						lua_pushstring (L, "MSGBOX_BEEP");
						return 1;
					}

					if(strlen(sDigBuffer) == 0){
					  // Se nao veio nada, retorna nil
					  return 0;
                    }

          // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
                    if(iRet == T_SW_CONF_MONITOR_REQUEST)
                    {
                        string sMessage;
                        iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
                        if( iRet <= 0 )
                        {
                            lua_pushnumber (L, iRet);
                            lua_pushstring(L, sMessage.c_str());
				            return 2;
                        }
                        continue;
                    }
#endif // _APPLICATION_SOCKET

                    lua_pushstring(L, sDigBuffer);
			        return 1;

                } // end for				

            case 4:
				// string de terminacao
				sTermDigits	= luaL_check_string(L, 3);
				// eco ou bip para cada digito colhido
				Eco_Bip			=	luaL_check_string(L, 4);

				if(Eco_Bip[0] == 'E' || Eco_Bip[0] == 'e')
                {
					FlagEco = true;
					FlagBip = false;
				}
				else
                {
					// Vai tocar bip para cada digito colhido
					FlagEco = false;
					FlagBip = true;
				}
			    break;

			case 5:
			default:

				// string de terminacao
				sTermDigits	= luaL_check_string(L, 3);
				// eco ou bip para cada digito colhido
				Eco_Bip			=	luaL_check_string(L, 4);

				if(Eco_Bip[0] == 'E' || Eco_Bip[0] == 'e'){
					FlagEco = true;
					FlagBip = false;
				}
				else if(Eco_Bip[0] == 'B' || Eco_Bip[0] == 'b'){
					// Vai tocar bip para cada digito colhido
					FlagEco = false;
					FlagBip = true;
				}
				else{
					// Nao toca nada
					FlagEco = false;
					FlagBip = false;
				}

			break;
		}

		// Guarda o formato de Play setado pelo script
		iPlayFormat = pIvr->GetCTI()->GetPlayFormat();

		// Seta o formato de Play para WAV
		pIvr->GetCTI()->SetPlayFormat(WAV);

        for(;;) 
        {
            bCofMonitor = false;

            for(int n = 0; n < iNumDigits; n++)
            {

			    sDigit[0] = 0;
			    sDigit[1] = 0;

                iRet = pIvr->GetCTI()->GetDigits(1, iInterDigitTime, sDigit, sTermDigits);
                string sMessage;

			    switch(iRet)
                {
				    case T_MAXDIG:
					    sDigBuffer[n] = sDigit[0];

					    if(FlagEco) // fala o digito recebido
						{

						    if(sDigit[0] < '0' || sDigit[0] > '9')
                            {
							    // Para os digitos (#*ABCD), toca beep
							    // monta o caminho para o arquivo de voz com bip
                                sprintf(sNomeArq, "%s\\beep.wav", pIvr->GetSwitch()->GetPathPrompts() );
							    // Nao interrompe o arquivo com nenhum digito
							    iRet = pIvr->GetCTI()->PlayFile(sNomeArq, "");

							    if(iRet == T_DISCONNECT)
                                {
								    pIvr->Log(LOG_SCRIPT, "Call disconnected");
								    // Restaura o formato de Play do script
								    pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
								    pIvr->SetScriptHangupFlag(true);
								    lua_pushnumber (L, R_CALL_DISCONNECTED);
								    return 1;
							    }

							    if(iRet == -1)
                                {
								    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
							    }
						    } else {
						  
								// monta o caminho para o arquivo de voz a ser reproduzido
								sprintf(sNomeArq, "%s\\values\\f\\f%d.wav", path_values, (int)sDigit[0] - '0');

							    // Nao interrompe o arquivo com nenhum digito
							    iRet = pIvr->GetCTI()->PlayFile(sNomeArq, "");

							    if(iRet == T_DISCONNECT)
                                {
								    pIvr->Log(LOG_SCRIPT, "Call disconnected");
								    // Restaura o formato de Play do script
								    pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
								    pIvr->SetScriptHangupFlag(true);
								    lua_pushnumber (L, R_CALL_DISCONNECTED);
								    return 1;
							    }

							    if(iRet == -1)
                                {
								    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
							    }
						    }
					    }
					    else if(FlagBip)	// toca bip para cada digito recebido
						{
						    // monta o caminho para o arquivo de voz com bip
                            sprintf(sNomeArq, "%s\\beep.wav", pIvr->GetSwitch()->GetPathPrompts());
						    // Nao interrompe o arquivo com nenhum digito
						    iRet = pIvr->GetCTI()->PlayFile(sNomeArq, "");

							if(iRet == T_DISCONNECT)
                            {
							    pIvr->Log(LOG_SCRIPT, "Call disconnected");
								// Restaura o formato de Play do script
								pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
								pIvr->SetScriptHangupFlag(true);
								lua_pushnumber (L, R_CALL_DISCONNECTED);
								return 1;
							}

							if(iRet == -1)
                            {
								pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
							}
					    }
				        break;

				    case T_DISCONNECT:

					    pIvr->Log(LOG_SCRIPT, "Call disconnected");
					    // Restaura o formato de Play do script
					    pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
					    pIvr->SetScriptHangupFlag(true);
					    lua_pushnumber (L, R_CALL_DISCONNECTED);
				        return 1;

				    case T_MESSAGEINI:
						lua_pushstring (L, "MSGBOX_BEEP");
						return 1;

					case T_INTERDIG:
					    if(strlen(sDigBuffer) == 0)
                        {
						    // Restaura o formato de Play do script
						    pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
						    // Se nao veio nada, retorna nil
						    return 0;
					    }
					    lua_pushstring(L, sDigBuffer);
					    return 1;
				        break;

				    case -1:
					    pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
					    // Restaura o formato de Play do script
					    pIvr->GetCTI()->SetPlayFormat(iPlayFormat);
				        return 0;

          // Trata evento do monitor        
#ifdef _APPLICATION_SOCKET
                    case T_SW_CONF_MONITOR_REQUEST:
                        bCofMonitor = true;
                        iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
                        if( iRet <= 0 )
                        {
                            lua_pushnumber (L, iRet);
                            lua_pushstring(L, sMessage.c_str());
				            return 2;
                        }
            
						break;
#endif // _APPLICATION_SOCKET
			    }
            }

            if( !bCofMonitor )
                break;
		}
		
		// Restaura o formato de Play do script
		pIvr->GetCTI()->SetPlayFormat(iPlayFormat);

		// Retorna os digitos
		lua_pushstring(L, sDigBuffer);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetDigits()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSetDigMask(lua_State *L)
{
	int iNumArgs;
	const char *sDisgMask;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	
	try 
    {
		pIvr->Log(LOG_SCRIPT, "SetDigMask()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0)
        {
			return 0;
		}

		sDisgMask	= luaL_check_string(L, 1);		
		pIvr->GetCTI()->SetDigMask(sDisgMask);
		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetDigMask()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaClearDigits(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);
	
	try 
    {
		pIvr->Log(LOG_SCRIPT, "ClearDigits()");

		iRet = pIvr->GetCTI()->ClearDigits();

		lua_pushnumber(L, iRet);

		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaClearDigits()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

/* Fun��o LuaAddTTS - Adiciona um texto que ser� convertido para voz na lista de prompts */
int LuaAddTTS (lua_State *L)
{
	int iNumArgs, status;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "AddTTS()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}
	
		status = pIvr->GetCTI()->AddTTS( luaL_check_string(L, 1) );

		// 0 - SUCESSO, diferente de zero erro
		lua_pushnumber	(L, status);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaAddTTS()");
		return 0;
	}
}

//--------------------------------------------------------------------------------------------------------------------

/* Fun��o LuaPlayTTS - Toca um texto convertendo-o para voz */
int LuaPlayTTS (lua_State *L)
{
	int status;	
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

    /* usada para guardar o retorno 
		0 - OK
		1 - NoK 
	*/ 	
	
	status = pIvr->GetCTI()->PlayTTS(luaL_check_string(L, 1));

	if(status == T_DISCONNECT)
	{
		pIvr->Log(LOG_SCRIPT, "Call disconnected");
		pIvr->SetScriptHangupFlag(true);
		lua_pushnumber (L, R_CALL_DISCONNECTED);
		return 1;
	}
	
	lua_pushnumber	(L, status);
	return 1;

}

//--------------------------------------------------------------------------------------------------------------------

/* Fun��o LuaPlay - Toca a lista de prompts sincronamente */
int LuaPlay(lua_State *L)
{
	int status;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
	{
		pIvr->Log(LOG_SCRIPT, "Play()");

		status = pIvr->GetCTI()->Play();
	
		// 0 - SUCESSO, diferente de zero erro
		lua_pushnumber	(L, status);
		return 1;
	}
	catch(...)
	{		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPlay()");
		return 0;
	}
}

//--------------------------------------------------------------------------------------------------------------------

/* Fun��o LuaClearPrompts - Limpa a lista de prompts */
int LuaClearPrompts (lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
	{
		pIvr->Log(LOG_SCRIPT, "ClearPrompts()");

		// 0 - SUCESSO, diferente de zero erro
		lua_pushnumber	(L, pIvr->GetCTI()->ClearPrompts());
		return 1;
	}
	catch(...)
	{		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaClearPrompts ()");
		return 0;
	}
}

//--------------------------------------------------------------------------------------------------------------------

int LuaRemotePlayFile(lua_State *L)
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs, i, iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {	
		pIvr->Log(LOG_SCRIPT, "RemotePlayFile()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0)
        {
			return 0;
		}

		for(i = 1; i <= iNumArgs; i++)
        {
			// monta o caminho para o arquivo de voz a ser reproduzido
			sprintf(sNomeArq, "%s", luaL_check_string(L, i));
			
			pIvr->Log(LOG_SCRIPT, sNomeArq);

			iRet = pIvr->GetCTI()->PlayFile(sNomeArq);

			if(iRet == T_DISCONNECT)
            {
				pIvr->Log(LOG_SCRIPT, "Call disconnected");
				pIvr->SetScriptHangupFlag(true);
				lua_pushnumber (L, R_CALL_DISCONNECTED);
				return 1;
			}

			if(iRet == -1)
            {
				pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
				// Quando der erro retorna 1
				lua_pushnumber (L, 1);
				return 1;
			}
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRemotePlayFile()");
		return 0;
	}
}

//--------------------------------------------------------------------------------------------------------------------

int LuaPlayFileUntil( lua_State *L )
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs, iRet;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "PlayFileUntil()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if( iNumArgs != 2 )
		{
			pIvr->Log(LOG_ERROR, "PlayFileUntil(): invalid number of arguments.");
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
		sprintf(sNomeArq, "%s", luaL_check_string(L, 1));
		
		pIvr->Log(LOG_SCRIPT, sNomeArq);

		iRet = pIvr->GetCTI()->PlayFile(sNomeArq, "", luaL_check_long(L, 2));

		if( iRet == T_DISCONNECT )
		{
			pIvr->Log( LOG_SCRIPT, "Call disconnected" );
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber( L, R_CALL_DISCONNECTED );
			return 1;
		}

		if( iRet == -1 )
		{
			pIvr->Log( LOG_SCRIPT, pIvr->GetCTI()->GetLastError() );
			// Quando der erro retorna 1
			lua_pushnumber( L, 1 );
			return 1;
		}

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPlayFileUntil()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaStopRecordFile(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {		
        pIvr->Log(LOG_SCRIPT, "StopRecordFile()");
		iRet = pIvr->GetCTI()->StopRecordFile();
		if(iRet == T_DISCONNECT)
			lua_pushnumber( L, R_CALL_DISCONNECTED );
		else
            lua_pushnumber (L, iRet);
        
        return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: StopRecordFile()");
		return 0;
	}
}

//--------------------------------------------------------------------------------------------------------------------

int LuaPlayFileUntilEvent( lua_State *L )
{
	char sNomeArq[_MAX_PATH+1];
	int iNumArgs, iRet;

    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "PlayFileUntilEvent()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// monta o caminho para o arquivo de voz a ser reproduzido
		sprintf(sNomeArq, "%s", luaL_check_string(L, 1));
		
		pIvr->Log(LOG_SCRIPT, sNomeArq);

		iRet = pIvr->GetCTI()->PlayFileEvent(sNomeArq);

		if( iRet == T_DISCONNECT )
		{
			pIvr->Log( LOG_SCRIPT, "Call disconnected" );
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber( L, R_CALL_DISCONNECTED );
			return 1;
		}

		if( iRet == -1 )
		{
			pIvr->Log( LOG_SCRIPT, pIvr->GetCTI()->GetLastError() );
			// Quando der erro retorna 1
			lua_pushnumber( L, 1 );
			return 1;
		}

      // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
        if(iRet == T_SW_CONF_MONITOR_REQUEST)
        {
            string sMessage;
            iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
            lua_pushnumber (L, iRet);
            lua_pushstring(L, sMessage.c_str());
			return 2;
        }
#endif // _APPLICATION_SOCKET

		// Quando for sucesso retorna 0
		lua_pushnumber (L, 0);
		return 1;
	}
	catch(...)
    {	
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPlayFileUntilEvent()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

// RecordFileWithMusic( record, play, timeout, [silence_timeout], [terminator] )

int LuaRecordFileWithMusic(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrAudio(L);

	try
	{
		int rc;
		int iNumArgs;
		

		int iRecordedTime = 0;
		const char *record;
		const char *play;
		int timeout;
		int silence_timeout;
		const char *terminator;

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		switch( iNumArgs )
		{
			case 3:
				record						= luaL_check_string( L, 1 );
				play							= luaL_check_string( L, 2 );
				timeout						= luaL_check_int( L, 3 );
				silence_timeout		= 0;
				terminator				= "@";
				break;

			case 4:
				record						= luaL_check_string( L, 1 );
				play							= luaL_check_string( L, 2 );
				timeout						= luaL_check_int( L, 3 );
				silence_timeout		= luaL_check_int( L, 4 );
				terminator				= "@";
				break;

			case 5:
				record						= luaL_check_string( L, 1 );
				play							= luaL_check_string( L, 2 );
				timeout						= luaL_check_int( L, 3 );
				silence_timeout		= luaL_check_int( L, 4 );
				terminator				= luaL_check_string( L, 5 );
				break;

			default:
				pIvr->Log( LOG_SCRIPT, "Invalid number of arguments. Expected: RecordFileWithMusic( record, play, timeout, silence_timeout, terminator )." );
				return 0;
		}

		pIvr->Log( LOG_SCRIPT, "RecordFileWithMusic( \"%s\", \"%s\", %d, %d, \"%s\" )", record, play, timeout, silence_timeout, terminator );

		rc = pIvr->RecordFileWithMusic( record, play, timeout, silence_timeout, terminator );

		// Obtem o tempo gravado em segundos
		iRecordedTime = pIvr->GetCTI()->GetTimeRecord();

		// Caso foi passado uma string vazia indicando que a grava��o n�o dever� ser
		// interrompida por d�gito nenhum, limpa os digitos da fila para impedir que
		// os digitos nao interfiram com as funcoes posteriores
		if( terminator[0] == '\0' )
			pIvr->GetCTI()->ClearDigits();

		if( rc == T_DISCONNECT )
		{
			pIvr->Log( LOG_SCRIPT, "Call disconnected" );
			pIvr->SetScriptHangupFlag(true);
			lua_pushnumber( L, R_CALL_DISCONNECTED );
			lua_pushnumber( L, iRecordedTime );
			return 2;
		}

		if( rc == T_SILENCE )
		{
			pIvr->Log( LOG_SCRIPT, "Silence Timeout" );
			lua_pushnumber( L, 0 );  // OK
			lua_pushnumber( L, iRecordedTime );
			lua_pushnumber( L, 1 );  // timeout de silencio
			return 3;
		}

		if( rc < 0 )
		{
			lua_pushnumber( L, 1 ); // Erro
			lua_pushnumber( L, 0 ); // Sem tempo de gravacao
			return 2;
		}

		if( rc == 1 )
		{
			lua_pushnumber( L, 2 ); // Nao ha recursos disponiveis
			lua_pushnumber( L, 0 ); // Sem tempo de gravacao
			return 2;
		}

    // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
        if( rc == T_SW_CONF_MONITOR_REQUEST )
        {
            string sMessage;
            rc = Lua_UserData->IVR->EventMonitor( rc, sMessage );
            if( rc <= 0 )
            {
                lua_pushnumber( L, iRecordedTime );
                lua_pushstring( L, sMessage.c_str() );
				return 2;
            }
        }
#endif // _APPLICATION_SOCKET

		lua_pushnumber( L, 0 ); // OK
		lua_pushnumber( L, iRecordedTime );
 		return 2;
		
	}
	catch(...)
	{		
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaRecordFileWithMusic()" );
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

#include "TelnetSession.h"
#include "ISwitch.h"



TelnetSession::TelnetSession(SOCKET ClientSocket, IThrTelnetListen * ts , boost::shared_ptr<Logger> pLog4cpp, const std::string & sTelnetPassword) : 
    m_socket(ClientSocket),  m_pLog4cpp( pLog4cpp ) , m_telnetServer(ts) ,
    m_bStop(false) , m_sTelnetPassword(sTelnetPassword), m_iNumLoginTries(0) , m_bLoggedIn(false)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    m_historyCursor = m_history.end();

    iState = 0;
    //m_msInterval = 3000;
    m_msInterval = 5*60*1000;
}

void TelnetSession::Start()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    m_before = boost::posix_time::microsec_clock::universal_time();	

    m_SessionThreadPtr.reset ( new boost::thread ( boost::bind (&TelnetSession::Loop, this)));
    m_SessionThreadPtr->detach();
}

void TelnetSession::Stop()
{
    //m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    closesocket(m_socket);
    m_bStop = true;
    m_SessionThreadPtr->join();	
}

void TelnetSession::Loop()
{  
    try
    {
        while( !m_bStop )
	    {
            m_after = boost::posix_time::microsec_clock::universal_time();
		    boost::posix_time::time_duration duration = m_after - m_before;

            if ( m_msInterval > (std::size_t)0 )
			{
			    if ( (double)duration.total_milliseconds() > (double)m_msInterval )
                {
                    m_bStop = true;
                    closesocket(m_socket);
                    m_pLog4cpp->debug("[%p][%s] TIMEOUT.", this, __FUNCTION__);
                    m_telnetServer->AddToGarbageCollector((void *)(this));
                    return;
                }
            }
            update();
            boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }

        
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error.", this, __FUNCTION__);
    }
}

void TelnetSession::initialise()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
        Start();

        // get details of connection
        SOCKADDR_IN client_info = { 0 };
        int addrsize = sizeof(client_info);
        getpeername(m_socket, (struct sockaddr*)&client_info, &addrsize);

        m_pLog4cpp->debug("[%p][%s] Host : ip %s , port %d \n", this, __FUNCTION__ ,  inet_ntoa(client_info.sin_addr) , ntohs(client_info.sin_port)); 
    

        // Set the connection to be non-blocking
        u_long iMode = 1;
        ioctlsocket(m_socket, FIONBIO, &iMode);

        // Set NVT mode to say that I will echo back characters.
        u_long iSendResult;
        unsigned char willEcho[3] = { 0xff, 0xfb, 0x01 };
        iSendResult = send(m_socket, (char *)willEcho, 3, 0);    

        // Set NVT requesting that the remote system DO echo back characters
        unsigned char doEcho[3] = { 0xff, 0xfe, 0x00 };
        iSendResult = send(m_socket, (char *)doEcho, 3, 0);

        // Set NVT mode to say that I will supress go-ahead. Stops remote clients from doing local linemode.
        unsigned char willSGA[3] = { 0xff, 0xfb, 0x03 };
        iSendResult = send(m_socket, (char *)willSGA, 3, 0);

        m_pLog4cpp->debug("[%p][%s] Going to call Server connected Callback.", this, __FUNCTION__);

        //if (m_telnetServer->OnConnectedCallback())
        //    (m_telnetServer->OnConnectedCallback())(this);
    
        sendLine("*********************************************\r\n");
        sendLine("                                            *\r\n");
        sendLine("*  SUPPORTCOMM VOICEGATEWAY TELNET SERVER   *\r\n");
        sendLine("                                            *\r\n");
        sendLine("*********************************************\r\n");
        sendLine("> ");
    
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at initialise().", this, __FUNCTION__);
        m_bStop = true;
    }
    
}

void TelnetSession::sendPromptAndBuffer()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
        // Output the prompt
        u_long iSendResult;
        iSendResult = send(m_socket, m_telnetServer->promptString().c_str(), (u_long)m_telnetServer->promptString().length(), 0);

        if (m_buffer.length() > 0)
        {
            // resend the buffer
            iSendResult = send(m_socket, m_buffer.c_str(), (u_long)m_buffer.length(), 0);
        }
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at sendPromptAndBuffer().", this, __FUNCTION__);
        m_bStop = true;
    }
}

void TelnetSession::eraseLine()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
    u_long iSendResult;
    // send an erase line       
    iSendResult = send(m_socket, ANSI_ERASE_LINE.c_str(), (u_long)ANSI_ERASE_LINE.length(), 0);

    // Move the cursor to the beginning of the line
    std::string moveBack = "\x1b[80D";
    iSendResult = send(m_socket, moveBack.c_str(), (u_long)moveBack.length(), 0);
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at eraseLine().", this, __FUNCTION__);
        m_bStop = true;
    }
}

void TelnetSession::sendLine(std::string data)
{
    //m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
        u_long iSendResult;
        // If is something is on the prompt, wipe it off

        if (m_telnetServer->interactivePrompt() || m_buffer.length() > 0)    
        if (m_buffer.length() > 0)    
        {
            eraseLine();
        }
    
        iSendResult = send(m_socket, data.c_str(), (u_long)data.length(), 0);

        if (m_telnetServer->interactivePrompt())
            sendPromptAndBuffer();
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at sendLine().", this, __FUNCTION__);
        m_bStop = true;
    }
}

void TelnetSession::echoBack(const char * buffer, u_long length)
{
    std::stringstream ss;

    for ( int i = 0 ; i < length ; i++ )
        ss << buffer[i];

    // If you are an NVT command (i.e. first it of data is 255) then ignore the echo back
    unsigned char firstItem = * buffer;
    if (firstItem == 0xff)
        return;

    u_long iSendResult;
    iSendResult = send(m_socket, buffer, length, 0);

    if (iSendResult == SOCKET_ERROR && iSendResult != WSAEWOULDBLOCK) 
    {
        m_pLog4cpp->debug("[%p][%s] Send failed with Winsock error: %d. \r\n", this, __FUNCTION__ , WSAGetLastError() );        
        m_pLog4cpp->debug("[%p][%s] Closing session and socket. \r\n", this, __FUNCTION__);
        
        closesocket(m_socket);
        m_bStop = true;
        return;
    }
}

void TelnetSession::stripNVT(std::string &buffer)
{    

    size_t found;
    do
    {
        unsigned char findChar = 0xff;
        found = buffer.find_first_of((char)findChar);
        if (found != std::string::npos && (found + 2) <= buffer.length() - 1)
        {
            buffer.erase(found, 3);
        }
    } while (found != std::string::npos);
}

void TelnetSession::stripEscapeCharacters(std::string &buffer)
{
    size_t found;

    std::array<std::string, 4> cursors = { ANSI_ARROW_UP, ANSI_ARROW_DOWN, ANSI_ARROW_RIGHT, ANSI_ARROW_LEFT };

    for (auto c : cursors)
    {
        do
        {
            found = buffer.find(c);
            if (found != std::string::npos)
            {
                buffer.erase(found, c.length());
            }
        } while (found != std::string::npos);
    }
}

bool TelnetSession::processBackspace(std::string &buffer)
{
    bool foundBackspaces = false;
    size_t found;
    do
    {
        // Need to handle both \x7f and \b backspaces
        unsigned char findChar = '\x7f';
        found = buffer.find_first_of((char)findChar);
        if (found == std::string::npos)
        {
            findChar = '\b';
            found = buffer.find_first_of((char)findChar);
        }

        if (found != std::string::npos)
        {
            if (buffer.length() > 1)
                buffer.erase(found - 1, 2);
            else
                buffer = "";
            foundBackspaces = true;
        }
    } while (found != std::string::npos);
    return foundBackspaces;
}

void TelnetSession::addToHistory(std::string line)
{
    // Add it to the history
    if (line != (m_history.size() > 0 ? m_history.back() : "") && line != "")
    {
        m_history.push_back(line);
        if (m_history.size() > 50)
            m_history.pop_front();
    }
    m_historyCursor = m_history.end();
}

bool TelnetSession::processCommandHistory(std::string &buffer)
{
    try
    {

        // Handle up and down arrow actions
        if (m_telnetServer->interactivePrompt())
        {
            if (buffer.find(ANSI_ARROW_UP) != std::string::npos && m_history.size() > 0)
            {
                if (m_historyCursor != m_history.begin())
                {
                    m_historyCursor--;
                }
                buffer = *m_historyCursor;

                // Issue a cursor command to counter it
                u_long iSendResult;
                iSendResult = send(m_socket, ANSI_ARROW_DOWN.c_str(), (u_long)ANSI_ARROW_DOWN.length(), 0);
                return true;
            }
            if (buffer.find(ANSI_ARROW_DOWN) != std::string::npos && m_history.size() > 0)
            {
                if (next(m_historyCursor) != m_history.end())
                {
                    m_historyCursor++;
                }
                buffer = *m_historyCursor;

                // Issue a cursor command to counter it
                u_long iSendResult;
                iSendResult = send(m_socket, ANSI_ARROW_UP.c_str(), (u_long)ANSI_ARROW_UP.length(), 0);
                return true;
            }
            if (buffer.find(ANSI_ARROW_LEFT) != std::string::npos || buffer.find(ANSI_ARROW_RIGHT) != std::string::npos)
            {
                // Ignore left and right and just reprint buffer
                return true;
            }
        }
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at processCommandHistory().", this, __FUNCTION__);
        m_bStop = true;
    }

    return false;
}

std::vector<std::string> TelnetSession::getCompleteLines(std::string &buffer)
{
    // Now find all new lines (<CR><LF>) and place in a vector and delete from buffer
    
    char CRLF[2] = { 0x0D, 0x0A };
    std::vector<std::string> lines;
    size_t found;
    do
    {
        found = buffer.find("\r\n");
        if (found != std::string::npos)
        {            
            lines.push_back(buffer.substr(0, found + 2));
            buffer.erase(0, found + 2);
            
            vecRecv.clear();
            iState = 0;

            //m_pLog4cpp->debug("[%p][%s] lines : %s", this, __FUNCTION__, buffer.substr(0, found));
        }
    } while (found != std::string::npos);

    
    return lines;
}

void TelnetSession::update()
{
    int  readBytes;
    char recvbuf[DEFAULT_BUFLEN];
    u_long  recvbuflen = DEFAULT_BUFLEN;
    std::stringstream ss;    
    std::string svecRecv;

    try
    {

        memset(recvbuf,0x00,sizeof(recvbuf));
        if ( m_socket != INVALID_SOCKET )
            readBytes = recv(m_socket, recvbuf, recvbuflen, 0);
        else
        {
            m_pLog4cpp->debug("[%p][%s] Receive won't receive from an INVALID SOCKET.", this, __FUNCTION__ );
            m_bStop = true;
            return;
        }

        // Check for errors from the read
        int error = WSAGetLastError();
        if (error != WSAEWOULDBLOCK && error != 0)
        {
            m_pLog4cpp->debug("[%p][%s] Receive failed with Winsock error code: %d.", this, __FUNCTION__, error);
            m_bStop = true;
            return;
        }

        if (readBytes > 0) 
        {   

            if ( iState == 1 )            
            {
                m_pLog4cpp->debug("[%p][%s][state 1] %c", this, __FUNCTION__, recvbuf[0] );

                echoBack("*",1);

                for ( int i = 0 ; i < readBytes ; i++ )            
                    vecRecv.push_back(recvbuf[0]);
            
            
                iState = 2;
                ss.str(std::string());
            }
            else if ( iState == 2 )
            {
                m_pLog4cpp->debug("[%p][%s][state 2] %c", this, __FUNCTION__, recvbuf[0] );

                for ( int i = 0 ; i < readBytes ; i++ )            
                    vecRecv.push_back(recvbuf[0]);                       
            
                ss << recvbuf;

                if ( ss.str().find("\r\n")  != std::string::npos )
                {                

                    std::copy(vecRecv.begin() , vecRecv.end() , std::back_inserter(m_sRecvPassword) );

                    //m_sRecvPassword.assign( vecPwd.begin() , vecPwd.end() );

                    echoBack(recvbuf, readBytes);

                    m_pLog4cpp->debug("[%p][%s] PASSWORD AT REGISTRY : %s", this, __FUNCTION__, m_sTelnetPassword.c_str() );
                    m_pLog4cpp->debug("[%p][%s] RECEIVED PASSWORD : %s", this, __FUNCTION__, m_sRecvPassword.c_str() );

                    if ( m_sRecvPassword.find(m_sTelnetPassword) == std::string::npos )                
                    {   
                        sendLine("\r\nInvalid PASSWORD.\r\n\r\n");
                        m_sRecvPassword.clear();
                        m_iNumLoginTries++;
                    } else {
                        sendLine("\r\nLogin OK\r\n\r\n");
                        m_pLog4cpp->debug("[%p][%s] *********** LOGIN OK ***********", this, __FUNCTION__ );
                        m_bLoggedIn = true;
                        SendHelpMenu();
                        iState = 3;                    
                    }
                }
                else
                {
                    echoBack("*",1);
                }
            
            }
            else
            {
                // Echo it back to the sender
                echoBack(recvbuf, readBytes);
            }

            CheckPassword(recvbuf, readBytes);

        

            // we've got to be careful here. Telnet client might send null characters for New Lines mid-data block. We need to swap these out. recv is not null terminated, so its cool
            for (int i = 0; i < readBytes; i++)
            {
                if (recvbuf[i] == 0x00)
                    recvbuf[i] = 0x0A;      // New Line
            }

            // Add it to the received buffer
            m_buffer.append(recvbuf, readBytes);

            stripNVT(m_buffer);                         // Remove telnet negotiation sequences

            bool requirePromptReprint = false;

            if (m_telnetServer->interactivePrompt())
            {
                if (processCommandHistory(m_buffer))   // Read up and down arrow keys and scroll through history
                    requirePromptReprint = true;
                stripEscapeCharacters(m_buffer);

                if (processBackspace(m_buffer))
                    requirePromptReprint = true;
            }

            auto lines = getCompleteLines(m_buffer);
            for (auto line : lines)
            {
                if (m_telnetServer->OnNewLineCallBack())
                    m_telnetServer->OnNewLineCallBack()( this , line);
                    //m_telnetServer->newLineCallBack()(shared_from_this(), line);

                if ( boost::iequals(line.c_str(),"quit\r\n") )            
                    closeClient();
            
                if ( m_bLoggedIn )
                    ParseReceivedBuffer(line);

                addToHistory(line);
            }

            if (m_telnetServer->interactivePrompt() && requirePromptReprint)
            {
                eraseLine();
                sendPromptAndBuffer();
            }

            if ( m_iNumLoginTries == 3 )
            {
                sendLine("Maximum tries exceeded.\r\n");
                closeClient();
            }
        
        }
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at update().", this, __FUNCTION__);
        m_bStop = true;
    }
}

void TelnetSession::ParseReceivedBuffer(const std::string & inputCommand)
{  

    //m_pLog4cpp->debug("[%p][%s] buffer : %s", this, __FUNCTION__ , m_buffer.c_str());

    if ( boost::iequals(inputCommand.c_str(),"quit\r\n") )
    {
        closeClient();
    } 
    else if ( boost::iequals(inputCommand.c_str(),"help\r\n") )
    {
        SendHelpMenu();
    }
    else if ( inputCommand.find("stoc") != std::string::npos )    
    {
        ParseStoc(inputCommand);
    }
    else if ( inputCommand.find("time") != std::string::npos )        
    {
        ParseTime(inputCommand);
    }
    else if ( inputCommand.find("date") != std::string::npos )
    {
        ParseDate(inputCommand);
    }
    else if ( inputCommand.find("stat") != std::string::npos )
    {
        ParseStat(inputCommand);
    }
    else if ( inputCommand.find("chnc") != std::string::npos )
    {
        ParseChnc(inputCommand);
    }    
    else if ( inputCommand.find("dial") != std::string::npos )
    {
        ParseDial(inputCommand);
    }
    else if ( inputCommand.find("disc") != std::string::npos )
    {
        ParseDisc(inputCommand);
    }
    else if ( inputCommand.find("blkc") != std::string::npos )
    {
        ParseBlkc(inputCommand);
    }
    else if ( inputCommand.find("unbc") != std::string::npos )
    {
        ParseUnbc(inputCommand);
    } else {
        sendLine( "INVALID COMMAND.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
    }
}

void TelnetSession::SendHelpMenu()
{
	std::stringstream ss;
    
    ss << "\r\n";
    ss << "HELP - Help" "\r\n"; 
    ss << "QUIT - Exit connection" "\r\n"; 
    ss << "TIME - TIME SET HH:MM:SS, TIME GET" "\r\n"; 
    ss << "DATE - DATE SET YYYY-MM-DD, DATE GET" "\r\n"; 
    ss << "DIAL - Dial: DIAL <Channel> <Destination> <Origination>" "\r\n";
    ss << "DISC - Disconnect Channel: DISC <Initial Channel> <Final Channel>" "\r\n"; 
    ss << "BLKC - Block Channel: BLKC <Initial Channel> <Final Channel>" "\r\n"; 
    ss << "UNBC - Unblock Channel: UNBC <Initial Channel> <Final Channel>" "\r\n"; 
    ss << "STOC - Channel Status: STOC <Initial Channel> <Final Channel>" "\r\n";
    ss << "STAT - Statistics Informations: STAT" "\r\n";
    ss << "CHNC - Channel count" "\r\n";
    ss << ">";

    m_pLog4cpp->debug("%s",ss.str().c_str());

    sendLine( ss.str() );
	
}

void TelnetSession::CheckPassword(const char * szRecv, int iLen)
{
    std::string svecRecv;    
        
    if ( iState == 0 )
    {
        for ( int i = 0 ; i < iLen ; i++ )
        {
            vecRecv.push_back(szRecv[i]);
        }

        std::copy(vecRecv.begin() , vecRecv.end() , std::back_inserter(svecRecv) );

        if( (svecRecv.find("pass ") != std::string::npos) && (iState == 0) )
        {
            iState = 1;
        
        }
    }
}

void TelnetSession::closeClient()
{
    u_long iResult;

    try
    {

        m_bStop = true;

        // attempt to cleanly shutdown the connection since we're done
        iResult = shutdown(m_socket, SD_SEND);
        if (iResult == SOCKET_ERROR) {
            m_pLog4cpp->debug("[%p][%s]shutdown failed with error: %d\n", this, __FUNCTION__ , WSAGetLastError());
            return;
        }

        // cleanup
        closesocket(m_socket);
    }
    catch(...)
    {
        m_pLog4cpp->debug("[%p][%s] Error at closeClient().", this, __FUNCTION__);
        m_bStop = true;
    }
}

void TelnetSession::ParseStocOK(const std::string & inputCommand)
{
    
    int iChannel = 0, iChannelLow = 0, iChannelHigh = 0;
	int rc, i;
	std::string bsParam;
	std::string bsChannelState;
	char sLastActivity[256];
	char sDnis[256];
	char sAni[256];
	time_t tLastActivity;
	struct tm *tmLastActivity;
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;
    int iChannelCount = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());

    
	iChannelLow		= 0;
	iChannelHigh	= 0;

	sscanf( inputCommand.c_str(), "%s %d %d", sCommand, &iChannelLow, &iChannelHigh );


    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();
        
	if( ( iChannelLow < 1 ) || ( iChannelHigh > iChannelCount ) )
	{		
        sendLine( "ERRO Invalid circuit range.\r\n" );
		return;
	}
	if( iChannelHigh < iChannelLow )
	{
		iChannelHigh = iChannelLow;
	}

	ss << "\r\n";
    ss << "|CH  |STATE        |LAST ACTIVITY       |CALLS IN |CALLS OUT |ERRORS |\r\n";
    sendLine(ss.str().c_str());

	for( i = iChannelLow; i <= iChannelHigh; i++ )
	{
		if( !m_telnetServer->ValidCircuit(i) )
		{
				bsChannelState = "DISABLED";
		}
		else
		{
			rc = m_telnetServer->StateCircuit( i );
			switch( rc )
			{
				case CHANNEL_DISABLED:
					bsChannelState = "DISABLED";
					break;
				case CHANNEL_IDLE:
					bsChannelState = "IDLE";
					break;
				case CHANNEL_BLOCKED:
					bsChannelState = "BLOCKED";
					break;
				case CHANNEL_OFFERED:
					bsChannelState = "OFFERED";
					break;
				case CHANNEL_ACCEPTED:
					bsChannelState = "ACCEPTED";
					break;
				case CHANNEL_ANSWERED:
					bsChannelState = "ANSWERED";
					break;
				case CHANNEL_ALERTING:
					bsChannelState = "ALERTING";
					break;
				case CHANNEL_CONNECTED:
					bsChannelState = "CONNECTED";
					break;
				case CHANNEL_DISCONNECTED:
					bsChannelState = "DISCONNECTED";
					break;
				case CHANNEL_CALLSTATUS:
					bsChannelState = "CALLSTATUS";
					break;
			}
		}
		tLastActivity = ChannelStat::Instance().GetLastActivityTime(i);
		if( !tLastActivity )
		{
			sprintf( sLastActivity, "0000-00-00 00:00:00" );
		}
		else
		{
			tmLastActivity = localtime( &tLastActivity );
			sprintf( sLastActivity, "%04d-%02d-%02d %02d:%02d:%02d", tmLastActivity->tm_year + 1900, tmLastActivity->tm_mon + 1, tmLastActivity->tm_mday, tmLastActivity->tm_hour, tmLastActivity->tm_min, tmLastActivity->tm_sec );
		}

        ss.str(std::string());
        ss << boost::format( "|%-4lu|%-13s|%-20s|%-9lu|%-10lu|%-7lu|\r\n") % i % bsChannelState.c_str() % sLastActivity %
                                ChannelStat::Instance().GetCallsAnsweredCount(i) % ChannelStat::Instance().GetCallsConnectedCount(i) %
                                ChannelStat::Instance().GetScriptErrorCount(i);
		sendLine(ss.str().c_str());
        
		// Zera os contadores
		ChannelStat::Instance().ResetCallsAnsweredCount(i);
		ChannelStat::Instance().ResetCallsConnectedCount(i);
		ChannelStat::Instance().ResetScriptErrorCount(i);
	}

    sendLine( "\r\n.\r\n" );
    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");

	
    
}

void TelnetSession::ParseStoc(const std::string & inputCommand)
{
    
    int iChannel = 0, iChannelLow = 0, iChannelHigh = 0;
	int rc, i;
	std::string bsParam;
	std::string bsChannelState;
	char sLastActivity[256];
	char sDnis[256];
	char sAni[256];
	time_t tLastActivity;
	struct tm *tmLastActivity;
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;
    int iChannelCount = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());

    
	iChannelLow		= 0;
	iChannelHigh	= 0;
    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();

    std::vector<std::string> vec = split_string(inputCommand);
    if ( vec.size() != 3 )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    try
    {
        iChannelLow = std::stoi(vec[1]);
    }
    catch( const std::invalid_argument & ia ) {
        iChannelLow = -1;
    }

    try
    {
        iChannelHigh = std::stoi(vec[2]);    
    }
    catch( const std::invalid_argument & ia ) {
        iChannelHigh = -1;
    }

    if ( iChannelHigh < iChannelLow )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelLow <= 0 || iChannelLow > iChannelCount )
    {
        sendLine( "Start channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelHigh < 0 || iChannelHigh > iChannelCount )
    {
        sendLine( "End channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

	ss << "\r\n";
    ss << "|CH  |STATE        |LAST ACTIVITY       |CALLS IN |CALLS OUT |ERRORS |\r\n";
    sendLine(ss.str().c_str());

	for( i = iChannelLow; i <= iChannelHigh; i++ )
	{
		if( !m_telnetServer->ValidCircuit(i) )
		{
				bsChannelState = "DISABLED";
		}
		else
		{
			rc = m_telnetServer->StateCircuit( i );
			switch( rc )
			{
				case CHANNEL_DISABLED:
					bsChannelState = "DISABLED";
					break;
				case CHANNEL_IDLE:
					bsChannelState = "IDLE";
					break;
				case CHANNEL_BLOCKED:
					bsChannelState = "BLOCKED";
					break;
				case CHANNEL_OFFERED:
					bsChannelState = "OFFERED";
					break;
				case CHANNEL_ACCEPTED:
					bsChannelState = "ACCEPTED";
					break;
				case CHANNEL_ANSWERED:
					bsChannelState = "ANSWERED";
					break;
				case CHANNEL_ALERTING:
					bsChannelState = "ALERTING";
					break;
				case CHANNEL_CONNECTED:
					bsChannelState = "CONNECTED";
					break;
				case CHANNEL_DISCONNECTED:
					bsChannelState = "DISCONNECTED";
					break;
				case CHANNEL_CALLSTATUS:
					bsChannelState = "CALLSTATUS";
					break;
			}
		}
		tLastActivity = ChannelStat::Instance().GetLastActivityTime(i);
		if( !tLastActivity )
		{
			sprintf( sLastActivity, "0000-00-00 00:00:00" );
		}
		else
		{
			tmLastActivity = localtime( &tLastActivity );
			sprintf( sLastActivity, "%04d-%02d-%02d %02d:%02d:%02d", tmLastActivity->tm_year + 1900, tmLastActivity->tm_mon + 1, tmLastActivity->tm_mday, tmLastActivity->tm_hour, tmLastActivity->tm_min, tmLastActivity->tm_sec );
		}

        ss.str(std::string());
        ss << boost::format( "|%-4lu|%-13s|%-20s|%-9lu|%-10lu|%-7lu|\r\n") % i % bsChannelState.c_str() % sLastActivity %
                                ChannelStat::Instance().GetCallsAnsweredCount(i) % ChannelStat::Instance().GetCallsConnectedCount(i) %
                                ChannelStat::Instance().GetScriptErrorCount(i);
		sendLine(ss.str().c_str());
        
		// Zera os contadores
		ChannelStat::Instance().ResetCallsAnsweredCount(i);
		ChannelStat::Instance().ResetCallsConnectedCount(i);
		ChannelStat::Instance().ResetScriptErrorCount(i);
	}

    sendLine( "\r\n.\r\n" );
    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");

	
    
}

void TelnetSession::ParseTime(const std::string & inputCommand)
{
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__ );
        
    sscanf( inputCommand.c_str(), "%s %s", sCommand, sSubCommand );
    if( !stricmp( sSubCommand, "SET" ) )
    {
	    unsigned short int uHour = 0, uMinute = 0, uSecond = 0;
	    int rc;
	    sscanf( inputCommand.c_str(), "%s %s %02d:%02d:%02d", sCommand, sSubCommand, &uHour, &uMinute, &uSecond );
	    SYSTEMTIME SystemTime;
	    GetLocalTime( &SystemTime );
	    SystemTime.wHour = uHour;
	    SystemTime.wMinute = uMinute;
	    SystemTime.wSecond = uSecond;
	    rc = SetLocalTime( &SystemTime );
	    if( !rc )
	    {
            ss << boost::format("ERRO Invalid time. The time format is hh:mm:ss. System error message: %s.\r\n") % strerror( ::GetLastError() );
		    sendLine(ss.str().c_str());
		    return;
	    }
    }
    else
    {
	    SYSTEMTIME SystemTime;
	    GetLocalTime( &SystemTime );
        ss << boost::format("Local system time: %02d:%02d:%02d\r\n") % SystemTime.wHour % SystemTime.wMinute % SystemTime.wSecond;
	    sendLine(ss.str().c_str());
    }

    sendLine( "OK\r\n" );
    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

void TelnetSession::ParseDate(const std::string & inputCommand)
{
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__ );
    
    sscanf( inputCommand.c_str() , "%s %s", sCommand, sSubCommand );
    if( !stricmp( sSubCommand, "SET" ) )
    {
        unsigned short int uDay = 0, uMonth = 0, uYear = 0;
        int rc;

        sscanf( inputCommand.c_str() , "%s %s %04d-%02d-%02d", sCommand, sSubCommand, &uYear, &uMonth, &uDay );
        SYSTEMTIME SystemTime;
        GetLocalTime( &SystemTime );
        SystemTime.wYear = uYear;
        SystemTime.wMonth = uMonth;
        SystemTime.wDay = uDay;
        rc = SetLocalTime( &SystemTime );
        if( !rc )
        {
            ss << boost::format("ERRO Invalid date. The date format is yyyy-mm-dd. System error message: %s.\r\n") % strerror( ::GetLastError() );
            sendLine(ss.str().c_str());
            return;
        }
    }
    else
    {
        SYSTEMTIME SystemTime;
        GetLocalTime( &SystemTime );
        ss << boost::format("Local system date: %04d-%02d-%02d\r\n") % SystemTime.wYear% SystemTime.wMonth % SystemTime.wDay;
        sendLine(ss.str().c_str());
    }

    sendLine( "OK\r\n" );
    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

void TelnetSession::ParseStat(const std::string & inputCommand)
{
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;
    int iChannelCount = 0;

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__ );

    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();
	sscanf( inputCommand.c_str(), "%s", sCommand );

    ss << "\r\n";
    ss << boost::format("Total channels: %d\r\n") % iChannelCount ;
    sendLine(ss.str().c_str());

	ss.str(std::string());
    ss << boost::format("Current busy: %d\r\n") % ChannelStat::Instance().GetBusyChannelCount();
    sendLine(ss.str().c_str());

	ss.str(std::string());
    ss << boost::format("Maximum busy: %d\r\n") % ChannelStat::Instance().GetMaxBusyChannelCount();
	sendLine(ss.str().c_str());

	// Zera o contador de maximo de chamadas em andamento.
	ChannelStat::Instance().ResetMaxBusyChannelCount();

	sendLine( "OK\r\n" );
    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

void TelnetSession::ParseChnc(const std::string & inputCommand)
{
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;
    int iChannelCount = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());

    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();

    ss << "\r\n";
    ss << boost::format("%u\r\n") % iChannelCount;
    sendLine(ss.str().c_str());
	sendLine( "OK\r\n" );

    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");

}

void TelnetSession::ParseDial(const std::string & inputCommand)
{
    char sCommand[256] = {0x00};
    char sSubCommand[256] = {0x00};
    std::stringstream ss;
    int iChannelCount = 0;
    int iChannel = 0;
    char sDnis[256] = {0};
    char sAni[256] = {0};

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__ );

    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();
    iChannel	= 0;	

	sscanf( inputCommand.c_str(), "%s %d %s %s", sCommand, &iChannel, &sDnis, &sAni );
				
	if( ( iChannel < 1 ) || ( iChannel > iChannelCount ) )
	{
		sendLine( "ERRO Invalid circuit range.\r\n" );
		return;
	}

	if( strlen(sDnis) <= 0 )
    {
		sendLine( "ERRO Invalid DNIS.\r\n" );
		return;
	}

	if( strlen(sAni) <= 0 )
    {
		sendLine( "ERRO Invalid ANI.\r\n" );
		return;
	}

	m_telnetServer->DialOnCircuit(iChannel, sDnis, sAni);

	sendLine( "OK\r\n" );

    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");

}


void TelnetSession::ParseDisc(const std::string & inputCommand)
{
    int iChannelCount = 0;
    char sCommand[256] = {0x00};
    int iChannel = 0, iChannelLow = 0, iChannelHigh = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());
    
	iChannelLow		= 0;
	iChannelHigh	= 0;
    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();

    

    std::vector<std::string> vec = split_string(inputCommand);
    if ( vec.size() != 3 )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    try
    {
        iChannelLow = std::stoi(vec[1]);
    }
    catch( const std::invalid_argument & ia ) {
        iChannelLow = -1;
    }

    try
    {
        iChannelHigh = std::stoi(vec[2]);    
    }
    catch( const std::invalid_argument & ia ) {
        iChannelHigh = -1;
    }

    if ( iChannelHigh < iChannelLow )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelLow <= 0 || iChannelLow > iChannelCount )
    {
        sendLine( "Start channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelHigh < 0 || iChannelHigh >iChannelCount )
    {
        sendLine( "End channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

	//sscanf( inputCommand.c_str(), "%s %d %d", sCommand, &iChannelLow, &iChannelHigh );
	

	m_telnetServer->DisconnectCircuit( iChannelLow, iChannelHigh );

	sendLine( "OK\r\n" );

    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

void TelnetSession::ParseBlkc(const std::string & inputCommand)
{
    int iChannelCount = 0;
    char sCommand[256] = {0x00};
    int iChannel = 0, iChannelLow = 0, iChannelHigh = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());

    
	iChannelLow		= 0;
	iChannelHigh	= 0;
    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();

    std::vector<std::string> vec = split_string(inputCommand);
    if ( vec.size() != 3 )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    try
    {
        iChannelLow = std::stoi(vec[1]);
    }
    catch( const std::invalid_argument & ia ) {
        iChannelLow = -1;
    }

    try
    {
        iChannelHigh = std::stoi(vec[2]);    
    }
    catch( const std::invalid_argument & ia ) {
        iChannelHigh = -1;
    }

    if ( iChannelHigh < iChannelLow )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelLow <= 0 || iChannelLow > iChannelCount )
    {
        sendLine( "Start channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelHigh < 0 || iChannelHigh >iChannelCount )
    {
        sendLine( "End channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }


	m_telnetServer->BlockCircuit( iChannelLow, iChannelHigh );

	sendLine( "OK\r\n" );

    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

void TelnetSession::ParseUnbc(const std::string & inputCommand)
{
    int iChannelCount = 0;
    char sCommand[256] = {0x00};
    int iChannel = 0, iChannelLow = 0, iChannelHigh = 0;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , inputCommand.c_str());

    
	iChannelLow		= 0;
	iChannelHigh	= 0;
    iChannelCount  = m_telnetServer->GetSwitch()->GetNumPortsInDataBase();

    std::vector<std::string> vec = split_string(inputCommand);
    if ( vec.size() != 3 )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    try
    {
        iChannelLow = std::stoi(vec[1]);
    }
    catch( const std::invalid_argument & ia ) {
        iChannelLow = -1;
    }

    try
    {
        iChannelHigh = std::stoi(vec[2]);    
    }
    catch( const std::invalid_argument & ia ) {
        iChannelHigh = -1;
    }

    if ( iChannelHigh < iChannelLow )
    {
        sendLine( "INVALID SYNTAX.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelLow <= 0 || iChannelLow > iChannelCount )
    {
        sendLine( "Start channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

    if ( iChannelHigh <= 0 || iChannelHigh >iChannelCount )
    {
        sendLine( "End channel invalid.\r\n" );
        if (m_telnetServer->OnNewLineCallBack())
            m_telnetServer->OnNewLineCallBack()( this , ">");
        return;
    }

	m_telnetServer->UnBlockCircuit( iChannelLow, iChannelHigh );

	sendLine( "OK\r\n" );

    if (m_telnetServer->OnNewLineCallBack())
        m_telnetServer->OnNewLineCallBack()( this , ">");
}

std::vector<std::string> TelnetSession::split_string(std::string input_string) 
{
    std::string::iterator new_end = unique(input_string.begin(), input_string.end(), 
        [] (const char &x, const char &y) 
        {
            return ( (x == y) && (x == ' ') );
        });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    std::vector<std::string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != std::string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}


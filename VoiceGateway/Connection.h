﻿#pragma once


#include <string>
#include <vector>
#include <winsock2.h>
#include <windows.h>

#include "SwitchDefines.h"
#include "SwitchTables.h"
#include "Tapi.h"
#include "ISwitch.h"
#include "Ivr.h"
#include "IConnection.h"
#include "Logger.h"

using namespace std;

#define MAX_PACKET_HANDLE			10

struct TCP_MSG {

	// identificacao do client remoto
	DWORD	RemoteId;
	// Dados		
	string Data;
	// identificador da chamada		
	char Handle[MAX_PACKET_HANDLE];

};


// Fila de dados dos eventos das conexoes
typedef queue <int, list<ST_CONNECTION> > QUEUE_ST_CONNECTION;

// Fila de dados dos eventos das conexoes
typedef queue <int, list<ST_CONNECTION> > QUEUE_ST_CONNECTION_MONITOR;

// Tipo para map de queues de resposta para a conexao TCP/IP 
typedef map < int, QUEUE_ST_CONNECTION_MONITOR > MAP_CONNECTION_MONITOR;

// Tipo para map de conexoes
typedef map < int, QUEUE_ST_CONNECTION > MAP_CONNECTION;


class ConnectionEvt : public IConnection
{
public:
    ConnectionEvt(ISwitch * p_Switch , boost::shared_ptr<Logger> pLog4cpp);
    ~ConnectionEvt(void);

    // Envia para a conexao Destino o Evento X com informacao da conexao de Origem,
    // indicando se houve mudanca do timeslot de transmissao local, default = false
    bool Connection_Send(ST_CONNECTION &Destino, int Evento, ST_CONNECTION &Origem, bool bLocalTimeSlotChange = false);

    // Envia evento para a conexao Destino na propria maquina
    bool Connection_Send(ST_CONNECTION &Destino, int Evento);
    // Envia evento para um canal local com informacoes de origem remota (ATM)
    bool Connection_Send(int Canal, ST_CONNECTION &Origem);
    // Pega a conexa de acordo com o canal passado, retorna true e tem conexao ou false se nao tem
    bool Connection_Get(int Channel, ST_CONNECTION &ref_st_connection, int iEvent = -1);

    // Pega a conex�o da fila de conex�es da confer�ncia. Retorna true se tem conexao ou false se nao tem
    bool Connection_Get_Monitor(ST_CONNECTION &ref_st_connection, int index);

    bool Connection_Get(ST_CONNECTION &ref_st_connection);
    // Limpa o map referente ao canal
    void Connection_Clear(int Channel);
    // Entra em uma secao critica para proteger o envio de eventos de uma canal para o outro
    void Connection_Lock(void);
    // Sai da secao critica que protege o envio de eventos de uma canal para o outro
    void Connection_Unlock(void);
    // Retorna o status do canal
    int Connection_ChannelStatus(int Channel);

    void Log(int iLevel, const char *sformat, ...);

private:
    //log4cpp::Category * 					m_pLog4cpp;	
    boost::shared_ptr<Logger>               m_pLog4cpp;
    va_list va;         // Lista para argumentos variaveis de log

    QUEUE_ST_CONNECTION         QueueConnectionConference;
    QUEUE_ST_CONNECTION_MONITOR QueueConnectionMonitor;
    MAP_CONNECTION	            MapConnection;
    MAP_CONNECTION_MONITOR	    MapConnectionMonitor;
    
    CRITICAL_SECTION CritConnection;
    char sPacote[4096];

    ISwitch * m_pSwitch;    
};


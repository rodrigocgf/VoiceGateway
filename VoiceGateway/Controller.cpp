#include "Controller.h"



Controller::Controller(boost::asio::io_service & p_ioService , fs::path p_fsCurrentPath) : 
    m_IoService(p_ioService), 
    m_fsCurrentPath(p_fsCurrentPath),
    CServiceBase(SERVICE_NAME, TRUE, TRUE, FALSE),
    m_work( new boost::asio::io_service::work(m_IoService) )
{    
    //m_work.reset( new boost::asio::io_service::work(m_IoService) );
}   

Controller::Controller(boost::asio::io_service & p_ioService ) : 
    m_IoService(p_ioService), 
    CServiceBase(SERVICE_NAME, TRUE, TRUE, FALSE),
    m_work( new boost::asio::io_service::work(m_IoService) )
{   
    //m_work.reset( new boost::asio::io_service::work(m_IoService) );


    /*
    m_fStopping = FALSE;

    // Create a manual-reset event that is not signaled at first to indicate 
    // the stopped signal of the service.
    m_hStoppedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (m_hStoppedEvent == NULL)
    {
        throw GetLastError();
    }
    */
}   

Controller::~Controller(void)
{
    /*
    if (m_hStoppedEvent)
    {
        CloseHandle(m_hStoppedEvent);
        m_hStoppedEvent = NULL;
    }
    */
}

fs::path Controller::GetCurrentPath(char ** argv)
{
    fs::path full_path( fs::initial_path<fs::path>() );

    full_path = fs::system_complete( fs::path( argv[0] ) );   
    
    return full_path;
}

fs::path Controller::GetCurrentPath()
{
    fs::path full_path( fs::initial_path<fs::path>() );
    LPQUERY_SERVICE_CONFIG lpsc;


    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, GENERIC_READ);

    if (!hSCManager)
    {
        full_path = fs::system_complete( fs::path( "c:\\" ) );        
        return full_path;
    }

    SC_HANDLE hService = OpenService(hSCManager, L"VoiceGatewayService", SERVICE_QUERY_CONFIG); 

    if (!hService)
    {
        CloseServiceHandle(hSCManager);
        full_path = fs::system_complete( fs::path( "c:\\" ) );        
        return full_path;
    }


    DWORD dwBytesNeeded = 0, dwBufSize;
    if (!QueryServiceConfig(hService, NULL, 0, &dwBytesNeeded))
    {
        DWORD dwError = GetLastError();
        if (ERROR_INSUFFICIENT_BUFFER == dwError)
        {
            dwBufSize = dwBytesNeeded;
            lpsc = (LPQUERY_SERVICE_CONFIG)HeapAlloc(GetProcessHeap(), 0, dwBufSize);
        }
        else
        {
            CloseServiceHandle(hService);
            CloseServiceHandle(hSCManager);
            full_path = fs::system_complete( fs::path( "c:\\" ) );        
            return full_path;
        }
    }

    if (QueryServiceConfig(hService, lpsc, dwBufSize, &dwBytesNeeded))
    {
        full_path = fs::system_complete( fs::path( lpsc->lpBinaryPathName ) );        
    }

    HeapFree(GetProcessHeap(), 0, lpsc);
    CloseServiceHandle(hService);
    CloseServiceHandle(hSCManager);

    return full_path;
}
/****************************************

        WINDOWS SERVICE MODE

****************************************/



void Controller::OnStart(DWORD dwArgc, LPWSTR *lpszArgv)
{
    _bstr_t bArgv(lpszArgv);
    char* szArgv = bArgv;
    //m_fsCurrentPath = GetCurrentPath(&szArgv);

    // Log a service start message to the Application log.
    WriteEventLogEntry(L"VoiceGateway in OnStart", EVENTLOG_INFORMATION_TYPE);
    

    m_Switch.reset(new Switch(m_IoService , m_fsCurrentPath) );
    m_Switch->ThreadInitSystem();
}





void Controller::OnStop()
{
    // Log a service stop message to the Application log.
    WriteEventLogEntry(L"VoiceGateway in OnStop", EVENTLOG_INFORMATION_TYPE);

    m_Switch->ThreadEndSystem();

    // Indicate that the service is stopping and wait for the finish of the 
    // main service function (ServiceWorkerThread).
    //m_fStopping = TRUE;
    //if (WaitForSingleObject(m_hStoppedEvent, INFINITE) != WAIT_OBJECT_0)
    //{
    //    throw GetLastError();
    //}
}


/****************************************

        COMMAND PROMPT MODE

****************************************/


void Controller::StartByPrompt()
{
    //m_work.reset( new boost::asio::io_service::work(m_IoService) );
    
    m_Switch.reset(new Switch(m_IoService , m_fsCurrentPath) );
    m_Switch->ThreadInitSystem();    
}

void Controller::StopByPrompt()
{
    m_Switch->ThreadEndSystem();
}



void Controller::ServiceWorkerThread(void)
{
    // Periodically check if the service is stopping.
    while (!m_fStopping)
    {
        // Perform main service function here...

        //boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        ::Sleep(2000);  // Simulate some lengthy operations.

    }

    // Signal the stopped event.
    SetEvent(m_hStoppedEvent);
}
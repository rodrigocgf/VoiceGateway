#pragma once


#include <stdio.h>
#include <string>
#include <tchar.h>
#include <utility>
#include <iostream>
#include <sstream>
#include <fstream>
#include <io.h>
#include <deque>
#include <queue>
#include <boost/shared_ptr.hpp>


#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include "IThrTelnetListen.h"


#include "ISwitch.h"
#include "QueueManager.hpp"
#include "Logger.h"
#include "TelnetSession.h"
#include "GarbageCollector.hpp"

#include "ProcessTelnetRequest.h"

using namespace boost::multi_index;


typedef QueueManager<ProcessTelnetRequest>::DataTypePtr ProcessTelnetRequestPtr;


typedef std::vector < TelnetSession * >             VEC_SP_TelnetSession;
typedef std::function< void( TelnetSession *) >     FPTR_ConnectedCallback;
typedef std::function< void(TelnetSession *, std::string) > FPTR_NewLineCallback;
//typedef std::function<void(const std::string & )> TimerCallback;

/*
class Sessions
{
public:
    Sessions(TelnetSession *    _pTelnetSession , Timer<TelnetSession> * _pTimer , const std::string & timerId) : 
        m_pTelnetSession(_pTelnetSession), m_pTimer(_pTimer), m_sTimerId(timerId)
    {
        
    }
    TelnetSession *         m_pTelnetSession;
    Timer<TelnetSession> *  m_pTimer;
    std::string             m_sTimerId;
};
*/

/*
struct ByTelnetSessionAndTimer{};
struct ByTelnetSession{};
struct ByTimer{};
struct ByTimerId{};

typedef multi_index_container
<
	boost::shared_ptr<Sessions>,
	indexed_by<		
        ordered_unique<
            tag<ByTimerId>,
            member< Sessions, std::string , &Sessions::m_sTimerId>
        >
	>
> ContTelnetSessTimer;
*/
/*
typedef multi_index_container
<
	boost::shared_ptr<Sessions>,
	indexed_by<		
		ordered_unique< 
			tag<ByTelnetSessionAndTimer>,
            composite_key<
                boost::shared_ptr<Sessions>,
			    member< Sessions,  TelnetSession * , &Sessions::m_pTelnetSession >,
                member< Sessions , Timer<TelnetSession> * , &Sessions::m_pTimer >
            >
		>,
        ordered_unique<
            tag<ByTelnetSession>,            
			member< Sessions,  TelnetSession * , &Sessions::m_pTelnetSession >
        >,
        ordered_unique<
            tag<ByTimer>,
            member< Sessions, Timer<TelnetSession> * , &Sessions::m_pTimer >
        >,
        ordered_unique<
            tag<ByTimerId>,
            member< Sessions, std::string , &Sessions::m_sTimerId>
        >
	>
> ContTelnetSessTimer;
*/

class ThrTelnetListen : public boost::enable_shared_from_this < ThrTelnetListen > , public IThrTelnetListen
//class ThrTelnetListen : public IThrTelnetListen
{
public:
    ThrTelnetListen(boost::asio::io_service & p_ioService , ISwitch * parent , boost::shared_ptr<Logger> pLog4cpp , const std::string & sTelnetPassword);
    ~ThrTelnetListen(void);

    
    void Start();
    void Stop();

    SOCKET                              sock_LISTEN;
	SOCKADDR_IN                         addr_LISTEN;

    SOCKET                              sock_ACCEPTED;
	SOCKADDR_IN                         addr;

    void SetConnectedCallback(FPTR_ConnectedCallback f);
    void SetNewLineCallback(FPTR_NewLineCallback f);

    VEC_SP_TelnetSession sessions() const { return m_sessions; }

    // IThrTelnetListen interface
    ISwitch * GetSwitch() const;
    bool interactivePrompt() const { return m_promptString.length() > 0; }
    void promptString(std::string prompt) { m_promptString = prompt; }
    std::string promptString() const { return m_promptString; }
    std::function< void( TelnetSession *) > OnConnectedCallback() const { return m_connectedCallback; }
    std::function< void(TelnetSession *, std::string) > OnNewLineCallBack() const { return m_newlineCallback; }
    bool ValidCircuit( const int ichannel );
    int StateCircuit( const int ichannel );
    void DialOnCircuit( const int ichannel, char *DNIS, char *ANI);
    int DisconnectCircuit( const int ichannel1, const int ichannel2 );
    int BlockCircuit( const int ichannel1, const int ichannel2 );
    int UnBlockCircuit( const int ichannel1, const int ichannel2 );
    // IThrTelnetListen interface
    
    void OnConnected( TelnetSession * session);    
    void OnNewLine( TelnetSession * session, std::string line);
    
    void AddToGarbageCollector(void * pObj);

private:
    std::string GenerateTimerId();
    static const int  uLocalScreenPort = 15151;
    boost::asio::io_service & 				        m_IoService;

    boost::shared_ptr<boost::thread>        m_ListenThreadPtr;
    boost::shared_ptr< boost::thread >	    m_QueueTelnetThrPtr;
    QueueManager<ProcessTelnetRequest>	    m_TelnetQMngr;
    
    boost::shared_ptr<Logger>                m_pLog4cppStart;

    std::string                         m_sTelnetPassword;
    bool                                m_bStop;
    ISwitch *                           m_Switch;
    
    boost::shared_ptr<GarbageCollector>   	m_GarbageCollector;    
    boost::shared_mutex                 m_ListMutex;    

    void Loop();    

    VEC_SP_TelnetSession                m_sessions;
    std::string                         m_promptString; // A string that denotes the current prompt    

    std::function< void(TelnetSession *, std::string) > m_newlineCallback;
    std::function< void( TelnetSession *) >             m_connectedCallback;
};



#include "XMLParser.h"

//------------------------------------------------------------------------------------------------------------------------

const char *XmlParser::ReturnLastError(void)
{
	try {
		return sLastError.c_str();
	}
	catch(...){
		return ((const char *)"");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::ChildrenCount(string sObject, int &iCount)
{
	try {
        int count = 0;
		int ret = CHILDREN_COUNT_ERROR;

		sXpath	=		"/";
		sXpath	+=	sObject;


        xpath_node_set xpathNodeSet = doc.select_nodes( (char *)sXpath	.c_str() );        
        
        for (pugi::xpath_node_set::const_iterator it = xpathNodeSet.begin(); it != xpathNodeSet.end(); ++it)
        {
            count++;
        }

        iCount = count;
        return OK;
        
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::ChildrenCount(string sObject, int &iCount)";
		iCount = 0;
		return CHILDREN_COUNT_ERROR;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::ChildrenCount(const char *sObject, int &iCount)
{
	try {

		int count = 0;
		int ret = CHILDREN_COUNT_ERROR;

		sXpath	=		"/";
		sXpath	+=	sObject;
        
        xpath_node_set xpathNodeSet = doc.select_nodes( (char *)sXpath	.c_str() );        
        
        for (pugi::xpath_node_set::const_iterator it = xpathNodeSet.begin(); it != xpathNodeSet.end(); ++it)
        {
            count++;
        }

        iCount = count;
        return OK;
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::ChildrenCount(const char *sObject, int &iCount)";
		iCount = 0;
		return CHILDREN_COUNT_ERROR;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::AttributeCount(string sObject, int &iCount)
{
	try {
        int count = 0;
		int ret;

		sXpath	=		"/";
		sXpath	+=	sObject;
        
        ret = ATTRIBUTE_COUNT_ERROR;
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );

        xml_node node = xpathNode.node();
        
        for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
        {
            //std::cout << " " << attr.name() << "=" << attr.value();
            count++;
        }

        iCount = count;
        return OK;
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::AttributeCount(string sObject, int &iCount)";
		iCount = 0;
		return ATTRIBUTE_COUNT_ERROR;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::AttributeCount(const char *sObject, int &iCount)
{
	try {
        int count = 0;
		int ret;

		sXpath	=		"/";
		sXpath	+=	sObject;
        
        ret = ATTRIBUTE_COUNT_ERROR;
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );

        xml_node node = xpathNode.node();
        
        for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
        {
            //std::cout << " " << attr.name() << "=" << attr.value();
            count++;
        }

        iCount = count;
        return OK;

	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::AttributeCount(const char *sObject, int &iCount)";
		iCount = 0;
		return ATTRIBUTE_COUNT_ERROR;
	}
}


//------------------------------------------------------------------------------------------------------------------------

int XmlParser::AttributeName(string sObject, string &sName, int Index)
{
	try {

		int ret;

		sXpath	=		"/";
		sXpath	+=	sObject;

        ret = SELECT_ATTRIBUTE_ERROR;
        
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );
        xml_node node = xpathNode.node();

        int i = 1;
        for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
        {
            if ( i == Index )
            {                
                sName.assign( attr.name() );
                ret = OK;
            }

            i++;
        }             		

		return ret;
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::AttributeName(string sObject, string &sName, int Index)";
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::AttributeValue(string sObject, string &sValue, int Index)
{
	try {
        int ret;

		sXpath	=		"/";
		sXpath	+=	sObject;

        ret = SELECT_ATTRIBUTE_ERROR;        
        
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );
        xml_node node = xpathNode.node();

        int i = 1;
        for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
        {
            if ( i == Index )
            {                
                sValue.assign( attr.value() );
                ret = OK;
            }

            i++;
        }

	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::AttributeValue(string sObject, string &sValue, int Index)";
		return ATTRIBUTE_VALUE_ERROR;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::AttributeValue(const char *sObject, const char *szAttributeName, string &sValue)
{
	try {

		int ret, i;

		string sAttrName , sAttrValue;
		string sAttributeName = szAttributeName;

		sXpath	=		"/";
		sXpath	+=	sObject;

        ret = SELECT_ATTRIBUTE_ERROR;
        sValue.erase();

        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );
        xml_node node = xpathNode.node();        
        for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
        {
            sAttrName = attr.name();
            sAttrValue = attr.value();

            //std::cout << "Parameter AttributeName : [" << sAttributeName.c_str() << "]\r\n";
            //std::cout << "Attribute (name/value) : [" <<  sAttrName.c_str() << "] = [" << sAttrValue.c_str() << "]\r\n";            

            if ( !sAttrName.compare(sAttributeName) )
            {
                sValue = sAttrValue;
                //std::cout << "return value : [" << sValue.c_str() << "] \r\n\r\n";
                ret = OK;
            }            
        }
        
        return ret;
        
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::AttributeValue(const char *sObject, const char *sAttributeName, string &sValue)";
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::ElementValue(const char *sObject, string &sValue)
{
	try {

		int ret;

		sXpath	=		"/";
		sXpath	+=	sObject;

        ret = SELECT_ELEMENT_ERROR;
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );

        if ( xpathNode )
        {            
            sValue.assign( xpathNode.node().text().get() );
            ret = OK;
        }
		
        return ret;        
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::ElementValue(const char *sObject, string &sValue)";
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int XmlParser::ElementValue(const char *sObject, int Index, string &sValue)
{
	try {

		int ret, iFilho;

        iFilho = Index;
        
		sXpath	=		"/";
		sXpath	+=	sObject;

        if(iFilho == 1)
			sprintf(sAux, sXpath.c_str());
		else
			sprintf(sAux, "%s/[%d]", sXpath.c_str(), iFilho-1);
			
		sXpath = sAux;
			
        ret = SELECT_ELEMENT_ERROR;
        xpath_node xpathNode = doc.select_node( (char *)sXpath	.c_str() );

        if ( xpathNode )
        {            
            sValue.assign( xpathNode.node().text().get() );
            ret = OK;
        }
		
        return ret;

	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::ElementValue(const char *sObject, int Index, string &sValue)";
		return -1;
	}
}



//------------------------------------------------------------------------------------------------------------------------

int XmlParser::Parser(string sXmlDocument)
{
	try {

		int ret;
        sXml = sXmlDocument;
                
        parseResult = doc.load_string( sXml.c_str() );

        if (parseResult)
        {
            //std::cout << "XML [" << sXml.c_str() << "] parsed WITHOUT errors, attr value: [" << doc.child("node").attribute("attr").value() << "]\n\n";

            return OK;
        }
        else
        {
            std::cout << "XML [" << sXml.c_str() << "] parsed with errors, attr value: [" << doc.child("node").attribute("attr").value() << "]\n";
            std::cout << "Error description: " << parseResult.description() << "\n";
            std::cout << "Error offset: " << parseResult.offset << " (error at [..." << ( sXml.c_str() + parseResult.offset) << "]\n\n";
        }        

		return 0;
	}
	catch(...){
		sLastError = "EXCEPTION: XmlParser::Parser(string sXmlDocument)";
		return -1;
	}
}


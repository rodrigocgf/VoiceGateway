﻿#include "Cdr.h"


Cdr::Cdr(boost::shared_ptr<Logger> pLog4cpp , ISwitch * pSwitch) : m_stop( false ) , m_pLog4cpp(pLog4cpp) , m_pSwitch(pSwitch)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    InitializeCriticalSection( &CritLogCdr );
}


Cdr::~Cdr()
{
    DeleteCriticalSection(&CritLogCdr);
}


void Cdr::ThreadInitSystem()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);


    m_ExecuteThreadPtr.reset ( new boost::thread ( boost::bind (&Cdr::MainLoop, this )));	
    m_ExecuteThreadPtr->detach();
}

void Cdr::ThreadEndSystem()
{
    m_pLog4cpp->debug("[%s]",__FUNCTION__);
    m_stop = true;
    m_cond.notify_one();
    m_ExecuteThreadPtr->join();

    m_pLog4cpp->debug("[%s] Cdr Thread stopped.",__FUNCTION__);
}


void Cdr::MainLoop()
{    
    int index, n, mday;
    char sPathCdr[_MAX_PATH], sPathDate[32], sIndex[4];
    char sNomeCdr[_MAX_PATH];
    std::string sFilename;
    std::string sIndexFileName;
    std::stringstream ss;
    FILE * IndexFile;
    time_t lBeginTime;
    time_t lEndTime;
    struct tm *actualTimer;
    bool DirectoryChange;
    std::string sAppName;

    try 
    {
        
        time(&lBeginTime);
        actualTimer = localtime(&lBeginTime);
        mday = actualTimer->tm_mday;


        while ( !m_stop )
        {
            Sleep(1000);

        
            if ( m_pSwitch->GetFlagEndProgram() )            
            {
                return ;
            }                        

            time(&lEndTime);
            actualTimer = localtime(&lEndTime);

            if(actualTimer->tm_mday != mday)
            {
                DirectoryChange = true;
                mday = actualTimer->tm_mday;
            }
            else 
                DirectoryChange = false;

            if( ( (lEndTime - lBeginTime) >= (m_pSwitch->GetImportCdrDelay() * 60) ) || DirectoryChange )            
            {
                EnterCriticalSection( &CritLogCdr );
                                
                std::for_each( MapLogCdr.begin() , MapLogCdr.end(), [&]( std::pair< std::string , STDIO_CDR > kv ) 
                {                    
                    sAppName = kv.first;
                    STDIO_CDR stdioCdr = kv.second;                    

                    if( ( stdioCdr.ChangeFile == 0) || (DirectoryChange))
                    {
                        sprintf(sPathCdr, "%s\\CDR\\%s", m_pSwitch->GetBasePathLog().c_str() , sAppName.c_str());
                        sprintf(sPathDate, "\\%04d-%02d-%02d", actualTimer->tm_year + 1900, actualTimer->tm_mon + 1, actualTimer->tm_mday);

                        if(DirectoryChange)
                        {
                            ss.str(std::string());
                            ss << boost::format("%s%s") % sPathCdr % sPathDate;
                            sFilename = ss.str();                            
                            boost::filesystem::create_directory(sFilename);                            
                            index = 1;
                        }
                        else
                        {
                            ss.str(std::string());
                            ss << boost::format("%s\\index.txt") % sPathCdr;
                            sFilename = ss.str();                            
                            index = ReadIndexFromFile(sFilename);

                            index++;
                        }

                        ss.str(std::string());
                        ss << boost::format("%s\\index.txt") % sPathCdr;
                        sIndexFileName = ss.str();

                        WriteIndexToFile(sIndexFileName , index);

                        sprintf(sNomeCdr,"%s%s\\CDR_%04d.txt", sPathCdr, sPathDate, index); 
                        

                        ss.str(std::string());
                        ss << "\r\n";
                        ss << "\t\t==================================================================================\r\n";
                        ss << "\t\t|                                                                                |\r\n";
                        ss << boost::format("\t\t|  CDR App Name   : %60s |\r\n") % sAppName;
                        ss << boost::format("\t\t|  CDR Index File : %60s |\r\n") % sIndexFileName.c_str();
                        ss << boost::format("\t\t|  CDR File Name  : %48s |\r\n") % sNomeCdr;
                        ss << "\t\t|                                                                                |\r\n";
                        ss << "\t\t==================================================================================\r\n";
                        //m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, ss.str().c_str() );                        

                        fflush( stdioCdr.mp_outFile );
                        fclose( stdioCdr.mp_outFile );                        
                                                
                        stdioCdr.mp_outFile = fopen(sNomeCdr , "a+");
                        stdioCdr.m_filename = sNomeCdr;
                        stdioCdr.ChangeFile = 1;
                        
                    }
                });
                
                LeaveCriticalSection( &CritLogCdr );                

                lBeginTime = lEndTime;
            }
        }
        
    }
    catch(...)
    {
        m_pLog4cpp->debug("Cdr MainLoop EXCECPTION !");
    }
}


void Cdr::WriteIndexToFile(const std::string & sFilename , int index)
{
    m_pLog4cpp->debug("[%p][%s] Index File : %s", this, __FUNCTION__, sFilename.c_str());
    
    std::ofstream outfile (sFilename.c_str(), std::ios::out | std::ios::trunc);
    std::stringstream ss;

    ss << boost::format("%04d") % index;
    if ( outfile.is_open() )
    {
        outfile << ss.str() << std::endl;
        outfile.flush();
        outfile.close();
    } else {
        m_pLog4cpp->debug("[%p][%s] ERROR opening file for WRITING", this, __FUNCTION__);
    }
    
    m_pLog4cpp->debug("[%p][%s] 4 characters written : %s", this, __FUNCTION__, ss.str().c_str() );

}

int Cdr::ReadIndexFromFile(const std::string & sFile)
{   
    m_pLog4cpp->debug("[%p][%s] Index File : %s", this, __FUNCTION__, sFile.c_str());

    std::string strLine;
    char szBuffer[5];
    int iRet = 0;
    memset(szBuffer,0x00,sizeof(szBuffer));

    std::ifstream in(sFile.c_str());
 	
	if(!in)
	{
        m_pLog4cpp->debug("[%p][%s] Cannot open the File : %s", this, __FUNCTION__, sFile.c_str());		
		return 0;
	}

    std::getline(in, strLine);    
    strncpy(szBuffer,strLine.c_str(),4);
    m_pLog4cpp->debug("[%p][%s] 4 first characters read : %s", this, __FUNCTION__, szBuffer);
    iRet = boost::lexical_cast<int>(szBuffer);
    m_pLog4cpp->debug("[%p][%s] Index : %d", this, __FUNCTION__, iRet);
    in.close();

    return iRet;

}

#include "ChannelStat.h"
/*
LONG ChannelStat::ScriptErrorCount[MAX_CHANNEL];
LONG ChannelStat::CallsAnsweredCount[MAX_CHANNEL];
LONG ChannelStat::CallsConnectedCount[MAX_CHANNEL];
LONG ChannelStat::LastActivityTime[MAX_CHANNEL];
LONG ChannelStat::BusyChannelCount;
LONG ChannelStat::MaxBusyChannelCount;
*/

ChannelStat::ChannelStat()
{
	int i;
	for( i = 0; i < MAX_CHANNEL; i++ )
	{
		ScriptErrorCount[i] = 0;
		CallsAnsweredCount[i] = 0;
		CallsConnectedCount[i] = 0;
		LastActivityTime[i] = 0;
	}
	BusyChannelCount = 0;
	MaxBusyChannelCount = 0;
}

ChannelStat::~ChannelStat()
{
}

void ChannelStat::ResetScriptErrorCount( const int& channel )
{
	try
	{
		InterlockedExchange( &ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::IncrementScriptErrorCount( const int& channel )
{
	try
	{
		InterlockedIncrement( &ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::DecrementScriptErrorCount( const int& channel )
{
	try
	{
		InterlockedDecrement( &ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

ULONG ChannelStat::GetScriptErrorCount( const int& channel )
{
	try
	{
		return ScriptErrorCount[CHECK_CHANNEL_LIMIT(channel)];
	}
	catch(...)
	{
	}
}

void ChannelStat::ResetCallsAnsweredCount( const int& channel )
{
	try
	{
		InterlockedExchange( &CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::IncrementCallsAnsweredCount( const int& channel )
{
	try
	{
		InterlockedIncrement( &CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::DecrementCallsAnsweredCount( const int& channel )
{
	try
	{
		InterlockedDecrement( &CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

ULONG ChannelStat::GetCallsAnsweredCount( const int& channel )
{
	try
	{
		return CallsAnsweredCount[CHECK_CHANNEL_LIMIT(channel)];
	}
	catch(...)
	{
	}
}

void ChannelStat::ResetCallsConnectedCount( const int& channel )
{
	try
	{
		InterlockedExchange( &CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::IncrementCallsConnectedCount( const int& channel )
{
	try
	{
		InterlockedIncrement( &CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::DecrementCallsConnectedCount( const int& channel )
{
	try
	{
		InterlockedDecrement( &CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)] );

		if( CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)] < 0 )
			InterlockedExchange( &CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)], 0 );
	}
	catch(...)
	{
	}
}

ULONG ChannelStat::GetCallsConnectedCount( const int& channel )
{
	try
	{
		return CallsConnectedCount[CHECK_CHANNEL_LIMIT(channel)];
	}
	catch(...)
	{
	}
}

void ChannelStat::SetLastActivityTime( const int& channel, const time_t& lastactivitytime )
{
	try
	{
		InterlockedExchange( &LastActivityTime[CHECK_CHANNEL_LIMIT(channel)], (LONG) lastactivitytime );
	}
	catch(...)
	{
	}
}

const time_t& ChannelStat::GetLastActivityTime( const int& channel )
{
	try
	{
		return (time_t) LastActivityTime[CHECK_CHANNEL_LIMIT(channel)];
	}
	catch(...)
	{
	}
}

void ChannelStat::IncrementBusyChannelCount()
{
	try
	{
		InterlockedIncrement( &BusyChannelCount );

		if( BusyChannelCount < 0 )
			InterlockedExchange( &BusyChannelCount, 0 );

		// Guarda sempre o valor maximo.
		if( BusyChannelCount > MaxBusyChannelCount )
			InterlockedExchange( &MaxBusyChannelCount, BusyChannelCount );
	}
	catch(...)
	{
	}
}

void ChannelStat::DecrementBusyChannelCount()
{
	try
	{
		InterlockedDecrement( &BusyChannelCount );

		if( BusyChannelCount < 0 )
			InterlockedExchange( &BusyChannelCount, 0 );
	}
	catch(...)
	{
	}
}

ULONG ChannelStat::GetBusyChannelCount()
{
	try
	{
		return BusyChannelCount;
	}
	catch(...)
	{
	}
}

void ChannelStat::ResetBusyChannelCount()
{
	try
	{
		InterlockedExchange( &BusyChannelCount, 0 );
	}
	catch(...)
	{
	}
}

void ChannelStat::ResetMaxBusyChannelCount()
{
	try
	{
		InterlockedExchange( &MaxBusyChannelCount, BusyChannelCount );
	}
	catch(...)
	{
	}
}

ULONG ChannelStat::GetMaxBusyChannelCount()
{
	try
	{
		return MaxBusyChannelCount;
	}
	catch(...)
	{
	}
}


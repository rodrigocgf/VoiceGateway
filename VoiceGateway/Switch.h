#pragma once

#include <map>
#include "StdIncludes.h"
#include "SwitchTables.h"
#include "SwitchDefines.h"
#include "ThrListen.h"
#include "ThrTelnetListen.h"
#include "DBAccess.h"
#include "Tapi.h"
#include "Ivr.h"
#include "Cdr.h"
#include "ISwitch.h"
#include "Connection.h"
#include "Class_TS_Manager.h"
#include "ConferenceManager.h"
//#include "GarbageCollector.hpp"

namespace fs = boost::filesystem;

typedef struct {
	int Channel;
	long LocalTimeslot;
	long GlobalTimeslot;
} ST_AUDIO;

typedef map<int,ST_AUDIO> MAP_AUDIO;

typedef struct _StRegister 
{
    char sTelnetPassword[_MAX_PATH+1];                   // Password do Telnet
    char sAvayaLogFile[_MAX_PATH+1];				// Caminho e nome para o arquivo de log das funcoes da lib AVAYA
    char BasePathLog[_MAX_PATH+1];					// Diretorio base de logs
    char PathPrompts[_MAX_PATH+1];					// Diretorio base de prompts
    char PathScripts[_MAX_PATH+1];					// Diretorio base de scripts
    char ASR_GrammarPackage[_MAX_PATH+1];		// Diretorio base das gramaticas para reconhecimento de voz
    char ASR_DatabaseType[_MAX_PATH+1];			// Tipo do servidor c/ a base de dados para uso de gramatica dinamica c/ reconhecimento de voz
    char ASR_DatabaseServer[_MAX_PATH+1];		// Servidor c/ a base de dados para uso de gramatica dinamica c/ reconhecimento de voz
    char ASR_DatabaseName[_MAX_PATH+1];			// Nome da base de dados para uso de gramatica dinamica c/ reconhecimento de voz
    char ASR_DatabaseUser[_MAX_PATH+1];			// Usuario da base de dados para uso de gramatica dinamica c/ reconhecimento de voz
    char ASR_DatabasePassword[_MAX_PATH+1];	// Senha do usuario da base de dados para uso de gramatica dinamica c/ reconhecimento de voz
    int ASR_PollingTime;										// Tempo maximo no polling de eventos da dialogic na implementacao de ASR
    char TTS_Server_Host[_MAX_PATH+1];			// Diretorio base das gramaticas para reconhecimento de voz
    char OdbcDataBase[256];									// Nome do banco de dados
    char OdbcDataSourceName[256];						// Data source name
    char OdbcPassWord[256];									// Senha
    char OdbcUser[256];											// Usuario
    char AppName[40];												// Nome da aplicacao usada para ler a configuracao do banco de dados
    char ScriptName[_MAX_PATH+1];						// Nome do script usado para ler a configuracao do banco de dados
    char IpRouter[128];											// Endereco IP do Roteador de mensagens IP
    unsigned short PortRouter;							// Porta do Roteador de mensagens IP
    int iR2MinDnis;													// Numero minimo de Dnis para pegar em protocolo R2 Digital	
    int iR2MaxDnis;													// Numero maximo de Dnis para pegar em protocolo R2 Digital	
    int iMaxIvrDbConnection;								// Numero maximo de conexoes com o banco de dados para as threads de atendimento (IVR)
    int LogLevel;														// Nivel de log
    int LogMaxFileSize;                         // Maximum log file size
    char LogMask[17];												// M�scara de bits para filtro pela origem da msg. (determina o valor do Qualifier do log)
    int Switch;															// Numero da Swicth
    int Machine;														// Numero da maquina
    int	TcpIpSendBufferSize;								// Tamanho do buffer tcpip de transmissao em bytes
    int	TcpIpRecvBufferSize;								// Tamanho do buffer tcpip de recepcao em bytes
    char TcpIpHost[16];											// Endereco IP da aplicacao
    int	TcpIpPort;								          // Numero da porta da aplicacao
    int FlagLogScreen;											// 0 - sem tela, 1 - somente status, 2 - normal
    int TimeOutDial;												// Time out para discagens em segundos
    int iAvayaLocalPort;										// Numero da porta local para comunicacao com CTI AVAYA
    int LogCDR;         										// Indica se deve logar o CDR no inicio da chamada e atualizar o CDR no fim da chamada
																				    //			0 - N�o logar o CDR no inicio e fim da chamada
																				    //			1 - Logar o CDR no inicio e fim da chamada
    int ImportCdrDelay;								  		// Tempo em minutos de grava��o de um arquivo de CDR para ser importado para o BD
    char sAvayaLocalIP[256];								// Numero do IP no qual o servidor Avaya local deve ouvir conexoes
    bool FlagTestMode;											// Flag indicando se a versao e de teste, de acordo com a chave TestMode no register
    bool NoRouter;													// Flag indicando se nao trabalha com o Roteador de mensagens
    WORD IdApp;															// Id da aplicacao para comunicacao TCP_IP
    WORD IdTypeApp;													// Tipo de aplicacao para comunicacao TCP_IP  
    int MC_Multichassis_Enabled;						// Habilita ou desabilita o Multichassis	
    char VXML_Url_App_Server[_MAX_PATH+1];	// Endereco da pagina vxml de inicio do browser
    char VXML_Url_Ssml_Server[_MAX_PATH+1];	// Endereco do servidor ssmlserver para processar os arquivos de audio
    bool DetectMessageBox;									// Habilita deteccao de caixa postal de celular.
} STREGISTER;

// Informacoes dos recursos cadastradas no banco de dados
typedef struct _DbResource
{
    int ResourceID;		// Id de identifica��o do recurso, sequencial n�o pode ser repetido	
    int ResourceTypeID; // Id de identifica�ao do tipo do rescruso
    std::string ResourceTypeName;	// Nome do tipo do recurso
    int ResourceCount;// Quantidade de recursos
    bool Enabled;			// Flag indicando se o recurso esta ativado
} DB_RESOURCE;

typedef struct _DbChannel
{
    int BoardID;				// Id de identifica��o da placa	
    int TrunkID;				// Id de identifica��o do tronco
    int BoardTrunkID;		// Id de identifica��o do tronco em rela��o � placa 
    int TrunkChannelID;	// Id de identifica��o do canal em rela��o ao tronco 
    char Direction;			// Dire��o do canal	
    bool Reserved;			// Flag indicando se o canal esta reservado
    bool AsrEnabled;		// Flag indicando se o Automatic speech recognition esta ativado
    bool Enabled;				// Flag indicando se o canal esta ativado
    bool FaxResource;		// Flag indicando se o canal tem recurso de FAX
    int Status;					// Status do canal
} DB_CHANNEL;

// Informacoes dos troncos cadastradas no banco de dados
typedef struct _DbTrunk
{
    int BoardID;								// Id de identifica��o da placa
    int TrunkRef;								// Id de identifica��o do tronco em rela��o � placa
    std::string TrunkTypeName;				// Tipo do tronco E1; T1
    int TrunkType;							// Tipo da interface (E1/T1/LSI/...)
    std::string TrunkSignalingName;	// Nome da sinaliza��o do tronco;
    int TrunkSignaling;					// Sinaliza��o do tronco;
    std::string TrunkProtocol;				// Protocolo de Sinalizacao GlobalCall: "br_r2_io", "isdn", "SS7", etc...
    int ChannelCount;						// Quantidade de canais por tronco
} DB_TRUNK;

// Informacoes das placas cadastradas no banco de dados
typedef struct _DbBoard
{  
    int BoardTypeID;										// Id de identifica��o do tipo da placa
    std::string BoardName;										// Nome da placa
    std::string ManufacturerName;						// Nome do fabricante da placa DIALOGIC; AMTELCO;
    std::string FamilyName;									// Nome da fam�lia que a placa pertence SPRINGWARE; DM3; XDS
    std::string IPProtocol;									// Protocolo IP (H323 ou SIP)
    int PropertyCount;									// Quantidade de propriedades da placa
    std::map<std::string, std::string> Properties;// Map de propriedade, armazena todas as propriedades da placa
    int TrunkCount;											// Quantidade de troncos da placa
    int ChannelCount;										// Quantidade de canais da placa
    int ResourceCount;									// Quantidade de recursos da placa
    std::vector<DB_RESOURCE> Resources; // Vetor de recursos, armazena todos os recursos da placa	
    bool IPResource;										// Flag indicando se a placa tem recurso de IP
} DB_BOARD;


class Switch : public ISwitch
{
private :
    boost::asio::io_service & 				        m_IoService;
    std::auto_ptr<boost::asio::io_service::work>    m_work;
    //boost::shared_ptr<IoServiceAcceptor> 	        m_ioServiceAcceptor;
    ThrListen *                                     m_pThrListen;
    ThrTelnetListen *                               m_pThrTelnetListen;

    ConferenceManagerClass *                        m_pConferenceManager;


    int                             NumBoardsInDataBase;
    int                             NumTrunksInDataBase;
    int                             NumPortsInDataBase;
    int                             IndexScreen;

    DB_BOARD					DbBoards[MAX_BOARD];
    DB_TRUNK					DbTrunks[MAX_TRUNK];
    DB_CHANNEL				    DbChannels[MAX_CHANNEL];


    boost::condition            			m_cond;	
    volatile bool               			m_stop;       
    boost::mutex                			m_Mutex;
    fs::path                                m_fsCurrentPath;
    std::string                             m_sFullLogPathName;
    std::string                             m_sMachineName;
    std::string                             m_sLocalIpAddress;
    STREGISTER                              m_StRegister;
    boost::shared_ptr<boost::thread>        m_ExecuteThreadPtr;
    CRITICAL_SECTION	                    m_CritDB;
    CRITICAL_SECTION                        m_CritStatus;

    MAP_AUDIO                               AudioTimeslots;
    int								        IdAudio;
    

    // LOG4CPP
	//log4cpp::Category * 					m_pLog4cpp;	
    //log4cpp::Layout *                       m_layout;
    //log4cpp::Appender *                     m_rfileAppender;
    //log4cpp::Category *                     m_category;

    boost::shared_ptr<Logger>               m_pLog4cpp;
        
    std::map<int, Ivr *>                    m_MapChanIvr;
    boost::shared_ptr<Cdr>                  m_Cdr;    
    boost::shared_ptr<DBAccess>             m_dbPtr;
    boost::shared_ptr<Tapi>                 m_Tapi;
    boost::shared_ptr<ConnectionEvt>        m_ConnectionPtr;
    //boost::shared_ptr<GarbageCollector>   	m_GarbageCollector;    

    // Guarda o nome da font para enviar Fax
    string sFaxFont;

    //std::string MountLogPath(const std::string & p_sBasePathLog);
    //void ConfigLog4cpp(const std::string & p_sFullLogFileName );    

    void GetMachineNameIp();
    void ReadRegister();
    void ReadRegisterODBC();
    void ReadRegisterPaths();
    void ReadRegisterGeneral();
    void ReadRegisterASR();
    void ReadRegisterCTI();    

    void PrintRegisterGeneral();
    std::string GetFullUdlPath();
    

    int OpenDatabase();
    int DbReadConfigIpRouter();
    int DbReadConfig();
    int DbReadBoards();
    int DbReadBoardInfo(int boardID);
    int DbReadTrunks(int boardID);
    int DbReadChannels(int boardID, int trunkID, int trunkRef);
    int DbReadResources(int boardID);

    void StartPortThreads();    
    void StopPortThreads();    
    void StartThreadListen_StatusScreenNet();
    void StopThreadListen_StatusScreenNet();
    void StartThreadListen_Telnet();
    void StopThreadListen_Telnet();
    void Run();
public:    

    Switch(boost::asio::io_service & p_ioService , fs::path p_fsCurrentPath);
    ~Switch(void);

    void Start();
    void Stop();
    void ThreadInitSystem();
    void ThreadEndSystem();

    Class_TS_Manager TS_Manager;
    std::string GetBaseLogPath() const;

    // ISwitch members
    int GetBoardId(int p_Channel) const;
    int GetTrunkId(int p_Channel) const;
    int GetTrunkChannelID(int p_Channel) const;
    int GetBoardTrunkID(int p_Channel) const;
    const std::string & GetFullLogPathName() const;
    int GetSwitch() const;
    int GetMachine() const;
    int GetIndexScreen() const;    
    void SetIndexScreen(int value);
    std::string GetManufacturerName(int p_Channel) const;
    std::string GetTrunkSignalingName(int p_TrunkId) const;
    int GetTrunkSignaling(int p_TrunkId) const;
    std::string GetTrunkProtocol(int p_TrunkId) const;
    std::string GetBasePathLog() const;
    int GetIdAudio() const;
    void SetAudioTimeslots_Channel(const int & p_IdAudio , const int & p_iChannel);
    void SetAudioTimeslots_LocalTimeSlot(const int & p_IdAudio , const int & p_iLocalTimeSlot);
    void SetAudioTimeslots_GlobalTimeSlot(const int & p_IdAudio , const int & p_iGlobalTimeSlot);
    int GetChannelsReserverd(int p_Channel) const;
    int GetChannelsStatus(int p_Channel);
    void SetChannelsStatus(int p_Channel, const int & p_Status);
    bool GetFlagEndProgram() const;
    int GetFlagLogScreen() const;
    std::string GetPathPrompts() const;
    int GetR2MinDnis() const;
    int GetR2MaxDnis() const;
    int GetLogCDR() const;
    std::string DbGetCallId(std::string sQuery);
    std::string GetPathScripts() const;
    void SetFaxFont(const std::string & p_sFaxFont) { sFaxFont = p_sFaxFont; }
    int GetTimeoutDial();
    void SetFaxCharSize(int value);
    void SetFaxLineWidth(int value);
    char GetChannelsDirection(int index);
    int GetNumPortsInDataBase();
    int GetMaxIvrDbConnection();
    int GetImportCdrDelay();
    int GetNumBoardsInDataBase();
    boost::shared_ptr<Cdr> GetCdr();
    std::string GetLocalIpAddress();
    std::string GetOdbcDataSourceName() const;
    std::string GetOdbcDataBase() const;
    std::string GetOdbcUser() const;
    std::string GetOdbcPassWord() const;
    std::string GetBoardResourceTypeName(int p_board, int p_resource);
    int GetTrunkCount(int p_board);    
    int GetResourceCount(int p_board);
    int GetChannelCount(int p_board);
    int GetTrunksRef(int p_trunkId);
    int GetTrunkType(int p_trunkId);
    bool GetIPResource(int p_boardId);
    std::string GetIPProtocol(int p_boardId);
    bool IsChannelEnabled(int p_channel);    
    int GetBoardResourceCount(int p_board , int p_resource);
    std::string GetBoardName(int p_board);
    int WinStatusChannel( int Channel, std::string& ani, std::string& dnis );
    const char* StatusChannel(int Status);    
    ThrListen * GetThrListen() const;
    boost::shared_ptr<Tapi> GetTapi();
    Ivr * GetIvr(int p_channel);
    WORD GetIdTypeApp();
    bool ValidCircuit( const int ichannel );
    boost::shared_ptr<IConnection> GetConnection();
    std::string GetAppName();
    Class_TS_Manager * GetTS_Manager();
    ConferenceManagerClass * GetConferenceManager();
    std::string GetRegistry(const std::string & sSubKey,LPCTSTR  pszValueName);
    // ISwitch members

    std::string GetVersion();
    
    
    // Guarda o tamanho do caracter da font do Fax
    int iFaxCharSize;
    // Guarda a largura da linha do Fax 
    int iFaxLineWidth;
        
    
};


#pragma once


#include <io.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>
#include <typeinfo>
#include "Logger.h"

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>


#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

#include <winsock2.h>
#include <windows.h>

#include "ISwitch.h"
#include "StatusScreenNet.h"
#include "IThrListen.h"
#include "Logger.h"

class ThrListen : public IThrListen
{
public:
    ThrListen(ISwitch * parent , boost::shared_ptr<Logger> pLog4cpp);
    ~ThrListen(void);

    ISwitch * GetSwitch() const;
    void Start();
    void Stop();

    void LogScreen(int ListIndex, int Item, char *s);
    void SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg);
    void SendMessageToAllScreens( std::string sMessage);

    SOCKET                              sock_LISTEN;
	SOCKADDR_IN                         addr_LISTEN;

    SOCKET                              sock_ACCEPTED;
	SOCKADDR_IN                         addr;

private:
    // Porta p/ recepcao da tela remota
    static const int  uLocalScreenPort = 13132;
    
    boost::shared_ptr<boost::thread>    m_ListenThreadPtr;
    //log4cpp::Category *                 m_pLog4cppStart;	
    boost::shared_ptr<Logger>             m_pLog4cppStart;
    bool                                m_bStop;
    ISwitch *                           m_Switch;

    StatusScreenNet *                   p_StatusScreen;

    boost::mutex               			m_ListMutex;
    std::list<StatusScreenNet *>        m_StatusScreenList;

    void Loop();
};


#pragma once

#include "StdIncludes.h"
#include "Switch.h"
#include "ServiceBase.h"

namespace fs = boost::filesystem;

// Internal name of the service
#define SERVICE_NAME             L"VoiceGateway"

// Displayed name of the service
#define SERVICE_DISPLAY_NAME     L"VoiceGateway Service"

// Service start options.
#define SERVICE_START_TYPE       SERVICE_DEMAND_START

// List of service dependencies - "dep1\0dep2\0\0"
#define SERVICE_DEPENDENCIES     L""

// The name of the account under which the service should run
#define SERVICE_ACCOUNT          L"NT AUTHORITY\\LocalService"

// The password to the service account name
#define SERVICE_PASSWORD         NULL

class Controller : public CServiceBase
{
private:
    boost::asio::io_service & 				        m_IoService;
    std::auto_ptr<boost::asio::io_service::work>    m_work;    
        
    boost::shared_ptr<Switch>                       m_Switch;
    fs::path                                        m_fsCurrentPath;

    BOOL m_fStopping;
    HANDLE m_hStoppedEvent;

    fs::path GetCurrentPath();
    fs::path GetCurrentPath(char ** argv);
public:    

    Controller(boost::asio::io_service & p_ioService , fs::path p_fsCurrentPath);
    Controller(boost::asio::io_service & p_ioService);    

    ~Controller(void);

    void StartByPrompt();
    void StopByPrompt();
        
protected:
                    
    virtual void OnStart(DWORD dwArgc, PWSTR *pszArgv);
    virtual void OnStop();

    void ServiceWorkerThread(void);

};


#ifndef _ICONNECTION_
#define _ICONNECTION_

#include "Ivr.h"

class IConnection
{
public:
    virtual void Connection_Clear(int Channel) = 0;
    virtual bool Connection_Send(ST_CONNECTION &Destino, int Evento, ST_CONNECTION &Origem, bool bLocalTimeSlotChange = false) = 0;
    virtual bool Connection_Send(ST_CONNECTION &Destino, int Evento) = 0;
    virtual bool Connection_Get(int Channel, ST_CONNECTION &ref_st_connection, int iEvent = -1) = 0;
    virtual bool Connection_Get(ST_CONNECTION &ref_st_connection) = 0;
    virtual int Connection_ChannelStatus(int Channel) = 0;
};


#endif

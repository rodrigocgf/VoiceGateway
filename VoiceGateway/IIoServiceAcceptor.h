#ifndef _IIOSERVICE_ACCEPTOR_
#define _IIOSERVICE_ACCEPTOR_

class StatusScreenNet;

class IIoServiceAcceptor
{
public:
    //virtual void AddToGarbage(StatusScreenNet * p_Obj) = 0;
    virtual ISwitch * GetSwitch() const = 0;
};

#endif
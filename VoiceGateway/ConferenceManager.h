/************************************************************************************************/
/* Support Comm Teleinformatica S/A																															*/
/*                                                                                              */
/* Modulo: ConferenceManager.cpp																																*/
/* Funcao: Funcoes relativas ao controle da conferência																					*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato - Criacao do Modulo                                               */
/* 30/OUT/2002 - Jairo Rosa - Integração com a Switch                                           */
/*                                                                                              */
/************************************************************************************************/
#ifndef CONFERENCE_MANAGER_H
#define CONFERENCE_MANAGER_H

#include <string>
#include <map>
#include <boost/shared_ptr.hpp>

#include <winsock2.h>
#include <windows.h>

// User includes
#include "DCB.h"
//#include "amtelco_conference.h"
//#include "dmv_conference.h"
#ifdef _LUA4
#include "Lua4\Ivr.h"
#else
#include "Ivr.h"
#endif // _LUA4
#include "ISwitch.h"
#include "ConferenceInterface.h"
#include "ConferenceDefines.h"
#include "Logger.h"


using namespace std;

class ConferenceManagerClass;

//Tabela de objetos de ConfMgmt por placa
typedef map <int, ConferenceManagerClass*> MAP_CONF;

class ConferenceManagerClass
{
private:
    int GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize);
    int GetDataPacket(const char *datapacket, char *cCommand, int &iValue);
    int GetDataPacket(const char *datapacket, char *cCommand, string &sValue);

    _ThreadParam *                          m_pThreadParam;
    MAP_CONF                                map_conf;
    volatile bool                          m_stop;   
    boost::shared_ptr<boost::thread>        m_ThrPtr;
    void Thr();    
        
public:
	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Constructor.
	ConferenceManagerClass(_ThreadParam * p_ThreadParam , ISwitch * parent , boost::shared_ptr<Logger> pLog4cpp);

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Destructor.
	~ConferenceManagerClass();


    void Start();
    void Stop();

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//  Process actions sent by IVR for calling conference functions
	void ProcessAppConfReq(void);

	void GetInfoConnection();
	void ClearConn(ST_CONNECTION &st_connection);
	void SetIconStatus(int Status);
	//void Log(int iLevel, const char *sformat, ...);
	bool SendConnection(int Event, string sData);
	int	ListenConnection(void);
    void SendErrorGetDataPacket(long lTimeSlot = 0, int iChannel = 0, int iRet = 0);

	void SendConferenceEvent();
	// Conference control semaphore
	HANDLE Semaphore;	

    ISwitch * m_pSwitch;

	// ponteiro para a classe log
    //log4cpp::Category * m_pLog4cpp;	
    boost::shared_ptr<Logger>   m_pLog4cpp;

	// Technology object ptr
	ConferenceInterfaceClass	* pTech;
	ST_CONNECTION	Connection;					// Estrutura usada como variavel temporaria para as operacoes de conexao
	ST_CONNECTION	Connection_remote;	// Estrutura de informacoes da conexao do canal remoto
	ST_CONNECTION	Connection_local;		// Estrutura de informacoes da conexao da conferência
};

#endif // CONFERENCE_MANAGER_H

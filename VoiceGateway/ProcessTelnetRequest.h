#pragma once
#include <functional>
#include "TelnetSession.h"
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>
//#include "Timer.hpp"
#include "TimerStd.hpp"
#include "TelnetSession.h"


typedef std::function<void(TelnetSession * pTelnetSession )> TimerCallback;


/*
class Sessions
{
public:
    Sessions(TelnetSession *    _pTelnetSession , Timer * _pTimer , const std::string & timerId) : 
        m_pTelnetSession(_pTelnetSession), m_pTimer(_pTimer), m_sTimerId(timerId)
    {
        
    }
    TelnetSession *         m_pTelnetSession;
    Timer *                 m_pTimer;
    std::string             m_sTimerId;
};
*/

/*
class Sessions
{
public:
    Sessions(TelnetSession *    _pTelnetSession , boost::shared_ptr<Timer> _spTimer , const std::string & timerId) : 
        m_pTelnetSession(_pTelnetSession), m_spTimer(_spTimer), m_sTimerId(timerId)
    {
        
    }
    TelnetSession *             m_pTelnetSession;
    boost::shared_ptr<Timer>    m_spTimer;
    std::string                 m_sTimerId;
};
*/

class IThrTelnetListen;

class ProcessTelnetRequest
{
public:
    ProcessTelnetRequest( IThrTelnetListen * _pThrTelnetListen ,   boost::shared_ptr<Logger>  pLog4cpp, SOCKET sock_ACCEPTED  , const std::string & sTelnetPassword);
    ~ProcessTelnetRequest(void);

    std::string GenerateTimerId();
    void operator()();

    //void OnTimerCallback( const std::string & sTimerId);

    void OnTimer();
    //void OnTimerCallback();
    
    
private:
    
    TimerCallback   fTimerCallback;
    boost::shared_ptr<Logger>  m_pLog4cpp;
    
    SOCKET      m_sock_ACCEPTED;
    IThrTelnetListen * m_pThrTelnetListen;    
    //boost::shared_ptr<Timer>        spTimer;
    TimerStd<TelnetSession> *        mp_TimerStd;
    //boost::shared_ptr<Sessions>     sessPtr;    
    std::string                     m_sTelnetPassword;
    TelnetSession * mp_TelnetSession;
    
    std::string     timerId;

};


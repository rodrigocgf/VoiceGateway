/*********************************************************************************************
	SwitchDefines.h

UPDATES:
	01/NOV/2002 - Jairo Rosa - Suporte a confer�ncia com placa DCB
*********************************************************************************************/

#ifndef SwitchDefinesH
	#define SwitchDefinesH

#include "DialogicDefines.h"
//#include "khomp_defines.h"

// Linha de separacao de logs
#define LOG_LINE	"------------------------------------------------------------------------------------------------------------------------------"
// Time out para operacoes gerais em segundos
#define	TIMEOUT_GENERAL						15
// Numero maximo de estacoes a serem controladas por uma thread
#define MAX_STATION_PER_THREAD		8

// Numero maximo de placas
#define MAX_BOARD								  8
// Numero maximo de troncos
#define MAX_TRUNK									64
// Numero maximo de canais para atendimento
#define MAX_CHANNEL								512
// Numero maximo de threads
#define MAX_THREADS								512
// Tamanho dos dados nas mensagens entre processos
#define MAX_MSG_DATA							256
// Ligacao de entrada
#define INBOUND										1
// Ligacao de saida
#define OUTBOUND									0
// Nome do serviso ATM
#define	ATM_SERVICE_NAME					"DeskATMLane"
// Nome do servico Dialogic
#define DIALOGIC_SERVICE_NAME			"Dialogic" 
// Numero maximo do contador para um semaforo
#define	MAX_SEMAPHORE_COUNT				65535

// EVENTOS DE ESTATISTICA
#define	START_CALL_IVR						"START_CALL_IVR"
#define	HANGUP_IVR								"HANGUP_IVR"
#define	VOICEMAIL									"VOICEMAIL"
#define	HANGUP_VOICEMAIL					"HANGUP_VOICEMAIL"
#define	HANGUP_VOICEMAIL					"HANGUP_VOICEMAIL"
#define	END_CALL_IVR							"END_CALL_IVR"

//Tamanho do buffer de envio de Actions (Confer�ncia)

#define MAX_ACTION_BUFFER			10240
// Tipos de tecnologia de FAX
enum {
	FAX_GAMMALINK,
	FAX_GAMMALINK_H100,
	FAX_DIALOGIC,
};

// Indices das telas de status e mensagens de log
enum {
	SCR_LOG = 1,
	SCR_TELNET,
	SCR_TCP_IP,
	SCR_ATM_LOG,
};

// Tipos de aplicacao cadastradas no banco de dados para comunicacao TCP_IP
enum {
	DAC,
	ATENDENTE,
	SUPERVISORA,
	SERVIDOR_FAX,
	GRAVADOR,
	DIALER,
};

typedef struct
{
	int Id;
	std::string BoardName;
} _BoardInfo;


// Parametros para as threads de telefonia
struct _ThreadParam{	
	bool bError;					// Flag de uso geral para infdicar algum erro durante a inicializacao da thread
	int Board;
	int Trunk;
	int Port;							// Canal ou estacao referente ao sistema
	int Stations[256];		// Estacoes a serem controladas por uma thread
	int FaxPorts[256];		// Numeros dos canais de FAX reais em relacao ao sistema
	int FaxIni;						// Canal de fax inicial
	int FaxEnd;						// Canal de fax final

	std::vector<_BoardInfo> BoardInfo;

	int Tecnologia;				// Tipo de tecnologia da placa
	HANDLE EventCreate;		// Evento que cada thread deve setar para indicar
};


enum IVR_COLUMNS_STATE 
{
    IVR_STATUS = 1,
    IVR_CALL_TYPE,
    IVR_DNIS,
    IVR_ANI,
    IVR_SERVICE,
    IVR_R2BITS
};

// Tipos de status em uma ligacao OUT
enum {
	ST_DIAL_RING,				// Chamando
	ST_DIAL_BUSY,				// Ocupado
	ST_DIAL_NOANSWER,		// Nao responde
	ST_DIAL_CONGESTION,	// Congestionamento
	ST_DIAL_HANGUP,			// Canal de saida desligou
	ST_DIAL_TIME_OUT,		// Timeout na tentiva de discar
};

// Eventos de comunicacao entre as threads da Switch

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// COLOCAR OS NOVOS EVENTOS SEMPRE NO FINAL
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

enum
{
	T_SW_USER = T_SWITCH + 1,				// Marca o inicio do enum, nao e usado pela aplicacao
	T_SW_LISTEN,										// Evento para o canal destino ouvir o timeslot da conexao
	T_SW_INIT_FAX,									// Evento para a thread de FAX para preparar para enviar um ou receber um FAX
	T_SW_SEND_FAX,									// Evento para a thread de FAX para enviar um FAX
	T_SW_RECV_FAX,									// Evento para a thread de FAX para receber um fax
	T_SW_FAX_ERROR,									// Evento para a thread que requisitou FAX indicando que houve erro no FAX
	T_SW_FAX_OK,										// Evento para a thread que requisitou FAX indicando sucesso na operacao do FAX
	T_SW_DIAL_RESULT,								// Evento indicando que foi efetuada uma ligacao e o canal de saida entrou em processo de ring
	T_SW_DIAL_TELNET,								// Evento para um canal discar sob comando telnet
	T_SW_DIAL_EX,										// Evento para um canal discar usado pelo script
	T_SW_DIAL_EX_HANGUP,						// Evento para um canal desligar uma ligacao de saida
	T_SW_DIAL_EX_RESULT,						// Evento para um canal desligar uma ligacao de saida
	T_SW_DIAL_EX_DISCONNECTED,			// Evento informando ao canal de entrada que o canal de saida desligou
	T_SW_DIAL_APPL,								  // Evento para um canal discar sob comando de uma aplica��o
  T_SW_ALLOCATE_CHANNEL,
	T_SW_CONF_REQUEST,							// Evento para executar uma fun��o de confer�ncia
	T_SW_CONF_OK,										// Evento de OK para uma fun��o de confer�ncia
	T_SW_CONF_ERROR,								// Evento de ERRO para uma fun��o de confer�ncia
  T_SW_CONF_MONITOR_REQUEST,      // Evento para executar uma fun��o de confer�ncia vindos do monitor
  T_SW_CONF_MONITOR_OK,           // Evento de OK para uma fun��o de confer�ncia vinda do monitor
  T_SW_CONF_MONITOR_ERROR,        // Evento de ERRO para uma fun��o de confer�ncia vinda do monitor
  T_SW_AUDIO_REQUEST,							// Evento requisitar servicos de audio
  T_SW_AUDIO_OK,									// Evento de OK para requisicoes de audio
  T_SW_AUDIO_ERROR								// Evento de ERRO para requisicoes de audio
};

template< typename a> class EventName
{
public:
    a GetEventName(int Event);    
};

template < typename a> a EventName<a>::GetEventName(int Event)
{
    a ssDef;
    
    ssDef << " EVENT ===>   ";
    switch(Event)
    {
    case T_SW_USER                  : ssDef << "T_SW_USER"; break;
    case T_SW_LISTEN                : ssDef << "T_SW_LISTEN	"; break;
    case T_SW_INIT_FAX              : ssDef << "T_SW_INIT_FAX"; break;
    case T_SW_SEND_FAX              : ssDef << "T_SW_SEND_FAX"; break;
    case T_SW_RECV_FAX              : ssDef << "T_SW_RECV_FAX"; break;
    case T_SW_FAX_ERROR             : ssDef << "T_SW_FAX_ERROR"; break;
    case T_SW_FAX_OK                : ssDef << "T_SW_FAX_OK"; break;
    case T_SW_DIAL_RESULT           : ssDef << "T_SW_DIAL_RESULT"; break;
    case T_SW_DIAL_TELNET           : ssDef << "T_SW_DIAL_TELNET"; break;
    case T_SW_DIAL_EX               : ssDef << "T_SW_DIAL_EX"; break;
    case T_SW_DIAL_EX_HANGUP        : ssDef << "T_SW_DIAL_EX_HANGUP"; break;
    case T_SW_DIAL_EX_RESULT        : ssDef << "T_SW_DIAL_EX_RESULT"; break;
    case T_SW_DIAL_EX_DISCONNECTED  : ssDef << "T_SW_DIAL_EX_DISCONNECTED"; break;
    case T_SW_DIAL_APPL             : ssDef << "T_SW_DIAL_APPL"; break;
    case T_SW_ALLOCATE_CHANNEL      : ssDef << "T_SW_DIAL_APPL"; break;
    case T_SW_CONF_REQUEST          : ssDef << "T_SW_CONF_REQUEST"; break;
    case T_SW_CONF_OK               : ssDef <<	 "T_SW_CONF_OK"; break;
    case T_SW_CONF_ERROR            : ssDef << "T_SW_CONF_ERROR"; break;
    case T_SW_CONF_MONITOR_REQUEST  : ssDef << "T_SW_CONF_MONITOR_REQUEST"; break;
    case T_SW_CONF_MONITOR_OK       : ssDef << "T_SW_CONF_MONITOR_OK"; break;
    case T_SW_CONF_MONITOR_ERROR    : ssDef << "T_SW_CONF_MONITOR_ERROR"; break;
    case T_SW_AUDIO_REQUEST         : ssDef << "T_SW_AUDIO_REQUEST"; break;
    case T_SW_AUDIO_OK              : ssDef << "T_SW_AUDIO_OK"; break;
    case T_SW_AUDIO_ERROR           : ssDef << "T_SW_AUDIO_ERROR"; break;
    }
    
    ssDef << "     ";
    return ssDef;
}

/*
#define GetEventName(Event) \
    do { \
    std::stringstream ssDef;\
    \
    ssDef << " EVENT ===>   ";\
    switch(Event)\
    {\
    case T_SW_USER                  : ssDef << "T_SW_USER"; break;\
    case T_SW_LISTEN                : ssDef << "T_SW_LISTEN	"; break;\
    case T_SW_INIT_FAX              : ssDef << "T_SW_INIT_FAX"; break;\
    case T_SW_SEND_FAX              : ssDef << "T_SW_SEND_FAX"; break;\
    case T_SW_RECV_FAX              : ssDef << "T_SW_RECV_FAX"; break;\
    case T_SW_FAX_ERROR             : ssDef << "T_SW_FAX_ERROR"; break;\
    case T_SW_FAX_OK                : ssDef << "T_SW_FAX_OK"; break;\
    case T_SW_DIAL_RESULT           : ssDef << "T_SW_DIAL_RESULT"; break;\
    case T_SW_DIAL_TELNET           : ssDef << "T_SW_DIAL_TELNET"; break;\
    case T_SW_DIAL_EX               : ssDef << "T_SW_DIAL_EX"; break;\
    case T_SW_DIAL_EX_HANGUP        : ssDef << "T_SW_DIAL_EX_HANGUP"; break;\
    case T_SW_DIAL_EX_RESULT        : ssDef << "T_SW_DIAL_EX_RESULT"; break;\
    case T_SW_DIAL_EX_DISCONNECTED  : ssDef << "T_SW_DIAL_EX_DISCONNECTED"; break;\
    case T_SW_DIAL_APPL             : ssDef << "T_SW_DIAL_APPL"; break;\
    case T_SW_ALLOCATE_CHANNEL      : ssDef << "T_SW_DIAL_APPL"; break;\
    case T_SW_CONF_REQUEST          : ssDef << "T_SW_CONF_REQUEST"; break;\
    case T_SW_CONF_OK               : ssDef <<	 "T_SW_CONF_OK"; break;\
    case T_SW_CONF_ERROR            : ssDef << "T_SW_CONF_ERROR"; break;\
    case T_SW_CONF_MONITOR_REQUEST  : ssDef << "T_SW_CONF_MONITOR_REQUEST"; break;\
    case T_SW_CONF_MONITOR_OK       : ssDef << "T_SW_CONF_MONITOR_OK"; break;\
    case T_SW_CONF_MONITOR_ERROR    : ssDef << "T_SW_CONF_MONITOR_ERROR"; break;\
    case T_SW_AUDIO_REQUEST         : ssDef << "T_SW_AUDIO_REQUEST"; break;\
    case T_SW_AUDIO_OK              : ssDef << "T_SW_AUDIO_OK"; break;\
    case T_SW_AUDIO_ERROR           : ssDef << "T_SW_AUDIO_ERROR"; break;\
    }\
    \
    ssDef << "     ";\
    return ssDef;} while(0)
*/

//Defini��es de features para placas multifun��o
#define	FT_DMV_VOICE						0x02
#define FT_DMV_CONFERENCE				0x04
#define FT_DMV_FAX							0x08
#define FT_DMV_RECOGNIZE				0x10


#endif

// Abstract class CTIBase definition.
/* 29/JUL/2003 - Jairo Rosa - Exportada DigitQueue () de Dialogic:: para uso em CTI					*/

#ifndef CTIBaseH
#define CTIBaseH

#ifndef _WIN32_WINNT
	// Defines the Windows NT 4 compilation.
	#define _WIN32_WINNT 0x0400
#endif

#include "StdIncludes.h"

#include <string>
#include <boost/shared_ptr.hpp>


//#include <windows.h>
//#include <setjmp.h>
//#include <string>

#include "DialogicDefines.h"
#include "ASR.h"

// Uses the ANSI Standard Namespace.
using namespace std;

// Computer Telephony Interface.
// Description:
//	This interface provides a layer to access a telephony hardware without acknowledgement of the correspondent API.
class CTIBase {

private:

	virtual const char *GetEventString() = 0;
	virtual void ResetUserTimer() = 0;
	virtual void PutError(char *fmt, ...) = 0;
	virtual void GetLastError(char *Buffer, int Size) = 0;
	virtual void GetMsiInfo(int BoardNumber, int &MsiBoardDev) = 0;
	virtual void RegObjetoTomOcupado() = 0;
	virtual void RegObjetoMusicaEspera() = 0;
	virtual void GetDigMask(char *Destino) = 0;
	virtual void ISDN_BuildMakecallBlk(char *OriginationPhoneNumber) = 0;
	virtual void ISDN_ReasonDisconnect() = 0;
	virtual void GC_ReasonDisconnect() = 0;
	virtual void CasSetBit(int bit) = 0;
	virtual int RetDigMaskTerm(const char *DigitsTerm) = 0;
	virtual int SetCallProgress() = 0;

	virtual int OuveTomOcupado() = 0;
	virtual int OuveMusicaEspera() = 0;
	virtual int GetTimeToWaitEvent() = 0;

	virtual char *gc_GetStringError() = 0;

	//## OpenChannel related functions
	virtual int OpenChannelE1(int Sinalizacao, const char *Protocolo) = 0;
	virtual int OpenChannelT1(int Sinalizacao, const char *Protocolo) = 0;
	virtual int OpenChannelLSI(int Sinalizacao) = 0;
	virtual int OpenChannelVOX() = 0;
	virtual int OpenChannelFAX() = 0;

public:
	virtual int OpenChannelFAX(int &iFaxChannel, long &lTxTimeSlot, int &iDevice) = 0;

public:

	virtual int OpenStation(int IdStation, int BoardNumber) = 0;
	virtual void FakeOpenChannel(int Channel, int InterfaceType, int Signaling = GLOBAL_CALL) = 0;
	virtual void SetPCM(int TypePcm) = 0;
	virtual void SetPlayFormat(int Format) = 0;
	virtual int GetPlayFormat() = 0;
	virtual void SetRecordFormat(int Format) = 0;
	virtual int GetVoxTermCause(void) = 0;
	virtual int GetRecordFormat() = 0;
	virtual void SetDevs(int *iArrayDevs, int iNumDevs) = 0;
	virtual const char *GetCallInfo() = 0;
	virtual int SetCallId( const char* lpcszCallId ) = 0;
	virtual const char* GetCallId() = 0;
	virtual void SetTimeToWaitEvent(int Time) = 0;
	virtual void SetPauseTime(int iPauseTime) = 0;
	virtual void SetFlashTime(int iFlashTime) = 0;
	virtual void CloseFile(void) = 0;
	virtual bool AlreadyDisconnected() = 0;
	virtual int GetChannelStatus( int Channel = -1 ) = 0;
	virtual int SetChannelState(int State) = 0;
	virtual int SendEvent(int Channel, int Event, int Param) = 0;
	virtual int SendEvent(int Channel, int Event, char *sMsg) = 0;
	virtual int SendEvent(int Channel, int Event) = 0;
	virtual int StopCh(int iTimeOut = 1000) = 0;
	virtual int StopChAsync(void) = 0;
	virtual char GetLastDigit() = 0;
	virtual int CasBitState(int Bit) = 0;
	virtual int Accept(bool bDoubleAnswer = false, bool bBilling = true) = 0;
	virtual int CallAck(int NumOfDnis) = 0;
	virtual int Volume(int Level) = 0;
	virtual int Speed(int Level) = 0;
	virtual int PlayToneSync(int Time, int Freq1, int Freq2 = 0) = 0;
	virtual int LineTone(int Time) = 0;
	virtual int RingTone(int Time) = 0;
	virtual int BusyTone(int Time) = 0;
	virtual int DetectTones( const int type ) = 0;
	virtual int GetDisconnectReasonCode(void) = 0;
	virtual char *GetReasonDisconnect() = 0;

	//## SCBUS related functions
	virtual int	ScUnlisten() = 0;
	virtual int ScListen(long Timeslot) = 0;
	virtual int ScConnect(int mode) = 0;
	virtual int ScRouteCallIP() = 0;
	virtual int GetTimeSlot() = 0;
  virtual int GetTimeSlotVox() = 0;
	virtual int GetLineTxTimeSlot(int DeviceType) = 0;

	//## System related functions
	virtual int	SetDXBD() = 0;
	virtual int	SetDXCAP() = 0;
	virtual int SetHANGUP() = 0;
	virtual int SetFAX() = 0;
	virtual int GetDevice(int Interface) = 0;
	virtual int GetLocalParam() = 0;
	virtual int	GetEventDev() = 0;
	virtual int	GetEventHandle() = 0;
	virtual int RegObj() = 0;
	virtual CTIBase *OBJ(int Canal) = 0;

	//## Events related processing functions
	/*
	virtual int GetEvent() = 0;
	virtual int ProcR2() = 0;
	virtual int ProcISDN() = 0;
	virtual int ProcGC() = 0;
	virtual int ProcLSI() = 0;
	virtual int ProcVOX() = 0;
	virtual int ProcFAX() = 0;
	virtual int ProcMSI() = 0;
	*/
	virtual int ProcMSI(long EventHandle) = 0;
	virtual int WaitEvent(void) = 0;
	virtual int WaitEventEx(int iNumDevices, int iMilliseconds) = 0;
	virtual int WaitForDisconnect(const int TimeToWait) = 0;
	virtual int WaitForPlayStopped() = 0;

	//## Dial related functions
	virtual int Dial(char *Dnis, char *Ani, int TimeConnect = 60, bool CallProgress = false) = 0;
	virtual int DialVox(char *Number, bool CallProgress) = 0;
	virtual int DialLSI(char *Number, bool CallProgress) = 0;
	virtual int DialAsync(char *Dnis, char *Ani, bool CallProgress = false) = 0;
	virtual int DialISDNAsync() = 0;
	virtual int DialGCAsync() = 0;
	virtual int IPDialAsync(char *Dnis, char *Ani) = 0;
	virtual int DialLineSide(char *sNumber) = 0;
	virtual int LineSideTransfer(char *sNumber) = 0;
	virtual int BlindTransfer( const char *lpcszNumber, int iTime = 10 ) = 0;


	//## VOX related functions
	virtual int HoldCall() = 0;
	virtual int RetrieveCall() = 0;
	virtual int OnHook() = 0;
	virtual int OffHook() = 0;
	virtual long GetTimeRecord() = 0;
	virtual long GetBytesIO() = 0;
	virtual	int HangupToneDetetion(bool bFlag) = 0;
	virtual	int MsgBoxBeepDetection(bool bFlag) = 0;
	virtual int PulseDetection(bool flag) = 0;


	//## MSI related functions
	virtual bool MsiHasRing() = 0;
	virtual int MsiGenRing(int NumRings) = 0;
	virtual int MsiStopRing() = 0;
	virtual int MsiGenZipTone() = 0;
	virtual int MsiHookStatus() = 0;
	virtual int MsiInitBoard() = 0;
 	virtual int MsiExtConnectionTs(long TimeSlot, int Type, long &TimeSlotConf, int &IdConnection) = 0;
	virtual int MsiExtConnectionSt(int Station, int Type, long &TimeSlotConf, int &IdConnection) = 0;
	virtual int MsiChangeExtConnectionTypeTs(long Tslot, int IdConnection, int Type) = 0;
	virtual int MsiChangeExtConnectionTypeSt(int Station, int IdConnection, int Type) = 0;
	virtual int MsiDelExtConnection() = 0;
	virtual unsigned short MsiSigEvt(unsigned short Bit) = 0;


//##		Playback funtions

	// Parameters:
	//	lpcszFilename		- [in] Pointer to a null-terminated string specifying the prompt file.
	//	lpcszTermDigit	- [in] Pointer to a null-terminated string specifying the termination mask of DTMF digits.
	//	lOffset					- [in] Long integer variable specifying the position in the file from where the playback will start (offset).
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a prompt file synchronously.
	virtual int PlayFile( const char *lpcszFilename, const char *lpcszTermDigit = NULL, const long lOffset = 0 ) = 0;
	
	// Parameters:
	//	lpcszFilename		- [in] Pointer to a null-terminated string specifying the prompt file.
	//	lpcszTermDigit	- [in] Pointer to a null-terminated string specifying the termination mask of DTMF digits.
	//	lOffset					- [in] Long integer variable specifying the position in the file from where the playback will start (offset).
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a prompt file assynchronously. This function requires a prompt playback control to stop or to wait the end of the playback.
	virtual int PlayFileAsync( const char *lpcszFilename, const char *lpcszTermDigit = NULL, const long lOffset = 0 ) = 0;

	// Parameters:
	//	lpcszFilename		- [in] Pointer to a null-terminated string specifying the prompt file.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a prompt file synchronously.
	virtual int PlayFileSync( const char *lpcszFilename ) = 0;

	// Parameters:
	//	Terminator    	- [in] Unsigned short variable specifying the termination mask of DTMF digits.
	//	lOffset					- [in] Long integer variable specifying the position in the file from where the playback will start (offset).
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a prompt list assynchronously.
	virtual int PlayAsync( unsigned short Terminator, const long lOffset = 0 ) = 0;

//##		Record funtions

//#########################################################################################################################
//##		NUANCE x DIALOGIC function definitions
//#########################################################################################################################

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the state of Vox Channel. Possible values are:
	// <CODE>
	//	enum VoxStates {
	//		S_DIAL,			// Dial state
	//		S_CALL,			// Call state
	//		S_GETDIG,		// Get Digit state
	//		S_HOOK,			// Hook state
	//		S_IDLE,			// Idle state
	//		S_PLAY,			// Play state
	//		S_RECORD,		// Record state
	//		S_STOPED,		// Stopped state
	//		S_TONE,			// Playing tone state
	//		S_WINK,			// Wink state
	//		S_UNKNOW,		// Unknow
	//	};
	// </CODE>
	// Description:
	//	This function abort play, record, recognize operations.
	virtual int VoxState() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the telephony interface. Possible values are:
	// <PRE>
	//	0 = E1
	//	1 = T1
	// </PRE>
	// Description:
	//	This function gets the telephony interface.
	virtual int GetInterface() = 0;

//#########################################################################################################################


//#########################################################################################################################
//##		NUANCE function definitions
//#########################################################################################################################

	// Parameters:
	//	parm	- [in] This parameter is used to pass all data needed to initialize the engine.
	//	A struct called NUANCE_PARAM_ENGINE is passed as the parameter to this function.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to initialize the Nuance engine.
	// See Also:
	//	NUANCE_PARAM_ENGINE
	virtual int InitEngine(void *parm) = 0;

	// Parameters:
	//	parm	- [in] This parameter is used to pass all data needed to use the engine.
	//	A struct called NUANCE_PARAM_ENGINE is passed as the parameter to this function.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to initialize the Nuance engine.
	// See Also:
	//	NUANCE_PARAM_ENGINE
	virtual int SetParamEngine(void *parm) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	This function waits for a incomming call.
	virtual void WaitCall() = 0;

	// Parameters:
	//	number	- [in] Pointer to a null-terminated string specifying the phone number.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to dial and, on call complete, transfer the call.
	virtual int PlaceCall(char const * number) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to drop a conference on tromboning calls. This is the scenario: "A" calls "B".
	//	"B" calls "C". "B" keeps listening to "A" while "A" talks to "C". This function disconnects
	//	"A" from "C" keeping "A" and "B".
  virtual int DropConference( void ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the transfer status. The transfer status can be the following values:
	// <CODE>
	//	#define REMOTE_PHONE_DIALING      0
	//	#define REMOTE_PHONE_ANSWER       1
	//	#define REMOTE_PHONE_BUSY         2
	//	#define REMOTE_PHONE_NO_ANSWER    3
	//	#define REMOTE_PHONE_DISCONNECT   4
	//	#define LOCAL_PHONE_DISCONNECT    5
	// </CODE>
	// Description:
	//	Function used to get the status of a outgoing call after the TransferCall function is
	//	called.
  virtual int GetTransferStatus( void ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero (false) if not enabled or 1 (true) if enabled.
	// Description:
	//	Function used to verify if the Nuance telephony mode is enabled or not.
	virtual int GetEnableTelephony(  ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero (false) if not enabled or 1 (true) if enabled.
	// Description:
	//	Function used to verify if the Nuance playback is enabled or not.
	virtual int GetEnablePlayBack() = 0;

	// Parameters:
	//	sProtocol			- [out] Reference to a standard string specifying the telephony protocol.
	//	Possible protocol strings can be found in the Nuance Documentation.
	//	iSignaling		- [out] Reference to a int specifying the telephony signalling protocol.
	//	Possible values can be:
	// <PRE>
	//	1 = GlobalCall
	//	2 = ISDN
	// </PRE>
	// Return Value:
	//	The return value is zero (false) if not enabled or 1 (true) if enabled.
	// Description:
	//	Function used to verify if the Nuance playback is enabled or not.
	virtual void GetProtocol(string &sProtocol, int &iSignaling) = 0;
  
  virtual int StopRecordFile(void) = 0;

  virtual int VoxScListen(long timeslot) = 0;

	// Parameters:
	//	TxTimeSlot	- [in] A long variable specifying the time slot the speech is coming in on.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to set what time slot the speech is coming in on if the Nuance Telephony Mode
	//	is not enabled.
	virtual void SetCallerPort(long TxTimeSlot) = 0;

	// Parameters:
	//	TxTimeSlot	- [in] A long variable specifying the time slot on which any playback will occur.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to set the time slot on which any playback will occur if the Nuance Playback
	//	is not enabled.
	virtual void SetPromptPort(long TxTimeSlot) = 0;

	// Parameters:
	//	Flag	- [in] A flag indicating if a call is connected or not. Possible values is: 1 (true)
	//	or 0 (false).
	// Return Value:
	//	None.
	// Description:
	//	Function used to indicate the Nuance Engine if a call is connected or not if the Nuance
	//	Telephony Mode is not enabled.
	virtual void ExternalCallConnected(int Flag) = 0;

	// Parameters:
	//	TxTimeSlot	- [out] Reference to a long variable to retrieve what time slot the speech is
	//	coming in on.
	// Return Value:
	//	None.
	// Description:
	//	Function used to get what time slot the speech is coming in on.
	virtual void GetPromptPort(long & TxTimeSlot)= 0;

	// Parameters:
	//	prompt	- [in] Pointer to a null-terminated string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a prompt to the prompt list.
  virtual int AddPrompt( const char *prompt ) = 0;

	// Parameters:
	//	prompt	- [in] Reference to a standard string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a prompt to the prompt list.
  virtual int AddPrompt( string &prompt ) = 0;

	// Parameters:
	//	prompt	- [in] Pointer to a null-terminated string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a error prompt to the prompt list. An error prompt is distinguished
	//	from a normal prompt for not be included in the prompt list restored by RestorePromptList.
  virtual int AddErrorPrompt( const char *prompt ) = 0;

	// Parameters:
	//	prompt	- [in] Reference to a standard string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a error prompt to the prompt list. An error prompt is distinguished
	//	from a normal prompt for not be included in the prompt list restored by RestorePromptList.
  virtual int AddErrorPrompt( string &prompt ) = 0;

	// Parameters:
	//	prompt	- [in] Pointer to a null-terminated string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a TTS (text-to-speech) prompt to the prompt list. An TTS prompt
	//	corresponds to the text to be converted.
  virtual int AddTTSPrompt( const char *prompt ) = 0;

	// Parameters:
	//	prompt	- [in] Reference to a standard string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to add a TTS (text-to-speech) prompt to the prompt list. An TTS prompt
	//	corresponds to the text to be converted.
  virtual int AddTTSPrompt( string &prompt ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to restore the prompt list ignoring the error prompts.
	virtual int RestorePromptList() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function aborts all pending operations.
	virtual void AbortAll() = 0;

//#########################################################################################################################


//#########################################################################################################################
//##		ASR function definitions
//#########################################################################################################################

#ifdef _ASR

	// Parameters:
	//	lpcszGrammar			- [in] A pointer to a null-terminated string specifying the grammar name. Default value lpcszGrammar = NULL.
	//	dNoSpeechTimeout	- [in] A double floating point variable specifying the no speech timeout value in seconds. Default value dNoSpeechTimeout = 2.0.
	//	lpcszFilename			- [in] A pointer to a null-terminated string specifying the filename where the utterance will be saved. Default value lpcszFilename = NULL.
	// Return Value:
	//	If the function succeeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Function used to recognize speech given a grammar. The speech recognition will stop if no speech is detected until the number of seconds specified by the dNoSpeechTimeout parameter. The default parameter is dNoSpeechTimeout = 2.0 seconds.
	// Remarks:
	//	The parameter lpcszGrammar only can be NULL during an enrollment session. The filename will not be saved if a no speech timeout or an error occurs.
	// See Also:
	//	ASRGetNumberOfAnswers, ASRGetAnswer, ASRGetNumberOfFilledSlots, ASRGetSlotName, ASRGetScore
	virtual	int ASRRecognize( const char* lpcszGrammar = NULL, const double& dNoSpeechTimeout = 2.0, const char* lpcszFilename = NULL ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	If the function suceeds, the return value is the number of answers. If the function fails, the return value is negative.
	// Description:
	//	Function used to get the number of recognized answers.
	// See Also:
	//	ASRGetAnswer, ASRGetNumberOfFilledSlots, ASRGetSlotName, ASRGetScore, ASRGetNumberOfInterpretations
	virtual	int ASRGetNumberOfAnswers() = 0;

	// Parameters:
	//	iAnswerIndex					- [in] A constant reference to a integer variable specifying the recognized answer index. Default value is iAnswerIndex = 0.
	// Return Value:
	//	If the function suceeds, the return value is the number of answers. If the function fails, the return value is negative.
	// Description:
	//	Function used to get the number of interpretations for a recognized answer.
	// See Also:
	//	ASRGetAnswer, ASRGetNumberOfFilledSlots, ASRGetSlotName, ASRGetScore, ASRGetNumberOfAnswers
	virtual	int ASRGetNumberOfInterpretations( const int& iAnswerIndex = 0 ) = 0;

	// Parameters:
	//	lpcszSlotName					- [in] A constant pointer to a null-terminated string specifying the slot name.
	//	iAnswerIndex					- [in] A constant reference to a integer variable specifying the recognized answer index. Default value is iAnswerIndex = 0.
	//	iInterpretationIndex	- [in] A constant reference to a integer variable specifying the interpretation index of the recognized answer. Default value is iInterpretationIndex = 0.
	// Return Value:
	//	If the function suceeds, the return value is a pointer to a null-terminated string reporting the answer for the given slot name. If the function fails, the return value is NULL;
	// Description:
	//	Function used to get the recognized answer given a slot name and an answer index. The default parameter is iIndex = 0 which indicates the 1st answer will be used by default when more answers are available (N-best).
	// See Also:
	//	ASRGetNumberOfAnswers, ASRGetNumberOfFilledSlots, ASRGetSlotName, ASRGetScore
	virtual	const char* ASRGetAnswer( const char* lpcszSlotName, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;

	// Parameters:
	//	lpcszSlotName					- [in] A constant pointer to a null-terminated string specifying the slot name. Default value is lpcszSlotName = NULL.
	//	iAnswerIndex					- [in] A constant reference to a integer variable specifying the recognized answer index. Default value is iAnswerIndex = 0.
	//	iInterpretationIndex	- [in] A constant reference to a integer variable specifying the interpretation index of the recognized answer. Default value is iInterpretationIndex = -1.
	// Return Value:
	//	If the function suceeds, the return value is the confidence score of the answer. If the function fails, the return value is negative.
	// Description:
	//	Function used to get the confidence score of an recognized answer given an index. The default parameter is iIndex = 0 which indicates the 1st answer will be used by default when more answers are available (N-best). The confidence score is in the range 0 to 100.
	// See Also:
	//	ASRGetNumberOfAnswers, ASRGetNumberOfFilledSlots, ASRGetSlotName, ASRGetAnswer
	virtual	int ASRGetScore( const char* lpcszSlotName = NULL, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;

	// Parameters:
	//	iAnswerIndex					- [in] A constant reference to a integer variable specifying the recognized answer index. Default value is iAnswerIndex = 0.
	//	iInterpretationIndex	- [in] A constant reference to a integer variable specifying the interpretation index of the recognized answer. Default value is iInterpretationIndex = 0.
	// Return Value:
	//	If the function suceeds, the return value is the number of filled slots in the answer. If the function fails, the return value is negative.
	// Description:
	//	Function used to get the number of filled slots given an answer index. The default parameter is iIndex = 0 which indicates the 1st answer will be used by default when more answers are available (N-best).
	// See Also:
	//	ASRGetNumberOfAnswers, ASRGetScore, ASRGetSlotName, ASRGetAnswer
	virtual	int ASRGetNumberOfFilledSlots( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;

	// Parameters:
	//	iAnswerIndex					- [in] A constant reference to a integer variable specifying the recognized answer index. Default value is iAnswerIndex = 0.
	//	iInterpretationIndex	- [in] A constant reference to a integer variable specifying the interpretation index of the recognized answer. Default value is iInterpretationIndex = 0.
	//	iSlotIndex						- [in] A constant reference to a integer variable specifying the slot index in the answer given. Default value is iSlotIndex = 0.
	// Return Value:
	//	If the function suceeds, the return value is a pointer to a null-terminated string reporting the slot name. If the function fails, the return value is NULL;
	// Description:
	//	Function used to get the slot name given an answer index and a slot index. The default parameter is iAnswerIndex = 0 and iSlotIndex = 0 which indicates the 1st answer will be used by default when more answers are available (N-best) and the 1st slot of this answer will be used by default.
	// See Also:
	//	ASRGetNumberOfAnswers, ASRGetScore, ASRGetNumberOfFilledSlots, ASRGetAnswer
	virtual	const char* ASRGetSlotName( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0, const int& iSlotIndex = 0 ) = 0;

	// Parameters:
	//	lpcszGrammarId		- [in] A null-terminated string used to identify this dynamic grammar.
	//	lpcszGrammarLabel	- [in] A null-terminated string specifying the point of insertion in the grammar.
	// Return Value:
	//	If the function suceeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Prepares a dynamic grammar to be used.
	virtual int ASRAddVocabulary( const char* lpcszGrammarId, const char* lpcszGrammarLabel ) = 0;

	// Parameters:
	//	lpcszGrammarId		- [in] A null-terminated string used to identify this dynamic grammar.
	//	lpcszPhraseId			- [in] A null-terminated string used to identify the voice-enrolled phrase in the specified dynamic grammar.
	//	lpcszPhraseText		- [in] A null-terminated string identifying the text to be recognized.
	//	lpcszPhraseNL			- [in] A null-terminated string used to identify the voice-enrolled phrase in the specified dynamic grammar when it is recognized (natural language answer).
	// Return Value:
	//	If the function suceeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Adds a phrase to a dynamic grammar.
	virtual int ASRAddPhrase( const char* lpcszGrammarId, const char* lpcszPhraseId, const char* lpcszPhraseText, const char* lpcszPhraseNL ) = 0;

	// Parameters:
	//	lpcszGrammarId	- [in] A null-terminated string used to identify this dynamic grammar in the specified database.
	//	lpcszPhraseId		- [in] A null-terminated string that acts as a handle.
	// Return Value:
	//	If the function suceeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Removes a phrase from a dynamic grammar.
	virtual int ASRRemovePhrase( const char* lpcszGrammarId, const char* lpcszPhraseId ) = 0;

	// Parameters:
	//	lpcszGrammarId		- [in] A null-terminated string used to identify this dynamic grammar.
	//	lpcszPhraseId			- [in] A null-terminated string used to identify the voice-enrolled phrase in the specified dynamic grammar.
	//	lpcszPhraseNL			- [in] A null-terminated string used to identify the voice-enrolled phrase in the specified dynamic grammar when it is recognized (natural language answer).
	//	lpcszGrammar			- [in] A null-terminated string used to specify the grammar used for the enrollment.
	// Return Value:
	//	If the function succeeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Starts a voice enrollment session. After call StartEnrollmentSession some recognition sessions must be created through StartRecognitionSession/StopRecognitionSession to generate voice samples for the voice enrollment process.
	// See Also:
	//	StopEnrollmentSession,StartRecognitionSession,StopRecognitionSession
	virtual int ASRStartEnrollmentSession( const char* lpcszGrammarId, const char* lpcszPhraseId, const char* lpcszPhraseNL, const char* lpcszGrammar ) = 0;

	// Parameters:
	//	bAbort						- [in] A boolean flag used to indicate the enrollment session aborting.
	//	lpcszFileName			- [in] A null-terminated string specifying the name of the file where the recording of the best repetition of the enrolled phrase should be stored. If this parameter is NULL none enrolled phrase will be stored. Default value: lpcszFileName = NULL.
	//	bsPhraseIdInClash	- [out] A basic string specifying the phrase id which the clash is happening. Default value: bsPhraseIdInClash = "".
	// Return Value:
	//	If the function succeeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Stops an enrollment session and saves enrolled phrase when required.
	// See Also:
	//	StartEnrollmentSession,StartRecognitionSession,StopRecognitionSession
	virtual int ASRStopEnrollmentSession( const bool& bAbort = false, const char * lpcszFileName = NULL, string& bsPhraseIdInClash = string("") ) = 0;

	// Parameters:
	//	None
	// Return Value:
	//	If the function succeeds, the return value is the number of repetitions still needed to conclude the enrollment session. If the function fails, the return value is negative.
	// Description:
	//	This function returns the number of repetitions (voice samples obtained by StartRecognitionSession/StopRecognitionSesssion function calls) still needed to conclude an enrollment session.
	virtual int ASRGetEnrollmentStatus() = 0;

	// Parameters:
	//	lpcszKey		- [in] A pointer to a null-terminated string specifying the configuration parameter key.
	// Return Value:
	//	If the function succeeds, the return value is a pointer to a null-terminated string specifying the configuration parameter value. If the function fails, the return value is NULL.
	// Description:
	//	Function used to get ASR configuration parameters values.
	// See Also:
	//	ASRSetParameter
	virtual const char *ASRGetParameter( const char* lpcszKey ) = 0;

	// Parameters:
	//	lpcszKey		- [in] A pointer to a null-terminated string specifying the configuration parameter key.
	//	lpcszValue	- [in] A pointer to a null-terminated string specifying the configuration parameter value.
	//	iValueType	- [in] The configuration parameter value type passed in lpcszValue.
	// Return Value:
	//	If the function succeeds, the return value is zero. If the function fails, the return value is non-zero.
	// Description:
	//	Function used to set configuration parameters values of string type.
	// See Also:
	//	ASRGetParameter
	virtual int ASRSetParameter( const char* lpcszKey, const char* lpcszValue, const int& iValueType = pvString ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The safe index of the start of speech.
	// Description:
	//	Gets the safe index of the start of speech.
	// See Also:
	//	ASRGetStartActualIndex, ASRGetEndSafeIndex and ASRGetEndActualIndex
	virtual	int ASRGetStartSafeIndex() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The actual index of the start of speech.
	// Description:
	//	Gets the actual index of the start of speech.
	// See Also:
	//	ASRGetStartSafeIndex, ASRGetEndSafeIndex and ASRGetEndActualIndex
	virtual	int ASRGetStartActualIndex() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The safe index of the start of speech.
	// Description:
	//	Gets the safe index of the start of speech.
	// See Also:
	//	ASRGetStartSafeIndex, ASRGetStartActualIndex and ASRGetEndActualIndex
	virtual	int ASRGetEndSafeIndex() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The safe index of the start of speech.
	// Description:
	//	Gets the safe index of the start of speech.
	// See Also:
	//	ASRGetStartSafeIndex, ASRGetStartActualIndex and ASRGetEndSafeIndex
	virtual	int ASRGetEndActualIndex() = 0;

#endif // _ASR

//#########################################################################################################################


//#########################################################################################################################
//##		Basic Functions
//#########################################################################################################################

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Destructor.
	virtual ~CTIBase() {};

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Function used to initialize the Telephony engine.
	virtual void InitSystem() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Function used to terminate the Telephony engine.
	virtual void EndSystem() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Function used to end the Telephony engine.
	virtual void EndObj() = 0;

	// Parameters:
	//	VoiceResource	- [in] Boolean variable specifying the open voice device.
	//	lpcszProtocol	- [in] Pointer to a null-terminated string specifying the protocol.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to open a IP telephony channel.
	virtual int OpenChannelIP(bool VoiceResource, const char *lpcszProtocol = "H323") = 0;

	// Parameters:
	//	iInterface		- [in] Integer variable specifying the interface type.
	//	iSignaling		- [in] Integer variable specifying the signaling type.
	//	lpcszProtocol	- [in] Pointer to a null-terminated string specifying the protocol.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to open a telephony channel using the specified tecnology. The tecnology is determined by the iInterface, iSignaling and the lpszProtocol parameters. To see the possible values to these parameters check the DialogicDefines.h include file. The default parameter is lpszProtocol = "br_r2_io".
	virtual int OpenChannel( int iInterface, int iSignaling = 0, const char *lpcszProtocol = "br_r2_io" ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	This function is used to close a telephony channel.
	virtual void CloseChannel() = 0;

	// Parameters:
	//	pJmp	- [in] Pointer to a jmp_buf that marks the disconnection point.
	// Return Value:
	//	None.
	// Description:
	//	This function sets the disconnection point. After SetJump to be called, when a hangup occurs,
	//	the code execution is deviated to the point where SetJump was called.
	virtual void SetJump(jmp_buf *pJmp) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function answers an incoming call.
	virtual int Answer(bool bDoubleAnswer = false, bool bBilling = true) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function disconnects a call.
	virtual int Disconnect(int CauseDropCall = 0  /* (DROP_NORMAL) */ ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function disconnects the second call.
	virtual int Disconnect2() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	Pointer to a null-terminated string containing the ANI (caller) number.
	// Description:
	//	This function gets the ANI number on a incoming call.
	virtual const char *GetAni() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	Pointer to a null-terminated string containing the DNIS (calling) number.
	// Description:
	//	This function gets the DNIS number on a incoming call.
	virtual const char *GetDnis() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the PCM format.
	// Description:
	//	This function gets the PCM format from the Telephony engine. Possible formats are ULAW or ALAW.
	virtual int GetPCM() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	Function used to clear the prompt list.
	virtual int ClearPrompts() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the telephony channel.
	// Description:
	//	This function gets the telephony channel from the instance of CTI.
	virtual int GetChannel() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the Site number.
	// Description:
	//	This function gets the Site number.
	virtual int GetSite() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the Machine Number.
	// Description:
	//	This function gets the Machine.
	virtual int GetMachine() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the Direction of channel.
	// Description:
	//	This function gets the Direction of channel.
	virtual int GetDirection() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	This function unblocks the channel and prepares it to receive calls when the Telephony engine is initialized.
	virtual int InitChannel() = 0;

	// Parameters:
	//	lpcszFilename	- [in] Pointer to a null-terminated string specifying the prompt to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to add a prompt file to the prompt list.
	virtual int AddFile( const char *lpcszFilename ) = 0;

	// Parameters:
	//	buffer			- [in] Pointer to buffer in memory specifying the prompt to be played.
	//	buffersize	- [in] An unsigned long value specifying the buffer size.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to add a buffer representing a file in memory to the prompt list.
	virtual int AddBuffer( char *buffer, const unsigned long buffersize ) = 0;
	
	// Parameters:
	//	lpcszFilename		- [in] Pointer to a null-terminated string specifying the prompt file.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a prompt file synchronously.
	virtual int PlayFileEvent( const char *lpcszFilename ) = 0;

	// Parameters:
	//	lpcszText	- [in] Pointer to a null-terminated string specifying the text to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to add a text prompt to the prompt list. The text prompt passed will be converted into speech (text-to-speech conversion).
	virtual int AddTTS( const char *lpcszText ) = 0;

	// Parameters:
	//	lpcszText				- [in] Pointer to a null-terminated string specifying the text to be played.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play a text prompt synchronously. The text prompt passed is converted into speech (text-to-speech conversion).
	virtual int PlayTTS( const char *lpcszText ) = 0;

	// Parameters:
	//	lOffset					- [in] Long integer variable specifying the position in the file from where the playback will start (offset).
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to play all prompts added in the prompt list synchronously.
	virtual int Play( const long lOffset = 0 ) = 0;

	// Parameters:
	//	lpcszFilename			- [in] Pointer to a null-terminated string specifying the file.
	//	iMaxRecordTime		- [in] Integer variable specifying the maximum time to record.
	//	iMaxSilenceTime		- [in] Integer variable specifying the maximum of silence time to record.
	//	lpcszTermDigit		- [in] Pointer to a null-terminated string specifying the termination digit.
	//	bBeep							- [in] Boolean variable specifying if a beep will be played before the start of the recording.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to record speech in a file synchronously.
	virtual int RecordFile(const char *lpcszFilename, int iMaxRecordTime, int iMaxSilenceTime, bool bBeep, const char *lpcszTermDigit) = 0;

	// Parameters:
	//	lpcszFilename			- [in] Pointer to a null-terminated string specifying the file.
	//	iMaxRecordTime		- [in] Integer variable specifying the maximum time to record.
	//	iMaxSilenceTime		- [in] Integer variable specifying the maximum of silence time to record.
	//	lpcszTermDigit		- [in] Pointer to a null-terminated string specifying the termination digit.
	//	bBeep							- [in] Boolean variable specifying if a beep will be played before the start of the recording.
	//	SecondarySrc			- [in] Long variable specifying the secondary source of signal to record.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to record speech in a file synchronously.
	virtual int RecordFileFrom2Src(const char *Arquivo, const long& SecondarySrc, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar) = 0;

	// Parameters:
	//	buffer						- [out]	Pointer to a pointer of type std::string specifying the memory buffer.
	//	iMaxRecordTime		- [in]	Integer variable specifying the maximum time to record.
	//	iMaxSilenceTime		- [in]	Integer variable specifying the maximum of silence time to record.
	//	lpcszTermDigit		- [in]	Pointer to a null-terminated string specifying the termination digit.
	//	bBeep							- [in]	Boolean variable specifying if a beep will be played before the start of the recording.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to record speech in memory synchronously.
	virtual int RecordBuffer(std::string** buffer, int iMaxRecordTime, int iMaxSilenceTime, bool bBeep, const char *lpcszTermDigit) = 0;

	// Parameters:
	//	iNumDigits					- [in]	Integer variable specifying the number of digits the function will wait for input.
	//	iMaxInterdigitTime	- [in]	Integer variable specifying the maximum interdigit time.
	//	lpszDigitsBuffer		- [out]	Pointer to a null-terminated string buffer to receive the digits inputted.
	//	lpcszTermDigit			- [in]	Pointer to a null-terminated string specifying the termination digit.
	// Return Value:
	//	The return value is T_MAXDIG if the number of digits requested was inputted. If an interdigit timeout occurred the return value is T_INTERDIG. If the function fails the return value is negative. The symbols T_MAXDIG and T_INTERDIG can be found in the DialogicDefines.h.
	// Description:
	//	This function is used to get digits input synchronously.
	// Remarks:
	//	The parameter lpcszTermDigit can be NULL or an empty string ("") if the user input must not be interrupted by a termination key.
	virtual int GetDigits( int iNumDigits, int iMaxInterdigitTime, char *lpszDigitsBuffer, const char *lpcszTermDigit ) = 0;

	// Parameters:
	//	lpcszMask	- [in]	Pointer to a null-terminated string specifying the digit mask.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to set the digit mask restricting the digit input to a determined set of digits. Possible values are "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "#", "A", "B", "C", "D", "@". The "@" charactere indicates all digits.
	// Example:
	//<PRE>
	//	SetDigMask( "0123456789#" );
	//	SetDigMask( "@" );
	//</PRE>
	virtual void SetDigMask( const char *lpcszMask ) = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function is used to clear the digits input buffer.
	virtual int ClearDigits() = 0;

	// Parameters:
	//	None.
	// Return Value:
	//	The return value is a pointer to a null terminated string containing the last error message.
	// Description:
	//	This function is used to get the last error message.
	virtual const char *GetLastError() = 0;

	// Parameters: 
	// true - Enabled; false - Disabled
	// Return Digit Value:
	//	The return value is the last digit pressed
	// Description:
	//	This function is used to enable digit listening
	virtual int DigitQueue(bool bFlag) = 0;
//#########################################################################################################################



};

#endif


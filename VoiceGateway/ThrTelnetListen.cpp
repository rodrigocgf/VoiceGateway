#include "ThrTelnetListen.h"
#include "ISwitch.h"
#include "IConnection.h"
#include "ProcessTelnetRequest.h"
#include "RequestExec.hpp"
#include "GarbageCollector.hpp"

ThrTelnetListen::ThrTelnetListen(boost::asio::io_service & p_ioService , ISwitch * parent , boost::shared_ptr<Logger> pLog4cpp, const std::string & sTelnetPassword) :
    m_IoService(p_ioService), 
    m_pLog4cppStart(pLog4cpp),
    m_bStop(false),    
    m_Switch(parent),
    m_sTelnetPassword(sTelnetPassword)
{
    m_pLog4cppStart->debug("[%p][%s]", this, __FUNCTION__);

    m_connectedCallback = std::bind(&ThrTelnetListen::OnConnected, this, std::placeholders::_1);
    m_newlineCallback = std::bind(&ThrTelnetListen::OnNewLine,this , std::placeholders::_1, std::placeholders::_2);
    
    SetConnectedCallback(m_connectedCallback);                                
    SetNewLineCallback(m_newlineCallback);

    m_QueueTelnetThrPtr.reset( new boost::thread( RequestExec<ProcessTelnetRequest>( m_TelnetQMngr ) ) );
    m_GarbageCollector.reset(new GarbageCollector(m_pLog4cppStart));
}


ThrTelnetListen::~ThrTelnetListen(void)
{
    m_pLog4cppStart->debug("[%p][%s]", this, __FUNCTION__);

    
}

void ThrTelnetListen::SetConnectedCallback(FPTR_ConnectedCallback f) 
{ 
    m_connectedCallback = f; 
}

void ThrTelnetListen::SetNewLineCallback(FPTR_NewLineCallback f) 
{ 
    m_newlineCallback = f; 
}

void ThrTelnetListen::Start()
{
    m_pLog4cppStart->debug("[%p][%s]", this, __FUNCTION__);

    m_ListenThreadPtr.reset ( new boost::thread ( boost::bind (&ThrTelnetListen::Loop, this)));
    m_ListenThreadPtr->detach();
}

void ThrTelnetListen::Stop()
{
    m_pLog4cppStart->debug("[%p][%s]", this, __FUNCTION__);
    m_bStop = true;
    m_ListenThreadPtr->join();

    m_TelnetQMngr.stop();
    m_QueueTelnetThrPtr->join();

        
}

std::string ThrTelnetListen::GenerateTimerId()
{
	std::stringstream ss;
	boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
	std::string strTimeStamp;
	
	ss.str(std::string());
    ss <<   boost::format("%04i%02i%02i%02i%02i%02i%03i") % current_date_milliseconds.date().year() % 
			current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
			current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
			current_date_milliseconds.time_of_day().seconds() %
			(current_date_milliseconds.time_of_day().fractional_seconds() / 1000);

	strTimeStamp = ss.str();
	
	return strTimeStamp;
}

void ThrTelnetListen::AddToGarbageCollector(void * pObj)
{
    TelnetSession * pTelnetSession =  reinterpret_cast< TelnetSession *>( pObj );
    m_GarbageCollector->AddToGarbage<TelnetSession>( pTelnetSession );
}

void ThrTelnetListen::Loop()
{
    SOCKADDR_IN addr_tmp;
	int addr_len;
    TelnetSession * pTelnetSession;        
    std::string     timerId;
	m_pLog4cppStart->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
	    sock_LISTEN = socket(AF_INET,SOCK_STREAM,0);	 
	    DWORD dwReUse = 1;

        while( !m_bStop )
	    {

            if(sock_LISTEN != INVALID_SOCKET) 
            {
		        addr_LISTEN.sin_family = AF_INET;

		        unsigned short us_PORT_LISTEN;		
                us_PORT_LISTEN = uLocalScreenPort;

                addr_LISTEN.sin_port = htons(us_PORT_LISTEN);

                m_pLog4cppStart->debug("[%s] PORT LISTEN = %d", __FUNCTION__ , us_PORT_LISTEN);
		        addr_LISTEN.sin_addr.s_addr = htonl(INADDR_ANY);
                
		        if (::bind(sock_LISTEN,(LPSOCKADDR)&addr_LISTEN,sizeof(addr_LISTEN)) != SOCKET_ERROR)
                {
			        // aguarda conex�es gerando fila de at� 5
			        if ( listen(sock_LISTEN,5) != SOCKET_ERROR )
                    {
				        // fica em looping esperando por accept's
				        while( true ) 
                        {
					        addr_len = sizeof(addr_tmp);
					        sock_ACCEPTED = accept(sock_LISTEN,(SOCKADDR *)&addr_tmp,&addr_len);					
					        if (sock_ACCEPTED != INVALID_SOCKET ) 
                            {
                                
                                m_pLog4cppStart->debug("[%s] SOCKET accepted.", __FUNCTION__ );                                
                                
                                ProcessTelnetRequestPtr processTelnetRequestPtr( new ProcessTelnetRequest( this,  m_pLog4cppStart , 
                                                                                                            sock_ACCEPTED , m_sTelnetPassword) );
                                m_TelnetQMngr.enqueue( processTelnetRequestPtr );                                
                                                            
                            }

                            boost::this_thread::sleep_for (boost::chrono::milliseconds(1) );
                        }
                    } else {
                        m_pLog4cppStart->debug("[%s] Erro na fun��o listen", __FUNCTION__);				
                    }
                } else {
                    m_pLog4cppStart->debug("[%s] Erro na fun��o bind", __FUNCTION__);			
                }
            } else {
                m_pLog4cppStart->debug("[%s] Erro na cria��o do socket de listen.", __FUNCTION__);			
            }
        }

        close(sock_LISTEN);    
    }
    catch(...)
    {

    }
}


/*
void ThrTelnetListen::TimerCallback(TelnetSession * pTelnetSession)
{
    m_pLog4cppStart->debug("TimerCallback");
}
*/
void ThrTelnetListen::OnConnected(TelnetSession * session)
{    
    boost::shared_lock<boost::shared_mutex> lock(m_ListMutex);
    
    session->sendLine("*********************************************\r\n");
    session->sendLine("                                            *\r\n");
    session->sendLine("*  SUPPORTCOMM VOICEGATEWAY TELNET SERVER   *\r\n");
    session->sendLine("                                            *\r\n");
    session->sendLine("*********************************************\r\n");
    session->sendLine("> ");
}


void ThrTelnetListen::OnNewLine(TelnetSession * session, std::string line)
{   
    session->sendLine("> ");
}

ISwitch * ThrTelnetListen::GetSwitch() const
{
    return m_Switch;
}

bool ThrTelnetListen::ValidCircuit( const int ichannel )
{
	try {		
		
        if( !m_Switch->IsChannelEnabled(ichannel) )
		{
			return false;
		}
	
		return true;
	}
	catch(...){
		m_pLog4cppStart->debug("EXCEPTION: ValidCircuit()");
		return false;
	}
}

int ThrTelnetListen::StateCircuit( const int ichannel )
{
	char sTemp[256];
	
	try {

		sprintf(sTemp, "STOC %d", ichannel );		

		// Obtem o status dos canais
        
		return ( m_Switch->GetConnection()->Connection_ChannelStatus(ichannel) );
	}
	catch(...)
    {
		m_pLog4cppStart->debug("EXCEPTION: StateCircuit()");
		return -1;
	}
}

void ThrTelnetListen::DialOnCircuit( const int ichannel, char *DNIS, char *ANI)
{
    char sTemp[256] = {0};
	ST_CONNECTION st_connection;

	try
	{
		m_pLog4cppStart->debug("DialOnCircuit: %d Dnis: %s Ani: %s", ichannel, DNIS, ANI);

		sprintf(sTemp, "DNIS=%s&ANI=%s", DNIS, ANI);

		st_connection.Switch		=	0;
		st_connection.Machine		=	0;
		st_connection.Channel		=	ichannel;
		st_connection.Timeslot	    =   0;
		st_connection.Event			=   T_SW_DIAL_TELNET;
		st_connection.sData			=   sTemp;

		if( m_Switch->GetConnection()->Connection_Send(st_connection, T_SW_DIAL_TELNET) )
        {
			m_pLog4cppStart->debug("Error sending event");
		}
	}
	catch(...)
	{
		m_pLog4cppStart->debug("EXCEPTION: DialOnCircuit()");
	}
}

int ThrTelnetListen::DisconnectCircuit( const int ichannel1, const int ichannel2 )
{
	int i;
    char sTemp[256] = {0};
	ST_CONNECTION st_connection;

	try
	{

		sprintf(sTemp, "DISC %d %d", ichannel1, ichannel2 );
		//print_lg(sTemp);
		// desconecta a ligacao do canal
		
		for( i = ichannel1; i <= ichannel2; i++ )
		{
			st_connection.Switch		=	0;
			st_connection.Machine		=	0;
			st_connection.Channel		=	i;
			st_connection.Timeslot	= 0;
			st_connection.Event			= T_DISCONNECT;
			st_connection.sData.erase();

			m_Switch->GetConnection()->Connection_Send(st_connection, T_DISCONNECT);
		}

		return 0;

	}
	catch(...)
	{
		m_pLog4cppStart->debug("EXCEPTION: DisconnectCircuit()");
		return -1;
	}
}

int ThrTelnetListen::BlockCircuit( const int ichannel1, const int ichannel2 )
{
	int i;
    char sTemp[256] = {0};
	ST_CONNECTION st_connection;

	try {

		sprintf(sTemp, "BLKC %d %d", ichannel1, ichannel2 );
		
		// Bloqueia Feixe
		
		for(i = ichannel1; i <= ichannel2; i++)
        {
			if( !ValidCircuit(i) )
            {
				continue;
			}

			st_connection.Switch		=	0;
			st_connection.Machine		=	0;
			st_connection.Channel		=	i;
			st_connection.Timeslot	= 0;
			st_connection.Event			= T_USER_BLOCK;
			st_connection.sData.erase();

			m_Switch->GetConnection()->Connection_Send(st_connection, T_USER_BLOCK);
		}

		return 0;
	}
	catch(...)
    {
		m_pLog4cppStart->debug("EXCEPTION: BlockCircuit()");
		return -1;
	}
}

int ThrTelnetListen::UnBlockCircuit( const int ichannel1, const int ichannel2 )
{
	int i;
    char sTemp[256] = {0};
	ST_CONNECTION st_connection;
	
	try
	{
		sprintf(sTemp, "UNBC %d %d", ichannel1, ichannel2 );
		
		// Bloqueia Feixe
		
		for(i = ichannel1; i <= ichannel2; i++)
        {

			if( !ValidCircuit(i) )
            {
				continue;
			}

			st_connection.Switch		=	0;
			st_connection.Machine		=	0;
			st_connection.Channel		=	i;
			st_connection.Timeslot	= 0;
			st_connection.Event			= T_USER_UNBLOCK;
			st_connection.sData.erase();

			m_Switch->GetConnection()->Connection_Send(st_connection, T_USER_UNBLOCK);
		}

		return 0;

	}
	catch(...)
	{
		m_pLog4cppStart->debug("EXCEPTION: UnBlockCircuit()");
		return -1;
	}
}
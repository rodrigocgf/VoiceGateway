#pragma once

#include <string>
#include <windows.h>


using namespace std;


class CriticalSection
{
private:	
	// Variavel de controle da secao critica
	CRITICAL_SECTION Critical;
	// handle da thread que e dona da secao critica
	DWORD dwThreadOwnerId;
	// Guarda o ultimo erro
	string sError;
public:
    CriticalSection(void);
    ~CriticalSection(void);

    // Entra na secao critica
	void Enter(void);
	// Sai da secao critica
	void Leave(void);
	// Tenta entrar na secao critica
	bool TryEnter(void);
	// Pega o ultimo erro ocorrido
	const char *GetLastError(void);
};


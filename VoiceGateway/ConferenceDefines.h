/************************************************************************************************/
/* Support Comm Teleinformatica S/A																															*/
/*                                                                                              */
/* Modulo: ConferenceDefines.h																																	*/
/* Funcao: Defini��o da valores para os m�dulos da confer�ncia																	*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 05/NOV/2002 - Jairo Rosa - Criacao do Modulo																									*/
/*                                                                                              */
/************************************************************************************************/
#ifndef CONFERENCE_DEFINES_H

#define CONFERENCE_DEFINES_H

#define CONF_MAX_USERS			4

//C�DIGOS DE ERRO PARA CONFER�NCIA
#define	CONFERENCE_OK					0
#define	CONFERENCE_WARNING		1
#define	CONFERENCE_ERROR			-2
#define	CONFERENCE_EXCEPTION	0xFFFF

#endif

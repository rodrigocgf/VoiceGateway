#pragma once

#include <functional>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/locks.hpp>


template< typename T>
class Worker 
{
public:
    Worker(T * s) : pool(s) { }
    void operator()(){

        std::function<void()> task;
        while(true)
        {
            {   // acquire lock
                boost::mutex::scoped_lock lock(pool->queue_mutex);
             
                // look for a work item
                while(!pool->stop && pool->tasks.empty())
                { // if there are none wait for notification
                    pool->condition.wait(lock);
                }
 
                if(pool->stop) // exit if the pool is stopped
                    return;
 
                // get the task from the queue
                task = pool->tasks.front();
                pool->tasks.pop_front();
 
            }   // release lock
 
            // execute the task
            task();
        }
    }
private:
    T * pool;
};


  
// the actual thread pool
class ThreadPool 
{
public:
    ThreadPool(size_t threads) :   stop(false)
    {
        for(size_t i = 0 ; i < threads ; ++i )
            workers.push_back(boost::thread(Worker<ThreadPool>(this)));
    }

    template<class F>
    void enqueue(F f)
    {
        { // acquire lock
            boost::mutex::scoped_lock lock(queue_mutex);            
         
            // add the task
            tasks.push_back(std::function<void()>(f));
        } // release lock
     
        // wake up one thread
        condition.notify_one();
    }

    ~ThreadPool()
    {
        // stop all threads
        stop = true;
        condition.notify_all();
     
        // join them
        for(size_t i = 0;i<workers.size();++i)
            workers[i].join();
    }
private:
    friend class Worker<ThreadPool>;
 
    // need to keep track of threads so we can join them
    std::vector< boost::thread > workers;
 
    // the task queue
    std::deque< std::function<void()> > tasks;
 
    // synchronization
    boost::mutex queue_mutex;
    boost::condition_variable condition;
    bool stop;
};
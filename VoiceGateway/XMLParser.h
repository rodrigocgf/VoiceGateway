
#ifndef _XMLPARSER_H_
#define _XMLPARSER_H_

#include <stdio.h>
#include <string>
#include <pugixml.hpp>
#include <iostream>

using namespace std;
using namespace pugi;

class XmlParser  
{
public:
    // Contrutor default
	XmlParser(){};
	// Destrutor default
	~XmlParser(){};

    xml_document doc;
    xml_parse_result parseResult;

	// Guarda o ultimo erro ocorrido
	string sLastError;
	// Guarda o documento XML
	string sXml;
	// Guarda a string para ser anallisada		
	string sXpath;
	// Buffer Auxiliar
	char sAux[4096];

	// Retorna quantos atributos existem para um filho
	int AttributeCount(string sObject, int &iCount);
	// Retorna quantos atributos existem para um filho
	int AttributeCount(const char *sObject, int &iCount);
	// Retorna quantos filhos existem para um elemento
	int ChildrenCount(string sObject, int &iCount);
	// Retorna quantos filhos existem para um elemento
	int ChildrenCount(const char *sObject, int &iCount);
	// Retorna o valor de um atributo de acordo com o seu index
	int AttributeValue(string sObject, string &sValue, int Index);
	// Retorna o valor de um atributo de acordo com o seu nome
	int AttributeValue(const char *sObject, const char *szAttributeName, string &sValue);
	// Retorna o nome de um atributo de acordo com o seu index
	int AttributeName(string sObject, string &sName, int Index);
	// Retorna o valor de um elemento
	int ElementValue(const char *sObject, string &sValue);
	// Retorna o valor de um elemento de acordo com o seu indice
	int ElementValue(const char *sObject, int Index, string &sValue);
	// Retorna o ultimo erro ocorrido
	const char *ReturnLastError(void);
	// Executa o parser do documento XML
	int Parser(string sXmlDocument);
		
};

#define OK                       0
#define SELECT_ATTRIBUTE_ERROR  -1
#define SELECT_ELEMENT_ERROR    -2
#define ATTRIBUTE_COUNT_ERROR   -3
#define CHILDREN_COUNT_ERROR    -4
#define ATTRIBUTE_VALUE_ERROR   -5

#endif //_XMLPARSER_H_

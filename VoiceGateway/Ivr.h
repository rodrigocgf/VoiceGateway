#pragma once

#include "StdIncludes.h"
#include "SwitchDefines.h"
#include "SwitchTables.h"
#include "ISwitch.h"
#include "ChannelStat.h"
#include "Tapi.h"
#include <lua.hpp>

#include "Logger.h"

#define MAX_STRING_LENGTH_LUA_LOG   100000

// Indice do ImageListIcons
#define STATUS_BASE								  0
#define	ICON_IDLEIN		              STATUS_BASE+0
#define	ICON_IDLEOUT  		          STATUS_BASE+1
#define	ICON_INACTIVE 		          STATUS_BASE+2
#define	ICON_OFFERED  		          STATUS_BASE+3
#define	ICON_CONNECTED		          STATUS_BASE+4
#define	ICON_ANSWERED		            STATUS_BASE+5
#define	ICON_HANGUP   		          STATUS_BASE+6
#define	ICON_DROPCALL 		          STATUS_BASE+7
#define	ICON_BLOCKED  		          STATUS_BASE+8   // Blocked
#define	ICON_ALERTING 		          STATUS_BASE+9
#define	ICON_DIALING 		            STATUS_BASE+10
#define	ICON_RECORDING		          STATUS_BASE+11
#define	ICON_RESERVED		            STATUS_BASE+12
#define	ICON_LOCKED			            STATUS_BASE+13
#define	ICON_UNUSED1		            STATUS_BASE+14  // Reservado para uso futuro
#define	ICON_UNUSED2		            STATUS_BASE+15  // Reservado para uso futuro
#define	ICON_UNUSED3                STATUS_BASE+16  // Reservado para uso futuro
#define	ICON_UNUSED4                STATUS_BASE+17  // Reservado para uso futuro
#define	ICON_BOARDENABLED           STATUS_BASE+18  // Placa Enabled
#define	ICON_BOARDDISABLED          STATUS_BASE+19  // Placa Disabled
#define	ICON_INFORMATION            STATUS_BASE+20  // Icone de informacao
#define	ICON_NETWORK                STATUS_BASE+21  // Rede
#define	ICON_LOG                    STATUS_BASE+22  // Log
#define	ICON_TRUNK                  STATUS_BASE+23  // Tronco
#define	ICON_RESOURCE               STATUS_BASE+24  // Recurso


// Strings de estados dos canais
#define	STR_IDLE     		"Idle"
#define	STR_BLOCKED  		"Blocked"
#define	STR_OFFERED  		"Offered"
#define	STR_CONNECTED		"Connected"
#define	STR_ANSWERED		"Answered"
#define	STR_HANGUP   		"Hangup"
#define	STR_DROPCALL 		"Dropcall"
#define	STR_LINE_OUT 		"Inactive"
#define	STR_ALERTING 		"Alerting"
#define	STR_DIALING 		"Dialing"
#define	STR_RECORDING		"Recording"
#define	STR_RESERVED		"Reserved"
#define	STR_LOCKED			"Locked"

#define EVENT_TIMEOUT		3


enum LOG_LEVELS {

	LOG_ERROR				= 1,		// Mensagens de erro
	LOG_TCP_IP			= 2,		// Mensagens de pacotes TCP_IP
	LOG_INFO				= 3,		// Mensagens de informacoes gerais
	LOG_DB_RESULT		= 4,		// Resultados de query do banco de dados
	LOG_SCRIPT			= 5,		// Log de entrada de cada funcao do Script Lua
	LOG_FUNCTIONS		= 6,		// Log de entrada de cada funcao
	LOG_CTI					= 7,		// Log de cada evento CTI (IVR e Estacao)
	LOG_ALL					= 8			// loga tudo
};

#define EVENT_TIMEOUT		3

// Retornos para a funcao GetDialExStatus()
#define STEX_DIALING						0
#define STEX_ALERTING						1
#define	STEX_BUSY							2
#define STEX_CONGESTION						3
#define STEX_REMOTE_HANGUP					4
#define STEX_LOCAL_HANGUP					5
#define STEX_NOANSWER						6
#define STEX_TIMEOUT						7


// Nome da variavel do script que contera a informacao passada na transferencia
#define	SCRIPT_INFO_VAR_NAME					"_TRANSFER_INFO"

// Numero maximo de estados possiveis para ao IVR
#define	MAX_STATE											128

// Estrutura de noh de conexao informando as caracteristicas do outro canal conectado

typedef struct {

	int	Switch;											// Numero da switch remota
	int	Machine;										// Numero da maquina remota
	int	Channel;										// Numero do canal remoto
	int Event;											// Evento enviado para validacao do mesmo
	long Timeslot;									    // Timeslot remoto SCBUS ou ATM
	std::string sData;									// Dados vindos do pacote TCP_IP

} ST_CONNECTION;



// Tipo de estrutura para guardar os dados do Detalhe do CDR
typedef struct 
{

	int iEvento;
	string sDataInicio;
	string sDataFim;

} ST_CDR_DETAIL;

namespace Exemplification 
{
    //using uint8_t = unsigned char; // C++17
    const uint8_t bufferSize = 16;
}

// Tipo de fila para os dados dos eventos de detalhe do CDR
typedef queue <int, list<ST_CDR_DETAIL> > QUEUE_CDR_DETAIL;

typedef map <int, HANDLE> MAP_INT_HANDLE;

typedef map <std::string, bool> MAP_STRING_BOOL;

// Fila de dados dos eventos das conexoes
typedef std::queue <int, list<ST_CONNECTION> > QUEUE_ST_CONNECTION;

// Fila de dados dos eventos das conexoes
typedef std::queue<int, list<ST_CONNECTION> > QUEUE_ST_CONNECTION_MONITOR;

// Tipo para map de queues de resposta para a conexao TCP/IP 
typedef map < int, QUEUE_ST_CONNECTION_MONITOR > MAP_CONNECTION_MONITOR;

// Tipo para map de conexoes
typedef std::map < int, QUEUE_ST_CONNECTION > MAP_CONNECTION;

// Map com para o record set do LuaDB
typedef map < int , map < int, string > > MAP_RECORD_SET;

class Ivr
{
private:
    boost::condition            			m_cond;	
    volatile bool               			m_stop;   
    boost::mutex                			m_Mutex;

    boost::shared_ptr<boost::thread>    m_ThrPtr;
    //boost::shared_ptr<Tapi>             m_TapiPtr;
    Tapi *                              m_pTapi;
    void Thr();    

    ST_CONNECTION	Connection;					// Estrutura usada como variavel temporaria para as operacoes de conexao
	ST_CONNECTION	Connection_local;		// Estrutura de informacoes da conexao do canal local
	ST_CONNECTION	Connection_remote;	// Estrutura de informacoes da conexao do canal remoto
	ST_CONNECTION	Connection_PA;			// Estrutura que guarda as informacoes da estacao que esta conectada com este canal
	ST_CONNECTION	Connection_OUT;			// Estrutura de informacoes da conexao do canal de saida conectado a este canal de entrada
	ST_CONNECTION	Connection_IN;			// Estrutura de informacoes da conexao do canal de entrada conectado a este canal de saida

    MAP_STRING_BOOL MapDnis;					// Map com os numeros de B que sao de tamanho menor que os do banco de dados
    // Map com os semaforos para eventos que nao podem ser enviados pelo device de telefonia
	MAP_INT_HANDLE MapSemaphore;
    QUEUE_CDR_DETAIL QueueCdrDetail;	// Fila de eventos dos detalhes do CDR
    MAP_RECORD_SET MapRecordSet;			// Map com para o record set do LuaDB

    int BoardTrunk;									// Tronco referente a placa
    int BoardPort;									// Canal referente a placa
    int IdBoard;										// Numero da placa
    int IdTrunk;										// Tronco referente ao Banco de Dados
    int Channel;										// Canal referente ao banco de dados
    int IdAudio;										// Id da linha do audio do Spyline

    bool FlagRecordUser;						// Indica se o usuario sera gravado ou nao (dialogo) 
    bool FlagAnswer;								// Flag que indica se a ligacao ja foi atendida
    bool FlagBlocked;								// Flag que indica se o canal esta bloqueado
    bool FlagHangup;								// Flag que indica se a ligacao ja foi desligada
    bool FlagTranfer;								// Flag que indica se uma ligacao foi transferida

    long FilePosToPlayHoldMusic;		// Posicao do arquivo de musica de espera a partir da qual sera tocada
    int IdCallFrom;									// 1 = Ligacao Interna, 2 = Ligacao externa
    int	iDialResult;								// Guarda o status do DialOut	
    int iCallDirection;										// Sentido da ligacao INBOUND ou OUTBOUND

    bool ScriptAnswer;							// Verdadeiro se o script que vai atender a ligacao	
    bool bDoubleAnswer;							// Bloqueia ou ligacoes a cobrar fazendo duplo atendimento
    int Category;										// Categoria do Chamador

    char Ani[128];									// Numero de A
	char Dnis[128];									// Numero de B
    char ServiceName[40];						    // Nome do servico
    std::string sInboundCallId;					    // Guarda o CallID da chamada de entrada quando
    std::string DbIndexConnection;                  // Indice da conex�o

    int HangupReason;								// 1 = usuario desligado pelo script
													// 2 = usuario desligado pela posicao de atendimento
													// 3 = usuario desligou
    char sDateTime[64];							    // Data atual com padrao de banco de dados
	char sIniDateOfCall[64];				        // Data de inicio da ligacao
	char sEndDateOfCall[64];				        // Data de termino da ligacao
    long TimeOfCall;								// Tempo em segundos da ligacao
    char CallId[40];								// ID da chamada corrente
    //char sInfo[2000];								// String para uso da funcao Print()

    bool bSSConference;                             // Variavel para identificar o Sigle Step Conference
    bool FlagScriptHangup;					        // Flag que indica se a ligacao ja foi desligada pelo script
    lua_State * LuaState;						    // Estrutura para o contexto do script Lua
    int EstadoIVR;									// Armazena o estado atual do Ivr setado pela funcao GoState()
        
    int                                     RemoteAudioPort;
    CTIBase *                               m_pCti;
    //boost::shared_ptr<CTIBase>              m_pCtiPtr;
    ISwitch *                               m_pSwitch;
    
    _ThreadParam *                          ThreadParam;

    // LOG4CPP
	//log4cpp::Category * 					m_pLog4cppStart;	
    

    boost::shared_ptr<Logger>               m_pLog4cpp;
    std::string                             m_sBasePathLog;
    int                                     m_iLogMaxFileSize;
    

    long TxTimeSlot;								// Timeslot de transmissao
    long TxTimeSlotVox;							    // Timeslot de Voz
    long TxGlobalTimeSlot;                          // Timeslot de Exporta��o Global, utilizado no Multi-Chassis
	long TxTimeSlotPrivate;					        // Timeslot da conferencia privada

	long TimeSlotConference;				        // Timeslot da confer�ncia
	long TimeSlotAudio;							    // Timeslot do audio
    long lCreateTimeDnisConfig;			            // Time NULL da ultima criacao do arquivo DNIS.INI

    
    int OpenChannel();
    
    void InitPortToAnswer();
    void PrintIvrInfo();
    
    void ExecLuaScript(const char *ScriptName);
    void GetDatetime(char *DateTime);
    
    void ReadDnisConfig();
    
    
    void T_Disconnect();
    void T_Call();
    void T_Blocked(void);
    void T_DropCall(void);
    void T_Idle(void);
    void T_Connect(void);
    void Block(void);
    void UnBlock(void);

    
    void InitInboundCall();
    int DbSetPortStatus(int Status, int Port);
    int PlayBusyToneAsync();
    void ClearConn(ST_CONNECTION &st_connection);
    int DbReadCallId();
    bool CheckFileDate(char *sNomeArq, long &lTimeLastChange);
    void ClearCdrDetail();
    void CloseChannel(void);
    void LongJmpDisconnect(void);
    
    // Essa funcao eh executada no canal que executou a requisicao de chamada (SAIDA)
	void Dial_Connected(void);
    // Essa funcao eh executada no canal que executou a requisicao de chamada (SAIDA)
	void DialOutEx(void);
        
    // Insere dados em tabela de recpcao de FAX
    int DbInsertRecvFax(int &iDep, char *sFaxName, int &iId);
    // Atualiza dados em tabela de recpcao de FAX
    int DbUpdateRecvFax(int &iId, int &iStatus);    
    // Retorna um ponteiro para o objeto LuaXmlParser para uso no LUA

    int DbGetDialPort(int Route, int &Switch, int &Machine, int &Port, string &sDnis, string &sAni);
    void GetInfoConnection(int Event);
    void GetInfoConnection(void);
    int DbCdr( const char * sAppName , const char * sCdrInfo);
    int GetDataPacket(const char *datapacket, char *cCommand, long &iValue);

    int ConfereeType;
	std::string ConferenceId;
public:    
    //Ivr(ISwitch *p_Switch, void *param , const std::string & sBasePathLog );
    Ivr(ISwitch *p_Switch, void *param , const std::string & sBasePathLog , int iLogMaxFileSize );
    ~Ivr(void);

    enum IVR_STATES
	{
		DIAL_CONNECTED = 0						// Processa eventos no canal de saida depois de ter efetuado uma ligacao
	};

    // Matriz de ponteiros de funcoes para as funcoes de estado do IVR
	void (Ivr::*FuncState[MAX_STATE])();
    void LongJmpBlocked(); // Implemented to close CDR before exiting.
    std::string MountCDRClose(const std::string & _callId);
    std::string ConnectionParam(std::string sParam, char *parser = ":");

    CTIBase * GetCTI() const;
    void Start();
    void Stop();

    void Log(int iLevel, const char *sformat, ...);    
    void StopLog();

    // Essa funcao eh executada no canal que requisita a chamada (ENTRADA)
    int DialEx(const char *sDestinationNumber, const char *sOriginationNumber, const char *sTimeoutDial);

    // Essa funcao eh executada no canal que requisitou a chamada para pegar
	// status da chamada de A e C (ENTRADA) ou para esvaziar a fila se semaforos
	int GetDialExStatus(bool bClearSemaphores = false);
    int SendFax(const char *sFile, const char *sDisplay);
    int RecvFax(const char *sFile, const char *sDisplay);

    // Fun��o para setar a variavel bSSConference para identificar Sigle Step Conference da Avaya
    void SetSSConferece(bool bFlag);
    bool GetSSConferece(void) const;
    void SetIconStatus(int Status);
    void Print(int Col, const char *sformat, ...);

    int DbGetResource();

    void Hangup();
    // Essa funcao eh executada no canal que requisita a chamada (ENTRADA)
	// e serve para desligar o canal que efetuou a ligacao
	void HangupEx(void);

    // Seta o flag de hangup ocorrido enquanto estava no script
	void SetScriptHangupFlag(bool value);

    // Guarda um evento na fila de detalhes de CDR
    void PushCdrDetail(int iEvento, string sDataInicio, string sDataFim);

    int GetLogCDREnabled(void);
    void LuaIniciaCdr(void);
    void LuaPrintStatus(const char *sStatus);
    void LuaPrintAni(const char *sStatus);
    void LuaPrintDnis(const char *sStatus);
    void LuaPrintBits(const char *sStatus);
    
    int AnswerCall(bool bDoubleAnswer, bool bBilling);
    ISwitch * GetSwitch();
    jmp_buf jmp_state;								// guarda o ambiente para os estados do IVR
    jmp_buf jmp_cti;								// guarda o ambiente para ao ponto de ligacao com a classe CTIBase
    //boost::shared_ptr<Tapi> GetTapi();
    Tapi * GetTapi();
    void EndScript();

    int UnListenGlobalAudio(void);
    int ListenGlobalAudio(long TimeslotRing);
    int GetGlobalTimeSlot(void);

    int RemoteAudioPlayFile( const char *play, const char *terminator, long& timeslotvox );
    int RemoteAudioStop();
    int RecordFileWithMusic( const char *record, const char *play, int timeout, int silence_timeout, const char *terminator );

    int GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize);
    int GetDataPacket(const char *datapacket, char *cCommand, string &sValue);
    int GetDataPacket(const char *datapacket, char *cCommand, int &iValue);
    char * ReturnAni(void) const;
    char * ReturnDnis(void) const;
    int ReturnCategory(void);
    int ListenConnection(void);
    int WaitEvent(void);
    void GoState(int State);
    void GoEvents(void);
    void GoEndThread(void);
    void InitOutboundCall(void);
    const std::string & GetDbIndexConnection();
    void SetDbIndexConnection(const std::string & value);

    void PushRecordSet(int iRow, int iCol, string &sData);
    void PopRecordSet(int iRow, int iCol, string &sData);
    void ClearRecordSet(void);
    std::string GetCallId();
    void GenerateCallID();
    int DbCallDetail(int Evento, char *sDataInicio, char *sDataFim);
    int DbHoliday(int iDia, int iMes, int iAno, int &iResult);

    void SendIvrEvent(int iChannel);
    bool SendConnection(int Event, const std::string & sData);
    bool SendConnection(int Event);

    void ProcessScriptReturn(void);

    // Conference functions
    int ConferenceAdd( const char* lpcszConferenceId, const int& iConferenceSize, const int& iConfereeType, const bool& bNotifyTone );
    int ConferenceRemove( const char* lpcszConferenceId, const int& iConfereeType );
    int ConferenceChange( const char* lpcszConferenceIdSource, const char* lpcszConferenceIdDestination, const int& iConferenceSize, const int& iConfereeType, const bool& bNotifyTone );
    int ConferenceListen( const char* lpcszConferenceId );
    int ConferenceUnlisten( const char* lpcszConferenceId );
    int ConferenceGetConfereeId( const char* lpcszConferenceId, const char* lpcszANI );
    int ConferenceChangeStatus( const char* lpcszConferenceId, const int& iConfereeType, const int& iUnListen);

    void DialFromTelnet(void);
};


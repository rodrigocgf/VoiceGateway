#include "Logger.h"


Logger::Logger(const std::string & p_sBasePathLog, int p_iMaxFileSize, const std::string & p_sLogName , int index  , bool bCheckNewDay ) : 
    m_sBasePathLog(p_sBasePathLog) , 
    m_iLogMaxFileSize(p_iMaxFileSize) ,
    m_sLogName(p_sLogName) ,
    m_bCheckNewDay(bCheckNewDay) ,
    m_Index( index )
    
{
    MountLogPath();    
    ConfigLog4cpp();
}


Logger::~Logger(void)
{
}

void Logger::MountLogPath()
{
    std::stringstream ss;
    boost::filesystem::path fsBasePathLog(m_sBasePathLog);

    boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);

	ss.str(std::string());
	ss << boost::format("%04i") % current_date_milliseconds.date().year();
    m_CurrentYear = ss.str();
	boost::filesystem::path YearPath ( ss.str() );

	ss.str(std::string());
	ss << boost::format("%02i") % current_date_milliseconds.date().month().as_number();
    m_CurrentMonth = ss.str();
	boost::filesystem::path MonthPath ( ss.str() );

	ss.str(std::string());
	ss << boost::format("%02i") % current_date_milliseconds.date().day();
    m_CurrentDay = ss.str();
	boost::filesystem::path DayPath( ss.str() );

	boost::filesystem::path  LogBaseYearPath;
	boost::filesystem::path  LogBaseYearMonthPath;
	boost::filesystem::path  LogBaseYearMonthDayPath;
	
    LogBaseYearPath = fsBasePathLog / YearPath;
	LogBaseYearMonthPath = fsBasePathLog / YearPath / MonthPath;
	LogBaseYearMonthDayPath = fsBasePathLog / YearPath / MonthPath / DayPath;
	
    if ( !boost::filesystem::exists( fsBasePathLog ) )
		boost::filesystem::create_directory( fsBasePathLog );
		
	if ( !boost::filesystem::exists( LogBaseYearPath ) )
		boost::filesystem::create_directory( LogBaseYearPath );
		
	if ( !boost::filesystem::exists( LogBaseYearMonthPath ) )
		boost::filesystem::create_directory( LogBaseYearMonthPath );
		
	if ( !boost::filesystem::exists( LogBaseYearMonthDayPath ) )
		boost::filesystem::create_directory( LogBaseYearMonthDayPath );
    
    m_sFullLogPathName = LogBaseYearMonthDayPath.string();
}

/*
void Logger::ConfigLog4cpp()
{
    
    unsigned int maxBackupIndex = 5;
    bool append = true;
    mode_t mode = 420;
    std::stringstream ss;

    ss << boost::format("%s.log") % m_sLogName;

    try
    {   

        boost::filesystem::path fsFullLogFilePath;
        boost::filesystem::path  LogBaseYearMonthDayPath(m_sFullLogPathName);
        boost::filesystem::path  fsLogName(ss.str());
    
        fsFullLogFilePath = LogBaseYearMonthDayPath / fsLogName;    

        m_layoutPtr.reset( new log4cpp::PatternLayout() );
        m_rfileAppenderPtr.reset( new log4cpp::RollingFileAppender(     m_sLogName, 
                                                                        std::string(fsFullLogFilePath.string()), m_iLogMaxFileSize,
                                                                        maxBackupIndex, append, mode ));

        m_category = &log4cpp::Category::getRoot().getInstance( m_sLogName );

        if ( m_layoutPtr.get() != NULL)
        {
            try
            {            
                log4cpp::PatternLayout * patternLayout = (log4cpp::PatternLayout * )(m_layoutPtr.get());                
                patternLayout->setConversionPattern( "%d{%Y-%m-%d %H:%M:%S,%l} %x : %m%n");               
            
                
                m_rfileAppenderPtr->setLayout(m_layoutPtr.get());
                m_category->removeAllAppenders();
                m_category->addAppender(m_rfileAppenderPtr.get());
                m_category->setAdditivity(true);

                try
                {
                    m_category->setPriority(log4cpp::Priority::getPriorityValue("DEBUG"));
                }
                catch(std::invalid_argument &ia)
                {
                    std::cerr << "Invalid Priority: "  << " DEBUG " << std::endl;
                    m_category->setPriority(log4cpp::Priority::INFO);
                }
            }
            catch(log4cpp::ConfigureFailure &cf)
            {
                std::cerr << cf.what() << std::endl;
            }
        }
        else
        {
            std::cerr << "Cannot initialize PatternLayout" << std::endl;        
        }

        m_pLog4cpp = &m_category->getInstance( m_sLogName );
    }
    catch (log4cpp::ConfigureFailure e) 
    {
        std::cout<<"Log4cpp Error: "<<e.what()<< std::endl;
    }
    catch (...) 
    {
        std::cout<< "Error at Logger::ConfigLog4cpp()." << std::endl;
    }
}
*/
void Logger::ConfigLog4cpp()
{
    
    unsigned int maxBackupIndex = 5;
    bool append = true;
    //mode_t mode = 420;
    std::stringstream ss;

    ss << boost::format("%s.log") % m_sLogName;

    try
    {   

        boost::filesystem::path fsFullLogFilePath;
        boost::filesystem::path  LogBaseYearMonthDayPath(m_sFullLogPathName);
        boost::filesystem::path  fsLogName(ss.str());
    
        fsFullLogFilePath = LogBaseYearMonthDayPath / fsLogName;  

        mp_OutFile = fopen(std::string(fsFullLogFilePath.string()).c_str(),"a+");
        m_FileName = fsFullLogFilePath.string();

        //patternLayout->setConversionPattern( "%d{%Y-%m-%d %H:%M:%S,%l} %x : %m%n");               

    }    
    catch (...) 
    {
        std::cout<< "Error at Logger::ConfigLog4cpp()." << std::endl;
    }
}

std::string Logger::GetLayoutPattern()
{
	std::stringstream ss;
	boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
	std::string strTimeStamp;
	
	ss.str(std::string());
    ss <<   boost::format("%04i-%02i-%02i %02i:%02i:%02i.%03i : ") % current_date_milliseconds.date().year() % 
			current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
			current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
			current_date_milliseconds.time_of_day().seconds() %
			(current_date_milliseconds.time_of_day().fractional_seconds() / 1000);

	strTimeStamp = ss.str();
	
	return strTimeStamp;
}

bool Logger::IsNewDay()
{
    bool bRet = false;
    std::stringstream ss;
    std::string                             l_CurrentDay;
    std::string                             l_CurrentMonth;
    std::string                             l_CurrentYear;

    boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
    long milliseconds = timeLocal.time_of_day().total_milliseconds();

	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
    
	ss.str(std::string());
	ss << boost::format("%02i") % current_date_milliseconds.date().day();
    l_CurrentDay = ss.str();

    if ( stricmp( l_CurrentDay.c_str() , m_CurrentDay.c_str() ) )
        bRet = true;
	
    return bRet;
}

void Logger::debug(const char *sformat, ...)
{

    std::stringstream ss;
    char szMsg[ MAX_CHAR_MESSAGE_TEXT + 1 ];

    memset(szMsg,0x00,sizeof(szMsg));
	va_start( va, sformat );
	_vsnprintf( szMsg, MAX_CHAR_MESSAGE_TEXT + 1, sformat, va );
	va_end( va );
	
    
    if ( m_bCheckNewDay )
    {
        if ( IsNewDay() )
        {
            MountLogPath();

            fflush(mp_OutFile);   
            fclose(mp_OutFile);

            ConfigLog4cpp();            
        }
    }
    fwrite(GetLayoutPattern().c_str(), sizeof(char) , strlen(GetLayoutPattern().c_str()) , mp_OutFile );
    fwrite(szMsg, sizeof(char), strlen(szMsg) , mp_OutFile );
    fwrite("\r\n", sizeof(char), 2 , mp_OutFile );
    fflush(mp_OutFile);   

    //m_pLog4cpp->debug(szMsg);
}


void Logger::error(const char *sformat, ...)
{

    char szMsg[ MAX_CHAR_MESSAGE_TEXT + 1 ];

    memset(szMsg,0x00,sizeof(szMsg));
	va_start( va, sformat );
	_vsnprintf( szMsg, MAX_CHAR_MESSAGE_TEXT + 1, sformat, va );
	va_end( va );
	
    
    if ( m_bCheckNewDay )
    {
        if ( IsNewDay() )
        {
            MountLogPath();

            fflush(mp_OutFile);   
            fclose(mp_OutFile);

            ConfigLog4cpp();
        }
    }
    
    fwrite(GetLayoutPattern().c_str(), sizeof(char) , strlen(GetLayoutPattern().c_str()) , mp_OutFile );
    fwrite(szMsg, sizeof(char), strlen(szMsg) , mp_OutFile );
    fwrite("\r\n", sizeof(char), 2 , mp_OutFile );
    fflush(mp_OutFile); 

    //m_pLog4cpp->error(szMsg);
}

void Logger::StopLog()
{    
    
}
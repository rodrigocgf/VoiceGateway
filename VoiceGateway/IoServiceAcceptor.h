#pragma once

#include "StdIncludes.h"
#include "StatusScreenNet.h"
#include "ISwitch.h"
//#include "GarbageCollector.hpp"
#include "IIoServiceAcceptor.h"

class IoServiceAcceptor : public IIoServiceAcceptor
{
private:
    // Porta da tela remota
    static const int uRemoteScreenPort = 13131;

    // Porta p/ recepcao UDP da tela remota
    static const int  uLocalScreenPort = 13132;

    // LOG4CPP
	log4cpp::Category *                 m_pLog4cppStart;	
    bool                                m_bStop;
    ISwitch *                           m_Switch;

    boost::shared_ptr<boost::thread>    m_ioServiceThrPtr;
    boost::asio::io_service             m_IoService;
    boost::asio::ip::tcp::acceptor      m_Acceptor;	
    //boost::shared_ptr<GarbageCollector> m_GarbageCollector;

    boost::mutex               			m_ListMutex;
    std::list<StatusScreenNet *>        m_StatusScreenList;
    
    
    void AcceptAsync();
    //void AcceptSync();
    //void HandleAccept( boost::shared_ptr<StatusScreenNetAsync> paramPtr, ISwitch * p_Switch, log4cpp::Category * pLog4cpp, boost::system::error_code const& Error );
    void HandleAccept( StatusScreenNet * paramPtr, ISwitch * p_Switch, log4cpp::Category * pLog4cpp, boost::system::error_code const& Error );
    
    void HandleAccept( );
    void IoServiceThread();
public:
    //IoServiceAcceptor(ISwitch * parent , log4cpp::Category * pLog4cpp, boost::shared_ptr<GarbageCollector> p_GarbageCollector );
    IoServiceAcceptor(ISwitch * parent , log4cpp::Category * pLog4cpp );
    ~IoServiceAcceptor(void);
    
    ISwitch * GetSwitch() const;
    void LogScreen(int ListIndex, int Item, char *s);
    void SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg);
    void SendMessageToAllScreens( std::string sMessage);
    //void AddToGarbage(StatusScreenNet * p_Obj);
    void Start();
    void Stop();

};


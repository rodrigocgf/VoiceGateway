#include "ThrListen.h"


ThrListen::ThrListen( ISwitch * parent, boost::shared_ptr<Logger> pLog4cpp ) :
    m_pLog4cppStart(pLog4cpp),
    m_bStop(false),    
    m_Switch(parent)
{
    
}

ThrListen::~ThrListen(void)
{

}

void ThrListen::Start()
{
    m_ListenThreadPtr.reset ( new boost::thread ( boost::bind (&ThrListen::Loop, this)));
    m_ListenThreadPtr->detach();	
}

void ThrListen::Stop()
{
    m_pLog4cppStart->debug("[%s]",__FUNCTION__);
    m_bStop = true;
    m_ListenThreadPtr->join();	
}

void ThrListen::Loop()
{
    SOCKADDR_IN addr_tmp;
	int addr_len;

	m_pLog4cppStart->debug("[%s]",__FUNCTION__);

    try
    {
	    sock_LISTEN = socket(AF_INET,SOCK_STREAM,0);	 
	    DWORD dwReUse = 1;

        while( !m_bStop )
	    {

            if(sock_LISTEN != INVALID_SOCKET) 
            {
		        addr_LISTEN.sin_family = AF_INET;

		        unsigned short us_PORT_LISTEN;		
                us_PORT_LISTEN = uLocalScreenPort;

                addr_LISTEN.sin_port = htons(us_PORT_LISTEN);

                m_pLog4cppStart->debug("[%s] PORT LISTEN = %d", __FUNCTION__ , us_PORT_LISTEN);
		        addr_LISTEN.sin_addr.s_addr = htonl(INADDR_ANY);		
		
		        if (bind(sock_LISTEN,(LPSOCKADDR)&addr_LISTEN,sizeof(addr_LISTEN)) != SOCKET_ERROR) 
                {
			        // aguarda conex�es gerando fila de at� 5
			        if ( listen(sock_LISTEN,5) != SOCKET_ERROR ) 
                    {
				        // fica em looping esperando por accept's
				        while( true ) 
                        {
					        addr_len = sizeof(addr_tmp);
					        sock_ACCEPTED = accept(sock_LISTEN,(SOCKADDR *)&addr_tmp,&addr_len);					
					        if (sock_ACCEPTED != INVALID_SOCKET ) 
                            {
                                p_StatusScreen = new StatusScreenNet(sock_ACCEPTED , m_Switch, this , m_pLog4cppStart);

                                {
                                    // TO DO => MAPPING
                                    boost::mutex::scoped_lock lk(  m_ListMutex );
                                    m_StatusScreenList.push_back(p_StatusScreen);
                                }
                            }
                        }

                    } else {
                        m_pLog4cppStart->debug("[%s] Erro na fun��o listen", __FUNCTION__);				
                    }
                } else {
                    m_pLog4cppStart->debug("[%s] Erro na fun��o bind", __FUNCTION__);			
                }
            } else {
                m_pLog4cppStart->debug("[%s] Erro na cria��o do socket de listen.", __FUNCTION__);			
            }

            //boost::this_thread::sleep(boost::posix_time::seconds(1));
        }

        close(sock_LISTEN);    
    }
    catch(...)
    {

    }
}

ISwitch * ThrListen::GetSwitch() const
{
    return m_Switch;
}

void ThrListen::LogScreen(int ListIndex, int Item, char *s)
{
    std::stringstream ss;

	try
	{
        if ( m_Switch->GetFlagLogScreen() < 2 )        		
		{
			return;
		}

        ss << boost::format( "SS_LOG_BOARD?ID=%d&CHANNEL=%d&TEXT=%s\r\n") % ListIndex % Item % s;
		SendMessageToAllScreens( ss.str() );
	}
	catch(...)
	{
	}
}

void ThrListen::SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg)
{
    std::stringstream ss;
    
    try
	{
        if ( m_Switch->GetFlagLogScreen() <= 0 )        		
		{
			return;
		}

		switch(Grid)
		{
			case 1:
				//Win::SendMessageToScreen( "SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n", ListIndex, Trunk, Item, sMsg );
				break;
			case 2:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&TYPE=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 3:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&DNIS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 4:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&ANI=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 5:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&INFO=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
			case 6:
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&BITS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
				SendMessageToAllScreens( ss.str() );
				break;
		}
	}
	catch(...)
	{
	}
}

void ThrListen::SendMessageToAllScreens( std::string sMessage)
{
    boost::mutex::scoped_lock lk(  m_ListMutex );
        
    std::list<StatusScreenNet *>::iterator it;
    for (it = m_StatusScreenList.begin(); it != m_StatusScreenList.end(); ++it)
    {
        StatusScreenNet * pStatusScreen = (*it);
        
        if ( (pStatusScreen != NULL) && (pStatusScreen->m_bStop == false ) )
            pStatusScreen->EnqueueWrite(sMessage);            
    
    }
}


#ifndef _INCLUDED_KHOMP_SCLUAAPI_H_
#define _INCLUDED_KHOMP_SCLUAAPI_H_

struct lua_State;
class Ivr;

typedef struct LUA_USERDATA
{
	class Ivr *IVR;
} LUA_USERDATA;

int lua_ExecFile( Ivr * p_Ivr , char const* Filename );
//char const* lua_scGetErrorMessage( );

#endif
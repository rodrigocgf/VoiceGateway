/*********************************************************************************************
	SwitchTables.h
*********************************************************************************************/

#ifndef SwitchTablesH
	#define SwitchTablesH

// Tabela TCtiBoardType
#define	BOARD_D41E					1			// Analog board (LSI+VOX) 4 Channels
#define	BOARD_D160					2			// Analog board (LSI+VOX) 16 Channels
#define	BOARD_MSI80					3			// Conference Board with 8 stations                  
#define	BOARD_MSI160				4			// Conference Board with 16 stations                 
#define	BOARD_MSI240				5			// Conference Board with 24 stations                 
#define	BOARD_D300					6			// 1 E1 (DTI+VOX)                                    
#define	BOARD_D600_JCT_1E1	        7			// 1 E1 (DTI+VOX+CSP)
#define	BOARD_D240					8			// 1 T1 (DTI+VOX)                                    
#define	BOARD_D120					9			// Analog board (LSI+VOX) 12 Channels
#define	BOARD_CP6					10		// Gammalink Fax 6 channels
#define	BOARD_CP12					11		// Gammalink Fax 12 channels
#define	BOARD_CPI3000				12		// Fax Board 30 channels
#define	BOARD_VFX40					13		// Dialogic Fax 4 channels
#define	BOARD_PCCS6					14		// DataKinetics PCCS6 SS7
#define	BOARD_DM_IP301			    15		// DM3 IPLink 30 Channels IP (H323) + E1 + Voice
#define	BOARD_DM_IP300			    16		// DM3 IPLink 30 Channels IP (H323)

#define	BOARD_DM_V					17		// DM3 30 Channels (DTI + VOX/CSP)

#ifdef _CONFERENCE
#define	BOARD_DCB_320				18		// DCB 1 DSP - 32 resources
#define	BOARD_DCB_640				19		// DCB 2 DSP - 64 resources
#define	BOARD_DCB_960				20		// DCB 3 DSP - 96 resources
#define	BOARD_AMTELCO_CONF	        22		// AMTELCO CONFERENCE BOARD 4 DSP - 256 resources
#define	BOARD_DMV600A_CONF	        23		// DM/V-A Multifunctional Series board - 60 resources
#endif

#define	BOARD_DMV_600A			    21		// DM3 60 Channels (DTI + VOX/CSP)


// Tabela TCtiSignaling
#define	SIG_R2_DIGITAL				1
#define	SIG_GLOBAL_CALL				2
#define	SIG_ISDN					3		// (BACK TO BACK)
#define	SIG_LOOP_START				4
#define	SIG_STATION					5
#define	SIG_LINE_SIDE				6
#define	SIG_GLOBAL_CALL_SS7		    7
#define	SIG_GLOBAL_CALL_IP		    8
#define SIG_LOOP_START_TRAP		    9
//<Antonio>
#define	SIG_R2_DIGITAL_2			10
//</Antonio>
#define	SIG_GLOBAL_CALL_DM3		    11
#define	SIG_ISDN_4ESS				12
#define	SIG_ISDN_NTT				13
#define	SIG_ISDN_QSIG				14
#define	SIG_ISDN_CTR4				15
#define	SIG_ISDN_DMS				16
#define	SIG_RESOURCE				17

enum TCtiPortStatus {

	PORT_ANSWER_CALL			    =	'A',			
	PORT_BLOCKED					=	'B',
	PORT_CONNECTED				    =	'C',
	PORT_DIALING					=	'D',
	PORT_FREE_AVAILABLE		        =	'F',
	PORT_STATION_OFFHOOK	        =	'H',
	PORT_INACTIVE					=	'I',
	PORT_LOCKED						=   'L',
	PORT_NOT_INIT					=	'N',
	PORT_STATION_ONHOOK		        =   'O',
	PORT_TRAPPING					=   'P',
	PORT_RECEIVING_FAX		        =   'R',
	PORT_SENDING_FAX			    =   'T',
	PORT_IVR_ON_RECORDER	        =   'V',
	PORT_RESERVED					=   'E'
};


enum DialResults {

	DIAL_OUT_CONNECTED = 1,
	DIAL_OUT_NOANSWER,
	DIAL_OUT_BUSY,
	DIAL_OUT_CONGESTION,
	DIAL_OUT_NO_LINES
};


#endif


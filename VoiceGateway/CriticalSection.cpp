#include "CriticalSection.h"


CriticalSection::CriticalSection(void)
{
    InitializeCriticalSection(&Critical);
}


CriticalSection::~CriticalSection(void)
{
    DeleteCriticalSection(&Critical);
}


// Entra na secao critica
void CriticalSection::Enter(void)
{
    EnterCriticalSection(&Critical);
}

// Sai da secao critica
void CriticalSection::Leave(void)
{
    LeaveCriticalSection(&Critical);
}

// Tenta entrar na secao critica
bool CriticalSection::TryEnter(void)
{
    bool bRet = false;
    try
    {
        EnterCriticalSection(&Critical);
        bRet = true;
    }
    catch(...)
    {

    }
    return bRet;
}

// Pega o ultimo erro ocorrido
const char *CriticalSection::GetLastError(void)
{
    return sError.c_str();
}
#ifndef Lua_DB_h
#define Lua_DB_h


#include <lua.hpp>


void Lua_DbLibOpen( lua_State *L );
void Lua_DbLibClose( lua_State *L );

#endif
#ifndef _ISWITCH_
#define _ISWITCH_

class Cdr;
class ThrListen;
class Ivr;
class Tapi;
class IConnection;
class Class_TS_Manager;
class ConferenceManagerClass;

class ISwitch
{
public:
    virtual int GetBoardId(int p_Channel) const = 0;
    virtual int GetTrunkId(int p_Channel) const = 0;
    virtual int GetTrunkChannelID(int p_Channel) const = 0;
    virtual int GetBoardTrunkID(int p_Channel) const = 0;
    virtual const std::string & GetFullLogPathName() const = 0;
    virtual int GetSwitch() const = 0;
    virtual int GetMachine() const = 0;
    virtual int GetIndexScreen() const = 0;    
    virtual void SetIndexScreen(int value) = 0;
    virtual std::string GetManufacturerName(int p_Channel) const = 0;
    virtual std::string GetTrunkSignalingName(int p_TrunkId) const = 0;
    virtual int GetTrunkSignaling(int p_TrunkId) const = 0;
    virtual std::string GetTrunkProtocol(int p_TrunkId) const = 0;
    virtual std::string GetBasePathLog() const = 0;
    virtual int GetIdAudio() const = 0;
    virtual void SetAudioTimeslots_Channel(const int & p_IdAudio , const int & p_iChannel) = 0;
    virtual void SetAudioTimeslots_LocalTimeSlot(const int & p_IdAudio , const int & p_iLocalTimeSlot) = 0;
    virtual void SetAudioTimeslots_GlobalTimeSlot(const int & p_IdAudio , const int & p_iGlobalTimeSlot) = 0;
    virtual int GetChannelsReserverd(int p_Channel) const = 0;
    virtual int GetChannelsStatus(int p_Channel)  = 0;
    virtual void SetChannelsStatus(int p_Channel, const int & p_Status) = 0;
    virtual bool GetFlagEndProgram() const = 0;
    virtual int GetFlagLogScreen() const = 0;
    virtual std::string GetPathPrompts() const = 0;
    virtual int GetR2MinDnis() const = 0;
    virtual int GetR2MaxDnis() const = 0;
    virtual int GetLogCDR() const = 0;
    virtual std::string DbGetCallId(std::string sQuery) = 0;
    virtual std::string GetPathScripts() const = 0;
    virtual void SetFaxFont(const std::string & p_sFaxFont) = 0;
    virtual int GetTimeoutDial() = 0;
    virtual void SetFaxCharSize(int value) = 0;
    virtual void SetFaxLineWidth(int value) = 0;
    virtual char GetChannelsDirection(int index) = 0;
    virtual int GetNumPortsInDataBase() = 0;
    virtual int GetMaxIvrDbConnection() = 0;
    virtual int GetImportCdrDelay() = 0;
    virtual int GetNumBoardsInDataBase() = 0;
    virtual boost::shared_ptr<Cdr> GetCdr() = 0;
    virtual std::string GetLocalIpAddress() = 0;
    virtual std::string GetOdbcDataSourceName() const = 0;
    virtual std::string GetOdbcDataBase() const = 0;
    virtual std::string GetOdbcUser() const = 0;
    virtual std::string GetOdbcPassWord() const = 0;
    virtual std::string GetBoardResourceTypeName(int p_board, int p_resource) = 0;
    virtual int GetTrunkCount(int p_board) = 0;
    virtual int GetResourceCount(int p_board) = 0;
    virtual int GetChannelCount(int p_board) = 0;    
    virtual int GetTrunksRef(int p_trunkId) = 0;
    virtual int GetTrunkType(int p_trunkId) = 0;    
    virtual bool GetIPResource(int p_boardId) = 0;
    virtual std::string GetIPProtocol(int p_boardId) = 0;
    virtual bool IsChannelEnabled(int p_channel) = 0;
    virtual int GetBoardResourceCount(int p_board , int p_resource) = 0;
    virtual std::string GetBoardName(int p_board) = 0;
    virtual int WinStatusChannel( int Channel, std::string& ani, std::string& dnis ) = 0;
    virtual const char* StatusChannel(int Status) = 0;
    virtual ThrListen * GetThrListen() const = 0;
    virtual boost::shared_ptr<Tapi> GetTapi() = 0;
    virtual Ivr * GetIvr(int p_channel) = 0;
    virtual WORD GetIdTypeApp() = 0;
    virtual bool ValidCircuit( const int ichannel ) = 0;
    virtual boost::shared_ptr<IConnection> GetConnection() = 0;
    virtual std::string GetAppName() = 0;
    virtual Class_TS_Manager * GetTS_Manager() = 0;
    virtual ConferenceManagerClass * GetConferenceManager() = 0;
};

#endif
#pragma once

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/shared_ptr.hpp>
#include <assert.h>


template<typename T>
class QueueManager 
{
public:
	typedef boost::shared_ptr<T>	DataTypePtr;
	typedef std::deque<DataTypePtr> QueueType;
		
	QueueManager()
	:m_stop(false)
	{
	}

	void enqueue(DataTypePtr & data)
	{
		{
			boost::mutex::scoped_lock lk( m_mutex );
			m_queue.push_back(data);
		}
		m_cond.notify_one();
	}
		
	void stop()
	{
		m_stop = true;
		m_cond.notify_all();
	}

	void operator()()
	{
		while ( !m_stop )
		{
			boost::mutex::scoped_lock lk(  m_mutex );
			while ( m_queue.empty() && !m_stop )
				m_cond.wait( lk );
				
			if ( !m_stop)
			{
				assert( lk.owns_lock() );
				assert( m_queue.size() );

				DataTypePtr spExec = m_queue.front();
				assert( spExec.get() );
				m_queue.pop_front();
				lk.unlock();
				(*spExec)();
			}
		}
	}
private:
	QueueType			m_queue;
	boost::mutex		m_mutex;
	boost::condition	m_cond;
	volatile bool		m_stop;
};

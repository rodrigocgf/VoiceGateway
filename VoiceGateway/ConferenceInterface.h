/************************************************************************************************/
/* Support Comm Teleinformatica S/A																															*/
/*                                                                                              */
/* Modulo: ConferenceInterface.h																																*/
/* Funcao: Defini��o da interface para uso de cada tecnologia																		*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato - Criacao do Modulo                                               */
/* 28/ABR/2003 - Jairo Rosa - Suporte para DMV 600A                                             */
/* 22/JUL/2003 - Jairo Rosa - Nova conf privada para DMV 600A e outras                          */
/*                                                                                              */
/************************************************************************************************/
#ifndef CONFERENCE_INTERFACE_H

#define CONFERENCE_INTERFACE_H

// Description:
//	A enumerated type specifying the conferee type. DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach), CP_CONF, (Chat Personalidade Conferencista - hears monitor timeslot), CP_MONI (Chat Personalidade Monitor - add monitor in conference)
typedef enum
{
    DUPLEX,
    MONITOR,
    COACH,
    PUPIL,
    PRIVATE,
    CP_CONF,
    CP_MONI,
    CP_VOX,
    CP_PERS,
    CP_GHOST
} conference_conferee_type;

class ConferenceInterfaceClass
{
public:
	// Parameters:
	//	None.
	// Return Value:
	//	None.
	// Description:
	//	Destructor.
	virtual ~ConferenceInterfaceClass() {};
	// Parameters:
	//	iBoardId	- [in] An integer value specifying the board identifier.
	//	cbtBoardType	- [in] An enumerated type value specifying the board type.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function opens a board device given its identifier.
	// See Also:
	//	CloseBoard.
	virtual int OpenBoard( const int& iBoardId, const char* sBoardName ) = 0;
	// Parameters:
	//	iBoardId	- [in] An integer value specifying the board identifier. The default value is iBoardId = 0.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function closes a board device previously openned.
	// See Also:
	//	OpenBoard.
	virtual int CloseBoard( const int& iBoardId = 0 ) = 0;
	// Parameters:
	//	lpcszConferenceId		- [in] An char point value identifying the conference.
	//	lTimeSlot				- [in] An long value identifying the timeslot for conferee in the conference.
  //	iConferenceSize 	- [in] An integer value specifying the number of conferees (parties) of the conference. The default value is iNumberOfConferees = 4.
	//	cctConfereeType	- [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach). Default value: DUPLEX.
	//	bNotifyTone			- [in] A boolean value specifying if the conferee is a supervisor or not. The default value is bNotifyTone = false.
	// Return Value:
	//	The return value is zero if the function succeeds, 1 if the conference has already reached the full capacity, negative if the function fails.
	// Description:
	//	This function adds an user to a previously created conference.
	// See Also:
	//	Remove, cctConfereeType.
  virtual int Add( const char * lpcszConferenceId, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot ) = 0;
	// Parameters:
	//	lpcszConferenceId		- [in] An char point value identifying the conference.
	//	lTimeSlot			- [in] An long value identifying the timeslot for conferee in the conference.
	//	cctConfereeType	- [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach). Default value: DUPLEX.
	// Return Value:
	//	The return value is zero if the function succeeds or non-zero if the function fails.
	// Description:
	//	This function removes an user from a conference.
	// See Also:
	//	Add.
	virtual int Remove( const char * lpcszConferenceId, long& lTimeSlot, conference_conferee_type cctConfereeType ) = 0;
  // Parameters:
	//	lpcszConferenceIdSource				- [in] An char point value identifying the conference where the conferee is.
	//	lpcszConferenceIdDestination	- [in] An char point value identifying the conference where the conferee is going.
	//	lTimeSlot											- [in] An long value identifying the timeslot for conferee in the conference.
	//	iConferenceSize 							- [in] An integer value specifying the number of conferees (parties) of the conference. The default value is iNumberOfConferees = 4.
  //	cctConfereeType								- [in] A enumerated type value specifying the conferee type in the conference. This value can be: DUPLEX (normal mode), MONITOR (receive-only mode), COACH (heard by pupil only), PUPIL (hears everyone including the coach).
	//	bNotifyTone										- [in] A boolean value specifying if the conferee is a supervisor or not. The default value is bNotifyTone = false.
	// Return Value:
	//	The return value is zero if the function succeeds, 1 if the conference has already reached the full capacity, CONF_ERROR if the function fails.
	// Description:
	//	This function moves an user from a given conference to another previously created conference.
	// See Also:
	//	Add, Remove, cctConfereeType.
  virtual int Change( const char * lpcszConferenceIdSource, const char * lpcszConferenceIdDestination, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot ) = 0;
	// Parameters:
	//	None.
	// Return Value:
	//	The return value is the conference identifier if the function succeeds or negative if the function fails.
	// Description:
	//	This function returns the conference identifier from a conference with less conferees.
	// See Also:
	//	Add, Remove.	
	virtual int SetConfereeAttribute( const char * lpcszConferenceId, const long& lTimeSlot, conference_conferee_type cctConfereeType = DUPLEX ) = 0;
  // Parameters:
	//	lpcszConferenceId	- [in] An char point value identifying the conference.
	//	lTimeSlot			- [in] An long value identifying the timeslot for conferee in the conference.
	// Return Value:
	//	The return value is timeslot if the function succeeds or negative if the function fails.
	// Description:
	//	This function returns the output timeslot for conferee in the conference.
  virtual int GetConferenceTimeSlot( const char * lpcszConferenceId, long& lTimeSlot ) = 0;
  // Parameters:
	//	lpcszConferenceId			- [in] An char point value identifying the conference.
	//	lChairPersonTimeSlot	- [in] An long value will receive the timeslot for chair person in the conference.
	// Return Value:
	//	The return value is timeslot if the function succeeds or negative if the function fails.
	// Description:
	//	This function returns the output timeslot for conferee in the conference.
  virtual int GetChairPersonTimeSlot( const char * lpcszConferenceId, long& lChairPersonTimeSlot, conference_conferee_type cctConfereeType = DUPLEX ) = 0;
	// Parameters:
	//	None.
	// Return Value:
	//	The return value is pointer to a null-terminated string specifying the last error message.
	// Description:
	//	This function returns the last error message.
	virtual const char* GetLastError() = 0;
};

#endif // CONFERENCE_INTERFACE_H

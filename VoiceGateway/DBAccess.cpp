#include "DBAccess.h"


//DBAccess::DBAccess(log4cpp::Category * pLog4cpp) : m_pLog4cpp(pLog4cpp)
DBAccess::DBAccess()
{   
    std::stringstream ss;

    try
    {  
        std::string sFullLogPathName;                

        //sFullLogPathName = MountLogPath(m_sBaseLogPath);        
        //ConfigLog4cpp(sFullLogPathName);

        ss << boost::format("[%s]") % __FUNCTION__;
        Log(ss.str());
        

        if( FAILED( ::CoInitialize( NULL ) ) )
        {
            bsLastErrorMessage = "Error initializing the COM subsystem.";
            return;
        }
        bsLastErrorMessage = "";
        m_pConnection = NULL;
        pRecordSet = NULL;
        
        CHECK_HRESULT( m_pConnection.CreateInstance( __uuidof( Connection ) ) );   
        ss.str(std::string());
        ss << boost::format("[%s] Connection OK") % __FUNCTION__;
        Log(ss.str());
        
        CHECK_HRESULT( pRecordSet.CreateInstance( __uuidof( Recordset ) ) );
        ss.str(std::string());
        ss << boost::format("[%s] RecordSet OK") % __FUNCTION__;
        Log(ss.str());
        

        if ( m_pConnection != NULL )
        {
            ss.str(std::string());
            ss << boost::format("[%s] m_pConnection is NOT NULL.") % __FUNCTION__;
            Log(ss.str());
        }

        // Change the cursor location to client-side.
        pRecordSet->CursorLocation = adUseClient;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
    }
    catch(...)
    {
        bsLastErrorMessage = "Exception in DBAccess().";
    }
}

DBAccess::~DBAccess()
{
    std::stringstream ss;

    try
    {
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());
        
        ::CoUninitialize();
    }
    catch(...)
    {        
        bsLastErrorMessage = "Exception in ~DBAccess().";
        ss.str(std::string());
        ss << boost::format("%s") % bsLastErrorMessage.c_str();
        Log(ss.str());
    }
}

const char* DBAccess::GetLastError()
{
    std::stringstream ss;

    try
    {
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());
        
        if( bsLastErrorMessage.c_str() != "" )
            return bsLastErrorMessage.c_str();
        else
            return "";
    }
    catch(...)
    {
        return "";
    }
}

void DBAccess::GetProviderError( )
{
    std::stringstream ss;

    try
    {
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());
        
        // Get Provider Errors from m_pConnection object.
        // pErr is a record object in the m_pConnection's Error collection.
        ErrorPtr pErr = NULL;
        std::strstream buffer;
        if ( m_pConnection == NULL )
            buffer << "m_pConnection = NULL !!! ";
        else 
        {
            if( ( m_pConnection->Errors->Count ) > 0 )
            {
                // Collection ranges from 0 to nCount -1.
                for( long i = 0; i < m_pConnection->Errors->Count; i++ )
                {
                    pErr = m_pConnection->Errors->GetItem( i );
                    //if ( pErr->Description != NULL )
                    if ( SysStringLen(pErr->Description) > 0 )
                        buffer << boost::format("Error Description(Number) : %s(#%d)") % (LPCSTR) pErr->Description % pErr->Number;
                    else
                        buffer << boost::format("Error Number : %l") %  pErr->Number;
                }
            }
        }

        buffer << std::ends;
        bsLastErrorMessage = buffer.str();

        ss.str(std::string());
        ss << boost::format("%s") % bsLastErrorMessage.c_str();

        Log( ss.str() );        
    }
    catch(...)
    {
        bsLastErrorMessage = "Exception in GetProviderError().";
    }
}

void DBAccess::GetComError( _com_error &e )
{
    try
    {
        std::strstream buffer;

        _bstr_t bstrSource(e.Source());
        _bstr_t bstrDescription(e.Description());

        if( ( !bstrSource ) && ( !bstrDescription ) )
        {
            buffer << "Error in ADO: " << "Code = " << e.Error() << ", Code meaning = " << e.ErrorMessage() << ".";
        }
        else if( !bstrSource )
        {
            buffer << "Error in ADO: " << "Code = " << e.Error() << ", Code meaning = " << e.ErrorMessage() << ", Description = " << (LPCSTR) bstrDescription;
        }
        else if( !bstrDescription )
        {
            buffer << "Error in ADO: " << "Code = " << e.Error() << ", Code meaning = " << e.ErrorMessage() << ", Source = " << (LPCSTR) bstrSource << ".";
        }
        else
        {
            buffer << "Error in ADO: " << "Code = " << e.Error() << ", Code meaning = " << e.ErrorMessage() << ", Source = " << (LPCSTR) bstrSource << ", Description = " << (LPCSTR) bstrDescription;
        }

        // Get Com errors.
        buffer << std::ends;
        bsLastErrorMessage = buffer.str();
    }
    catch(...)
    {
        bsLastErrorMessage = "Exception in GetComError().";
    }
}

int DBAccess::Open( const char* connection_string )
{
    std::stringstream ss;

    try
    {
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());

        if( !connection_string && ( SysStringLen(bsConnectionString) == 0 ) )
        {
            bsLastErrorMessage = "Error in ADO: Null connection string.";
            return -1;
        }

        if( connection_string )
        {
            bsConnectionString = connection_string;
        }

        /*
        // Check if the connection is closed.
        if( m_pConnection->State != adStateClosed )
            bsLastErrorMessage = "Connection is Closed !";
            return 1;
        */

        // Set Connection timeout in seconds.
        m_pConnection->PutConnectionTimeout(3);        

        // Open connection.
        CHECK_HRESULT( m_pConnection->Open( bsConnectionString, "", "", adConnectUnspecified ) );        
        return 0;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        //GetComError( e );
        return 0xFFFFFFFF;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::Close()
{
    std::stringstream ss;

    try
    {        
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());
        
        // Check if the connection is closed.
        if( m_pConnection->State == adStateClosed )
            return 1;

        // Close connection.
        CHECK_HRESULT( m_pConnection->Close() );
        return 0;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return 0xFFFFFFFF;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::Execute( const char* command )
{
    std::stringstream ss;

    try
    {        
        ss << boost::format("%s") % __FUNCTION__;
        Log(ss.str());

        
        long lRowsAffected = 0;
        _RecordsetPtr pNextRecordSet;

        if( !command || ( command == "" ) )
        {
            bsLastErrorMessage = "Error in Execute(). Invalid command.";
            return -1;
        }

        // Check if the record set is closed.
        if( pRecordSet->State != adStateClosed )
            CHECK_HRESULT( pRecordSet->Close() );

        // Open the record set.
        CHECK_HRESULT( pRecordSet->Open( command, _variant_t( ( IDispatch * ) m_pConnection, true ), adOpenForwardOnly, adLockReadOnly, adCmdText ) );
        //CHECK_HRESULT( pRecordSet->Open( command, _variant_t( ( IDispatch * ) m_pConnection, true ), adOpenStatic, adLockReadOnly, adCmdText ) );
        //CHECK_HRESULT( pRecordSet->Open( command, _variant_t( ( IDispatch * ) m_pConnection, true ), adOpenStatic, adLockOptimistic, adCmdText ) );

        // Pula p/ o ultimo record set.
        while( ( pNextRecordSet = pRecordSet->NextRecordset( (VARIANT *) lRowsAffected ) ) != NULL )
        {
            pRecordSet = pNextRecordSet;
        }

        return 0;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return -1;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::MoveFirst()
{    

    try
    {        
        if( pRecordSet->BOF && pRecordSet->EndOfFile )
        {
            // Nao ha resultados.
            return 1;
        }
        if( pRecordSet->BOF )
        {
            // Cursor ja esta apontando p/ o comeco dos resultados.
            return 2;
        }
        // Aponta p/ o primeiro registro.
        CHECK_HRESULT( pRecordSet->MoveFirst() );
        return 0;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return -1;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::MoveNext()
{    

    try
    {        
        if( pRecordSet->BOF && pRecordSet->EndOfFile )
        {
            // Nao ha resultados.
            return 1;
        }
        if( pRecordSet->EndOfFile )
        {
            // Cursor esta apontando p/ o final dos resultados.
            return 2;
        }
        // Aponta p/ o proximo registro.
        CHECK_HRESULT( pRecordSet->MoveNext() );
        return 0;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return -1;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

const char* DBAccess::GetItem( const std::string& item )
{    

    try
    {        
        _variant_t value;
        if( pRecordSet->State == adStateClosed )
        {
            // Record set fechado indicando q nao ha resultados.
            return NULL;
        }
        if( pRecordSet->BOF || pRecordSet->EndOfFile )
        {
            // Nao esta apontando p/ um item valido.
            return NULL;
        }
        // Obtem o conteudo do item de um registro.
        value = pRecordSet->Fields->GetItem( item.c_str() )->Value;
        switch( value.vt )
        {
        case VT_NULL:
            return "";
        case VT_BOOL:
            if( !value.boolVal )
                return "0";
            else
                return "1";
        default:
            bstrValue = value;
            return (LPCSTR) bstrValue;
        }
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return NULL;
    }
    catch(...)
    {
        return NULL;
    }
}

const char* DBAccess::GetItem( const long& index )
{    

    try
    {        
        _variant_t value;
        if( pRecordSet->State == adStateClosed )
        {
            // Record set fechado indicando q nao ha resultados.
            return NULL;
        }
        if( pRecordSet->BOF || pRecordSet->EndOfFile )
        {
            // Nao esta apontando p/ um item valido.
            return NULL;
        }
        // Obtem o conteudo do item de um registro.
        value = pRecordSet->Fields->GetItem( index )->Value;
        switch( value.vt )
        {
        case VT_NULL:
            return "";
        case VT_BOOL:
            if( !value.boolVal )
                return "0";
            else
                return "1";
        default:
            bstrValue = value;
            return (LPCSTR) bstrValue;
        }
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return NULL;
    }
    catch(...)
    {
        return NULL;
    }
}

int DBAccess::GetRowCount()
{    

    try
    {        
        if( pRecordSet->State == adStateClosed )
        {
            // Record set fechado indicando q nao ha resultados.
            return 0;
        }
        if( pRecordSet->BOF && pRecordSet->EndOfFile )
        {
            // Nao ha resultados.
            return 0;
        }
        // Retorna o numero de registros no record set.
        return pRecordSet->RecordCount;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return -1;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::GetColCount()
{    

    try
    {        
        if( pRecordSet->State == adStateClosed )
        {
            // Record set fechado indicando q nao ha resultados.
            return 0;
        }
        if( pRecordSet->BOF && pRecordSet->EndOfFile )
        {
            // Nao ha resultados.
            return 0;
        }
        // Retorna o numero de colunas de um registro no record set.
        return pRecordSet->Fields->Count;
    }
    catch( _com_error& e )
    {
        GetProviderError();        
        GetComError( e );
        return -1;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

int DBAccess::TrimAll( std::string& item )
{
    try
    {
        int iIndex;
        if( item.c_str() == "" )
        {
            // String invalida.
            return -1;
        }

        // Limpa espacos, tabs, CRs, LFs a esquerda.
        iIndex = item.find_first_not_of( " \t\n\r" );
        if( ( iIndex != -1 ) && ( iIndex != 0 ) )
        {
            item.erase( 0, iIndex );
        }

        // Limpa espacos, tabs, CRs, LFs a direita.
        iIndex = item.find_last_not_of( " \t\n\r" );
        if( ( iIndex != -1 ) && ( iIndex != ( item.size() - 1 ) ) )
        {
            item.erase( iIndex + 1, item.size() - iIndex );
        }
        return 0;
    }
    catch(...)
    {
        return 0xFFFFFFFF;
    }
}

std::string DBAccess::MountLogPath(const std::string & p_sBasePathLog)
{
    std::stringstream ss;    
    boost::filesystem::path fsFullLogFilePath;
    boost::filesystem::path fsBasePathLog(p_sBasePathLog);

    boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);

	ss.str(std::string());
	ss << boost::format("%04i") % current_date_milliseconds.date().year();
	boost::filesystem::path YearPath ( ss.str() );

	ss.str(std::string());
	ss << boost::format("%02i") % current_date_milliseconds.date().month().as_number();
	boost::filesystem::path MonthPath ( ss.str() );

	ss.str(std::string());
	ss << boost::format("%02i") % current_date_milliseconds.date().day();
	boost::filesystem::path DayPath( ss.str() );

	boost::filesystem::path  LogBaseYearPath;
	boost::filesystem::path  LogBaseYearMonthPath;
	boost::filesystem::path  LogBaseYearMonthDayPath;
	
    LogBaseYearPath = fsBasePathLog / YearPath;
	LogBaseYearMonthPath = fsBasePathLog / YearPath / MonthPath;
	LogBaseYearMonthDayPath = fsBasePathLog / YearPath / MonthPath / DayPath;
	
    if ( !boost::filesystem::exists( fsBasePathLog ) )
		boost::filesystem::create_directory( fsBasePathLog );
		
	if ( !boost::filesystem::exists( LogBaseYearPath ) )
		boost::filesystem::create_directory( LogBaseYearPath );
		
	if ( !boost::filesystem::exists( LogBaseYearMonthPath ) )
		boost::filesystem::create_directory( LogBaseYearMonthPath );
		
	if ( !boost::filesystem::exists( LogBaseYearMonthDayPath ) )
		boost::filesystem::create_directory( LogBaseYearMonthDayPath );

    boost::filesystem::path  fsLogName("Log_GW.log");
    fsFullLogFilePath = LogBaseYearMonthDayPath / fsLogName;

    return fsFullLogFilePath.string();


}

void DBAccess::ConfigLog4cpp(const std::string & p_sFullLogFileName )
{    
    size_t maxFileSize = 10000000;
    unsigned int maxBackupIndex = 5;
    bool append = true;
    mode_t mode = 420;
        
    m_layoutPtr.reset(new log4cpp::PatternLayout());

    // Create RollingFileAppender    
    m_rfileAppenderPtr.reset( new log4cpp::RollingFileAppender(    std::string("GW"), std::string(p_sFullLogFileName), maxFileSize, maxBackupIndex, append, mode ) );
    m_category = &log4cpp::Category::getRoot().getInstance("GW");

    //if ( m_layout != NULL)
    if ( m_layoutPtr.get() != NULL)
    {
        try
        {
            //log4cpp::PatternLayout * patternLayout = (log4cpp::PatternLayout * )(m_layout);
            log4cpp::PatternLayout * patternLayout = (log4cpp::PatternLayout * )(m_layoutPtr.get());

            // Set up Pattern
            patternLayout->setConversionPattern( "%d{%Y-%m-%d %H:%M:%S,%l} %x : %m%n");

            // Bind Layout to RollingFileAppender            
            m_rfileAppenderPtr->setLayout(m_layoutPtr.get());            
            m_category->addAppender(m_rfileAppenderPtr.get());
            m_category->setAdditivity(true);

            try
            {
                m_category->setPriority(log4cpp::Priority::getPriorityValue("DEBUG"));
            }
            catch(std::invalid_argument &ia)
            {
                std::cerr << "Invalid Priority: "  << " DEBUG " << std::endl;
                m_category->setPriority(log4cpp::Priority::INFO);
            }

            // Bind RollingFileAppender to Category            
            m_category->addAppender(m_rfileAppenderPtr.get());
        }
        catch(log4cpp::ConfigureFailure &cf)
        {
            std::cerr << cf.what() << std::endl;
            //return 1;
        }
    }
    else
    {
        std::cerr << "Cannot initialize PatternLayout" << std::endl;
        //return 1;
    }

    m_pLog4cpp = &m_category->getInstance( "GW" );
}


void DBAccess::Log(const std::string & msg)
{
    /*
    if ( m_pLog4cpp != NULL )
    {
        m_pLog4cpp->debug(msg);
    }
    */
}
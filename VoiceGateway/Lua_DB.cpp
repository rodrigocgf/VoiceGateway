/*************************************************************************************************************************
Lua_DB.cpp
*************************************************************************************************************************/

// para STL
#ifdef _DEBUG
#pragma warning(disable:4786) 
#endif

// Nao altere a ordem dos includes
#include <iostream>
#include <strstream>
#include <lua.hpp>

#include "IDBAccess.h"
#include "Ivr.h"
#include "Switch.h"
#include "CriticalSection.h"
//#include "..\Functions.h"
#include <io.h>

typedef struct {

    bool bError;							// Flag indicando se houve um erro na execucao da ultima query
    IDBAccess * pDbc;								// Ponteiro para o objeto de banco de dados
    CriticalSection * CritDb;	// Secao critica para acesso ao mesmo ponteiro do objeto do banco de dados
    
} LUA_DBC;

// Map que armazenara as conexoes com seus dados indexados por um nome de conexao
static map <string, LUA_DBC> MapLuaDBC;

// Secao critica para a criacao de uma conexao
static CriticalSection CritConnectionCreate; 

int DbConnect		(lua_State *L);
int DbExecProc		(lua_State *L);
int DbExecQuery		(lua_State *L);
int DbGetNumRows	(lua_State *L);
int DbGetNumCols	(lua_State *L);
int DbGetRow		(lua_State *L);

// Essa funcao nao eh exportada para o script (uso interno)
void LuaDbResult	(lua_State *L);


int LuaTestTable(lua_State *L);

static const struct luaL_reg MyLibFunctions[] = 
{
    {"DbConnect"    ,   DbConnect       },
    {"DbExecProc"   , 	DbExecProc		},
    {"DbExecQuery"  , 	DbExecQuery		},
    {"DbGetNumRows" ,	DbGetNumRows	},
    {"DbGetNumCols" ,	DbGetNumCols	},
    {"DbGetRow"     , 	DbGetRow        }
};

Ivr * GetIvrDB( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );
    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return pIvr;
}

void Lua_DbLibOpen(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    pIvr->Log(LOG_INFO,"Lua_DbLibOpen()");

    luaL_openl(L, MyLibFunctions);    
}

void Lua_DbLibClose(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    pIvr->Log(LOG_INFO,"Lua_DbLibClose()");

    try
    {        
    
        map<string, LUA_DBC>::iterator it;

        for ( it = MapLuaDBC.begin(); it != MapLuaDBC.end(); it++ )
        {

            LUA_DBC luaDbc = it->second;


            

            if ( luaDbc.CritDb != NULL )
            {

                //DeleteCriticalSection( luaDbc.CritDb );

                //delete luaDbc.CritDb;        

            }
            if ( luaDbc.pDbc != NULL )
            {
                //delete luaDbc.pDbc;

            }
        }
    } 
    catch(...)
    {
    }
}

//------------------------------------------------------------------------------------------------------------------------

bool TimeStamp(char *sPar)
{
    try {

        if(strchr(sPar, '-') == NULL)
            return false;
        if(strchr(sPar, '-') == NULL)
            return false;
        if(strchr(sPar, '-') == NULL)
            return false;
        if(strchr(sPar, ' ') == NULL)
            return false;
        if(strchr(sPar, ':') == NULL)
            return false;
        if(strchr(sPar, ':') == NULL)
            return false;

        return true;
    }
    catch(...){
    }
}

//------------------------------------------------------------------------------------------------------------------------
//
// Parameters:
//
// string sDbType		- ORACLE ou ODBC
// string sUser
// string sPassword
// string sHostString
// string sDataSource
// string sDataBase
//
// Return Value:
//	Primeiro valor = 0 se OK e -1 se erro
//	Segundoi valor = mensagem de erro de existir
// Description:
//	Connecta com um Data Source Name

int DbConnect(lua_State *L)
{

    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try
    {

        int iNumArgs;
        char sAux[5];
        string sDbType;
        string sHostString;
        string sUser;
        string sPassword;
        string sDataSource;
        string sDataBase;
        string sError;
        string sIndex;
        string sPortInfo;
        std::strstream ConnectionString;

        pIvr->Log(LOG_INFO,"DbConnect()");
        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if( ( iNumArgs != 1 ) && ( iNumArgs != 6 ) )
        {
            return 0;
        }

        if( iNumArgs == 1 )
        {
            ConnectionString << luaL_check_string( L, 1 ) << ends;
            pIvr->Log(LOG_INFO,"DbConnect() : ConnectionString : %s", ConnectionString.str() );
        }
        else
        {
            sDbType		= luaL_check_string(L, 1);
            sUser		= luaL_check_string(L, 2);
            sPassword	= luaL_check_string(L, 3);
            sHostString	= luaL_check_string(L, 4);
            sDataSource	= luaL_check_string(L, 5);
            sDataBase	= luaL_check_string(L, 6);

            pIvr->Log(LOG_INFO,"DbConnect() : Database Type : %s", sDbType.c_str() );
            pIvr->Log(LOG_INFO,"DbConnect() : User : %s", sUser.c_str() );
            pIvr->Log(LOG_INFO,"DbConnect() : Password : %s", sPassword.c_str() );
            pIvr->Log(LOG_INFO,"DbConnect() : Host : %s", sHostString.c_str() );
            pIvr->Log(LOG_INFO,"DbConnect() : Data Source : %s", sDataSource.c_str() );
            pIvr->Log(LOG_INFO,"DbConnect() : Database : %s", sDataBase.c_str() );

            // Provider=SQLOLEDB.1;Password="";Persist Security Info=True;User ID=sa;Initial Catalog=Voxxi-oi;Data Source=DEVELOP01\DSV01
            ConnectionString << "Provider=SQLOLEDB" << ";" << "Data Source=" << sDataSource.c_str() << ";";
            ConnectionString << "Initial Catalog=" << sDataBase.c_str() << ";" << "User ID=" << sUser.c_str() << ";";
            ConnectionString << "Password=\"" << sPassword.c_str() << "\"" << ";" << ends;
        }

        sprintf( sAux, "[%d]", pIvr->GetCTI()->GetChannel() % pIvr->GetSwitch()->GetMaxIvrDbConnection() );
        sPortInfo = sAux;

        // Monta o nome da conexao com o numero da porta em ralacao a essa instancia do LUA
        sIndex = ConnectionString.str() + sPortInfo;
        //sIndex = sDbType + sUser + sPassword + sHostString + sDataSource + sDataBase + sPortInfo;

        // Guarda o ultimo index de conexao que sera usado nas chamadas da funcoes
        // e Query e coleta dos dados
        pIvr->SetDbIndexConnection( sIndex );        
                
        CritConnectionCreate.Enter();

        if( MapLuaDBC.count( sIndex.c_str() ) <= 0 )
        {
            MapLuaDBC[ sIndex.c_str() ].pDbc = new DBAccess();
            MapLuaDBC[ sIndex.c_str() ].CritDb = new CriticalSection();
            MapLuaDBC[ sIndex.c_str() ].bError = false;

            if( MapLuaDBC[ sIndex.c_str() ].pDbc->Open( ConnectionString.str() ) != 0 )
            {
                MapLuaDBC[ sIndex.c_str() ].bError = true;

                pIvr->Log(LOG_ERROR, "Error in DbConnect(). %s", MapLuaDBC[ sIndex.c_str() ].pDbc->GetLastError());

                lua_pushnumber (L, -1);
                lua_pushstring (L, MapLuaDBC[ sIndex.c_str() ].pDbc->GetLastError());

                // Zera o ultimo index de conexao para nao ser usado nas chamadas da funcoes
                // e Query e coleta dos dados
                pIvr->SetDbIndexConnection("");

            }
            else
            {
                lua_pushnumber (L, 0);
                lua_pushstring (L, "Connected");				
            }

            CritConnectionCreate.Leave();
            ConnectionString.freeze(0);

            return 2;
        }

        CritConnectionCreate.Leave();

        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Enter();		

        // Verifica se nao houve um erro na execucao da ultima query
        if( !MapLuaDBC[ sIndex.c_str() ].bError )
        {
            // A conexao ja existe e nao ha erro.
            lua_pushnumber (L, 0);
            lua_pushstring (L, "Connected");

            // Guarda o ultimo index de conexao que sera usado nas chamadas da funcoes
            // e Query e coleta dos dados
            pIvr->SetDbIndexConnection( sIndex );

            MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();            
            ConnectionString.freeze(0);
            return 2;
        }
        else
        {
            // Se houve um erro, reconecta.
            MapLuaDBC[ sIndex.c_str() ].pDbc->Close();
            if( MapLuaDBC[ sIndex.c_str() ].pDbc->Open( ConnectionString.str() ) < 0 )
            {
                MapLuaDBC[ sIndex.c_str() ].bError = true;

                pIvr->Log(LOG_ERROR, "Error in DbConnect(). %s", MapLuaDBC[ sIndex.c_str() ].pDbc->GetLastError());

                lua_pushnumber (L, -1);
                lua_pushstring (L, MapLuaDBC[ sIndex.c_str() ].pDbc->GetLastError());

                MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();                


                // Zera o ultimo index de conexao para nao ser usado nas chamadas da funcoes
                // e Query e coleta dos dados
                pIvr->SetDbIndexConnection("");
                ConnectionString.freeze(0);
                return 2;
            }

            lua_pushnumber (L, 0);
            lua_pushstring (L, "Connected");

            // Guarda o ultimo index de conexao que sera usado nas chamadas da funcoes
            // e Query e coleta dos dados
            //strcpy(L->cIndex, sIndex.c_str());

            MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();
            ConnectionString.freeze(0);
            return 2;
        }
    }
    catch(...)
    {
        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbConnect()");
        return 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:
// string sProcedure - Procedure a ser executada
// Return Value:
//	Primeiro valor	= 0 se OK e -1 se erro
//	Segundo valor		= mensagem de erro de existir
// Description:
//	Executa uma procedure no banco de dados
int DbExecProc(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try
    {

        int iNumArgs, l, c;
        char *Query = NULL;
        char *pChar = NULL;
        bool bNumeric = false;
        string sError, sParam, sData;
        unsigned long QueryTimeStart;
        unsigned long QueryTimeStop;
        int iRows = 0, iCols = 0;

        pIvr->Log(LOG_INFO,"DbExecProc()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1)
        {
            return 0;
        }

        QueryTimeStart = GetTickCount();

        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Enter();        

        QueryTimeStop = GetTickCount();

        if( ( QueryTimeStop - QueryTimeStart ) > 1000 )
        {
            pIvr->Log( LOG_INFO, "Lock time high: %d ms", ( QueryTimeStop - QueryTimeStart ) );
        }

        Query = (char *)luaL_check_string(L, 1);

        pIvr->Log(LOG_INFO, "Lua Query: %s", Query);

        QueryTimeStart = GetTickCount();

        if( MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->Execute(Query) )
        {
            // Erro na execucao da query, seta o flag de erro para que a conexao 
            // seja refeita novamente
            MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].bError = true;
            pIvr->Log(LOG_ERROR, "Error in DbExecProc(). %s", MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetLastError());

            lua_pushnumber (L, -1);
            lua_pushstring (L, MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetLastError());
        }
        else
        {

            QueryTimeStop = GetTickCount();

            if( ( QueryTimeStop - QueryTimeStart ) > 1000 )
            {
                pIvr->Log( LOG_INFO, "Query time high: %d ms", ( QueryTimeStop - QueryTimeStart ) );
            }
            // Query executado com sucesso, se tiver resultado, quarda em uma estrutura local

            // Limpa o RecordSet atual
            pIvr->ClearRecordSet();

            iRows = 0;
            iCols = 0;

            iRows = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetRowCount();
            iCols = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetColCount();

            //LuaDbResult(L);

            if( iRows > 0 ){

                for( l = 0; l < iRows; l++){

                    for( c = 0; c < iCols; c++){

                        // Obtem o valor de um campo
                        sData = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetItem(c);

                        // Guarda o RecordSet retornado
                        pIvr->PushRecordSet(l, c, sData);
                    }

                    MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->MoveNext();
                }
            }

            lua_pushnumber (L, 0);
            lua_pushstring (L, "OK");
        }

        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();        
        return 2;
    }
    catch(...){
        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();        
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbExecProc()");
        return 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:
// string sQuery - Query a ser executada
// Return Value:
//	Primeiro valor	= 0 se OK e -1 se erro
//	Segundo valor		= mensagem de erro de existir
// Description:
//	Executa uma query no banco de dados
int DbExecQuery(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try {

        int iNumArgs, l, c;
        char *Query = NULL;
        char *pChar = NULL;
        string sError, sParam, sData;
        unsigned long QueryTimeStart;
        unsigned long QueryTimeStop;
        int iRows = 0, iCols = 0;

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1){
            return 0;
        }

        QueryTimeStart = GetTickCount();

        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Enter();

        QueryTimeStop = GetTickCount();

        if( ( QueryTimeStop - QueryTimeStart ) > 1000 )
        {
            pIvr->Log( LOG_INFO, "Lock time high: %d ms", ( QueryTimeStop - QueryTimeStart ) );
        }

        Query = (char *)luaL_check_string(L, 1);

        pIvr->Log(LOG_INFO, "%s", Query);

        QueryTimeStart = GetTickCount();

        if( MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->Execute(Query) ){
            // Erro na execucao da query, seta o flag de erro para que a conexao 
            // seja refeita novamente
            MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].bError = true;
            pIvr->Log(LOG_ERROR, "Error in DbExecQuery(). %s", MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetLastError());
            lua_pushnumber (L, -1);
            lua_pushstring (L, MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetLastError());
        }
        else{

            QueryTimeStop = GetTickCount();

            if( ( QueryTimeStop - QueryTimeStart ) > 1000 )
            {
                pIvr->Log( LOG_INFO, "Query time high: %d ms", ( QueryTimeStop - QueryTimeStart ) );
            }
            // Query executado com sucesso, se tiver resultado, quarda em uma estrutura local

            // Limpa o RecordSet atual
            pIvr->ClearRecordSet();

            iRows = 0;
            iCols = 0;

            iRows = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetRowCount();
            iCols = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetColCount();

            //LuaDbResult(L);

            if( iRows > 0 ){

                for( l = 0; l < iRows; l++){

                    for( c = 0; c < iCols; c++){

                        // Obtem o valor de um campo
                        sData = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetItem(c);

                        // Guarda o RecordSet retornado
                        pIvr->PushRecordSet(l, c, sData);
                    }
                    MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->MoveNext();
                }
            }

            lua_pushnumber (L, 0);
            lua_pushstring (L, "OK");
        }

        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();        
        return 2;
    }
    catch(...)
    {
        MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].CritDb->Leave();        
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbExecQuery()");
        return 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:
// Nenhum
// Return Value:
//	Numero de linhas do resultado
// Description:
//	Retorna o numero de linhas do resultado
int DbGetNumRows(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try {

        lua_pushnumber (L, MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetRowCount());
        return 1;
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbGetNumRows()");
        return 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:
// Nenhum
// Return Value:
//	Numero de colunas do resultado
// Description:
//	Retorna o numero de colunas do resultado
int DbGetNumCols(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try {

        lua_pushnumber (L, MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetColCount());
        return 1;
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbGetNumCols()");
        return 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:
// int Linha
// Return Value:
//	Tabela com todas as colunas da linha
// Description:
//	Retorna uma tabela com todas as colunas da linha
int DbGetRow(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try {

        int iNumArgs, Linha, Coluna;
        string sData;
        int iCols = 0;

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1){
            return 0;
        }

        Linha = luaL_check_int(L, 1);

        iCols = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetColCount();

        // Cria a Table para retornar para o script
        lua_newtable (L);

        for(Coluna = 1; Coluna <= iCols; Coluna++){
            // Obtem o dado do recordSet retornado pelo banco de dados
            pIvr->PopRecordSet( (Linha-1), (Coluna-1), sData);
            // Coloca o dado na pilha
            lua_pushstring(L, sData.c_str());
            // Set o valor da Table do LUA
            lua_rawseti(L, 2, Coluna);
        }
        return 1;
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbGetRow()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

void LuaDbResult(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    string sResult;

    try {
        int iRows = 0, iCols = 0;

        iRows = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetRowCount();
		iCols = MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetColCount();

        for(int l = 0; l < iRows; l++){

            sResult = "Query Result: ";

            for(int c = 0; c < iCols; c++){

                sResult += MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->GetItem(c);
                sResult += "  ";
            }

            MapLuaDBC[ pIvr->GetDbIndexConnection().c_str() ].pDbc->MoveNext();

            pIvr->Log(LOG_DB_RESULT, "%s", sResult.c_str());
        }
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - LuaDbResult)");
    }
}

//---------------------------------------------------------------------------------------------


int LuaTestTable(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrDB(L);

    try {

        int iNumArgs, Coluna, Linha;
        string sData;

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1){
            return 0;
        }

        Linha = luaL_check_int(L, 1);

        // Cria a Table para retornar para o script
        lua_newtable (L);

        for(Coluna = 1; Coluna <= 1000; Coluna++)
        {
            sData = "#AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA#";
            // Coloca o dado na pilha
            lua_pushstring(L, sData.c_str());
            // Set o valor da Table do LUA
            lua_rawseti(L, 2, Coluna);
        }
        return 1;
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua module - DbGetRow()");
        return 0;
    }
}

#ifndef _TELNETSESSION_H_
#define _TELNETSESSION_H_

#include <boost/enable_shared_from_this.hpp>
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <string>
#include <memory>
#include <vector>
#include <functional>
#include <list>
#include <iostream>
#include <cassert>
#include <array>
#include <string.h>
#include <stdlib.h>
#include <type_traits>
#include <limits>
#include <algorithm>
#include <utility>
#include "DialogicDefines.h"
#include "Logger.h"
#include "IThrTelnetListen.h"
#include <boost/algorithm/string.hpp>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>
#include "ChannelStat.h"


const std::string ANSI_FG_BLACK   ("\x1b[30m");
const std::string ANSI_FG_RED     ("\x1b[31m");
const std::string ANSI_FG_GREEN   ("\x1b[32m");
const std::string ANSI_FG_YELLOW  ("\x1b[33m");
const std::string ANSI_FG_BLUE    ("\x1b[34m");
const std::string ANSI_FG_MAGENTA ("\x1b[35m");
const std::string ANSI_FG_CYAN    ("\x1b[36m");
const std::string ANSI_FG_WHITE   ("\x1b[37m");
const std::string ANSI_FG_DEFAULT ("\x1b[39m");

const std::string ANSI_BG_BLACK   ("\x1b[40m");
const std::string ANSI_BG_RED     ("\x1b[41m");
const std::string ANSI_BG_GREEN   ("\x1b[42m");
const std::string ANSI_BG_YELLOW  ("\x1b[43m");
const std::string ANSI_BG_BLUE    ("\x1b[44m");
const std::string ANSI_BG_MAGENTA ("\x1b[45m");
const std::string ANSI_BG_CYAN    ("\x1b[46m");
const std::string ANSI_BG_WHITE   ("\x1b[47m");
const std::string ANSI_BG_DEFAULT ("\x1b[49m");

const std::string ANSI_BOLD_ON       ("\x1b[1m");
const std::string ANSI_BOLD_OFF      ("\x1b[22m");

const std::string ANSI_ITALICS_ON    ("\x1b[3m");
const std::string ANSI_ITALCIS_OFF   ("\x1b[23m");

const std::string ANSI_UNDERLINE_ON  ("\x1b[4m");
const std::string ANSI_UNDERLINE_OFF ("\x1b[24m");

const std::string ANSI_INVERSE_ON    ("\x1b[7m");
const std::string ANSI_INVERSE_OFF   ("\x1b[27m");

const std::string ANSI_STRIKETHROUGH_ON  ("\x1b[9m");
const std::string ANSI_STRIKETHROUGH_OFF ("\x1b[29m");

const std::string ANSI_ERASE_LINE        ("\x1b[2K");
const std::string ANSI_ERASE_SCREEN      ("\x1b[2J");

const std::string ANSI_ARROW_UP("\x1b\x5b\x41");
const std::string ANSI_ARROW_DOWN("\x1b\x5b\x42");
const std::string ANSI_ARROW_RIGHT("\x1b\x5b\x43");
const std::string ANSI_ARROW_LEFT("\x1b\x5b\x44");


const std::string TELNET_ERASE_LINE      ("\xff\xf8");


#define DEFAULT_BUFLEN 512

class TelnetSession : public boost::enable_shared_from_this < TelnetSession >
{
public:           
    
    //TelnetSession( SOCKET ClientSocket, boost::shared_ptr<IThrTelnetListen> ts , boost::shared_ptr<Logger> pLog4cpp , const std::string & sTelnetPassword );
    TelnetSession( SOCKET ClientSocket, IThrTelnetListen * ts , boost::shared_ptr<Logger> pLog4cpp , const std::string & sTelnetPassword );
    
    
    void Start( );
    void Stop();

    void sendLine(std::string data);    // Send a line of data to the Telnet Server
    void closeClient();                 // Finish the session

    //static void UNIT_TEST();

    void initialise();                  // 
    void update();                      // Called every frame/loop by the Terminal Server

private:
    void sendPromptAndBuffer();         // Write the prompt and any data sat in the input buffer
    void eraseLine();                   // Erase all characters on the current line and move prompt back to beginning of line
    void echoBack(const char * buffer, u_long length);
    void ParseReceivedBuffer(const std::string & buffer);

    void SendHelpMenu();
    void ParseStocOK(const std::string & inputCommand);
    void ParseStoc(const std::string & inputCommand);
    void ParseTime(const std::string & inputCommand);
    void ParseDate(const std::string & inputCommand);
    void ParseStat(const std::string & inputCommand);
    void ParseChnc(const std::string & inputCommand);
    void ParseDial(const std::string & inputCommand);
    void ParseDisc(const std::string & inputCommand);
    void ParseBlkc(const std::string & inputCommand);
    void ParseUnbc(const std::string & inputCommand);

    static void stripNVT(std::string &buffer);
    static void stripEscapeCharacters(std::string &buffer);                 // Remove all escape characters from the line
    static bool processBackspace(std::string &buffer);                      // Takes backspace commands and removes them and the preceeding character from the m_buffer. // Handles arrow key actions for history management. Returns true if the input buffer was changed.
    //static std::vector<std::string> getCompleteLines(std::string &buffer);  
    std::vector<std::string> getCompleteLines(std::string &buffer);  

    void addToHistory(std::string line);                                    // Add a command into the command history
    bool processCommandHistory(std::string &buffer);                        // Handles arrow key actions for history management. Returns true if the input buffer was changed.

    boost::shared_ptr<boost::thread>    m_SessionThreadPtr;
    void Loop();    
    volatile bool m_bStop;

    std::vector<char> vecRecv;    
    void CheckPassword(const char * szRecv, int iLen);
    int iState; // 1 - show * ; 0 - normal char

    std::vector<std::string> split_string(std::string input_string);

    boost::posix_time::ptime	m_before;
	boost::posix_time::ptime	m_after;
    std::size_t				    m_msInterval;

private:
    boost::shared_ptr<Logger>       m_pLog4cpp;
    std::string                     m_sTelnetPassword;
    std::string                     m_sRecvPassword;
    int                             m_iNumLoginTries;
    bool                            m_bLoggedIn;

    SOCKET m_socket;                // The Winsock socket
    
    //boost::shared_ptr<IThrTelnetListen> m_telnetServer; // Parent TelnetServer class
    IThrTelnetListen * m_telnetServer; // Parent ThrTelnetListen class

    std::string m_buffer;           // Buffer of input data (mid line)
    std::list<std::string>           m_history;  // A history of all completed commands
    std::list<std::string>::iterator m_historyCursor;


    //friend TelnetServer;
};


#endif
/*************************************************************************************************************************
Lua_CDR.cpp
*************************************************************************************************************************/

// para STL
#ifdef _DEBUG
#pragma warning(disable:4786) 
#endif

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>

#include <boost/algorithm/string/trim_all.hpp>
#include "Cdr.h"
#include "Switch.h"
#include <lua.hpp>
#include <io.h>
#include "log4cpp/FileAppender.hh"

int LuaLogCdr			(lua_State *L);
int ParseCDRDirectory( lua_State* L , const std::string & sFileIndex , const std::string & sAppName , int & p_index);
//std::tuple<int,std::string> ParseDateCDRDirectory( lua_State*L , const std::string & sPathCdr , int & p_index);
std::pair<int,std::string> ParseDateCDRDirectory( lua_State*L , const std::string & sPathCdr , int & p_index);
void WriteIndex(lua_State* L , const std::string & p_sFileName, const int & iValue);
int ReadIndex(lua_State * L, const std::string & p_sFileName);
void Log4cpp(lua_State * L , const std::string & p_sText, const std::string & p_sAppName );
void StoreCdrMapInfo(lua_State * L , const std::string & p_sFilename , const std::string & p_sAppName);
std::string GetCallId(lua_State * L ,const std::string _pCdrText);

static const struct luaL_reg CDRLibFunctions[] = 
{
    {"LogCdr",		LuaLogCdr}
};

Ivr * GetIvrCdr( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );
    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return pIvr;
}


log4cpp::Category * GetCdrCategory( lua_State* L )
{
    log4cpp::Category * p_Category;
    lua_getglobal( L, "__Category" );
    p_Category = static_cast<log4cpp::Category *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return p_Category;
}


log4cpp::Layout * GetCdrLayout( lua_State* L )
{
    log4cpp::Layout * p_PatternLayout;
    lua_getglobal( L, "__PatternLayout" );
    p_PatternLayout = static_cast<log4cpp::Layout *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return p_PatternLayout;
}

void Lua_CdrLibOpen(lua_State *L)
{
    luaL_openl(L, CDRLibFunctions);
}

//------------------------------------------------------------------------------------------------------------------------

// Parameters:

// string sAppName
// string sCdrInfo
// Return Value:
//	valor = 0 se OK e -1 se erro
// Description:
//	Grava o CDR no arquivo texto

int LuaLogCdr(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);    

    ISwitch * p_Switch = pIvr->GetSwitch();

    if ( p_Switch == NULL )
        return 0;

    boost::shared_ptr<Cdr> p_CdrPtr = p_Switch->GetCdr();

    if ( p_CdrPtr.get() == NULL )
        return 0;
    
    pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );

    try
    {   

        int iNumArgs, index, n;
        string sAppName;
        string sCdrInfo;
        char sPathCdr[_MAX_PATH], sPathDate[50], sIndex[4];
        string sFilename, sFileIndex;
        FILE *IndexFile;
        struct tm *timer;
        time_t ltime;
        std::stringstream ss;
        int iRet = 0;

        index = 0;

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs != 2) 
        {
            return 0;
        }

        sAppName			= luaL_check_string(L, 1);
        sCdrInfo			= luaL_check_string(L, 2);        
        
        
        EnterCriticalSection( &p_CdrPtr->CritLogCdr );
                
        if ( p_CdrPtr->MapLogCdr.find(sAppName) == p_CdrPtr->MapLogCdr.end() )
        {   
            sprintf(sPathCdr, "%s\\CDR\\%s", p_Switch->GetBasePathLog().c_str(), sAppName.c_str());

            sFileIndex.clear();
            ss.str(std::string());
            ss << boost::format("%s\\index.txt") % sPathCdr;
            sFileIndex = ss.str();

            pIvr->Log(LOG_INFO,"===== INDEX FILE : %s", sFileIndex.c_str() );

                
            iRet = ParseCDRDirectory( L , sFileIndex , sAppName , index) ;
            if (iRet != 0 )
                return iRet;
                                
            //tie( iRet , sFilename ) = ParseDateCDRDirectory( L , sPathCdr , index );
            pair<int,std::string> pRet = ParseDateCDRDirectory( L , sPathCdr , index );
            iRet = pRet.first;
            sFilename = pRet.second;
                
            if ( iRet != 0 )
                return iRet;                

            WriteIndex(L, sFileIndex, index);
                
            pIvr->Log(LOG_INFO,"[%s] CDR FILE : %s", __FUNCTION__ ,  sFilename.c_str() );
                
            StoreCdrMapInfo( L , sFilename , sAppName );
        }

        if(p_CdrPtr->MapLogCdr[sAppName.c_str()].ChangeFile)
			p_CdrPtr->MapLogCdr[sAppName.c_str()].ChangeFile = 0;

        Log4cpp(L, sCdrInfo, sAppName);        

        LeaveCriticalSection( &p_CdrPtr->CritLogCdr );
        

        lua_pushnumber (L, 0);
        return 1;


    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function LuaLogCdr()");
        return 0;
    }
}

void StoreCdrMapInfo(lua_State * L , const std::string & p_sFilename , const std::string & p_sAppName )
{
    std::stringstream ss;
    Ivr * pIvr = NULL;    
    pIvr = GetIvrCdr(L);

    ISwitch * p_Switch = pIvr->GetSwitch();

    if ( p_Switch == NULL )
        return;

    try
    {
        boost::shared_ptr<Cdr> p_CdrPtr = p_Switch->GetCdr();

        if ( p_CdrPtr.get() == NULL )
            return;
        
        STDIO_CDR stdioCdr;
        stdioCdr.mp_outFile = fopen(p_sFilename.c_str(),"w+");
        stdioCdr.m_filename = p_sFilename;
        stdioCdr.ChangeFile = 0;

        p_CdrPtr->MapLogCdr.emplace(std::make_pair(p_sAppName.c_str(), stdioCdr));
        
        ss << "\r\n";
        ss << "===================================================================================================" << std::endl;
        ss << "|                                                                                                 |" << std::endl;
        ss << "|                                        STORE CDR MAP INFO                                       |" << std::endl;
        ss << "|                                          ( Lua_Cdr.cpp )                                        |" << std::endl;
        ss << "|                                                                                                 |" << std::endl;
        ss << boost::format("|[%s]    FileName : %64s |\r\n") % __FUNCTION__ % p_sFilename.c_str();
        ss << boost::format("|[%s]    App Name : %64s |\r\n") % __FUNCTION__ % p_sAppName.c_str();    
        ss << "|                                                                                                 |" << std::endl;
        ss << boost::format("|[%s]    map < string , LOGGER_CDR > NEW ENTRY FOR KEY : %27s |\r\n") % __FUNCTION__ % p_sAppName.c_str();
        ss << "===================================================================================================" << std::endl;
        ss << "\r\n";
        pIvr->Log(LOG_INFO,ss.str().c_str() ); // DEBUG ONLY
    
        pIvr->Log(LOG_INFO,"[%s] End", __FUNCTION__ );
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function StoreCdrMapInfo()");
        return;
    }

}

void Log4cpp(lua_State * L , const std::string & p_sText , const std::string & p_sAppName )
{
    std::stringstream ss;
    
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);    

    ISwitch * p_Switch = pIvr->GetSwitch();

    if ( p_Switch == NULL )
        return;

    try
    {
        pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );

        boost::shared_ptr<Cdr> p_CdrPtr = p_Switch->GetCdr();
        if ( p_CdrPtr.get() != NULL )
        {   
            if ( p_CdrPtr->MapLogCdr.find(p_sAppName) != p_CdrPtr->MapLogCdr.end() )
            {                  
                FILE * p_outFile = p_CdrPtr->MapLogCdr[p_sAppName].mp_outFile;

                fwrite(p_sText.c_str(), sizeof(char), strlen(p_sText.c_str()) , p_outFile );
                fwrite("\r\n", sizeof(char), 2 , p_outFile );
                fflush(p_outFile);                

                ss << "\r\n";
                ss << "===================================================================================================" << std::endl;
                ss << "|                                                                                                 |" << std::endl;
                ss << "|                                               CDR                                               |" << std::endl;
                ss << "|                                          ( Lua_Cdr.cpp )                                        |" << std::endl;
                ss << "|                                                                                                 |" << std::endl;            
                ss << boost::format("[%s]    App Name : %s \r\n") % __FUNCTION__ % p_sAppName.c_str();
                ss << boost::format("[%s]    Text     : %s \r\n") % __FUNCTION__ % p_sText.c_str();
                ss << "|                                                                                                 |" << std::endl;            
                ss << "===================================================================================================" << std::endl;
                ss << "\r\n";
                //pIvr->Log(LOG_INFO,ss.str().c_str() ); // DEBUG ONLY
            }
       
        }
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function Log4cpp()");
        return;
    }
}


int ParseCDRDirectory( lua_State* L , const std::string & p_sFileIndex , const std::string & p_AppName , int & p_index)
{    
    int n;
    char sIndex[10];
    char sPathCdr[_MAX_PATH];
    FILE *IndexFile;
    std::stringstream ss;
    int iRet = 0;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);

    try
    {
        pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );

        boost::filesystem::path pFileIndex(p_sFileIndex);
        if ( !boost::filesystem::exists(pFileIndex) )    
        {
            sprintf(sPathCdr, "%s\\CDR", pIvr->GetSwitch()->GetBasePathLog().c_str());
            boost::filesystem::path pCdr(sPathCdr);
            if ( !boost::filesystem::exists(pCdr) )        
            {
                if ( !boost::filesystem::create_directory(sPathCdr) )                        
                {
                    pIvr->Log(LOG_ERROR, "LuaLogCdr(): N�o conseguiu criar o diret�rio %s", sPathCdr);
                    lua_pushnumber (L, 1);
                    iRet = 1;
                } else {
                    sprintf(sPathCdr, "%s\\CDR\\%s", pIvr->GetSwitch()->GetBasePathLog().c_str() , p_AppName.c_str());

                    boost::filesystem::path pCdrAppName(sPathCdr);
                    if ( !boost::filesystem::exists(pCdrAppName) )
                    {        
                        if ( !boost::filesystem::create_directory(sPathCdr) )                        
                        {
                            pIvr->Log(LOG_ERROR, "LuaLogCdr(): N�o conseguiu criar o diret�rio %s", sPathCdr);
                            lua_pushnumber (L, 1);
                            iRet = 1;
                        }
                    }
        
                    p_index = 1;
                }
            }        
        }
        else
        {
            p_index = ReadIndex(L, p_sFileIndex);        
            p_index = p_index + 1;
        }
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function ParseCDRDirectory()");        
    }

    return iRet;
}

//std::tuple<int , std::string> ParseDateCDRDirectory( lua_State*L , const std::string & sPathCdr , int & p_index )
std::pair<int , std::string> ParseDateCDRDirectory( lua_State*L , const std::string & sPathCdr , int & p_index )
{
    int iRet = 0;
    std::string sNameRet;

    struct tm *timer;
    time_t ltime;
    char sPathDate[50];
    string sFilename;
    std::stringstream ss;
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);

    try
    {
        pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );


        time(&ltime);
        timer = localtime(&ltime);
        memset(sPathDate, 0x00, sizeof(sPathDate) );
        sprintf(sPathDate, "\\%04d-%02d-%02d", timer->tm_year + 1900, timer->tm_mon + 1, timer->tm_mday);

        sFilename.clear();
        ss.str(std::string());
        ss << boost::format("%s%s") % sPathCdr % sPathDate;
        sFilename = ss.str();
        pIvr->Log(LOG_INFO,"[%s] Date Directory : %s ", __FUNCTION__ , sFilename.c_str() );                

        if(_access( sFilename.c_str(), 0) != 0)		//o diret�rio n�o existe
        {
            if ( !boost::filesystem::create_directory(sFilename.c_str()) )                    
            {
                pIvr->Log(LOG_ERROR, "LuaLogCdr(): N�o conseguiu criar o diret�rio %s", sFilename.c_str());
                lua_pushnumber (L, 1);
                iRet =  1;
            } else
                p_index = 1;
        }

        memset(sPathDate, 0x00, sizeof(sPathDate) );
        sprintf(sPathDate, "\\CDR_%04d.txt", p_index);     
        ss.str(std::string());
        ss << boost::format("%s%s") % sFilename % sPathDate;
        sNameRet = ss.str();    
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function ParseCDRDirectory()");        
    }
    

    //return std::make_tuple(iRet, sNameRet);
    return make_pair(iRet , sNameRet );
}

void WriteIndex(lua_State* L , const std::string & p_sFileName, const int & iValue)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);

    try
    {
        pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );
        std::stringstream ss;
        boost::filesystem::path p(p_sFileName);
        boost::filesystem::ofstream out( p , std::ios::out );
        ss << boost::format("%d") % iValue;
        out << ss.str();

        pIvr->Log(LOG_INFO,"[%s] WRITTEN TO FILE - INDEX = %d", __FUNCTION__ , iValue );
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function WriteIndex()");        
    }
}

int ReadIndex(lua_State * L, const std::string & p_sFileName)
{    
    int iRet = 0;
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);

    try
    {
        pIvr->Log(LOG_INFO,"[%s]", __FUNCTION__ );

        boost::filesystem::path p(p_sFileName);
        boost::filesystem::ifstream in( p , std::ios::out );

        if ( !in.is_open() )
        {
            pIvr->Log(LOG_INFO,"[%s] Could not open file %s", __FUNCTION__ , p_sFileName.c_str() );
            return 0;
        }

        std::string line;
        std::getline(in, line);
        boost::algorithm::trim_all(line);

        pIvr->Log(LOG_INFO,"[%s] line read : [%s]", __FUNCTION__ , line.c_str() );
        iRet = boost::lexical_cast<int>(line.c_str());

        pIvr->Log(LOG_INFO,"[%s] READ FROM FILE - INDEX = %d", __FUNCTION__ , iRet );
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function ReadIndex()");        
    }

    return iRet;    
}

class WordDelimitedByComma : public std::string
{};

std::istream& operator>>(std::istream& is, WordDelimitedByComma& output)
{
   std::getline(is, output, ',');
   return is;
}

std::string GetCallId(lua_State * L , const std::string _pCdrText)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrCdr(L);    
    std::string strCallId;

    try
    {
        pIvr->Log(LOG_INFO,"[%s] CDR INFO : %s", __FUNCTION__ , _pCdrText.c_str());

        
        std::istringstream iss(_pCdrText);    
    
        std::vector<std::string> results((std::istream_iterator<WordDelimitedByComma>(iss)),
                                     std::istream_iterator<WordDelimitedByComma>());
    
        for( std::string sPart : results )
        {   
            if ( sPart.find("CALL_ID") != std::string::npos )
            {
                std::cout << sPart << std::endl;
                std::size_t init = sPart.find("='") + 2 ;
                std::size_t end = sPart.find_last_of("'");                
                strCallId = sPart.substr( init , end - init);
            }
        }

        pIvr->Log(LOG_INFO,"[%s] CallId : %s", __FUNCTION__ , strCallId.c_str());
    }
    catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_Cdr m�dule, function GetCallId()");        
    }

    return strCallId;
}
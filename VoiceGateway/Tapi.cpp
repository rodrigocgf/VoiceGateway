#include "Tapi.h"
#include "Dialogic.h"

Tapi::Tapi(boost::shared_ptr<Logger>  pLog4cpp , TAPI_PARAM *pTapiParam) : m_pLog4cpp(pLog4cpp) ,  m_pTapiParam(pTapiParam)
{
	
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	if(pTapiParam == NULL)
	{	
        m_CTIBasePtr.reset( new Dialogic(pLog4cpp) );
		return;
	}
	
    switch(m_pTapiParam->Tecnologia)
    {
    case DIALOGIC: 	
		if(pTapiParam->Channel == 0)
            m_CTIBasePtr.reset( new Dialogic(pLog4cpp) );			
		else
            m_CTIBasePtr.reset( new Dialogic(pLog4cpp, pTapiParam) );			
	    break;
    case KHOMP:
        //m_CTIBasePtr.reset( new Khomp(m_pLog4cpp) );
        break;
    }

}


void Tapi::InitSystem()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__); 

    if ( m_CTIBasePtr.get() != nullptr )
        m_CTIBasePtr->InitSystem();
}

void Tapi::EndSystem()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__); 

    if ( m_CTIBasePtr.get() != nullptr )
        m_CTIBasePtr->EndSystem();
}

CTIBase * Tapi::GetCTI() const
{
    return m_CTIBasePtr.get();
}


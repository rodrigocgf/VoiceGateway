#pragma once


#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <winsock2.h>
#include <windows.h>

#include <sql.h>
#include <sqlext.h>
#ifndef ADO_H
#define ADO_H

#include <iostream>
#include <strstream>
#include <string>
#include <ole2.h>
#include "IDBAccess.h"

#include "log4cpp/Category.hh"
#include "log4cpp/RollingFileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/Priority.hh"


//#import "C:\\Program Files\\Common Files\\System\\ado\\msado15.dll" rename_namespace("ADO") rename("EOF", "EndOfFile")

// FROM WINDOWS AT DESENV AND HOMOL MACHINES ( LAB04 AND URA-SUPPORT )
#import "Z:\\msado15.dll" rename_namespace("ADO") rename("EOF", "EndOfFile") 

// FROM WINDOWS 2012 SERVER
//#import "msado28.tlb" 

using namespace ADO;

class DBAccess : public IDBAccess
{
private:
	//std::string bsConnectionString;
    _bstr_t bsConnectionString;
	std::string bsLastErrorMessage;
    _ConnectionPtr m_pConnection;
    _RecordsetPtr pRecordSet;
    _bstr_t bstrValue;

	inline void CHECK_HRESULT( HRESULT x )
	{
		if( FAILED( x ) )
			_com_issue_error( x );
	};
	void GetProviderError();    
	void GetComError( _com_error &e );

    // LOG4CPP
	log4cpp::Category * 					m_pLog4cpp;	
    
    //log4cpp::Layout *                       m_layout;
    //log4cpp::Appender *                     m_rfileAppender;

    boost::shared_ptr<log4cpp::Layout>                       m_layoutPtr;
    boost::shared_ptr<log4cpp::Appender>                     m_rfileAppenderPtr;

    log4cpp::Category *                     m_category;

    std::string MountLogPath(const std::string & p_sBasePathLog);
    void ConfigLog4cpp(const std::string & p_sFullLogFileName );

    //std::string         m_sBaseLogPath;
    void Log(const std::string & msg);
public:
	
    //DBAccess(log4cpp::Category * pLog4cpp);
    DBAccess();
	~DBAccess();
	const char* GetLastError();
	int Open( const char* connection_string = NULL );
	int Close();
	int Execute( const char* command );
	int MoveFirst();
	int MoveNext();
	const char* GetItem( const std::string& item );
	const char* GetItem( const long& index );
	int GetRowCount();
	int GetColCount();
	int TrimAll( std::string& item );
};

#endif // ADO_H

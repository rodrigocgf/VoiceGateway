#ifndef _IDBACCESS_H_
#define _IDBACCESS_H_

class IDBAccess
{
public:	    
	virtual const char* GetLastError() = 0;
	virtual int Open( const char* connection_string = NULL ) = 0;
	virtual int Close() = 0;
	virtual int Execute( const char* command ) = 0;
	virtual int MoveFirst() = 0;
	virtual int MoveNext() = 0;
	virtual const char* GetItem( const std::string& item ) = 0;
	virtual const char* GetItem( const long& index ) = 0;
	virtual int GetRowCount() = 0;
	virtual int GetColCount() = 0;
	virtual int TrimAll( std::string& item ) = 0;
};

#endif // DB_INTERFACE_H

#ifndef TS_MANAGER_H
#define TS_MANAGER_H

// para STL
#ifdef _DEBUG
	#pragma warning(disable:4786) 
#endif

#include <string>
#include <map>
#include <list>
#include <windows.h>

class Class_TS_Manager
{
private:	

	typedef struct _TS_ALLOC_COUNTER
	{
		_TS_ALLOC_COUNTER();
		long timeslot;
		long count;
	} TS_ALLOC_COUNTER;

    std::list <long> GlobalList;
    std::list <long> LocalList;
    std::map <long, TS_ALLOC_COUNTER> GlobalMap;
    std::map <long, TS_ALLOC_COUNTER> LocalMap;    
    CRITICAL_SECTION critTS_Manager;
    bool bEnterCriticalSection;

public:
    Class_TS_Manager();
    ~Class_TS_Manager();

    long AllocateGlobalTs(long LocalTimeslot);
    long AllocateLocalTs(long GlobalTimeslot);
    long MapGlobalTs(long LocalTimeslot);
    long MapLocalTs(long GlobalTimeslot);
    int ReleaseGlobalTs(long LocalTimeslot);
    int ReleaseLocalTs(long GlobalTimeslot);
    int SetGlobalTsRange(long min, long max);
    int SetLocalTsRange(long min, long max);
    long PopLocalTs(void);
    int PushLocalTs(long timeslot);
};

#endif // TS_MANAGER_H

#include "StatusScreenNet.h"
#include "switchdefines.h"
#include <stdio.h>
#include <sys/timeb.h>
#include <time.h>


StatusScreenNet::StatusScreenNet( WPARAM wParam , ISwitch * p_Switch , IThrListen * p_ThrListen , boost::shared_ptr<Logger>   pLog4cpp ) : 
        m_pLog4cpp(pLog4cpp) ,
	    m_Socket( wParam ), 
	    m_bStop( false ), 	    
        m_pSwitch(p_Switch) , 
	    m_pThrListen(p_ThrListen)
{
	m_pLog4cpp->debug("<-v-v-v-v-v-v-v-v-v-v-v- StatusScreenNet -v-v-v-v-v-v-v-v-v-v-v-v->");    
    InitializeCriticalSection(&m_critClientScreenList);
    
    m_MonitorThreadPtr.reset ( new boost::thread ( boost::bind (&StatusScreenNet::MonitorEvents, this)));
    m_MonitorThreadPtr->detach();

    //m_PingThreadPtr.reset ( new boost::thread ( boost::bind (&StatusScreenNet::Ping, this)));
    //m_PingThreadPtr->detach();

    Start();	
}

StatusScreenNet::~StatusScreenNet(void)
{
	m_pLog4cpp->debug("<-^-^-^-^-^-^-^-^-^-^-^- StatusScreenNet -^-^-^-^-^-^-^-^-^-^-^-^-> ");
}

void StatusScreenNet::Ping()
{
    while( !m_bStop )
	{
        boost::this_thread::sleep(boost::posix_time::seconds(3));
        EnqueueWrite("?PING\r\n");
    }
}

void StatusScreenNet::MonitorEvents()
{
    EVT_COMMANDS * cmData;
    PST_DOREAD     pst_DoRead;
    PST_DOWRITE    pst_DoWrite;
    PST_ONERROR    pst_OnError;

    while( !m_bStop )
	{
		{			
			boost::mutex::scoped_lock lk(  m_EvtMutex );
			while ( m_CommandsQueue.empty() && !m_bStop )
                //m_condQueue.timed_wait( lk , boost::posix_time::milliseconds(1));
				m_condQueue.wait( lk );
		
			cmData = m_CommandsQueue.back();
			m_CommandsQueue.pop_back();
		}
		
		if ( !m_bStop)
		{
            if( cmData )
			{
				switch( cmData->Type )
				{
                    case DOWRITE:
                        pst_DoWrite = (ST_DOWRITE *)cmData->Object;
						//m_pLog4cpp->debug( "<<<< COMMAND : DOWRITE %p DEQUEUED. ", pst_DoWrite);

                        DoWrite(pst_DoWrite->sData);
                        delete ( PST_DOWRITE )cmData->Object;
                        break;
                    case DOREAD:
                        pst_DoRead = ( PST_DOREAD )cmData->Object;
						//m_pLog4cpp->debug( "<<<< COMMAND : DOREAD %p DEQUEUED. ", pst_DoRead);
                        
                        DoRead();

                        delete ( PST_DOREAD )cmData->Object;
                        break;
                    case ONERROR:
                        pst_OnError = ( PST_ONERROR )cmData->Object;
						m_pLog4cpp->debug( "<<<< COMMAND : ONERROR %p DEQUEUED. ", pst_OnError);
                        m_pLog4cpp->debug(" ERROR : %s ", pst_OnError->sErrorDescription.c_str() );
                        Stop();                        

                        delete ( PST_ONERROR )cmData->Object;                        
                        break;


                }
            }
        }

    }

    shutdown(m_Socket,2);
    m_pLog4cpp->debug("[%s] Thread STOPPED.",__FUNCTION__);
    
    // Add this to garbage collector
    //m_pIoServiceAcceptor->AddToGarbage(this);
}


void StatusScreenNet::Start( )
{   
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);    

    int _IndexScreen;
    std::stringstream ss;    
    char sName[128];
    int board, channel, resource;
	int vox_count, asr_count, conf_count, fax_count, ip_count, mc_count;
	int trunkId, trunkRef, channelRef;

    std::string sBoardResourceTypeName;
    std::string sBuffer;
    std::string sApplicationName = "SupportComm VoiceGateway ";	    
	std::string sVersion =  "Phoenix REBORN";
    
    ss << boost::format("HELLO?NAME=%s%s\r\n") % sApplicationName.c_str() % sVersion.c_str();
    EnqueueWrite(ss.str());
    
    ss.str(std::string());
    ss << "SS_START\r\n";
    EnqueueWrite(ss.str());        
    LogSys(ss.str());    

	ss.str(std::string());
    ss << boost::format("SS_SET_TITLE?TITLE=%s%s\r\n") % sApplicationName.c_str() % WinTitle().c_str();    
    EnqueueWrite(ss.str());    
    LogSys(ss.str());
    
    _IndexScreen = 1;
    m_pSwitch->SetIndexScreen(_IndexScreen);
    ss.str(std::string());
    ss << boost::format("SS_ADD_VIEWER?NAME=Log\r\n");
    EnqueueWrite(ss.str());
    LogSys(ss.str());

    ss.str(std::string());
    ss << boost::format("SS_SET_CHANEL_COLUMN?ID=%d&STATUS=Info\r\n") % _IndexScreen;
    EnqueueWrite(ss.str());
    LogSys(ss.str());    

    _IndexScreen++;
    m_pSwitch->SetIndexScreen(_IndexScreen);
    ss.str(std::string());
    ss << boost::format("SS_ADD_VIEWER?NAME=Telnet\r\n");
    EnqueueWrite(ss.str());
    LogSys(ss.str());

    ss.str(std::string());
    ss << boost::format("SS_SET_CHANEL_COLUMN?ID=%d&STATUS=Info\r\n") % _IndexScreen;    
    EnqueueWrite(ss.str());
    LogSys(ss.str());
        

    _IndexScreen++;
    m_pSwitch->SetIndexScreen(_IndexScreen);
    ss.str(std::string());
    ss << boost::format("SS_ADD_VIEWER?NAME=IP\r\n");
    EnqueueWrite(ss.str());
    LogSys(ss.str());

    ss.str(std::string());
    ss << boost::format("SS_SET_CHANEL_COLUMN?ID=%d&STATUS=Info\r\n") % _IndexScreen;
    EnqueueWrite(ss.str());
    LogSys(ss.str());


    // Varre a estrutura de placas
	for(board = 1; board <= m_pSwitch->GetNumBoardsInDataBase(); board++)
	{
		//sprintf(sName, "%s Id_%02d", DbBoards[board].BoardName.c_str(), board);
        sprintf(sName, "%s Id_%02d", m_pSwitch->GetBoardName(board).c_str(), board);


		m_pLog4cpp->debug("Opening %s", sName);
		vox_count = asr_count = conf_count = fax_count = ip_count = mc_count = 0;


		// Varre a estrutura de recursos
		for(resource = 0; resource < m_pSwitch->GetResourceCount(board); resource++)
		{
            sBoardResourceTypeName = m_pSwitch->GetBoardResourceTypeName(board, resource);
            if ( !sBoardResourceTypeName.compare("VOX") )
            {
                vox_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } else if (!sBoardResourceTypeName.compare("ASR")) {
                asr_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } else if (!sBoardResourceTypeName.compare("FAX")) {

                fax_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } else if (!sBoardResourceTypeName.compare("CONF")) {
                conf_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } else if (!sBoardResourceTypeName.compare("MULTICHASSIS")) {
                mc_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } else if (!sBoardResourceTypeName.compare("IP")) {
                ip_count = m_pSwitch->GetBoardResourceCount(board, resource);
            } 
		}

        m_pLog4cpp->debug("Trunk count [ board %d] : %d", board, m_pSwitch->GetTrunkCount(board) );

		if( m_pSwitch->GetTrunkCount(board) <= 0)
		{
            m_pLog4cpp->debug("Channel count [ board %d] : %d", board, m_pSwitch->GetChannelCount(board) );

            ss.str(std::string());
            ss << boost::format("SS_ADD_BOARD?NAME=%s&TRUNK_COUNT=%d&CHANNEL_COUNT=%d&VOX_COUNT=%d&ASR_COUNT=%d&CONF_COUNT=%d&FAX_COUNT=%d&IP_COUNT=%d&MC_COUNT=%d\r\n") % 
                sName % 0 % m_pSwitch->GetChannelCount(board) % vox_count % asr_count % conf_count % fax_count % ip_count % mc_count;
            EnqueueWrite(ss.str());
            LogSys(ss.str());
            m_pLog4cpp->debug("[SENT] %s", ss.str().c_str() );
		}
		else
		{
			ss.str(std::string());
            ss << boost::format("SS_ADD_BOARD?NAME=%s&TRUNK_COUNT=%d&CHANNEL_COUNT=%d&VOX_COUNT=%d&ASR_COUNT=%d&CONF_COUNT=%d&FAX_COUNT=%d&IP_COUNT=%d&MC_COUNT=%d\r\n") %
                sName % m_pSwitch->GetTrunkCount(board) % m_pSwitch->GetChannelCount(board) % vox_count % asr_count % conf_count % fax_count % ip_count % mc_count;
            EnqueueWrite(ss.str());
            LogSys(ss.str());
            m_pLog4cpp->debug("[SENT] %s", ss.str().c_str() );
		}

	}


    // Seta o status dos canais
	for(channel = 1; channel <= m_pSwitch->GetNumPortsInDataBase(); channel++)
	{
		board =  m_pSwitch->GetBoardId(channel); 
		trunkId = m_pSwitch->GetTrunkId(channel); 
		trunkRef = m_pSwitch->GetTrunksRef(trunkId); 
		channelRef = m_pSwitch->GetTrunkChannelID(channel);

		if( m_pSwitch->IsChannelEnabled(channel) )
		{
			std::string ani;
			std::string dnis;

			int channelStatus = m_pSwitch->WinStatusChannel( channel, ani, dnis );

            ss.str(std::string());
            ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n") %
                (_IndexScreen + board ) %	
                m_pSwitch->GetBoardTrunkID(channel) %     
                m_pSwitch->GetTrunkChannelID(channel) %                
                m_pSwitch->StatusChannel( channelStatus ) ;

            EnqueueWrite(ss.str());
            LogSys(ss.str());

            
			switch(channelStatus)
			{
				case ICON_OFFERED:
				case ICON_ANSWERED:
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_CALL_TYPE, "IN");
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_ANI, ani.c_str());
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_DNIS, dnis.c_str());
					break;					

				case ICON_ALERTING:
				case ICON_DIALING:
				case ICON_CONNECTED:
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_CALL_TYPE, "OUT");
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_ANI, ani.c_str());
					SendStringGrid(_IndexScreen + board, trunkRef, channelRef, IVR_DNIS, dnis.c_str());
					break;					
			}
            
		}
		else
		{
            ss.str(std::string());
            ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n") % 
                (_IndexScreen + board) %	
                m_pSwitch->GetBoardTrunkID(channel) %
                m_pSwitch->GetTrunkChannelID(channel) %                
                m_pSwitch->StatusChannel( ICON_INACTIVE );
		}
	}
    
    //EnqueueRead();    
}

void StatusScreenNet::SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg)
{
    std::stringstream ss;

	try
	{
        if ( m_pThrListen->GetSwitch()->GetFlagLogScreen() <= 0 )
        {
            return;
        }

		switch(Grid)
		{
			case 1:
				//Win::SendMessageToScreen( "SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n", ListIndex, Trunk, Item, sMsg );
				break;
			case 2:
                ss.str(std::string());
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&TYPE=%s\r\n") % ListIndex % Trunk % Item % sMsg; 
                m_pLog4cpp->debug(ss.str().c_str());
                EnqueueWrite(ss.str());
				break;
			case 3:
                ss.str(std::string());
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&DNIS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
                m_pLog4cpp->debug(ss.str().c_str());
                EnqueueWrite(ss.str());
				break;
			case 4:
                ss.str(std::string());
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&ANI=%s\r\n") % ListIndex % Trunk % Item % sMsg;
                m_pLog4cpp->debug(ss.str().c_str());
                EnqueueWrite(ss.str());
				break;
			case 5:
                ss.str(std::string());
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&INFO=%s\r\n") % ListIndex % Trunk % Item % sMsg;
                m_pLog4cpp->debug(ss.str().c_str());
                EnqueueWrite(ss.str());
				break;
			case 6:
                ss.str(std::string());
                ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&BITS=%s\r\n") % ListIndex % Trunk % Item % sMsg;
                m_pLog4cpp->debug(ss.str().c_str());
                EnqueueWrite(ss.str());
				break;
		}
	}
	catch(...)
	{
	}
}

void StatusScreenNet::Stop()
{
    m_pLog4cpp->debug("[%s]",__FUNCTION__);
        
    m_bStop = true;    
    m_MonitorThreadPtr->join();
    //m_PingThreadPtr->join();
        
}

void StatusScreenNet::EnqueueError( const std::string & sErrorMessage )
{
    m_pLog4cpp->debug("[%s]",__FUNCTION__);
    PST_ONERROR pst_OnError = new ST_ONERROR();
    pst_OnError->sErrorDescription.assign(sErrorMessage);
    AddCommand(pst_OnError);
}

void StatusScreenNet::EnqueueWrite( const std::string & Message )
{
    if ( Message.length() > 0 )
    {
        PST_DOWRITE pst_Write = new ST_DOWRITE();
        pst_Write->sData.assign(Message);    
        AddCommand(pst_Write);

        //boost::this_thread::sleep(boost::posix_time::milliseconds(100));       
    }
}

void StatusScreenNet::EnqueueRead()
{
    PST_DOREAD pst_Read = new ST_DOREAD();    
    AddCommand(pst_Read);
}

void StatusScreenNet::LogSys(const std::string  & message )
{
    int len = 0;
    char szMessage[10000];
    tm * Time;
    _timeb TimeBuffer;

    _ftime( &TimeBuffer );
	Time = localtime( &TimeBuffer.time );
    memset(szMessage , 0x00 , sizeof(szMessage) );

	// Armazena a data e hora da ocorr�ncia antes de colocar a mensagem
	len = sprintf( szMessage,"%04d-%02d-%02d %02d:%02d:%02d.%03d", Time->tm_year+1900, Time->tm_mon + 1, Time->tm_mday, Time->tm_hour, Time->tm_min, Time->tm_sec, TimeBuffer.millitm );
	// Concatena com o tipo, origem, detalhes e com o texto da msg. propriamente dito
	len += sprintf( &szMessage[len], "|---|GTW|               |%s", message.c_str() );

    m_pLog4cpp->debug("[%s][%s]",__FUNCTION__ , szMessage);	

	LogScreenSystem(szMessage);
}



void StatusScreenNet::LogScreenSystem(char *s)
{
    std::stringstream ss;

    ss << boost::format("SS_LOG_VIEWER?ID=%d&TEXT=%s\r\n") % SCR_LOG % s;
    EnqueueWrite(ss.str());
}

void StatusScreenNet::DoWrite( const std::string & Message )
{       
    //m_pLog4cpp->debug("[%s][%s]",__FUNCTION__, Message.c_str());
    int i_BytesSent = 0;
    

    try
    {        
        i_BytesSent = Send( Message.c_str() , Message.length() );
        //m_pLog4cpp->debug("[%s] %d bytes sent.",__FUNCTION__, i_BytesSent );        
    }
    catch(...)
    {
        EnqueueError("WRITE ERROR");
    }
    
}


void StatusScreenNet::DoRead()
{    
    m_pLog4cpp->debug("[%s]",__FUNCTION__);

    try
    {        

        //m_pLog4cpp->debug("[%s] Received : [%s]",__FUNCTION__, m_sReadMessage.c_str() );    
    }
    catch(...)
    {
        EnqueueError("READ ERROR");
    }
}

int StatusScreenNet::Send(const char *Buffer, int Tamanho)
{
	int BytesEnviados;
	int TotalEnviado = 0;
	DWORD dw_res;
	
	mError = 0;
	
	while(TotalEnviado < Tamanho)
	{
		if( !WaitToSend(TIMEOUT_SENDRECEIVE) ) {
			if(mError)  return 0;
			else		continue;
		}

		BytesEnviados = send(m_Socket, &Buffer[TotalEnviado], 
			Tamanho - TotalEnviado, 0);

		if ( BytesEnviados == SOCKET_ERROR ) {
			dw_res = WSAGetLastError();
			if ( dw_res != WSAEWOULDBLOCK ) {
				mError = dw_res;
				return BytesEnviados;
			}
		}

		if(!BytesEnviados) 
			continue;

		TotalEnviado += BytesEnviados;
	}
	return TotalEnviado;
}

int StatusScreenNet::WaitToSend(int nTimeOut)
{
	FD_SET fdsSend;
	FD_SET fdsExcept;
	TIMEVAL tvSend = {nTimeOut, 0};
	int nSockets;
	
	mError = 0;
	
	FD_ZERO(&fdsSend);
	FD_ZERO(&fdsExcept);
	FD_SET(m_Socket, &fdsSend);
	FD_SET(m_Socket, &fdsExcept);
	
	//  Verifica se pode enviar dados pela rede
	nSockets = select(NULL, NULL, &fdsSend, &fdsExcept, &tvSend);
	
	//  Saiu por timeout
	if(nSockets == 0) {		
		mError = WSAETIMEDOUT;		
		return 0;
	}
	
	//  Verifica se ocorreu algum erro
	if( (nSockets == SOCKET_ERROR) || FD_ISSET(m_Socket, &fdsExcept) ) {
		mError = WSAGetLastError();
		//if((mError = WSAGetLastError()) == 0)
		//	mError = WSAECONNRESET;
		return 0;
	}
	return 1;
}

/************************************************************************************************
  M�todo: WinTitle()                                                                                              
  Descr.: Fun��o chamada obter o titulo da janela
************************************************************************************************/
std::string StatusScreenNet::WinTitle(void)
{
	struct _timeb TimeBuffer;
	struct tm *ptm;
    char szTitulo[256] = {0};
    std::string sTitulo;    

	try 
	{
		
		// coloca no titulo a data de inicio da aplicacao
		_ftime( &TimeBuffer );
		ptm = localtime( &TimeBuffer.time );

		// Coloca o titulo
		sprintf(szTitulo,"  [Started in %04d-%02d-%02d %02d:%02d]",											
							ptm->tm_year+1900,
							ptm->tm_mon + 1,
							ptm->tm_mday,
							ptm->tm_hour,
							ptm->tm_min);
		
        sTitulo.assign(szTitulo);
		return sTitulo;
	}
	catch(...)
	{
		m_pLog4cpp->debug("EXCEPTION: Switch::WinTitle()");
		return "";
	}
}

/*********************************************************************************************
	LuaGeneric.cpp
*********************************************************************************************/

#include <winsock2.h> // Usado para o winsocket
#include <direct.h>
#include "Ivr.h"
#include "Switch.h"

#include <sys/timeb.h>
#include "Switch.h"
#include <lua.hpp>

//-------------------------------------------------------------------------------------------------------------------------

int LuaTime						        (lua_State *L);
int LuaTick						        (lua_State *L);
int LuaFindFiles			            (lua_State *L);
int LuaGetDateTime		                (lua_State *L);
int LuaCallDetail			            (lua_State *L);
int LuaCdrCallDetail	                (lua_State *L);
int LuaCreateDir			            (lua_State *L);
int LuaRemoveDir			            (lua_State *L);
int LuaFindIndexFile	                (lua_State *L);
int LuaHoliday				            (lua_State *L);
int LuaOpenCdr				            (lua_State *L);
int LuaGetLocalIP			            (lua_State *L);
int LuaGetDataSourceName                (lua_State *L);
int LuaGetDatabaseName                  (lua_State *L);
int LuaGetDatabaseUser                  (lua_State *L);
int LuaGetDatabasePassword              (lua_State *L);
int LuaGetFileSize                      (lua_State *L);
int LuaGetLogCDREnabled                 (lua_State *L);
int LuaGetChannel						(lua_State *L);
int LuaGetMachine						(lua_State *L);
int LuaGetSwitch						(lua_State *L);
int LuaGetCallId						(lua_State *L);
int LuaPrintStatus					    (lua_State *L);
int LuaPrintAni							(lua_State *L);
int LuaPrintDnis						(lua_State *L);
int LuaPrintBits						(lua_State *L);
int LuaSleep							(lua_State *L);

//-------------------------------------------------------------------------------------------------------------------------
// Registra as funcoes a serem interpretadas pelo engine do lua

static const struct luaL_reg MyLibFunctions[] = 
{
	{"Time",					LuaTime	                },
	{"Tick",					LuaTick					},
	{"FindFiles",				LuaFindFiles			},
	{"GetDateTime",				LuaGetDateTime		    },
	{"CallDetail",				LuaCallDetail			},
	{"CdrCallDetail",			LuaCdrCallDetail	    },
	{"CreateDir",				LuaCreateDir			},
	{"RemoveDir",				LuaRemoveDir			},
	{"FindIndexFile",			LuaFindIndexFile	    },
	{"Holiday",					LuaHoliday				},
	{"OpenCdr",					LuaOpenCdr				},
    {"GetLocalIP",				LuaGetLocalIP 		    },
    {"GetDataSourceName",		LuaGetDataSourceName	},
    {"GetDatabaseName",			LuaGetDatabaseName	    },
    {"GetDatabaseUser",			LuaGetDatabaseUser	    },
    {"GetDatabasePassword",	    LuaGetDatabasePassword	},
    {"GetFileSize",				LuaGetFileSize	        },
    {"GetLogCDREnabled",		LuaGetLogCDREnabled	    },
    {"GetChannel",				LuaGetChannel			},
	{"GetMachine",				LuaGetMachine			},
	{"GetSwitch",				LuaGetSwitch			},
	{"GetCallId",				LuaGetCallId			},
    {"PrintStatus",				LuaPrintStatus			},
	{"PrintAni",				LuaPrintAni				},
	{"PrintDnis",				LuaPrintDnis			},
    {"PrintBits",               LuaPrintBits            },
	{"Sleep",					LuaSleep                }
};

Ivr * GetIvrGen( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );
    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return pIvr;
}

//-------------------------------------------------------------------------------------------------------------------------

// Inicia biblioteca das funcoes para variaveis estaticas do Lua
void Lua_GenericLibOpen(lua_State *L, const char *sCallId, const char *sVoiceMailBox)
{    
	luaL_openl(L, MyLibFunctions);

	// Seta variavel de sistema para a ligacao corrente com o CallId da ligacao
	lua_pushstring(L, sCallId);
    lua_setglobal(L, "_IdCall");

	// Seta variavel de sistema para o numero da caixa de voz do VoiceMail para deposito de mensagens
	lua_pushstring(L, sVoiceMailBox);
    lua_setglobal(L, "_VoiceMailBox");
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaTime(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {
		
		lua_pushnumber (L, time(NULL) );
		return 1;
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaTime()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaTick(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		lua_pushnumber (L, GetTickCount() );
		return 1;
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaTick()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaFindFiles(lua_State *L)
{
	char p1[256];
	WIN32_FIND_DATA find_data;
	HANDLE h_file = INVALID_HANDLE_VALUE;
	int cont=0, i;
    LPWSTR ptr;

    wchar_t wtext[300];    
    char szFileName[300];

    memset(szFileName , 0x00, sizeof( szFileName ) );

	Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		strcpy(p1, luaL_check_string(L, 1));

        pIvr->Log(LOG_SCRIPT, "[%s] %s", __FUNCTION__ , p1);

        mbstowcs(wtext, p1, strlen(p1)+1);//Plus null
        ptr = wtext;

		h_file = FindFirstFile(ptr,&find_data);

		// contando quando arquivos existem
		if ( h_file != INVALID_HANDLE_VALUE) 
        {
			do
            {   
				cont++;
			}while(FindNextFile(h_file,&find_data) != 0);
        }

		lua_pushnumber	(L, cont);

		// Fecha o Handle da procura anterior
		FindClose(h_file);

		// listando nome dos arquivos encontrados
		h_file = FindFirstFile(ptr,&find_data);
		
        pIvr->Log(LOG_SCRIPT, "[%s] FOUND FILES (%d): ", __FUNCTION__ , cont);

		for(i=1;i<=cont;i++)
        {
            memset(szFileName , 0x00, sizeof( szFileName ) );
            sprintf(szFileName , "%ws", find_data.cFileName);

            pIvr->Log(LOG_SCRIPT, "[%s] %s", __FUNCTION__ , szFileName);

            lua_pushstring (L, szFileName);
			FindNextFile(h_file,&find_data);
		}

		FindClose(h_file);
		return cont+1;
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaFindFiles()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetDateTime(lua_State *L)
{
    char szTime[40];
    time_t lBeginTime;    
    struct tm *actualTimer;

    memset( szTime, 0x00, sizeof(szTime) );
    time(&lBeginTime);
    actualTimer = localtime(&lBeginTime);

	sprintf( szTime ,"%04d-%02d-%02d %02d:%02d:%02d.%03d", actualTimer->tm_year+1900, actualTimer->tm_mon + 1, actualTimer->tm_mday, 
                                                     actualTimer->tm_hour, actualTimer->tm_min, actualTimer->tm_sec, 0 );
    
    lua_pushstring(L, szTime);
    return 1;
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaCallDetail(lua_State *L)
{
	int iNumArgs, iEvento, iRet;
	const char *sDataInicio;
	const char *sDataFim;

    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 3)
        {
			return 0;
		}

		iEvento			= luaL_check_int(L, 1);
		sDataInicio	= luaL_check_string(L, 2);
		sDataFim		= luaL_check_string(L, 3);

		iRet = pIvr->DbCallDetail(iEvento, (char *)sDataInicio, (char *)sDataFim);

		lua_pushnumber (L, iRet);
		return 1;
	}
	catch(...){
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaCallDetail()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaCdrCallDetail(lua_State *L)
{
	int iNumArgs, iEvento;
	const char *sDataInicio;
	const char *sDataFim;

    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 3){
			return 0;
		}

		iEvento			= luaL_check_int(L, 1);
		sDataInicio	= luaL_check_string(L, 2);
		sDataFim		= luaL_check_string(L, 3);

		pIvr->PushCdrDetail(iEvento, (char *)sDataInicio, (char *)sDataFim);

		return 0;
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaCdrCallDetail()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaCreateDir(lua_State *L)
{
	// Declaracao das variaveis
	char path[256];
	int iNumArgs;
	
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "CreateDir()");
		
		//Verifica��o do numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 1){
			return 0;
		}
		
		strcpy(path, luaL_check_string(L, 1));
		
		//Cria��o de diret�rio
		if( _mkdir(path) == 0 )
			lua_pushnumber (L, 0);
		else
			lua_pushnumber (L, 1);
		return 1;
	}
	catch(...){
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaCreateDir()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaRemoveDir(lua_State *L)
{
	// Declaracao das variaveis
	char path[256];
	int iNumArgs;
	
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "RemoveDir()");
		
		iNumArgs = lua_gettop(L);

		//Verifica��o do numero de argumentos
		if(iNumArgs < 1){
			return 0;
		}
		
		strcpy(path, luaL_check_string(L, 1));
		
		//Remo��o de diret�rio
		if( _rmdir(path) == 0 )
			lua_pushnumber (L, 0);
		else
			lua_pushnumber (L, 1);
		return 1;
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRemoveDir()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaFindIndexFile(lua_State *L)
{
	char p1[256];
	WIN32_FIND_DATA find_data;
	HANDLE h_file = INVALID_HANDLE_VALUE;
    LPWSTR ptr;
	int i;
	int index;
	
    wchar_t wtext[300];
    char szFileName[300];

    memset(szFileName , 0x00, sizeof( szFileName ) );

    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {

		pIvr->Log(LOG_SCRIPT, "[%s]", __FUNCTION__ );

		strcpy(p1, luaL_check_string(L, 1));
		index = luaL_check_int(L, 2);
        mbstowcs(wtext, p1, strlen(p1)+1);//Plus null
        ptr = wtext;

		h_file = FindFirstFile(ptr,&find_data);

		if (h_file!=INVALID_HANDLE_VALUE)
		{
			for(i=1;i<index;i++)
			{
				if (FindNextFile(h_file,&find_data) == 0 )
				{
					FindClose(h_file);
					lua_pushnumber	(L, 0);
					return 1;
				}
			}
            
            sprintf( szFileName , "%ws" , find_data.cFileName );
            pIvr->Log(LOG_SCRIPT, "[%s] File : %s", __FUNCTION__ , szFileName );
            lua_pushstring (L , szFileName );	
		}
		else{
			lua_pushnumber	(L, 0);
		}

		FindClose(h_file);
		return 1;
	}
	catch(...){
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaFindIndexFile()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaHoliday(lua_State *L)
{
	// Declaracao das variaveis
	int iNumArgs, iDia, iMes, iAno, iResult;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {

		pIvr->Log(LOG_SCRIPT, "Holiday()");
		
		iNumArgs = lua_gettop(L);

		// Verifica��o do numero de argumentos
		if(iNumArgs < 1){
			return 0;
		}
		
		iDia = luaL_check_int(L, 1);
		iMes = luaL_check_int(L, 2);
		iAno = luaL_check_int(L, 3);

		iResult = 0;
		
		pIvr->DbHoliday(iDia, iMes, iAno, iResult);
		
		lua_pushnumber(L, iResult);
		return 1;
	}
	catch(...){
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaHoliday()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaOpenCdr(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {

		pIvr->Log(LOG_SCRIPT, "OpenCdr()");
		
		// inicia o CDR para as versoes de Switch que precisam inserir registros
		// de CDR no inicio da ligacao
		pIvr->LuaIniciaCdr();

		return 0;
	}
	catch(...){
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaOpenCdr()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------
int LuaGetLocalIP(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
        char szIPAddress[128] = "";        // Guarda o endere�o IP local em formato string
    
        strcpy( szIPAddress, pIvr->GetSwitch()->GetLocalIpAddress().c_str() );

		lua_pushstring( L, szIPAddress );
		return 1; 
	}
	catch(...)
    {
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetLocalIP()");
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetDataSourceName(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try
	{
		lua_pushstring( L, pIvr->GetSwitch()->GetOdbcDataSourceName().c_str() );
		return 1; 
	}
	catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetDataSourceName()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetDatabaseName(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try
	{
		lua_pushstring( L, pIvr->GetSwitch()->GetOdbcDataBase().c_str() );
		return 1; 
	}
	catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetDatabaseName()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetDatabaseUser(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try
	{
		lua_pushstring( L, pIvr->GetSwitch()->GetOdbcUser().c_str() );
		return 1; 
	}
	catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaDatabaseGetUser()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetDatabasePassword(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try
	{
		lua_pushstring( L, pIvr->GetSwitch()->GetOdbcPassWord().c_str() );
		return 1; 
	}
	catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetDatabasePassword()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetFileSize(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try
	{
		int iNumArgs = 0;
		FILE* file = NULL;
		const char *lpcszFilename = NULL;
		long lFileSize = 0;

		iNumArgs = lua_gettop( L );

		//Verifica��o do numero de argumentos
		if( iNumArgs < 1 )
		{
			pIvr->Log( LOG_ERROR, "LuaGetFileSize(): invalid number of arguments." );
			return 0;
		}
		
		lpcszFilename = luaL_check_string(L, 1);

		pIvr->Log( LOG_SCRIPT, "LuaGetFileSize( %s )", lpcszFilename );

		if( ( file = fopen( lpcszFilename, "rb" ) ) == NULL )
		{
			pIvr->Log( LOG_ERROR, "LuaGetFileSize(): cannot open the file specified." );
			return 0;
		}

		fseek( file, 0, SEEK_END );
		lFileSize = ftell( file );

		fclose( file );

		lua_pushnumber( L, lFileSize );
		return 1;
	}
	catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetFileSize()" );
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------------------------------

int LuaGetLogCDREnabled(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

    try
	{
        int iRet;
    
        // Recupera o do registre se a switch ir� gravar o inicio e o fim do log cdr
        iRet = pIvr->GetLogCDREnabled();
        lua_pushnumber (L, iRet);
		return 1;

    }
    catch(...)
	{
		pIvr->Log( LOG_ERROR, "EXCEPTION: LuaGetLogCDREnabled()" );
		return 0;
	}
}

int LuaGetChannel(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {		

		pIvr->Log(LOG_SCRIPT, "GetChannel()");

        iRet = pIvr->GetCTI()->GetChannel();

		lua_pushnumber (L, iRet);

		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetChannel()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaGetMachine(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "GetMachine()");

        iRet = pIvr->GetSwitch()->GetMachine();

		lua_pushnumber (L, iRet);

		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetMachine()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaGetSwitch(lua_State *L)
{
	int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "GetSwitch()");

        iRet = pIvr->GetSwitch()->GetSwitch();

		lua_pushnumber (L, iRet);

		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetSwitch()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaGetCallId(lua_State *L)
{
	timeb TimeBuffer;
    std::string sCallId;
    //char szCallId[256] = {0};
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try {		
        /*
		::ftime( &TimeBuffer );

        sprintf(sCallId,"%02d%02d%03d%10lu%03d",    pIvr->GetSwitch()->GetSwitch(),
                                                    pIvr->GetSwitch()->GetMachine(),
                                                    pIvr->GetCTI()->GetChannel(),
                                                    TimeBuffer.time,TimeBuffer.millitm);
        */
        sCallId = pIvr->GetCallId();

        pIvr->Log(LOG_INFO, "LuaGetCallId() - CallId: %s", sCallId.c_str());

        lua_pushstring(L, sCallId.c_str());
		//lua_pushstring(L, szCallId);

		return 1;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetCallId()");
		return 0;
	}
}

int LuaPrintStatus(lua_State *L)
{
	int iNumArgs;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// Coloca um string na tela de status dos canais de atendimento no campo Service
		pIvr->LuaPrintStatus( luaL_check_string(L, 1) );

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPrintStatus()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaPrintAni(lua_State *L)
{
	int iNumArgs;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// Coloca um string na tela de status dos canais de atendimento no campo Ani
		pIvr->LuaPrintAni( luaL_check_string(L, 1) );

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPrintAni()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaPrintDnis(lua_State *L)
{
	int iNumArgs;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// Coloca um string na tela de status dos canais de atendimento no campo Dnis
		pIvr->LuaPrintDnis( luaL_check_string(L, 1) );

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPrintDnis()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaPrintBits(lua_State *L)
{
	int iNumArgs;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		// Coloca um string na tela de status dos canais de atendimento no campo Dnis
		pIvr->LuaPrintBits( luaL_check_string(L, 1) );

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPrintDnis()");
		return 0;
	}
}

//---------------------------------------------------------------------------------------------

int LuaSleep(lua_State *L)
{
	int iNumArgs;
    Ivr * pIvr = NULL;
    pIvr = GetIvrGen(L);

	try 
    {
		pIvr->Log(LOG_SCRIPT, "Sleep()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			return 0;
		}

		Sleep( luaL_check_int(L, 1) );

		return 0;
	}
	catch(...)
    {		
		pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSleep()");
		return 0;
	}
}

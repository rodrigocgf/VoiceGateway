#ifndef _ITHR_LISTEN_
#define _ITHR_LISTEN_



class StatusScreenNet;

class IThrListen
{
public:
    
    virtual ISwitch * GetSwitch() const = 0;
};

#endif
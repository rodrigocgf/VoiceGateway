#ifndef _ITHR_TELNET_LISTEN_
#define _ITHR_TELNET_LISTEN_


class Sessions;
class TelnetSession;
class ISwitch;

class IThrTelnetListen
{
public:    
    virtual ISwitch * GetSwitch() const = 0;    
    virtual bool interactivePrompt() const = 0;
    virtual void promptString(std::string prompt) = 0;
    virtual std::string promptString() const = 0;
    virtual std::function< void( TelnetSession *) > OnConnectedCallback() const = 0;
    virtual std::function< void(TelnetSession *, std::string) > OnNewLineCallBack() const = 0;
    virtual bool ValidCircuit( const int ichannel ) = 0;
    virtual int StateCircuit( const int ichannel ) = 0;
    virtual void DialOnCircuit( const int ichannel, char *DNIS, char *ANI) = 0;
    virtual int DisconnectCircuit( const int ichannel1, const int ichannel2 ) = 0;
    virtual int BlockCircuit( const int ichannel1, const int ichannel2 ) = 0;
    virtual int UnBlockCircuit( const int ichannel1, const int ichannel2 ) = 0;
    virtual void AddToGarbageCollector(void * pObj) = 0;
};

#endif
#ifndef _Lua_CTI_h_
#define _Lua_CTI_h_


#include <lua.hpp>

void Lua_GenericLibOpen(lua_State *L, const char *sCallId, const char *sVoiceMailBox);

#endif
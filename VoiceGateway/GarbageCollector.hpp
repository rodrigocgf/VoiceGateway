
#ifndef __GARBAGECOLLECTOR_H__
#define __GARBAGECOLLECTOR_H__

#include <string>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>


#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

#include "StatusScreenNet.h"
#include "TelnetSession.h"

#include "Logger.h"

typedef enum CollectableObjects
{
    STATUSSCREENNET,
    TELNETSESSION
} ObjectType;

struct GCData
{
    ObjectType Type;
    void* Object;
};


class GarbageCollector 
{
private:
    
    boost::mutex                		m_GCMutex;
    std::deque<GCData *>        		ObjectsList;
    boost::condition            		m_cond;
    volatile bool               		m_stop;    
		
	boost::shared_ptr<Logger> 				m_pLog4cpp;	

    boost::shared_ptr<boost::thread> m_ExecuteThreadPtr;
public:	    
	GarbageCollector(boost::shared_ptr<Logger> pLog4cpp) : m_pLog4cpp(pLog4cpp) , m_stop(false)
	{
        m_pLog4cpp->debug("[%s]",__FUNCTION__);
		//m_pLog4cpp = &log4cpp::Category::getInstance( std::string("MAIN") ); 
		
		Execute();
	}
	
    ~GarbageCollector() 
	{
		m_stop = true;
		m_cond.notify_one();
		m_ExecuteThreadPtr->join();
	}
	
    GarbageCollector( GarbageCollector& );
    GarbageCollector& operator=( GarbageCollector const& );
    
    void Execute()
	{
		m_ExecuteThreadPtr.reset ( new boost::thread ( boost::bind (&GarbageCollector::RunThread, this  )));	
	}
private:	
    void RunThread()
	{
		std::stringstream ss;
		GCData * gcData;
		
		while( !m_stop )
		{
			{
				boost::mutex::scoped_lock lk(  m_GCMutex );
				while ( ObjectsList.empty() && !m_stop )
					m_cond.wait( lk );
			
				gcData = ObjectsList.back();
				ObjectsList.pop_back();
			}			
			
			if ( !m_stop)
			{
				if( gcData )
				{
					switch( gcData->Type )
					{
					case STATUSSCREENNET:						
						ss.str(std::string());
						ss << boost::format("Deleting StatusScreenNet : %p ") % gcData->Object;
						m_pLog4cpp->debug( ss.str().c_str() );
						delete (StatusScreenNet *) gcData->Object;
						break;
					
                    case TELNETSESSION:
                        ss.str(std::string());
						ss << boost::format("Deleting TelnetSession : %p ") % gcData->Object;
						m_pLog4cpp->debug( ss.str().c_str() );
						delete (TelnetSession *) gcData->Object;
                        break;
					default:
						break;
					}
					delete gcData;
					gcData = nullptr;
				}
			}

		}
	}
public:
    /*
    static GarbageCollector& GetInstance()
	{
		static GarbageCollector myInstance;
		return myInstance;
	}
    */
	
    template<typename T> void AddToGarbage( T* Object )
	{
		std::stringstream ss;
		GCData* gcData = new GCData;
		if( gcData )
		{			
			gcData->Object = Object;
			
			std::string strType = typeid(T).name();
			if ( strType.find("StatusScreenNet") != std::string::npos )
			{
				gcData->Type = STATUSSCREENNET;				
				ss.str(std::string());
				ss << boost::format("Adding StatusScreenNet %p to Garbage collection.") % Object;
				m_pLog4cpp->debug( ss.str().c_str() );
			} else if ( strType.find("TelnetSession") != std::string::npos )
			{
				gcData->Type = TELNETSESSION;				
				ss.str(std::string());
				ss << boost::format("Adding TelnetSession %p to Garbage collection.") % Object;
				m_pLog4cpp->debug( ss.str().c_str() );
			}
			
			boost::mutex::scoped_lock lk(  m_GCMutex );			
			ObjectsList.push_front( gcData );				
			m_cond.notify_one();
		}
	}
};

#endif 

#include "LuaAPI.h" 
#include "Lua_XML.h"
#include "LUA_Cdr.h"
#include "Lua_DB.h"
#include "Lua_CTI.h"
#include "Lua_Audio.h"
#include "Lua_Generic.h"
#include "Lua_Conference.h"

#include "XMLParser.h"

#include <lua.hpp>
#include <iostream>
#include <time.h>
#include <direct.h>
#include <fstream>
#include <io.h>

#include <lua.hpp>
#include <sys\timeb.h>
#include "Ivr.h"
#include "Switch.h"
#include <setjmp.h>

extern "C"
{
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
	#include "luadebug.h"
}

struct lua_sclongjmpnode
{
    jmp_buf env_buffer;
    struct lua_sclongjmpnode* previous;
};

struct lua_scjmpstack
{
    lua_sclongjmpnode* top;
};

struct lua_sclongjmpnode* GetNode( lua_State* L )
{
    struct lua_sclongjmpnode* node;
    lua_getglobal( L, "__Node" );
    if( lua_isuserdata( L, -1 ) )
        node = static_cast<struct lua_sclongjmpnode*>( lua_touserdata( L, -1 ) );
    else
        node = NULL;
    lua_remove( L, -1 );
    return node;
}


int lua_ExecFile( Ivr * p_Ivr , char const* Filename )
{
    std::string ScriptPath;					// Diretorio do script
    int iRet = 0;
    string sVoiceMailBox;   
    XmlParser   xmlParser;

    //log4cpp::Category * p_Cdrcategory;    
    //log4cpp::Layout * p_CdrpatternLayout;   

    //p_CdrpatternLayout = new log4cpp::PatternLayout();   
    //p_Cdrcategory = &log4cpp::Category::getRoot().getInstance("CDR");    


    p_Ivr->Log(LOG_INFO,"lua_ExecFile()");

    struct lua_sclongjmpnode node;
    struct lua_scjmpstack jmpStack;
	jmpStack.top = NULL;
    node.previous = jmpStack.top;

    lua_State* newState = lua_open( 65536 );


    lua_pushuserdata( newState, &xmlParser );              
    lua_setglobal( newState, "__XmlParser");

    //lua_pushuserdata( newState, p_Cdrcategory);
    //lua_setglobal( newState , "__Category");

    //lua_pushuserdata( newState, p_CdrpatternLayout);
    //lua_setglobal( newState , "__PatternLayout");

    lua_pushuserdata( newState, p_Ivr );              // save the class that handles de call related to the scpript
    lua_setglobal( newState, "__Ivr" );

    lua_pushuserdata( newState, &jmpStack );
    lua_setglobal( newState, "__JmpStack" );
	
    lua_pushuserdata( newState, &node );
	lua_setglobal( newState , "__Node");

    p_Ivr->Log(LOG_INFO, "Going to open lib's");



    lua_baselibopen( newState );                       // load the base lib
    lua_iolibopen( newState );                         // also load the i/o lib
    lua_strlibopen( newState );                        // and the string lib
	lua_dblibopen( newState );
	lua_mathlibopen( newState );
	lua_socketlibopen( newState );
    lua_sqlodbclibopen ( newState );


    sVoiceMailBox = "";

	// Inicia biblioteca CTI para Lua passando o ID da chamada corrente
    Lua_CtiLibOpen(newState);

    // Inicia biblioteca de funcoes para banco de dados
    Lua_DbLibOpen(newState);

    // Inicia biblioteca XML
    Lua_XmlLibOpen(newState);

    // Inicia biblioteca de Conferência
    Lua_ConferenceLibOpen(newState);

    // Inicia biblioteca AUDIO
    Lua_AudioLibOpen(newState);

    // Inicia biblioteca CDR
    Lua_CdrLibOpen(newState);

    // Inicia biblioteca Generica de funcoes e variaveis globais do LUA
    Lua_GenericLibOpen(newState, p_Ivr->GetCallId().c_str(), (char *)sVoiceMailBox.c_str());

    // Guarda o diretorio do script
    ScriptPath = p_Ivr->GetSwitch()->GetPathScripts();
    ScriptPath += "\\IVR";    

    // Seta para pegar todos os digitos
	p_Ivr->GetCTI()->SetDigMask("@");

	// Flag que indica se a ligacao ja foi desligada pelo script
    p_Ivr->SetScriptHangupFlag(false);	

    //p_Ivr->Log(LOG_INFO, "Lua stack size before loadfile(): %d", lua_gettop( newState ));
    p_Ivr->Log(LOG_INFO, "Going to call lua_dofile(%s)", Filename);
	
    
    // Load script
    if( setjmp( node.env_buffer ) == 0 )	
    {
		iRet = lua_dofile( newState, Filename );        // read and run the script
    }
    
    /*
    if( iRet == 0 )
	{
		p_Ivr->Log(LOG_INFO, "Lua stack size after loadfile(): %d", lua_gettop( newState ));
		// Executa apenas se nao houve erro no carregamento do script.
		int stackbase = lua_gettop( newState );  // function index
        lua_pushlstring(newState , "_TRACEBACK" , (sizeof("_TRACEBACK")/sizeof(char))-1);
		//lua_pushliteral( newState, "_TRACEBACK" );
		lua_rawget( newState, -10001 );  // get traceback function
		lua_insert( newState, stackbase );  // put it under chunk and args

		p_Ivr->Log(LOG_INFO, "Lua stack size before pcall(): %d", lua_gettop( newState ));

		// Execute script
		//iRet = lua_pcall(newState, 0, LUA_MULTRET, stackbase);

		p_Ivr->Log(LOG_INFO, "Lua stack size after pcall(): %d", lua_gettop( newState ));

		lua_remove( newState, stackbase );  // remove traceback function
	}
    */
     
    Lua_DbLibClose(newState);
    
    p_Ivr->Log(LOG_INFO," ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ");
    p_Ivr->Log(LOG_INFO," | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | ");
    p_Ivr->Log(LOG_INFO,"=================================================================");
    p_Ivr->StopLog();
    
    
    lua_close(newState);

    return iRet;
}
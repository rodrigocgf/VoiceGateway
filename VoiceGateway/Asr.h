#ifndef ASR_H
#define ASR_H				// Includes this file only once.

#include <stdio.h>
#include <string>

using namespace std;

// Parameter value type.
enum pvType
{
	pvString,
	pvInt,
	pvFloat
};
 
// Enum representing the possible returns for recognition status.
enum rsStatus
{
	rsIdle,
	rsStarted,
	rsRecognizing,
	rsCompleted,
	rsAborted,
	rsException
};

// Enum representing the answer types.
enum atType
{
	atRejected			= -1,
	atNoSpeech			= -2,
	atException			= 0xFFFF
};


// Struct used to pass all data needed for CTIBase::InitEngine.
typedef struct 
{
	string bsGrammarPackage;
	string bsDatabaseType;
	string bsDatabaseServer;
	string bsDatabaseName;
	string bsDatabaseUser;
	string bsDatabasePassword;
	string bsTelephonyDevice;
} NUANCE_ENGINE_PARAMETERS;


class ClassASR
{
public:

	ClassASR() {};
	virtual ~ClassASR() {};


//////////////////////////////////////////////////////////////////////////////////////////////////
//	ASR Interface Functions (implemented)
//////////////////////////////////////////////////////////////////////////////////////////////////
	virtual int StartEngine( void *Param ) = 0;
	virtual int StopEngine() = 0;
	virtual void ShutdownEngine() = 0;
	virtual int RestartEngine() = 0;
	virtual const char *GetLastError() = 0;
	virtual const char *GetParameter( const char* lpcszKey ) = 0;
	virtual int SetParameter( const char* lpcszKey, const char* lpcszValue, const int& iValueType = pvString ) = 0;
	virtual int StartRecognitionSession( const char* lpcszGrammarName = NULL  ) = 0;
	virtual int StopRecognitionSession() = 0;
	virtual int GetRecognitionStatus() = 0;
	virtual const char *GetAnswer( const char* lpcszSlotName, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;
	virtual int GetNumberOfAnswers() = 0;
	virtual int GetNumberOfInterpretations( const int& iAnswerIndex = 0 ) = 0;
	virtual int GetScore( const char* lpcszSlotName = NULL, const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;
	virtual const char *GetSlotName( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0, const int& iSlotIndex = 0 ) = 0;
	virtual int GetNumberOfFilledSlots( const int& iAnswerIndex = 0, const int& iInterpretationIndex = 0 ) = 0;
	virtual int Reset() = 0;
	virtual int AddVocabulary( const char* lpcszGrammarId, const char* lpcszGrammarLabel ) = 0;
	virtual int AddPhrase( const char* lpcszDbKey, const char* lpcszPhraseId, const char* lpcszPhraseText, const char* lpcszPhraseNl ) = 0;
	virtual int RemovePhrase( const char* lpcszDbKey, const char* lpcszPhraseId ) = 0;
	virtual int ModifyPhrase( const char* lpcszDbKey, const char* lpcszOldPhraseId, const char* lpcszNewPhraseTextAndId, const char* lpcszNewPhraseNl, bool bIsVoiceEnrolled = true ) = 0;
	virtual int StartEnrollmentSession( const char* lpcszGrammarId, const char* lpcszPhraseId, const char* lpcszPhraseNL, const char* lpcszGrammar ) = 0;
	virtual int StopEnrollmentSession( const bool& bAbort = false, const char * lpcszFileName = NULL, string& bsPhraseIdInClash = string("") ) = 0;	
	virtual int GetEnrollmentStatus() = 0;
	virtual int StartVerificationSession( const char *db_key ) = 0;
	virtual int StopVerificationSession() = 0;
	virtual int OpenSVDatabase( const char * lpcszServer, const char * lpcszDatabase, const char * lpcszAuth ) = 0;
	virtual int CloseSVDatabase() = 0;
	virtual int StartTrainingSession( const char *db_key ) = 0;
	virtual int StopTrainingSession() = 0;
	virtual int AbortTrainingSession() = 0;
	virtual int StartBufferingData() = 0;
	virtual int StopBufferingData() = 0;
	virtual int GetStartSafeIndex() = 0;
	virtual int GetStartActualIndex() = 0;
	virtual int GetEndSafeIndex() = 0;
	virtual int GetEndActualIndex() = 0;
	
//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
//	ASR Interface Functions (not implemented)
//////////////////////////////////////////////////////////////////////////////////////////////////
	// virtual int AddVocabulary() = 0;
	virtual int RemoveVocabulary() = 0;
	// virtual int AddPhrase() = 0;
	// virtual int RemovePhrase() = 0;
	// virtual int ModifyPhrase() = 0;
	// virtual int StartEnrollmentSession() = 0;
	// virtual int EnrollPhrase() = 0;
	// virtual int StopEnrollmentSession() = 0;
	// virtual int GetEnrollmentStatus() = 0;
	// virtual int StartTrainingSession() = 0;
	// virtual int StopTrainingSession() = 0;
	virtual int GetTrainingStatus() = 0;
	// virtual int StartVerificationSession() = 0;
	// virtual int StopVerificationSession() = 0;
	virtual int GetVerificationStatus() = 0;
//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
//	Telephony Functions/Variables (obsolete)
//////////////////////////////////////////////////////////////////////////////////////////////////
	virtual int WaitCall() = 0;
	virtual int AnswerCall() = 0;
	virtual int Hangup() = 0;
//////////////////////////////////////////////////////////////////////////////////////////////////

};

extern ClassASR *CreateASREngine();


#endif // ASR_H

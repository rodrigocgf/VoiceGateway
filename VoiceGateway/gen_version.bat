@echo off
setlocal enabledelayedexpansion
set filename=version.h
set _bBuildNumber=1
set _bMinorNumber=0
set _bMajorNumber=0
set _aux=0
set _aux1=0
set _aux2=0

for /f "tokens=1-3 delims= " %%A in (version.h) do (
    
    if "%%B"=="VERSION_BUILD_NUMBER" (
        echo READ:  %%A %%B            %%C
        set _aux=%%C
        
        if "!_aux!"=="10" (                        
            set /a _bMinorNumber=1            
            set _aux=0
            
            echo WRITE: %%A %%B            !_aux!
            echo.
            echo %%A %%B            !_aux!>>"%filename%.temp" 
        ) else (
			
            set /a _aux+=1
        
            echo WRITE: %%A %%B            !_aux!
            echo.
            echo %%A %%B            !_aux!>>"%filename%.temp" 
        )
    )    
    
    if "%%B"=="VERSION_MINOR_NUMBER" (
        echo READ:  %%A %%B            %%C
        set /a _aux1=%%C
        
        if "!_bMinorNumber!"=="1" (
            set /a _bMinorNumber=0
            
            set /a _aux1+=1            
		
			if "!_aux1!"=="10" (
				set _aux1=0
				set _bMajorNumber=1
				
				echo WRITE: %%A %%B            !_aux1!
				echo.
				echo %%A %%B            !_aux1!>>"%filename%.temp"
			) else (
				echo WRITE: %%A %%B            !_aux1!
				echo.
				echo %%A %%B            !_aux1!>>"%filename%.temp"             			
			)
            
        ) else (
            echo WRITE: %%A %%B            !_aux1!
            echo.
            echo %%A %%B            !_aux1!>>"%filename%.temp" 
        )
    ) 
    
    if "%%B"=="VERSION_MAJOR_NUMBER"    (
		set _aux2=%%C            
        if "!_bMajorNumber!"=="0" (
            
            echo READ:  %%A %%B            %%C
            echo WRITE: %%A %%B            !_aux2!
            echo.
            echo %%A %%B            !_aux2!>>"%filename%.temp" 
        ) else (
            set _bMajorNumber=0
            
            set /a _aux2+=1
            echo READ:  %%A %%B            %%C
            echo WRITE: %%A %%B            !_aux2!
            echo.
            echo %%A %%B            !_aux2!>>"%filename%.temp" 
        )
    )
     
               
)
del "%filename%"
ren "%filename%.temp" "%filename%"
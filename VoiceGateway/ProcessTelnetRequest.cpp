#include "ProcessTelnetRequest.h"
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>



ProcessTelnetRequest::ProcessTelnetRequest( IThrTelnetListen * pThrTelnetListen , boost::shared_ptr<Logger>  pLog4cpp, 
                                            SOCKET sock_ACCEPTED, const std::string & sTelnetPassword) :    
    m_pThrTelnetListen(pThrTelnetListen),     
    m_sock_ACCEPTED(sock_ACCEPTED) ,
    m_sTelnetPassword(sTelnetPassword),
    m_pLog4cpp(pLog4cpp) 
{   
    mp_TelnetSession = new TelnetSession( sock_ACCEPTED, pThrTelnetListen , m_pLog4cpp , m_sTelnetPassword );
    
    
 
    //timerId = GenerateTimerId();    
    //spTimer = boost::make_shared<Timer>(fTimerCallback , timerId );

    //spTimer->Create();
    //spTimer->SetInterval(3000); // milisseconds

    //fTimerCallback = std::bind(&ProcessTelnetRequest::OnTimerCallback , this , std::placeholders::_1  );
    //fTimerCallback = std::bind(&ProcessTelnetRequest::OnTimerCallback , this  );
    //fTimerCallback = std::bind(&IThrTelnetListen::TimerCallback , pThrTelnetListen);
    
    
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
}


ProcessTelnetRequest::~ProcessTelnetRequest(void)
{
    
}

std::string ProcessTelnetRequest::GenerateTimerId()
{
	std::stringstream ss;
	boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);
	std::string strTimeStamp;
	
	ss.str(std::string());
    ss <<   boost::format("%04i%02i%02i%02i%02i%02i%03i") % current_date_milliseconds.date().year() % 
			current_date_milliseconds.date().month().as_number() % current_date_milliseconds.date().day() %
			current_date_milliseconds.time_of_day().hours() % current_date_milliseconds.time_of_day().minutes() %
			current_date_milliseconds.time_of_day().seconds() %
			(current_date_milliseconds.time_of_day().fractional_seconds() / 1000);

	strTimeStamp = ss.str();
	
	return strTimeStamp;
}

void ProcessTelnetRequest::operator()()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

                              
                                
    mp_TelnetSession->initialise();
    
    //spTimer->Start();    
    //sessPtr = boost::make_shared<Sessions>(pTelnetSession , spTimer , timerId);

    //m_pThrTelnetListen->EmplaceSession(std::move(sessPtr));
    //std::function<void(TelnetSession * pTelnetSession )> fCallback = fTimerCallback(mp_TelnetSession);

    
    

}

void ProcessTelnetRequest::OnTimer()
{

    boost::this_thread::sleep_for (boost::chrono::milliseconds(3000) );			
    m_pLog4cpp->debug("TimerCallback");

    /*
    if ( pTelnetSession != nullptr )
    {
        pTelnetSession->Stop();
        delete pTelnetSession;
    }
    */

    
    /*
    auto ItrContainer = m_Container.get<ByTimerId>().find(sTimerId);
    if ( ItrContainer != m_Container.get<ByTimerId>().end() )
    {
        boost::shared_ptr<Sessions> sessPtr = (*ItrContainer);
        sessPtr->m_pTimer->Stop();
            
        sessPtr->m_pTelnetSession->Stop();

        if ( sessPtr->m_pTelnetSession != nullptr )
            delete sessPtr->m_pTelnetSession;

        sessPtr->m_pTimer->Destroy();
        if ( sessPtr->m_pTimer != nullptr )
            delete sessPtr->m_pTimer;

        m_pLog4cpp->debug("Delete from Sessions : TimerId %s\r\n", sTimerId.c_str());
        m_Container.get<ByTimerId>().erase(ItrContainer);
    }
    */
}


#include "Lua_XML.h"
#include <lua.hpp>
#include <iostream>
#include <time.h>
#include <direct.h>
#include <fstream>
#include "Switch.h"

#include "XmlParser.h"

#define	 XML_OK				0
#define	 XML_ERROR			1

int LuaXmlParser			(lua_State *L);
int LuaXmlAttributeValue	(lua_State *L);
int LuaXmlElementValue		(lua_State *L);
int LuaXmlAttributeCount	(lua_State *L);
int LuaXmlChildrenCount		(lua_State *L);


static const struct luaL_reg XmlLibFunctions[] = 
{
	{"XmlParser",			LuaXmlParser			},
	{"XmlAttributeValue",	LuaXmlAttributeValue	},
	{"XmlElementValue",		LuaXmlElementValue		},
	{"XmlAttributeCount",	LuaXmlAttributeCount	},
	{"XmlChildrenCount",	LuaXmlChildrenCount		}
};

Ivr * GetIvrXml( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );
    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return pIvr;
}

XmlParser * GetXMLParser( lua_State* L )
{
    XmlParser * myXmlParser;
    lua_getglobal( L, "__XmlParser" );
    myXmlParser = static_cast<XmlParser*>( lua_touserdata( L, -1 ) );
    lua_remove( L, -1 );
    return myXmlParser;
}


void Lua_XmlLibOpen(lua_State *L)
{
    luaL_openl( L, XmlLibFunctions );    
}

//------------------------------------------------------------------------------------------------------------------------

// XmlParser(xml)
// Executa o Parser do XML passado como argumento
// Retorna 0 se OK, 1 se Erro no primero argumento e a mensagem de erro no segundo

int LuaXmlParser(lua_State *L)
{
	int iNumArgs;
	const char *sXml;
	XmlParser *pXml;
    Ivr * pIvr = NULL;
    pIvr = GetIvrXml(L);    
	string s;


	try {        

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs <= 0){
			lua_pushnumber (L, XML_ERROR);
			lua_pushstring(L, "No arguments");
			return 2;
		}		

		sXml = luaL_check_string(L, 1);		
        pXml = GetXMLParser( L );

		// Executa o parser do XML
		if( pXml->Parser(sXml) )        
        {
			lua_pushnumber(L, XML_ERROR);            
			lua_pushstring(L, pXml->ReturnLastError());
			return 2;
		}

		lua_pushnumber(L, XML_OK);
		lua_pushstring(L, "Parser OK");
		return 2;
	}
	catch(...)
    {		
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_XML m�dule, function LuaXmlParser()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// AttributeValue(xPath, AttributeName)
// Retorna o valor de um atributo que esta no Xpath

int LuaXmlAttributeValue(lua_State *L)
{
	int iNumArgs, Index;
	const char *sXpath;
	const char *sAttributeName;
	XmlParser *pXml;    
	string sValue;
    Ivr * pIvr = NULL;
    pIvr = GetIvrXml(L);    

	try {
		//Lua_UserData->IVR->Log(LOG_SCRIPT, "XmlParser()");

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 2){
			lua_pushstring(L, "");
			return 1;
		}

		sXpath			= luaL_check_string(L, 1);
		sAttributeName	= luaL_check_string(L, 2);
		
        pXml = GetXMLParser( L );
			
        // Pega o valor do atributo do elemento corrente
		pXml->AttributeValue(sXpath, sAttributeName, sValue);
            
		// Retorna o valor para o script
		lua_pushstring(L, sValue.c_str());		

		return 1;
	}
	catch(...)
    {		
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_XML m�dule, function LuaXmlAttributeValue()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// ElementValue(xpath, Index)
// ElementValue(xpath)
// Retorna o valor de um elemento que esta no Xpath

int LuaXmlElementValue(lua_State *L)
{
	int iNumArgs, Index;
	const char *sXpath;
	XmlParser *pXml;    
	string sValue;
    Ivr * pIvr = NULL;
    pIvr = GetIvrXml(L);    

	try {

		//Lua_UserData->IVR->Log(LOG_SCRIPT, "XmlParser()");
        pXml = GetXMLParser( L );

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 1){
			lua_pushstring(L, "");
			return 1;
		}

		sXpath = luaL_check_string(L, 1);

		// Pega o valor do elemento corrrente			            
        pXml->ElementValue(sXpath, sValue);

		// Retorna o valor para o script
		lua_pushstring(L, sValue.c_str());
		

		return 1;
	}
	catch(...)
    {
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_XML m�dule, function LuaXmlElementValue()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// AttributeCount(xPath)
// Retorna o numero de atributos de xPath

int LuaXmlAttributeCount(lua_State *L)
{
	int iNumArgs;
	const char *sXpath;
	int iAttrinuteCount = 0;
	XmlParser *pXml;
    Ivr * pIvr = NULL;
    pIvr = GetIvrXml(L);  
    //XmlParser   xmlParser;

	try {
		//Lua_UserData->IVR->Log(LOG_SCRIPT, "XmlParser()");
        pXml = GetXMLParser( L );

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 1){
			lua_pushnumber (L, 0);
			return 1;
		}

		sXpath = luaL_check_string(L, 1);

		pXml->AttributeCount(sXpath, iAttrinuteCount);        

		lua_pushnumber (L, iAttrinuteCount);
		return 1;
	}
	catch(...)
    {		
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_XML m�dule, function LuaXmlAttributeCount()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int LuaXmlChildrenCount(lua_State *L)
{
	int iNumArgs;
	const char *sXpath;
	int iChildrenCount = 0;
	XmlParser *pXml;
    Ivr * pIvr = NULL;
    pIvr = GetIvrXml(L);  
    
	try {		
		//Lua_UserData->IVR->Log(LOG_SCRIPT, "XmlParser()");
        pXml = GetXMLParser( L );

		// Pega o numero de argumentos
		iNumArgs = lua_gettop(L);

		if(iNumArgs < 1){
			lua_pushnumber (L, 0);
			return 1;
		}

		sXpath = luaL_check_string(L, 1);
		
		pXml->ChildrenCount(sXpath, iChildrenCount);        

		// A biblioteca da IPWorks retorna esse valor quando nao existem filhos
		// para um determinado Xpath
		if( iChildrenCount >= 0xFFFF ){
			iChildrenCount = 0;
		}

		lua_pushnumber (L, iChildrenCount);
		return 1;
	}
	catch(...)
    {		
        pIvr->Log(LOG_ERROR, "EXCEPTION: Lua_XML m�dule, function LuaXmlChildrenCount()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

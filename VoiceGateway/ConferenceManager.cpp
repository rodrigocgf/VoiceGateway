/************************************************************************************************/
/* Support Comm Teleinformatica S/A																*/
/*                                                                                              */
/* Modulo: ConferenceManager.cpp																*/
/* Funcao: Trata requisi��es de controle da confer�ncia vindos do IVR							*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato - Criacao do Modulo                                               */
/* 30/OUT/2002 - Jairo Rosa - Integra��o com a Switch                                           */
/*                                                                                              */
/* 17/JUL/2019 - Rodrigo Fran�a - pequenas altera��es em interfaces                             */
/*                                                                                              */
/************************************************************************************************/
// Disables the STL warning message.
#ifdef _DEBUG
	#pragma warning(disable:4786) 
#endif

// Includes conference tecnologies.
#include "ConferenceManager.h"
#include "Class_TS_Manager.h"
#include "ISwitch.h"
#include "IConnection.h"

//HANDLE ConferenceManagerClass::Semaphore;	

//ConferenceInterfaceClass	*ConferenceManagerClass::pTech;
//ST_CONNECTION	ConferenceManagerClass::Connection;
//ST_CONNECTION	ConferenceManagerClass::Connection_remote;	
//ST_CONNECTION	ConferenceManagerClass::Connection_local;		

/************************************************************************************************/
/* M�todo: ConferenceManagerClass()																*/
/*                                                                                              */
/* Descr.:  Construtor da classe de gerenciamento da confer�ncia                                */
/* Param:   N/A       																			*/
/* Retorno: Nenhum																				*/
/************************************************************************************************/
ConferenceManagerClass::ConferenceManagerClass(_ThreadParam * p_ThreadParam, ISwitch * parent, boost::shared_ptr<Logger> pLog4cpp) : 
    m_pThreadParam(p_ThreadParam), m_pSwitch(parent), m_pLog4cpp(pLog4cpp), m_stop(false)
{
	Semaphore = NULL;
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    /*
	//Inicia objeto da tecnologia
	if(_strnicmp(BoardName.c_str(),"H100_PCI_MC3",12) == 0)
		pTech = new AmtelcoConferenceClass();
	else
	if((BoardName == "DMV600A") || (BoardName == "HMP"))
		pTech = new DMVConferenceClass();
	else
		pTech = new DCBClass();
    */
	pTech = new DCBClass(pLog4cpp);

	// Cria semaforo para controle da confer�ncia
	Semaphore = CreateSemaphore(NULL, 0, MAX_SEMAPHORE_COUNT, NULL);

    Start();
}

ConferenceManagerClass::~ConferenceManagerClass()
{
	if (pTech)
		delete pTech;
	
    pTech = NULL;

	if (Semaphore)
		CloseHandle (Semaphore);
	Semaphore = NULL;
}

void ConferenceManagerClass::Start()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    m_ThrPtr.reset ( new boost::thread ( boost::bind (&ConferenceManagerClass::Thr, this)));    
    m_ThrPtr->detach();
}

/***************************************************************************************************/
/* M�todo: Thread_Conference()																	   */
/*																								   */
/* Descr.:  Thread de inicializa��o da placa de confer�ncia. Criada em InitPortThreads (switch.cpp)*/
/* Param:   Par�metros para a thread (ThreadParam)       										   */
/* Retorno: Nenhum																				   */
/***************************************************************************************************/

void ConferenceManagerClass::Thr()
{
    m_pLog4cpp->debug("[%p][%s] Conference Thread %d", this, __FUNCTION__, GetCurrentThreadId() );

    ConferenceManagerClass *ConfMgmt = NULL;
	try 
    {   
		vector< _BoardInfo > Boards( m_pThreadParam->BoardInfo );
		vector< _BoardInfo >::iterator itBoards;
		_BoardInfo BoardInfo;

		//Abre todas as placas de confer�ncia
		for(itBoards = Boards.begin(); itBoards != Boards.end(); itBoards++)
		{
			BoardInfo = (*itBoards);

            m_pLog4cpp->debug("[%p][%s] Going to open BOARD %s", this, __FUNCTION__, BoardInfo.BoardName.c_str() );
			if(pTech->OpenBoard ( BoardInfo.Id, BoardInfo.BoardName.c_str() ))
			{
				// Erro na inicializacao do canal
			    m_pLog4cpp->error("[%p][%s] %s",this, __FUNCTION__,  pTech->GetLastError() );				
			}
		}

		// libera a criacao da proxima thread pelo processo principal
		SetEvent(m_pThreadParam->EventCreate);


        while(!m_stop)
        {
            boost::this_thread::sleep_for(boost::chrono::milliseconds(100));  

            // Verifica se a aplicacao terminou
			if(m_stop)
			{
				//Encerra a placa
				pTech->CloseBoard();				
			}

			// Pega eventos de requisicao da aplicacao			
			ProcessAppConfReq();
        }

        m_pLog4cpp->debug("[%p][%s] Conference Thread %d Terminated.", this, __FUNCTION__, GetCurrentThreadId() );
        
    }
    catch(...)
	{
        m_pLog4cpp->error("EXCEPTION: Ivr::Thr()");
		
	}   
}


void ConferenceManagerClass::Stop()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    m_stop = true;    
    m_ThrPtr.get()->join();
    
    m_pLog4cpp->debug("[%p][%s] ConferenceManagerClass THREAD STOPPED.", this, __FUNCTION__);
}


/************************************************************************************************/
/* M�todo: ProcessAppConfReq																																		*/
/*                                                                                              */
/* Descr.:  Trata as ACTIONS enviadas do IVR (IvrConference.cpp) para a thread de confer�ncia		*/
/* Param:   N/A																			       																			*/
/* Retorno: Nenhum																																							*/
/************************************************************************************************/
void ConferenceManagerClass::ProcessAppConfReq(void)
{
    char cValue[256];
    char pActionBuffer [MAX_ACTION_BUFFER];
    string sValue, sANI, sConferenceID, sConferenceIDSource, sConferenceIDDestination;
    int iValue = 0, iConferenceID = 0, iConferenceSize = 0, iNumberOfConferees = 0,
    iConfereeID = 0,  ret = 0, iConfereeType = 0, iChannel = 0, iMachine = 0;
    conference_conferee_type	eConfereeType;
    bool bNotifyTone = false;
    long lTimeSlot = -1, lMonitorTimeSlot = -1;
    long lTimeSlotRing = -1, lMonitorTimeSlotRing = -1;
    long lTimeSlotRingRX = -1;
    long lTimeSlotVox = -1;

	try 
	{
        //m_pLog4cpp->debug("[%p][%s] Waiting for Conference SEMAPHORE.", this, __FUNCTION__);


		// Verifica se foi enfilerado algum evento de confer�ncia para a aplicacao
		if(WaitForSingleObject(Semaphore, 0) != WAIT_OBJECT_0)
			return;
		
        memset(pActionBuffer , 0x00, sizeof(pActionBuffer));
		m_pLog4cpp->debug( LOG_LINE);
        m_pLog4cpp->debug("[%p][%s] Conference SEMAPHORE released.", this, __FUNCTION__);
		

		// Pega informacoes sobre a conexao remota (IVR)
		GetInfoConnection();

		// Recupera o Timeslot do canal
		lTimeSlot = Connection.Timeslot;
        		

		if(GetDataPacket(Connection.sData.c_str(), "ACTION", cValue, sizeof(cValue)-1))
		{
			m_pLog4cpp->debug( "There is no information in ACTION");
            SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
			return;
		}

		sValue = cValue;

/************************************************************************************
  CRIA A SALA DE CONFER�NCIA
  ACTION=CONFERENCE_CREATE&CONFERENCE_ID=xxxx&CONFEE_TYPE=yyyy&ANI=zzzz&SUPERVISOR=u
*************************************************************************************/

		if(sValue == "CONFERENCE_CREATE")
		{
            m_pLog4cpp->debug("ACTION=CONFERENCE_CREATE");

			if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
                SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

            if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID", sConferenceID))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_ID");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_SIZE", iConferenceSize))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_SIZE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "NOTIFYTONE", iValue))
			{
				m_pLog4cpp->error("There is no information in tag NOTIFYTONE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			bNotifyTone = (iValue == 1) ? true : false;

			eConfereeType = CP_GHOST;

			// Pega um timeslot da fila
            lTimeSlot = m_pSwitch->GetTS_Manager()->PopLocalTs();

			//Chama fun��o low level
			//No lTimeSlot vai o timeslot de transmiss�o e no retorno da fun��o,
		    //lTimeSlot ter� o timeslot de recep��o (vindo da confer�ncia)
            ret = pTech->Add (sConferenceID.c_str(), lTimeSlot, iConferenceSize, eConfereeType, bNotifyTone, lMonitorTimeSlot);

			
			// Estrutura com dados do destino.
			// Neste caso, a pr�pria m�quina
			// O canal n�o � alterado pois j� cont�m o canal destino			
			Connection.Timeslot	= lTimeSlot;
            Connection.Channel = iChannel;

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                    "&CHANNEL=%d"
                                    "&VALUE=%d",
                                    iChannel,
                                    ret);
		
			Connection.sData		= pActionBuffer;

			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			{
				m_pLog4cpp->debug("Add conference failed .");

				if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION))
			        m_pLog4cpp->error("%s", pTech->GetLastError() );
        
                Connection.sData += "&STATUS=ERROR";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Create result.");
					return;	
				}
			}
			else
			{
				Connection.sData += "&STATUS=OK";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Create result.");
					return;	
				}

				m_pLog4cpp->debug("Create success.");
			}

			return;
		}

/************************************************************************************
  DESTROY UMA CONFERENCIA
  ACTION=CONFERENCE_DESTROY&CONFERENCE_ID=
*************************************************************************************/

		if(sValue == "CONFERENCE_DESTROY")
		{

			m_pLog4cpp->debug("ACTION=CONFERENCE_DESTROY");

            if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID", sConferenceID))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_ID");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}
			
			eConfereeType = CP_GHOST;
			
			pTech->GetChairPersonTimeSlot( sConferenceID.c_str(), lMonitorTimeSlot, eConfereeType );

			ret = pTech->Remove( sConferenceID.c_str(), lTimeSlot, eConfereeType );

			Connection.Timeslot	= lTimeSlot;
			Connection.Channel = iChannel;

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                    "&CHANNEL=%d"
                                    "&VALUE=%d",
                                    iChannel,
                                    ret);

			Connection.sData		= pActionBuffer;
			
			// Devolve um timeslot para a fila
			m_pSwitch->GetTS_Manager()->PushLocalTs(lTimeSlot);

			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			{
				m_pLog4cpp->error("Remove conference failed: %s.", pTech->GetLastError());
        
                Connection.sData += "&STATUS=ERROR";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
					return;	
				}
			}
			else
			{
				Connection.sData += "&STATUS=OK";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
					return;	
				}

				m_pLog4cpp->debug("Remove conference success.");
				
			}

			return;
		}

/************************************************************************************
  ADICIONAR PARTICIPANTE NA CONFER�NCIA
  ACTION=CONFERENCE_ADD&CONFERENCE_ID=xxxx&CONFEE_TYPE=yyyy&ANI=zzzz&SUPERVISOR=u
*************************************************************************************/

		if(sValue == "CONFERENCE_ADD")
		{

			m_pLog4cpp->debug("ACTION=CONFERENCE_ADD");

            if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
                SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID", sConferenceID))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_ID");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_SIZE", iConferenceSize))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_SIZE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFEREE_TYPE", iConfereeType))
			{
				m_pLog4cpp->error("There is no information in tag CONFEREE_TYPE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			switch (iConfereeType)
			{
				case 0:
					eConfereeType = DUPLEX;
				break;

				case 1:
					eConfereeType = MONITOR;
				break;

				case 2:
					eConfereeType = COACH;
				break;

				case 3:
					eConfereeType = PUPIL;
				break;

				case 4:
					eConfereeType = PRIVATE;
				break;

                case 5:
					eConfereeType = CP_CONF;
				break;

                case 6:
					eConfereeType = CP_MONI;
				break;

                case 7:
                    eConfereeType = CP_VOX;
                break;

                case 8:
                    eConfereeType = CP_PERS;
				break;

				case 9:
                    eConfereeType = CP_GHOST;
				break;

				default:
					eConfereeType = DUPLEX;
				break;
			}

			if(GetDataPacket(Connection.sData.c_str(), "NOTIFYTONE", iValue))
			{
				m_pLog4cpp->error("There is no information in tag NOTIFYTONE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			bNotifyTone = (iValue == 1) ? true : false;

			// Teste para colocar o timeslot de voz na conferencia
            if( iConfereeType == CP_VOX )
            {
                // Pega o timeslot de voz
                if(GetDataPacket(Connection.sData.c_str(), "TIMESLOTVOX", iValue))
			    {
				    m_pLog4cpp->error("There is no information in tag TIMESLOTVOX");
				    SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				    return;
			    }

                lTimeSlot = iValue;
            }

			//Chama fun��o low level
			//No lTimeSlot vai o timeslot de transmiss�o e no retorno da fun��o,
		    //lTimeSlot ter� o timeslot de recep��o (vindo da confer�ncia)
            ret = pTech->Add (sConferenceID.c_str(), lTimeSlot, iConferenceSize, eConfereeType, bNotifyTone, lMonitorTimeSlot);		


            // M�quina Remota
			if( m_pSwitch->GetMachine() != Connection.Machine )
			{
				if(ret == CONFERENCE_OK)
				{	
						
					// Estrutura com dados do destino.
					// Neste caso, a pr�pria m�quina
					// O canal n�o � alterado pois j� cont�m o canal destino			
					Connection.Timeslot	= lTimeSlotRing;
					Connection.Channel = iChannel;

					sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                            "&CHANNEL=%d"
                                            "&TIMESLOT=%d"
                                            "&VALUE=%d"
                                            "&MONITOR_TIMESLOT=%ld",
                                            iChannel,
                                            lTimeSlotRing,
                                            ret,
                                            lMonitorTimeSlotRing);
                }
			}
			else
            {
                // Estrutura com dados do destino.
			    // Neste caso, a pr�pria m�quina
			    // O canal n�o � alterado pois j� cont�m o canal destino			
			    Connection.Timeslot	= lTimeSlot;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%ld"
                                        "&VALUE=%d"
                                        "&MONITOR_TIMESLOT=%ld",
                                        iChannel,
                                        lTimeSlot,
                                        ret,
                                        lMonitorTimeSlot);
            }
	
			//Connection.sData		= pActionBuffer;
            Connection.sData.assign(pActionBuffer);

			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			{
				m_pLog4cpp->debug("Add conference failed .");
				
				if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION))
			        m_pLog4cpp->error("%s", pTech->GetLastError() );
        
                Connection.sData += "&STATUS=ERROR";
                m_pLog4cpp->debug("SendConnection T_SW_CONF_ERROR : %s", Connection.sData.c_str() );
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Add result.");
					return;	
				}
			}
			else
			{
				Connection.sData += "&STATUS=OK";

                m_pLog4cpp->debug("SendConnection T_SW_CONF_OK : %s", Connection.sData.c_str() );
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Add result.");
					return;	
				}

                m_pLog4cpp->debug("[%p][%s] Add success.", this, __FUNCTION__);				
			}

			return;
		}


/************************************************************************************
  REMOVER PARTICIPANTE DA CONFER�NCIA	(cada canal remove o respectivo participante
  ACTION=CONFERENCE_REMOVE&CONFERENCE_ID=
*************************************************************************************/

		if((sValue == "CONFERENCE_REMOVE") || (sValue == "REMOVE_CONFEREE"))
		{
			m_pLog4cpp->debug("ACTION=CONFERENCE_REMOVE");

            if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID", sConferenceID))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_ID");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

            if(GetDataPacket(Connection.sData.c_str(), "CONFEREE_TYPE", iConfereeType))
			{
				m_pLog4cpp->error("There is no information in tag CONFEREE_TYPE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			switch (iConfereeType)
			{
				case 0:
					eConfereeType = DUPLEX;
				break;

				case 1:
					eConfereeType = MONITOR;
				break;

				case 2:
					eConfereeType = COACH;
				break;

				case 3:
					eConfereeType = PUPIL;
				break;

				case 4:
					eConfereeType = PRIVATE;
				break;

                case 5:
					eConfereeType = CP_CONF;
				break;

                case 6:
					eConfereeType = CP_MONI;
				break;

                case 7:
                    eConfereeType = CP_VOX;
                break;

                case 8:
                    eConfereeType = CP_PERS;
				break;

				case 9:
                    eConfereeType = CP_GHOST;
				break;

				default:
					eConfereeType = DUPLEX;
				break;
			}

			if(sValue == "REMOVE_CONFEREE")
			{
				if(GetDataPacket(Connection.sData.c_str(), "MACHINE", iMachine))
				{
					m_pLog4cpp->error("There is no information in tag MACHINE");
					SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
					return;
				}
			}
			else
				iMachine = Connection.Machine;

            // >>> Ricardo Hayama 11/08/2003
            // Teste para colocar o timeslot de voz no conferencia
            if( iConfereeType == CP_VOX )
            {
                // Pega o timeslot de voz
                if(GetDataPacket(Connection.sData.c_str(), "TIMESLOTVOX", iValue))
			    {
				    m_pLog4cpp->error("There is no information in tag TIMESLOTVOX");
				    SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				    return;
			    }
        
                lTimeSlot = iValue;
            }

            if( iConfereeType == CP_CONF )
            {
				ret = pTech->GetChairPersonTimeSlot( sConferenceID.c_str(), lMonitorTimeSlot, eConfereeType );
				if( lMonitorTimeSlot != -1 )			//DCB ou AMTELCO
				{
					ret = pTech->Remove( sConferenceID.c_str(), lTimeSlot, eConfereeType );
					// M�quina Remota
					if( m_pSwitch->GetMachine() != iMachine )
					{
						Connection.Timeslot	= lMonitorTimeSlotRing;
						Connection.Channel = iChannel;
						sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                                "&CHANNEL=%d"
                                                "&TIMESLOT=%d"
                                                "&VALUE=%d",
                                                iChannel,
                                                lMonitorTimeSlotRing,
                                                ret);
					}
					else
					{
						Connection.Timeslot	= lMonitorTimeSlot;
						Connection.Channel = iChannel;
						sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                                "&CHANNEL=%d"
                                                "&TIMESLOT=%d"
                                                "&VALUE=%d",                              
                                                iChannel,
                                                lMonitorTimeSlot,
                                                ret);
					}

					if( ret == CONFERENCE_OK )
					{
						Connection.sData = pActionBuffer;
						Connection.sData += "&STATUS=OK";
						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
						{
							m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
							return;	
						}
						m_pLog4cpp->debug("Remove conference success.");
					}
					else
					{
						Connection.sData = pActionBuffer;
						Connection.sData += "&STATUS=ERROR";
						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
						{
							m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
							return;	
						}
						m_pLog4cpp->error("Remove conference failed: %s.", pTech->GetLastError());
					}

					return;
				}
            }

			// M�quina Remota
            if( m_pSwitch->GetMachine() != iMachine )
            {
				long lTxCTbusTimeslot = 0;				
				ret = pTech->GetConferenceTimeSlot(sConferenceID.c_str(), lTxCTbusTimeslot);
        
				if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			    {
				    m_pLog4cpp->error("GetConferenceTimeSlot failed: %s.", pTech->GetLastError());
                    SendErrorGetDataPacket(lTimeSlotRingRX, iChannel, ret);
				    return;
			    }

                m_pLog4cpp->debug("GetConferenceTimeSlot ConferenceID: %s - IN: %d - OUT: %d", sConferenceID.c_str(), lTimeSlot, lTxCTbusTimeslot);
            }

			if(	iConfereeType == CP_PERS || iConfereeType == CP_MONI )
			{
				pTech->GetChairPersonTimeSlot( sConferenceID.c_str(), lMonitorTimeSlot, eConfereeType );				
			}

			ret = pTech->Remove( sConferenceID.c_str(), lTimeSlot, eConfereeType );

			// Verifica se � local ou remoto
            if( m_pSwitch->GetMachine() != iMachine )
			{
				if(ret == CONFERENCE_OK)
				{
					Connection.Timeslot	= lTimeSlotRing;          
					Connection.Channel = iChannel;

					sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                            "&CHANNEL=%d"
                                            "&TIMESLOT=%d"
                                            "&MONITOR_TIMESLOT=%ld"
                                            "&VALUE=%d",                              
                                            iChannel,
                                            lTimeSlotRing,
                                            lMonitorTimeSlotRing,
                                            ret);
				}
				else
				{
					sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                            "&CHANNEL=%d"
                                            "&TIMESLOT=%d"
                                            "&MONITOR_TIMESLOT=%ld"
                                            "&VALUE=%d",                              
                                            iChannel,
                                            lTimeSlot,
                                            lMonitorTimeSlot,
                                            ret);
				}

			}
			else
            {
                Connection.Timeslot	= lTimeSlot;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%d"
                                        "&VALUE=%d",                              
                                        iChannel,
                                        lTimeSlot,
                                        ret);

            }

			Connection.sData		= pActionBuffer;

			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			{
				m_pLog4cpp->error("Remove conference failed: %s.", pTech->GetLastError());
        
                Connection.sData += "&STATUS=ERROR";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
					return;	
				}
			}
			else
			{
				Connection.sData += "&STATUS=OK";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending CONFERENCE_REMOVE result.");
					return;	
				}

                m_pLog4cpp->debug("[%p][%s] Remove conference success.", this, __FUNCTION__);				
				
			}

			return;
		}

/************************************************************************************
  ADICIONAR PARTICIPANTE NA CONFER�NCIA
  ACTION=CONFERENCE_CHANGE&CONFERENCE_ID_SOURCE=xxxx&CONFERENCE_ID_DESTINATION=yyyy&CONFEREE_TYPE=zzzz&ANI=aaaa&SUPERVISOR=b
*************************************************************************************/

		if(sValue == "CONFERENCE_CHANGE")
		{

			m_pLog4cpp->debug("ACTION=CONFERENCE_CHANGE");

            if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
                SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID_SOURCE", sConferenceIDSource))
			{
				m_pLog4cpp->error( "There is no information in tag CONFERENCE_ID_SOURCE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID_DESTINATION", sConferenceIDDestination))
			{
				m_pLog4cpp->error( "There is no information in tag CONFERENCE_ID_DESTINATION");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_SIZE", iConferenceSize))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_SIZE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFEREE_TYPE", iConfereeType))
			{
				m_pLog4cpp->error( "There is no information in tag CONFEREE_TYPE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			switch (iConfereeType)
			{
				case 0:
					eConfereeType = DUPLEX;
				break;

				case 1:
					eConfereeType = MONITOR;
				break;

				case 2:
					eConfereeType = COACH;
				break;

				case 3:
					eConfereeType = PUPIL;
				break;

				case 4:
					eConfereeType = PRIVATE;
				break;

                case 5:
					eConfereeType = CP_CONF;
				break;

                case 6:
					eConfereeType = CP_MONI;
				break;

                case 7:
                    eConfereeType = CP_VOX;
                break;

                case 8:
                    eConfereeType = CP_PERS;
				break;

				default:
					eConfereeType = DUPLEX;
				break;
			}

			if(GetDataPacket(Connection.sData.c_str(), "NOTIFYTONE", iValue))
			{
				m_pLog4cpp->error("There is no information in tag NOTIFYTONE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			bNotifyTone = (iValue == 1) ? true : false;

			if(iConfereeType == CP_CONF)
			{
			    Connection.Timeslot	= lTimeSlot;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%d"
                                        "&VALUE=%d"
                                        "&MONITOR_TIMESLOT=%ld",
                                        iChannel,
                                        lTimeSlot,
                                        ret,
                                        lMonitorTimeSlot);

		        m_pLog4cpp->error( "Change conference failed." );
			    m_pLog4cpp->error( "Cannot execute a 'CONFERENCE_CHANGE' action on a CP_CONF conferee." );
        
                Connection.sData += "&STATUS=ERROR";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Change result.");
					return;	
				}
			}

            // >>> Ricardo Hayama 08/08/2003
            // Teste para colocar o timeslot de voz na conferencia
            if( iConfereeType == CP_VOX )
            {
                // Pega o timeslot de voz
                if(GetDataPacket(Connection.sData.c_str(), "TIMESLOTVOX", iValue))
			    {
				    m_pLog4cpp->error("There is no information in tag TIMESLOTVOX");
				    SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				    return;
			    }
        
                lTimeSlot = iValue;
            }

			// M�quina Remota
            if( m_pSwitch->GetMachine() != Connection.Machine )
            {
    		    long lTxCTbusTimeslot = 0;
				ret = pTech->GetConferenceTimeSlot(sConferenceIDSource.c_str(), lTxCTbusTimeslot);

                if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			    {
				    m_pLog4cpp->error("GetConferenceTimeSlot failed: %s.", pTech->GetLastError());
                    SendErrorGetDataPacket(lTimeSlotRingRX, iChannel, ret);
				    return;
			    }

                m_pLog4cpp->debug("GetConferenceTimeSlot ConferenceID: %s - IN: %d - OUT: %d", sConferenceID.c_str(), lTimeSlot, lTxCTbusTimeslot);
				lTimeSlotRingRX = lTimeSlotRing;
            }

			ret = pTech->Change( sConferenceIDSource.c_str(), sConferenceIDDestination.c_str(), lTimeSlot, iConferenceSize, eConfereeType, bNotifyTone, lMonitorTimeSlot);
			
			// M�quina Remota
            if( m_pSwitch->GetMachine() != Connection.Machine )
			{
				if(ret == CONFERENCE_OK)
				{
					lMonitorTimeSlotRing = lMonitorTimeSlot;

					// Estrutura com dados do destino.
					// Neste caso, a pr�pria m�quina
					// O canal n�o � alterado pois j� cont�m o canal destino			
					Connection.Timeslot	= lTimeSlotRing;
					Connection.Channel = iChannel;

					sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
											"&CHANNEL=%d"
											"&TIMESLOT=%d"
											"&VALUE=%d"
											"&SOURCE_TIMESLOT=%ld"
											"&MONITOR_TIMESLOT=%ld",
											iChannel,
											lTimeSlotRing,
											ret,
											lTimeSlotRingRX,
 											lMonitorTimeSlotRing);
				}
				else
				{
					sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
											"&CHANNEL=%d"
											"&TIMESLOT=%d"
											"&VALUE=%d"
											"&MONITOR_TIMESLOT=%ld",
											iChannel,
											lTimeSlot,
											ret,
											lMonitorTimeSlot);
				}
			}
			else
            {
                // Estrutura com dados do destino.
			    // Neste caso, a pr�pria m�quina
			    // O canal n�o � alterado pois j� cont�m o canal destino			
			    Connection.Timeslot	= lTimeSlot;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%d"
                                        "&VALUE=%d"
                                        "&MONITOR_TIMESLOT=%ld",
                                        iChannel,
                                        lTimeSlot,
                                        ret,
                                        lMonitorTimeSlot);
            }
		
			Connection.sData		= pActionBuffer;

			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION) || (ret == CONFERENCE_WARNING))
			{
                m_pLog4cpp->debug("Change conference failed .");
			    if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION))
			        m_pLog4cpp->error( "%s", pTech->GetLastError() );
        
                Connection.sData += "&STATUS=ERROR";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_ERROR, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Change result.");
					return;	
				}
			}
			else
			{
				Connection.sData += "&STATUS=OK";
                // Envia para a conexao Destino o Evento X com informacao da conexao de Origem
				if( !SendConnection(T_SW_CONF_OK, Connection.sData) )
				{
					m_pLog4cpp->error("Error sending Change result.");
					return;	
				}

                m_pLog4cpp->debug("[%p][%s] Change success.", this, __FUNCTION__);				
			}

			return;
		}

/************************************************************************************
  PESQUISAR O ID DE UM USUARIO DADO O SUFIXO DO ANI
  ACTION=CONFERENCE_SETCONFEREEATTRIBUTE
*************************************************************************************/

		if(sValue == "CONFERENCE_SETCONFEREEATTRIBUTE")
		{
 
			m_pLog4cpp->debug("ACTION=CONFERENCE_SETCONFEREEATTRIBUTE" );

            if(GetDataPacket(Connection.sData.c_str(), "CHANNEL", iChannel))
			{
				m_pLog4cpp->error("There is no information in tag CHANNEL");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFERENCE_ID", sConferenceID))
			{
				m_pLog4cpp->error("There is no information in tag CONFERENCE_ID");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			if(GetDataPacket(Connection.sData.c_str(), "CONFEREE_TYPE", iValue))
			{
				m_pLog4cpp->error("There is no information in tag CONFEREE_TYPE");
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
				return;
			}

			switch (iValue)
			{
				case 0:
					eConfereeType = DUPLEX;
				break;

				case 1:
					eConfereeType = MONITOR;
				break;

				case 2:
					eConfereeType = COACH;
				break;

				case 3:
					eConfereeType = PUPIL;
				break;

				case 4:
					eConfereeType = PRIVATE;
				break;

                case 5:
					eConfereeType = CP_CONF;
				break;

                case 6:
					eConfereeType = CP_MONI;
				break;

				case 7:
                    eConfereeType = CP_VOX;
                break;

                case 8:
					eConfereeType = CP_PERS;
				break;

				default:
					eConfereeType = DUPLEX;
				break;
			}


			ret = pTech->SetConfereeAttribute( sConferenceID.c_str(), lTimeSlot, eConfereeType );

			// M�quina Remota
            if( m_pSwitch->GetMachine() != Connection.Machine )
			{
			    // Estrutura com dados do destino.
			    Connection.Timeslot	= lTimeSlotRing;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%d"
                                        "&VALUE=%d",
                                        iChannel,
                                        lTimeSlotRing,
                                        ret);

			}
			else
            {
                Connection.Timeslot	= lTimeSlot;
                Connection.Channel = iChannel;

			    sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                                        "&CHANNEL=%d"
                                        "&TIMESLOT=%d"
                                        "&VALUE=%d",
                                        iChannel,
                                        lTimeSlot,
                                        ret);
            }
		
			Connection.sData		= pActionBuffer;

			if( ret == CONFERENCE_WARNING )
			{				
                m_pLog4cpp->debug(  "Cannot set the conferee attributes." );
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
			}
			else
			if ((ret == CONFERENCE_ERROR)	|| (ret == CONFERENCE_EXCEPTION))
			{
                m_pLog4cpp->debug("%s", pTech->GetLastError());
				SendErrorGetDataPacket(lTimeSlot, iChannel, ret);
			}
			else
			{
				Connection.sData += "&STATUS=OK";
                m_pLog4cpp->debug( "A conference id was choosen success." );
				SendConnection(T_SW_CONF_OK, Connection.sData);
			}

			return;
		}
	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: ConferenceManagerClass::ProcessAppConfReq()");
	}
}


void ConferenceManagerClass::SendConferenceEvent()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try 
	{
        m_pLog4cpp->debug("[%p][%s] Release Conference SEMAPHORE.", this, __FUNCTION__);
		ReleaseSemaphore(Semaphore, 1, NULL);
	}

	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: ConferenceManagerClass::SendConferenceEvent()");
	}
}



int ConferenceManagerClass::GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize)
{
    string data, valor;
    int pos_ini, pos_fim;

    try 
    {
		data = datapacket;

		pos_ini = data.find(cCommand, 0);

		if( pos_ini == -1 )
        {
			strcpy(cValue, "");
			return -1;
		}

		pos_ini += (strlen(cCommand) + 1);
		pos_fim = data.find("&", pos_ini);

		strncpy(cValue, data.substr(pos_ini, pos_fim-pos_ini).c_str(), valuesize);

		return 0;
	}
	catch(...)
    {
		m_pLog4cpp->error("EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize)");
		strcpy(cValue, "");
		return -1;
	}
}

int ConferenceManagerClass::GetDataPacket(const char *datapacket, char *cCommand, int &iValue)
{
    string data, valor, command;
    int pos_ini, pos_fim;

    try 
    {

		data = datapacket;
        command = cCommand;
        command += "=";

		pos_ini = data.find(command.c_str(), 0);

		if( pos_ini == -1 )
        {
			iValue = 0;
			return -1;
		}

		pos_ini += command.size();
		pos_fim = data.find("&", pos_ini);

        if( pos_fim == -1 )
        {
			pos_fim = data.size();
		}

		iValue = atoi( data.substr(pos_ini, pos_fim-pos_ini).c_str() );

		return 0;
	}
	catch(...)
    {
		iValue = 0;
		m_pLog4cpp->error( "EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, int &iValue)");
		return -1;
	}
}

int ConferenceManagerClass::GetDataPacket(const char *datapacket, char *cCommand, string &sValue)
{
    string data, valor;
    int pos_ini, pos_fim;

    try 
    {
		data = datapacket;
		pos_ini = data.find(cCommand, 0);

		if( pos_ini == -1 )
        {
			sValue= "";
			return -1;
		}

		pos_ini += (strlen(cCommand) + 1);
		pos_fim = data.find("&", pos_ini);
		sValue = data.substr(pos_ini, pos_fim-pos_ini).c_str();

		return 0;
	}
	catch(...)
    {
		sValue= "";
		m_pLog4cpp->error( "EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, string &sValue)");
		return -1;
	}
}

void ConferenceManagerClass::SendErrorGetDataPacket(long lTimeSlot, int iChannel, int iRet)
{
    char pActionBuffer [MAX_ACTION_BUFFER];

	Connection.Timeslot	= lTimeSlot;          
    Connection.Channel = iChannel;

	sprintf (pActionBuffer, "?ACTION=CONFERENCE_RESULT"
                            "&CHANNEL=%d"
                            "&TIMESLOT=%d"
                            "&VALUE=%d",                          
                            iChannel,
                            lTimeSlot,
                            iRet);

	Connection.sData		= pActionBuffer;

    Connection.sData += "&STATUS=ERROR";
  
    SendConnection(T_SW_CONF_ERROR, Connection.sData);
}


void ConferenceManagerClass::GetInfoConnection()
{
	try 
	{
        
		m_pLog4cpp->debug( "ConferenceManagerClass::GetInfoConnection()");

		ClearConn(Connection);

		// Pega informacoes sobre a conexao destino
        if ( m_pSwitch->GetConnection()->Connection_Get(Connection) == false )
		//if(Connection_Get(Connection) == false)
			m_pLog4cpp->debug("Event: There is no information for Connection");

		m_pLog4cpp->debug("                        ");

//		m_pLog4cpp->debug("Connection.Switch      :%d",	Connection.Switch);
//		m_pLog4cpp->debug("Connection.Machine     :%d",	Connection.Machine);
		m_pLog4cpp->debug("Connection.Channel     :%d",	Connection.Channel);
		m_pLog4cpp->debug("Connection.Timeslot    :%d",	Connection.Timeslot);
		m_pLog4cpp->debug("Connection.sData       :%s",	Connection.sData.c_str());

		m_pLog4cpp->debug("                          ");
	}

	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: ConferenceManagerClass::GetInfoConnection()");
	}
}


void ConferenceManagerClass::ClearConn(ST_CONNECTION &st_connection)
{
	try {

		st_connection.Machine		=	0;
		st_connection.Switch		=	0;
		st_connection.Channel		=	0;
		st_connection.Event			=	0;
		st_connection.Timeslot	=	0;
		st_connection.sData			= "";
	}
	catch(...)
    {
		m_pLog4cpp->error("EXCEPTION: Ivr::ClearConn()");
	}
}


bool ConferenceManagerClass::SendConnection(int Event, string sData)
{
    EventName<std::stringstream> EvtName;

	try 
	{
        m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, EvtName.GetEventName(Event).str().c_str());

		// Informacoes da conexao origem

        Connection_local.Switch			= m_pSwitch->GetSwitch();
		Connection_local.Machine		= m_pSwitch->GetMachine();
		Connection_local.Channel		= 0;
		Connection_local.Timeslot		= Connection.Timeslot;
		Connection_local.Event			= Event;
		Connection_local.sData			= sData;

		// Informacoes da conexao destino (canal do usuario)
		Connection_remote.Switch		= Connection.Switch;
		Connection_remote.Machine		= Connection.Machine;
		Connection_remote.Channel		= Connection.Channel;
		Connection_remote.Timeslot	    = 0;
		Connection_remote.Event			= Event;
		Connection_remote.sData			= sData;

//		Log(LOG_INFO, " ");

//		Log(LOG_INFO,"Connection_local.Switch        :%d",	Connection_local.Switch);
//		Log(LOG_INFO,"Connection_local.Machine       :%d",	Connection_local.Machine);
//		Log(LOG_INFO,"Connection_local.Channel       :%d",	Connection_local.Channel);
//		Log(LOG_INFO,"Connection_local.TimeSlot      :%d",	Connection_local.Timeslot);
//		Log(LOG_INFO,"Connection_local.sData         :%s",	Connection_local.sData.c_str());

		m_pLog4cpp->debug(" ");

//		Log(LOG_INFO,"Connection_remote.Switch       :%d",	Connection_remote.Switch);
//		Log(LOG_INFO,"Connection_remote.Machine      :%d",	Connection_remote.Machine);
		m_pLog4cpp->debug("Connection_remote.Channel      :%d",	Connection_remote.Channel);
		m_pLog4cpp->debug("Connection_remote.TimeSlot     :%d",	Connection_remote.Timeslot);
		m_pLog4cpp->debug("Connection_remote.sData        :%s",	Connection_remote.sData.c_str());

		m_pLog4cpp->debug(" ");

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
        return m_pSwitch->GetConnection()->Connection_Send(Connection_remote, Event, Connection_local);
		//return Connection_Send(Connection_remote, Event, Connection_local);
	}

	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: ConferenceManagerClass::SendConnection()");
		return false;
	}
}
#ifndef _CHANNELSTAT_H
#define _CHANNELSTAT_H

#define MAX_CHANNEL							512
#define CHECK_CHANNEL_LIMIT(x) ( ( ( x < 0 ) && ( x > MAX_CHANNEL ) ) ? 0 : x )

#include <time.h>
#include <windows.h>
#include <memory>

class ChannelStat
{
private:
	LONG ScriptErrorCount[MAX_CHANNEL];			// Guarda o numero de erros ocorridos na execucao dos scripts.
	LONG CallsAnsweredCount[MAX_CHANNEL];		// Guarda o numero de chamadas atendidas (in).
	LONG CallsConnectedCount[MAX_CHANNEL];		// Guarda o numero de chamadas atendidas (out).
	LONG LastActivityTime[MAX_CHANNEL];			// Valor da ultima atividade no canal.
	LONG BusyChannelCount;										// Guarda o numero de canais ocupados (in e out).
	LONG MaxBusyChannelCount;								// Guarda o numero de canais ocupados (in e out).


    ChannelStat();

public:
    static ChannelStat &Instance()
    {
        static std::unique_ptr<ChannelStat> instance( new ChannelStat() );
        return *instance;
    }

	
	~ChannelStat();

	void ResetScriptErrorCount( const int& channel );
	void IncrementScriptErrorCount( const int& channel );
	void DecrementScriptErrorCount( const int& channel );
	ULONG GetScriptErrorCount( const int& channel );

	void ResetCallsAnsweredCount( const int& channel );
	void IncrementCallsAnsweredCount( const int& channel );
	void DecrementCallsAnsweredCount( const int& channel );
	ULONG GetCallsAnsweredCount( const int& channel );

	void ResetCallsConnectedCount( const int& channel );
	void IncrementCallsConnectedCount( const int& channel );
	void DecrementCallsConnectedCount( const int& channel );
	ULONG GetCallsConnectedCount( const int& channel );

	void SetLastActivityTime( const int& channel, const time_t& lastactivitytime );
	const time_t& GetLastActivityTime( const int& channel );

	void ResetBusyChannelCount();
	void IncrementBusyChannelCount();
	void DecrementBusyChannelCount();
	ULONG GetBusyChannelCount();

	void ResetMaxBusyChannelCount();
	ULONG GetMaxBusyChannelCount();
};

#endif // _CHANNELSTAT_H

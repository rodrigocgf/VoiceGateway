// VoiceGateway.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <tchar.h>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "Controller.h"
#include "ServiceInstaller.h"
#include "ServiceBase.h"
#include "version.h"



boost::asio::io_service					ioService;	
log4cpp::Category * 					m_pLog4cpp;	

namespace fs = boost::filesystem;


void PrintInfo();
fs::path GetCurrentPath(char ** argv);
void WaitExit();

static boost::shared_ptr<Controller> 	controllerPtr;

int main(int argc , char ** argv)
{   
    fs::path fsCurrentPath;
    m_pLog4cpp = &log4cpp::Category::getInstance( std::string("VoiceGateway") ); 

    if ((argc > 1) && ((*argv[1] == L'-' || (*argv[1] == L'/'))))
    {
        //if (_wcsicmp(L"install", argv[1] + 1) == 0)
        if (stricmp("install", argv[1] + 1) == 0)
        {
            // Install the service when the command is 
            // "-install" or "/install".
            InstallService(
                SERVICE_NAME,               // Name of service
                SERVICE_DISPLAY_NAME,       // Name to display
                SERVICE_START_TYPE,         // Service start type
                SERVICE_DEPENDENCIES,       // Dependencies
                SERVICE_ACCOUNT,            // Service running account
                SERVICE_PASSWORD            // Password of the account
                );
        }
        //else if (_wcsicmp(L"remove", argv[1] + 1) == 0)
        else if (stricmp("remove", argv[1] + 1) == 0)
        {
            // Uninstall the service when the command is 
            // "-remove" or "/remove".
            UninstallService(SERVICE_NAME);
        }
        else if (stricmp("version", argv[1] + 1) == 0)
        {
            PrintInfo();
        }
        else if (stricmp("run", argv[1] + 1) == 0)
        {
            fsCurrentPath = GetCurrentPath(argv);
            PrintInfo();    
            
            controllerPtr.reset(new Controller(ioService , fsCurrentPath));
            controllerPtr->StartByPrompt();
    
            WaitExit();

            controllerPtr->StopByPrompt();
        }
    }
    else
    {
        wprintf(L"Parameters:\n");
        wprintf(L" -install  to install the service.\n");
        wprintf(L" -remove   to remove the service.\n");

        fsCurrentPath = GetCurrentPath(argv);
        
        Controller controller(ioService , fsCurrentPath);
        if (!CServiceBase::Run(controller))
        {
            wprintf(L"Service failed to run w/err 0x%08lx\n", GetLastError());
        }

        /*
        fsCurrentPath = GetCurrentPath(argv);
        PrintInfo();
    
        controllerPtr.reset(new Controller(ioService , fsCurrentPath));
        controllerPtr->StartByPrompt();
    
        WaitExit();

        controllerPtr->StopByPrompt();
        */
    }
	return 0;
}


fs::path GetCurrentPath(char ** argv)
{
    fs::path full_path( fs::initial_path<fs::path>() );

    full_path = fs::system_complete( fs::path( argv[0] ) );

    std::cout << "EXECUTABLE PATH : " << full_path.remove_filename() << std::endl;
    
    return full_path;
}

void PrintInfo()
{
    std::string sInfo;
    std::stringstream ss;

    ss << " \r\n\r\n";
    ss << "                                                                            \r\n";
    ss << "    __     ______           ____  _   _  ___  _____ _   _ _____  __         \r\n";
    ss << "    \\ \\   / / ___|         |  _ \\| | | |/ _ \\| ____| \\ | |_ _\\ \\/ /  \r\n";
    ss << "     \\ \\ / / |  _   _____  | |_) | |_| | | | |  _| |  \\| || | \\  /      \r\n";
    ss << "      \\ V /| |_| | |_____| |  __/|  _  | |_| | |___| |\\  || | /  \\       \r\n";
    ss << "       \\_/  \\____|         |_|   |_| |_|\\___/|_____|_| \\_|___/_/\\_\\   \r\n";
    ss << "            ____  _____ ____   ___  ____  _   _                             \r\n";
    ss << "           |  _ \\| ____| __ ) / _ \\|  _ \\| \\ | |                        \r\n";
    ss << "           | |_) |  _| |  _ \\| | | | |_) |  \\| |                          \r\n";
    ss << "           |  _ <| |___| |_) | |_| |  _ <| |\\  |                           \r\n";
    ss << "           |_| \\_\\_____|____/ \\___/|_| \\_\\_| \\_|                      \r\n";
    ss << "                                                                            \r\n";
    ss << "                                                                            \r\n";
    ss << boost::format("                      VERSION : %d.%d.%d ") % VERSION_MAJOR_NUMBER % VERSION_MINOR_NUMBER % VERSION_BUILD_NUMBER;
    ss << "                                                                            \r\n";
    ss << "                                                                            \r\n";

    std::cout << ss.str().c_str() << std::endl;
}

void WaitExit()
{
    std::string input;

    int maxIo = _getmaxstdio();
    do
    {        
        std::cout << std::endl;
        std::cout << "\t\tDigite 'quit' para sair." << std:: endl;
        std:: cin >> input;
    } while ( !boost::iequals(input,"quit") );

    //controllerPtr->Stop();
    
}


#pragma once

#include "StdIncludes.h"
#include "SwitchDefines.h"
#include "ISwitch.h"
//#include "IIoServiceAcceptor.h"
#include "IThrListen.h"
#include <boost/system/error_code.hpp>
#include <io.h>
#include "Logger.h"

using namespace boost::asio;
using namespace boost::posix_time;

#define TIMEOUT_SENDRECEIVE         60 // Segundos
#define MAX_CHAR_MESSAGE_TEXT		255

#define WM_CONSTRUCTOR            (WM_APP + 400)
#define WM_DESTRUCTOR             (WM_APP + 401)
#define WM_ADD_BOARD              (WM_APP + 402)
#define WM_REMOVE_BOARD           (WM_APP + 403)
#define WM_ADD_DATA               (WM_APP + 404)
#define WM_STATUS                 (WM_APP + 405)
#define WM_DETAILS                (WM_APP + 406)
#define WM_SPLASH                 (WM_APP + 407)
#define WM_TITLE                  (WM_APP + 408)
#define WM_COLUMN_SIZE            (WM_APP + 409)
#define WM_ADD_BOARD_WITH_NAME		(WM_APP + 410)
#define WM_ADD_BOARD_NETWORK      (WM_APP + 411)
#define WM_ADD_BOARD_LOG          (WM_APP + 412)
#define WM_ADD_RESOURCE           (WM_APP + 413)

// Indice do ImageListIcons
#define STATUS_BASE								  0
#define	ICON_IDLEIN		              STATUS_BASE+0
#define	ICON_IDLEOUT  		          STATUS_BASE+1
#define	ICON_INACTIVE 		          STATUS_BASE+2
#define	ICON_OFFERED  		          STATUS_BASE+3
#define	ICON_CONNECTED		          STATUS_BASE+4
#define	ICON_ANSWERED		            STATUS_BASE+5
#define	ICON_HANGUP   		          STATUS_BASE+6
#define	ICON_DROPCALL 		          STATUS_BASE+7
#define	ICON_BLOCKED  		          STATUS_BASE+8   // Blocked
#define	ICON_ALERTING 		          STATUS_BASE+9
#define	ICON_DIALING 		            STATUS_BASE+10
#define	ICON_RECORDING		          STATUS_BASE+11
#define	ICON_RESERVED		            STATUS_BASE+12
#define	ICON_LOCKED			            STATUS_BASE+13
#define	ICON_UNUSED1		            STATUS_BASE+14  // Reservado para uso futuro
#define	ICON_UNUSED2		            STATUS_BASE+15  // Reservado para uso futuro
#define	ICON_UNUSED3                STATUS_BASE+16  // Reservado para uso futuro
#define	ICON_UNUSED4                STATUS_BASE+17  // Reservado para uso futuro
#define	ICON_BOARDENABLED           STATUS_BASE+18  // Placa Enabled
#define	ICON_BOARDDISABLED          STATUS_BASE+19  // Placa Disabled
#define	ICON_INFORMATION            STATUS_BASE+20  // Icone de informacao
#define	ICON_NETWORK                STATUS_BASE+21  // Rede
#define	ICON_LOG                    STATUS_BASE+22  // Log
#define	ICON_TRUNK                  STATUS_BASE+23  // Tronco
#define	ICON_RESOURCE               STATUS_BASE+24  // Recurso

typedef struct
{
  char *Status;
  char *Type;
  char *DNIS;
  char *ANI;
  char *Info1;
  char *Info2;
} DETAILS;


typedef enum EVT_COMMAND_Objects
{
	DOREAD,
	DOWRITE,
    ONCLOSE,
	ONERROR
} EVT_COMMAND_Type;


struct EVT_COMMANDS
{
    EVT_COMMAND_Type Type;
    void* Object;
};

typedef struct _ST_DOREAD
{
	std::string sData;
	
} ST_DOREAD, *PST_DOREAD;

typedef struct _ST_DOWRITE
{
	std::string sData;
	
} ST_DOWRITE, *PST_DOWRITE;

typedef struct _ST_ONERROR
{
    int iErrorCode;
	std::string sErrorDescription;
	
} ST_ONERROR, *PST_ONERROR;


class StatusScreenNet
{
public:
    typedef StatusScreenNet self_type;

    SOCKET			            m_Socket;    

    std::string                 m_sReadMessage;    
	//log4cpp::Category * 		m_pLog4cpp;	
    boost::shared_ptr<Logger>   m_pLog4cpp;
    
	    
    StatusScreenNet( WPARAM wParam , ISwitch * p_Switch , IThrListen * p_ThrListen , boost::shared_ptr<Logger> pLog4cpp  );
    ~StatusScreenNet(void);	
	
    
    void Start( );
    void Stop();
    bool HasStarted() const { return m_bStop; }    

    typedef boost::shared_ptr< StatusScreenNet > ptr;
    typedef boost::system::error_code error_code;
    

    template<typename T> void AddCommand( T * Object )
	{	
		boost::mutex::scoped_lock lk(  m_EvtMutex );			
		
		EVT_COMMANDS * cmData = new EVT_COMMANDS();
		if( cmData )
		{			
			cmData->Object = Object;
			
			std::string strType = typeid(T).name();
			
			if ( strType.find("DOREAD") != std::string::npos )
			{
				cmData->Type = DOREAD;				
				m_pLog4cpp->debug( ">>>> COMMAND : DOREAD %p ENQUEUED.", Object );
			}

            if ( strType.find("DOWRITE") != std::string::npos )
			{
				cmData->Type = DOWRITE;				
				//m_pLog4cpp->debug( ">>>> COMMAND : DOWRITE %p ENQUEUED.", Object );
			}

            if ( strType.find("ONERROR") != std::string::npos )
			{
				cmData->Type = ONERROR;				
				m_pLog4cpp->debug( ">>>> COMMAND : ONERROR %p ENQUEUED.", Object );
			}

            m_CommandsQueue.push_front( cmData );				
			m_condQueue.notify_one();
        }
    }

    void EnqueueRead();
    void EnqueueError( const std::string & sErrorMessage );
    void EnqueueWrite( const std::string & Message );

    volatile bool                         m_bStop;
private:
    int                                     mError;
	ISwitch *                               m_pSwitch;
    IThrListen *                            m_pThrListen;
    CRITICAL_SECTION                        m_critClientScreenList;
    boost::shared_ptr<boost::thread>        m_MonitorThreadPtr;
    boost::shared_ptr<boost::thread>        m_PingThreadPtr;
    std::deque<EVT_COMMANDS *>        		m_CommandsQueue;
    boost::condition            			m_condQueue;	
    boost::mutex                			m_EvtMutex;

    
    void DoRead();
    void DoWrite( const std::string & Message );
    int Send(const char *Buffer, int Tamanho);
    int WaitToSend(int nTimeOut);
    void MonitorEvents();
    void Ping();
    

    void SendStringGrid(int ListIndex, int Trunk, int Item, int Grid, const char *sMsg);
    void LogScreenSystem(char *s);
    std::string WinTitle(void);
	void LogSys(const std::string  & message );
};


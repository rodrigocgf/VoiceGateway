﻿#include "Connection.h"
#include <sys/timeb.h>
#include "ConferenceManager.h"

ConnectionEvt::ConnectionEvt(ISwitch * p_Switch , boost::shared_ptr<Logger> pLog4cpp) : m_pSwitch(p_Switch), m_pLog4cpp(pLog4cpp)
{
    InitializeCriticalSection(&CritConnection);
}


ConnectionEvt::~ConnectionEvt(void)
{
    DeleteCriticalSection(&CritConnection);
}

// Envia para a conexao Destino o Evento X com informacao da conexao de Origem,
// indicando se houve mudanca do timeslot de transmissao local, default = false
bool ConnectionEvt::Connection_Send(ST_CONNECTION &Destino, int Evento, ST_CONNECTION &Origem, bool bLocalTimeSlotChange)
{
    EventName<std::stringstream> EvtName;

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__ );

    try 
	{		
        //m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, EvtName.GetEventName(Evento).str().c_str());

        EnterCriticalSection( &CritConnection );		

        if ( (m_pSwitch->GetSwitch() == Destino.Switch) &&
             (m_pSwitch->GetMachine() == Destino.Machine) )
        {
			// DESTINO LOCAL
            m_pLog4cpp->debug("[%p][%s] DESTINO LOCAL", this, __FUNCTION__);

			// Quando for esses eventos envia os mesmos para o canal via semaforo (discagem via script)		

            m_pLog4cpp->debug("[%p][%s] Evento = %s", this, __FUNCTION__,
                (Evento == T_SW_DIAL_EX_DISCONNECTED?"T_SW_DIAL_EX_DISCONNECTED":
                (Evento == T_SW_DIAL_EX_RESULT?"T_SW_DIAL_EX_RESULT":
                (Evento == T_SW_CONF_REQUEST?"T_SW_CONF_REQUEST":"UNKNOWN EVENT"))) );

			if( (Evento == T_SW_DIAL_EX_DISCONNECTED) ||
				(Evento == T_SW_DIAL_EX_RESULT) )
            {

				// Passa as informacoes do canal local para o canal remoto
				MapConnection[Destino.Channel].push(Origem);
				// Envia evento para o canal remoto

                Ivr * pIvr = m_pSwitch->GetIvr(Destino.Channel);
                pIvr->SendIvrEvent(Destino.Channel);
		
                LeaveCriticalSection(&CritConnection);
				
				return true;
			}

#ifdef _FAX
				
			if( (Evento == T_SW_SEND_FAX) || 
				(Evento == T_SW_RECV_FAX) ||
				(Evento == T_SW_INIT_FAX) )                
            {

				// Passa as informacoes do canal local para o canal remoto
				MapConnection[Destino.Channel].push(Origem);
				// Envia evento para o canal remoto
				Fax::SendFaxEvent(Destino.Channel);
					
				CritConnection.Leave();
				return true;
			}
#endif // _FAX
                
			//Verifica se s�o eventos de confer�ncia vindos do IVR
			if(Evento == T_SW_CONF_REQUEST)
			{

				//Enfilera pacote de dados (ST_CONNECTION)
				QueueConnectionConference.push(Origem);
					
				// Envia evento 
                if ( m_pSwitch->GetConferenceManager() != NULL )
                    m_pSwitch->GetConferenceManager()->SendConferenceEvent();
				//ConferenceManagerClass::SendConferenceEvent();
					
				LeaveCriticalSection(&CritConnection);

				return true;
			}



#ifdef _APPLICATION_SOCKET

        if(Destino.Channel < 0 )
        {
					// Pega o id da conexao tcp/ip
					int index = Destino.Channel * -1;
          
					//Enfilera pacote de dados (ST_CONNECTION)
					MapConnectionMonitor[index].push(Origem);

					// Envia evento 
          ConferenceMonitor::SendConferenceEvent(index);
					
					CritConnection.Leave();
					return true;
        }

#endif  // _APPLICATION_SOCKET

			// Passa as informacoes do canal local para o canal remoto
			MapConnection[Destino.Channel].push(Origem);

            int iBoardID = m_pSwitch->GetBoardId(Destino.Channel);

            if ( m_pSwitch->GetManufacturerName(Destino.Channel) == "DIALOGIC" )			
			{

                m_pLog4cpp->debug("[%p][%s] DIALOGIC BOARD", this, __FUNCTION__);

				//m_pSwitch->GetTapi()->Create(DIALOGIC);
				// Envia evento para o canal local
                if ( m_pSwitch->GetTapi() != NULL )
                {                    
                    if ( m_pSwitch->GetTapi()->GetCTI() != NULL ) 
                    {                        
				        m_pSwitch->GetTapi()->GetCTI()->SendEvent(Destino.Channel, Evento);
                    } else 
                        m_pLog4cpp->debug("[%p][%s] Tapi's Cti NOT instantiated.", this, __FUNCTION__);
                } else
                    m_pLog4cpp->debug("[%p][%s] Tapi NOT instantiated.", this, __FUNCTION__);
			}
			else if( m_pSwitch->GetManufacturerName(Destino.Channel) == "KHOMP")
			{
				

                int trunkID = m_pSwitch->GetTrunkId(Destino.Channel);				

                if ( m_pSwitch->GetTrunkSignalingName(trunkID) == "SIP" )
				{
					// Envia evento para o canal local
					// Soma 30 para pular os 30 canais TDM e subtrai 1 para comecar os canais a partir de 0.
                    m_pSwitch->GetTapi()->GetCTI()->SendEvent( m_pSwitch->GetTrunkChannelID(Destino.Channel)  + 30 - 1, Evento);
				}
				else
				{
					// Envia evento para o canal local
					// Subtrai 1 para comecar os canais a partir de 0.
					m_pSwitch->GetTapi()->GetCTI()->SendEvent( m_pSwitch->GetTrunkChannelID(Destino.Channel) - 1, Evento);
				}
			}
			
				
			LeaveCriticalSection(&CritConnection);
			return true;
		}
		else
		{
            m_pLog4cpp->debug("[%p][%s] DESTINO REMOTO\r\n", this, __FUNCTION__);

			// DESTINO REMOTO

            TCP_MSG tcp_msg;
		    tcp_msg.Data			= Destino.sData;
            tcp_msg.RemoteId	= MAKELONG(Destino.Machine, m_pSwitch->GetIdTypeApp() );

            m_pLog4cpp->debug("[%p][%s] <missing code here> !!!\r\n", this, __FUNCTION__);
		    //TcpIp_Send(tcp_msg);

            LeaveCriticalSection(&CritConnection);
            return true;
		}

		LeaveCriticalSection(&CritConnection);
		return false;
	}
	catch(...)
    {

		LeaveCriticalSection(&CritConnection);
		Log(LOG_ERROR , "EXCEPTION: Connection_Send(ST_CONNECTION, int, ST_CONNECTION)");
		return false;
	}
}

// Envia evento para a conexao Destino na propria maquina
bool ConnectionEvt::Connection_Send(ST_CONNECTION &Destino, int Evento)
{
    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , Destino.sData.c_str() );

    try {
		EnterCriticalSection( &CritConnection );

		// Passa as informacoes do canal local para o canal remoto
		MapConnection[Destino.Channel].push(Destino);

        int iBoardID = m_pSwitch->GetBoardId(Destino.Channel);		

        if ( m_pSwitch->GetManufacturerName(Destino.Channel) == "DIALOGIC" )
		{
            
			// Envia evento para o canal local
            m_pLog4cpp->debug("[%p][%s] Going to call DIALOGIC SendEvent to channel %d", this, __FUNCTION__ , Destino.Channel );
			m_pSwitch->GetTapi()->GetCTI()->SendEvent(Destino.Channel, Evento);
		}
		else if( m_pSwitch->GetManufacturerName(Destino.Channel) == "KHOMP")
		{			

            int trunkID = m_pSwitch->GetTrunkId(Destino.Channel);
			
			if ( m_pSwitch->GetTrunkSignalingName(trunkID) == "SIP" )
			{
				// Envia evento para o canal local
				// Soma 30 para pular os 30 canais TDM e subtrai 1 para comecar os canais a partir de 0.
				m_pSwitch->GetTapi()->GetCTI()->SendEvent( m_pSwitch->GetTrunkChannelID(Destino.Channel) + 30 - 1, Evento);
			}
			else
			{
				// Envia evento para o canal local
				// Subtrai 1 para comecar os canais a partir de 0.
				m_pSwitch->GetTapi()->GetCTI()->SendEvent( m_pSwitch->GetTrunkChannelID(Destino.Channel) - 1, Evento);
			}
		}
		
		
		LeaveCriticalSection(&CritConnection);
		return true;
	}
	catch(...)
    {
		LeaveCriticalSection(&CritConnection);
		Log(LOG_ERROR, "EXCEPTION: Connection_Send(ST_CONNECTION, int)");
		return false;
	}


    return true;
}

// Envia evento para um canal local com informacoes de origem remota (ATM)
bool ConnectionEvt::Connection_Send(int Canal, ST_CONNECTION &Origem)
{

    return true;
}

// Pega a conexa de acordo com o canal passado, retorna true e tem conexao ou false se nao tem
bool ConnectionEvt::Connection_Get(int Channel, ST_CONNECTION &ref_st_connection, int iEvent)
{   
    try {

		EnterCriticalSection( &CritConnection );

		if(MapConnection[Channel].empty())
        {
			LeaveCriticalSection(&CritConnection);
			return false;
		}

		if( iEvent == -1 )
		{
			ref_st_connection = MapConnection[Channel].front();

			MapConnection[Channel].pop();

			LeaveCriticalSection(&CritConnection);

			return true;
		}
		else
		{
			int i;

			for( i = 0; i < MapConnection[Channel].size(); i++ )
			{
				ref_st_connection = MapConnection[Channel].front();

				MapConnection[Channel].pop();

				if( ref_st_connection.Event == iEvent )
				{
					LeaveCriticalSection(&CritConnection);
					return true;
				}

				MapConnection[Channel].push( ref_st_connection );
			}
			LeaveCriticalSection(&CritConnection);

			return false;
		}

	}
	catch(...)
    {
		LeaveCriticalSection(&CritConnection);
		Log(LOG_ERROR, "EXCEPTION: Connection_Get()");
		return false;
	}
}

/************************************************************************************************/
/* Metodo: Connection_Get (ST_CONNECTION)																												*/
/*                                                                                              */
/* Descr.:  Pega um dado da estrututa ST_CONNECTION da fila                                     */
/*																																															*/
/* Obs   :  Trabalha com a estrutura da confer�ncia (ou seja, sem indexa��o por canal)					*/
/*																																															*/
/* Param:   [in] Dados																																					*/
/*																																															*/
/* Retorno: false - Sem dados, true - pegou dado                                                */
/************************************************************************************************/
bool ConnectionEvt::Connection_Get(ST_CONNECTION &ref_st_connection)
{
	try 
	{

        m_pLog4cpp->debug( "GetConnection(string)");

		EnterCriticalSection( &CritConnection );

		if(QueueConnectionConference.empty ())
        {
			LeaveCriticalSection(&CritConnection);
            return false;
        }

		ref_st_connection = QueueConnectionConference.front ();

		QueueConnectionConference.pop ();
				
		LeaveCriticalSection(&CritConnection);
		return true;
	}
	catch(...)
	{
		LeaveCriticalSection(&CritConnection);
		m_pLog4cpp->error("EXCEPTION: Connection_Get()");
		return false;
	}
}

// Pega a conex�o da fila de conex�es da confer�ncia. Retorna true se tem conexao ou false se nao tem
bool ConnectionEvt::Connection_Get_Monitor(ST_CONNECTION &ref_st_connection, int index)
{

    return true;
}

// Limpa o map referente ao canal
void ConnectionEvt::Connection_Clear(int Channel)
{
    try 
    {
        EnterCriticalSection( &CritConnection );

		for(int i = 0; !(MapConnection[Channel].empty()); i++)
        {
			MapConnection[Channel].pop();
		}

		LeaveCriticalSection(&CritConnection);
	}
	catch(...){
		LeaveCriticalSection(&CritConnection);
		Log(LOG_ERROR, "EXCEPTION: Connection_Clear()");
	}
}

// Entra em uma secao critica para proteger o envio de eventos de uma canal para o outro
void ConnectionEvt::Connection_Lock(void)
{

}

// Sai da secao critica que protege o envio de eventos de uma canal para o outro
void ConnectionEvt::Connection_Unlock(void)
{

}

// Retorna o status do canal
int ConnectionEvt::Connection_ChannelStatus(int Channel)
{
    try
	{
        int iBoardID = m_pSwitch->GetBoardId(Channel);
        		
		
		if( m_pSwitch->GetManufacturerName(Channel) == "KHOMP")
		{
            
			
            int trunkID = m_pSwitch->GetTrunkId(Channel);
			
			if ( m_pSwitch->GetTrunkSignalingName(trunkID) == "SIP" )
			{
				// Soma 30 para pular os 30 canais TDM e subtrai 1 para comecar os canais a partir de 0.
				Channel = m_pSwitch->GetTrunkChannelID(Channel) + 30 - 1;
			}
			else
			{
				// Subtrai 1 para comecar os canais a partir de 0.
				Channel = m_pSwitch->GetTrunkChannelID(Channel) - 1;
			}
		}

		int ret = m_pSwitch->GetTapi()->GetCTI()->GetChannelStatus(Channel);
		
		
		
		return ret;
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Connection_ChannelStatus()");
		return false;
	}

}


void ConnectionEvt::Log(int iLevel, const char *sformat, ...)
{    
    tm * Time;
    _timeb TimeBuffer;
    int len = 0;
    char szMessage[500];

    char sMsg[ 450 + 1 ];    
    memset(sMsg , 0x00 , sizeof(sMsg) );

    _ftime( &TimeBuffer );
	Time = localtime( &TimeBuffer.time );

	va_start( va, sformat );
	_vsnprintf( sMsg, 450 + 1, sformat, va );
	va_end( va );
	sMsg[ 450 ] = 0;

    // Substitui enventuais caracteres | (pipe) da mensagem por #
	for ( int i = 0; i < strlen( sMsg ); i++ )
		if ( sMsg[i] == '|' )
			sMsg[i] = '#';

    len = sprintf( szMessage,"%04d-%02d-%02d %02d:%02d:%02d.%03d", Time->tm_year+1900, Time->tm_mon + 1, Time->tm_mday, Time->tm_hour, Time->tm_min, Time->tm_sec, TimeBuffer.millitm );
    len += sprintf( &szMessage[len], "|---|GTW|               |%s", sMsg  );
        
    m_pLog4cpp->debug(szMessage);
}
#pragma once

#include <io.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>
#include <typeinfo>
#include "Logger.h"

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>


#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

#include <winsock2.h>
#include <windows.h>

#include "ISwitch.h"

typedef struct fstreamCdr
{    
    FILE *                                  mp_outFile;
    std::string                             m_filename;
    int                                     ChangeFile;
} STDIO_CDR;


typedef std::map<std::string , STDIO_CDR> MAP_LOGCDR;


class Cdr
{
private:
    boost::condition            			m_cond;	
    volatile bool               			m_stop;   
    boost::mutex                			m_Mutex;

    boost::shared_ptr<boost::thread>        m_ExecuteThreadPtr;    
    boost::shared_ptr<Logger>               m_pLog4cpp;
    ISwitch *                               m_pSwitch;

    int ReadIndexFromFile(const std::string & sFile);
    void WriteIndexToFile(const std::string & sFilename , int index);
    
public:
    Cdr(boost::shared_ptr<Logger> pLog4cpp, ISwitch * pSwitch);
    Cdr();
    ~Cdr();
    
    MAP_LOGCDR                              MapLogCdr;
    CRITICAL_SECTION                        CritLogCdr;

    void ThreadInitSystem();
    void ThreadEndSystem();    
    void MainLoop();    

};


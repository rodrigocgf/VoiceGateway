#pragma once


#include <stdio.h>
#include <string>
#include <tchar.h>
#include <winsock2.h>
#include <windows.h>
#include <boost/shared_ptr.hpp>
#include "CTIBase.hpp"
#include "Logger.h"

// Parametros gerais passados para o construtor de telefonia
typedef struct
{
	int iListIndex;								// Indice para a lista da tela para os bits CAS
	int iBoardTrunk;							// Item do indice para a lista da tela para os bits CAS
	int iBoardPort;								// Item da tela  para os bits CAS
	int Tecnologia;								// Tipo de tecnologia
	int Channel;									// Numero do canal
	int Site;											// Numero do site
	int Machine;									// Numero da m�quina
	int iCtiLogLevel;							// Nivel de log para a funcao CtiLog
	int R2DnisMinimumSize;				// Numero minimo de Dnis para pegar em R2 Digital 
	
	bool bAsrEnabled;							// Flag indicando se o o reconhecimento de voz esta ativo
	string ASR_GrammarPackage;		// Path para o pacote de gramatica
	string ASR_DatabaseType;			// Tipo de banco de dados para o reconhecimento de voz
	string ASR_DatabaseServer;		// Servidor do banco de dados para o reconhecimento de voz
	string ASR_DatabaseName;			// Nome do banco de dados para o reconhecimento de voz
	string ASR_DatabaseUser;			// Usuario do banco de dados para o reconhecimento de voz
	string ASR_DatabasePassword;	// Senha para o banco de dados para o reconhecimento de voz
	string bsTtsServerHost;				// Nome ou IP da maquina do servidor de TTS
	int ASR_PollingTime;					// Tempo maximo no polling de eventos da dialogic na implementacao de ASR
} TAPI_PARAM;

class Tapi
{
private:
    
    TAPI_PARAM *                m_pTapiParam;
    boost::shared_ptr<Logger>   m_pLog4cpp;

public:
    Tapi(boost::shared_ptr<Logger>  pLog4cpp , TAPI_PARAM *pTapiParam = NULL);
    
    virtual void InitSystem();
    virtual void EndSystem();
	
    
    boost::shared_ptr<CTIBase>            m_CTIBasePtr;
    CTIBase * GetCTI() const;
};


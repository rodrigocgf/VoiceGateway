#ifndef DIALOGIC_DEFINES_H
#define DIALOGIC_DEFINES_H


// dialogic
#include <srllib.h>
#include <dxxxlib.h>

//#include <faxlib.h>
//#include <dtilib.h>
//#include <msilib.h>
//#include <dcblib.h> 
#include <sctools.h>
//#include <dxxxlib.h>
//#include <dxxxlib.h>

// Numero minimo de rings que deve ser tocado na estacao MSI para ter certeza que vai ringar
#define	MSI_MINIMUM_NUMBER_RINGS			3

// Valor de retorno usado na funcao longjmp para desligar uma chamada
#define JUMP_DISCONNECT								1

// Valor de retorno usado na funcao longjmp para sair da thread
#define JUMP_EXIT											2

// Valor de retorno usado na funcao longjmp para tratar o bloqueio da linha
#define JUMP_BLOCKED									3


enum Events {
	T_CALL = (TDX_UNKNOWN + 1),	// Chamada chegou
	T_ACCEPT,										// Chamada aceita
	T_ACKCALL,									// Pegou mais numero de DNIS
	T_MAKE_CALL,								// Fazer chamada
	T_DISCONNECT,								// Desligou
	T_CONNECT,									// Ligacao de saida atendida
	T_BUSY,											// Telefone ocupado na discagem
	T_NOANSWER,									// Telefone nao atende na discagem
	T_NODIALTONE,								// Sem tom de linha na discagem
	T_PLAY,											// Funcao de play terminou
	T_RECORD,										// Funcao de record terminou
	T_SILENCE,									// Houve silencio durante a execucao da funcao Record
	T_MAXDIG,										// O numero de digitos requisitados, foram digitados
	T_DIGIT,										// Veio um digito pela fila de eventos
	T_INTERDIG,									// Houve timeOut durante uma coleta de digitos
	T_DIAL,											// Discagem comletada para canal de voz sem CallProgress
	T_ANSWER,										// Ligacao de entrada atendida
	T_ALERTING,									// Terminou a discagem e esta ringando
	T_PLAYTONE,									// Terminou de tocar um tom
	T_TASKFAIL,									// Chamada de funcao em estado invalido
	T_DROPCALL,									// Chamada foi desconnectada com sucesso
	T_NOTUSED,									// Evento vago ou nao usado
	T_ERROR,										// Erro
	T_TIMER,										// O Timer programado pelo usuario chegou ao fim
	T_NO_EVENTS,								// Sem eventos
	T_BLOCKED,									// O canal foi bloqueado
	T_IDLE,											// O canal foi liberado
	T_USER_BLOCK,								// Evento para bloquear o canal
	T_USER_UNBLOCK,							// Evento para desbloquear o canal
	T_RESET,										// Evento gerado apos o reset da linha
	T_TONEON,										// Evento de tom na linha
	T_SILENCE_ON,								// Evento de inicio de silencio na linha
	T_SILENCE_OFF,							// Evento de fim de silencio na linha
	T_LOOP_CURRENT_ON,					// Evento de inicio de loop de corrente
	T_LOOP_CURRENT_OFF,					// Evento de fim de loop de corrente
	T_TONEOFF,									// Evento de tom na linha
	T_SEIZE,										// Evento de ocupacao do canal em protocolo R2 Digital
	T_HOLD,											// Evento de chamada em espera
	T_RESUME,										// Evento de resumo de chamada
	T_MESSAGEBOX,								// Evento indicando o atendimento da caixa postal do usuario de celular
	T_SETBILLING,								// Evento indicando a confirmacao na alteracao da bilhetagem da chamada. gc_SetBilling().
	T_MESSAGEINI,								// Evento indicando o inicio da grava��o da mensagem na caixa postal
	T_OPENEX,										// Evento indicando abertura do canal IP
	T_LISTEN,										// Evento indicando que o timeslot foi routeado com sucesso
	T_UNLISTEN,									// Evento indicando que o timeslot foi desrouteado com sucesso
	T_DISCONNECT2,							// Desligou a segunda chamada (outbound)
	T_UNBLOCKED,								// O canal foi desbloqueado
	T_INVOKEXFER,								// A chamada foi transferida com sucesso
	T_SWITCH										// Evento da switch, SEMPRE ULTIMO
};

enum DropCall {
	DROP_NORMAL = 0,
	DROP_BUSY,
	DROP_CONGESTION
};

enum CasBits {
	BIT_AR,
	BIT_BR,
	BIT_CR,
	BIT_DR,
	BIT_AT,
	BIT_BT,
	BIT_CT,
	BIT_DT,
};

enum InterfaceType {
	E1,									// Interface de linha digital E1 (canal de voz associado)
	T1,									// Interface de linha digital T1 (canal de voz associado)
	LSI,								// Interface de linha analogica (canal de voz associado)
	DTI,								// Interface de linha digital
	MSI,								// Interface de posicao de atendimento (sem canal de voz associado)
	VOX,								// Interface de recurso de voz sem interface de linha associado
	FAX,								// Interface de recurso de fax sem interface de linha associado
	DEV,								// Interface de recurso GlobalCall
	IP,									// Interface de linha IP
};


enum Protocols {
	R2,									// Protocolo R2 digital
	GLOBAL_CALL,				// API GlobalCall
	GLOBAL_CALL_SS7,		// API GlobalCall SS7
	GLOBAL_CALL_IP,			// API GlobalCall IP
	ISDN,								// Protocolo ISDN	
	LOOP_START,					// Protocolo analogico (loop start interface)
	LINE_SIDE,					// Protocolo CAS DTMF para Lucent Definity
	LOOP_START_TRAP,    // Protocolo analogico (loop start interface) para grampo telefonico
	R2_DIGITAL,					// Protocolo R2 digital
	GLOBAL_CALL_DM3,		// API GlobalCall para placas DM3
	ISDN_4ESS,					// ISDN 4ESS AT&T
	ISDN_NTT,						// ISDN NTT JAPAO
	ISDN_QSIG,					// ISDN QSIG
	ISDN_CTR4,					// ISDN CTR4 EURO
	ISDN_DMS,						// ISDN DMS (NORTEL A211-1 AND A211-4)
	RESOURCE						// Recurso de voz
};


enum BoardManufacturer {
	bmDIALOGIC	=	0x00000001,
	bmAMTELCO		=	0x00000002
};

enum BoardFamily {
	bfDM3					=	0x01,
	bfSPRINGWARE	=	0x02,
	bfXDS					= 0x03
};

enum TrunkSignaling {
	tsNOSIGNALING	= 0x10,
	tsLOOPSTART		=	0x20,
	tsISDN				=	0x30,
	tsSS7					=	0x40,
	tsLINESIDE		=	0x50,
	tsR2					= 0x60
};

enum FileFormats {
	VOX24 = 0,					// Formato de arquivo de voz ADCPCM			  	6KHZ	4 BITS
	VOX32,							// Formato de arquivo de voz ADCPCM					8KHZ	4 BITS
	PCM48,							// Formato de arquivo de voz PCM					  6KHZ	8 BITS
	PCM64,							// Formato de arquivo de voz PCM						8KHZ	8 BITS
	WAV,								// Formato de arquivo de voz WAV						11KHZ 8	BITS
	WAV_G711_ALAW,			// Formato de arquivo de voz WAV_G711_ALAW	8KHZ	8	BITS (DM3)
	PCM_G711_ALAW,			// Formato de arquivo de voz PCM_G711_ALAW	8KHZ	8	BITS (DM3)
  WAV_PCM, 						// Formato de arquivo de voz WAV_PCM 				8KHZ	8	BITS (DM3)
};

enum PCMEncoding {
	ULAW,								// Formato ULAW de PCMEnconding para arquivo de voz do tipo PCM
	ALAW								// Formato ALAW de PCMEnconding para arquivo de voz do tipo PCM
};

enum EventosMsi {
	MSI_ONHOOK,					// Estacao colocou o fone no gancho
	MSI_OFFHOOK,				// Estacao tirou o fone no gancho
	MSI_HOOKFLASH,			// Estacao executou um HookFlash no telefone
	MSI_RING_OFFHOOK,		// Estacao tirou o fone do gancho durante Ring
	MSI_RING_TERM,			// O tempo de ring terminou e a Estacao nao tirou o fone do gancho
	MSI_NORING,					// Nao foi possivel gerar ring na estacao
	MSI_RING_STOP				// O ring foi terminado pela funcao ms_stopfn()
};

enum MsiStationStatus {
	STATION_ONHOOK,			// Estacao esta com o fone no gancho
	STATION_OFFHOOK			// Estacao esta com o fone fora do gancho
};

enum ScBus {
	FULLDUP,						// Tipo de conexao SC_BUS FULL_DUPLEX
	HALFDUP							// Tipo de conexao SC_BUS HALF_DUPLEX
};

enum ChannelStates {
	CHANNEL_IN_SERVICE,					// Canal livre para atender ligacoes
	CHANNEL_OUT_OF_SERVICE			// Canal for de servico
};

enum ExtendedConnectionTypes {
	EXT_LISTEN,				// A estacao de Supervisao so ouve a conexao
	EXT_COACH_PUPIL,	// A estacao de Supervisao fala com a Atendente, mas o usuario nao ouve
	EXT_CHAIR_PERSON,	// A estacao de Supervisao fala com todos
};

enum VoxStates {
	S_DIAL,			// Dial state
	S_CALL,			// Call state
	S_GETDIG,		// Get Digit state
	S_HOOK,			// Hook state
	S_IDLE,			// Idle state
	S_PLAY,			// Play state
	S_RECORD,		// Record state
	S_STOPED,		// Stopped state
	S_TONE,			// Playing tone state
	S_WINK,			// Wink state
	S_UNKNOWN		// Unknown
};

enum ChannelStatus {
	CHANNEL_DISABLED,						// Canal desabilitado    
    CHANNEL_NULL,
    CHANNEL_DETECTED,
    CHANNEL_PROCEEDING,
	CHANNEL_IDLE,							// Canal livre
	CHANNEL_BLOCKED,						// Canal bloqueado
	CHANNEL_OFFERED,						// Canal recebendo uma chamada (ENTRADA)
	CHANNEL_ACCEPTED,						// Canal aceitou uma chamada (ENTRADA)
	CHANNEL_ANSWERED,						// Canal atendeu uma chamada (ENTRADA)
	CHANNEL_ALERTING,						// Canal esta alertando (ringing) o telefone de destino (SAIDA)
	CHANNEL_CONNECTED,					// Canal estabeleceu uma chamada (SAIDA)
	CHANNEL_DISCONNECTED,				// Canal desconectou uma chamada (ENTRADA/SAIDA)
	CHANNEL_CALLSTATUS,					// Canal nao conseguiu completar uma chamada (SAIDA)
	CHANNEL_DIALING,						// Canal discando (SAIDA)
	CHANNEL_ONHOLD							// Canal em espera
};



enum DisconnectionReasons {
	REASON_NORMAL,			// Desconexao normal
	REASON_NOANSWER,		// Telefone nao atende
	REASON_BUSY,				// Telefone ocupado
	REASON_CONGESTION		// Congestionamento
};

// Enum representing the ASR answer types.
enum ASR_AnswerType
{
	ASR_Rejected			= -1,
	ASR_NoSpeech			= -2,
	ASR_Exception			= 0xFFFF
};

// Enum representing the direction on the channels
enum ChannelDirection
{
	CH_OUTBOUND				=	0,
	CH_INBOUND,
	CH_DUAL
};

// Enum specifying the set of tones to be detected.
enum TonesSetType
{
	TST_VOICE_MAIL = 0
};

enum BulkQueueBufferSizeType
{
	bqbs8Kb = 1,
	bqbs4Kb = 0,
	bqbs2Kb = 4,
	bqbs1Kb = 6
};

#endif // DIALOGIC_DEFINES_H

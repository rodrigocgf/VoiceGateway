#pragma once

/*
#include "log4cpp/Category.hh"
#include "log4cpp/RollingFileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/Priority.hh"
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h> 
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>



#define MAX_CHAR_MESSAGE_TEXT	10000

class Logger
{
private:
    va_list va;												

    // LOG4CPP
    //log4cpp::Category * 					m_pLog4cpp;	    
    //boost::shared_ptr<log4cpp::Layout>      m_layoutPtr;    
    //boost::shared_ptr<log4cpp::Appender>   m_rfileAppenderPtr;
    //log4cpp::Category *                     m_category;

    FILE *                                  mp_OutFile;
    std::string                             m_FileName;
    

    std::string                             m_sBasePathLog;
    std::string                             m_sFullLogPathName;

    std::string                             m_CurrentDay;
    std::string                             m_CurrentMonth;
    std::string                             m_CurrentYear;

    std::string                             m_sLogName;
    int                                     m_iLogMaxFileSize;    
    bool                                    m_bCheckNewDay;
    int                                     m_Index;
    bool IsNewDay();
public:    
    Logger(const std::string & p_sBasePathLog, int p_iMaxFileSize, const std::string & p_sLogName = "Log_System" , int index = 0 , bool bCheckNewDay = true );
    ~Logger(void);

    void MountLogPath();    
    void ConfigLog4cpp();
    void debug(const char *sformat, ...);
    void error(const char *sformat, ...);

    void StopLog();
    std::string GetLayoutPattern();
    
};

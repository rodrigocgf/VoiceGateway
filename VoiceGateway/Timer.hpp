#ifndef _TIMER_H_
#define _TIMER_H_

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/lexical_cast.hpp>

#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

using namespace boost;
using namespace std;

#define MAX_WAIT_THR	300

//template<typename T>
class Timer
{
	
public:
	
	Timer( std::function< void (const std::string & )> _callback , const std::string & tid )	:	m_sTimerId(tid),	
																					m_bStop(false),
																					m_bThrStopped(false),
																					m_tick_count(0),
																					m_total_diff(0),																					
																					fOnCallBack(_callback)
	{
		m_msInterval = 0;
		m_active = false;        		
	}

		
	~Timer() 
	{ 		
		Destroy();
	}

	void Create()
	{		
		m_pThread.reset( new boost::thread ( boost::bind (&Timer::MainThreadLoop, this) ) );
		
	}

	void Destroy()
	{
        try
        {
		    m_bStop = true;
		    m_active = false;			
        
        
		    boost::mutex::scoped_lock lock(m_mtxTimerWait);					
		
		    int count = 0;
		    while (!m_bThrStopped && (count < MAX_WAIT_THR)) 
		    {                
			    m_conditionTimer.timed_wait( lock , boost::posix_time::milliseconds(1) );						
			    count++;						
		    }
        }
        catch(...)
        {
            std::cout << "Timer Destroy exception caught." << std::endl;
        }
        
	}
	
	void Start()
	{
		m_active = true;
		m_before = boost::posix_time::microsec_clock::universal_time();		
	}

	void Stop()
	{	
		m_active = false;
		m_after = boost::posix_time::microsec_clock::universal_time();
		boost::posix_time::time_duration duration = m_after - m_before;
	}

	void SetInterval(const std::size_t& milliseconds)
	{
		m_msInterval = milliseconds;
	}

    std::string GetTimerId() 
    {
        return m_sTimerId;
    }

private:
		
	boost::shared_ptr<boost::thread> 		m_pThread;
	
	volatile int						m_intEventTimer;
	boost::condition_variable			m_conditionTimer;
	boost::mutex						m_mtxTimerWait;	
	
    bool DoWait()
	{
		bool bRet = false;
			
		boost::this_thread::sleep_for (boost::chrono::milliseconds(1) );			

		if ( m_intEventTimer == 1 )
			bRet = true;

		return bRet;
	}
		
	void MainThreadLoop()
	{	
		try
		{
			while ( !DoWait() && !m_bStop )		
			{
                

				if ( m_active )
				{
					m_after = boost::posix_time::microsec_clock::universal_time();
					boost::posix_time::time_duration duration = m_after - m_before;

					if ( m_msInterval > (std::size_t)0 )
					{
						if ( (double)m_msInterval < (double)duration.total_milliseconds() )
						{
							OnTimer();					
						}
					}
				}
			}			
		}
		catch(...)
		{
			std::cout << "HmpTimer::MainThreadLoop exception! \r\n";
		}
		
		m_bThrStopped = true;
		m_conditionTimer.notify_one();
	}
		
	void OnTimer()
	{	
		if ( fOnCallBack != nullptr )
			fOnCallBack(m_sTimerId);
	}
	
	std::function< void (const std::string &)> fOnCallBack;    
	//T							* m_pParent;		
	
    std::string             m_sTimerId;
	std::size_t				m_msInterval;
	std::size_t				m_tick_count;		
	boost::posix_time::ptime	m_before;
	boost::posix_time::ptime	m_after;
	std::size_t				m_total_diff;
	volatile bool				m_bStop;
	volatile bool				m_bThrStopped;
	volatile bool				m_active;
};

#endif
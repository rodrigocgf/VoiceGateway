/**********************
	Dialogic.cpp

	Bibliotecas a serem incluidas na compilacao

	libdtimt.lib	funcoes DTI
	libdxxmt.lib	funcoes VOX
	libfaxmt.lib	funcoes FAX
	libgc.lib			global call
	libsrlmt.lib	manipulacao de eventos
	30/MAI/2003 - Jairo Rosa   - Removida a flag bTtsPlaying
*********************************************************************************************/

#include <boost/dll.hpp>
#include "Dialogic.h"
//#include "atlstr.h"
//#include "Switch.h"
#include <lua.hpp>

struct PAR{
	char *nome_par;
	char *valor_default;
};

static const PAR ParDXBD[] = {

	"flashtm"	,	"50",				// tempo de flash 50(x10ms)
	"pausetm"	,	"200",			// pausa da virgula 200(x10ms)
	"digrate"	,	"600",			// razao de digitalizacao 6000 Hz
	"sch_tm"	,	"20",				// scheduler time 20 * 1/18secs
	"p_bk"		,	"6",				// intervalo de discagem por pulso offhook 6(x10ms)
	"p_mk"		,	"4",				// intervalo de discagem por pulso onhook  4(x10ms)
	"p_idd"		,	"100",			// tempo interdigito de discagem por pulso 100(x10ms)
	"t_idd"		,	"5",				// tempo interdigito de discagem por tom 5(x10ms)
	"oh_dly"	,	"50",				// atraso de execucao apos offhook 50(x10ms)
	"r_on"		,	"3",				// duracao de voltagem-on  de um ring 3(x100ms)
	"r_off"		,	"5",				// duracao de voltagem-off de um ring 5(x100ms)
	"r_ird"		,	"80",				// intervalo entre rings 80(x100ms)
	"s_bnc"		,	"4",				// minimo tempo para gerar t_silon ou t_siloff 4(x10ms)
	"ttdata"	,	"10",				// duracao dos digitos na discagem por tom 10(x10ms)
	"minpdon"	,	"2",				// minimo intervalo para digito loop pulso-on  2(x10ms)
	"minpdoff",	"2",				// minimo intervalo para digito loop pulso-off 2(x10ms)
	"minipd"	,	"25",				// tempo interdigito para loop pulso 25(x10ms)
	"minlcoff",	"-1",				// duracao minima para loop de corrente off -1(x10ms)
	"redge"		,	"2",				// deteccao de ring na borda de subida=1 ou descida=2, 2
	"maxpdoff",	"50",				// maximo intervalo para digito loop pulso-on 50(x10ms)

	NULL, NULL
};

static const PAR ParCAP[] = {

	"stdely"			,	"25",		// atraso depois de discar e comecar analise 25(x10ms)
	"cnosig"			,	"4000",	// maximo silencio na linha ao discar 4000(x10ms)
	"lcdly"				,	"400",	// atraso na analise de loop de corrente ao discar 400(x10ms)
	"lcdly1"			,	"10",		// atraso depois de loop de corrente=conect 10(x10ms)
	"hedge"				,	"2",		// retorna um conect na borda de subida=1 ou descida=2, 2
	"intflg"			,	"4",		// habilita deteccao positiva de voz
	"intfltr"			,	"10",		// minimo sinal para deteccao de frequencia 10(x10ms)
	"lowerfrq"		,	"900",	// frequencia baixa do primeiro tom de um SIT 900(hz)
	"upperfrq"		,	"1000",	// frequencia alta do primeiro tom SIT 1000(hz)
	"timefrq"			,	"5",		// minimo tempo para deteccao do primeiro tom SIT 5(x10ms)
	"maxansr"			,	"1000",	// maxima resposta para PVD retornar um conect 1000(x10ms)
	"ansrdgl"			,	"-1",		// maximo silencio para medir a saudacao em PVD -1(x10ms)
	"mxtimefrq"		,	"0",		// maximo tempo para deteccao do primeiro tom SIT 0(x10ms)
	"lower2frq"		,	"0",		// frequencia baixa do segundo tom de um SIT 0(hz)
	"upper2frq"		,	"0",		// frequencia alta do segundo tom SIT 0(hz)
	"time2frq"		,	"0",		// minimo tempo para deteccao do segundo tom SIT 0(x10ms)
	"mxtime2frq"	,	"0",		// maximo tempo para deteccao do segundo tom SIT 0(x10ms)
	"lower3frq"		,	"0",		// frequencia baixa do terceiro tom de um SIT 0(hz)
	"upper3frq"		,	"0",		// frequencia alta do terceiro tom SIT 0(hz)
	"time3frq"		,	"0",		// minimo tempo para deteccao do terceiro tom SIT 0(x10ms)
	"mxtime3frq"	,	"0",		// maximo tempo para deteccao do terceiro tom SIT 0(x10ms)
	"dtn_pres"		,	"100",	// tempo do tom de discagem 100(x10ms)
	"dtn_npres"		,	"300",	// atraso para acusar falha no tom de discagem 300(x10ms)
	"dtn_deboff"	,	"10",		// atraso para tom de discar em sentido contrario 10(x10ms)
	"noanswer"		,	"3000",	// maximo tempo de nao responde ao discar 3000(x10ms)
	"maxintering"	,	"800",	// maximo atraso interrings antes de um concect 800(x10ms)

	NULL, NULL
};

static void ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, TONE_DEF &ToneDef);
static void ReadTones(char *Arq, char *sAppName, char *sKey, TONE_DEF &ToneDef);
static int ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, SINGLE_TONE_DEF &ToneDef);
static int ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, DOUBLE_TONE_DEF &ToneDef);




GC_START_STRUCT     Dialogic::startp;
GC_CCLIB_STATUS		Dialogic::cclib_status;
CRITICAL_SECTION	Dialogic::CritSecR2Tones;
CRITICAL_SECTION	Dialogic::CritMsiZipTone;

int					Dialogic::OFFSET_VOX                = 1;
int					Dialogic::OFFSET_DTI                = 1;
int					Dialogic::OFFSET_IPT                = 0;
bool				Dialogic::FlagEndObject             = false;
bool				Dialogic::FlagLastOpenChannel_E1	= false;
bool				Dialogic::FlagInitDialogic          = false;
bool				Dialogic::FlagFirstAnalogPort       = false;
bool				Dialogic::FlagIpTelephonyInitiate   = false;
DWORD				Dialogic::dwThreadIdGlobalCallStart = 0;


/********************************************************************************************
	NOME:	Dialogic

	DESCRICAO:	Funcao construtora da classe Dialogic sobrecarregada para ser ter acesso
							as funcoes da classe Dialogic, porem abrir nenhum canal

	PARAMETROS:

	RETORNO:
********************************************************************************************/
//Dialogic::Dialogic(const std::string & p_sBaseLogPath)  : m_sBaseLogPath(p_sBaseLogPath)
Dialogic::Dialogic(boost::shared_ptr<Logger>  pLog4cpp ) : m_pLog4cpp(pLog4cpp)
{    
    m_pLog4cpp->debug("[%p][%s](log4cpp::Category * pLog4cpp )", this, __FUNCTION__);    
    std::string sFullLogPathName;


	Canal						= 0;			// Esse objeto nao esta associado a nenhum canal
	BufLastError[0] = 0;			// Guarda a string que descreve o ultimo erro ocorrido
	//pUDPStream			= NULL;
	JumpBuffer			= NULL;		// Variavel de jump para comunicacao com a aplicacao

    TimeslotLine = 0;
    TimeslotLineIP = 0;
    TimeslotVox = 0;

#ifdef _ASR
	ASR							= NULL;
	bAsrEnabled = false;
	ASR_GrammarPackage		= "";
	ASR_DatabaseType			= "";
	ASR_DatabaseServer		= "";
	ASR_DatabaseName			= "";
	ASR_DatabaseUser			= "";
	ASR_DatabasePassword	= "";
#endif // _ASR
}


Dialogic::~Dialogic()
{
    /*
	if (pUDPStream != NULL)
	{
		delete pUDPStream;
	}
    */
}


/********************************************************************************************
	NOME:	Dialogic

	DESCRICAO:	Funcao construtora da classe Dialogic

	PARAMETROS:	int Channel - Canal

	RETORNO:
********************************************************************************************/
Dialogic::Dialogic(boost::shared_ptr<Logger>  pLog4cpp , TAPI_PARAM *pTapiParam) : m_pLog4cpp(pLog4cpp)
{
	char path_dir[_MAX_PATH+1];    
    std::string sFullLogPathName;

    TimeslotLine = 0;
    TimeslotLineIP = 0;
    TimeslotVox = 0;

    m_pLog4cpp->debug("[%p][%s](log4cpp::Category * pLog4cpp , TAPI_PARAM *pTapiParam)", this, __FUNCTION__);    

	try {

		// Guarda o canal de referencia
		Canal		= pTapiParam->Channel;
		Site		= pTapiParam->Site;
		Machine = pTapiParam->Machine;

		// Guarda os dados referente a tela para posterior colocacao dos estados dos bits
		iListIndex		= pTapiParam->iListIndex;
		iBoardTrunk	  = pTapiParam->iBoardTrunk;
		iBoardPort		= pTapiParam->iBoardPort;

		// Guarda o tamanho minimo do Dnis para protocolo R2 Digital
		R2DnisMinimumSize = pTapiParam->R2DnisMinimumSize;

		if( R2DnisMinimumSize < 3 || R2DnisMinimumSize > 8 ){
			R2DnisMinimumSize = 4;
		}
		
		// Guarda o ponteiro para o arquivo de log
		//pLogInterface = pTapiParam->pLogInterface;

		// Guarda o nivel de log para CTI
		iCtiLogLevel = pTapiParam->iCtiLogLevel;

		// Guarda o ponteiro para o buffer de log
		//pLogBuffer = pLogInterface->GetBuffer();

		// Instancia objeto de UDP stream para TTS Server
		//pUDPStream = new UDPStreamClass (pTapiParam->bsTtsServerHost.c_str(), Canal, pTapiParam->pLogInterface);

		// Inicia o sistema
		if(!FlagInitDialogic){
			int mode;

			// Inicializa placas MSI
			MsiInitBoard();

			// Seta modo de operacao da lib Dialogic
			mode = SR_STASYNC | SR_POLLMODE;
			
			if(sr_setparm(SRL_DEVICE, SR_MODELTYPE, &mode)){
				PutError("sr_setparm(SRL_DEVICE, SR_MODELTYPE, SR_STASYNC|SR_POLLMODE) Error in function InitSystem()");
			}

			InitializeCriticalSection(&CritSecR2Tones);
			InitializeCriticalSection(&CritMsiZipTone);

			FlagInitDialogic	= true;
			FlagEndObject			= false;
		}


		IDTone = 1;				// seta o numero inicial para ID's de tons a serem criados

		TimeToWaitEvent			= 1000;							// Tempo para esperar eventos de telefonia, default (1000 ms)
		PcmEnconding				= ALAW;							// Tipo de PCMEncondig para arquivo PCM, ULAW ou ALAW
		TypePlayFormat			= PCM_G711_ALAW;		// Formato de play setado
		TypeRecordFormat		= PCM_G711_ALAW;		// Formato de record setado
		DigMask							= DM_ALL;						// Mascara de digitos para interromper Play e Record

		PlayFileFormat			=	FILE_FORMAT_VOX;				// Formato do arquivo para Play
		PlayDataFormat			= DATA_FORMAT_G711_ALAW;	// Formato de dados para Play
		PlaySamples					= DRT_8KHZ;								// Numero de amostras para Play
		PlayBitsPerSample		= 8;											// Bits por amostra para Play

		RecordFileFormat		=	FILE_FORMAT_VOX;				// Formato do arquivo para Record
		RecordDataFormat		= DATA_FORMAT_G711_ALAW;	// Formato de dados para Record
		RecordSamples				= DRT_8KHZ;								// Numero de amostras para Record
		RecordBitsPerSample	= 8;											// Bits por amostra para Record

		JumpBuffer					= NULL;							// Variavel de jump para comunicacao com a aplicacao

        std::string strPidPath = GetExePath();
        strcpy(path_dir , strPidPath.c_str() );

		// Guarda o caminho para o diretorio corrente
		//_getdcwd( _getdrive(), path_dir, _MAX_PATH );

		// Seta o caminho para o nome de arquivo de parametros analogicos
		sprintf(NomeArquivoPar, "%s\\PAR.INI", path_dir);

		UserTimerSet	= 0;	// Desabilita UserTimer
		UserTimerInit	= 0;

		// Indica que ocorreu uma requisicao de bloqueio do canal
		FlagUserBlock		= false;
		// Indica se o canal IP ja foi inicializado para atender ligacoes		
		FlagIPChannelInit	= false;
		// Indica se o canal ja foi inicializado pronto para atender ligacoes		
		FlagChannelInit	= false;
		// Seta um flag indicando que os tons para frente ja foram criados
		R2ForwardTones	= false;
		// Seta um flag indicando que os tons para tras estao deletados
		R2BackwardTones	= false;
		// Flag indicando se sera necessario pegar mais numero de B
		R2_AckCall			= false;
		// Numero minimo de DNIS a ser recebido em R2 antes de voltar T_CALL		
		DnisMinimumSize	= 4;
		// Dnis variavel setado para false
		DnisVariavel		= false;
		// Seta a Causa default da desconexao quando o sistema desliga a ligacao		
		CauseDropCall	= DROP_NORMAL;
		// Inicializa o status do canal como desabilitado
		ChannelStatus = CHANNEL_DISABLED;

		// Inicializa variavel c/ o ANI
		Ani[0]					= 0;
		// Inicializa variavel c/ o DNIS
		Dnis[0]					= 0;
		// Inicializa variavel c/ o ultimo erro ocorrido
		BufLastError[0] = 0;

		memset( &dx_iott, 0, sizeof(dx_iott) );
		// Inicializa o tamanho do buffer de play c/ 64 Kb (tamanho default).
		PlayBufferSize = 65535;

        m_pLog4cpp->debug("[%p][%s] crn = 0", this, __FUNCTION__ );
		crn = 0;

		MessageBoxMelody = 0;
		ipdev = -1;
		dtidev = -1;
		Dti = -1;
		FlagCallIP = false;
		iDirection = CH_INBOUND;

#ifdef _ASR
		ASR										= NULL;
		bAsrEnabled						= pTapiParam->bAsrEnabled;
		ASR_GrammarPackage		= pTapiParam->ASR_GrammarPackage;
		ASR_DatabaseType			= pTapiParam->ASR_DatabaseType;
		ASR_DatabaseServer		= pTapiParam->ASR_DatabaseServer;
		ASR_DatabaseName			= pTapiParam->ASR_DatabaseName;
		ASR_DatabaseUser			= pTapiParam->ASR_DatabaseUser;
		ASR_DatabasePassword	= pTapiParam->ASR_DatabasePassword;
		ASR_PollingTime				= pTapiParam->ASR_PollingTime;
#endif // _ASR

	}
	catch(...)
	{
		PutError("Exception Dialogic()");
	}
}


/********************************************************************************************
	NOME:	InitInitSystem

	DESCRICAO:	Funcao de inicializacao da Dialogic

	PARAMETROS: 

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
void Dialogic::InitSystem(void)
{
    std::stringstream ss;
    m_pLog4cpp->debug("[%p][%s] Canal %d", this, __FUNCTION__, Canal);

	try {
#ifdef _HMP
		if( !dwThreadIdGlobalCallStart )
		{
			dwThreadIdGlobalCallStart = ::GetCurrentThreadId();
			/* initialize start parameters */
			IPCCLIB_START_DATA cclibStartData;
			memset(&cclibStartData,0,sizeof(IPCCLIB_START_DATA)); 
    
			IP_VIRTBOARD virtBoards[1];
			memset(virtBoards,0,sizeof(IP_VIRTBOARD));

			INIT_IPCCLIB_START_DATA(&cclibStartData,1,virtBoards);
			INIT_IP_VIRTBOARD(&virtBoards[0]);

//			cclibStartData.version     = 0x0100;   // must be set to 0x0100
//			cclibStartData.delimiter   = ',';
//			cclibStartData.num_boards  = 1;
//			cclibStartData.board_list  = virtBoards;
    
//			virtBoards[0].version = 0x0100; 
//			virtBoards[0].total_max_calls = IP_CFG_DEFAULT;
//			virtBoards[0].h323_max_calls = IP_CFG_DEFAULT;
//			virtBoards[0].sip_max_calls = IP_CFG_DEFAULT;
//			virtBoards[0].localIP.ip_ver = IPVER4;    // must be set to IPVER4
//			virtBoards[0].localIP.u_ipaddr.ipv4 = IP_CFG_DEFAULT;
//			virtBoards[0].h323_signaling_port = IP_CFG_DEFAULT;
//			virtBoards[0].sip_signaling_port = IP_CFG_DEFAULT;
//			virtBoards[0].reserved = NULL;            // must be set to NULL

			virtBoards[0].sup_serv_mask = IP_SUP_SERV_CALL_XFER;		// enable Call Transfer

			CCLIB_START_STRUCT cclibStartStruct[] = {	
					{"GC_PDKRT_LIB",NULL},
					{"GC_IPM_LIB",NULL}	,
					{"GC_H3R_LIB",&cclibStartData},
					{"GC_SS7_LIB",NULL},
					{"GC_DM3CC_LIB",NULL},
					{"GC_ISDN_LIB",NULL}, 
					{"GC_ICAPI_LIB",NULL},
					{"GC_ANAPI_LIB",NULL}
			};

			GC_START_STRUCT gcStartStruct;
			gcStartStruct.num_cclibs = 8;
			gcStartStruct.cclib_list = cclibStartStruct;
			gc_Start(&gcStartStruct);
		}
#else
		if( !dwThreadIdGlobalCallStart )
		{
			dwThreadIdGlobalCallStart = ::GetCurrentThreadId();
			CCLIB_START_STRUCT cclibStartStruct[] = {	
					{"GC_PDKRT_LIB",NULL},
					{"GC_IPM_LIB",NULL}	,
					{"GC_SS7_LIB",NULL},
					{"GC_DM3CC_LIB",NULL},
					{"GC_ISDN_LIB",NULL}, 
					{"GC_ICAPI_LIB",NULL},
					{"GC_ANAPI_LIB",NULL}
			};

            GC_INFO         gc_error_info;   /* GlobalCall error information data */ 
			GC_START_STRUCT gcStartStruct;
			gcStartStruct.num_cclibs = 7;
			gcStartStruct.cclib_list = cclibStartStruct;
			//gc_Start(&gcStartStruct);            
			//gc_Start(NULL);

            if ( gc_Start( &gcStartStruct ) != GC_SUCCESS )    
            {
                ss << "\r\n";
                ss << "*********************************************************\r\n";
                ss << "              ERROR AT GLOBAL CALL START                \r\n";
                ss << "*********************************************************\r\n";

                m_pLog4cpp->debug("[%p][%s] Canal %d  %s", this, __FUNCTION__, Canal, ss.str().c_str() );

                ss.str(std::string());

                /* process error return as shown */
                gc_ErrorInfo( &gc_error_info );
                ss << boost::format("Error: gc_Start(), GC ErrorValue: 0x%hx - %s, CCLibID: %i - %s, CC ErrorValue: 0x%lx - %s\n" ) %
                    gc_error_info.gcValue % gc_error_info.gcMsg %
                    gc_error_info.ccLibId % gc_error_info.ccLibName %
                    gc_error_info.ccValue % gc_error_info.ccMsg;

                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, ss.str().c_str() );                
            } else {
                ss << "\r\n";
                ss << "*********************************************************\r\n";
                ss << "           GLOBAL CALL STARTED SUCCESSFULLY              \r\n";
                ss << "*********************************************************\r\n";

                m_pLog4cpp->debug("[%p][%s] Canal %d  %s", this, __FUNCTION__, Canal, ss.str().c_str() );
            }
		}
#endif // _HMP
	}
	catch(...)
	{
		//PutError("Exception InitSystem()");
	}
}

/********************************************************************************************
	NOME:	EndSystem

	DESCRICAO:	Funcao de encerramento da Dialogic

	PARAMETROS: 

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
void Dialogic::EndSystem(void)
{
	try {

		if( dwThreadIdGlobalCallStart == ::GetCurrentThreadId() )
		{
			// Encerra o GlobalCall somente 1 vez pela thread q criou (a thread do canal 1)
			gc_Stop();
			dwThreadIdGlobalCallStart = 0;
		}

	}
	catch(...)
	{
		//PutError("Exception InitSystem()");
	}
}

/********************************************************************************************
	NOME:	FakeOpenChannel

	DESCRICAO:	Abre um canal de voz falsamente, eh so para incrementar o contador de recursos
							de voz, por que caso algum canal esteja desabilitado pela aplicacao,
							os proximos abriram corretamente

	PARAMETROS:	int Channel				- Canal a ser falsamente aberto
							int InterfaceType	-	Tipo de interface associada ao canal de voz
							int Signaling			- Sinalizacao (default GLOBAL_CALL)

	RETORNO:
********************************************************************************************/
void Dialogic::FakeOpenChannel(int Channel, int InterfaceType, int Signaling)
{
	try {
        m_pLog4cpp->debug("[%s]",__FUNCTION__);

		int iOffSetResult = 0;

		switch( InterfaceType ){
			
			case E1:	// Interface de linha digital E1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
			case IP:
				
				OFFSET_DTI++;
				iOffSetResult = (OFFSET_VOX / 4);
				iOffSetResult++;

				if( Signaling == GLOBAL_CALL_DM3 ){
					// Em placas DM3 os canais de voz sao continuos				
					OFFSET_VOX++;
					FlagLastOpenChannel_E1 = true;
					break;
				}

				// Ultimo DSP do E1 e device invalido.
				if( ( (iOffSetResult % 8) == 0 ) && ( ( OFFSET_VOX % 4 ) == 3 ) )
				{
					OFFSET_VOX += 2;
				}
				else
				{
					if( ( Channel % 30 ) == 0 )
					{
						OffSetVoxIncrement( E1, Signaling );
					}
					else
					{
						OFFSET_VOX++;
					}
				}

				FlagLastOpenChannel_E1 = true;
			break;

			case T1:	// Interface de linha digital T1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
				
				OFFSET_DTI++;
				// Nao implementado.
				if(Channel == 24){ // canal de dados
					return;
				}

				OffSetVoxIncrement( T1, Signaling );
			break;
			
			case LSI:	// Interface de linha analogica (canal de voz associado)
			case VOX:	// Interface de recurso de voz sem interface de linha associado
			case FAX:	// Interface de recurso de fax sem interface de linha associado
				OffSetVoxIncrement( InterfaceType, Signaling );
			break;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::FakeOpenChannel()");
	}
}


void Dialogic::OffSetVoxIncrement(int InterfaceType, int Signaling)
{
	try {
		m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

		int iOffSetResult = 0;

        if ( 
                (InterfaceType == E1) ||    // Interface de linha digital E1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
                (InterfaceType == IP) )     // Interface de linha digital E1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
        {
            if( Signaling == GLOBAL_CALL_DM3 ){
				// Em placas DM3 os canais de voz sao continuos				
				OFFSET_VOX++;
                return;
			}

			iOffSetResult = (OFFSET_VOX / 4);
			iOffSetResult++;
				
			// Ultimo DSP do E1 e device invalido.
			if( ( (iOffSetResult % 8) == 0 ) && ( ( OFFSET_VOX % 4 ) == 2 ) )
			{
				OFFSET_VOX += 3;
			}
			else
			{
				OFFSET_VOX++;
			}
        } else if ( 
            (InterfaceType == T1) ||    // Interface de linha digital T1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
            (InterfaceType == LSI) ||   // Interface de linha analogica (canal de voz associado)
            (InterfaceType == VOX) ||   // Interface de recurso de voz sem interface de linha associado
            (InterfaceType == FAX))     // Interface de recurso de fax sem interface de linha associado
        {
            OFFSET_VOX++;
        }
	}
	catch(...)
	{
		PutError("Exception Dialogic::OffSetVoxIncrement()");
	}
}

/********************************************************************************************
	NOME:	OpenChannel

	DESCRICAO:	Abre o canal de telefonia do objeto

	PARAMETROS:	int		Interface					- Tipo de Interface LSI, E1, T1, MSI, VOX ou FAX
							int		Sinalizacao				-	Tipo de Sinalizacao, R2, GLOBAL_CALL, ISDN, LOOP_START
																				parametro default = 0
							char	*Protocolo				- String que define o protocolo para GlobalCall
	RETORNO:
********************************************************************************************/
int Dialogic::OpenChannel(int Interface, int Sinalizacao, const char *Protocolo)
{
    m_pLog4cpp->debug("[%p][%s] Canal %d", this, __FUNCTION__, Canal);    

	try {
		int rc = -1;

		if(FlagInitDialogic == false)
			return -1;

		TipoInterface = Interface;
		TipoSinalizacao = Sinalizacao;
		TipoProtocolo = Protocolo;

		// Seta o status do canal como bloqueado
		ChannelStatus = CHANNEL_BLOCKED;

        if ( Interface == E1 )  // Interface de linha digital E1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
        {
            rc = OpenChannelE1(Sinalizacao, Protocolo);            
        } 
        else if ( Interface == IP ) // Interface IP, (canal de voz associado)
        {
            rc = OpenChannelIP(true, Protocolo); 
        } 
        else if ( Interface == T1 ) // Interface de linha digital T1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
        {
            rc = OpenChannelT1(Sinalizacao, Protocolo);
        }
        else if ( Interface == LSI )    // Interface de linha analogica (canal de voz associado)
        {
            rc = OpenChannelLSI( Sinalizacao );
        }
        else if ( Interface == VOX )    // Interface de recurso de voz sem interface de linha associado
        {
            rc = OpenChannelVOX();
        }
        else if ( Interface == FAX )    // Interface de recurso de fax sem interface de linha associado
        {
            rc = OpenChannelFAX();
        }

        m_pLog4cpp->debug("[%p][%s] Line TX ts: %ld, Vox TX ts: %ld", this, __FUNCTION__, TimeslotLine, TimeslotVox );
        
		return rc;
	}
	catch(...)
	{
		PutError("Exception OpenChannel()");
		return -1;
	}
	return -1;
}


/********************************************************************************************
	NOME:	OpenChannelE1

	DESCRICAO:	Abre canal digital em conexao E1 e seta parametros de acordo com a sinalizacao

	PARAMETROS:	int		Sinalizacao				-	Tipo de Sinalizacao, R2, GLOBAL_CALL, ISDN
							char	*Protocolo				- String que define o Protocolo para GlobalCall

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelE1(int Sinalizacao, const char *Protocolo = "br_r2_io")
{
    int placa, canal_dti;
	long tslot;
	char NomeDevice[256] = "";
	SC_TSINFO sc_tsinfo;
    int iRet = 0;

	try {
        m_pLog4cpp->debug("[%p][%s] Canal ", this, __FUNCTION__, Canal);

        m_pLog4cpp->debug("[%p][%s] Sinalizacao = %d", this, __FUNCTION__ , Sinalizacao);
        m_pLog4cpp->debug("[%p][%s] Protocolo = %s", this, __FUNCTION__ , Protocolo);

		canal_dti = OFFSET_DTI % 30 ? OFFSET_DTI % 30 : 30;	// Interface Digital
			
		// A interface E1 tem 30 canais, entao determina em qual placa esta o canal
		if( (OFFSET_DTI % 30) == 0 )
			placa = ((OFFSET_DTI - 1) / 30) + 1;
		else
			placa = (OFFSET_DTI / 30) + 1;



        if ( Sinalizacao == LINE_SIDE )
        {
            // abre canal digital
			::sprintf(NomeDevice, "dtiB%dT%d", placa, canal_dti);
                                
			Dti = dt_open(NomeDevice, 0);
			if(Dti == -1){
				PutError("dt_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Dti));
				return -1;
			}
            m_pLog4cpp->debug("[%p][%s] dt_open(%s) OK", this , __FUNCTION__, NomeDevice);

			// seta parametros para o canal digital para receber eventos de transicao de bits
			if(dt_setevtmsk(Dti, DTG_SIGEVT, R2_BITMASK, DTA_SETMSK)){
				PutError("dt_setevtmsk() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}
			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
				PutError("dt_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}

			TimeslotLine = tslot;

			sprintf(NomeDevice,"dxxxB%dC%d", (OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
																				(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);

			//CtiLog( "Voice Resource: %s", NomeDevice );
            m_pLog4cpp->debug("[%p][%s] Voice Resource : %s", this, __FUNCTION__ , NomeDevice );

			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				//PutError("dx_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Vox));
                m_pLog4cpp->debug("[%p][%s] dx_open() Error - Channel %d Device %s: %s", this, __FUNCTION__ , Canal, NomeDevice, ATDV_ERRMSGP(Vox));
				return -1;
			}
            m_pLog4cpp->debug("[%p][%s] dx_open(%s) OK", this , __FUNCTION__, NomeDevice);

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
                m_pLog4cpp->debug("[%p][%s] dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", this , __FUNCTION__ , Canal, ATDV_ERRMSGP(Vox) );
				return -1;
			}

			TimeslotVox = tslot;

#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
#endif // _ASR

			if(nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP)){ // conecta os canais
				PutError("nr_scroute() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}					
			if(dx_deltones(Vox)){
				PutError("dx_deltones() error in function dx_deltones() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
			// Seta parametros de hangup.
			if(SetHANGUP()){
				return -1;
			}
				
            /*
			if(Switch::StRegister.DetectMessageBox)
			{
				// Prepara p/ detectar melodia da caixa postal de celular.
				if(SetMESSAGEBOX()){
					return -1;
				}
			}
            */

			// limpa digitos
			dx_clrdigbuf(Vox);
			// Coloca o canal em estado de bloqueio
			CasSetBit(AON);
			CasSetBit(BON);

			OffSetVoxIncrement( E1, TipoSinalizacao );
			OFFSET_DTI++;

			FlagLastOpenChannel_E1 = true;
			return 0;
        } 
        else if ( (Sinalizacao == R2) || (Sinalizacao == R2_DIGITAL) )
        {
            // abre canal digital
			::sprintf(NomeDevice, "dtiB%dT%d", placa, canal_dti);

			Dti = dt_open(NomeDevice, 0);
			if(Dti == -1){
				PutError("dt_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Dti));
				return -1;
			}
			// seta parametros para o canal digital para receber eventos de transicao de bits
			if(dt_setevtmsk(Dti, DTG_SIGEVT, R2_BITMASK, DTA_SETMSK)){
				PutError("dt_setevtmsk() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}
			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
				PutError("dt_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}

			TimeslotLine = tslot;

			::sprintf(NomeDevice,"dxxxB%dC%d", (OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
																				(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);

			CtiLog( "Voice Resource: %s", NomeDevice );

			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				PutError("dx_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Vox));
				return -1;
			}

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;

#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
#endif // _ASR

			if(nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP)){ // conecta os canais
				PutError("nr_scroute() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}					
			// limpa digitos
			dx_clrdigbuf(Vox);

			// limpa tons
			if(dx_deltones(Vox)){
				PutError("dx_deltones() error in function dx_deltones() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

            /*
			if(Switch::StRegister.DetectMessageBox)
			{
				// Prepara p/ detectar melodia da caixa postal de celular.
				if(SetMESSAGEBOX()){
					return -1;
				}
			}
            */

			// Coloca o canal em estado de bloqueio
			CasSetBit(AON);
			CasSetBit(BON);

			OffSetVoxIncrement( E1, TipoSinalizacao );
			OFFSET_DTI++;

			FlagLastOpenChannel_E1 = true;

			// Coloca o status dos bits na tela
			PrintCasBits();
			return 0;
        }
        else if (   (Sinalizacao == ISDN) || (Sinalizacao == ISDN_4ESS) || (Sinalizacao == ISDN_NTT) || 
                    (Sinalizacao == ISDN_QSIG) || (Sinalizacao == ISDN_CTR4) || (Sinalizacao == ISDN_DMS) )
        {
            // abre canal digital
            memset(NomeDevice, 0x00, sizeof(NomeDevice ) );
			sprintf(NomeDevice, "dtiB%dT%d", placa, canal_dti);

            m_pLog4cpp->debug("[%p][%s] NomeDevice : %s", this, __FUNCTION__ , NomeDevice);

			if(cc_Open((long *)&Dti, NomeDevice, 0) == -1){
				PutError("cc_Open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Dti));
                m_pLog4cpp->debug("[%p][%s] cc_Open() Error - Channel %d Device %s: %s", this, __FUNCTION__ , Canal, NomeDevice, ATDV_ERRMSGP(Dti));

				return -1;
			}
			if(cc_SetEvtMsk(Dti, CCMSK_ALERT | CCMSK_PROGRESS, CCACT_ADDMSK)){
				PutError("cc_SetEvtMsk() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
                m_pLog4cpp->debug("[%p][%s] cc_SetEvtMsk() error in function OpenChannelE1() Channel %d: %s", this, __FUNCTION__ , Canal , ATDV_ERRMSGP(Dti));
				return -1;
			}

			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
				PutError("dt_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
                m_pLog4cpp->debug("[%p][%s] dt_getxmitslot() error in function OpenChannelE1() Channel %d: %s", this, __FUNCTION__ , Canal , ATDV_ERRMSGP(Dti));
				return -1;
			}

			TimeslotLine = tslot;

			// abre canal de voz associado ao canal digital
			// tenta abrir o canal tres vezes incrementando o offset
            memset(NomeDevice, 0x00, sizeof(NomeDevice ) );
			sprintf(NomeDevice,"dxxxB%dC%d", (OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
																				(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
			//CtiLog( "Voice Resource: %s", NomeDevice );
            m_pLog4cpp->debug("[%p][%s] Voice Resource : %s", this, __FUNCTION__ , NomeDevice );

			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				//PutError("dx_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Vox));
                m_pLog4cpp->debug("[%p][%s] dx_open() Error - Channel %d Device %s: %s", this, __FUNCTION__ , Canal, NomeDevice, ATDV_ERRMSGP(Vox));
				return -1;
			}

            m_pLog4cpp->debug("[%p][%s] dx_open(%s) OK - Channel %d ", this , __FUNCTION__, NomeDevice, Canal);

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				//PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
                m_pLog4cpp->debug("[%p][%s] dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", this, __FUNCTION__ , Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;

#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
#endif //_ASR

			if(nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP)){ // conecta os canais
				PutError("nr_scroute() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}					
			// limpa digitos
			dx_clrdigbuf(Vox);

			// limpa tons
			if(dx_deltones(Vox)){
				PutError("dx_deltones() error in function dx_deltones() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

            /*
			if(Switch::StRegister.DetectMessageBox)
			{
				// Prepara p/ detectar melodia da caixa postal de celular.
				if(SetMESSAGEBOX()){
					return -1;
				}
			}
            */

            m_pLog4cpp->debug("[%p][%s] Going to call OffSetVoxIncrement()", this, __FUNCTION__);
			OffSetVoxIncrement( E1, TipoSinalizacao );
			OFFSET_DTI++;

			FlagLastOpenChannel_E1 = true;
			return 0;
        }
        else if (   (Sinalizacao == GLOBAL_CALL) || (Sinalizacao == GLOBAL_CALL_SS7) || (Sinalizacao == GLOBAL_CALL_DM3) )
        {
            if ( Sinalizacao == GLOBAL_CALL_SS7 )
            {
                if( stricmp(Protocolo, "SS7_dk") == 0 )
				{
					// Abre o canal para trabalhar com a placa PCCS6 SS7 (DataKinetics)
					sprintf(NomeDevice, ":N_dkB%dT%d:P_SS7:V_dxxxB%dC%d",
															placa,
															canal_dti,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
				else
				{
					// Abre o canal para trabalhar com a placa dti em SS7
					sprintf(NomeDevice, ":N_dtiB%dT%d:P_SS7:V_dxxxB%dC%d",
															placa,
															canal_dti,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
            }
            else if ( Sinalizacao == GLOBAL_CALL_DM3 )
            {
                if( stricmp(Protocolo, "SS7_dk") == 0 )
				{
					// Abre o canal para trabalhar com a placa PCCS6 SS7 (DataKinetics)
					sprintf(NomeDevice, ":L_SS7:N_dkB%dT%d:P_SS7:V_dxxxB%dC%d",
															placa,
															canal_dti,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
				else if( stricmp(Protocolo, "SS7_dti") == 0 )
				{
					// Abre o canal para trabalhar com a placa dti em SS7
					sprintf(NomeDevice, ":L_SS7:N_dtiB%dT%d:P_SS7:V_dxxxB%dC%d",
															placa,
															canal_dti,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
				else
				{
					// Abre o canal para trabalhar com a placa dti em ISDN e PDK's
					sprintf(NomeDevice, ":N_dtiB%dT%d:P_%s:V_dxxxB%dC%d",
															placa,
															canal_dti,
															Protocolo,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
            } 
            else 
            {
                if( stricmp(Protocolo, "isdn") == 0 )
				{
					sprintf(NomeDevice, ":N_dtiB%dT%d:P_%s",
															placa,
															canal_dti,
															Protocolo);
				}
				else
				{
					// Abre o canal para trabalhar com outros protocolos
					sprintf(NomeDevice, ":N_dtiB%dT%d:P_%s:V_dxxxB%dC%d",
															placa,
															canal_dti,
															Protocolo,
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);
				}
            }

            //CtiLog( "LineDevice: %s", NomeDevice );
            m_pLog4cpp->debug("[%p][Dialogic::OpenChannelE1] LineDevice :  %s.", this, NomeDevice );
            m_pLog4cpp->debug("[%p][Dialogic::OpenChannelE1] Protocolo :  %s.", this, Protocolo );

			if( stricmp( Protocolo, "isdn" ) == 0 )
			{
				if(Sinalizacao == (int)GLOBAL_CALL_DM3)
				{
					if(gc_OpenEx(&dtidev, NomeDevice, 0, NULL) != GC_SUCCESS) {
						PutError("gc_OpenEx() Error - Channel %d Device %s: %s", Canal, NomeDevice, gc_GetStringError());
                        m_pLog4cpp->debug("[%p][%s] gc_OpenEx() Error - Channel %d Device %s: %s", this, __FUNCTION__,
                            Canal, NomeDevice, gc_GetStringError());
						return -1;
					}

					if(gc_GetNetworkH(dtidev, &Dti) != GC_SUCCESS) {
						PutError("gc_GetNetworkH() error in function OpenChannelE1() Channel %d: %s", Canal, gc_GetStringError());
                        m_pLog4cpp->debug("[%p][%s] gc_GetNetworkH() error in function OpenChannelE1() Channel %d: %s", this, __FUNCTION__,
                            Canal, gc_GetStringError());
						return -1;
					}					
                        
					// Obtem o recurso de voz associado
					if(gc_GetVoiceH(dtidev, &Vox) != GC_SUCCESS) {
						// Nao possui recurso de voz associado
						Vox	= 0;
					}

                    m_pLog4cpp->debug("[%p][%s] Resource: %s OPENNED.", this, __FUNCTION__, NomeDevice );
				}
				else
				{
					if(gc_OpenEx(&dtidev, NomeDevice, 0, NULL) != GC_SUCCESS) {
						PutError("gc_OpenEx() Error - Channel %d Device %s: %s", Canal, NomeDevice, gc_GetStringError());
                        m_pLog4cpp->debug("[%p][%s] gc_OpenEx() Error - Channel %d Device %s: %s", this, __FUNCTION__,
                            Canal, NomeDevice, gc_GetStringError());
						return -1;
					}
					if(gc_GetNetworkH(dtidev, &Dti) != GC_SUCCESS) {
						PutError("gc_GetNetworkH() error in function OpenChannelE1() Channel %d: %s", Canal, gc_GetStringError());
                        m_pLog4cpp->debug("[%p][%s] gc_GetNetworkH() error in function OpenChannelE1() Channel %d: %s", this, __FUNCTION__,
                            Canal, gc_GetStringError());
						return -1;
					}					

					// Abre o canal para trabalhar com a placa dti em SS7
					sprintf(NomeDevice, "dxxxB%dC%d",
															(OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
															(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);

						
					//CtiLog( "Voice Resource: %s", NomeDevice );
                    m_pLog4cpp->debug("[%p][Dialogic::OpenChannelE1] Voice Resource: %s.", this, NomeDevice );

					Vox = dx_open(NomeDevice, 0);
					if(Vox == -1){
						PutError("dx_open() Error - Channel %d Device %s: %s", Canal, NomeDevice, ATDV_ERRMSGP(Vox));
						return -1;
					}

                    m_pLog4cpp->debug("[%p][Dialogic::OpenChannelE1] Voice Resource: %s OPENNED.", this, NomeDevice );

                        
				}
			}
			else
			{

				if(gc_OpenEx(&dtidev, NomeDevice, 0, NULL) != GC_SUCCESS) {
					PutError("gc_OpenEx() Error - Channel %d Device %s: %s", Canal, NomeDevice, gc_GetStringError());
					CtiLog(GetLastError());
					return -1;
				}
				if(gc_GetNetworkH(dtidev, &Dti) != GC_SUCCESS) {
					PutError("gc_GetNetworkH() error in function OpenChannelE1() Channel %d: %s", Canal, gc_GetStringError());
					CtiLog(GetLastError());
					return -1;
				}					
				if(gc_GetVoiceH(dtidev, &Vox) != GC_SUCCESS) {
					PutError("gc_GetVoiceH() error in function OpenChannelE1() Channel %d: %s", Canal, gc_GetStringError());
					CtiLog(GetLastError());
					return -1;
				}

                m_pLog4cpp->debug("[%p][Dialogic::OpenChannelE1] Resource: %s OPENNED.", this, NomeDevice );
			}

			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if( ( Sinalizacao == (int)GLOBAL_CALL_SS7 ) || ( Sinalizacao == (int)GLOBAL_CALL_DM3 ) )
			{
				if(gc_GetXmitSlot(dtidev, &sc_tsinfo) == -1){
					PutError("gc_GetXmitSlot() error in function OpenChannelE1() Channel %d: %s", Canal, gc_GetStringError());
					CtiLog(GetLastError());
					return -1;
				}

				if( strstr(Protocolo, "SS7") != NULL )
				{
					if(gc_SetEvtMsk(dtidev, GCMSK_PROCEEDING | GCMSK_PROGRESS | GCMSK_DIALING, GCACT_ADDMSK) == -1 ){
						PutError("gc_SetEvtMsk() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
						CtiLog(GetLastError());
						return -1;
					}
				}

			}
			else
            {

				if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
					PutError("dt_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
					CtiLog(GetLastError());
					return -1;
				}
			}

			// limpa digitos
			//dx_clrdigbuf(Vox);
				
			TimeslotLine = tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				CtiLog(GetLastError());
				return -1;
			}

			TimeslotVox = tslot;

#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
#endif //_ASR

			OffSetVoxIncrement( E1, TipoSinalizacao );

			OFFSET_DTI++;

			if( stricmp( Protocolo, "isdn" ) == 0 )
			{
				// Quando e GLOBALCALL ISDN conecta o recurso de voz c/ o recurso de rede.
				nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP);
			}

			// limpa tons
			if(dx_deltones(Vox)){
				PutError("dx_deltones() error in function dx_deltones() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				CtiLog(GetLastError());
				return -1;
			}

            /*
			if(Switch::StRegister.DetectMessageBox)
			{
				// Prepara p/ detectar melodia da caixa postal de celular.
				if(SetMESSAGEBOX()){
					return -1;
				}
			}
            */

			FlagLastOpenChannel_E1 = true;
            return 0;

        }
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelE1()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenChannelT1

	DESCRICAO:	Abre canal digital em conexao T1 e seta parametros de acordo coma sinalizacao

	PARAMETROS:	int		Sinalizacao				-	Tipo de Sinalizacao, R2, GLOBAL_CALL, ISDN
							char	*Protocolo				- String que define o Protocolo para GlobalCall

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelT1(int Sinalizacao,  const char *Protocolo = "br_r2_io")
{
  int placa, canal_dti;
	long tslot;
	char NomeDevice[256];
	SC_TSINFO sc_tsinfo;
    int iRet = 0;

	try {

		if(Canal == 24) // canal de dados
			return 0;

		canal_dti = Canal % 24 ? Canal % 24 : 24;

		OFFSET_VOX = Canal;

		// A interface E1 tem 30 canais, entao determina em qual placa esta o canal
		if( (Canal % 24) == 0 )
			placa = ((Canal - 1) / 24) + 1;
		else
			placa = (Canal / 24) + 1;

        if (    (Sinalizacao == ISDN ) || (Sinalizacao == ISDN_4ESS ) || (Sinalizacao == ISDN_NTT ) || 
                (Sinalizacao == ISDN_QSIG ) || (Sinalizacao == ISDN_CTR4 ) || (Sinalizacao == ISDN_DMS )
                )
        {
            // abre canal digital
			sprintf(NomeDevice, "dtiB%dT%d", placa, canal_dti);

			if(cc_Open((long *)&Dti, NomeDevice, 0) == -1){
				PutError("Cannot open Dti Channel %d in function OpenChannelE1()", Canal);
				return -1;
			}
			if(Dti == -1){
				PutError("Cannot open Dti Channel %d in function OpenChannelT1()", Canal);
				return -1;
			}
			if(cc_SetEvtMsk(Dti, CCMSK_ALERT | CCMSK_PROGRESS, CCACT_ADDMSK)){
				PutError("cc_SetEvtMsk() error in function OpenChannelT1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}
			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
				PutError("dt_getxmitslot() error in function OpenChannelT1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}
			TimeslotLine = tslot;

			// abre canal de voz associado ao canal digital
			// tenta abrir o canal tres vezes incrementando o offset
			sprintf(NomeDevice,"dxxxB%dC%d", (OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
																				(OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);

			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				PutError("Cannot open Vox Channel %d in function OpenChannelE1()", Canal);
				return -1;
			}

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;

#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
#endif // _ASR

			if(nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP)){ // conecta os canais
				PutError("nr_scroute() error in function OpenChannelT1() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
				return -1;
			}					
			// limpa digitos
			dx_clrdigbuf(Vox);
			return 0;
        } else 
            return -1;
		
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelT1()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenChannelLSI

	DESCRICAO:	Abre canal analogico com canal de voz associado

	PARAMETROS:

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelLSI(int Sinalizacao)
{
  int i, NumRings;
	long tslot;
	char NomeDevice[256];
	SC_TSINFO sc_tsinfo;

	try {

/*
		if(FlagLastOpenChannel_E1){
			
			FlagLastOpenChannel_E1 = false;

			if( ((Canal / 30) > 0) && ((Canal % 30) == 1) ){
				// pula os canais de voz nao usados em ambientes digitais
				OFFSET_VOX += 2;
			}
			else{
				OFFSET_VOX++;
			}
		}
		else{
			if( OFFSET_VOX == 1 && FlagFirstAnalogPort == false){
				FlagFirstAnalogPort = true;
				OFFSET_VOX = 0;
			}

			OFFSET_VOX++;
		}
*/

		if(Sinalizacao == LOOP_START_TRAP)
        {

			i = 1;
			sprintf(NomeDevice,"dxxxB%dC%d",((OFFSET_VOX)%4)?((OFFSET_VOX)/4)+1:1+((OFFSET_VOX)/4)-1,
																			((OFFSET_VOX)%4)?((OFFSET_VOX)%4):4);
			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				PutError("Cannot open Vox Channel %d in function OpenChannelLSI() Device %s", Canal, NomeDevice);
				return -1;
			}
			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(ag_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("ag_getxmitslot() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
			
			TimeslotLine = tslot;

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;

			// conecta os canais
			if(nr_scroute(Vox, SC_LSI, Vox, SC_VOX, SC_HALFDUP)){
				PutError("nr_scroute() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}					
			// seta parametros analogicos
			if(SetDXBD()){
				PutError("SetDXBD() error");
				return -1;
			}
			// Deleta todos os tons 
			if(dx_deltones(Vox)){
				PutError("dx_deltones() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}								

			// habilita a deteccao de silencio de nao silencio, e loop de corrente
			if(dx_setevtmsk(Vox, DM_SILON|DM_SILOF|DM_LCOFF|DM_LCON)){
				PutError("dx_setevtmsk() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
			// SEMPRE DEVERA ESTAR NO GANCHO
			dx_sethook(Vox, DX_ONHOOK, EV_SYNC); 

			OffSetVoxIncrement( LSI, TipoSinalizacao );
			
			return 0;
		}
		else
        {
			
			i = 1;
			sprintf(NomeDevice,"dxxxB%dC%d",((OFFSET_VOX)%4)?((OFFSET_VOX)/4)+1:1+((OFFSET_VOX)/4)-1,
																			((OFFSET_VOX)%4)?((OFFSET_VOX)%4):4);
			Vox = dx_open(NomeDevice, 0);
			if(Vox == -1){
				PutError("Cannot open Vox Channel %d in function OpenChannelLSI() Device %s", Canal, NomeDevice);
				return -1;
			}

			// guarda o timeslot DTI de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(ag_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("ag_getxmitslot() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
			
			TimeslotLine = tslot;

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;

			// conecta os canais
			if(nr_scroute(Vox, SC_VOX, Vox, SC_LSI, SC_FULLDUP)){
				PutError("nr_scroute() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}					
			if(SetDXBD()){					// seta parametros analogicos
				PutError("SetDXBD() error");
				return -1;
			}
			if(SetDXCAP()){					// seta parametros analogicos
				PutError("SetDXCAP() error");
				return -1;
			}	
			if(SetCallProgress()){	// seta parametros de CallProgress
				PutError("SetCallProgress() error");
				return -1;
			}
			if(SetHANGUP()){				// seta parametros de hangup
				PutError("SetHANGUP() error");
				return -1;
			}

            /*
			if(Switch::StRegister.DetectMessageBox)
			{
				// Prepara p/ detectar melodia da caixa postal de celular.
				if(SetMESSAGEBOX()){
					return -1;
				}
			}
            */

			//if(SetFAX()){						// seta parametros de FAX
			//	return -1;
			//}
			dx_clrdigbuf(Vox);			// limpa digitos

			// coloca o canal fora do gancho para nao atender ligacoes ainda
			dx_sethook(Vox, DX_OFFHOOK, EV_SYNC);

			OffSetVoxIncrement( LSI, TipoSinalizacao );

	#ifdef _ASR
			if( ASRInitialize() < 0 )
			{
				return -1;
			}
	#endif // _ASR

			// habilita a deteccao de ring, loop de corrent off e digitos pela fila de eventos
			if(dx_setevtmsk(Vox, LSI_MASK)){
				PutError("dx_setevtmsk() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
			// seta o numero de rings
			NumRings = 1;
			if(dx_setparm(Vox, DXCH_RINGCNT, &NumRings)){
				PutError("dx_setparm() error in function OpenChannelLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			return 0;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelLSI()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenChannelVOX

	DESCRICAO:	Abre canal para recurso de voz auxiliar

	PARAMETROS:

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelVOX(void)
{
	long tslot;
	char NomeDevice[256];
	SC_TSINFO sc_tsinfo;
        
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {
	
		sprintf(NomeDevice,"dxxxB%dC%d",((OFFSET_VOX)%4)?((OFFSET_VOX)/4)+1:1+((OFFSET_VOX)/4)-1,
																		((OFFSET_VOX)%4)?((OFFSET_VOX)%4):4);
		Vox = dx_open(NomeDevice, 0);
		if(Vox == -1){
			PutError("Cannot open Vox Channel %d in function OpenChannelVOX()", Canal);
			return -1;
		}

        m_pLog4cpp->debug("[%s] dx_open(%s) OK",__FUNCTION__, NomeDevice);

		// guarda o timeslot DTI de transmissao para uso posterior
		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp = &tslot;

		if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
			PutError("dx_getxmitslot() error in function OpenChannelVOX() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
            m_pLog4cpp->debug("[%p][%s] dx_getxmitslot() error in function OpenChannelVOX() Channel %d: %s", this, __FUNCTION__, Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		TimeslotVox = TimeslotLine = tslot;

#ifdef _ASR
		if( ASRInitialize() < 0 )
		{
			return -1;
		}
#endif _ASR

		if(dx_deltones(Vox)){
			PutError("dx_deltones() error in function OpenChannelVOX() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
            m_pLog4cpp->debug("[%p][%s] dx_deltones() error in function OpenChannelVOX() Channel %d: %s", this, __FUNCTION__, Canal, ATDV_ERRMSGP(Vox) );
			return -1;
		}

		OffSetVoxIncrement( VOX, TipoSinalizacao );

		return 0;	
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelVOX()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenChannelFAX

	DESCRICAO:	Abre um canal de fax

	PARAMETROS:

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelFAX(void)
{
	long tslot;
	char NomeDevice[256];
	SC_TSINFO sc_tsinfo;

	try {

		sprintf(NomeDevice,"dxxxB%dC%d",((OFFSET_VOX)%4)?((OFFSET_VOX)/4)+1:1+((OFFSET_VOX)/4)-1,
																		((OFFSET_VOX)%4)?((OFFSET_VOX)%4):4);

		CtiLog( "Fax Device: %s", NomeDevice );
		Fax = fx_open(NomeDevice, 0);
		
		if(Fax == -1){
			PutError("Cannot open Fax Channel %d in function OpenChannelFAX()", Canal);
			return -1;
		}

		// guarda o timeslot DTI de transmissao para uso posterior
		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp = &tslot;

		if(fx_getxmitslot(Fax, &sc_tsinfo) == -1){
			PutError("fx_getxmitslot() error in function OpenChannelFAX() Channel %d: %s", Canal, ATDV_ERRMSGP(Fax));
			return -1;
		}
		
		TimeslotLine = tslot;

		OffSetVoxIncrement( FAX, TipoSinalizacao );

		return 0;	
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelFAX()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenChannelFAX

	DESCRICAO:	Abre um canal de fax

	PARAMETROS:	int &iFaxChannel		- Numero do canal de FAX
							long &lTxTimeSlot		- Referencia para retornar o timeslot de transmissao do FAX
							int &iDevice				- Referencia para retornar o device aberto do canal de FAX

	RETORNO:		0 sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelFAX(int &iFaxChannel, long &lTxTimeSlot, int &iDevice)
{
	long tslot;
	char NomeDevice[256];
	SC_TSINFO sc_tsinfo;

	try {

		sprintf(NomeDevice,"dxxxB%dC%d",((OFFSET_VOX)%4)?((OFFSET_VOX)/4)+1:1+((OFFSET_VOX)/4)-1,
																			((OFFSET_VOX)%4)?((OFFSET_VOX)%4):4);

		CtiLog( "Fax Device: %s", NomeDevice );

		iDevice = fx_open(NomeDevice, NULL);
			
		if(iDevice == -1){
			PutError("Cannot open Fax Channel: %d device: %s in function OpenChannelFAX()", iFaxChannel, NomeDevice);
			return -1;
		}

		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp = &tslot;
	
		if(fx_getxmitslot(iDevice, &sc_tsinfo) == -1){
			PutError("fx_getxmitslot() error in function OpenChannelFAX() Channel %d: %s", iFaxChannel, ATDV_ERRMSGP(iDevice));
			return -1;
		}
		
		lTxTimeSlot = tslot;

		OffSetVoxIncrement( FAX, TipoSinalizacao );

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelFAX(int Channel)");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OpenStation

	DESCRICAO:	Abre canal para posicao de atendimento sem canal de voz associado

	PARAMETROS:

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenStation(int IdStation, int BoardNumber)
{
	long tslot;
	SC_TSINFO sc_tsinfo;
	char Name[100];

	try {

		TipoInterface = MSI;

		GetMsiInfo(BoardNumber, MsiBoardDev);

		MsiBoardNumber	= BoardNumber;
		MsiRealStation	= IdStation;

		// abre o device da estacao MSI
		sprintf(Name, "msiB%dC%d", MsiBoardNumber, MsiRealStation);

		// abre device da placa MSI
		Msi = ms_open(Name, 0);

		if(Msi == -1){
			PutError("Cannot open Msi Station in function OpenChannelMSI() Station %d", Canal);
			return -1;
		}

		// guarda o timeslot DTI de transmissao para uso posterior
		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp = &tslot;

		if(ms_getxmitslot(Msi, &sc_tsinfo) == -1){
			PutError("ms_getxmitslot() error in function OpenStation() Channel %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}

		TimeslotLine = tslot;

		// Faz o timeslot de recepcao da estacao nao ouvir nada
		ms_unlisten(Msi);
		
		// Seta mascara de eventos para o device da estacao MSI
		ms_setevtmsk(Msi, MSEV_SIGMSK, MSMM_OFFHOOK|MSMM_ONHOOK|MSMM_HOOKFLASH, DTA_SETMSK);

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelMSI()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	CloseChannel

	DESCRICAO:	Funcao construtora da classe Dialogic

	PARAMETROS:

	RETORNO:
********************************************************************************************/
void Dialogic::CloseChannel(void)
{
	try {

		switch(TipoInterface){

			case E1:	// Interface de linha digital E1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)
			case IP:	// Interface IP (H323, SIP), (canal de voz associado)
			case T1:	// Interface de linha digital T1 (R2, ISDN, GLOBAL CALL), (canal de voz associado)

#ifdef _ASR
				ASRTerminate();
#endif // _ASR
				
				switch(TipoSinalizacao){
				
					case LINE_SIDE:
					case R2_DIGITAL:
					case R2:
#ifdef _ASR
						if( !bAsrEnabled )
							dx_stopch(Vox, EV_ASYNC);
						else
							ec_stopch(Vox, SENDING, EV_ASYNC);
#else
						dx_stopch(Vox, EV_ASYNC);
#endif // _ASR



						Devs[0] = Vox;
						Devs[1] = Dti;
						Devs[2] = 0;
						
						sr_waitevtEx(Devs, 2, 100, &EventHandle);
						
						if(Dti){
							dt_close(Dti);
						}
						if(Vox){
							dx_close(Vox);
						}
					break;
					
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:

						switch( GetChannelStatus() )
						{
							case CHANNEL_IDLE:
							case CHANNEL_BLOCKED:
							case CHANNEL_DISCONNECTED:
								break;

							case CHANNEL_OFFERED:
							case CHANNEL_ACCEPTED:
							case CHANNEL_ANSWERED:
							case CHANNEL_ALERTING:
							case CHANNEL_CONNECTED:
							case CHANNEL_CALLSTATUS:
							case CHANNEL_DIALING:
								// Anula o ponteiro do longjmp para poder pegar eventos 
								JumpBuffer = NULL;
								Disconnect();
								break;
						}

						if(Dti){
							cc_Close(Dti);
						}
						if(Vox){
							dx_close(Vox);
						}
					break;

					case GLOBAL_CALL:
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						
						switch( GetChannelStatus() )
						{
							case CHANNEL_IDLE:
							case CHANNEL_BLOCKED:
							case CHANNEL_DISCONNECTED:
								break;

							case CHANNEL_OFFERED:
							case CHANNEL_ACCEPTED:
							case CHANNEL_ANSWERED:
							case CHANNEL_ALERTING:
							case CHANNEL_CONNECTED:
							case CHANNEL_CALLSTATUS:
							case CHANNEL_DIALING:
								// Anula o ponteiro do longjmp para poder pegar eventos 
								JumpBuffer = NULL;
								Disconnect();
								break;
						}

						gc_Close(dtidev);
						if(ipdev < 0)
							gc_Close(ipdev);
						break;
				}
			break;

			case LSI:	// Interface de linha analogica (canal de voz associado)
			case VOX:	// Interface de recurso de voz sem interface de linha associado

#ifdef _ASR
				ASRTerminate();
#endif // _ASR

#ifdef _ASR
				if( !bAsrEnabled )
					dx_stopch(Vox, EV_ASYNC);
				else
					ec_stopch(Vox, SENDING, EV_ASYNC);
#else
				dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

				Devs[0] = Vox;
				Devs[1] = 0;
						
				sr_waitevtEx(Devs, 1, 1, &EventHandle);

				if(Vox > 0){
					dx_close(Vox);
				}
			break;

			case MSI:	// Interface de posicao de atendimento (sem canal de voz associado)
				if(Msi > 0){
					ms_close(Msi);
				}
			break;

			case FAX:	// Interface de recurso de fax sem interface de linha associado
				if(Fax > 0){
					fx_close(Fax);
				}
			break;
		}
	}
	catch(...)
	{
		PutError("Exception CloseChannel()");
	}
}

std::string Dialogic::GetExePath()
{	
    boost::filesystem::path pathExec;
    std::string strExecPath;
    pathExec = boost::dll::program_location().parent_path();
    return pathExec.string();
}


/********************************************************************************************
	Funcao:			SetDXBD
							
	Comentario:	Esta funcao le e seta os parametros para discagem em LOOP START

	Parametros:	nenhum
							
	Retorno:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetDXBD(void)
{	
	int i;
	char ras[_MAX_PATH+1];

    TCHAR tzAppName[_MAX_PATH];
    TCHAR tzKey[_MAX_PATH];
    TCHAR tzDefaultValue[_MAX_PATH];
    TCHAR tzValor[_MAX_PATH];
    TCHAR tzArq[_MAX_PATH];
    

	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		memset(&dxbd, 0, sizeof(dxbd));

		for(i = 0; ParDXBD[i].nome_par != NULL; i++){
			
            swprintf(tzAppName,_MAX_PATH, L"%hs", "DXBD");
            swprintf(tzKey,_MAX_PATH, L"%hs", ParDXBD[i].nome_par);
            swprintf(tzDefaultValue,_MAX_PATH, L"%hs", ParDXBD[i].valor_default);
            swprintf(tzValor,_MAX_PATH, L"%hs", ras);
            swprintf(tzArq,_MAX_PATH, L"%hs", NomeArquivoPar);

            GetPrivateProfileString(tzAppName, tzKey, tzDefaultValue, tzValor, 80, tzArq);

            /*
			GetPrivateProfileString(	"DXBD",
																ParDXBD[i].nome_par,
																ParDXBD[i].valor_default,
																ras,
																80,
																NomeArquivoPar);
            */
			switch(i){	

				case 0:		dxbd.flashtm	= atoi(ras);	break;
				case 1:		dxbd.pausetm	= atoi(ras);	break;
				case 2:		dxbd.digrate	= atoi(ras);	break;
				case 3:		dxbd.sch_tm		= atoi(ras);	break;
				case 4:		dxbd.p_bk			= atoi(ras);	break;
				case 5:		dxbd.p_mk			= atoi(ras);	break;
				case 6:		dxbd.p_idd		= atoi(ras);	break;
				case 7:		dxbd.t_idd		= atoi(ras);	break;
				case 8:		dxbd.oh_dly		= atoi(ras);	break;
				case 9:		dxbd.r_on			= atoi(ras);	break;
				case 10:	dxbd.r_off		= atoi(ras);	break;
				case 11:	dxbd.r_ird		= atoi(ras);	break;
				case 12:	dxbd.s_bnc		= atoi(ras);	break;
				case 13:	dxbd.ttdata		= atoi(ras);	break;
				case 14:	dxbd.minpdon	= atoi(ras);	break;
				case 15:	dxbd.minpdoff	= atoi(ras);	break;
				case 16:	dxbd.minipd		= atoi(ras);	break;
				case 17:	dxbd.minlcoff	= atoi(ras);	break;
				case 18:	dxbd.redge		= atoi(ras);	break;
				case 19:	dxbd.maxpdoff	= atoi(ras);	break;
			}
		}

		if(dxbd.flashtm != 0)	dx_setparm(Vox,	DXBD_FLASHTM,		(void *)&dxbd.flashtm);
		if(dxbd.pausetm != 0)	dx_setparm(Vox,	DXBD_PAUSETM,		(void *)&dxbd.pausetm);
		if(dxbd.p_bk	  != 0)	dx_setparm(Vox,	DXBD_P_BK,			(void *)&dxbd.p_bk);
		if(dxbd.p_mk    != 0)	dx_setparm(Vox,	DXBD_P_MK,			(void *)&dxbd.p_mk);
		if(dxbd.p_idd   != 0)	dx_setparm(Vox,	DXBD_P_IDD,			(void *)&dxbd.p_idd);
		if(dxbd.t_idd   != 0)	dx_setparm(Vox,	DXBD_T_IDD,			(void *)&dxbd.t_idd);
		if(dxbd.oh_dly  != 0)	dx_setparm(Vox,	DXBD_OFFHDLY,		(void *)&dxbd.oh_dly);
		if(dxbd.r_on    != 0)	dx_setparm(Vox,	DXBD_R_ON,			(void *)&dxbd.r_on);
		if(dxbd.r_ird		!= 0)	dx_setparm(Vox,	DXBD_R_IRD,			(void *)&dxbd.r_ird);
		if(dxbd.s_bnc   != 0)	dx_setparm(Vox,	DXBD_S_BNC,			(void *)&dxbd.s_bnc);
		if(dxbd.ttdata  != 0)	dx_setparm(Vox,	DXBD_TTDATA,		(void *)&dxbd.ttdata);
		if(dxbd.minpdon != 0)	dx_setparm(Vox,	DXBD_MINPDON,		(void *)&dxbd.minpdon);
		if(dxbd.minpdoff!= 0)	dx_setparm(Vox,	DXBD_MINPDOFF,	(void *)&dxbd.minpdoff);
		if(dxbd.minipd  != 0)	dx_setparm(Vox,	DXBD_MINIPD,		(void *)&dxbd.minipd);
		if(dxbd.minlcoff!= 0)	dx_setparm(Vox,	DXBD_MINLCOFF,	(void *)&dxbd.minlcoff);
		if(dxbd.redge   != 0)	dx_setparm(Vox,	DXBD_R_EDGE,		(void *)&dxbd.redge);
		if(dxbd.maxpdoff!= 0)	dx_setparm(Vox,	DXBD_MAXPDOFF,	(void *)&dxbd.maxpdoff);

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetDXBD()");
		return -1;
	}
	return 0;
}
/********************************************************************************************
	Funcao:			SetDXCAP
							
	Comentario:	Esta funcao le e seta os parametros para os recursos de voz

	Parametros:	nenhum
							
	Retorno:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetDXCAP(void)
{	
	int i;
	char ras[_MAX_PATH+1];

    TCHAR tzAppName[_MAX_PATH];
    TCHAR tzKey[_MAX_PATH];
    TCHAR tzDefaultValue[_MAX_PATH];
    TCHAR tzValor[_MAX_PATH];
    TCHAR tzArq[_MAX_PATH];

	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		dx_clrcap(&dx_cap);

		for(i = 0; ParCAP[i].nome_par != NULL; i++){
			
            swprintf(tzAppName,_MAX_PATH, L"%hs", "CAP");
            swprintf(tzKey,_MAX_PATH, L"%hs", ParDXBD[i].nome_par);
            swprintf(tzDefaultValue,_MAX_PATH, L"%hs", ParDXBD[i].valor_default);
            swprintf(tzValor,_MAX_PATH, L"%hs", ras);
            swprintf(tzArq,_MAX_PATH, L"%hs", NomeArquivoPar);

            /*
			GetPrivateProfileString(	"CAP",
																ParCAP[i].nome_par,
																ParCAP[i].valor_default,
																ras,
																80,
																NomeArquivoPar);
            */
			switch(i){

				case 0:		dx_cap.ca_stdely     	= atoi(ras);	break;
				case 1:		dx_cap.ca_cnosig     	= atoi(ras);	break;
				case 2:		dx_cap.ca_lcdly      	= atoi(ras);	break;
				case 3:		dx_cap.ca_lcdly1     	= atoi(ras);	break;
				case 4:		dx_cap.ca_hedge      	= atoi(ras);	break;
				case 5:		dx_cap.ca_intflg     	= atoi(ras);	break;
				case 6:		dx_cap.ca_intfltr    	= atoi(ras);	break;
				case 7:		dx_cap.ca_lowerfrq   	= atoi(ras);	break;
				case 8:		dx_cap.ca_upperfrq   	= atoi(ras);	break;
				case 9:		dx_cap.ca_timefrq    	= atoi(ras);	break;
				case 10:	dx_cap.ca_maxansr    	= atoi(ras);	break;
				case 11:	dx_cap.ca_ansrdgl    	= atoi(ras);	break;
				case 12:	dx_cap.ca_mxtimefrq  	= atoi(ras);	break;
				case 13:	dx_cap.ca_lower2frq  	= atoi(ras);	break;
				case 14:	dx_cap.ca_upper2frq  	= atoi(ras);	break;
				case 15:	dx_cap.ca_time2frq   	= atoi(ras);	break;
				case 16:	dx_cap.ca_mxtime2frq 	= atoi(ras);	break;
				case 17:	dx_cap.ca_lower3frq  	= atoi(ras);	break;
				case 18:	dx_cap.ca_upper3frq  	= atoi(ras);	break;
				case 19:	dx_cap.ca_time3frq   	= atoi(ras);	break;
				case 20:	dx_cap.ca_mxtime3frq 	= atoi(ras);	break;
				case 21:	dx_cap.ca_dtn_pres   	= atoi(ras);	break;
				case 22:	dx_cap.ca_dtn_npres  	= atoi(ras);	break;
				case 23:	dx_cap.ca_dtn_deboff 	= atoi(ras);	break;
				case 24:	dx_cap.ca_noanswer   	= atoi(ras);	break;
				case 25:	dx_cap.ca_maxintering	= atoi(ras);	break;
			}
		}

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...){
		PutError("Exception Dialogic::SetDXCAP()");
		return -1;
	}
	return 0;
}
/********************************************************************************************
	Funcao:			SetFlashTime
							
	Comentario:	Esta funcao seta o tempo de flash para discagem analogica

	Parametros:	int iFlashTime - tempo de flash em (ms)

	Retorno:		Nenhum
********************************************************************************************/
void Dialogic::SetFlashTime(int iFlashTime)
{
	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		int iRef = iFlashTime / 10; // unidades de (x10ms)
	
		dx_setparm(Vox,	DXBD_FLASHTM, (void *)&iRef);

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...){
		PutError("Exception Dialogic::SetFlashTime()");
	}
}
/********************************************************************************************
	Funcao:			SetPauseTime
							
	Comentario:	Esta funcao seta o tempo de pausa (,) na string de discagem

	Parametros:	int iPauseTime - tempo de pausa

	Retorno:		Nenhum
********************************************************************************************/
void Dialogic::SetPauseTime(int iPauseTime)
{
	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		int iRef = iPauseTime / 10; // unidades de (x10ms)
	
		dx_setparm(Vox,	DXBD_PAUSETM, (void *)&iRef);

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...){
		PutError("Exception Dialogic::SetPauseTime()");
	}
}
/********************************************************************************************
	Funcao:			ReadTones
							
	Comentario:	Esta funcao le e seta os parametros para o tom de hangup em conexao LOOP START

	Parametros:	char *Arq						- Nome do arquivo INI
							char *sAppName			-	Nome da chave da aplicacao
							char *sKey					-	Nome da subchave
							char *sDefaultValue	-	Valor default
							TONE_DEF &ToneDef		-	Estrutura para a definicao do tom
							
	Retorno:		Nenhum
********************************************************************************************/
static void ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, TONE_DEF &ToneDef)
{
	char sValor[100];

	try {
        TCHAR tzAppName[_MAX_PATH];
        TCHAR tzKey[_MAX_PATH];
        TCHAR tzDefaultValue[_MAX_PATH];
        TCHAR tzValor[_MAX_PATH];
        TCHAR tzArq[_MAX_PATH];

        swprintf(tzAppName,_MAX_PATH, L"%hs", sAppName);
        swprintf(tzKey,_MAX_PATH, L"%hs", sKey);
        swprintf(tzDefaultValue,_MAX_PATH, L"%hs", sDefaultValue);
        swprintf(tzValor,_MAX_PATH, L"%hs", sValor);
        swprintf(tzArq,_MAX_PATH, L"%hs", Arq);

        GetPrivateProfileString(tzAppName, tzKey, tzDefaultValue, tzValor, 80, tzArq);
		//GetPrivateProfileString(sAppName, sKey, sDefaultValue, sValor, 80, Arq);

		ToneDef.id_tone			= atoi( strtok(sValor,	",") );
		ToneDef.freq_1			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_1			=	atoi( strtok(NULL,		",") );
		ToneDef.freq_2			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_2			=	atoi( strtok(NULL,		",") );
		ToneDef.ontime			=	atoi( strtok(NULL,		",") );
		ToneDef.ontdev			=	atoi( strtok(NULL,		",") );
		ToneDef.offtime			=	atoi( strtok(NULL,		",") );
		ToneDef.offtdev			=	atoi( strtok(NULL,		",") );
		ToneDef.repet_cont	=	atoi( strtok(NULL,		",") );
	}
	catch(...){
	}
}
/********************************************************************************************
	Funcao:			ReadTones
							
	Comentario:	Esta funcao le e seta os parametros para o tom de hangup em conexao LOOP START

	Parametros:	char *Arq						- Nome do arquivo INI
							char *sAppName			-	Nome da chave da aplicacao
							char *sKey					-	Nome da subchave
							TONE_DEF &ToneDef		-	Estrutura para a definicao do tom
							
	Retorno:		Nenhum
********************************************************************************************/
static void ReadTones(char *Arq, char *sAppName, char *sKey, TONE_DEF &ToneDef)
{
	char sValor[100];

	try {
        TCHAR tzAppName[_MAX_PATH];
        TCHAR tzKey[_MAX_PATH];
        TCHAR tzDefaultValue[_MAX_PATH];
        TCHAR tzValor[_MAX_PATH];
        TCHAR tzArq[_MAX_PATH];

        swprintf(tzAppName,_MAX_PATH, L"%hs", sAppName);
        swprintf(tzKey,_MAX_PATH, L"%hs", sKey);
        swprintf(tzDefaultValue,_MAX_PATH, L"%hs", "");
        swprintf(tzValor,_MAX_PATH, L"%hs", sValor);
        swprintf(tzArq,_MAX_PATH, L"%hs", Arq);

        GetPrivateProfileString(tzAppName, tzKey, tzDefaultValue, tzValor, 80, tzArq);
		//GetPrivateProfileString(sAppName, sKey, "", sValor, 80, Arq);

		ToneDef.id_tone			= atoi( strtok(sValor,	",") );
		ToneDef.freq_1			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_1			=	atoi( strtok(NULL,		",") );
		ToneDef.freq_2			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_2			=	atoi( strtok(NULL,		",") );
	}
	catch(...){
	}
}
/********************************************************************************************
	Funcao:			ReadTones
							
	Comentario:	Esta funcao le e seta os parametros para o tom de hangup em conexao LOOP START

	Parametros:	char *Arq						- Nome do arquivo INI
							char *sAppName			-	Nome da chave da aplicacao
							char *sKey					-	Nome da subchave
							char *sDefaultValue	-	Valor default
							SINGLE_TONE_DEF &ToneDef		-	Estrutura para a definicao do tom
							
	Retorno:		0 sucesso, -1 erro
********************************************************************************************/
static int ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, SINGLE_TONE_DEF &ToneDef)
{
	char sValor[100];

	try {
        TCHAR tzAppName[_MAX_PATH];
        TCHAR tzKey[_MAX_PATH];
        TCHAR tzDefaultValue[_MAX_PATH];
        TCHAR tzValor[_MAX_PATH];
        TCHAR tzArq[_MAX_PATH];

        swprintf(tzAppName,_MAX_PATH, L"%hs", sAppName);
        swprintf(tzKey,_MAX_PATH, L"%hs", sKey);
        swprintf(tzDefaultValue,_MAX_PATH, L"%hs", "");
        swprintf(tzValor,_MAX_PATH, L"%hs", sValor);
        swprintf(tzArq,_MAX_PATH, L"%hs", Arq);

        GetPrivateProfileString(tzAppName, tzKey, tzDefaultValue, tzValor, 80, tzArq);
		//GetPrivateProfileString(sAppName, sKey, "", sValor, 80, Arq);

		if( sValor[0] == '\0' )
			return -1;

		ToneDef.id_tone		= atoi( strtok(sValor,	",") );
		ToneDef.freq			=	atoi( strtok(NULL,		",") );
		ToneDef.desv			=	atoi( strtok(NULL,		",") );

		return 0;
	}
	catch(...){
	}
}
/********************************************************************************************
	Funcao:			ReadTones
							
	Comentario:	Esta funcao le e seta os parametros para o tom de hangup em conexao LOOP START

	Parametros:	char *Arq						- Nome do arquivo INI
							char *sAppName			-	Nome da chave da aplicacao
							char *sKey					-	Nome da subchave
							char *sDefaultValue	-	Valor default
							SINGLE_TONE_DEF &ToneDef		-	Estrutura para a definicao do tom
							
	Retorno:		0 sucesso, -1 erro
********************************************************************************************/
static int ReadTones(char *Arq, char *sAppName, char *sKey, char *sDefaultValue, DOUBLE_TONE_DEF &ToneDef)
{
	char sValor[100];

	try {
        TCHAR tzAppName[_MAX_PATH];
        TCHAR tzKey[_MAX_PATH];
        TCHAR tzDefaultValue[_MAX_PATH];
        TCHAR tzValor[_MAX_PATH];
        TCHAR tzArq[_MAX_PATH];

        swprintf(tzAppName,_MAX_PATH, L"%hs", sAppName);
        swprintf(tzKey,_MAX_PATH, L"%hs", sKey);
        swprintf(tzDefaultValue,_MAX_PATH, L"%hs", "");
        swprintf(tzValor,_MAX_PATH, L"%hs", sValor);
        swprintf(tzArq,_MAX_PATH, L"%hs", Arq);

        GetPrivateProfileString(tzAppName, tzKey, tzDefaultValue, tzValor, 80, tzArq);
		//GetPrivateProfileString(sAppName, sKey, "", sValor, 80, Arq);

		if( sValor[0] == '\0' )
			return -1;

		ToneDef.id_tone			= atoi( strtok(sValor,	",") );
		ToneDef.freq_1			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_1			=	atoi( strtok(NULL,		",") );
		ToneDef.freq_2			=	atoi( strtok(NULL,		",") );
		ToneDef.desv_2			=	atoi( strtok(NULL,		",") );

		return 0;
	}
	catch(...){
	}
}

/********************************************************************************************
	Funcao:			SetHANGUP
							
	Comentario:	Esta funcao le e seta os parametros para o tom de hangup em conexao LOOP START

	Parametros:	nenhum
							
	Retorno:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetHANGUP(void)
{
	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		// le os parametros do primeiro tom
		ReadTones(NomeArquivoPar, "ToneDetection", "Hangup1", DEFAULT_HANGUP1, DefHangup1);

		if(dx_blddtcad(	DefHangup1.id_tone,
										DefHangup1.freq_1,
										DefHangup1.desv_1,
										DefHangup1.freq_2,
										DefHangup1.desv_2,
										DefHangup1.ontime,
										DefHangup1.ontdev,
										DefHangup1.offtime,
										DefHangup1.offtdev,
										DefHangup1.repet_cont) != 0){

			PutError("dx_bldstcad() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_addtone(Vox, 0, 0) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_enbtone(Vox, DefHangup1.id_tone, DM_TONEON) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}

		// le os parametros	do segundo tom
		ReadTones(NomeArquivoPar, "ToneDetection", "Hangup2", DEFAULT_HANGUP2, DefHangup2);

		if(dx_blddtcad(	DefHangup2.id_tone,
										DefHangup2.freq_1,
										DefHangup2.desv_1,
										DefHangup2.freq_2,
										DefHangup2.desv_2,
										DefHangup2.ontime,
										DefHangup2.ontdev,
										DefHangup2.offtime,
										DefHangup2.offtdev,
										DefHangup2.repet_cont) != 0){

			PutError("dx_bldstcad() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_addtone(Vox, 0, 0) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_enbtone(Vox, DefHangup2.id_tone, DM_TONEON) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetHANGUP Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...){
		PutError("Exception Dialogic::SetHANGUP()");
		LeaveCriticalSection( &CritSecR2Tones );
		return -1;
	}
	return 0;
}
/********************************************************************************************
	Funcao:			SetFAX
							
	Comentario:	Esta funcao le e seta os parametros para o primeiro tom de FAX

	Parametros:	nenhum
							
	Retorno:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetFAX(void)
{
	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		// le os parametros do primeiro tom
		ReadTones(NomeArquivoPar, "ToneDetection", "Fax1", DEFAULT_FAX1, DefFax1);

		if(dx_blddtcad(	DefFax1.id_tone,
										DefFax1.freq_1,
										DefFax1.desv_1,
										DefFax1.freq_2,
										DefFax1.desv_2,
										DefFax1.ontime,
										DefFax1.ontdev,
										DefFax1.offtime,
										DefFax1.offtdev,
										DefFax1.repet_cont) != 0){
			
			PutError("dx_bldstcad() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_addtone(Vox, 0, 0) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_enbtone(Vox, DefFax1.id_tone, DM_TONEON) != 0){
			PutError("dx_enbtone() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}

		// le os parametros do segundo tom
		ReadTones(NomeArquivoPar, "ToneDetection", "Fax2", DEFAULT_FAX1, DefFax2);

		if(dx_blddtcad(	DefFax2.id_tone,
										DefFax2.freq_1,
										DefFax2.desv_1,
										DefFax2.freq_2,
										DefFax2.desv_2,
										DefFax2.ontime,
										DefFax2.ontdev,
										DefFax2.offtime,
										DefFax2.offtdev,
										DefFax2.repet_cont) != 0){

			PutError("dx_bldstcad() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_addtone(Vox, 0, 0) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		if(dx_enbtone(Vox, DefFax2.id_tone, DM_TONEON) != 0){
			PutError("dx_addtone() Error in function Dialogic::SetFAX Channel: %d", Canal);
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}

		LeaveCriticalSection( &CritSecR2Tones );
	}
	catch(...){
		PutError("Exception Dialogic::SetFAX()");
		LeaveCriticalSection( &CritSecR2Tones );
		return -1;
	}
	return 0;
}
/********************************************************************************************
	Funcao:			SetMESSAGEBOX
							
	Comentario:	Esta funcao le e seta os parametros para detectar a melodia de voice mail, que
							� composta pelas notas de frequencia simples C5(1001,523,10),D5(1002,587,10),
							E5(1003,659,10), F5(1004,698,10) e G5(1005,783,10). Ou pelas frequencias duplas
							C6(2001,523,10,1046,20), D6(2002,587,10,1174,20), E6(2003,659,10,1318,20),
							F6(2004,698,10,1396,20) e G6(2005,783,10,1566,20). Os tons F5 e F6 n�o s�o pro-
							gramados para n�o exceder o limite. Os tons s� s�o programados se estiverem
							configurados no arquivo Par.ini.

	Parametros:	nenhum
							
	Retorno:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetMESSAGEBOX(void)
{
	try {
		bool AmplitudeSet = false;

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		// le os parametros do tom C5
		if( ReadTones(NomeArquivoPar, "ToneDetection", "C5", DEFAULT_MBOX1, DefC5) == 0 )
		{
			if(DefC5.id_tone)
			{
				if(dx_bldst( DefC5.id_tone, DefC5.freq, DefC5.desv, TN_TRAILING) != 0)
				{
					PutError("dx_bldst() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefC5.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}
		
		// le os parametros do tom D5
		if( ReadTones(NomeArquivoPar, "ToneDetection", "D5", DEFAULT_MBOX1, DefD5) == 0 )
		{
			if(DefD5.id_tone)
			{
				if(dx_bldst( DefD5.id_tone, DefD5.freq, DefD5.desv, TN_TRAILING) != 0)
				{
					PutError("dx_bldst() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefD5.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom E5
		if( ReadTones(NomeArquivoPar, "ToneDetection", "E5", DEFAULT_MBOX1, DefE5) == 0 )
		{
			if(DefE5.id_tone)
			{
				if(dx_bldst( DefE5.id_tone, DefE5.freq, DefE5.desv, TN_TRAILING) != 0)
				{
					PutError("dx_bldst() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefE5.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom G5
		if( ReadTones(NomeArquivoPar, "ToneDetection", "G5", DEFAULT_MBOX1, DefG5) == 0 )
		{
			if(DefG5.id_tone)
			{
				if(dx_bldst( DefG5.id_tone, DefG5.freq, DefG5.desv, TN_TRAILING) != 0)
				{
					PutError("dx_bldst() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefG5.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom C5 e C6 (harmonicos)
		if( ReadTones(NomeArquivoPar, "ToneDetection", "C6", DEFAULT_MBOX2, DefC6) == 0 )
		{
			if(DefC6.id_tone)
			{
				if( !AmplitudeSet )
				{
					// sets up the amplitudes
					dx_setgtdamp(-39,0,-39,0);
					AmplitudeSet = true;
				}

				if(dx_blddt( DefC6.id_tone, DefC6.freq_1, DefC6.desv_1, DefC6.freq_2, DefC6.desv_2, TN_TRAILING) != 0)
				{
					PutError("dx_blddt() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefC6.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom D5 e D6 (harmonicos)
		if( ReadTones(NomeArquivoPar, "ToneDetection", "D6", DEFAULT_MBOX2, DefD6) == 0 )
		{
			if(DefD6.id_tone)
			{
				if( !AmplitudeSet )
				{
					// sets up the amplitudes
					dx_setgtdamp(-39,0,-39,0);
					AmplitudeSet = true;
				}

				if(dx_blddt( DefD6.id_tone, DefD6.freq_1, DefD6.desv_1, DefD6.freq_2, DefD6.desv_2, TN_TRAILING) != 0)
				{
					PutError("dx_blddt() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefD6.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom E5 e E6 (harmonicos)
		if( ReadTones(NomeArquivoPar, "ToneDetection", "E6", DEFAULT_MBOX2, DefE6) == 0 )
		{
			if(DefE6.id_tone)
			{
				if( !AmplitudeSet )
				{
					// sets up the amplitudes
					dx_setgtdamp(-39,0,-39,0);
					AmplitudeSet = true;
				}

				if(dx_blddt( DefE6.id_tone, DefE6.freq_1, DefE6.desv_1, DefE6.freq_2, DefE6.desv_2, TN_TRAILING) != 0)
				{
					PutError("dx_blddt() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefE6.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		// le os parametros do tom G5 e G6 (harmonicos)
		if( ReadTones(NomeArquivoPar, "ToneDetection", "G6", DEFAULT_MBOX2, DefG6) == 0 )
		{
			if(DefG6.id_tone)
			{
				if( !AmplitudeSet )
				{
					// sets up the amplitudes
					dx_setgtdamp(-39,0,-39,0);
					AmplitudeSet = true;
				}

				if(dx_blddt( DefG6.id_tone, DefG6.freq_1, DefG6.desv_1, DefG6.freq_2, DefG6.desv_2, TN_TRAILING) != 0)
				{
					PutError("dx_blddt() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_addtone(Vox, NULL, 0) != 0)
				{
					PutError("dx_addtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
				if(dx_enbtone(Vox, DefG6.id_tone, DM_TONEON) != 0)
				{
					PutError("dx_enbtone() Error in function Dialogic::SetVOICEMAIL Channel: %d. %s", Canal, ATDV_ERRMSGP(Vox));
					LeaveCriticalSection( &CritSecR2Tones );
					return -1;
				}
			}
		}

		LeaveCriticalSection( &CritSecR2Tones );

		return 0;
	}
	catch(...){
		PutError("Exception Dialogic::SetMESSAGEBOX()");
		LeaveCriticalSection( &CritSecR2Tones );
		return -1;
	}
}
/********************************************************************************************
	Funcao:			SetCallProgress
							
	Comentario:	Seta os parametros para Call Progress

	Parametros:	nenhum
							
	Retorno:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::SetCallProgress(void)
{
	try {

		// Protege outras threads de executarem funcoes relacionadas a tons simultaneamente.
		EnterCriticalSection( &CritSecR2Tones );

		// le os parametros do primeiro tom de ocupado
		ReadTones(NomeArquivoPar, "CallProgressTones", "Busy1", DEFAULT_BUSY1_TONE, DefCpBusyTone1);

		// ID de tom reservado pela Dialogic
		DefCpBusyTone1.id_tone = TID_BUSY1;

		dx_chgfreq(	DefCpBusyTone1.id_tone,
								DefCpBusyTone1.freq_1,
								DefCpBusyTone1.desv_1,
								DefCpBusyTone1.freq_2,
								DefCpBusyTone1.desv_2);

		dx_chgdur(	DefCpBusyTone1.id_tone,
								DefCpBusyTone1.ontime,
								DefCpBusyTone1.ontdev,
								DefCpBusyTone1.offtime,
								DefCpBusyTone1.offtdev);

		dx_chgrepcnt(DefCpBusyTone1.id_tone, DefCpBusyTone1.repet_cont);

		// le os parametros do primeiro tom de ocupado
		ReadTones(NomeArquivoPar, "CallProgressTones", "Busy2", DEFAULT_BUSY1_TONE, DefCpBusyTone2);

		// ID de tom reservado pela Dialogic
		DefCpBusyTone2.id_tone = TID_BUSY2;

		dx_chgfreq(	DefCpBusyTone2.id_tone,
								DefCpBusyTone2.freq_1,
								DefCpBusyTone2.desv_1,
								DefCpBusyTone2.freq_2,
								DefCpBusyTone2.desv_2);

		dx_chgdur(	DefCpBusyTone2.id_tone,
								DefCpBusyTone2.ontime,
								DefCpBusyTone2.ontdev,
								DefCpBusyTone2.offtime,
								DefCpBusyTone2.offtdev);

		dx_chgrepcnt(DefCpBusyTone2.id_tone, DefCpBusyTone2.repet_cont);

		// le os parametros do primeiro tom de fax
		ReadTones(NomeArquivoPar, "CallProgressTones", "Fax1", DEFAULT_FAX1_TONE, DefCpFaxTone1);

		// ID de tom reservado pela Dialogic
		DefCpFaxTone1.id_tone = TID_FAX1;

		dx_chgfreq(	DefCpFaxTone1.id_tone,
								DefCpFaxTone1.freq_1,
								DefCpFaxTone1.desv_1,
								DefCpFaxTone1.freq_2,
								DefCpFaxTone1.desv_2);

		dx_chgdur(	DefCpFaxTone1.id_tone,
								DefCpFaxTone1.ontime,
								DefCpFaxTone1.ontdev,
								DefCpFaxTone1.offtime,
								DefCpFaxTone1.offtdev);

		dx_chgrepcnt(DefCpFaxTone1.id_tone, DefCpFaxTone1.repet_cont);

		// le os parametros do segundo tom de fax
		ReadTones(NomeArquivoPar, "CallProgressTones", "Fax2", DEFAULT_FAX2_TONE, DefCpFaxTone2);

		// ID de tom reservado pela Dialogic
		DefCpFaxTone2.id_tone = TID_FAX2; 

		dx_chgfreq(	DefCpFaxTone2.id_tone,
								DefCpFaxTone2.freq_1,
								DefCpFaxTone2.desv_1,
								DefCpFaxTone2.freq_2,
								DefCpFaxTone2.desv_2);

		dx_chgdur(	DefCpFaxTone2.id_tone,
								DefCpFaxTone2.ontime,
								DefCpFaxTone2.ontdev,
								DefCpFaxTone2.offtime,
								DefCpFaxTone2.offtdev);

		dx_chgrepcnt(DefCpFaxTone2.id_tone, DefCpFaxTone2.repet_cont);

		// le os parametros do tom de linha local
		ReadTones(NomeArquivoPar, "CallProgressTones", "LocalDialTone", DEFAULT_LOCAL_DIAL_TONE, DefCpLocalDialTone);

		// ID de tom reservado pela Dialogic
		DefCpLocalDialTone.id_tone = TID_DIAL_LCL;

		dx_chgfreq(	DefCpLocalDialTone.id_tone,
								DefCpLocalDialTone.freq_1,
								DefCpLocalDialTone.desv_1,
								DefCpLocalDialTone.freq_2,
								DefCpLocalDialTone.desv_2);

		dx_chgdur(	DefCpLocalDialTone.id_tone,
								DefCpLocalDialTone.ontime,
								DefCpLocalDialTone.ontdev,
								DefCpLocalDialTone.offtime,
								DefCpLocalDialTone.offtdev);

		dx_chgrepcnt(DefCpLocalDialTone.id_tone, DefCpLocalDialTone.repet_cont);

		// le os parametros do tom de linha internacional
		ReadTones(NomeArquivoPar, "CallProgressTones", "InterDialTone", DEFAULT_INTER_DIAL_TONE, DefCpInterDialTone);

		// ID de tom reservado pela Dialogic
		DefCpInterDialTone.id_tone = TID_DIAL_INTL;

		dx_chgfreq(	DefCpInterDialTone.id_tone,
								DefCpInterDialTone.freq_1,
								DefCpInterDialTone.desv_1,
								DefCpInterDialTone.freq_2,
								DefCpInterDialTone.desv_2);

		dx_chgdur(	DefCpInterDialTone.id_tone,
								DefCpInterDialTone.ontime,
								DefCpInterDialTone.ontdev,
								DefCpInterDialTone.offtime,
								DefCpInterDialTone.offtdev);

		dx_chgrepcnt(DefCpInterDialTone.id_tone, DefCpInterDialTone.repet_cont);

		// le os parametros do tom de linha extra
		ReadTones(NomeArquivoPar, "CallProgressTones", "ExtraDialTone", DEFAULT_EXTRA_DIAL_TONE, DefCpExtraDialTone);

		// ID de tom reservado pela Dialogic
		DefCpExtraDialTone.id_tone = TID_DIAL_XTRA;

		dx_chgfreq(	DefCpExtraDialTone.id_tone,
								DefCpExtraDialTone.freq_1,
								DefCpExtraDialTone.desv_1,
								DefCpExtraDialTone.freq_2,
								DefCpExtraDialTone.desv_2);

		dx_chgdur(	DefCpExtraDialTone.id_tone,
								DefCpExtraDialTone.ontime,
								DefCpExtraDialTone.ontdev,
								DefCpExtraDialTone.offtime,
								DefCpExtraDialTone.offtdev);

		dx_chgrepcnt(DefCpExtraDialTone.id_tone, DefCpExtraDialTone.repet_cont);

		// le os parametros do primeiro tom de ringback
		ReadTones(NomeArquivoPar, "CallProgressTones", "RingBack1", DEFAULT_RING_BACK1_TONE, DefCpRingBackTone1);

		// ID de tom reservado pela Dialogic
		DefCpRingBackTone1.id_tone = TID_RNGBK1;

		dx_chgfreq(	DefCpRingBackTone1.id_tone,
								DefCpRingBackTone1.freq_1,
								DefCpRingBackTone1.desv_1,
								DefCpRingBackTone1.freq_2,
								DefCpRingBackTone1.desv_2);

		dx_chgdur(	DefCpRingBackTone1.id_tone,
								DefCpRingBackTone1.ontime,
								DefCpRingBackTone1.ontdev,
								DefCpRingBackTone1.offtime,
								DefCpRingBackTone1.offtdev);

		dx_chgrepcnt(DefCpRingBackTone1.id_tone, DefCpRingBackTone1.repet_cont);

		// le os parametros do segundo tom de ringback
		ReadTones(NomeArquivoPar, "CallProgressTones", "RingBack2", DEFAULT_RING_BACK2_TONE, DefCpRingBackTone2);

		// ID de tom reservado pela Dialogic
		DefCpRingBackTone2.id_tone = TID_RNGBK2;

		dx_chgfreq(	DefCpRingBackTone2.id_tone,
								DefCpRingBackTone2.freq_1,
								DefCpRingBackTone2.desv_1,
								DefCpRingBackTone2.freq_2,
								DefCpRingBackTone2.desv_2);

		dx_chgdur(	DefCpRingBackTone2.id_tone,
								DefCpRingBackTone2.ontime,
								DefCpRingBackTone2.ontdev,
								DefCpRingBackTone2.offtime,
								DefCpRingBackTone2.offtdev);

		dx_chgrepcnt(DefCpRingBackTone2.id_tone, DefCpRingBackTone2.repet_cont);

		// deleta todos os tons da placa
		if(dx_deltones(Vox)){
			PutError("dx_initcallp() error in function SetCallProgress() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}
		// inicia o processo de analise de chamadas
		if(dx_initcallp(Vox)){
			PutError("dx_initcallp() error in function SetCallProgress() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			LeaveCriticalSection( &CritSecR2Tones );
			return -1;
		}

		LeaveCriticalSection( &CritSecR2Tones );
		return 0;
	}
	catch(...){
		PutError("Exception Dialogic::SetCallProgress()");
		LeaveCriticalSection( &CritSecR2Tones );
		return -1;
	}
}

//=============================================================================================================================
//
// FROM DIALOGICGENERIC.CPP
//

void Dialogic::CtiLog(const char *sformat, ...)
{
    char sMsg[ MAX_CHAR_MESSAGE_TEXT + 1 ];
	va_start( ArgPtrLog, sformat );
	_vsnprintf( sMsg, MAX_CHAR_MESSAGE_TEXT + 1, sformat, ArgPtrLog );
	va_end( ArgPtrLog );
	sMsg[ MAX_CHAR_MESSAGE_TEXT ] = 0;

    m_pLog4cpp->debug(sMsg);
}

/********************************************************************************************
	NOME:	CtiLog

	DESCRICAO:	Loga as informacoes usando um ponteiro de funcao se o mesmo
							estiver inicializado

	PARAMETROS:	string sInfo - informacao a ser logada

	RETORNO:
********************************************************************************************/
void Dialogic::CtiLog_Original(const char *sformat, ...)
{
	try {

		//if( pLogInterface == NULL ){
		//	return;
		//}

		if( iCtiLogLevel < 0 ){
			// Se o nivel de log estiver negativo, seta o mesmo para o nivel de erro
			iCtiLogLevel = 0;
		}

		// verifica se o nivel de log esta ativado
		//if( !pLogInterface->CheckLevel(iCtiLogLevel) )
		//	return;

		//_ftime( &TimeBuffer );
		//PTimeStruct = localtime( &TimeBuffer.time );
        PTimeStruct = localtime( &TimeBuffer);

		// Prepara o texto da mensagem propriamente dita,
		// limitando-o no n�mero m�ximo de caracteres
		char sMsg[ MAX_CHAR_MESSAGE_TEXT + 1 ];
		va_start( ArgPtrLog, sformat );
		_vsnprintf( sMsg, MAX_CHAR_MESSAGE_TEXT + 1, sformat, ArgPtrLog );
		va_end( ArgPtrLog );
		sMsg[ MAX_CHAR_MESSAGE_TEXT ] = 0;

		// Substitui enventuais caracteres | (pipe) da mensagem por #
		for ( int i = 0; i < strlen( sMsg ); i++ )
			if ( sMsg[i] == '|' )
				sMsg[i] = '#';

		// Tamanho da linha de mensagem
		int len = 0;
		// Armazena a data e hora da ocorr�ncia antes de colocar a mensagem
		len = sprintf( pLogBuffer,"%04d-%02d-%02d %02d:%02d:%02d.%03d", PTimeStruct->tm_year+1900, PTimeStruct->tm_mon + 1, PTimeStruct->tm_mday, 
                                                                        PTimeStruct->tm_hour, PTimeStruct->tm_min, PTimeStruct->tm_sec, 
                                                                        0 );
                                                                        //TimeBuffer.millitm );
		// Concatena com o tipo, origem, detalhes e com o texto da msg. propriamente dito
		len += sprintf( &pLogBuffer[len], "|---|VOX|Dialogic       |%s", sMsg );

		//pLogInterface->Print();
	}
	catch(...)
	{
	}
}
/********************************************************************************************
	NOME:	PutError

	DESCRICAO:	Guarda o ultimo errro ocorrido

	PARAMETROS:	igual a printf

	RETORNO:
********************************************************************************************/
void Dialogic::PutError(char *fmt, ...)
{
	try {

		va_start(ArgPtr, fmt);
		vsprintf(BufLastError, fmt, ArgPtr);
		va_end(ArgPtr);
	}
	catch(...)
	{
	}
}
/********************************************************************************************
	NOME:	GetLastError

	DESCRICAO:	Retorna o ultimo erro ocorrido

	PARAMETROS:	char *Buffer	-	Buffer para guardar o erro
							int Size			-	Tamanho do Buffer -1 caracter

	RETORNO:
********************************************************************************************/
void Dialogic::GetLastError(char *Buffer, int Size)
{
	try {

		if( (int) strlen(BufLastError) >= Size )
			strncpy(Buffer, BufLastError, Size - 1); 
		else
			strcpy(Buffer, BufLastError);
	}
	catch(...)
	{
	}
}
/********************************************************************************************
	NOME:	GetLastError

	DESCRICAO:	Retorna o ultimo erro ocorrido

	PARAMETROS:

	RETORNO:		const char * - string com o ultimo erro ocorrido
********************************************************************************************/
const char *Dialogic::GetLastError(void)
{
	try {
		return BufLastError;
	}
	catch(...)
	{
	}
}
/********************************************************************************************
	NOME:	GetEvent

	DESCRICAO:	Fica esperando evento

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
							-1 se nao tem evento
********************************************************************************************/
int Dialogic::GetEvent( int Time )
{
	try {

		switch(TipoInterface){

			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
						return(ProcLineSide( Time ));

					case R2:
						return(ProcR2( Time ));

					case R2_DIGITAL:
						return(ProcR2digital( Time ));
					
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
						return(ProcISDN( Time ));
					
					case GLOBAL_CALL:
					case GLOBAL_CALL_DM3:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_IP:
						return(ProcGCGeneric( Time ));
				}
			break;
			
			case LSI:
				switch(TipoSinalizacao){

					case LOOP_START:
						return(ProcLSI( Time ));

					case LOOP_START_TRAP:
						return(ProcLSITrap( Time ));
				}
			break;

			case VOX:
				return(ProcVOX( Time ));
			
			case FAX:
				return(ProcFAX( Time ));
			
			case MSI:
				return(ProcMSI( Time ));
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetEvent()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	InitChannel

	DESCRICAO:	Seta o estado do canal para livre

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::InitChannel(void)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {
        int iRet = 0;
        ChannelStatus = CHANNEL_IDLE;
        m_pLog4cpp->debug("[%p][%s] Set ChannelStatus = CHANNEL_IDLE", this, __FUNCTION__);

		if(FlagChannelInit)
			return 0;

        int rc = -1;
		// So pode chamar essa funcao uma vez
		FlagChannelInit = true;

		switch(TipoInterface){

			case E1:
			case IP:
			case T1:
			
				switch(TipoSinalizacao){

					case LINE_SIDE:
						ChannelStatus = CHANNEL_IDLE;
						// Habilita a deteccao de digitos pela fila de eventos 
						DigitQueue(true);
						// Desbloqueia o canal
						CasSetBit(AOFF);
						CasSetBit(BON);
						CasSetBit(COFF);
						CasSetBit(DON);
						return 0;

					case R2:
						ChannelStatus = CHANNEL_IDLE;
						// Desbloqueia o canal
						CasSetBit(AON);
						CasSetBit(BOFF);
						return 0;

					case R2_DIGITAL:
						ChannelStatus = CHANNEL_IDLE;
						// Desbloqueia o canal
						CasSetBit(AON);
						CasSetBit(BOFF);
						return 0;

					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
						ChannelStatus = CHANNEL_IDLE;
						if(cc_WaitCall(Dti, NULL, NULL, 0, EV_ASYNC) != 0){
						}					
						return 0;

					case GLOBAL_CALL:
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						return 0;
				}
			break;

			case LSI:
				ChannelStatus = CHANNEL_IDLE;
				ScConnect(FULLDUP);
				if(TipoSinalizacao == LOOP_START_TRAP)
					dx_sethook(Vox, DX_OFFHOOK, EV_SYNC);
				else
					dx_sethook(Vox, DX_ONHOOK, EV_SYNC);
				return 0;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::InitChannel()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	SetChannelState

	DESCRICAO:	Seta o estado do canal para bloqueado ou livre

	PARAMETROS:	int State -	CHANNEL_IN_SERVICE			Canal livre para atender ligacoes
													CHANNEL_OUT_OF_SERVICE	Canal for de servico

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::SetChannelState(int State)
{
	int Event;
	int iChannelStatus = 0;
    int iRet = 0;
    int rc = -1;

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {
		
		if( ( ChannelStatus == CHANNEL_CONNECTED ) || ( ChannelStatus == CHANNEL_ANSWERED ) )
		{			
			return 0;
		}

		if( ( ChannelStatus == CHANNEL_BLOCKED ) && ( State == CHANNEL_OUT_OF_SERVICE ) )
		{
			return 0;
		}

		if( ( ChannelStatus == CHANNEL_IDLE ) && ( State == CHANNEL_IN_SERVICE ) )
		{
			return 0;
		}
		
        if ( (TipoInterface == E1) || ( TipoInterface == IP ) || ( TipoInterface == T1 ) )
        {
            if ( TipoSinalizacao == LINE_SIDE )
            {
                if(State == CHANNEL_OUT_OF_SERVICE)
                {
					// Bloqueia o canal
					CasSetBit(AON);
					CasSetBit(BON);
					this->ChannelStatus = CHANNEL_BLOCKED;
				}
				else
                {
					// Desbloqueia o canal
					CasSetBit(AOFF);
					CasSetBit(BON);
					this->ChannelStatus = CHANNEL_IDLE;
				}
				FlagUserBlock = false;
				return 0;
            }
            else if ( TipoSinalizacao == R2 )
            {
                if(State == CHANNEL_OUT_OF_SERVICE){
					// Bloqueia o canal
					CasSetBit(AON);
					CasSetBit(BON);
					this->ChannelStatus = CHANNEL_BLOCKED;
				}
				else
                {
					// Desbloqueia o canal
					CasSetBit(AON);
					CasSetBit(BOFF);
					this->ChannelStatus = CHANNEL_IDLE;
				}
				FlagUserBlock = false;
				return 0;
            }
            else if ( TipoSinalizacao == R2_DIGITAL )
            {
                if(State == CHANNEL_OUT_OF_SERVICE)
                {
					// Bloqueia o canal
					CasSetBit(AON);
					CasSetBit(BON);
					this->ChannelStatus = CHANNEL_BLOCKED;
				}
				else
                {
					// Desbloqueia o canal
					CasSetBit(AON);
					CasSetBit(BOFF);
					this->ChannelStatus = CHANNEL_IDLE;
				}
				FlagUserBlock = false;
				return 0;
            } 
            else if ( (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) ||
                       (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS)
                     )
            {
                if(State == CHANNEL_OUT_OF_SERVICE)
				{
                    m_pLog4cpp->debug("[%p][%s] State = OUT_OF_SERVICE", this, __FUNCTION__);
					cc_SetChanState( Dti, OUT_OF_SERVICE, EV_ASYNC );
					do {
						Event = GetEvent();
					} while( (Event != -1) && (Event != T_BLOCKED) );
				}
				else
                {
                    m_pLog4cpp->debug("[%p][%s] State = IN_SERVICE", this, __FUNCTION__);
					cc_SetChanState( Dti, IN_SERVICE, EV_ASYNC );
					do {
						Event = GetEvent();
					} while( (Event != -1) && (Event != T_IDLE) );

					cc_Restart( Dti, EV_ASYNC );
					do {
						Event = GetEvent();
					} while( (Event != -1) && (Event != T_RESET) );

					cc_WaitCall( Dti, NULL, NULL, 0, EV_ASYNC );
				}
				FlagUserBlock = false;
				return 0;
            }
            else if (  (TipoSinalizacao == GLOBAL_CALL) || (TipoSinalizacao == GLOBAL_CALL_IP) || 
                        (TipoSinalizacao == GLOBAL_CALL_SS7) || (TipoSinalizacao == GLOBAL_CALL_DM3) )
            {
                if( State == CHANNEL_OUT_OF_SERVICE )
				{
					//ChannelStatus = CHANNEL_IDLE;
                    m_pLog4cpp->debug("[%p][%s] State = OUT_OF_SERVICE", this, __FUNCTION__);
					if( gc_SetChanState( ldev, GCLS_OUT_OF_SERVICE, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::SetChannelState()->gc_SetChanState(): %s", gc_GetStringError() );
						return -1;
					}
					do
					{
						Event = GetEvent();
					} while( (Event != -1) && (Event != T_BLOCKED) );

				}							
				else
				{
					//ChannelStatus = CHANNEL_BLOCKED;
                    m_pLog4cpp->debug("[%p][%s] State = IN_SERVICE", this, __FUNCTION__);
					if( gc_SetChanState( ldev, GCLS_INSERVICE, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::SetChannelState()->gc_SetChanState(): %s", gc_GetStringError() );
						return -1;
					}
					do
					{
						Event = GetEvent();
					} while( (Event != -1) && (Event != T_IDLE) );

					if( gc_ResetLineDev( ldev, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::SetChannelState()->gc_ResetLineDev(): %s", gc_GetStringError() );
						return -1;
					}
					do
					{
						Event = GetEvent();
					} while( (Event != -1) && ( Event != T_RESET ) );

					if( !DialOut )
					{
						if( gc_WaitCall( ldev, NULL, NULL, 0, EV_ASYNC ) != GC_SUCCESS )
						{
							PutError( "Error in Dialogic::SetChannelState()->gc_WaitCall() error: %s", gc_GetStringError() );
							return -1;
						}
					}
				}
				FlagUserBlock = false;
				return 0;
            } else {
                m_pLog4cpp->debug("[%p][%s] 'TipoSinalizacao' n�o encontrada.", this, __FUNCTION__);
                return -1;
            }
        } 
        else if ( TipoInterface == LSI )
        {
            ScConnect(FULLDUP);

			if(State == CHANNEL_OUT_OF_SERVICE)
            {
				dx_sethook(Vox, DX_OFFHOOK, EV_SYNC);
				this->ChannelStatus = CHANNEL_BLOCKED;
			}
			else
            {
				dx_sethook(Vox, DX_ONHOOK, EV_SYNC);
				this->ChannelStatus = CHANNEL_IDLE;
			}
			FlagUserBlock = false;
			return 0;
        }
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetChannelState()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	Disconnect

	DESCRICAO:	Desliga a ligacao corrente

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/

int Dialogic::Disconnect(int CauseDropCall)
{
	int Event;
	long tslot = 0;
	CRN	drop_crn;
    std::stringstream ss;
    int iRet = 0;
    int rc = -1;

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {

        if ( (TipoInterface == E1) || (TipoInterface == IP) || (TipoInterface = T1) )
        {
            if ( TipoSinalizacao == LINE_SIDE )
            {
                switch(CauseDropCall)
                {

					case DROP_NORMAL:
						ScConnect(FULLDUP);

						CasSetBit(AOFF);
						CasSetBit(BON);
						this->ChannelStatus = CHANNEL_IDLE;
						R2_State = R2_IDLE; // Seta o estado do canal para LIVRE
					break;

					case DROP_BUSY:
					case DROP_CONGESTION:
					break;
				}
                return 0;
            } 
            else if ( TipoSinalizacao == R2 )
            {
                switch(CauseDropCall)
                {

					case DROP_NORMAL:
						ScConnect(FULLDUP);
								
						CasSetBit(AON);
						CasSetBit(BOFF);
						this->ChannelStatus = CHANNEL_IDLE;
						R2_State = R2_IDLE; // Seta o estado do canal para LIVRE
					break;

					case DROP_BUSY:
					case DROP_CONGESTION:
						R2_PlayBackward(SIG_4); // Envia SIG_A4
					break;
				}
                return 0;
            } 
            else if ( TipoSinalizacao == R2_DIGITAL )
            {
                switch(CauseDropCall){

					case DROP_NORMAL:
						ScConnect(FULLDUP);
								
						CasSetBit(AON);
						R2WaitEvent();
						CasSetBit(BOFF);
						this->ChannelStatus = CHANNEL_IDLE;
					break;

					case DROP_BUSY:
						// Envia SIG_B2
						R2_PlayBackward(SIG_2);
						// Espera o tom enviado pela origem parar
						R2RecvToneOFF();
						// Para o tom enviado
#ifdef _ASR
						if( !bAsrEnabled )
							dx_stopch(Vox, EV_ASYNC);
						else
							ec_stopch(Vox, SENDING, EV_ASYNC);
#else
						dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
						// Espera o tom parar
						R2WaitEvent();
						// Libera o canal
						CasSetBit(BOFF);
					break;

					case DROP_CONGESTION:
						// Envia SIG_A4
						R2_PlayBackward(SIG_4);
						// Espera o tom enviado pela origem parar
						R2RecvToneOFF();
						// Para o tom enviado
#ifdef _ASR
						if( !bAsrEnabled )
							dx_stopch(Vox, EV_ASYNC);
						else
							ec_stopch(Vox, SENDING, EV_ASYNC);
#else
						dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
						// Espera o tom parar
						R2WaitEvent();
						// Libera o canal
						CasSetBit(BOFF);
					break;
				}
                return 0;
            } else if ( (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) ||
                         (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS) )
            {
                ScConnect(FULLDUP);
						
				if(cc_DropCall(crn, NORMAL_CLEARING, EV_ASYNC) == 0)
                {
                    // Missing something ?
				}	 
				do 
                {
					Event = GetEvent( 30000 );
				} while(Event != -1 && Event != T_DROPCALL);

				if(cc_ReleaseCall(crn) != GC_SUCCESS)
                {
                    // Missing something ?
				}
                return 0;
            } 
            else if ( (TipoSinalizacao == GLOBAL_CALL) || (TipoSinalizacao == GLOBAL_CALL_SS7) || 
                         (TipoSinalizacao == GLOBAL_CALL_IP) || (TipoSinalizacao == GLOBAL_CALL_DM3) )
            {
                m_pLog4cpp->debug("[%p][%s] TipoSinalizacao = %s", this, __FUNCTION__,
                    (TipoSinalizacao == GLOBAL_CALL?"GLOBAL_CALL":
                    (TipoSinalizacao == GLOBAL_CALL_SS7?"GLOBAL_CALL_SS7":
                    (TipoSinalizacao == GLOBAL_CALL_IP?"GLOBAL_CALL_IP":
                    (TipoSinalizacao == GLOBAL_CALL_DM3?"GLOBAL_CALL_DM3":"UNKNOWN")))));                        

                m_pLog4cpp->debug("[%p][%s] FlagUserBlock = %s", this, __FUNCTION__, (FlagUserBlock == true?"true":"false"));

                for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                {
                    if ( (kvCallStats.second == GCST_DIALING)|| (kvCallStats.second == GCST_OFFERED)||
                        (kvCallStats.second == GCST_ACCEPTED)|| (kvCallStats.second == GCST_CONNECTED)|| (kvCallStats.second == GCST_GETMOREINFO)||
                        (kvCallStats.second == GCST_SENDMOREINFO)|| (kvCallStats.second == GCST_CALLROUTING)|| (kvCallStats.second == GCST_PROCEEDING)||
                        (kvCallStats.second == GCST_ALERTING)
                        )
                    {
                        mapCrnToStatus[kvCallStats.first] = GCST_DISCONNECTED;                            
                        m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first);

                        if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                        {
						    PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
						    CtiLog( "%s", GetLastError() );
						    return -1;
					    }
                    }
                });


				return 0;	
            } else {
                return -1;
            }
        } 
        else if ( TipoInterface == LSI )
        {
            // Esvazia a fila de eventos
			ClearEventQueue();
			// Reconecta o recurso de loop start interface com o recurso de voz
			dx_unlisten(Vox);
			ag_unlisten(Vox);
			ScConnect(FULLDUP);
			// Poe no gancho
			dx_sethook(Vox, DX_ONHOOK, EV_SYNC);
			this->ChannelStatus = CHANNEL_IDLE;
           
            return 0;
        } else {
            return -1;
        }

		//if( FlagUserBlock )
		//{       
        //    m_pLog4cpp->debug("[%p][%s] FlagUserBlock == true => ChannelState = CHANNEL_OUT_OF_SERVICE", this, __FUNCTION__);
		//	SetChannelState( CHANNEL_OUT_OF_SERVICE );
		//}

#ifdef _ASR
		// Avisa ao engine de reconhecimento q uma chamada acabou.
		ASRSetCallConnected( false );
#endif // _ASR				

		
	}
	catch(...)
	{
		PutError("Exception Dialogic::Disconnect()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	Disconnect2

	DESCRICAO:	Desliga a ligacao corrente

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/

int Dialogic::Disconnect2()
{
	int Event;
	bool disconnected = false;
    std::stringstream ss;

	try {

		if( ldev == ipdev )
			ldev = dtidev;
		else
			ldev = ipdev;
		ScConnect(FULLDUP);
        

        for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
        {
            if ( (kvCallStats.second == GCST_DIALING)|| (kvCallStats.second == GCST_OFFERED)||
                (kvCallStats.second == GCST_ACCEPTED)|| (kvCallStats.second == GCST_CONNECTED)|| (kvCallStats.second == GCST_GETMOREINFO)||
                (kvCallStats.second == GCST_SENDMOREINFO)|| (kvCallStats.second == GCST_CALLROUTING)|| (kvCallStats.second == GCST_PROCEEDING)||
                (kvCallStats.second == GCST_ALERTING)
                )
            {
                mapCrnToStatus[kvCallStats.first] = GCST_DISCONNECTED;                            
                m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first);

                if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                {
					PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
					CtiLog( "%s", GetLastError() );
					return -1;
				}
            }
        });
        

		return T_DISCONNECT;		
	}
	catch(...)
	{
		PutError("Exception Dialogic::Disconnect2()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	Answer

	DESCRICAO:	Atende a ligacao corrente

	PARAMETROS: bool bDoubleAnswer - se = true bloqueia ligacoes a cobrar com duplo atendimento

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::Answer(bool bDoubleAnswer, bool bBilling)
{
	int Event;
	int rc;
    int iRet = 0;
	
	try {
        m_pLog4cpp->debug("[%p][%s] ", this , __FUNCTION__);

        if ( (TipoInterface == E1) || (TipoInterface == IP) || (TipoInterface == T1) )
        {
            if ( TipoSinalizacao == LINE_SIDE )
            {
                ChannelStatus = CHANNEL_ANSWERED;                
				return T_ANSWER;
            } 
            else if ( TipoSinalizacao == R2_DIGITAL )
            {
                R2Answer(bDoubleAnswer, bBilling);
#ifdef _ASR
					// Avisa ao engine de reconhecimento q uma chamada chegou.
				ASRSetCallConnected( true );
#endif // _ASR
                return 0;
            } 
            else if ( TipoSinalizacao == R2 )
            {

                if(R2_State == R2_GET_ANI)
                {
					Accept();
				}
						
				if(R2_State == R2_SEND_GROUP_B)
                {

					// Verifica se a ligacao sera cobrada ou nao
					if(bBilling){
						// Informa que a ligacao SERA COBRADA
						CtiLog("Send B1");
						R2_PlayBackward(SIG_1); // Envia SIG_B1
					}
					else{
						// Informa que a ligacao NAO SERA COBRADA
						CtiLog("Send B5");
						R2_PlayBackward(SIG_5); // Envia SIG_B5
					}

					do {
						Event = GetEvent();
					}while(Event == -1);

					switch(Event){
								
						case T_ACCEPT:
							// Espera um tempo antes de atender para nao confundir a CENTRAL									
							Sleep(R2_TIME_TO_SEND_AOFF);

							if(bDoubleAnswer){
								CtiLog("Send DoubleAnswer");
								// Faz o duplo atendimento
								CasSetBit(AOFF);
								Sleep(1000);
								CasSetBit(AON);
								Sleep(1000);
								CasSetBit(AOFF);
							}
							else{
								// Atende a ligacao
								CtiLog("Send Answer");
								CasSetBit(AOFF);
							}

							#ifdef _ASR
							// Avisa ao engine de reconhecimento q uma chamada chegou.
							ASRSetCallConnected( true );
							#endif // _ASR
									
						return T_ANSWER;	

						case T_DISCONNECT:
							longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
						break;	
					}
				}

				// Espera um tempo antes de atender para nao confundir a CENTRAL									
				Sleep(R2_TIME_TO_SEND_AOFF);

				if(bDoubleAnswer){
					// Faz o duplo atendimento
					CasSetBit(AOFF);
					Sleep(1000);
					CasSetBit(AON);
					Sleep(1000);
					CasSetBit(AOFF);
				}
				else{
					// Atende a ligacao
					CasSetBit(AOFF);
				}

#ifdef _ASR
				// Avisa ao engine de reconhecimento q uma chamada chegou.
				ASRSetCallConnected( true );
#endif // _ASR                
				return T_ANSWER;
                
            } 
            else if (  (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) || 
                        (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS) )
            {
                int rc = 0;

                int state = 0;

                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

                // Se o estado da chamada e OFFERED, aceita a chamada antes de atender
				if (state == GCST_OFFERED)
                {
					Accept();
				}

				// Se o estado da chamada e ACCEPTED, atende, caso contrario espera
				// por evento de desconexao, pois a chamada ja foi encerrada
				if(state == GCST_ACCEPTED)
                {
					if(cc_AnswerCall(crn, 0, EV_ASYNC) != GC_SUCCESS) 
                    {
						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					}				
                }

				do 
                {
					Event = GetEvent();
				} while(Event == -1);

				switch(Event)
                {
					case T_ANSWER:

#ifdef _ASR
						// Avisa ao engine de reconhecimento q uma chamada chegou.
						ASRSetCallConnected( true );
#endif // _ASR

						rc = T_ANSWER;	
                        break;

					case T_DISCONNECT:
						if(cc_DropCall(crn, NORMAL_CLEARING, EV_ASYNC) != 0)
                        {
						}
						do 
                        {
							Event = GetEvent();
						} while(Event != -1 && Event != T_DROPCALL);
								
						if(cc_ReleaseCall(crn) != GC_SUCCESS)
                        {
						}

						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					    break;	
				}
                
                return rc;
            } 
            else if (   (TipoSinalizacao == GLOBAL_CALL) ||  (TipoSinalizacao == GLOBAL_CALL_IP) || 
                        (TipoSinalizacao == GLOBAL_CALL_SS7) || (TipoSinalizacao == GLOBAL_CALL_DM3) )
            {
                int state = 0;

                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

                if (ldev == ipdev)
				{
					IPSetCapabilities();
					VoxScListen(TimeslotLineIP);
					ScListen(TimeslotVox);
				}

				// Se a ligacao ainda nao foi aceita, chama Accept().                
                if (state == GCST_OFFERED) 
                {
					rc = Accept( bDoubleAnswer, bBilling );
					if( rc != T_ACCEPT )
					{
						return rc;
					}
                }
				
                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

				// Se o estado da chamada e ACCEPTED, atende, caso contrario espera por evento
				// de desconexao, pois a chamada ja foi encerrada
				if(state == GCST_ACCEPTED)
				{
					// Verifica se a ligacao sera cobrada ou nao
					if( !bBilling )
					{
						// Se nao deve ser cobrada altera a configuracao padrao de cobrar.
						if( gc_SetBilling( crn, GCR_NOCHARGE, NULL, EV_ASYNC ) != GC_SUCCESS )
						{
							PutError( "gc_SetBilling() Error in Dialogic::Answer(). %s.", gc_GetStringError() );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						do
						{
							Event = GetEvent( 5000 );
						} while( Event != -1 && Event != T_SETBILLING && Event != T_DISCONNECT && Event != T_TASKFAIL );
						if( Event == T_DISCONNECT )
						{
							return (int)T_DISCONNECT;
						}
					}

					// Atende a ligacao
					CtiLog("gc_AnswerCall()");
					if( gc_AnswerCall( crn, 0, EV_ASYNC ) != GC_SUCCESS )
					{
						PutError( "gc_AnswerCall() Error in Dialogic::Answer(). %s.", gc_GetStringError() );
						CtiLog( "%s", GetLastError() );
						return -1;
					}
					do
					{
						Event = GetEvent( 30000 );
					} while( Event != -1 && Event != T_ANSWER && Event != T_DISCONNECT && Event != T_TASKFAIL );

					switch(Event)
					{
						case -1:
							// Timeout. Nao veio o evento esperado.
							PutError( "Timeout waiting event in gc_AnswerCall()." );
							CtiLog( "%s", GetLastError() );
							return -1;
						case T_ANSWER:
							CtiLog("Event T_ANSWER");
							if(ldev == dtidev)
							{
								if( TipoSinalizacao == GLOBAL_CALL_SS7 )
								{
									if( bDoubleAnswer )
									{
										rc = MakeDoubleAnswerGCSS7();
										if( rc )
										{
											return rc;
										}
									}
								}
								else if( TipoSinalizacao == GLOBAL_CALL_DM3 )
								{
									if( ( TipoProtocolo == "SS7_dk" ) || ( TipoProtocolo == "SS7_dti" ) )
									{
										if( bDoubleAnswer )
										{
											rc = MakeDoubleAnswerGCSS7();
											if( rc )
											{
												return rc;
											}
										}
									}
								}

								#ifdef _ASR
								// Avisa ao engine de reconhecimento q uma chamada chegou.
								ASRSetCallConnected( true );
								#endif // _ASR

#ifndef _ATENTO
								ScConnect(FULLDUP);
#endif // _ATENTO
							}

							return (int)T_ANSWER;	

						case T_DISCONNECT:
							return (int)T_DISCONNECT;
                        default:
                            return -1;
					}
				}                
                return (int)Event;
            }
            else 
            {
                return -1;
            }
        } 
        else if ( TipoInterface == LSI )
        {
#ifdef _ASR
			// Avisa ao engine de reconhecimento q uma chamada chegou.
			ASRSetCallConnected( true );
#endif // _ASR

			ScConnect(FULLDUP);        
            
            if( !dx_sethook(Vox, DX_OFFHOOK, EV_SYNC) )
                return (int)T_ANSWER;
            else
				return 0;
        } else {
            return (int)T_ANSWER;
        }
        
	}
	catch(...)
	{
		PutError("Exception Dialogic::Answer()");
		return -1;
	}
	return -1;
}

/********************************************************************************************
	NOME:	Accept

	DESCRICAO:	Aceita uma chamada antes do atendimento propriamente

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::Accept(bool bDoubleAnswer, bool bBilling)
{
	int Event;
    int iRet = 0;
    
    m_pLog4cpp->debug("[%p][%s] ", this , __FUNCTION__);

	try {

        if ( TipoInterface == E1 || TipoInterface == IP || TipoInterface == T1 )
        {
            if ( TipoSinalizacao == LINE_SIDE )
            {
                return (int)T_ACCEPT;
            } 
            else if ( TipoSinalizacao == R2_DIGITAL )
            {
                R2Accept(bDoubleAnswer, bBilling);
                return (int)T_ACCEPT;
            } 
            else if ( TipoSinalizacao == R2 )
            {
                int lEvent = 0;
                if(DnisVariavel && !R2_AckCall)
                {
					// O Dnis e variavel esta configurado, mas a aplicacao nao chamou a funcao CallAck()
					// para pegar mais numeros de B, portanto vai para fim de selecao
					// Numero de DNIS completado
					R2_State = R2_SENT_A3; // Seta o estado do canal para enviou o sinal A3
					// Preparar para recepcao dos sinais do GRUPO B
					R2_PlayBackward(SIG_3); // Envia SIG_A3

					do {
						lEvent = GetEvent();
					}while(lEvent == -1);

					switch(lEvent){

						case T_CALL:
							// Verifica se a ligacao sera cobrada ou nao
							if(bBilling)// Informa que a ligacao SERA COBRADA
								R2_PlayBackward(SIG_1); // Envia SIG_B1
							else // Informa que a ligacao NAO SERA COBRADA
								R2_PlayBackward(SIG_5); // Envia SIG_B5

							do {
								lEvent = GetEvent();
							}while(lEvent == -1);

							switch(lEvent){
										
								case T_ACCEPT:
									return (int)T_ACCEPT;	

								case T_DISCONNECT:
									longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
								break;	
							}
						break;	

						case T_DISCONNECT:
							longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
						break;	
					}
				}
				else{
					// Verifica se a ligacao sera cobrada ou nao
					if(bBilling)// Informa que a ligacao SERA COBRADA
						R2_PlayBackward(SIG_1); // Envia SIG_B1
					else // Informa que a ligacao NAO SERA COBRADA
						R2_PlayBackward(SIG_5); // Envia SIG_B5

					do {
						lEvent = GetEvent();
					}while(lEvent == -1);

					switch(lEvent){
								
						case T_ACCEPT:
							return (int)T_ACCEPT;	

						case T_DISCONNECT:
							longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
						break;	
					}
				}

                return lEvent;
            } 
            else if (  (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) || 
                        (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS) )
            {
                // Se o estado da chamada e OFFERED, atende, caso contrario espera por evento
				// de desconexao, pois a chamada ja foi encerrada
                int state = 0;

                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

				if ( state == GCST_OFFERED )
                {
					if(cc_AcceptCall(crn, 0, EV_ASYNC) != GC_SUCCESS) {
						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					}
				}
				do {
					Event = GetEvent();
				}while(Event == -1);

				switch(Event){

					case T_ACCEPT:
						return (int)T_ACCEPT;	

					case T_DISCONNECT:
						if(cc_DropCall(crn, NORMAL_CLEARING, EV_ASYNC) != 0)
                        {
						}
						do {
							Event = GetEvent();
						}while(Event != -1 && Event != T_DROPCALL);
								
						if(cc_ReleaseCall(crn) != GC_SUCCESS)
                        {
						}

						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					break;	
				}
                
                return (int)T_NO_EVENTS;
            }
            else if ( (TipoSinalizacao == GLOBAL_CALL) || (TipoSinalizacao == GLOBAL_CALL_IP) || 
                       (TipoSinalizacao == GLOBAL_CALL_SS7) || (TipoSinalizacao == GLOBAL_CALL_DM3)  )
            {
                int state = 0;

                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

                // Se o estado da chamada e OFFERED, atende, caso contrario espera por evento
				// de desconexao, pois a chamada ja foi encerrada				
                if ( state == GCST_OFFERED )
				{
					// Verifica se a ligacao sera cobrada ou nao
					if( !bBilling )
					{
						// Se nao deve ser cobrada altera a configuracao padrao de cobrar.
						if( gc_SetBilling( crn, GCR_NOCHARGE, NULL, EV_ASYNC ) != GC_SUCCESS )
						{
							PutError( "gc_SetBilling() Error in Dialogic::Accept(). %s.", gc_GetStringError() );
							CtiLog( "%s", GetLastError() );
							return -1;
						}                       
                        
						do
						{
							Event = GetEvent( 5000 );
						} while( Event != -1 && Event != T_SETBILLING && Event != T_DISCONNECT && Event != T_TASKFAIL );
						switch( Event )
						{
							case -1:
								// Timeout. Nao veio o evento esperado.
								PutError( "Timeout waiting event in gc_SetBilling()." );
								CtiLog( "%s", GetLastError() );
								break;
							case T_DISCONNECT:
								return (int)T_DISCONNECT;
						}
                        
					}
                           
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if( kvCallStats.second == GCST_OFFERED)                            
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_ACCEPTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to CHANNEL_ACCEPTED", this, __FUNCTION__ , kvCallStats.first);  

                            if( gc_AcceptCall( kvCallStats.first, 0, EV_ASYNC ) != GC_SUCCESS )
							{
								PutError( "gc_AcceptCall() Error in Dialogic::Accept(). %s.", gc_GetStringError() );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
                        }
                    });                            

					do
					{
						Event = GetEvent( 30000 );
					} while( Event != -1 && Event != T_ACCEPT && Event != T_DISCONNECT && Event != T_TASKFAIL );
					switch( Event )
					{
						case -1:
							// Timeout. Nao veio o evento esperado.
							PutError( "Timeout waiting event in gc_AcceptCall()." );
							CtiLog( "%s", GetLastError() );
							return -1;
						case T_DISCONNECT:
							return (int)T_DISCONNECT;
                        case T_ACCEPT:
                            return (int)T_ACCEPT;        
					}
                    
				}
                return (int)T_NO_EVENTS;
            } 
            else 
            {
                return (int)T_ACCEPT;
            }
        } else {
            return (int) T_ACCEPT;
        }		
	}
	catch(...)
	{
		PutError("Exception Dialogic::Accept()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	AckCall

	DESCRICAO:	Pega mais numero de Dnis

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::CallAck(int NumberOfDnis)
{
    std::stringstream ss;
    int state = 0;
    int iRet = 0;

	try {

        if ( (TipoInterface == E1) || (TipoInterface == IP) || (TipoInterface = T1) )
        {

            if ( TipoSinalizacao == LINE_SIDE )
            {
                return (int)T_ACKCALL;
            } 
            else if ( TipoSinalizacao == R2_DIGITAL )
            {
                if( R2GetCompleteDnis(NumberOfDnis) )
                {
					return -1;
				}
				else
                {
					return 0;
				}
            }
            else if ( TipoSinalizacao == R2 )
            {
                int lEvent = 0;
                if(!DnisVariavel || NumberOfDnis <= 0)
                {
					// Se nao foi configurado DNIS variavel ou o numero de DNIS a mais
					// para pegar for <= 0, retorna
					return (int)T_ACKCALL;
				}
				// Flag indicando se sera necessario pegar mais numero de B
				R2_AckCall = true;
				// Guarda o numero de DNIS a mais desejado
				NumMoreDnis = NumberOfDnis;
				// Seta o estado canal para continuar pegando numero de b
				R2_State = R2_GET_DNIS;
				// Pede o proximo numero de B
				R2_PlayBackward(SIG_1); // Envia SIG_A1

				do {
					lEvent = GetEvent();
				}while(lEvent == -1);

				switch(Event){
							
					case T_ACKCALL:
						return (int)T_ACKCALL;

					case T_DISCONNECT:
						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					break;	
				}
                return lEvent;
            }
            else if (  (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) || 
                        (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS) )
            {
                return (int)T_ACKCALL;
            }
            else if ( TipoSinalizacao == GLOBAL_CALL_IP ) 
            {
                return (int)T_ACKCALL;
            }
            else if (  (TipoSinalizacao == GLOBAL_CALL)  || 
                        (TipoSinalizacao == GLOBAL_CALL_SS7) || (TipoSinalizacao == GLOBAL_CALL_DM3) )
            {
                int lEvent = 0;
                // Se o estado da chamada e OFFERED, pede mais numero de B,
				// caso contrario espera por evento de desconexao, pois a chamada ja foi encerrada
                if( gc_GetCallState( crn, &state ) != GC_SUCCESS )
                    return -1;

				if(state == GCST_OFFERED)
                {
					gc_callack.type									= GCACK_SERVICE_DNIS;
					gc_callack.service.dnis.accept	= NumberOfDnis;

					if(gc_CallAck(crn, &gc_callack, EV_ASYNC) != GC_SUCCESS)
                    {
						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					}
				}
				do {
					lEvent = GetEvent();
				}while(lEvent == -1);

				switch(lEvent)
                {
							
					case T_ACKCALL:
						memset(Dnis,			0, sizeof(Dnis));
						memset(Ani,				0, sizeof(Ani));
						memset(CallInfo,	0, sizeof(CallInfo));
						// Pega o numero de B
						if(gc_GetDNIS(crn, Dnis) != GC_SUCCESS){
						}
						// Pega o numero de A
						if(gc_GetANI(crn, Ani) != GC_SUCCESS){
						}
						// Pega a categoria do chamador
						if(gc_GetCallInfo(crn, CATEGORY_DIGIT, CallInfo) != GC_SUCCESS){
						}
					    return (int)T_ACKCALL;

					case T_DISCONNECT:                        
                        for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStatus) 
                        {
                            if ( kvCallStatus.second == GCST_OFFERED )
                            {
                                mapCrnToStatus[kvCallStatus.first] = GCST_IDLE;                            
                                m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStatus.first);  

                                if( gc_DropCall( kvCallStatus.first, GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                                {
				                    PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
				                    CtiLog( "%s", GetLastError() );
				                    return -1;
			                    }
                            }
                        });
							
						longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
					break;	
				}
                return lEvent;
            }
            else 
            {
                return (int)T_ACKCALL;
            }

        } else {
            return (int) T_ACKCALL;
        }        
	}
	catch(...)
	{
		PutError("Exception Dialogic::AckCall()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	Dial

	DESCRICAO:	Faz uma ligacao sicronamente, a funcao controla os eventos

	PARAMETROS:	char *Dnis				- Numero de destino
							char *Ani					- Numero do chamador
							int TimeConnect		- Tempo para atender em segundos (parametro default = 60)
							bool CallProgress	- true se tem CallProgress, false se nao tem (so para VOX/LSI)
																	(parametro default = false)

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::Dial(char *Dnis, char *Ani, int TimeConnect, bool CallProgress)
{
	try {
	
		iDirection = CH_OUTBOUND;

		strcpy(Dialogic::Dnis,	Dnis);	// Numero a ser chamado
		strcpy(Dialogic::Ani,		Ani);		// Numero do chamador

		switch(TipoInterface){
			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
						return(DialLineSide(Dialogic::Dnis));
					break;

					case R2:
					//	return(DialR2());
					break;	
				
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
				//		return(DialISDN());
					break;	
					
					case GLOBAL_CALL:
					case GLOBAL_CALL_DM3:
					//	return(DialGC());
					break;
					
					case GLOBAL_CALL_IP:
					//	return(DialIP());
					break;	
				}
			break;
			
			case LSI:
				return(DialLSI(Dnis, CallProgress));
			break;

			case VOX:
				return(DialVox(Dnis, CallProgress));
			break;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::Dial()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	DialAsync

	DESCRICAO:	Faz uma ligacao assincronamente, a thread que a chamou deve pegar os eventos

	PARAMETROS:	char *Dnis				- Numero de destino
							char *Ani					- Numero do chamador
							bool CallProgress	- true se tem CallProgress, false se nao tem (so para VOX/LSI)
																	(parametro default = false)

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::DialAsync(char *Dnis, char *Ani, bool CallProgress)
{
	int iRet, Event;

	try {

		iDirection = CH_OUTBOUND;

		strcpy(Dialogic::Dnis,	Dnis);	// Numero a ser chamado
		strcpy(Dialogic::Ani,	Ani);		// Numero do chamador

		switch(TipoInterface){
		
			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
					break;

					case R2:
					//	iRet = DialR2Async();
					break;	

					case R2_DIGITAL:
						iRet = DialR2Async();
					break;	
				
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
						iRet = DialISDNAsync();
					break;	
					
					case GLOBAL_CALL:
						iRet = DialGCAsync();
					break;	
					case GLOBAL_CALL_DM3:
						if( strstr(TipoProtocolo.c_str(), "SS7") != NULL )
						{
							iRet = DialGCSS7Async();
						}
						else
						{
							iRet = DialGCAsync();
						}
					break;	

					case GLOBAL_CALL_SS7:
						iRet = DialGCSS7Async();
					break;
					
					case GLOBAL_CALL_IP:
						iRet = IPDialAsync(Dnis,Ani);
					break;	
				}
			break;
			
			case LSI:
				iRet = DialLSIAsync(Dnis, CallProgress);
			break;

			case VOX:
//				iRet = DialVoxAsync(Dnis, CallProgress);
			break;

			default:
				return -1;
		}

		do
		{
			Event = GetEvent();
		} while( Event == -1 );

		return Event;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialAsync()");
		return -1;
	}
	return -1;
}

/********************************************************************************************
	NOME:	BlindTransfer

	DESCRICAO:	Faz "transferencia cega"

	PARAMETROS:	const char *lpcszNumber		Numero de destino da transferencia
							int iTime									Tempo em segundos para fazer o blindtransfer

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::BlindTransfer( const char *lpcszNumber, int iTime )
{
	try {
		int iRet = 0;

		iDirection = CH_OUTBOUND;

		strcpy(Dialogic::Dnis,	lpcszNumber);	// Numero a ser chamado
	
		switch(TipoInterface){
			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
						iRet = LineSideTransfer(Dnis);
						return 0;

					case R2:
					break;	
				
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
						break;
					
					case GLOBAL_CALL:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						iRet = BlindTransferGC(Dnis, iTime);
					break;
					
					case GLOBAL_CALL_IP:
						iRet = BlindTransferIP(Dnis);
					break;	
				}
			break;
			
			case LSI:
					break;	
		}
		return iRet;
	}
	catch(...)
	{
		PutError("Exception Dialogic::BlindTransfer()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	OnHook

	DESCRICAO:	Coloca a linha no gancho

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::OnHook(void)
{
	try {
	
		switch(TipoInterface){
			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
						CasSetBit(AOFF);
						return 0;

					case R2:
					break;	
				
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
					
					case GLOBAL_CALL:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_DM3:
					break;	
				}
			break;
			
			case LSI:
				ScConnect(FULLDUP);
				return(dx_sethook(Vox, DX_ONHOOK, EV_SYNC));
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::OnHook()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	OffHook

	DESCRICAO:	Coloca a linha fora do gancho

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::OffHook(void)
{
	try {
	
		switch(TipoInterface){
			case E1:
			case IP:
			case T1:
				switch(TipoSinalizacao){

					case LINE_SIDE:
						CasSetBit(AON);
						return 0;

					case R2:
					break;	
				
					case ISDN:
					case ISDN_4ESS:
					case ISDN_NTT:
					case ISDN_QSIG:
					case ISDN_CTR4:
					case ISDN_DMS:
					break;	
					
					case GLOBAL_CALL:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_DM3:
					break;	
				}
			break;
			
			case LSI:
				ScConnect(FULLDUP);
				return(dx_sethook(Vox, DX_OFFHOOK, EV_SYNC));
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::OffHook()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	GetDnis

	DESCRICAO:	Retorna em DnisNumber numero de destino ou DNIS ou NULL se nao existir

	PARAMETROS:

	RETORNO:
********************************************************************************************/
const char *Dialogic::GetDnis()
{
	try
	{
		return Dnis;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetDnis()");
	}
}
/********************************************************************************************
	NOME:	GetAni

	DESCRICAO:	Retorna em AniNumber numero do chamador ou NULL se nao existir

	PARAMETROS:

	RETORNO:
********************************************************************************************/
const char *Dialogic::GetAni()
{
	try
	{
		return Ani;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetAni()");
	}
}
/********************************************************************************************
	NOME:	GetCallInfo

	DESCRICAO:	Retorna em Info a categoria do chamador ou NULL se nao existir

	PARAMETROS:

	RETORNO:
********************************************************************************************/
const char *Dialogic::GetCallInfo()
{
	try
	{
		return CallInfo;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetCallInfo()");
	}
}
/********************************************************************************************
	NOME:	GetChannel

	DESCRICAO:	Retorna o canal do objeto

	PARAMETROS:

	RETORNO:	Canal
********************************************************************************************/
int Dialogic::GetChannel(void)
{
	return Canal;
}

/********************************************************************************************
	NOME:	GetSite

	DESCRICAO:	Retorna o n�mero do Site

	PARAMETROS:

	RETORNO: Site
********************************************************************************************/
int Dialogic::GetSite(void)
{
	try
	{
		return Site;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetSite()");
	}
}

/********************************************************************************************
	NOME:	GetMachine

	DESCRICAO:	Retorna o n�mero da m�quina

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::GetMachine(void)
{
	try
	{
		return Machine;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetMachine()");
	}
}

/********************************************************************************************
	NOME:	GetDirection

	DESCRICAO:	Retorna a dire��o da chamada (inbound/outbound/dual)

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::GetDirection(void)
{
	try
	{
		return iDirection;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetDirection()");
	}
}

/********************************************************************************************
	NOME:	GetLocalParam

	DESCRICAO:	Retorna o Parametro local usado pela funcao SendEvent

	PARAMETROS:

	RETORNO:	Canal
********************************************************************************************/
int Dialogic::GetLocalParam(void)
{
	return LocalParam;
}
/********************************************************************************************
	NOME:	GetEventString

	DESCRICAO:	Retorna a mesagem enviada por SendEvent

	PARAMETROS:

	RETORNO:	Canal
********************************************************************************************/
const char *Dialogic::GetEventString(void)
{
	return sEventString;
}
/********************************************************************************************
	NOME:	GetInterface

	DESCRICAO:	Retorna o tipo de interface do canal

	PARAMETROS:	

	RETORNO:	TipoInterface
********************************************************************************************/
int Dialogic::GetInterface(void)
{
	return TipoInterface;
}
/********************************************************************************************
	NOME:	GetDevice

	DESCRICAO:	Retorna o device manipulado pelo objeto

	PARAMETROS:	int Interface	- Tipo de Interface do device (MSI, LSI, VOX, DTI, etc...)

	RETORNO:	Device
						-1 erro
********************************************************************************************/
int Dialogic::GetDevice(int Interface)
{
	switch(Interface){

		case E1:
		case T1:
		case DTI:
			return Dti;

		case IP:
			return ipdev;
		
		case LSI:
			return Vox;

		case MSI:
			return Msi;

		case VOX:
			return Vox;

		case FAX:
			return Fax;

		case DEV:
			return ldev;

		default:
			return -1;
	}
}
/********************************************************************************************
	NOME:	SetTimeToWaitEvent

	DESCRICAO:	Seta o tempo que o sistema deve esperar por um evento de telefonia

	PARAMETROS:	int Time - Tempo em (ms) ou -1 se infinito

	RETORNO:
********************************************************************************************/
void Dialogic::SetTimeToWaitEvent(int Time)
{
	TimeToWaitEvent = Time;
}
/********************************************************************************************
	NOME:	GetTimeToWaitEvent

	DESCRICAO:	Retorna o tempo que o sistema deve esperar por um evento de telefonia
							ou -1 se infinito

	PARAMETROS:

	RETORNO:		Retorna o tempo que o sistema deve esperar por um evento de telefonia
							ou -1 se infinito
********************************************************************************************/
int Dialogic::GetTimeToWaitEvent(void)
{
	return TimeToWaitEvent;
}
/********************************************************************************************
	NOME:	GetReasonDisconnect

	DESCRICAO:	Retorna uma string com o motivo da desconexao

	PARAMETROS:

	RETORNO:		Retorna uma string com o motivo da desconexao
********************************************************************************************/
char *Dialogic::GetReasonDisconnect(void)
{
	return sReasonDisconnect;
}

/********************************************************************************************
	NOME:	GetDisconnectReasonCode

	DESCRICAO:	Retorna o codigo do motivo da desconexao

	PARAMETROS:

	RETORNO:		Retorna o codigo do motivo da desconexao
********************************************************************************************/
int Dialogic::GetDisconnectReasonCode(void)
{
	return DisconnectReasonCode;
}
/********************************************************************************************
	NOME:	GetTxTimeSlot

	DESCRICAO:	Retorna o TimeSlot de transmissao dom SC_BUS de acordo com o tipo
							de device

	PARAMETROS:	int DeviceType - Tipo de device para obter o timeslot

	RETORNO:		TimeSlot ou -1 se erro
********************************************************************************************/
int Dialogic::GetLineTxTimeSlot(int DeviceType)
{
	long tslot;

	try {

		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp = &tslot;

		switch(DeviceType){

			case LSI:
				if(ag_getxmitslot(Vox, &sc_tsinfo) == -1){
					PutError("ag_getxmitslot() error in function GetLineTxTimeSlot() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					return -1;
				}
			break;
			case DTI:
				if(dt_getxmitslot(Dti, &sc_tsinfo) == -1){
					PutError("dt_getxmitslot() error in function GetLineTxTimeSlot() Channel %d: %s", Canal, ATDV_ERRMSGP(Dti));
					return -1;
				}
			break;
			case MSI:
				if(ms_getxmitslot(Msi, &sc_tsinfo) == -1){
					PutError("ms_getxmitslot() error in function GetLineTxTimeSlot() Channel %d: %s", Canal, ATDV_ERRMSGP(Msi));
					return -1;
				}
			break;
			case VOX:
				if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
					PutError("dx_getxmitslot() error in function GetLineTxTimeSlot() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					return -1;
				}
			break;
			case FAX:
				if(fx_getxmitslot(Fax, &sc_tsinfo) == -1){
					PutError("fx_getxmitslot() error in function GetLineTxTimeSlot() Channel %d: %s", Canal, ATDV_ERRMSGP(Fax));
					return -1;
				}
			break;
		}

		return tslot;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetLineTxTimeSlot()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	GetTimeSlot

	DESCRICAO: Retorna o timeslot de transmissao atribuido ao canal na sua abertura

	PARAMETROS:

	RETORNO: TxTimeSlot
********************************************************************************************/
int Dialogic::GetTimeSlot(void)
{
    int iRet = 0;    

	if(dtidev < 0)
    {        
		iRet = TimeslotLineIP;
    } else {
	    if(FlagCallIP)
        {
		    iRet = TimeslotLineIP;            
        }
	    else
        {
		    iRet = TimeslotLine;            
        }
    }

    //m_pLog4cpp->debug("[%p][%s] return = %d", this, __FUNCTION__ , iRet);
    return iRet;
}
/********************************************************************************************
	NOME:	GetTimeSlotVox

	DESCRICAO: Retorna o timeslot de voz atribuido ao canal na sua abertura

	PARAMETROS:

	RETORNO: TimeslotVox
********************************************************************************************/
int Dialogic::GetTimeSlotVox(void)
{
	return TimeslotVox;
}
/********************************************************************************************
	NOME:	GetEventDev

	DESCRICAO: Retorna o device onde ocorreu um evento pela funcao WaitEventEx()

	PARAMETROS:

	RETORNO:	Device
********************************************************************************************/
int Dialogic::GetEventDev(void)
{
	return Dev;
}
/********************************************************************************************
	NOME:	GetEventHandle

	DESCRICAO: Retorna o manipulador de eventos gerado pela funcao WaitEventEx()

	PARAMETROS:

	RETORNO:	Device
********************************************************************************************/
int Dialogic::GetEventHandle(void)
{
	return EventHandle;
}
/********************************************************************************************
	NOME:	ClearEventQueue

	DESCRICAO: Limpa a fila de eventos

	PARAMETROS: Nenhum

	RETORNO:	Nenhum
********************************************************************************************/
void Dialogic::ClearEventQueue(void)
{
	int TimeWait;

	try {

		// guarda o intervalo de tempo atual			
		TimeWait = GetTimeToWaitEvent();
		// seta o intervalo de tempo para esperar eventos para 100 ms
		SetTimeToWaitEvent(100);
		// Pega eventos ate esvaziar a fila
		while( GetEvent() != -1 );
		// restaura o intervalo de tempo	
		SetTimeToWaitEvent(TimeWait);
	}
	catch(...)
	{
		PutError("Exception Dialogic::ClearEventQueue()");
		// restaura o intervalo de tempo para valor default
		SetTimeToWaitEvent(1000);
	}
}
/********************************************************************************************
	NOME:	AlreadyDisconnected

	DESCRICAO: Retorna true se o usuario ja desligou, ou false se ainda nao desligou

	PARAMETROS:

	RETORNO:	Device
********************************************************************************************/
bool Dialogic::AlreadyDisconnected(void)
{
	return FlagDisconnected;
}
/********************************************************************************************
	NOME:	HangupToneDetetion

	DESCRICAO: Habilita ou desabilita o tom de hangup

	PARAMETROS:	bool bFlag - true = habilita o tom de hangup, false = desabilita

	RETORNO:	Device
********************************************************************************************/
int Dialogic::HangupToneDetetion(bool bFlag)
{
	try {
	
		if( bFlag	){

			if(dx_enbtone(Vox, DefHangup1.id_tone, DM_TONEON) != 0){
				PutError("dx_enbtone() Error in function Dialogic::EnableHangupTone: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
			if(dx_enbtone(Vox, DefHangup2.id_tone, DM_TONEON) != 0){
				PutError("dx_enbtone() Error in function Dialogic::EnableHangupTone: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		else{

			if(dx_distone(Vox, DefHangup1.id_tone, DM_TONEON) != 0){
				PutError("dx_distone() Error in function Dialogic::EnableHangupTone: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
			if(dx_distone(Vox, DefHangup2.id_tone, DM_TONEON) != 0){
				PutError("dx_distone() Error in function Dialogic::EnableHangupTone: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::HangupToneDetetion()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsgBoxBeepDetection

	DESCRICAO: Habilita ou desabilita o tom do Beep da Caixa Postal para BCP e TIM

	PARAMETROS:	bool bFlag - true = habilita o tom do beep, false = desabilita

	RETORNO:	Device
********************************************************************************************/
int Dialogic::MsgBoxBeepDetection(bool bFlag)
{
	try {
	
		if( bFlag	)
		{

			if(dx_enbtone(Vox, DefFS5.id_tone, DM_TONEON) != 0)
			{
				PutError("dx_enbtone() Error in function Dialogic::EnableMsgBoxBeep: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		else
		{

			if(dx_distone(Vox, DefFS5.id_tone, DM_TONEON) != 0)
			{
				PutError("dx_distone() Error in function Dialogic::DisableMsgBoxBeep: %s", ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsgBoxBeepDetection()");
		return -1;
	}
}

// Parameters:
//	lpcszCallId	- [in] Pointer to a null-terminated string specifying the call ID.
// Return Value:
//	The return value is zero if the function succeeds or non-zero if the function fails.
// Description:
//	This function is used to set the call ID generated by an external implementation.
//	This call ID is used for internal purposes.
int Dialogic::SetCallId( const char* lpcszCallId )
{
	CallId = lpcszCallId;
	return 0;
}

// Parameters:
//	None.
// Return Value:
//	The return value is a null-terminated string specifying the call ID.
// Description:
//	This function is used to get the call ID generated by an external implementation.
//	This call ID is used for internal purposes.
const char* Dialogic::GetCallId()
{
	return CallId.c_str();
}

void Dialogic::JumpExit(void)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	if( JumpBuffer != NULL ){
		longjmp(*JumpBuffer, JUMP_EXIT);
	}
}

/********************************************************************************************
	NOME:	ProcVox

	DESCRICAO:	Processa os eventos para o canal de voz

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::ProcVOX( int Time )
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {

		Devs[0] = Vox;
		Devs[1] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 1, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 1, Time, &EventHandle) == -1)
				return -1;
		}

		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case TDX_CST:                                                                                                                                         
				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;
				
				switch(Event){

					case DE_TONEON:	// recebeu tom de ocupado do pabx
					case DE_LCOFF:	// recebeu loop de corrente off
						FlagDisconnected = true;
						return T_DISCONNECT;

					case DE_RINGS:
						FlagDisconnected = false;
						iDirection = CH_INBOUND;
						return T_CALL;

					case DE_DIGITS:
						LastDigit = (char) Data; // guarda o digito recebido
						return T_DIGIT;
				}
			break;

			case TDX_DIAL:
				return T_DIAL;

			case TDX_CALLP:
				CpTerm = ATDX_CPTERM(Vox);

				switch(CpTerm){

					case CR_CEPT:
					case CR_FAXTONE:
					case CR_CNCT:		// atendeu
						return T_CONNECT;

					case CR_BUSY:		// linha ocupada
						return T_BUSY;

					case CR_NOANS:	// nao responde
						return T_NOANSWER;

					case CR_NODIALTONE: // sem tom de linha
						return T_NODIALTONE;

					case CR_STOPD:
					case CR_ERROR:			// erro na chamada
					case CR_NORB:				// sem ringback
						return T_ERROR;
				}
			break;

			case TDX_PLAY:
				return T_PLAY;

			case TDX_RECORD:
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_PLAYTONE:
				return T_PLAYTONE;

			case TDX_ERROR:
				return T_ERROR;

			default:
				return EventType;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcLSI()");
		return -1;
	}
	return -1;
}

/********************************************************************************************
	NOME:	PlayFileEvent

	DESCRICAO:	Toca um arquivo de voz no canal especificado

	PARAMETROS:	char *Arquivo		- Nome do arquivo de voz

	RETORNO:		PLAY se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::PlayFileEvent(const char *Arquivo)
{
	try {

		int Event;

		//if( _access( Arquivo, 0 ) )
        if ( !boost::filesystem::exists( Arquivo ) )
		{
			PutError("Cannot open file: %s in function PlayFileEvent() Channel %d", Arquivo, Canal);
			return -1;
		}

		// Aguarda se o recurso ja estiver tocando algo.
		if(WaitForPlayStopped() != 0)
			return T_DISCONNECT;

		if(dx_iott.io_fhandle > 0){
			dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			dx_iott.io_fhandle = -1;
		}

		dx_iott.io_fhandle = dx_fileopen(Arquivo, _O_RDONLY |_O_BINARY, _S_IREAD);//_O_RDONLY

		if(dx_iott.io_fhandle <= 0){
			dx_iott.io_fhandle = -1;
			PutError("Cannot open file: %s in function PlayFileEvent() Channel %d", Arquivo, Canal);
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		// le do disco
		dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		dx_iott.io_offset	= 0;
		dx_iott.io_bufp		= 0;
		dx_iott.io_length	= -1;
		dx_iott.io_nextp	= NULL;
		dx_iott.io_prevp	= NULL;

		// seta a mascara de digitos
		dx_clrtpt(&dv_tpt[0], 1);

		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[0].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[0].tp_length = (unsigned short)DigMask;

		// seta o formato do arquivo a reproduzir
		dx_xpb.wFileFormat    = PlayFileFormat;
		dx_xpb.wDataFormat    = PlayDataFormat;
		dx_xpb.nSamplesPerSec = PlaySamples;
		dx_xpb.wBitsPerSample = PlayBitsPerSample;

		if(dx_playiottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_ASYNC)){
			dx_fileclose(dx_iott.io_fhandle);
			dx_iott.io_fhandle = -1;
			PutError("dx_playiottdata() error in function PlayFileEvent() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		for(;;){
		
			Event = GetEvent();

			switch(Event){

				case T_DISCONNECT:
					StopCh();
					return T_DISCONNECT;

				case T_PLAY:
					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;
					return T_PLAY;

#if defined _APPLICATION_SOCKET
        case T_SW_CONF_MONITOR_REQUEST:
					if(StopCh() == T_DISCONNECT)
						return T_DISCONNECT;
					else
						return T_SW_CONF_MONITOR_REQUEST;
#endif // _APPLICATION_SOCKET

			}
		}
		
		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayFileEvent()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	PlayFile

	DESCRICAO:	Toca um arquivo de voz no canal especificado

	PARAMETROS:	char *Arquivo		- Nome do arquivo de voz
							char *TermChar	- Caracteres de termino
							long lOffset		- Offset (em bytes) de inicio do prompt

	RETORNO:		PLAY se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::PlayFile(const char *Arquivo, const char *TermChar, const long lOffset)
{
	try {
		int Event;

		//if( _access( Arquivo, 0 ) )
        if ( !boost::filesystem::exists( Arquivo ) )
		{
			PutError("Cannot open file: %s in function PlayFile() Channel %d", Arquivo, Canal);
			return -1;
		}

		// Aguarda se o recurso ja estiver tocando algo.
		if(WaitForPlayStopped() != 0)
			return T_DISCONNECT;

		if(dx_iott.io_fhandle > 0){
			dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			dx_iott.io_fhandle = -1;
		}

		dx_iott.io_fhandle = dx_fileopen(Arquivo, _O_RDONLY |_O_BINARY, _S_IREAD);

		if(dx_iott.io_fhandle <= 0){
			dx_iott.io_fhandle = -1;
			PutError("Cannot open file: %s in function PlayFile() Channel %d", Arquivo, Canal);
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		// le do disco
		dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		dx_iott.io_offset	= 0;
		dx_iott.io_bufp		= 0;
		dx_iott.io_length	= -1;
		dx_iott.io_nextp	= NULL;
		dx_iott.io_prevp	= NULL;

		// seta a mascara de digitos
		dx_clrtpt(&dv_tpt[0], 1);

		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[0].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[0].tp_length = (unsigned short)DigMask;

		// seta o formato do arquivo a reproduzir
		dx_xpb.wFileFormat    = PlayFileFormat;
		dx_xpb.wDataFormat    = PlayDataFormat;
		dx_xpb.nSamplesPerSec = PlaySamples;
		dx_xpb.wBitsPerSample = PlayBitsPerSample;

		if(dx_playiottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_ASYNC)){
			dx_fileclose(dx_iott.io_fhandle);
			dx_iott.io_fhandle = -1;
			PutError("dx_playiottdata() error in function PlayFile() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			CtiLog( "%s", GetLastError() );
			return -1;
		}
				
		for(;;){
		
			Event = GetEvent();

			switch(Event)
			{
				case T_DISCONNECT:
					StopCh();
					return T_DISCONNECT;

				case T_PLAY:
					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;
					return T_PLAY;

				case T_ERROR:
					dx_fileclose(dx_iott.io_fhandle);
					dx_iott.io_fhandle = -1;
					PutError("Error in function PlayFile() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					CtiLog(GetLastError());
					return -1;

				case T_SW_AUDIO_REQUEST:
					CtiLog( "Event: T_SW_AUDIO_REQUEST" );
          SendEvent(Canal, Event);
					return T_SW_AUDIO_REQUEST;

#if defined _APPLICATION_SOCKET
        case T_SW_CONF_MONITOR_REQUEST:
          SendEvent(Canal, Event);
          break;
#endif // _APPLICATION_SOCKET

			}
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayFile()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	PlayFileSync

	DESCRICAO:	Toca um arquivo de voz no canal especificado sincronamente sem pegar eventos

	PARAMETROS:	char *Arquivo		- Nome do arquivo de voz

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::PlayFileSync(const char *Arquivo)
{
	try {
		//if( _access( Arquivo, 0 ) )
        if ( !boost::filesystem::exists( Arquivo ) )
		{
			PutError("Cannot open file: %s in function PlayFileSync() Channel %d", Arquivo, Canal);
			return -1;
		}

		// Aguarda se o recurso ja estiver tocando algo.
		if(WaitForPlayStopped() != 0)
			return T_DISCONNECT;

		if(dx_iott.io_fhandle > 0){
			dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			dx_iott.io_fhandle = -1;
		}

		dx_iott.io_fhandle = dx_fileopen(Arquivo, _O_RDONLY |_O_BINARY, _S_IREAD);

		if(dx_iott.io_fhandle <= 0){
			dx_iott.io_fhandle = -1;
			PutError("Cannot open file: %s in function PlayFileSync() Channel %d", Arquivo, Canal);
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		// le do disco
		dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		dx_iott.io_offset	= 0;
		dx_iott.io_bufp		= 0;
		dx_iott.io_length	= -1;
		dx_iott.io_nextp	= NULL;
		dx_iott.io_prevp	= NULL;

		// seta a mascara de digitos
		dx_clrtpt(&dv_tpt[0], 1);


		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[0].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[0].tp_length = (unsigned short)DigMask;

		// seta o formato do arquivo a reproduzir
		dx_xpb.wFileFormat    = PlayFileFormat;
		dx_xpb.wDataFormat    = PlayDataFormat;
		dx_xpb.nSamplesPerSec = PlaySamples;
		dx_xpb.wBitsPerSample = PlayBitsPerSample;

		if(dx_playiottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_SYNC)){
			dx_fileclose(dx_iott.io_fhandle);
			dx_iott.io_fhandle = -1;
			PutError("dx_playiottdata() error in function PlayFileSync() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayFileSync()");
		return -1;
	}
}

int Dialogic::PlayFileAsync(const char *lpcszFilename, const char *lpcszTermDigit, const long lOffset)
{
	try {
		//if( _access( lpcszFilename, 0 ) )
        if ( !boost::filesystem::exists( lpcszFilename ) )
		{
			PutError("Cannot open file: %s in function PlayFileAsync() Channel %d", lpcszFilename, Canal);
			return -1;
		}

		// Aguarda se o recurso ja estiver tocando algo.
		if(WaitForPlayStopped() != 0)
			return T_DISCONNECT;

		if(dx_iott.io_fhandle > 0){
			dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			dx_iott.io_fhandle = -1;
		}

		dx_iott.io_fhandle = dx_fileopen(lpcszFilename, _O_RDONLY |_O_BINARY, _S_IREAD);

		if(dx_iott.io_fhandle <= 0){
			dx_iott.io_fhandle = -1;
			PutError("Cannot open file: %s in function PlayFileAsync() Channel %d", lpcszFilename, Canal);
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		// le do disco
		dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		dx_iott.io_offset	= 0;
		dx_iott.io_bufp		= 0;
		dx_iott.io_length	= -1;
		dx_iott.io_nextp	= NULL;
		dx_iott.io_prevp	= NULL;

		// seta a mascara de digitos
		dx_clrtpt(&dv_tpt[0], 1);

		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[0].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[0].tp_length = (unsigned short)DigMask;

		// seta o formato do arquivo a reproduzir
		dx_xpb.wFileFormat    = PlayFileFormat;
		dx_xpb.wDataFormat    = PlayDataFormat;
		dx_xpb.nSamplesPerSec = PlaySamples;
		dx_xpb.wBitsPerSample = PlayBitsPerSample;

		if(dx_playiottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_ASYNC)){
			dx_fileclose(dx_iott.io_fhandle);
			dx_iott.io_fhandle = -1;
			PutError("dx_playiottdata() error in function PlayFileAsync() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			CtiLog( "%s", GetLastError() );
			return -1;
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayFileAsync()");
		return -1;
	}
}

int Dialogic::PlayAsync(unsigned short Terminator, const long lOffset)
{																						
	try 
	{
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayAsync()");
		return -1;
	}
}

int Dialogic::Play(const long lOffset)
{
	try
	{
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::Play()");
		CtiLog(GetLastError());
		return -1;
	}
}

/********************************************************************************************
	NOME:	AddTTS

	DESCRICAO:	Adiciona um prompt de texto na lista de dados a serem tocados

	PARAMETROS:	const char *prompt		- String com o prompt

	RETORNO:		 0 se sucesso
	  					-1 se erro
********************************************************************************************/
int Dialogic::AddTTS(const char *prompt)
{
	try 
	{
		return 0;
	}

	catch(...)
	{
		PutError("Exception Dialogic::AddTTS()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	AddFile

	DESCRICAO:	Adiciona um arquivo de voz na lista de arquivos a serem tocados

	PARAMETROS:	const char *lpcszFilename		- Nome do arquivo de voz

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::AddFile(const char *lpcszFilename)
{
	try 
	{
		return 0;
	}

	catch(...)
	{
		PutError("Exception Dialogic::AddFile()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	AddBuffer

	DESCRICAO:	Adiciona um buffer em memoria contendo um arquivo de voz na lista de arquivos a serem tocados

	PARAMETROS:	const char *buffer		- Buffer contendo o arquivo de voz

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::AddBuffer(char *buffer, const unsigned long buffersize)
{
	try 
	{
		return 0;
	}

	catch(...)
	{
		PutError("Exception Dialogic::AddFile()");
		return -1;
	}
}

int Dialogic::PlayTTS (const char *lpcszText)
{
	try
	{
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayTTS()");
		CtiLog ( "%s", GetLastError() );
		return -1;
	}
}


int Dialogic::PlayTTSAsync(const char *lpcszText)
{
	try
	{
		return 0;
	}

	catch(...)
	{
		PutError("Exception Dialogic::PlayTTSAsync()");
		CtiLog ( "%s", GetLastError() );
		return -1;
	}
}


int Dialogic::ClearPrompts()
{
	try
	{
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::ClearPrompts()");
		return -1;
	}
}

int Dialogic::RecordFile(const char *Arquivo, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar)
{
	try
	{
		int Event, Ret, index;
		long Term;

    if(VoxState() != S_RECORD)
    {
			if((Arquivo == NULL) || (Arquivo[0] == '\0'))
			{
				PutError("Invalid filename in function RecordFile(), channel %d.", Canal);
				CtiLog( "%s", GetLastError() );
				return -1;
			}

			// Remove o arquivo.
			remove(Arquivo);

		  if(dx_iott.io_fhandle > 0){
			  dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			  dx_iott.io_fhandle = -1;
		  }

		  dx_iott.io_fhandle = dx_fileopen(Arquivo, _O_RDWR|_O_BINARY|_O_CREAT|_O_TRUNC, _S_IREAD|_S_IWRITE);

		  if(dx_iott.io_fhandle <= 0){
			  dx_iott.io_fhandle = -1;
			  PutError("Cannot open file: %s in function RecordFile() Channel %d", Arquivo, Canal);
				CtiLog( "%s", GetLastError() );
			  return -1;
		  }

		  // grava no disco
		  dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		  dx_iott.io_offset	= 0;
		  dx_iott.io_bufp		= 0;
		  dx_iott.io_length	= -1;
		  dx_iott.io_nextp	= NULL;

		  index = 0;

			if(TempoSilencio > 0)
			{
			  dx_clrtpt(&dv_tpt[0], 3);

				// seta o maximo de silencio
				dv_tpt[index].tp_type   = (unsigned short)IO_CONT;
			  dv_tpt[index].tp_termno = (unsigned short)DX_MAXSIL;
			  dv_tpt[index].tp_length = (unsigned short)(TempoSilencio * 10);
				dv_tpt[index].tp_flags  = (unsigned short)TF_MAXSIL;
				index++;
			}
			else
			  dx_clrtpt(&dv_tpt[0], 2);

		  // seta o tempo para gravar
		  dv_tpt[index].tp_type   = (unsigned short)IO_CONT;
		  dv_tpt[index].tp_termno = (unsigned short)DX_MAXTIME;
		  dv_tpt[index].tp_length = (unsigned short)(TempoGravar * 10);
		  dv_tpt[index].tp_flags  = (unsigned short)TF_MAXTIME;
			index++;

		  // seta a mascara de digitos
		  dv_tpt[index].tp_type   = (unsigned short)IO_EOT;
		  dv_tpt[index].tp_termno = (unsigned short)DX_DIGMASK;
		  dv_tpt[index].tp_flags  = (unsigned short)TF_DIGMASK;
		  dv_tpt[index].tp_length = (unsigned short)RetDigMaskTerm(TermChar);

		  // seta o formato do arquivo a gravar
		  dx_xpb.wFileFormat    = RecordFileFormat;	
		  dx_xpb.wDataFormat    = RecordDataFormat;	
		  dx_xpb.nSamplesPerSec = RecordSamples;			
		  dx_xpb.wBitsPerSample = RecordBitsPerSample;

		  if(bip)
			  Ret = dx_reciottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, PM_TONE|EV_ASYNC);
		  else
			  Ret = dx_reciottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_ASYNC);

		  if(Ret){
			  PutError("dx_reciottdata() error in function RecordFile() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				CtiLog( "%s", GetLastError() );
			  return -1;
		  }

		  TimeRecord = time(NULL);
    }

		for(;;){
		
			Event = GetEvent();

			switch(Event){

				case T_DISCONNECT:
					TimeRecord = time(NULL) - TimeRecord; // guarda o tempo de gravacao
					StopCh();
					return T_DISCONNECT;

				case T_RECORD:
					TimeRecord = time(NULL) - TimeRecord; // guarda o tempo de gravacao

					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;
					
					Term = ATDX_TERMMSK(Vox);

					if(Term == AT_FAILURE){
						return -1;
					}
					if(Term & TM_MAXSIL){
						return T_SILENCE;
					}
					return T_RECORD;

				case T_ERROR:
					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;
					PutError("dx_reciottdata() error in function RecordFile() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					CtiLog( "%s", GetLastError() );
					return -1;
			}
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RecordFile()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	RecordFileFrom2Src

	DESCRICAO:	Grava um arquivo de voz no canal especificado

	PARAMETROS:	char *Arquivo			- Nome do arquivo de voz
							int TempoGravar		- Tempo em segundos para gravar
							int TempoSilencio	- Tempo em segundos de silencio para parar de gravar
							char *TermChar		- Caracteres de termino
							bool bip					- Flag indicando se havera bip antes da gravacao
							long SecondarySrc	- Timeslot c/ 2o. sinal de entrada

	RETORNO:		RECORD		se terminou de gravar pelo tempo especificado
							SILENCIO	se terminou por silencio	
							-1 se erro
********************************************************************************************/
int Dialogic::RecordFileFrom2Src(const char *Arquivo, const long& SecondarySrc, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar)
{
	try
	{
		int Event, Ret, index;
		long Term;

    if(VoxState() != S_RECORD)
    {
			if((Arquivo == NULL) || (Arquivo[0] == '\0'))
			{
				PutError("Invalid filename in function RecordFile(), channel %d.", Canal);
				CtiLog( "%s", GetLastError() );
				return -1;
			}

			// Remove o arquivo.
			remove(Arquivo);

		  if(dx_iott.io_fhandle > 0){
			  dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			  dx_iott.io_fhandle = -1;
		  }

		  dx_iott.io_fhandle = dx_fileopen(Arquivo, _O_RDWR|_O_BINARY|_O_CREAT|_O_TRUNC, _S_IREAD|_S_IWRITE);

		  if(dx_iott.io_fhandle <= 0){
			  dx_iott.io_fhandle = -1;
			  PutError("Cannot open file: %s in function RecordFile() Channel %d", Arquivo, Canal);
				CtiLog( "%s", GetLastError() );
			  return -1;
		  }

		  // grava no disco
		  dx_iott.io_type		=	(unsigned short)(IO_DEV|IO_EOT);
		  dx_iott.io_offset	= 0;
		  dx_iott.io_bufp		= 0;
		  dx_iott.io_length	= -1;
		  dx_iott.io_nextp	= NULL;

		  index = 0;

			if(TempoSilencio > 0)
			{
			  dx_clrtpt(&dv_tpt[0], 3);

				// seta o maximo de silencio
				dv_tpt[index].tp_type   = (unsigned short)IO_CONT;
			  dv_tpt[index].tp_termno = (unsigned short)DX_MAXSIL;
			  dv_tpt[index].tp_length = (unsigned short)(TempoSilencio * 10);
				dv_tpt[index].tp_flags  = (unsigned short)TF_MAXSIL;
				index++;
			}
			else
			  dx_clrtpt(&dv_tpt[0], 2);

		  // seta o tempo para gravar
		  dv_tpt[index].tp_type   = (unsigned short)IO_CONT;
		  dv_tpt[index].tp_termno = (unsigned short)DX_MAXTIME;
		  dv_tpt[index].tp_length = (unsigned short)(TempoGravar * 10);
		  dv_tpt[index].tp_flags  = (unsigned short)TF_MAXTIME;
			index++;

		  // seta a mascara de digitos
		  dv_tpt[index].tp_type   = (unsigned short)IO_EOT;
		  dv_tpt[index].tp_termno = (unsigned short)DX_DIGMASK;
		  dv_tpt[index].tp_flags  = (unsigned short)TF_DIGMASK;
		  dv_tpt[index].tp_length = (unsigned short)RetDigMaskTerm(TermChar);

		  // seta o formato do arquivo a gravar
		  dx_xpb.wFileFormat    = RecordFileFormat;	
		  dx_xpb.wDataFormat    = RecordDataFormat;	
		  dx_xpb.nSamplesPerSec = RecordSamples;			
		  dx_xpb.wBitsPerSample = RecordBitsPerSample;

			TimeSlotArrayRec[0] = TimeslotLine;
			TimeSlotArrayRec[1] = SecondarySrc;
			sc_tsinfo_rec.sc_numts		= 2;
			sc_tsinfo_rec.sc_tsarrayp	=	TimeSlotArrayRec;

		  if(bip)
			  Ret = dx_mreciottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, RM_TONE|EV_ASYNC, &sc_tsinfo_rec);
		  else
			  Ret = dx_mreciottdata(Vox, &dx_iott, &dv_tpt[0], &dx_xpb, EV_ASYNC, &sc_tsinfo_rec);

		  if(Ret){
			  PutError("dx_mreciottdata() error in function RecordFileFrom2Src() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				CtiLog( "%s", GetLastError() );
			  return -1;
		  }

		  TimeRecord = time(NULL);
    }

		for(;;){
		
			Event = GetEvent();

			switch(Event){

				case T_DISCONNECT:

					TimeRecord = time(NULL) - TimeRecord; // guarda o tempo de gravacao
			
					StopCh();

					return T_DISCONNECT;

				case T_RECORD:
					TimeRecord = time(NULL) - TimeRecord; // guarda o tempo de gravacao

					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;

					Term = ATDX_TERMMSK(Vox);

					if(Term == AT_FAILURE){
						return -1;
					}
					if(Term & TM_MAXSIL){
						return T_SILENCE;
					}
					return T_RECORD;

				case T_ERROR:
					dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
					dx_iott.io_fhandle = -1;
					PutError("dx_mreciottdata() error in function RecordFileFrom2Src() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					CtiLog( "%s", GetLastError() );
					return -1;
			}
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RecordFileFrom2Src()");
		return -1;
	}
}

int Dialogic::RecordBuffer(std::string** buffer, int TempoGravar, int TempoSilencio, bool bip, const char *TermChar)
{
	try
	{
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RecordBuffer()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	GetDigits

	DESCRICAO:	Pega digitos com digitos de terminacao

	PARAMETROS:	int NumDigitos					-	Numero maximo de digitos a pegar
							int TempoInterdigitos		-	Tempo em segundos entre os digitos
							char *BufferDigitos			-	Ponteiro para string para guardar os digitos
							char *DigitosTerminacao	- Digitos de terminacao 0123456789#*ABCD, se desejar
																				todos usar '@', se desejar barrar todos usar NULL

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::GetDigits(int NumDigitos, int TempoInterdigitos, char *BufferDigitos, const char *DigitosTerminacao)
{
	int Event;
	long Term;

	try {

    if(VoxState() != S_GETDIG)
    {

		  dx_clrtpt(&dv_tpt[0], 3);

		  // seta o numero maximo de digitos a coletar
		  dv_tpt[0].tp_type   = (unsigned short)IO_CONT;
		  dv_tpt[0].tp_termno = (unsigned short)DX_MAXDTMF;
		  dv_tpt[0].tp_length = (unsigned short)NumDigitos;
		  dv_tpt[0].tp_flags  = (unsigned short)TF_MAXDTMF;
		  
		  // seta o tempo maximo para interdigito
		  dv_tpt[1].tp_type   = (unsigned short)IO_CONT;
		  dv_tpt[1].tp_termno = (unsigned short)DX_IDDTIME;
		  dv_tpt[1].tp_length = (unsigned short)(10 * TempoInterdigitos); // 100 ms resolution * timer
		  dv_tpt[1].tp_flags  = (unsigned short)TF_IDDTIME;

		  if( ( DigitosTerminacao != NULL ) && ( DigitosTerminacao[0] != 0 ) )
		  {
			  // seta o digito de terminacao
			  dv_tpt[2].tp_type   = (unsigned short)IO_EOT;
			  dv_tpt[2].tp_termno = (unsigned short)DX_DIGMASK;
			  dv_tpt[2].tp_flags  = (unsigned short)TF_DIGMASK;
			  dv_tpt[2].tp_length = (unsigned short)RetDigMaskTerm(DigitosTerminacao);
		  }
		  else
		  {
			  // s/ digitos de terminacao passados p/ a funcao
			  dv_tpt[1].tp_type   = (unsigned short)IO_EOT;
		  }

		  if(dx_getdig(Vox, &dv_tpt[0], &dv_digit, EV_ASYNC)){
			  PutError("dx_getdig() error in function GetDigits() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				CtiLog( "%s", GetLastError() );
			  return -1;
		  }

    }

		for(;;){
		
			Event = GetEvent();

			switch(Event){

				case T_DISCONNECT:
					StopCh();
				return T_DISCONNECT;

				case T_MAXDIG:

					if(strlen(dv_digit.dg_value) > 0){
						// guarda a informacao se os digitos sao de pulso ou de tom
						if(dv_digit.dg_type[0] == DG_DPD_ASCII)
							DigType = 'P';
						else
							DigType = 'T';
						
						// armazena os digitos
						strcpy(BufferDigitos, dv_digit.dg_value);
					}

					Term = ATDX_TERMMSK(Vox);

					if(Term == AT_FAILURE){
						return -1;
					}
					if(Term & TM_IDDTIME){
						return T_INTERDIG;
					}
					if(Term & TM_DIGIT){
						int pos = strlen(dv_digit.dg_value) - 1;
						BufferDigitos[pos] = 0;
						return T_MAXDIG;
					}
				return T_MAXDIG;

        case T_MESSAGEINI:          
					if( StopCh() == T_DISCONNECT )
						return T_DISCONNECT;
          return T_MESSAGEINI;

#if defined _APPLICATION_SOCKET
        case T_SW_CONF_MONITOR_REQUEST:          
					if( StopCh() == T_DISCONNECT )
						return T_DISCONNECT;
          return T_SW_CONF_MONITOR_REQUEST;
#endif // _APPLICATION_SOCKET         

			}
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetDigits com terminacao()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	ClearDigits

	DESCRICAO:	Limpa digitos da fila

	PARAMETROS:

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::ClearDigits(void)
{
	try {
		
		if( dx_clrdigbuf(Vox) == -1){
			PutError("dx_clrdigbuf() error in function ClearDigits() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::ClearDigits()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	LineTone

	DESCRICAO:	Toca um tom de linha assincronamente

	PARAMETROS:	int Time - tempo em segundos para tocar o tom

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::LineTone(int Time)
{
	try {

		// configura o tom a ser tocado
		dx_bldtngen(&tngen, 425, 0, -10, 0, Time * 100);	// tempo em unidades de 10ms

		dx_clrtpt(&dv_tpt[0], 2);

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_CONT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(Time * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		// seta a mascara de digitos para parar o tom
		dv_tpt[1].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[1].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[1].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[1].tp_length = (unsigned short)DigMask;

		if(dx_playtone(Vox, &tngen, &dv_tpt[0], EV_ASYNC)){
			PutError("dx_playtone() error in function LineTone() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::LineTone()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	RingTone

	DESCRICAO:	Toca um tom de ring assincronamente

	PARAMETROS:	int Time - tempo em segundos para tocar o ring

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::RingTone(int Time)
{
	try {

		dx_clrtpt(&dv_tpt[0], 2);

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_CONT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(Time * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		// seta a mascara de digitos para parar o tom
		dv_tpt[1].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[1].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[1].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[1].tp_length = (unsigned short)0;

		if(dx_playtoneEx(Vox, CP_RINGBACK1, &dv_tpt[0], EV_ASYNC) == -1 ){
			PutError("dx_playtoneEx() error in function BusyTone() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RingTone()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	BusyTone

	DESCRICAO:	Toca um tom de ocupado assincronamente

	PARAMETROS:	int Time - tempo em segundos para tocar o tom

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::BusyTone(int Time)
{
	try {

		dx_clrtpt(&dv_tpt[0], 1);

		// 425 Hz at -10dB ON for 25 * 10 ms and OFF for 25 *10 ms.

		tngencad.cycles = Time * 2;
		tngencad.numsegs = 1;

		tngencad.offtime[0] = 25;
		
		dx_bldtngen( &tngencad.tone[0], 425, 0, -10, 0, 25 );

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(Time * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		if(dx_playtoneEx(Vox, CP_BUSY, &dv_tpt[0], EV_ASYNC) == -1 ) {
//		if(dx_playtoneEx(Vox, &tngencad, &dv_tpt[0], EV_ASYNC) == -1 ) {
			PutError("dx_playtoneEx() error in function BusyTone() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::BusyTone()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	PlayToneSync

	DESCRICAO:	Toca um tom de sincronamente

	PARAMETROS:	int Time	- Tempo em ms para tocar o tom
							int Freq1	- Frequencia 1
							int	Freq2	- Frequencia 2

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::PlayToneSync(int Time, int Freq1, int Freq2 = 0)
{
	try {

		// configura o tom a ser tocado
		dx_bldtngen(&tngen, Freq1, Freq2, -10, -10, Time / 10);	// tempo em unidades de 10ms

		dx_clrtpt(&dv_tpt[0], 2);

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_CONT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(Time * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		// seta a mascara de digitos para parar o tom
		dv_tpt[1].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[1].tp_termno = (unsigned short)DX_DIGMASK;
		dv_tpt[1].tp_flags  = (unsigned short)TF_DIGMASK;
		dv_tpt[1].tp_length = (unsigned short)DigMask;

		if(dx_playtone(Vox, &tngen, &dv_tpt[0], EV_SYNC)){
			PutError("dx_playtone() error in function PlayTone() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PlayToneSync()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	DialVox

	DESCRICAO:	Disca pelo canal de voz

	PARAMETROS:	char *Number			- Numero para discar
							bool CallProgress	- Se true faz CallProgress, false faz discagem cega

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::DialVox(char *Number, bool CallProgress)
{
	int Ret;

	try {

		if(CallProgress)
			Ret = dx_dial(Vox, Number, &dx_cap, DX_CALLP | EV_ASYNC);
		else
			Ret = dx_dial(Vox, Number, &dx_cap, EV_ASYNC);

		if(Ret){
			PutError("dx_dial error in function DialVox() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		for(;;){

			Event = GetEvent();

			switch(Event){

				case T_DISCONNECT: // se vier evento de desconexao, retorna ocupado
				case T_BUSY:
					return T_BUSY;

				case T_DIAL:
				case T_CONNECT:
				case T_NOANSWER:
				case T_NODIALTONE:
					return Event;

				case T_ERROR:
					return T_ERROR;
			}
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialVox()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	CloseFile

	DESCRICAO:	Fecha o handle do arquivo de vox corrente que esta sendo tocado ou gravado

	PARAMETROS:	

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
void Dialogic::CloseFile(void)
{
	try {

		if(dx_iott.io_fhandle > 0){
			dx_fileclose(dx_iott.io_fhandle);	// fecha o arquivo
			dx_iott.io_fhandle = -1;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::CloseFile()");
	}
}
		
/********************************************************************************************
	NOME:	StopChAsync

	DESCRICAO:	Para o canal de voz assincronamente, sem retirar os eventos da fila

	PARAMETROS:	

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::StopChAsync(void)
{
	try {

#ifdef _ASR
		if( !bAsrEnabled )
		{
			if(dx_stopch(Vox, EV_ASYNC)){ // para o canal de voz
				PutError("dx_stopch() error in function StopChAsync() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		else
		{
			if(ec_stopch(Vox, SENDING, EV_ASYNC)){ // para o canal de voz
				PutError("dx_stopch() error in function StopChAsync() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
#else
		dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::StopChAsync()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	StopCh

	DESCRICAO:	Para o canal de voz assincronamente retirando todos os eventos da fila
							e retornando se o usuario desconectou

	PARAMETROS:	int iTimeOut - Tempo para esperar eventos em (ms)	default 1000

	RETORNO:		0 se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::StopCh(int iTimeOut)
{
	try
	{
        m_pLog4cpp->debug("[%p][%s] ", this , __FUNCTION__);

		bool eventDisconnect = false;
		bool eventIdle = false;
		
		// Fixa o timeout p/ parar o canal em 30s.
		iTimeOut = 30000;

		if(ATDX_STATE(Vox) != CS_IDLE)
		{
			// para o canal de voz
#ifdef _ASR
			if( !bAsrEnabled )
			{
				if(dx_stopch(Vox, EV_ASYNC)){ // para o canal de voz
					PutError("dx_stopch() error in function StopCh() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					return -1;
				}
			}
			else
			{
				// Verifica se o recurso de voz esta reconhecendo/gravando.
				//if(ATDX_STATE(Vox) == CS_RECD)
					// Operacoes de reconhecimento/gravacao sao controladas pela Nuance, entao apenas retorna.
					//return 0;
				if(ec_stopch(Vox, SENDING, EV_ASYNC)){ // para o canal de voz
					PutError("dx_stopch() error in function StopCh() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
					return -1;
				}
			}
#else
			if(dx_stopch(Vox, EV_ASYNC)){ // para o canal de voz
				PutError("dx_stopch() error in function StopCh() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
#endif // _ASR

			do {
				Event = GetEvent( iTimeOut );
				if(Event == T_DISCONNECT)
					// A ligacao foi deconectada. Sinaliza p/ retornar depois.
					eventDisconnect = true;

				if( Event == T_IDLE )
					// O canal foi desbloqueado. Sinaliza p/ retornar depois.
					eventIdle = true;

			}while( ( Event != -1 ) && ( Event != T_PLAY ) && ( Event != T_RECORD ) && ( Event != T_MAXDIG ) && ( Event != T_PLAYTONE ) );

			if( Event == -1 )
			{
				PutError( "Error in StopCh(). Timeout waiting vox resource to stop." );
				CtiLog( "%s", GetLastError() );
				return -1;
			}

			// Se veio desconexao durante a parada do canal, retorna T_DISCONNECT.
			if(eventDisconnect)
				return T_DISCONNECT;

			// Se veio desconexao durante a parada do canal, retorna T_IDLE.
			if(eventIdle)
				return T_IDLE;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::StopCh()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	SetPCM

	DESCRICAO:	Seta o PCMEncondig para arquivos PCM, ULAW ou ALAW

	PARAMETROS:	int TypePcm	-	ULAW ou ALAW

	RETORNO:
********************************************************************************************/
void Dialogic::SetPCM(int TypePcm)
{
	try {

		if(TypePcm != ULAW && TypePcm != ALAW)
			PcmEnconding = ULAW;
		else
			PcmEnconding = TypePcm;
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetPCM()");
	}
}
/********************************************************************************************
	NOME:	GetPCM

	DESCRICAO:	Retorna o PCMEncondig setado para arquivos PCM, ULAW ou ALAW

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::GetPCM(void)
{
	return PcmEnconding;
}
/********************************************************************************************
	NOME:	SetPlayFormat

	DESCRICAO:	Seta o o formato para a reproducao de arquivos de voz, apos a chamada a essa
							funcao qualquer chamada a funcao PlayFile utilizara esse formato

	PARAMETROS:	int Format - formato do arquivo VOX24, VOX32, PCM48, PCM64, WAV

	RETORNO:
********************************************************************************************/
void Dialogic::SetPlayFormat(int Format)
{
	try {

		switch(Format){

			case VOX24:
				TypePlayFormat		= VOX24;

				PlayFileFormat		= FILE_FORMAT_VOX;
				PlayDataFormat		= DATA_FORMAT_DIALOGIC_ADPCM;
				PlaySamples				= DRT_6KHZ;
				PlayBitsPerSample	= 4;
			break;

			case VOX32:
				TypePlayFormat		= VOX32;

				PlayFileFormat		= FILE_FORMAT_VOX;
				PlayDataFormat		= DATA_FORMAT_DIALOGIC_ADPCM;
				PlaySamples				= DRT_8KHZ;
				PlayBitsPerSample	= 4;
			break;

			case PCM48:
				TypePlayFormat		= PCM48;

				PlayFileFormat		= FILE_FORMAT_VOX;
				PlayDataFormat		= PcmEnconding == ULAW ? DATA_FORMAT_MULAW : DATA_FORMAT_ALAW;
				PlaySamples				= DRT_8KHZ;
				PlayBitsPerSample	= 6;
			break;

			case PCM64:
				TypePlayFormat		= PCM64;

				PlayFileFormat		= FILE_FORMAT_VOX;
				PlayDataFormat		= PcmEnconding == ULAW ? DATA_FORMAT_MULAW : DATA_FORMAT_ALAW;
				PlaySamples				= DRT_8KHZ;
				PlayBitsPerSample	= 8;
			break;
			
			case WAV:
				TypePlayFormat		= WAV;

				PlayFileFormat		=	FILE_FORMAT_WAVE;
				PlayDataFormat		= DATA_FORMAT_PCM;
				PlaySamples				= DRT_11KHZ;
				PlayBitsPerSample	= 8;
			break;

			case WAV_G711_ALAW:
				TypePlayFormat		= WAV_G711_ALAW;

				PlayFileFormat		=	FILE_FORMAT_WAVE;
				PlayDataFormat		= DATA_FORMAT_G711_ALAW;
				PlaySamples				= DRT_8KHZ;
				PlayBitsPerSample	= 8;
			break;

			case PCM_G711_ALAW:
				TypePlayFormat		= PCM_G711_ALAW;

				PlayFileFormat		=	FILE_FORMAT_VOX;
				PlayDataFormat		= DATA_FORMAT_G711_ALAW;
				PlaySamples				= DRT_8KHZ;
				PlayBitsPerSample	= 8;
			break;

      case WAV_PCM:
				TypePlayFormat			= WAV_PCM;

				PlayFileFormat			=	FILE_FORMAT_WAVE;
				PlayDataFormat			= DATA_FORMAT_PCM;
				PlaySamples					= DRT_8KHZ;
				PlayBitsPerSample		= 8;
			break;
		}
	}
	catch(...){
		PutError("Exception Dialogic::SetPlayFormat()");
	}
}
/********************************************************************************************
	NOME:	GetPlayFormat

	DESCRICAO:	Retorna o formato setado para a reproducao de arquivos de voz,
							apos a chamada a essa funcao qualquer chamada a funcao PlayFile
							utilizara esse formato

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::GetPlayFormat(void)
{
	return TypePlayFormat;
}
/********************************************************************************************
	NOME:	SetRecordFormat

	DESCRICAO:	Seta o o formato para a gravacao de arquivos de voz, apos a chamada a essa
							funcao qualquer chamada a funcao RecordFile utilizara esse formato

	PARAMETROS:

	RETORNO:
********************************************************************************************/
void Dialogic::SetRecordFormat(int Format)
{
	try {

		switch(Format){

			case VOX24:
				TypeRecordFormat		= VOX24;

				RecordFileFormat		= FILE_FORMAT_VOX;
				RecordDataFormat		= DATA_FORMAT_DIALOGIC_ADPCM;
				RecordSamples				= DRT_6KHZ;
				RecordBitsPerSample	= 4;
			break;

			case VOX32:
				TypeRecordFormat		= VOX32;
				
				RecordFileFormat		= FILE_FORMAT_VOX;
				RecordDataFormat		= DATA_FORMAT_DIALOGIC_ADPCM;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 4;
			break;

			case PCM48:
				TypeRecordFormat		= PCM48;

				RecordFileFormat		= FILE_FORMAT_VOX;
				RecordDataFormat		= PcmEnconding == ULAW ? DATA_FORMAT_MULAW : DATA_FORMAT_ALAW;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 6;
			break;

			case PCM64:
				TypeRecordFormat		= PCM64;

				RecordFileFormat		= FILE_FORMAT_VOX;
				RecordDataFormat		= PcmEnconding == ULAW ? DATA_FORMAT_MULAW : DATA_FORMAT_ALAW;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 8;
			break;
			
			case WAV:
				TypeRecordFormat		= WAV;

				RecordFileFormat		=	FILE_FORMAT_WAVE;
				RecordDataFormat		= DATA_FORMAT_PCM;
				RecordSamples				= DRT_11KHZ;
				RecordBitsPerSample	= 8;
			break;

			case WAV_G711_ALAW:
				TypeRecordFormat		= WAV_G711_ALAW;
				                   
				RecordFileFormat		=	FILE_FORMAT_WAVE;
				RecordDataFormat		= DATA_FORMAT_G711_ALAW;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 8;
			break;      

			case PCM_G711_ALAW:
				TypeRecordFormat		= PCM_G711_ALAW;
				                   
				RecordFileFormat		=	FILE_FORMAT_VOX;
				RecordDataFormat		= DATA_FORMAT_G711_ALAW;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 8;
			break;

      case WAV_PCM:
				TypeRecordFormat		= WAV_PCM;
				RecordFileFormat		=	FILE_FORMAT_WAVE;
				RecordDataFormat		= DATA_FORMAT_PCM;
				RecordSamples				= DRT_8KHZ;
				RecordBitsPerSample	= 8;
			break;
		}
	}
	catch(...){
		PutError("Exception Dialogic::SetFileRecordFormat()");
	}
}
/********************************************************************************************
	NOME:	GetRecordFormat

	DESCRICAO:	Retorna o formato setado para a gravacao de arquivos de voz,
							apos a chamada a essa funcao qualquer chamada a funcao RecordFile
							utilizara esse formato

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::GetRecordFormat(void)
{
	return TypeRecordFormat;
}
/********************************************************************************************
	NOME:	SetDigMask

	DESCRICAO:	Seta a mascara de digitos para interromper Play e Record

	PARAMETROS:	char *Mask -	0123456789#*ABCD, se desejar todos usar '@',
														se desejar barrar todos usar NULL
													

	RETORNO:
********************************************************************************************/
void Dialogic::SetDigMask(const char *Mask)
{
	int i, len;

	try {

		DigMask = 0;

		if(Mask == NULL){
			// string vazia ou NULL, nao permite cortar Play e Record com nenhum digito
			DigMask = 0;
			return;
		}

		len = strlen(Mask);

		if(len == 0){
			// string vazia ou NULL, nao permite cortar Play e Record com nenhum digito
			DigMask = 0;
			return;
		}

		for(i = 0; i < len; i++){

			switch(Mask[i]){

				case '0': DigMask |= (unsigned short)DM_0;		break;
				case '1': DigMask |= (unsigned short)DM_1;		break;
				case '2': DigMask |= (unsigned short)DM_2;		break;
				case '3': DigMask |= (unsigned short)DM_3;		break;
				case '4': DigMask |= (unsigned short)DM_4;		break;
				case '5': DigMask |= (unsigned short)DM_5;		break;
				case '6': DigMask |= (unsigned short)DM_6;		break;
				case '7': DigMask |= (unsigned short)DM_7;		break;
				case '8': DigMask |= (unsigned short)DM_8;		break;
				case '9': DigMask |= (unsigned short)DM_9;		break;
				case '#': DigMask |= (unsigned short)DM_P;		break;
				case '*': DigMask |= (unsigned short)DM_S;		break;
		
				case 'A': 
				case 'a':	DigMask |= (unsigned short)DM_A;		break;
				
				case 'B':
				case 'b': DigMask |= (unsigned short)DM_B;		break;
				
				case 'C':
				case 'c': DigMask |= (unsigned short)DM_C;		break;
				
				case 'D':
				case 'd': DigMask |= (unsigned short)DM_D;		break;
				
				case '@': DigMask = (unsigned short)DM_ALL;	break;
				
				default:  DigMask = (unsigned short)DM_ALL; break;
			}
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetDigMask()");
	}
}
/********************************************************************************************
	NOME:	GetDigMask

	DESCRICAO:	Retorna a mascara de digitos atual em Mask

	PARAMETROS:	char *Destino - Ponteiro para a variavel que contera a mascara de digitos atual

	RETORNO:		Nenhum
********************************************************************************************/
void Dialogic::GetDigMask(char *Destino)
{
	int i = 0;

	try {

		if(DigMask == 0){
			// string vazia, nao permite cortar Play e Record com nenhum digito
			Destino = "";
			return;
		}
		if(DigMask == DM_ALL){
			strcpy(Destino, "@");
			return;
		}
		else{
			if(DigMask | DM_0){
				Destino[i++] = '0';
			}
			if(DigMask | DM_1){
				Destino[i++] = '1';
			}
			if(DigMask | DM_2){
				Destino[i++] = '2';
			}
			if(DigMask | DM_3){
				Destino[i++] = '3';
			}
			if(DigMask | DM_4){
				Destino[i++] = '4';
			}
			if(DigMask | DM_5){
				Destino[i++] = '5';
			}
			if(DigMask | DM_6){
				Destino[i++] = '6';
			}
			if(DigMask | DM_7){
				Destino[i++] = '7';
			}
			if(DigMask | DM_8){
				Destino[i++] = '8';
			}
			if(DigMask | DM_9){
				Destino[i++] = '9';
			}
			if(DigMask | DM_P){
				Destino[i++] = '#';
			}
			if(DigMask | DM_S){
				Destino[i++] = '*';
			}
			if(DigMask | DM_A){
				Destino[i++] = 'A';
			}
			if(DigMask | DM_B){
				Destino[i++] = 'B';
			}
			if(DigMask | DM_C){
				Destino[i++] = 'C';
			}
			if(DigMask | DM_D){
				Destino[i++] = 'D';
			}
			Destino[i] = 0;
			return;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetDigMask()");
	}
}
/********************************************************************************************
	NOME:	RetDigMaskTerm

	DESCRICAO:	Retorna uma mascara de digitos de acordo com a string de digitos passada

	PARAMETROS:	char *DigitsTerm	- Digitos de terminacao 0123456789#*ABCD, se desejar
																	todos usar '@', se desejar barrar todos usar NULL
	RETORNO:
********************************************************************************************/
int Dialogic::RetDigMaskTerm(const char *DigitsTerm)
{
	int i, len;
	unsigned short MaskResult = 0;

	try {

		if(DigitsTerm == NULL){
			// string vazia ou NULL, nao permite cortar Play e Record com nenhum digito
			return 0;
		}

		len = strlen(DigitsTerm);

		if(len == 0){
			// string vazia ou NULL, nao permite cortar Play e Record com nenhum digito
			return 0;
		}

		for(i = 0; i < len; i++){

			switch(DigitsTerm[i]){

				case '0': MaskResult |= (unsigned short)DM_0;		break;
				case '1': MaskResult |= (unsigned short)DM_1;		break;
				case '2': MaskResult |= (unsigned short)DM_2;		break;
				case '3': MaskResult |= (unsigned short)DM_3;		break;
				case '4': MaskResult |= (unsigned short)DM_4;		break;
				case '5': MaskResult |= (unsigned short)DM_5;		break;
				case '6': MaskResult |= (unsigned short)DM_6;		break;
				case '7': MaskResult |= (unsigned short)DM_7;		break;
				case '8': MaskResult |= (unsigned short)DM_8;		break;
				case '9': MaskResult |= (unsigned short)DM_9;		break;
				case '#': MaskResult |= (unsigned short)DM_P;		break;
				case '*': MaskResult |= (unsigned short)DM_S;		break;
		
				case 'A': 
				case 'a':	MaskResult |= (unsigned short)DM_A;		break;
				
				case 'B':
				case 'b': MaskResult |= (unsigned short)DM_B;		break;
				
				case 'C':
				case 'c': MaskResult |= (unsigned short)DM_C;		break;
				
				case 'D':
				case 'd': MaskResult |= (unsigned short)DM_D;		break;
				
				case '@': MaskResult = (unsigned short)DM_ALL;	break;
				
				default:  MaskResult = (unsigned short)DM_ALL; break;
			}
		}
		
		return MaskResult;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RetDigMaskTerm()");
		return 0;
	}
}
/********************************************************************************************
	NOME:	DigitQueue

	DESCRICAO:	Habilita ou nao o canal a retornar eventos pela fila 
							quando digitos sao detectados.

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		IMPORTANTE: NUNCA CHAME ESSA FUNCAO COM O CANAL DE VOZ OCUPADO, RESETA A MAQUINA
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	PARAMETROS:	bool bFlag - se true ativa, se false, desativa

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::DigitQueue(bool bFlag)
{
	try {

		if(bFlag){
			if(dx_setevtmsk(Vox, LSI_MASK | DM_DIGITS)){
				PutError("dx_setevtmsk() error in function DigitQueue() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		else{
			if(dx_setevtmsk(Vox, LSI_MASK | DM_DIGOFF)){
				PutError("dx_setevtmsk() error in function DigitQueue() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DigitQueue()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	GetLastDigit

	DESCRICAO:	Retorna o ultimo digitado e recebido pela fila de eventos

	PARAMETROS:

	RETORNO:	Ultimo digito ou zero se nao existir nenhum digito
********************************************************************************************/
char Dialogic::GetLastDigit(void)
{
	try {

		switch(LastDigit){

			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '#':
			case '*':
			case 'A': 
			case 'a':
			case 'B':
			case 'b':
			case 'C':
			case 'c':
			case 'D':
			case 'd':
				return LastDigit;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetLastDigit()");
		return 0;
	}
}
/********************************************************************************************
	NOME:	GetTimeRecord

	DESCRICAO:	Retorna o tempo de gravado do ultimo arquivo gravado

	PARAMETROS:	

	RETORNO:	Tempo gravado em segundos
********************************************************************************************/
long Dialogic::GetTimeRecord(void)
{
	try {
		return TimeRecord;
	}
	catch(...)
	{
		PutError("Exception Dialogic::TimeRecord()");
		return 0;
	}
}
/********************************************************************************************
	NOME:	VoxState

	DESCRICAO:	Retorna o estado do canal de vox

	PARAMETROS:	

	RETORNO:	Tempo gravado em segundos
********************************************************************************************/
int Dialogic::VoxState(void)
{
	try {

		switch( ATDX_STATE(Vox) ){

			case CS_DIAL:				// Dial state
				return S_DIAL;
			break;
			case CS_CALL:				// Call state
				return S_CALL;
			break;
			case CS_GTDIG:			// Get Digit state
				return S_GETDIG;
			break;
			case CS_HOOK:				// Hook state
				return S_HOOK;
			break;
			case CS_IDLE:				// Idle state
				return S_IDLE;
			break;
			case CS_PLAY:				// Play state
				return S_PLAY;
			break;
			case CS_RECD:				// Record state
				return S_RECORD;
			break;
			case CS_STOPD:			// Stopped state
				return S_STOPED;
			break;
			case CS_TONE:				// Playing tone state
				return S_TONE;
			break;
			case CS_WINK:				// Wink state
				return S_WINK;
			break;
			default:
				return S_UNKNOWN;	// Unknow
			break;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::VoxState()");
		return 0;
	}
}
/********************************************************************************************
	NOME:	GetBytesIO

	DESCRICAO:	Retorna os bytes que passaram pelo canal no ultimo record ou play

	PARAMETROS:	

	RETORNO:	bytes
********************************************************************************************/
long Dialogic::GetBytesIO(void)
{
	try {
		return ATDX_TRCOUNT(Vox);
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetBytesIO()");
		return 0;
	}
}
/********************************************************************************************
	NOME:	Volume

	DESCRICAO:	Ajusta o volume da reproducao dos arquivos de voz

	PARAMETROS:	int Level - Volume em passos de 2 dB:
	
							 1	Increase Play-Back Volume by 2db
							 2	Increase Play-Back Volume by 4db
							 3	Increase Play-Back Volume by 6db
							 4	Increase Play-Back Volume by 8db
							 0	Normal
							-1	Decrease Play-Back Volume by 2db
							-2	Decrease Play-Back Volume by 4db
							-3	Decrease Play-Back Volume by 6db
							-4	Decrease Play-Back Volume by 8db

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::Volume(int Level)
{
	try {

		if( Level > 4 )
			Level = 4;
		
		if( Level < -4 )
			Level = -4;
		
		if( Level == 0 )
			Level = SV_NORMAL;

		if(dx_adjsv(Vox, SV_VOLUMETBL, SV_ABSPOS, Level) == -1){
			PutError("dx_adjsv() error in function Volume() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::Volume()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	Speed

	DESCRICAO:	Ajusta a velocidade com que o arquivo de voz e reproduzido

	PARAMETROS:	int Level - Speed

							 1 Speed up Play-Back 10 Percent
							 2 Speed up Play-Back 20 Percent
							 3 Speed up Play-Back 30 Percent
							 4 Speed up Play-Back 40 Percent
							 5 Speed up Play-Back 50 Percent
							 0 Normal Speed
							-1 Slow Down Play-Back 10 Percent
							-2 Slow Down Play-Back 20 Percent
							-3 Slow Down Play-Back 30 Percent
							-4 Slow Down Play-Back 40 Percent
							-5 Slow Down Play-Back 50 Percent

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::Speed(int Level)
{
	try {

		// Em testes com a DM3 o valor para voltar a velocidade normal eh zero

		if(dx_adjsv(Vox, SV_SPEEDTBL, SV_ABSPOS, Level) == -1){
			PutError("dx_adjsv() error in function Speed() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::Speed()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	SetPlayBufferSize

	DESCRICAO:	Ajusta o tamanho do buffer de reproducao

	PARAMETROS:	BulkQueueBufferSize - tamanho do buffer (conforme parametrizacao da Dialogic)

	RETORNO:	0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::SetPlayBufferSize( const enum BulkQueueBufferSizeType& BulkQueueBufferSize )
{
	int rc = 0;
	rc = dx_setchxfercnt( Vox, (int) BulkQueueBufferSize );
	if( rc )
	{
		PutError( "dx_setparm() error in function SetPlayBufferSize() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox) );
		CtiLog( "%s", GetLastError() );
		return -1;
	}

	//Dialogic::PlayBufferSize = PlayBufferSize;

	return 0;
}
/********************************************************************************************
	NOME:	PulseDetection

	DESCRICAO:	Habilita ou desabilita a detecao de pulso

	PARAMETROS:	bool flag - true = habilita pegar pulso, flase somente tom

	RETORNO:	bytes
********************************************************************************************/
int Dialogic::PulseDetection(bool flag)
{
	try {

		if(flag){
			// seta o tipo de digitos para pegar (PULSO E TOM)
			if(dx_setdigtyp(Vox,(unsigned short)(D_DTMF|D_DPDZ))){
				PutError(ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		else{
			// seta o tipo de digitos para pegar somente (TOM)
			if(dx_setdigtyp(Vox,(unsigned short)(D_DTMF))){
				PutError(ATDV_ERRMSGP(Vox));
				return -1;
			}
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::PulseDetection()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	GetVoxTermCause

	DESCRICAO:	Retorna a causa do termino de uma funcao de voz

	PARAMETROS:	Nenhum

	RETORNO:	int Causa
********************************************************************************************/
int Dialogic::GetVoxTermCause(void)
{
	try {

		int Term;

		Term = ATDX_TERMMSK(Vox);

		if(Term & TM_IDDTIME)		// Inter Digit Delay
			return T_INTERDIG;
		
		if(Term & TM_MAXDTMF)		// Max Number of Digits Recd
			return T_MAXDIG;

		if(Term & TM_MAXSIL)		// Max Silence
			return T_SILENCE;

		if(Term & TM_DIGIT)			// Digit Mask or Digit Type Term
			return T_DIGIT;

		/*
		// <Antonio causas nao usadas ainda>
		if(Term & TM_NORMTERM)	// Normal Termination
			return 0;
		
		if(Term & TM_MAXNOSIL)	// Max Non-Silence
			return 0;
		
		if(Term == AT_FAILURE)	// Failure in function ATDX_TERMMSK()
			return 0;

		if(Term & TM_LCOFF)			// Loop Current Off
			return 0;

		if(Term & TM_MAXTIME)		// Max Function Time Exceeded
			return 0;

		if(Term & TM_USRSTOP)		// Function Stopped by User
			return 0;

		if(Term & TM_EOD)				// End of Data Reached on Playback
			return 0;

		if(Term & TM_TONE)			// Tone On/Off Termination
			return 0;

		if(Term & TM_BARGEIN)		// Play terminated due to Barge-in
			return 0;

		if(Term & TM_ERROR)			// I/O Device Error
			return 0;

		if(Term & TM_MAXDATA)		// Max Data reached for FSK
			return 0;
		// </Antonio>
		*/

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetVoxTermCause()");
		return 0;
	}
}

int Dialogic::CheckMESSAGEBOXTone(const unsigned int ToneId)
{
	try 
	{
		// Contabiliza as notas da melodia de MessageBox celular
		if((MessageBoxMelody == 0) && ((ToneId == 1001) || (ToneId == 2001)))				// C5
		{
			iMelody = ToneId/1000;
			MessageBoxMelody++;
		}
		else if((MessageBoxMelody == 1) && ((ToneId == 1005) || (ToneId == 2005)))	// G5
		{
			if(iMelody == ToneId/1000)
				MessageBoxMelody++;
		}	
//		else if((MessageBoxMelody == 2) && ((ToneId == 1004) || (ToneId == 2004)))	// F5
//		{
//			if(iMelody == ToneId/1000)
//				MessageBoxMelody++;
//		}	
		else if((MessageBoxMelody == 2) && ((ToneId == 1002) || (ToneId == 2002)))	// D5
		{
			if(iMelody == ToneId/1000)
				MessageBoxMelody++;
		}	
		else if((MessageBoxMelody == 3) && ((ToneId == 1003) || (ToneId == 2003)))	// E5
		{
			if(iMelody == ToneId/1000)
				MessageBoxMelody++;
		}	
		else if((MessageBoxMelody == 4) && ((ToneId == 1001) || (ToneId == 2001)))	// C5
		{
			if(iMelody == ToneId/1000)
				MessageBoxMelody++;
		}	
		else if((MessageBoxMelody == 5) && ((ToneId == 1002) || (ToneId == 2002)))	// D5
		{
			if(iMelody == ToneId/1000)
				MessageBoxMelody++;
		}	
		else if((MessageBoxMelody == 6) && ((ToneId == 1001) || (ToneId == 2001)))	// C5
			MessageBoxMelody++;			// nao esta detectando essa nota qdo BCP/TIM

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::CheckMESSAGEBOXTone()");
		return -1;
	}
}

int Dialogic::DetectTones( const int type )
{
	try 
	{
		if( type == TST_VOICE_MAIL )
		{
			// Verifica se a melodia esta completa e as notas estao em ordem.
			if( MessageBoxMelody == 7 )
				return 1;
			else
				return 0;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DetectTones()");
		return -1;
	}
}

int Dialogic::StopRecordFile(void)
{
	bool bFlagDisconnect = false;
	
  if(VoxState() != S_IDLE) {
    
    TimeRecord = time(NULL) - TimeRecord; // guarda o tempo de gravacao

		if( StopCh() == T_DISCONNECT )
			bFlagDisconnect = true;

    if(dx_iott.io_fhandle > 0) {
			if(bFlagDisconnect)
				return T_DISCONNECT;
			else return 1;
    }
    else
			if(bFlagDisconnect)
				return T_DISCONNECT;
			else return 0;

	}

  return 1;
}


/********************************************************************************************
	NOME:	MsiInitBoard

	DESCRICAO:	Inicia e guarda parametros de todas as placas MSI no sistema

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::MsiInitBoard(void)
{
	MS_CDT ms_cdt[20];
	int i, j, b, Ring, Device, ZipTone, NumStations, qtd;
	char Name[100];

	try {		

		// obtem o numero de placas MSI
		if(sr_getboardcnt(DEV_CLASS_MSI, &NumBoardsMsi)){
			PutError("sr_getboardcnt() error in function MsiInitBoard()");
			return -1;
		}

		for(b = 1; b <= NumBoardsMsi; b++){

			sprintf(Name, "msiB%d", b);

			// abre device da placa MSI
			Device = ms_open(Name, 0);
			
			if(Device == -1){
				PutError("ms_open() error in function MsiInitBoard() Board %d", i);
				return -1;
			}

			// desaloca qualquer recurso de conferencia previamente alocado
			for(i = 1; i <= MAX_MSI_DSP; i++){

				if(ms_getcnflist(Device, i, &qtd, &ms_cdt[0]) == 0){
					
					for(j = 0; qtd > 1; j++, qtd--){ 
						ms_remfromconf(Device, i, &ms_cdt[j]);
					}
					ms_delconf(Device, i);
				}
				// deleta conexoes extendidas pre-existentes
				ms_delxtdcon(Device, i);
			}

			// verifica se a placa possui ring
			Ring = MS_NORNGBRD;

			if(ms_getbrdparm(Device, MSG_RING, &Ring)){
				PutError("ms_getbrdparm() error in function MsiInitBoard() Board %d", i);
				return -1;
			}

			if(Ring == MS_NORNGBRD)
				Ring = FALSE;
			else
				Ring = TRUE;

			// habilita zip tone
			ZipTone = MS_ZIPENABLE;

			if(ms_setbrdparm(Device, MSG_ZIPENA, (void *)&ZipTone)){
				PutError("ms_setbrdparm() error in function MsiInitBoard(MSG_ZIPENA) Board %d", i);
				return -1;
			}

			// obtem quantas estacoes tem a placa
			NumStations = ATDV_SUBDEVS(Device);	

			MsiInfo[b].NumStations		=	NumStations;
			MsiInfo[b].MsiBoardDevice	=	Device;
			MsiInfo[b].Ring						=	Ring;
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiInitBoard()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	MsiInfo

	DESCRICAO:	Retorna informacoes de uma placa MSI

	PARAMETROS:	int		BoardNumber		- Numero da placa
							int		&MsiBoardDev	- Referencia para o numero do  device da placa

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
void Dialogic::GetMsiInfo(int BoardNumber, int &MsiBoardDev)
{
	try {

		if(MsiInfo[BoardNumber].Ring)
			FlagMsiRing = true;
		else
			FlagMsiRing = false;

		MsiBoardDev	= MsiInfo[BoardNumber].MsiBoardDevice;
	}
	catch(...)
	{
		PutError("Exception Dialogic::GetMsiInfo()");
	}
}
/********************************************************************************************
	NOME:	MsiHasRing

	DESCRICAO:	Retorna true se a placa MSI tem ring,. false se nao tem

	PARAMETROS:
	RETORNO:		true ou false
							-1 erro
********************************************************************************************/
bool Dialogic::MsiHasRing(void)
{
	return FlagMsiRing;
}
/********************************************************************************************
	NOME:	ProcMSI

	DESCRICAO:	Processa os eventos para a posicao de atendimento MSI

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h ou -1 se nao houve evento
********************************************************************************************/
int Dialogic::ProcMSI( int Time )
{
	try {

		Devs[0] = Msi;
		Devs[1] = 0;

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 1, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 1, Time, &EventHandle) == -1)
				return -1;
		}

		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case MSEV_RING:

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				switch( MsiSigEvt(Bit) ){

					case MSMM_RNGOFFHK:		// o ring terminou porque o fone foi retirado do gancho
						return MSI_RING_OFFHOOK;

					case MSMM_TERM:				// o ring terminou e nao atendeu
						return MSI_RING_TERM;

					case MSMM_RNGSTOP:		// o ring foi terminado pela funcao ms_stopfn()
						return MSI_RING_STOP;
				}
			break;

			case MSEV_NORING:	// erro ao iniciar ring
				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				// Se a estacao foi para OFFHOOK na hora que o ring chegou,
				// entao retorna MSI_RING_OFFHOOK, se realmente houve um erro ao gerar um Ring
				// retorna MSI_NORING para que a aplicacao possa gerar outro ring
				if(Bit == E_MSBADRNGSTA)
					return MSI_RING_OFFHOOK;
				else
					return MSI_NORING; 
			break;

			case MSEV_SIGEVT:
	
				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				switch( MsiSigEvt(Bit) ){

					case MSMM_ONHOOK:			// estacao colocou o fone no gancho
						return MSI_ONHOOK;

					case MSMM_OFFHOOK:		// estacao tirou o fone do gancho
						return MSI_OFFHOOK;

					case MSMM_HOOKFLASH:	// estacao executou o HOOKFLASH
						return MSI_HOOKFLASH;

					case MSMM_RNGOFFHK:		// o ring terminou porque o fone foi retirado do gancho
						return MSI_RING_OFFHOOK;

					case MSMM_TERM:				// o ring terminou e nao atendeu
						return MSI_RING_TERM;
				}
			break;	

			default:
				return EventType;
		}
	}
	catch(...)
	{
		return 0;
	}
	return 0;
}
/********************************************************************************************
	NOME:	ProcMSI

	DESCRICAO:	Processa os eventos para a posicao de atendimento MSI
							de acordo com o manipulador de eventos passado

	PARAMETROS:	long EventHandle - Manipulador de eventos gerado pela funcao sr_waitevtEx()

	RETORNO:		Evento de acordo com Dialogic.h ou -1 se nao houve evento
********************************************************************************************/
int Dialogic::ProcMSI(long EventHandle)
{
	try {

		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case MSEV_RING:

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				switch( MsiSigEvt(Bit) ){

					case MSMM_RNGOFFHK:		// o ring terminou porque o fone foi retirado do gancho
						return MSI_RING_OFFHOOK;

					case MSMM_TERM:				// o ring terminou e nao atendeu
						return MSI_RING_TERM;

					case MSMM_RNGSTOP:		// o ring foi terminado pela funcao ms_stopfn()
						return MSI_RING_STOP;
				}
			break;

			case MSEV_NORING:	// erro ao iniciar ring
			
				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				// Se a estacao foi para OFFHOOK na hora que o ring chegou,
				// entao retorna MSI_RING_OFFHOOK, se realmente houve um erro ao gerar um Ring
				// retorna MSI_NORING para que a aplicacao possa gerar outro ring
				if(Bit == E_MSBADRNGSTA)
					return MSI_RING_OFFHOOK;
				else
					return MSI_NORING; 
			break;

			case MSEV_SIGEVT:

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));

				switch( MsiSigEvt(Bit) ){

					case MSMM_ONHOOK:			// estacao colocou o fone no gancho
						return MSI_ONHOOK;

					case MSMM_OFFHOOK:		// estacao tirou o fone do gancho
						return MSI_OFFHOOK;

					case MSMM_HOOKFLASH:	// estacao executou o HOOKFLASH
						return MSI_HOOKFLASH;

					case MSMM_RNGOFFHK:		// o ring terminou porque o fone foi retirado do gancho
						return MSI_RING_OFFHOOK;

					case MSMM_TERM:				// o ring terminou e nao atendeu
						return MSI_RING_TERM;
				}
			break;	

			default:
				return EventType;
		}
	}
	catch(...)
	{
		return 0;
	}
	return 0;
}
/********************************************************************************************
	NOME:	MsiSigEvt

	DESCRICAO:	Retorna o evento especifico do evento MSEV_SIGEVT

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
unsigned short Dialogic::MsiSigEvt(unsigned short Bit)
{
	try {

		// estacao tirou o fone do gancho
		if( (Bit & MSMM_OFFHOOK) == MSMM_OFFHOOK)
			return MSMM_OFFHOOK;

		// o ring terminou porque o fone foi retirado do gancho				
		if( (Bit & MSMM_RNGOFFHK) == MSMM_RNGOFFHK)
			return MSMM_RNGOFFHK;

		// o ring terminou porque foi chamada a funcao ms_stopfn()
		if( (Bit & MSMM_RNGSTOP) == MSMM_RNGSTOP)
			return MSMM_RNGSTOP;

		// estacao colocou o fone no gancho
		if( (Bit & MSMM_ONHOOK) == MSMM_ONHOOK )
			return MSMM_ONHOOK;

		// estacao executou o HOOKFLASH
		if( (Bit & MSMM_HOOKFLASH) == MSMM_HOOKFLASH)
			return MSMM_HOOKFLASH;

		// o ring terminou e nao atendeu
		if( (Bit & MSMM_TERM) == MSMM_TERM)
			return MSMM_TERM;

		// o primeiro ring terminou
		if( (Bit & MSMM_FIRSTRING) == MSMM_FIRSTRING)
			return MSMM_FIRSTRING;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiSigEvt()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	MsiGenRing

	DESCRICAO:	Gera ring na estacao

	PARAMETROS:	int NumRings - Numero de rings (2 a 255)

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::MsiGenRing(int NumRings)
{
	try {

		if(NumRings > 255) // o numero de rings nao pode ser maior que 255
			return -1;

		// o numero de rings deve se de pelo menos 2,
		// pois caso contrario a estacao pode nao ringar
		if(NumRings < MSI_MINIMUM_NUMBER_RINGS){
			NumRings = MSI_MINIMUM_NUMBER_RINGS;
		}

		if(ms_genring(Msi, NumRings, EV_ASYNC)){
			PutError("ms_genring() error in function MsiGenRing() Station %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiGenRing()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiStopRing

	DESCRICAO:	Para o ring estacao

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::MsiStopRing(void)
{
	try {

		if(ms_stopfn(Msi, MTF_RING)){
			PutError("ms_stopfn() error in function MsiStopRing() Station %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiStopRing()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiGenZipTone

	DESCRICAO:	Para o ring estacao

	PARAMETROS:

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::MsiGenZipTone(void)
{
	int Ret;

	try {

		EnterCriticalSection(&CritMsiZipTone);
		
		Ret = ms_genziptone(Msi);
		
		LeaveCriticalSection(&CritMsiZipTone);

		if(Ret){
			PutError("ms_genziptone() error in function MsiGenZipTone() Station %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiGenZipTone()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiHookStatus

	DESCRICAO:	Retorna o estado da estacao

	PARAMETROS: 

	RETORNO:		STATION_ONHOOK	- estacao esta com o fone no gancho
							STATION_OFFHOOK	- estacao esta com o fone fora do gancho
							-1 se erro
********************************************************************************************/
int Dialogic::MsiHookStatus(void)
{
	long Bit;

	try {		
		
		Bit = ATMS_TSSGBIT(Msi); // obtem o estado corrente da estacao
		
		if(Bit == -1){
			PutError("ATMS_TSSGBIT() error in function MsiHookStatus() Station %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;		
		}
		switch(Bit) {
			
			case MS_ONHOOK:
				return STATION_ONHOOK;
				
			case MS_OFFHOOK:
				return STATION_OFFHOOK;

			default:
				PutError("ATMS_TSSGBIT() undefined bits in function MsiHookStatus() Station %d: %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;		
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiHookStatus()");
		return -1;		
	}
	return -1;		
}
/********************************************************************************************
	NOME:	MsiExtConnectionTs

	DESCRICAO:	Faz uma conexao extendida

	PARAMETROS: long TimeSlot - Timeslot como terceira parte

							int Type		- Tipo de conexao
														LISTEN,				// A estacao de Supervisao so ouve a conexao
														COACH_PUPIL,	// A estacao de Supervisao fala com a Atendente, mas o usuario nao ouve
														CHAIR_PERSON,	// A estacao de Supervisao fala com todos
							
							long &TimeSlotConf	- Referencia para guardar o tiume slot da confencia criada
							int &IdConnection		- Referencia para guardar o Id da conexao extendida


	RETORNO:		0 sucesso ou -1 se erro
********************************************************************************************/
int Dialogic::MsiExtConnectionTs(long time_slot, int type_of_connection, long &time_slot_conf, int &id_connection)
{
	int iRet;

	try {

		// Numero real da estacao da Atendente usado como referencia, porque a conexao
		// sera extendida na placa que esta conectada a estacao da Atendente
		ms_cdt[0].chan_num	= MsiRealStation;
		ms_cdt[0].chan_sel	= MSPN_STATION;
		ms_cdt[0].chan_attr	= MSPA_PUPIL;

		// O Timeslot ou Station e mantido em ReadOnly para nao haver ruidos no momento
		// da conexao depois ele passa a ouvir o timeslot da conexao extendida
		ms_cdt[1].chan_num	= time_slot;
		ms_cdt[1].chan_sel	= MSPN_TS;
		ms_cdt[1].chan_attr	= MSPA_RO;

		// Extende a conexao SCBUS
		iRet = ms_estxtdcon(MsiBoardDev, ms_cdt, &MsiIdExtConnection);

		if(iRet == -1){
			PutError("MsiExtConnectionTs(): %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}

		id_connection = MsiIdExtConnection;

		time_slot_conf = ms_cdt[1].chan_lts;

		switch(type_of_connection){

			default:
			case EXT_LISTEN:
			break;
		
			case EXT_CHAIR_PERSON:
				ms_cdt[0].chan_num	= ms_cdt[1].chan_lts;
				ms_cdt[0].chan_sel	= MSPN_TS;
				ms_cdt[0].chan_attr	= MSPA_NULL;

				if( ms_chgxtder(MsiBoardDev, MsiIdExtConnection, ms_cdt) ){
					PutError("MsiExtConnectionTs(): %s", Canal, ATDV_ERRMSGP(Msi));
				}					
			break;
			
			case EXT_COACH_PUPIL:
				ms_cdt[0].chan_num	= ms_cdt[1].chan_lts;
				ms_cdt[0].chan_sel	= MSPN_TS;
				ms_cdt[0].chan_attr	= MSPA_COACH;

				if( ms_chgxtder(MsiBoardDev, MsiIdExtConnection, ms_cdt) ){
					PutError("MsiExtConnectionTs(): %s", Canal, ATDV_ERRMSGP(Msi));
				}					
			break;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiExtConnectionTs()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiExtConnectionSt

	DESCRICAO:	Faz uma conexao extendida

	PARAMETROS: int Station - Numero da estacao como terceira parte

							int Type		- Tipo de conexao
														LISTEN,				// A estacao de Supervisao so ouve a conexao
														COACH_PUPIL,	// A estacao de Supervisao fala com a Atendente, mas o usuario nao ouve
														CHAIR_PERSON,	// A estacao de Supervisao fala com todos
							
							long &TimeSlotConf - Referencia para guardar o tiume slot da confencia criada

	RETORNO:		0 sucesso ou -1 se erro
********************************************************************************************/
int Dialogic::MsiExtConnectionSt(int Station, int Type, long &TimeSlotConf, int &IdConnection)
{
	int iRet;

	try {

		// Numero real da estacao da Atendente usado como referencia, porque a conexao
		// sera extendida na placa que esta conectada a estacao da Atendente
		ms_cdt[0].chan_num	= MsiRealStation;
		ms_cdt[0].chan_sel	= MSPN_STATION;
		ms_cdt[0].chan_attr	= MSPA_PUPIL;

		// A terceira parte na conferencia eh uma estacao que esta na mesma placa
		// da estacao da atendente, entao ela nao e tratada como timeslot,
		// ela fica como ReadOnly para nao haver ruidos no momento
		// da conexao depois ele passa a ouvir o timeslot da conexao extendida
		ms_cdt[1].chan_num	= Station;
		ms_cdt[1].chan_sel	= MSPN_STATION;
		ms_cdt[1].chan_attr	= MSPA_RO;

		// Extende a conexao SCBUS
		iRet = ms_estxtdcon(MsiBoardDev, ms_cdt, &MsiIdExtConnection);

		if(iRet == -1){
			PutError("MsiExtConnectionSt(): %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}

		IdConnection = MsiIdExtConnection;

		TimeSlotConf = ms_cdt[1].chan_lts;

		switch(Type){

			default:
			case EXT_LISTEN:
			break;
		
			case EXT_CHAIR_PERSON:
				ms_cdt[0].chan_num	= ms_cdt[1].chan_lts;
				ms_cdt[0].chan_sel	= MSPN_TS;
				ms_cdt[0].chan_attr	= MSPA_NULL;

				if( ms_chgxtder(MsiBoardDev, MsiIdExtConnection, ms_cdt) ){
					PutError("MsiExtConnectionSt(): %s", Canal, ATDV_ERRMSGP(Msi));
					return -1;
				}
			break;
			
			case EXT_COACH_PUPIL:
				ms_cdt[0].chan_num	= ms_cdt[1].chan_lts;
				ms_cdt[0].chan_sel	= MSPN_TS;
				ms_cdt[0].chan_attr	= MSPA_COACH;

				if( ms_chgxtder(MsiBoardDev, MsiIdExtConnection, ms_cdt) ){
					PutError("MsiExtConnectionSt(): %s", Canal, ATDV_ERRMSGP(Msi));
					return -1;
				}
			break;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiExtConnectionSt()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiDelExtConnection

	DESCRICAO:	Remove uma conexao extendida

	PARAMETROS:

	RETORNO:		0 sucesso ou -1 se erro
********************************************************************************************/
int Dialogic::MsiDelExtConnection(void)
{
	try {

		if(ms_delxtdcon(MsiBoardDev, MsiIdExtConnection)){
			PutError("MsiDelExtConnection(): %s", Canal, ATDV_ERRMSGP(Msi));
		}

		MsiIdExtConnection = 0;

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiDelExtConnection()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiChangeExtConnectionTypeSt

	DESCRICAO:	Altera o atributo de uma conexao extendida quando a terceira
							parte e uma estacao

	PARAMETROS: int Station				- Numero da Estacao
							int IdConnection	- Id da conexao extendida
							int Type					-	Tipo de conexao

	RETORNO:		0 sucesso ou -1 se erro
********************************************************************************************/
int Dialogic::MsiChangeExtConnectionTypeSt(int Station, int IdConnection, int Type)
{
	try {

		ms_cdt[0].chan_num	= Station;
		ms_cdt[0].chan_sel	= MSPN_STATION;

		switch(Type){

			default:
			case EXT_LISTEN:
				ms_cdt[0].chan_attr	= MSPA_RO;
			break;
			case EXT_CHAIR_PERSON:
				ms_cdt[0].chan_attr	= MSPA_NULL;
			break;
			case EXT_COACH_PUPIL:
				ms_cdt[0].chan_attr	= MSPA_COACH;
			break;
		}

		if( ms_chgxtder(MsiBoardDev, IdConnection, ms_cdt) ){
			PutError("MsiChangeExtConnectionTypeSt(): %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiChangeExtConnectionTypeSt()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	MsiChangeExtConnectionTypeTs

	DESCRICAO:	Altera o atributo de uma conexao extendida quando a terceira
							parte e um timeslot

	PARAMETROS: int Tslot					- Timeslot
							int IdConnection	- Id da conexao extendida
							int Type					-	Tipo de conexao

	RETORNO:		0 sucesso ou -1 se erro
********************************************************************************************/
int Dialogic::MsiChangeExtConnectionTypeTs(long Tslot, int IdConnection, int Type)
{
	try {

		ms_cdt[0].chan_num	= Tslot;
		ms_cdt[0].chan_sel	= MSPN_TS;

		switch(Type){

			default:
			case EXT_LISTEN:
				ms_cdt[0].chan_attr	= MSPA_RO;
			break;
			case EXT_CHAIR_PERSON:
				ms_cdt[0].chan_attr	= MSPA_NULL;
			break;
			case EXT_COACH_PUPIL:
				ms_cdt[0].chan_attr	= MSPA_COACH;
			break;
		}

		if( ms_chgxtder(MsiBoardDev, IdConnection, ms_cdt) ){
			PutError("MsiChangeExtConnectionTypeSt(): %s", Canal, ATDV_ERRMSGP(Msi));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::MsiChangeExtConnectionTypeTs()");
		return -1;
	}
}


/********************************************************************************************
	NOME:	WaitEventEx

	DESCRICAO:	Fica esperando eventos em varios devices ao mesmo tempo

	PARAMETROS:	int iNumDevices			- Numero de devices para pegar eventos
							int iMilliseconds		- Tempo em ms para esperar por eventos, -1 espera
																		indefinidamente
							long &l_EventHandle -	Referencia para o manipulador de eventos gerado
							int &i_Dev					- Referencia para o device que recebeu o evento

	RETORNO:		Eventos das classe herdadas
********************************************************************************************/
int Dialogic::WaitEventEx(int iNumDevices, int iMilliseconds)
{
	try {

		if(FlagEndObject){
			JumpExit();
		}
		
		if(sr_waitevtEx(Devs, iNumDevices, iMilliseconds, &EventHandle) == -1){
			return -1;
		}

		Dev = sr_getevtdev(EventHandle); 

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::WaitEventEx()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	WaitEvent

	DESCRICAO:	Fica esperando eventos em apenas uma canal, ou seja, do proprio objeto

	PARAMETROS:

	RETORNO:		Eventos das classe herdadas
********************************************************************************************/
int Dialogic::WaitEvent(void)
{
	int Event;
	long Timer;

	try {
		
        //boost::this_thread::sleep_for(boost::chrono::milliseconds(10));

		Event = GetEvent(); // espera evento de telefonia

		if(Event != -1)
        {
			if(Event > T_SWITCH)
            {
				// Se o evento e de usuario guarda em LocalParam o parametro passado
				LocalParam = *((int *)sr_getevtdatap(EventHandle));
			}				
			return Event;	// Retorna o evento
		}

		if(UserTimerSet) // Se o usuario programou um timer, testa para ver se terminou
        {
			Timer = time(NULL) - UserTimerInit;

			if( Timer >= UserTimerSet )
            {
				// Se o timer do usuario terminou, retorna o evento T_TIMER e reseta o timer
				UserTimerSet	= 0;
				UserTimerInit	= 0;
				return T_TIMER;
			}
		}

		// Nao houve eventos
		return T_NO_EVENTS;
		
	}
	catch(...)
	{
		PutError("Exception Dialogic::WaitEvent()");
		return -1;
	}
}
/********************************************************************************************
	NOME: SetDevs

	DESCRICAO:	Guarda os devices que funcao WaitEventEx manipulara

	PARAMETROS:	int *iArrayDevs		- ponteiro para uma array de devices
							int iNumDevs	- numero de devices no array

	RETORNO:		Nenhum
********************************************************************************************/
void Dialogic::SetDevs(int *iArrayDevs, int iNumDevs)
{
	try {

		memset(Devs, 0, sizeof(Devs));

		for(int i = 0; i < iNumDevs; i++){
			Devs[i] = iArrayDevs[i];
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetDevs()");
	}
}
/********************************************************************************************
	NOME:	SetUserTimer

	DESCRICAO:	Habilita o timer do usuario enquanto pega eventos de Telefonia

	PARAMETROS:	long TimeLimit	- Tempo limite em segundos para a funcao WaitEvent()
																retornar o evento T_TIMER

	RETORNO:		Nenhum
********************************************************************************************/
void Dialogic::SetUserTimer(long TimeLimit)
{
	UserTimerSet	= TimeLimit;	// seta o tempo limite
	UserTimerInit = time(NULL);	// inicia e habilita o timer
}
/********************************************************************************************
	NOME:	ResetUserTimer

	DESCRICAO:	Reseta o timer do usuario enquanto pega eventos de Telefonia

	PARAMETROS:	Nenhum

	RETORNO:		Nenhum
********************************************************************************************/
void Dialogic::ResetUserTimer(void)
{
	UserTimerSet	= 0;
	UserTimerInit	= 0;
}
/********************************************************************************************
	NOME:	RegObj

	DESCRICAO:	Armazena o proprio objeto para ser usado por outros objetos

	RETORNO:
********************************************************************************************/
int Dialogic::RegObj(void)
{
	try {

		ObjectTapi[Canal] = this; // armazena o ponteiro do proprio objeto para ser usado
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RegObj()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	EndObj

	DESCRICAO:	Seta o flag FlagEndObject indicando aos objetos que devem
							fechar o handle de telefonia e terminar a thread do canal

	PARAMETROS:	

	RETORNO:
********************************************************************************************/
void Dialogic::EndObj(void)
{
	FlagEndObject = true;
}
/********************************************************************************************
	NOME:	OBJ

	DESCRICAO:	Obtem endereco do objeto 

	PARAMETROS:	int Channel - Canal do objeto

	RETORNO:		Endereco do objeto
********************************************************************************************/
CTIBase *Dialogic::OBJ(int Canal)
{
	return ObjectTapi[Canal];
}
/********************************************************************************************
	NOME:	SetJump

	DESCRICAO:	Guarda o ponteiro para o ponto de desconexao

	PARAMETROS:	jmp_buf *pJmp - ponteiro do tipo jmp_buf

	RETORNO:
********************************************************************************************/
void Dialogic::SetJump(jmp_buf *pJmp)
{
	try {

		JumpBuffer = pJmp;
	}
	catch(...)
	{
		PutError("Exception Dialogic::SetJump()");
	}
}
/********************************************************************************************
	NOME:	SendEvent

	DESCRICAO:	Envia evento para o canal Dialogic, se o device Vox estiver
							disponivel envia por ele, se nao, envia pelo Dti

	PARAMETROS:	int Channel	- Canal para ser enviado o evento				
							int Event		- Evento a ser enviado
							int Param		-	Parametro a ser passado para a thread que receber o evento

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::SendEvent(int Channel, int Event, int Param)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {

		// Utiliza preferencialmente o recurso de DTI para enviar eventos
		if(ObjectTapi[Channel]->Dti > 0){

			LocalParam = Param;
			
			if( sr_putevt(ObjectTapi[Channel]->Dti, Event, sizeof(LocalParam), &LocalParam, 0) ){

				if( Canal ){
					// Se o objeto esta associado, loga em arquivo
					CtiLog("sr_putevt() Error in Dialogic::SendEvent() Channel %d: %s",
										Channel, ATDV_ERRMSGP(ObjectTapi[Channel]->Dti));
				}
				else{
					PutError("sr_putevt() Error in Dialogic::SendEvent() Channel %d: %s",
										Channel, ATDV_ERRMSGP(ObjectTapi[Channel]->Dti));
				}

				return -1;
			}
		}
		else{

			LocalParam = Param;

			if( sr_putevt(ObjectTapi[Channel]->Vox, Event, sizeof(LocalParam), &LocalParam, 0) ){

				if( Canal ){
					// Se o objeto esta associado, loga em arquivo
					CtiLog("sr_putevt() Error in Dialogic::SendEvent() Channel %d: %s",
										Channel, ATDV_ERRMSGP(ObjectTapi[Channel]->Vox));
				}
				else{
					PutError("sr_putevt() Error in Dialogic::SendEvent() Channel %d: %s",
										Channel, ATDV_ERRMSGP(ObjectTapi[Channel]->Vox));
				}

				return -1;
			}
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::SendEvent()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	SendEvent

	DESCRICAO:	Envia evento para o canal Dialogic

	PARAMETROS:	int Channel		- Canal para ser enviado o evento				
							int Event			- Evento a ser enviado

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::SendEvent(int Channel, int Event, char *sMsg)
{
	int Dev;
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {

		switch(ObjectTapi[Channel]->TipoInterface)
		{
			case E1:
			case IP:
			case T1:
			case DTI:
				switch( ObjectTapi[Channel]->TipoSinalizacao )
				{
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_IP:
						Dev = ObjectTapi[Channel]->ldev;
					break;

					default:
						Dev = ObjectTapi[Channel]->Dti;
					break;
				}
			break;
			case LSI:
			case VOX:
				Dev = ObjectTapi[Channel]->Vox;
			break;
			case MSI:
				Dev = ObjectTapi[Channel]->Msi;
			break;
			case FAX:
				Dev = ObjectTapi[Channel]->Fax;
			break;
		}

		strcpy(ObjectTapi[Channel]->sEventString, sMsg);

		if( sr_putevt(Dev, Event, 0, NULL, 0) ){
	
			ObjectTapi[Channel]->sEventString[0] = 0;

			if( Canal ){
				// Se o objeto esta associado, loga em arquivo
				CtiLog("sr_putevt() Error in Dialogic::SendCtiEvent() Channel %d: %s",
									Channel, ATDV_ERRMSGP(Dev));
			}
			else{
				PutError("sr_putevt() Error in Dialogic::SendCtiEvent() Channel %d: %s",
									Channel, ATDV_ERRMSGP(Dev));
			}

			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::SendEvent()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	SendEvent

	DESCRICAO:	Envia evento para o canal Dialogic

	PARAMETROS:	int Channel		- Canal para ser enviado o evento				
							int Event			- Evento a ser enviado

	RETORNO:		0 sucesso
							-1 erro
********************************************************************************************/
int Dialogic::SendEvent(int Channel, int Event)
{
	int Dev;
    EventName<std::stringstream> EvtName;
    m_pLog4cpp->debug("[%p][%s] Channel %d | Event %s", this, __FUNCTION__ , Channel , EvtName.GetEventName(Event).str().c_str());

	try {

		switch(ObjectTapi[Channel]->TipoInterface)
		{
			case E1:
			case IP:
			case T1:
			case DTI:

                m_pLog4cpp->debug("[%p][%s] TipoInterface : %s",    this,   __FUNCTION__ , 
                                                                    (ObjectTapi[Channel]->TipoInterface==E1?"E1":
                                                                     ObjectTapi[Channel]->TipoInterface==IP?"IP":
                                                                     ObjectTapi[Channel]->TipoInterface==T1?"T1":"DTI"));

				switch( ObjectTapi[Channel]->TipoSinalizacao )
				{
					case GLOBAL_CALL:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_DM3:
						Dev = ObjectTapi[Channel]->ldev;
					break;

					default:
						Dev = ObjectTapi[Channel]->Dti;
					break;
				}
			break;
			case LSI:
			case VOX:
				Dev = ObjectTapi[Channel]->Vox;
			break;
			case MSI:
				Dev = ObjectTapi[Channel]->Msi;
			break;
			case FAX:
				Dev = ObjectTapi[Channel]->Fax;
			break;
		}

        m_pLog4cpp->debug("[%p][%s] Dev : %d , Event : %d",    this,   __FUNCTION__ , Dev, Event );

		if( sr_putevt(Dev, Event, 0, NULL, 0) ){

			if( Canal ){
					// Se o objeto esta associado, loga em arquivo
				CtiLog("sr_putevt() Error in Dialogic::SendCtiEvent() Channel %d: %s",
									Channel, ATDV_ERRMSGP(Dev));

                m_pLog4cpp->debug("[%p][%s] Error in Dialogic::SendCtiEvent() Channel %d: %s",    this,   __FUNCTION__ , Channel, ATDV_ERRMSGP(Dev) );
			}
			else{
				PutError("sr_putevt() Error in Dialogic::SendCtiEvent() Channel %d: %s",
									Channel, ATDV_ERRMSGP(Dev));

                m_pLog4cpp->debug("[%p][%s] Error in Dialogic::SendCtiEvent() Channel %d: %s",    this,   __FUNCTION__ , Channel, ATDV_ERRMSGP(Dev) );
			}

			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::SendEvent(int, int)");
		return -1;
	}
}
/********************************************************************************************
	NOME:	RegObjetoTomOcupado

	DESCRICAO:	Esta funcao devera ser chamada pela thread que toca tom de ocupado
							constantemente para tornar global o seu objeto, para que as 
							outras threads do sistema possam se conectar ao seu recurso de voz

	PARAMETROS:

	RETORNO:
********************************************************************************************/
void Dialogic::RegObjetoTomOcupado(void)
{
	try {

		ObjetoTomOcupado = this;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RegObjetoTomOcupado()");
	}
}
/********************************************************************************************
	NOME:	RegObjetoMusicaEspera

	DESCRICAO:	Esta funcao devera ser chamada pela thread que toca musica de espera
							constantemente para tornar global o seu objeto, para que as 
							outras threads do sistema possam se conectar ao seu recurso de voz

	PARAMETROS:

	RETORNO:
********************************************************************************************/
void Dialogic::RegObjetoMusicaEspera(void)
{
	try {

		ObjetoMusicaEspera = this;
	}
	catch(...)
	{
		PutError("Exception Dialogic::RegObjetoMusicaEspera()");
	}
}
/********************************************************************************************
	NOME:	OuveTomOcupado

	DESCRICAO:

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::OuveTomOcupado(void)
{
	int Ret;

	try {

		// faz com que o device deste objeto ouca o recurso de voz que
		// esta tocando tom de ocupado

		Ret = ScListen( ObjetoTomOcupado->GetTimeSlot() );
		return (Ret);
	}
	catch(...)
	{
		PutError("Exception Dialogic::OuveTomOcupado()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	OuveMusicaEspera

	DESCRICAO:

	PARAMETROS:

	RETORNO:
********************************************************************************************/
int Dialogic::OuveMusicaEspera(void)
{
	int Ret;	

	try {

		// faz com que o device deste objeto ouca o recurso de voz que
		// esta tocando musica de espera

		Ret = ScListen( ObjetoMusicaEspera->GetTimeSlot() );
		return (Ret);
	}
	catch(...)
	{
		PutError("Exception Dialogic::OuveMusicaEspera()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	WaitForDisconnect

	DESCRICAO:	Fica esperando por evento de desconexao, essa funcao normalmente
							sera usada por scripts

	PARAMETROS:	int TimeToWait	- tempo para esperar por desconexao

	RETORNO:		1 se houve desconexao, 0 se nao houve
********************************************************************************************/
int Dialogic::WaitForDisconnect(const int TimeToWait)
{
	int Event;

	try {

		Event = GetEvent(TimeToWait);

		switch(Event){

			case -1:
				// Nao houve evento nenhum
			return 0;

			case T_DISCONNECT:
				if(Vox <= 0){
					// Nao tem recurso de voz
					return 1;
				}
				StopCh();
			return 1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::WaitForDisconnect()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	WaitForPlayStopped

	DESCRICAO:	Fica esperando por evento de desconexao / termino de play, essa funcao normalmente
							sera usada por scripts

	PARAMETROS:	nenhum

	RETORNO:		1 se houve desconexao, 0 se nao houve
********************************************************************************************/
int Dialogic::WaitForPlayStopped()
{
	int Event;

	try
	{
		if( ATDX_STATE( Vox ) != CS_PLAY )
		{
			return 0;
		}

		do
		{
			// pega evento para que a funcao termine normalmente
			Event = GetEvent();
			// se nao existem mais eventos sai do laco
		} while( ( Event != T_DISCONNECT ) && ( Event != T_PLAY ) && ( Event != T_ERROR ) );

		if( Event == T_DISCONNECT )
		{
			if( Vox <= 0 )
			{
				// Nao tem recurso de voz
				return 1;
			}
			StopCh();
			return 1;
		}

		if( Event == T_PLAY )
		{
			return 0;
		}

		return 0;
	}
	catch(...)
	{
		PutError( "Exception Dialogic::WaitForPlayStopped()" );
		return -1;
	}
}
/********************************************************************************************
	NOME:	GetChannelStatus

	DESCRICAO:	Retorna o "status do canal". 
                � um misto de "status" do canal com o "status" da chamada. Retorna um "or" 
                dos dois.

	PARAMETROS:

	RETORNO:		Retorna o status do canal
********************************************************************************************/
int Dialogic::GetChannelStatus( int Channel )
{
    int iRet = 0;

	try
	{
		if( Channel <= 0 )
		{

            if ( (TipoInterface == E1) || (TipoInterface == IP) || (TipoInterface == T1) )
            {
                if ( (TipoSinalizacao == R2) || (TipoSinalizacao == R2_DIGITAL) )
                {
                    //////////////////////////////////////////////
					//	AR BR AT BT		STATUS				    //
					//------------------------------------------// 
					//	1  0  1  0		LIVRE				    //
					//	0  0  0  1		CONNECTADO		(IN)	//
					//	0  1  0  0		CONNECTADO		(OUT)	//
					//	0  0  1  1		SINALIZANDO		(IN)	//
					//	1  1  0  0		SINALIZANDO     (OUT)	//
					//////////////////////////////////////////////

					if( AR() && !BR()	&& AT() && !BT() )
					{
                        m_pLog4cpp->debug("[%p][%s] ChannelStatus = CHANNEL_IDLE", this, __FUNCTION__);
						this->ChannelStatus = CHANNEL_IDLE;
					}
					else if( !AR() && !BR() && !AT() && BT() )
					{
                        m_pLog4cpp->debug("[%p][%s] ChannelStatus = CHANNEL_ANSWERED", this, __FUNCTION__);
						this->ChannelStatus = CHANNEL_ANSWERED;
					}
					else if( !AR() && BR()	&& !AT() && !BT() )
					{
                        m_pLog4cpp->debug("[%p][%s] ChannelStatus = CHANNEL_CONNECTED", this, __FUNCTION__);
						this->ChannelStatus = CHANNEL_CONNECTED;
					}
					else if( !AR() && !BR() && AT() && BT() )
					{
                        m_pLog4cpp->debug("[%p][%s] ChannelStatus = CHANNEL_ALERTING", this, __FUNCTION__);
						this->ChannelStatus = CHANNEL_ALERTING;
					}
					else if( AR() && BR() && !AT() && !BT() )
					{
                        m_pLog4cpp->debug("[%p][%s] ChannelStatus = CHANNEL_DIALING", this, __FUNCTION__);
						this->ChannelStatus = CHANNEL_DIALING;
					}
					return this->ChannelStatus;
                } else if ( (TipoSinalizacao == ISDN) || (TipoSinalizacao == ISDN_4ESS) || (TipoSinalizacao == ISDN_NTT) ||
                             (TipoSinalizacao == ISDN_QSIG) || (TipoSinalizacao == ISDN_CTR4) || (TipoSinalizacao == ISDN_DMS) 
                           )
                {
                    m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] TipoSinalizacao = %s", this, 
                        (TipoSinalizacao == ISDN?"ISDN":
                        (TipoSinalizacao == ISDN_4ESS?"ISDN_4ESS":
                        (TipoSinalizacao == ISDN_NTT?"ISDN_NTT":
                        (TipoSinalizacao == ISDN_QSIG?"ISDN_QSIG":
                        (TipoSinalizacao == ISDN_CTR4?"ISDN_CTR4":"ISDN_DMS"))) )) );
					int state = 0;
					if( cc_GetBChanState( Dti, &state ) != 0 )
					{
						PutError( "Error in Dialogic::GetChannelStatus()->cc_GetBChanState()." );
						CtiLog( "%s", GetLastError() );
						return -1;
					}
					switch( state )
					{
						case ISDN_IN_SERVICE:
							if( !crn )
							{        
                                m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_IDLE", this );
								this->ChannelStatus = CHANNEL_IDLE;
								return this->ChannelStatus;
							}
							break;
						case ISDN_MAINTENANCE:
						case ISDN_OUT_OF_SERVICE:                                    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_BLOCKED", this);
							this->ChannelStatus = CHANNEL_BLOCKED;
							return this->ChannelStatus;
					}
					if( cc_CallState( crn, &state ) != 0 )
					{
						PutError( "Error in Dialogic::GetChannelStatus()->cc_CallState(). State : %d", state );
						CtiLog( "%s", GetLastError() );
						return -1;
					}
					switch( state )
					{
						case CCST_ACCEPTED:                                    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_ACCEPTED", this);
							this->ChannelStatus = CHANNEL_ACCEPTED;
							break;
								
						case CCST_ALERTING:                                    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_ALERTING", this);
							this->ChannelStatus = CHANNEL_ALERTING;
							break;

						case CCST_CONNECTED:    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_CONNECTED", this);
							this->ChannelStatus = CHANNEL_CONNECTED;
							break;

						case CCST_DIALING:                                    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_DIALING", this);
							this->ChannelStatus = CHANNEL_DIALING;
							break;

						case CCST_DISCONNECTED:                                    
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_DISCONNECTED", this);
							this->ChannelStatus = CHANNEL_DISCONNECTED;
							break;
								
						case CCST_NULL:
						case CCST_IDLE:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_IDLE", this);
							this->ChannelStatus = CHANNEL_IDLE;
							break;

						case CCST_OFFERED:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_OFFERED", this);
							this->ChannelStatus = CHANNEL_OFFERED;
							break;
					}
					return this->ChannelStatus;
                } else if ( (TipoSinalizacao == GLOBAL_CALL) || (TipoSinalizacao == GLOBAL_CALL_DM3) || 
                             (TipoSinalizacao == GLOBAL_CALL_SS7) || (TipoSinalizacao == GLOBAL_CALL_IP) )
                {
                    m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] TipoSinalizacao = %s", this, 
                        (TipoSinalizacao == GLOBAL_CALL_SS7?"GLOBAL_CALL_SS7":
                        (TipoSinalizacao == GLOBAL_CALL_IP?"GLOBAL_CALL_IP":                                
                        (TipoSinalizacao == GLOBAL_CALL?"GLOBAL_CALL":
                        (TipoSinalizacao == GLOBAL_CALL_DM3?"GLOBAL_CALL_DM3":"UNKNOWN"))))  );
					int callState = 0;
                    int channelState = 0;					
						
					if( gc_GetLinedevState( ldev, GCGLS_BCHANNEL, &channelState ) != GC_SUCCESS )
					{
						PutError( "Error in Dialogic::GetChannelStatus()->gc_GetLinedevState(): %s", gc_GetStringError() );
						CtiLog( "%s", GetLastError() );
						return -1;
					}
					        
                    m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] cc_GetBChanState state = %s", this,  
                        (channelState == GCLS_INSERVICE?"GCLS_INSERVICE":
                        (channelState == GCLS_MAINTENANCE?"GCLS_MAINTENANCE":
                        (channelState == GCLS_OUT_OF_SERVICE?"GCLS_OUT_OF_SERVICE":"UNKNOWN")) ));

					switch( channelState )
					{                                    
						case GCLS_INSERVICE:									
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_IDLE", this);
							this->ChannelStatus = CHANNEL_IDLE;
							break;
									
						case GCLS_MAINTENANCE:
						case GCLS_OUT_OF_SERVICE:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_BLOCKED", this);
							this->ChannelStatus = CHANNEL_BLOCKED;
							return this->ChannelStatus;
					}
					
                    if ( mapCrnToStatus.count(crn) == 0 )
                    {
                        this->ChannelStatus = CHANNEL_IDLE;
						return this->ChannelStatus;
                    }
					                                        

					if( gc_GetCallState( crn, &callState ) != GC_SUCCESS )
					{
						PutError( "Error in Dialogic::GetChannelStatus()->gc_GetCallState(): %s", gc_GetStringError() );
						CtiLog( "%s", GetLastError() );
                        this->ChannelStatus = CHANNEL_IDLE;
						return this->ChannelStatus;						
					}                    

					switch( callState )
					{
						case GCST_ACCEPTED:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_ACCEPTED", this);
							this->ChannelStatus = CHANNEL_ACCEPTED;
							break;
								
						case GCST_SENDMOREINFO:
						case GCST_PROCEEDING:
						case GCST_ALERTING:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_ALERTING", this);
							this->ChannelStatus = CHANNEL_ALERTING;
							break;

						case GCST_ONHOLD:
						case GCST_ONHOLDPENDINGTRANSFER:
						case GCST_CONNECTED:
							// Verifica se a chamada e de entrada ou saida.
							if( iDirection == CH_OUTBOUND )
							{
                                m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_CONNECTED", this);
								this->ChannelStatus = CHANNEL_CONNECTED;
							}
							else
							{
                                m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_ANSWERED", this);
								this->ChannelStatus = CHANNEL_ANSWERED;
							}
							break;

						case GCST_CALLROUTING:
						case GCST_DIALING:
						case GCST_DIALTONE:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_DIALING", this);
							this->ChannelStatus = CHANNEL_DIALING;
							break;

						case GCST_DISCONNECTED:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_DISCONNECTED", this);
							this->ChannelStatus = CHANNEL_DISCONNECTED;
							break;
								
						case GCST_NULL:
						case GCST_IDLE:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_IDLE", this);
							this->ChannelStatus = CHANNEL_IDLE;
							break;

						case GCST_GETMOREINFO:
						case GCST_DETECTED:
						case GCST_OFFERED:
                            m_pLog4cpp->debug("[%p][Dialogic::GetChannelStatus] ChannelStatus = CHANNEL_OFFERED", this);
							this->ChannelStatus = CHANNEL_OFFERED;
							break;
					}
					return this->ChannelStatus;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
		}
		else
		{
			if( ObjectTapi[ Channel ] )
			{
				return( ObjectTapi[ Channel ]->GetChannelStatus() );
			}
			else
			{
				return -1;
			}
		}
	}
	catch(...)
	{
		PutError( "Exception Dialogic::GetChannelStatus()" );
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::ProcR2( int Time )
{
	int term;
	unsigned int ToneId = 0;

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {
		
		Devs[0] = Vox;
		Devs[1] = Dti;
		Devs[2] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 2, TimeToWaitEvent, &EventHandle) == -1){

				/*
				switch(R2_State){
					
					case R2_GET_DNIS:
					
						if( R2TimeOutNoMoreDnis > 0 ){
							
							if( (time(NULL) - R2TimeOutNoMoreDnis) >= R2_TIME_OUT_NO_MORE_DNIS ){
							
								R2TimeOutNoMoreDnis = 0;
								// Seta o estado do canal para pegando categoria de A
								R2_State = R2_GET_CAT;
								// Pede o numero de A
								R2_PlayBackward(SIG_5); // Envia SIG_A5
							}
						}
					break;
				}*/

				return -1;
			}
		}
		else
		{
			if(sr_waitevtEx(Devs, 2, Time, &EventHandle) == -1)
				return -1;
		}

		datap			= (void *)sr_getevtdatap(EventHandle);
		
		cstp			= (DX_CST *)sr_getevtdatap(EventHandle);

		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		
		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case DTEV_SIG:                                                                                                                                        

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));                                                                                                         

				LogCasBits();

				if((Bit & DTMM_AOFF)	== DTMM_AOFF){

					//		0				0			  1				0
					if( !AR() && !BR() && AT() && !BT() ){	// Ocupacao do canal (in)

						ChannelStatus = CHANNEL_OFFERED;

						// Confirma a ocupacao do canal
						CasSetBit(BON);

						// Cria os tons para frente
						R2_CreateForwardTones();
						// Zera as variaveis
						memset(Dnis, 0, sizeof(Dnis));
						memset(Ani, 0, sizeof(Ani));
						memset(CallInfo, 0, sizeof(CallInfo));
						CountDnis		= 0;
						CountAni		= 0;
						NumMoreDnis	=	0;
						R2_AckCall	= 0;
						// Seta a Causa default da desconexao quando o sistema desliga a ligacao		
						CauseDropCall	= DROP_NORMAL;
						// Seta o estado apropriado						
						R2_State = R2_GET_DNIS;
					}
				}

				
				if((Bit & DTMM_AON)		== DTMM_AON){

					// Confirmacao de ocupacao ou troca de tons (IN)

					//	0				0				1				1		Estado anterior
					//	1				0				1				1		Estado atual
					//	1				0				1				0		Estado destino (LIVRE)

					if( AR() && !BR() && AT() && BT() ){


						// Para o canal de voz sincronamente
						
						if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
							if( !bAsrEnabled )
								dx_stopch(Vox, EV_ASYNC);
							else
								ec_stopch(Vox, SENDING, EV_ASYNC);
#else
							dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
						}

						if( FlagUserBlock ){
							ChannelStatus = CHANNEL_BLOCKED;
						}
						else{
							// Vai para LIVRE se o canal nao foi bloqueado manualmente
							CasSetBit(BOFF);
//							LogCasBits();
							ChannelStatus = CHANNEL_DISCONNECTED;
						}

						R2_State = R2_IDLE; // Seta o estado do canal para LIVRE
						FlagDisconnected = true;
						return T_DISCONNECT;
					}

					// Atendimento (IN)

					//	 0				0				0				1		Estado anterior
					//	 1				0				0				1		Estado atual

					if( AR() && !BR() && !AT() && BT() ){	// Atendimento (IN)

						ChannelStatus = CHANNEL_DISCONNECTED;

						// Para o canal de voz sincronamente
						if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
							if( !bAsrEnabled )
								dx_stopch(Vox, EV_ASYNC);
							else
								ec_stopch(Vox, SENDING, EV_ASYNC);
#else
							dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
						}

						// retorna para a aplicacao desligar
						R2_State = R2_DISCONNECT; // Seta o estado do canal para DISCONNECT
						FlagDisconnected = true;
						return T_DISCONNECT;
					}
				}

				
				if((Bit & DTMM_BON)		== DTMM_BON){
				
//					LogCasBits();

					// Condicao de Livre (IN/OUT)

					//	1				0				1				0		Estado anterior (LIVRE)
					//	1				1				1				0		Estado atual (BLOQUEIO)
					//	1				1				1				1		Estado destino (BLOQUEADO)

					if( AR() && BR() && AT() && !BT() ){

						ChannelStatus = CHANNEL_BLOCKED;

						// Para o canal de voz sincronamente
						if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
							if( !bAsrEnabled )
								dx_stopch(Vox, EV_ASYNC);
							else
								ec_stopch(Vox, SENDING, EV_ASYNC);
#else
							dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
						}

						// Vai para BLOQUEIO
						CasSetBit(BON);
//						LogCasBits();
						R2_State = R2_BLOCKED; // Seta o estado do canal para BLOQUEADO
						return T_BLOCKED;
					}
				}

				
				if((Bit & DTMM_BOFF)	== DTMM_BOFF){

//					LogCasBits();

					// Condicao de Bloqueado (IN/OUT)

					//	1				1				1				1		Estado anterior (BLOQUEADO)
					//	1				0				1				1		Estado atual (RETIRADA DO BLOQUEIO)
					//	1				0				1				0		Estado destino (LIVRE)

					if( AR() && !BR() && AT() && BT() ){

						ChannelStatus = CHANNEL_IDLE;

						// Sai do BLOQUEIO e vai para LIVRE
						CasSetBit(BOFF);
//						LogCasBits();
						R2_State = R2_IDLE; // Seta o estado do canal para LIVRE
						return T_IDLE;
					}
				}
			break;


			case TDX_CST:                                                                                                                                         

				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;

				switch(Event){

					case DE_TONEON:

						ToneId = (unsigned int) Data;
						CtiLog("DE_TONEON: ToneId=%d", ToneId);
						// Verifica se o tom e nota (de melodia).
						if( ToneId > 1000 )
						{
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							return T_NO_EVENTS;
						}

						CtiLog("Recv      #%x", SigChar(Data) - 48);

						//R2TimeOutNoMoreDnis = 0;

						switch(R2_State){

							case R2_GET_DNIS: // Pega o numero de B

								switch(Data){

									case SIG_1:		// Algarismo 1
									case SIG_2:		// Algarismo 2
									case SIG_3:		// Algarismo 3
									case SIG_4:		// Algarismo 4
									case SIG_5:		// Algarismo 5
									case SIG_6:		// Algarismo 6
									case SIG_7:		// Algarismo 7
									case SIG_8:		// Algarismo 8
									case SIG_9:		// Algarismo 9
									case SIG_10:	// Algarismo 0

										// O tamanho do numero de B requisitado ja chegou retorna para a aplicacao para
										// ver se ela quer mais.

										// Caso a aplicacao chame a funcao CallAck(int NumberOfDnis), essa devera completar a
										// sinalizacao com o numero de DNIS desejado

										// Caso a aplicacao chamar a funcao Accept(), o protocolo vai ate o ponto
										// onde sera enviado o sinal do GRUPO B que informara a CENTRAL se a ligacao
										// sera cobrada ou nao. Isso e feito atraves do parametro bBilling de Accept() ou Answer()
										// para true ou false.
										// Apos isso o protocolo retorna para a aplicacao o evento T_ACCEPT
										// Quando a aplicacao chamar a funcao Answer(void), o bit A sera setado para OFF atendendo
										// a ligacao.

										// Caso a aplicaocao chame diretamente a funcao Answer(void), o protocolo sequira ate
										// o final atendendo a ligacao

										Dnis[CountDnis] = SigChar(Data);

										CountDnis++;
										
										if(DnisVariavel){
											// Se aplicacao configurou o protocolo para DNIS variavel e o tamanho
											// do DNIS chegou ao minimo desejado, pede o numero de A
											if(DnisMinimumSize == CountDnis){
												// DNIS variavel
												R2_State = R2_GET_CAT; // Seta o estado do canal para pegando categoria de A
												// Pede o numero de A
												R2_PlayBackward(SIG_5); // Envia SIG_A5
												break;
											}
											if((DnisMinimumSize + NumMoreDnis) == CountDnis){
												// Numero de DNIS completado
												R2_State = R2_SENT_A3; // Seta o estado do canal para enviou o sinal A3
												// Preparar para recepcao dos sinais do GRUPO B
												R2_PlayBackward(SIG_3); // Envia SIG_A3
												break;
											}
											else{
												// Pede o proximo numero de B
												R2_PlayBackward(SIG_1); // Envia SIG_A1
												break;
											}
										}
										else{
                                            /*
											// Se o tamamho do numero de B e igual ao default, pede a categoria do assinante
											if(Switch::StRegister.iR2MaxDnis == CountDnis){
												
												R2_State = R2_GET_CAT; // Seta o estado do canal para pegando categoria de A
												// Pede o numero de A
												R2_PlayBackward(SIG_5); // Envia SIG_A5
												break;
											}
											else{
												// Pede o proximo numero de B
												R2_PlayBackward(SIG_1); // Envia SIG_A1
											}
                                            */
										}
									break;

									case SIG_11: // Insercao de semi-supressor de eco na origem
									case SIG_12: // Pedido recusado ou indicacao de transito internacional
									case SIG_13: // Acesso a equipamento de teste
									case SIG_14: // Insercao de semi-supressor de eco de destino
										// Ignora esses sinais e pede o proximo numero de B
										R2_PlayBackward(SIG_1); // Envia SIG_A1
									break;

									case SIG_15: // Fim de numero de B
										R2_State = R2_GET_CAT; // Seta o estado do canal para pegando categoria de A
										// Pede o numero de A
										R2_PlayBackward(SIG_5); // Envia SIG_A5
									break;
								}
							break;
									 
							case R2_GET_CAT: // Pega a categoria do assinante A

								switch(Data){
									
									case SIG_1:		// Assinante comum
									case SIG_2:		// Assinante com tarifacao especial
									case SIG_3:		// Equipamento de manutencao
									case SIG_4:		// Telefone publico local
									case SIG_5:		// Telefonista
									case SIG_6:		// Equipamentos de comunicacao de dados
									case SIG_7:		// Telefone publico interurbano
									case SIG_8:		// Comunicacao de dados, servico internacional
									case SIG_9:		// Assinante com prioridade

									
									
									case SIG_10:	// Telefonista com facilidade de transferencia
									case SIG_11:	// Assinante com facilidade de transferencia
									case SIG_12:	// Reserva
									case SIG_13:	// Reserva
									case SIG_14:	// Reserva
									case SIG_15:	// Reserva
										CallInfo[0] = SigChar(Data);
										CallInfo[1] = 0;
										R2_State = R2_GET_ANI; // Seta o estado do canal para pegando numero de A
										// Pede o numero de A
										R2_PlayBackward(SIG_5); // Envia SIG_A5
									break;

									default:
										CallInfo[0] = SigChar(1);
										CallInfo[1] = 0;
										R2_State = R2_GET_ANI; // Seta o estado do canal para pegando numero de A
										// Pede o numero de A
										R2_PlayBackward(SIG_5); // Envia SIG_A5
									break;
								}
							break;

							case R2_GET_ANI:

								switch(Data){
									case SIG_1:		// Algarismo 1
									case SIG_2:		// Algarismo 2
									case SIG_3:		// Algarismo 3
									case SIG_4:		// Algarismo 4
									case SIG_5:		// Algarismo 5
									case SIG_6:		// Algarismo 6
									case SIG_7:		// Algarismo 7
									case SIG_8:		// Algarismo 8
									case SIG_9:		// Algarismo 9
									case SIG_10:	// Algarismo 0
										Ani[CountAni] = SigChar(Data);
										CountAni++;
										// Pega o proximo numero de A
										R2_PlayBackward(SIG_5); // Envia SIG_A5
									break;
															 
									case SIG_11: // Insercao de semi-supressor de eco na origem
									case SIG_12: // Pedido recusado ou indicacao de transito internacional
									case SIG_13: // Acesso a equipamento de teste
									case SIG_14: // Insercao de semi-supressor de eco de destino
										// Ignora esses sinais e pede o proximo numero de A
										R2_PlayBackward(SIG_5); // Envia SIG_A5
									break;

									case SIG_15: // Fim de numero de A
										if(DnisVariavel){
											// Flag indicando se sera necessario pegar mais numero de B
											// A aplicacao deve chamar a funcao CallAck() para pegar mais numero de B,
											// retorna para a aplicacao
											FlagDisconnected = false;
											iDirection = CH_INBOUND;
											return T_CALL;
										}

										R2_State = R2_SENT_A3; // Seta o estado do canal para enviou o sinal A3
										// Preparar para recepcao dos sinais do GRUPO B
										R2_PlayBackward(SIG_3); // Envia SIG_A3
									break;
								}
							break;

							case R2_SENT_A3:
								// A Origem envia um sinal com a categoria do assinante so para completar a sequencia

								R2_State = R2_SEND_GROUP_B; // Seta o estado do canal para pegando numero de A

								if(R2_AckCall){
									// Se a aplicacao configurou o protocolo para DNIS variavel retorna informando que o
									// ja terminou a coleta
									return T_ACKCALL;
								}
								else{
									// Retorno normal para a aplicacao atender a ligacao
									FlagDisconnected = false;
									iDirection = CH_INBOUND;
									return T_CALL;
								}
							break;
						} // switch(R2_State)
					break;

					case DE_TONEOFF:
						switch(R2_State){
							case R2_IDLE:
							case R2_GET_DNIS:
							case R2_GET_CAT:
							case R2_GET_ANI:
							case R2_SENT_A3:
							case R2_SEND_GROUP_B:
							case R2_SEND_AOFF:

								// Seta time para verificacao de DNIS de tamanho menor que o protocolo
								// esta pedindo para a TELE (DIAL IN)
								//R2TimeOutNoMoreDnis = time(NULL);
								
								// Para o tom que esta sendo enviado para a CENTRAL, por que ela parou de tocar o dela
								if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
								if( !bAsrEnabled )
									dx_stopch(Vox, EV_ASYNC);
								else
									ec_stopch(Vox, SENDING, EV_ASYNC);
#else
								dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
								}
							break;
						}
					break;

					case DE_DIGITS:
						LastDigit = (char) Data; // guarda o digito recebido
						return T_DIGIT;
				}
			break;

			case TDX_PLAYTONE:

				term = ATDX_TERMMSK(Vox);
				
				if(term & TM_USRSTOP) {
					// O tom foi interrompido pela funcao dx_stopch(), pois foi
					// detectado que o equipamento de origem enviou seu tom de resposta
					switch(R2_State){

						case R2_IDLE:					// (IN)
						case R2_GET_DNIS:			// (IN)
						case R2_GET_ANI:			// (IN)
						case R2_GET_CAT:			// (IN)
						case R2_SENT_A3:			// (IN)
						break;

						case R2_SEND_GROUP_B: //  (IN)
								// Tom do GRUPO B completado a aplicacao pode atender a ligacao
							// O canal enviou o sinal do grupo B e agora falta atender								
							R2_State = R2_SEND_AOFF;
						return T_ACCEPT;
					}
				}
				else{
					// O tom terminou normalmente, sem ser interrompido, erro nao hora de discar (DIAL OUT)
				}
			break;

			case TDX_PLAY:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_PLAY;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_ERROR:
				return T_ERROR;

			case T_USER_BLOCK: // Evento para bloquear o canal
                CtiLog("T_USER_BLOCK");
				FlagUserBlock = true;
				return T_USER_BLOCK;

			case T_USER_UNBLOCK: // Evento para desbloquear o canal
				FlagUserBlock = false;
				return T_USER_UNBLOCK;

			default:
				return EventType;
		}

		return -1; // Sem eventos
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcR2()");
		return -1;
	}
}


//------------------------------------------------------------------------------------------------------------------------
/*

TELE              EQUIP

1	->							
									TONEON
									<- A1
TONEON
STOP_TONE
									TONEOFF
									STOP_TONE
TONEOFF
2->								
									TONEON
									<- A1
TONEON
STOP_TONE
									TONEOFF
									STOP_TONE
									
									START TIME
NAO EXISTE
ALGARISMO
PARA ENVIAR

									TIME OUT

									<- A5 OU A3

*/


int Dialogic::ProcISDN( int Time )
{
	try {
		
		unsigned int ToneId = 0;
		int iBChannelState = 0;
		
		Devs[0] = Dti;
		Devs[1] = Vox;
		Devs[2] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 2, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 2, Time, &EventHandle) == -1)
				return -1;
		}

		datap			= (void *)sr_getevtdatap(EventHandle);
		
		cstp			= (DX_CST *)sr_getevtdatap(EventHandle);

		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		
		EventType	= sr_getevttype(EventHandle);

		switch(EventType){
		
			case CCEV_SETCHANSTATE:
				cc_GetBChanState( Dti, &iBChannelState );
				if( iBChannelState == ISDN_IN_SERVICE )
				{
					return T_IDLE;
				}
				else
				{
					return T_BLOCKED;
				}

			case CCEV_TASKFAIL:
				strcpy(sReasonDisconnect, "TASKFAIL");
				ChannelStatus = CHANNEL_DISCONNECTED;
				FlagDisconnected = true;
			return T_DISCONNECT;

			case CCEV_RESTART:
				ChannelStatus = CHANNEL_BLOCKED;
			return T_RESET;

			case CCEV_DISCONNECTED:
				ChannelStatus = CHANNEL_DISCONNECTED;
				ISDN_ReasonDisconnect();
				FlagDisconnected = true;
			return T_DISCONNECT;

			case CCEV_DROPCALL:
				ChannelStatus = CHANNEL_IDLE;
				ScConnect(FULLDUP);
			return T_DROPCALL;

			case CCEV_CALLPROGRESS:
			case CCEV_ALERTING:
				// Terminou a sinalizacao, canal de voz aberto (SAIDA)
				// O evento CCEV_CALLPROGRESS pode vir no lugar do evento GCEV_ALERTING
				// caso seja feita uma ligacao para celular, caso venha este evento,
				// o evento alerting nao vira
				ChannelStatus = CHANNEL_ALERTING;
			return T_ALERTING;

			case CCEV_CONNECTED: // Ligacao conectada (SAIDA)
				ChannelStatus = CHANNEL_CONNECTED;
			return T_CONNECT;

			case CCEV_OFFERED: // uma chamada acaba de chegar (ENTRADA)
				ChannelStatus = CHANNEL_OFFERED;
				memset(Dnis,			0, sizeof(Dnis));
				memset(Ani,				0, sizeof(Ani));
				memset(CallInfo,	0, sizeof(CallInfo));
				if(cc_GetCRN(&crn, datap) != 0){
					return -1;
				}
				if(cc_GetDNIS(crn, Dnis) != 0){
					// IsdnReasonCode();
				}
				if(cc_GetANI(crn, Ani) != 0){
					// IsdnReasonCode();
				}
				if(cc_AcceptCall(crn, 0, EV_ASYNC) != 0){
				}
				iDirection = CH_INBOUND;
			return -1;

			case CCEV_ACCEPT: // a chamada foi aceita (ENTRADA)
				ChannelStatus = CHANNEL_ACCEPTED;
				FlagDisconnected = false;
			return T_CALL;

			case CCEV_ANSWERED:	// Ligacao atendida (ENTRADA)
				ChannelStatus = CHANNEL_ANSWERED;
			return T_ANSWER;
			
			case TDX_CST:                                                                                                                                         
				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;
				
				switch(Event)
				{
						case DE_TONEON:
							ToneId = (unsigned int) Data;
							CtiLog("DE_TONEON: ToneId=%d", ToneId);
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							return T_NO_EVENTS;

					case DE_DIGITS:
						LastDigit = (char) Data; // guarda o digito recebido
					return T_DIGIT;
				}
			break;

			case TDX_PLAY:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_PLAY;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_PLAYTONE:
				return T_PLAYTONE;

			case TDX_ERROR:
				return T_ERROR;

			case T_USER_BLOCK: // Evento para bloquear o canal
                CtiLog("T_USER_BLOCK");
				FlagUserBlock = true;
				return T_USER_BLOCK;

			case T_USER_UNBLOCK: // Evento para desbloquear o canal
				FlagUserBlock = false;
				return T_USER_UNBLOCK;

			default:
				return EventType;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcISDN()");
		return -1;
	}
	return -1;
}
/********************************************************************************************
	NOME:	DialISDN

	DESCRICAO:	Disca pelo canal digital em protocolo ISDN assincronamente, a thread que
							chamou deve controlar os eventos

	PARAMETROS:	Nenhum

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::DialISDNAsync(void)
{
	try {

		ISDN_BuildMakecallBlk(Ani);

		if(cc_MakeCall(Dti, &crn, Dnis, &makecall_blk, 0, EV_ASYNC)){
			return -1;
		}
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialISDNAsync()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	 ISDN_BuildMakecallBlk

	DESCRICAO:	Inicializa a estrura de parametros para discagem em ISDN

	PARAMETROS:	char *OriginationPhoneNumber

	RETORNO:		Nenhum
********************************************************************************************/
void Dialogic::ISDN_BuildMakecallBlk(char *OriginationPhoneNumber)
{
	try {

		switch(TipoSinalizacao){

			case ISDN:
				// ESTRUTURA PARA FAZER LIGACOES BACK TO BACK NO ESCRITORIO

				memset(&makecall_blk, 0xFF, sizeof(MAKECALL_BLK));

				makecall_blk.isdn.BC_xfer_cap											=	BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_mode										=	ISDN_ITM_CIRCUIT;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.usrinfo_layer1_protocol					= ISDN_UIL1_G711ALAW;

				makecall_blk.isdn.usr_rate												= ISDN_NOTUSED;

				makecall_blk.isdn.destination_number_type					= NAT_NUMBER;
				makecall_blk.isdn.destination_number_plan					= ISDN_NUMB_PLAN;

				makecall_blk.isdn.destination_sub_number_type			= ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_phone_number[0]	= 0;
				
				makecall_blk.isdn.origination_number_type					= NAT_NUMBER;
				makecall_blk.isdn.origination_number_plan					= ISDN_NUMB_PLAN;

				makecall_blk.isdn.origination_sub_number_type			= ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_phone_number[0]	= 0;

				strcpy(makecall_blk.isdn.origination_phone_number, OriginationPhoneNumber);
				
				makecall_blk.isdn.facility_feature_service				= ISDN_NOTUSED;
				makecall_blk.isdn.facility_coding_value						= ISDN_NOTUSED;

				makecall_blk.isdn.facility_feature_service				= ISDN_SERVICE;
				makecall_blk.isdn.facility_coding_value						= ISDN_NOTUSED;

				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;

			case ISDN_4ESS:
				makecall_blk.isdn.BC_xfer_cap											= BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.BC_xfer_mode										= ISDN_ITM_CIRCUIT;
				makecall_blk.isdn.usrinfo_layer1_protocol					= ISDN_UIL1_G711ULAW;
				makecall_blk.isdn.usr_rate												=	ISDN_NOTUSED;
				makecall_blk.isdn.facility_feature_service				= ISDN_SERVICE;
				makecall_blk.isdn.facility_coding_value						=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_number_plan					=	ISDN_NUMB_PLAN;

				// se o primeiro digito do numero de destino discado pelo usuario for = '1'
				// entao o numero desejado e nacional, caso contrario e internacional
				if(1)
					makecall_blk.isdn.destination_number_type				=	NAT_NUMBER;
				else
					makecall_blk.isdn.destination_number_type				=	INTL_NUMBER;
				
				makecall_blk.isdn.origination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.origination_number_plan					=	ISDN_NOTUSED;
				
				makecall_blk.isdn.destination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_number_plan			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_plan			=	ISDN_NOTUSED;
				
				makecall_blk.isdn.destination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.origination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;
			
			case ISDN_NTT:
				makecall_blk.isdn.BC_xfer_cap											= BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.BC_xfer_mode										= ISDN_ITM_CIRCUIT;
				makecall_blk.isdn.usrinfo_layer1_protocol					= ISDN_UIL1_G711ULAW;
				makecall_blk.isdn.facility_feature_service				= 0xff;
				makecall_blk.isdn.facility_coding_value						=	0xff;
				makecall_blk.isdn.destination_number_type					=	0xff;
				makecall_blk.isdn.destination_number_plan					=	0x00;
				makecall_blk.isdn.origination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.origination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;
			
			case ISDN_QSIG:
				makecall_blk.isdn.BC_xfer_cap											= BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.BC_xfer_mode										= ISDN_ITM_CIRCUIT;
				
				if(TipoInterface == E1)
					makecall_blk.isdn.usrinfo_layer1_protocol				= ISDN_UIL1_G711ALAW;
				else
					makecall_blk.isdn.usrinfo_layer1_protocol				= ISDN_UIL1_G711ULAW;
				
				makecall_blk.isdn.usr_rate												=	ISDN_NOTUSED;
				makecall_blk.isdn.facility_feature_service				= ISDN_SERVICE;
				makecall_blk.isdn.facility_coding_value						=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.origination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.destination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.origination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.destination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_number_plan			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_plan			=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.origination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;

			case ISDN_CTR4:
				makecall_blk.isdn.BC_xfer_cap											= BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.BC_xfer_mode										= ISDN_ITM_CIRCUIT;
				makecall_blk.isdn.usrinfo_layer1_protocol					= ISDN_UIL1_G711ALAW;
				makecall_blk.isdn.usr_rate												=	ISDN_NOTUSED;
				makecall_blk.isdn.facility_feature_service				= ISDN_SERVICE;
				makecall_blk.isdn.facility_coding_value						=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.origination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.destination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.origination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.destination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_type			=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_number_plan			=	ISDN_NOTUSED;
				makecall_blk.isdn.origination_sub_number_plan			=	ISDN_NOTUSED;
				makecall_blk.isdn.destination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.origination_sub_phone_number[0]	= 0;
				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;
			
			case ISDN_DMS:
				makecall_blk.isdn.BC_xfer_cap											= BEAR_CAP_SPEECH;
				makecall_blk.isdn.BC_xfer_rate										=	BEAR_RATE_64KBPS;
				makecall_blk.isdn.BC_xfer_mode										= ISDN_ITM_CIRCUIT;
				makecall_blk.isdn.usrinfo_layer1_protocol					= ISDN_UIL1_G711ULAW;
				makecall_blk.isdn.facility_feature_service				= 0xff;
				makecall_blk.isdn.facility_coding_value						=	0xff;
				makecall_blk.isdn.destination_number_type					=	0xff;
				makecall_blk.isdn.destination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.origination_number_type					=	NAT_NUMBER;
				makecall_blk.isdn.origination_number_plan					=	ISDN_NUMB_PLAN;
				makecall_blk.isdn.usrinfo_bufp										= NULL;
				makecall_blk.isdn.nsfc_bufp												= NULL;
			break;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::BuildMakecallBlk()");
	}
}

/*

void f_blk_isdn(int canal, int protocolo_isdn)
{
	char string[256];
	__try {

	memset(&config_canal[canal].blk, ISDN_NOTUSED, sizeof(config_canal[canal].blk));

	memset(makecall_blk.isdn.destination_sub_phone_number, 0, sizeof(makecall_blk.isdn.destination_sub_phone_number));
	memset(makecall_blk.isdn.origination_sub_phone_number, 0, sizeof(makecall_blk.isdn.origination_sub_phone_number));
		
	switch(protocolo_isdn){

		case PROT_ISDN_4ESS:	// protocolo ISDN AT&T

			
		break;

		case PROT_ISDN_NTT:		// protocolo ISDN NTT JAPAO
		break;

		case PROT_ISDN_QSIG:	// protocolo ISDN QSIG E1/T1
		break;

		case PROT_ISDN_CTR4:	// protocolo ISDN EURO ISDN
			
		break;

		case PROT_ISDN_DMS:	// ISDN NORTHERN TELECOM CUSTOM SWITCH A211-1 AND A211-4
			
		break;
	}

	memset(makecall_blk.isdn.origination_phone_number, 0, sizeof(makecall_blk.isdn.origination_phone_number));
	strcpy(makecall_blk.isdn.origination_phone_number, var[canal].numero_chamador);
}
__except(EXCEPTION_EXECUTE_HANDLER){
	f_log_exception(0, "f_blk_isdn");
}
}
 */

/********************************************************************************************
	Funcao:			ISDN_ReasonDisconnect

	Comentario	Coloca na variavel  global uma string com a descricao da causa
							da desconexao

	Parametros:	int canal				Canal dialogic
							int reason			razao da desconexao

	Retorno:		nenhum
********************************************************************************************/
void Dialogic::ISDN_ReasonDisconnect(void)
{
	try {
		
		int iCode;

		iCode = cc_ResultValue(datap) - ERR_ISDN_CAUSE;

		switch(iCode) {

			case NORMAL_CLEARING:
				DisconnectReasonCode = REASON_NORMAL;
				strcpy(sReasonDisconnect,"NORMAL_CLEARING");
			break;
			case USER_BUSY:
				DisconnectReasonCode = REASON_BUSY;
				strcpy(sReasonDisconnect,"USER_BUSY");
			break;
			case NO_USER_RESPONDING:
				DisconnectReasonCode = REASON_NOANSWER;
				strcpy(sReasonDisconnect,"NO_USER_RESPONDING");
			break;
			case NO_ANSWER_FROM_USER:
				DisconnectReasonCode = REASON_NOANSWER;
				strcpy(sReasonDisconnect,"NO_ANSWER_FROM_USER");
			break;
			case NETWORK_CONGESTION:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NETWORK_CONGESTION");
			break;
			case UNASSIGNED_NUMBER:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"UNASSIGNED_NUMBER");
			break;
			case NO_ROUTE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NO_ROUTE");
			break;
			case CHANNEL_UNACCEPTABLE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"CHANNEL_UNACCEPTABLE");
			break;
			case CALL_REJECTED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"CALL_REJECTED");
			break;
			case NUMBER_CHANGED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NUMBER_CHANGED");
			break;
			case DEST_OUT_OF_ORDER:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"DEST_OUT_OF_ORDER");
			break;
			case INVALID_NUMBER_FORMAT:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INVALID_NUMBER_FORMAT");
			break;
			case FACILITY_REJECTED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"FACILITY_REJECTED");
			break;
			case RESP_TO_STAT_ENQ:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"RESP_TO_STAT_ENQ");
			break;
			case UNSPECIFIED_CAUSE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"UNSPECIFIED_CAUSE");
			break;
			case NO_CIRCUIT_AVAILABLE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NO_CIRCUIT_AVAILABLE");
			break;
			case NETWORK_OUT_OF_ORDER:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NETWORK_OUT_OF_ORDER");
			break;
			case TEMPORARY_FAILURE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"TEMPORARY_FAILURE");
			break;
			case ACCESS_INFO_DISCARDED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"ACCESS_INFO_DISCARDED");
			break;
			case REQ_CHANNEL_NOT_AVAIL:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"REQ_CHANNEL_NOT_AVAIL");
			break;
			case PRE_EMPTED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"PRE_EMPTED");
			break;
			case FACILITY_NOT_SUBSCRIBED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"FACILITY_NOT_SUBSCRIBED");
			break;
			case OUTGOING_CALL_BARRED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"OUTGOING_CALL_BARRED");
			break;
			case INCOMING_CALL_BARRED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INCOMING_CALL_BARRED");	
			break;
			case BEAR_CAP_NOT_AVAIL:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"BEAR_CAP_NOT_AVAIL");
			break;
			case SERVICE_NOT_AVAIL:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"SERVICE_NOT_AVAIL");
			break;
			case CAP_NOT_IMPLEMENTED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"CAP_NOT_IMPLEMENTED");
			break;
			case CHAN_NOT_IMPLEMENTED:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"CHAN_NOT_IMPLEMENTED");
			break;
			case FACILITY_NOT_IMPLEMENT:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"FACILITY_NOT_IMPLEMENT");
			break;
			case INVALID_CALL_REF:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INVALID_CALL_REF");
			break;
			case CHAN_DOES_NOT_EXIST:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"CHAN_DOES_NOT_EXIST");
			break;
			case INCOMPATIBLE_DEST:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INCOMPATIBLE_DEST");
			break;
			case INVALID_MSG_UNSPEC:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INVALID_MSG_UNSPEC");
			break;
			case MANDATORY_IE_MISSING:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"MANDATORY_IE_MISSING");
			break;
			case NONEXISTENT_MSG:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"NONEXISTENT_MSG");
			break;
			case WRONG_MESSAGE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"WRONG_MESSAGE");
			break;
			case BAD_INFO_ELEM:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"BAD_INFO_ELEM");
			break;
			case INVALID_ELEM_CONTENTS:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INVALID_ELEM_CONTENTS");
			break;
			case WRONG_MSG_FOR_STATE:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"WRONG_MSG_FOR_STATE");
			break;
			case TIMER_EXPIRY:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"TIMER_EXPIRY");
			break;   
			case MANDATORY_IE_LEN_ERR:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"MANDATORY_IE_LEN_ERR");
			break;   
			case PROTOCOL_ERROR:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"PROTOCOL_ERROR");
			break;   
			case INTERWORKING_UNSPEC:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"INTERWORKING_UNSPEC");
			break;
			default:
				DisconnectReasonCode = REASON_CONGESTION;
				strcpy(sReasonDisconnect,"REASON NOT FOUND");
			break;
		}

		CtiLog(sReasonDisconnect);
	}
	catch(...){
	}
}


/********************************************************************************************
	NOME:	ProcGCGeneric

	DESCRICAO:	Processa os eventos de GlobalCall para canal de voz e DTI

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::ProcGCGeneric( int Time )
{
    std::stringstream ss;

	try
	{  

		unsigned int count = 0, ToneId = 0;
		CRN l_crn;

		Devs[count++] = Vox;
		if(Dti >= 0)
			Devs[count++] = Dti;
		if(dtidev >= 0)
			Devs[count++] = dtidev;
		if(ipdev >= 0)
			Devs[count++] = ipdev;

		Devs[count] = 0;

		if(FlagEndObject)
        {
			JumpExit();
		}

		if( Time < 0 )
		{
			if( sr_waitevtEx( Devs, count, TimeToWaitEvent, &EventHandle ) == -1 )
				return -1;
		}
		else
		{
			if( sr_waitevtEx( Devs, count, Time, &EventHandle ) == -1 )
				return -1;
		}

		if( gc_GetMetaEventEx( &metaevent, EventHandle ) != GC_SUCCESS )
			return -1;

		cstp			= (DX_CST *) metaevent.evtdatap;
		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		EventType	=	metaevent.evttype;

		if( metaevent.flags & GCME_GC_EVENT )
		{
			ldev			=	metaevent.linedev;

			if( gc_GetCRN( &l_crn, &metaevent ) != GC_SUCCESS )
			{
				PutError( "gc_GetCRN() Error in Dialogic::ProcGCGeneric(). %s.", gc_GetStringError() );
				CtiLog( "%s", GetLastError() );
				return -1;
			}

            m_pLog4cpp->debug("[%p][%s] Canal %d - GlobalCall event received: %s, crn=%d", this, __FUNCTION__ , Canal, GCEV_MSG( EventType ), l_crn);
			//CtiLog( "GlobalCall event received: %s, crn=%d", GCEV_MSG( EventType ), l_crn );
			crn = l_crn;

            //auto itRange = mmapDev2Crn.equal_range(ldev);

			switch( EventType )
			{
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  LINE-RELATED EVENTS  /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				case GCEV_OPENEX:
					return T_OPENEX;
					break;

				case GCEV_OPENEX_FAIL:
					return T_TASKFAIL;

				case GCEV_SETCHANSTATE:
					return T_NO_EVENTS;

				case GCEV_RESETLINEDEV:
					if( gc_WaitCall( ldev, NULL, NULL, 0, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::ProcGCGeneric()->gc_WaitCall(): %s", gc_GetStringError() );
						CtiLog( "%s", GetLastError() );
						return -1;
					}

					if (ldev == ipdev)
					{
						VoxScListen(TimeslotLineIP);
						ScListen(TimeslotVox);
					}

					ChannelStatus = CHANNEL_IDLE;
                    
					MessageBoxMelody = 0;
					return T_IDLE;

                // Unsolicited event.
                case GCEV_BLOCKED:
					m_pLog4cpp->debug("[%p][%s] Event received : GCEV_BLOCKED ", this, __FUNCTION__ );                    

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {                        
                        if (kvCallStats.second == GCST_IDLE) 
                        {
                            m_pLog4cpp->debug("[%p][%s] Set ChannelStatus = CHANNEL_BLOCKED", this, __FUNCTION__ );
                            this->ChannelStatus = CHANNEL_BLOCKED;
					        if( gc_SetChanState( ldev, GCLS_OUT_OF_SERVICE, EV_ASYNC ) !=  0 )
					        {
						        PutError( "Error in Dialogic::ProcGCGeneric()->gc_SetChanState(): %s", gc_GetStringError() );
						        return -1;
					        }

                            MessageBoxMelody = 0;

                            ss.str(std::string());
                            ss << std::endl;
                            ss << "======================================================" << std::endl;
                            ss << "                                                      " << std::endl;
                            ss << "          !!!!!!!!  JUMP_BLOCKED EVENT !!!!!!!!       " << std::endl;
                            ss << "                                                      " << std::endl;
                            ss << "              NO LONG JUMP GENERATED !!!              " << std::endl;
                            ss << "======================================================" << std::endl;
                            m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , ss.str().c_str() );

                            return (int)T_BLOCKED;
                        }
                    });


                    /*
					if( JumpBuffer )
						longjmp( *JumpBuffer, JUMP_BLOCKED ); // vai para o ponto de controle de bloqueio
                    */
                    return T_NO_EVENTS;

					

				// Unsolicited event.
				case GCEV_UNBLOCKED:
					long tslot;
					SC_TSINFO sc_tsinfo;

                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_UNBLOCKED ", this, __FUNCTION__ );
                    
					if( (ldev == ipdev) && !FlagIPChannelInit )
					{
						FlagIPChannelInit = true;
						sc_tsinfo.sc_numts		= 1;
						sc_tsinfo.sc_tsarrayp = &tslot;
						// Get the TX timeslot of the IP voice device
						if(gc_GetXmitSlot(ipdev, &sc_tsinfo) != GC_SUCCESS){
							PutError("gc_GetXmitSlot() error in function OpenChannelIP() Channel %d: %s", Canal, gc_GetStringError());
							return -1;
						}
						TimeslotLineIP = tslot;
						CtiLog( "IP TX ts: %ld", TimeslotLineIP );
					}
                    
                    /*
                    m_pLog4cpp->debug("[%p][%s] Set ChannelStatus = CHANNEL_IDLE", this, __FUNCTION__ );
                    ChannelStatus = CHANNEL_IDLE;
					if( gc_SetChanState( ldev, GCLS_INSERVICE, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::ProcGCSS7()->gc_SetChanState(): %s", gc_GetStringError() );
						return -1;
					}
                    */
                                                
                    
                    m_pLog4cpp->debug("[%p][%s] gc_ResetLineDev will be called.", this, __FUNCTION__ );

                    if( gc_ResetLineDev( ldev, EV_ASYNC ) !=  0 )
					{
						PutError("Error in Dialogic::ProcGCGeneric()->gc_ResetLineDev(): %s", gc_GetStringError());
						CtiLog( "%s", GetLastError() );
						return -1;
					}
                    

					return T_UNBLOCKED;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  CALL-RELATED EVENTS (INBOUND/OUTBOUND)  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				// Unsolicited event.
				case GCEV_TASKFAIL:
				case GCEV_RELEASECALL_FAIL:
				{
					GC_INFO gc_info;
					memset( &gc_info, 0, sizeof(gc_info) );
					if( gc_ResultInfo( &metaevent, &gc_info ) != 0 )
					{
						PutError("Error in Dialogic::ProcGCGeneric()->gc_ResultInfo(): %s", gc_GetStringError());
						CtiLog( "%s", GetLastError() );
						return -1;
					}
					CtiLog( "Task fail cause: gcValue=%d. gcMsg=%s. ccValue=%d. ccMsg=%s. additionalInfo=%s.", gc_info.gcValue, gc_info.gcMsg, gc_info.ccValue, gc_info.ccMsg, gc_info.additionalInfo );

					if( gc_ResetLineDev( ldev, EV_ASYNC ) !=  0 )
					{
						PutError("Error in Dialogic::ProcGCGeneric()->gc_ResetLineDev(): %s", gc_GetStringError());
						CtiLog( "%s", GetLastError() );
					}
					return T_TASKFAIL;
				}

				// Unsolicited event.
				case GCEV_DISCONNECTED:
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_DISCONNECTED ", this, __FUNCTION__ );
                    

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {                        
                        if ( (kvCallStats.second == GCST_DIALING) || (kvCallStats.second == GCST_OFFERED) || (kvCallStats.second == GCST_ACCEPTED) || 
                             (kvCallStats.second == GCST_CONNECTED) || (kvCallStats.second == GCST_ALERTING) ) 
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_DISCONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_DISCONNECTED", this, __FUNCTION__ , kvCallStats.first );  

                            if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                            {
						        PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
						        CtiLog( "%s", GetLastError() );
						        return -1;
					        }
                        }
                    });
                    
					
					GC_ReasonDisconnect();
					FlagDisconnected = true;

					if( DisconnectReasonCode == GCRV_NOANSWER )
						return T_NOANSWER;
					else											
					    return T_DISCONNECT;
					

				case GCEV_RELEASECALL:
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_RELEASECALL ", this, __FUNCTION__ );

                    if ( mapCrnToStatus.count(crn) )
                    {
                        long callStatus = mapCrnToStatus[crn];
                        if (callStatus == GCST_IDLE)
                        {
                            mapCrnToStatus[crn] = GCST_NULL;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_NULL", this, __FUNCTION__ , crn);                            

                            mapCrnToStatus.erase(crn);
                            m_pLog4cpp->debug("[%p][%s]  (crn = %ld) removed from map < CRN -> CALL STATUS >", this, __FUNCTION__ , crn);
                        } else if ( (callStatus == GCST_DISCONNECTED) || (callStatus == GCST_DETECTED) || (callStatus == GCST_DIALING) || 
                             (callStatus == GCST_OFFERED) || (callStatus == GCST_ACCEPTED) || (callStatus == GCST_CONNECTED) ||
                             (callStatus == GCST_PROCEEDING) || (callStatus == GCST_ALERTING)
                             ) {
                            mapCrnToStatus[crn] = GCST_IDLE;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , crn);
                        }
                    }


					MessageBoxMelody = 0;
					return T_NO_EVENTS;

				case GCEV_DROPCALL:
					FlagDisconnected = true;
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_DROPCALL ", this, __FUNCTION__ );                      
                    
                    if ( mapCrnToStatus.count(crn) )
                    {
                        long callStatus = mapCrnToStatus[crn];

                        if ( (callStatus == GCST_DISCONNECTED) || (callStatus == GCST_DETECTED) || (callStatus == GCST_DIALING) || 
                             (callStatus == GCST_OFFERED) || (callStatus == GCST_ACCEPTED) || (callStatus == GCST_CONNECTED) ||
                             (callStatus == GCST_PROCEEDING) || (callStatus == GCST_ALERTING)
                             )
                        {

                            mapCrnToStatus[crn] = GCST_IDLE;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , crn);

                            if( gc_ReleaseCallEx( crn, EV_ASYNC ) != GC_SUCCESS )
                            {
							    PutError( "Error in Dialogic::Disconnect()->gc_ReleaseCallEx(): %s", gc_GetStringError() );
							    CtiLog( "%s", GetLastError() );
							    return -1;
						    }
                            else if (callStatus == GCST_IDLE)
                            {
                                mapCrnToStatus[crn] = GCST_NULL;                            
                                m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_NULL", this, __FUNCTION__ , crn);                            
                            }
                        }
                    }
                    
					return T_DROPCALL;

				case GCEV_SETBILLING:
					return T_SETBILLING;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  CALL-RELATED EVENTS (OUTBOUND)  ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				// O evento CCEV_CALLPROGRESS pode vir no lugar do evento GCEV_ALERTING
				// caso seja feita uma ligacao para celular, caso venha este evento,
				// o evento alerting nao vira

				// Seta o flag de evento de alerta, pois se a tele enviar um sinal de 
				// congestionamento ou de ocupado, quando vier o evento de desconexao
				// nao devemos esperar por duplo atendimento
				
				// Unsolicited events.
				case GCEV_DIALING:
				case GCEV_ALERTING:							// canal alertando (ringing)
				case GCEV_CALLPROGRESS:					// equivalente a GCEV_ALERTING
				case GCEV_PROCEEDING:						// equivalente a GCEV_ALERTING
				case GCEV_PROGRESSING:					// equivalente a GCEV_ALERTING
				case GCEV_SETUP_ACK:						// equivalente a GCEV_ALERTING
                    m_pLog4cpp->debug("[%p][%s] Set ChannelStatus = CHANNEL_ALERTING", this, __FUNCTION__ );
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_CALLSTATUS:						// nao conseguiu completar a ligacao
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_CALLSTATUS ", this, __FUNCTION__ );

                    m_pLog4cpp->debug("[%p][%s] Set ChannelStatus = CHANNEL_CALLSTATUS", this, __FUNCTION__ );
					ChannelStatus = CHANNEL_CALLSTATUS;
					return T_NOANSWER;

				case GCEV_CONNECTED:						// ligacao completada
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_CONNECTED ", this, __FUNCTION__ );                                        
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if( kvCallStats.second == GCST_OFFERED)                            
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_CONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_CONNECTED", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });
					
					return T_CONNECT;
				
				case GCEV_BLINDTRANSFER:				// evento de termino de uma transferencia cega por gc_BlindTransfer()
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_BLINDTRANSFER ", this, __FUNCTION__ );

					// retorna T_DROPCALL porque a funcao gc_BlindTransfer() disconecta a ligacao corrente (de entrada)
					// Guarda o crn da chamada atual p/ evitar a colisao de uma nova chamada (novo crn)
					// sem antes ter liberado o atual.
					FlagDisconnected = true;					
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if( kvCallStats.second == GCST_CONNECTED)                            
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_IDLE;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });

					
					return T_DROPCALL;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  CALL-RELATED EVENTS (INBOUND)  /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				case GCEV_ANSWERED:							// ligacao atendida
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_ANSWERED ", this, __FUNCTION__ );
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if( kvCallStats.second == GCST_ACCEPTED)                            
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_CONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_CONNECTED", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });
					
					return T_ANSWER;

				// Unsolicited event.
				case GCEV_OFFERED:							// chamada sendo oferecida
					m_pLog4cpp->debug("[%p][%s] Event received : GCEV_OFFERED ", this, __FUNCTION__ );
					
					memset(Dnis,			0, sizeof(Dnis));
					memset(Ani,				0, sizeof(Ani));
					memset(CallInfo,	0, sizeof(CallInfo));
					FlagCallIP = false;
					
					// Pega o numero de B
					gc_GetCallInfo(crn, DESTINATION_ADDRESS, Dnis);

					// Pega o numero de A
					gc_GetCallInfo(crn, ORIGINATION_ADDRESS, Ani);

					if(ldev != ipdev)
					{
						// Pega a categoria do chamador
						gc_GetCallInfo(crn, CATEGORY_DIGIT, CallInfo);
					}
					else
						FlagCallIP = true;

					FlagDisconnected = false;
					iDirection = CH_INBOUND;

                    {                        
                        if ( mapCrnToStatus.count(crn) == 0 )
                        {
                            mapCrnToStatus[crn] = GCST_OFFERED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_OFFERED", this, __FUNCTION__ , crn);
                        }
                    }

                    
					return T_CALL;
                    

				case GCEV_ACCEPT:
                    m_pLog4cpp->debug("[%p][%s] Event received : GCEV_ACCEPT ", this, __FUNCTION__ );
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if( kvCallStats.second == GCST_OFFERED)                            
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_ACCEPTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_ACCEPTED", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });

					//ChannelStatus = CHANNEL_ACCEPTED;
					return T_ACCEPT;

				case GCEV_LISTEN:
					return T_LISTEN;

				case GCEV_UNLISTEN:
					return T_UNLISTEN;

				case GCEV_ACKCALL:							// evento de termino da funcao gc_CallAck()
					return T_ACKCALL;

				case GCEV_RETRIEVECALL:					// chamada resumida (PDK)
					RetrieveCallAck();
					return T_NO_EVENTS;

				case GCEV_RETRIEVEACK:					// chamada resumida (ISDN)
					return T_RESUME;

				case GCEV_HOLDCALL:							// chamada colocada em espera (PDK)
					HoldCallAck();
					return T_NO_EVENTS;

				case GCEV_HOLDACK:							// chamada colocada em espera (ISDN)
					return T_HOLD;

				case GCEV_HOLDREJ:							// chamada n�o est� em espera (ISDN)				
					return T_TASKFAIL;

#ifdef _HMP
				case GCEV_INVOKE_XFER:					// chamada transferida com sucesso				
					return T_INVOKEXFER;

				case GCEV_INVOKE_XFER_FAIL:					// chamada transferida com sucesso				
					return T_TASKFAIL;
#endif // _HMP

				default:
					CtiLog( "Event not handled: %s", GCEV_MSG( EventType ) );
					break;
			}
		}
		else
		{ // Eventos normais de voz
			switch( EventType )
			{

				case TDX_CST:                                                                                                                                         
					cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
					Event	= cstp->cst_event;
					Data	= cstp->cst_data;
					
					switch( Event )
					{
						case DE_TONEON:
							ToneId = (unsigned int) Data;
							CtiLog("DE_TONEON: ToneId=%d", ToneId);
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							
							return T_NO_EVENTS;

						case DE_DIGITS:
							CtiLog("DE_DIGITS");
							LastDigit = (char) Data; // guarda o digito recebido
							return T_DIGIT;
					}
				break;

				case TDX_PLAY:
					switch( ATDX_TERMMSK( Vox ) )
					{
						case TM_NORMTERM:
							CtiLog( "TDX_PLAY (reason: TM_NORMTERM)" );
							break;
						case TM_MAXDTMF:
							CtiLog( "TDX_PLAY (reason: TM_MAXDTMF)" );
							break;
						case TM_MAXSIL:
							CtiLog( "TDX_PLAY (reason: TM_MAXSIL)" );
							break;
						case TM_MAXNOSIL:
							CtiLog( "TDX_PLAY (reason: TM_MAXNOSIL)" );
							break;
						case TM_LCOFF:
							CtiLog( "TDX_PLAY (reason: TM_LCOFF)" );
							break;
						case TM_IDDTIME:
							CtiLog( "TDX_PLAY (reason: TM_IDDTIME)" );
							break;
						case TM_MAXTIME:
							CtiLog( "TDX_PLAY (reason: TM_MAXTIME)" );
							break;
						case TM_DIGIT:
							CtiLog( "TDX_PLAY (reason: TM_DIGIT)" );
							break;
						case TM_PATTERN:
							CtiLog( "TDX_PLAY (reason: TM_PATTERN)" );
							break;
						case TM_USRSTOP:
							CtiLog( "TDX_PLAY (reason: TM_USRSTOP)" );
							break;
						case TM_EOD:
							CtiLog( "TDX_PLAY (reason: TM_EOD)" );
							break;
						case TM_TONE:
							CtiLog( "TDX_PLAY (reason: TM_TONE)" );
							break;
						case TM_BARGEIN:
							CtiLog( "TDX_PLAY (reason: TM_BARGEIN)" );
							break;
						case TM_ERROR:
							CtiLog( "TDX_PLAY (reason: TM_ERROR)" );
							break;
						case TM_MAXDATA:
							CtiLog( "TDX_PLAY (reason: TM_MAXDATA)" );
							break;
						default:
							CtiLog("TDX_PLAY");
					}
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_PLAY;

				case TDX_RECORD:
					CtiLog("TDX_RECORD");
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_RECORD;

				case TDX_GETDIG:
					CtiLog("TDX_GETDIG");
					return T_MAXDIG;

				case TDX_PLAYTONE:
					CtiLog("TDX_PLAYTONE");
					return T_PLAYTONE;

				case TDX_ERROR:
					CtiLog("TDX_ERROR");
					return T_ERROR;

				case T_USER_BLOCK: // Evento para bloquear o canal
					CtiLog("T_USER_BLOCK");
					FlagUserBlock = true;
					return T_USER_BLOCK;

				case T_USER_UNBLOCK: // Evento para desbloquear o canal
					CtiLog("T_USER_UNBLOCK");
					FlagUserBlock = false;
					return T_USER_UNBLOCK;

				default:
					return EventType;
			}
		}
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::ProcGCGeneric()");
		PutError("Exception Dialogic::ProcGCGeneric()");
		return -1;
	}
	return -1;
}



/********************************************************************************************
	NOME:	ProcGC

	DESCRICAO:	Processa os eventos de GlobalCall para canal de voz e DTI

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::ProcGC( int Time )
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    std::stringstream ss;

	try {
		unsigned int ToneId = 0;

		Devs[0] = Vox;
		Devs[1] = Dti;
		Devs[2] = ldev;
		Devs[3] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 3, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 3, Time, &EventHandle) == -1)
				return -1;
		}

		if(gc_GetMetaEventEx(&metaevent, EventHandle) != GC_SUCCESS)
			return -1; // erro ao obter evento global call

		cstp			= (DX_CST *)metaevent.evtdatap;
		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		EventType	=	metaevent.evttype;

		if(metaevent.flags & GCME_GC_EVENT) // Eventos da API Global Call
        {
			if(gc_GetCRN(&crn, &metaevent) != GC_SUCCESS)
            {
				PutError("gc_GetCRN() Error Exception Dialogic::ProcLSI()");
				return -1;
			}

			LogCasBits();

            //auto itRange = mmapDev2Crn.equal_range(ldev);

			switch(EventType){

				case GCEV_SETCHANSTATE:
					CtiLog("GCEV_SETCHANSTATE");
					return T_NO_EVENTS;

				case GCEV_BLOCKED:
					CtiLog("GCEV_BLOCKED");
					ChannelStatus = CHANNEL_BLOCKED;
					return T_BLOCKED;

				case GCEV_UNBLOCKED:
					CtiLog("GCEV_UNBLOCKED");
					ChannelStatus = CHANNEL_IDLE;
					return T_IDLE;

				case GCEV_RESETLINEDEV:
					CtiLog("GCEV_RESETLINEDEV");
					if(gc_WaitCall(ldev, NULL, NULL, 0, EV_ASYNC) != GC_SUCCESS) {
						PutError("Error in function gc_WaitCall(): %s", gc_GetStringError());
						CtiLog("Error in function gc_WaitCall(): %s", gc_GetStringError());
					}
					break;

				case GCEV_TASKFAIL:
					CtiLog("GCEV_TASKFAIL");
					if(gc_ResetLineDev(ldev, EV_ASYNC) !=  0){
						PutError("Error in function gc_ResetLineDev(): %s", gc_GetStringError());
					}
					return T_TASKFAIL;

				case GCEV_RELEASECALL_FAIL:
					CtiLog("GCEV_RELEASECALL_FAIL");
					return T_DROPCALL;

				case GCEV_RELEASECALL:
					CtiLog("GCEV_RELEASECALL");

                    if ( mapCrnToStatus.count(crn) )
                    {
                        long callStatus = mapCrnToStatus[crn];
                        if (callStatus == GCST_IDLE)
                        {
                            mapCrnToStatus[crn] = GCST_NULL;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_NULL", this, __FUNCTION__ , crn);                            

                            mapCrnToStatus.erase(crn);
                            m_pLog4cpp->debug("[%p][%s]  (crn = %ld) removed from map < CRN -> CALL STATUS >", this, __FUNCTION__ , crn);
                        }
                    }

					return T_DROPCALL;

				case GCEV_DROPCALL:
					CtiLog("GCEV_DROPCALL");                    
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if ( (kvCallStats.second == GCST_DISCONNECTED) || (kvCallStats.second == GCST_DETECTED) || (kvCallStats.second == GCST_DIALING) || 
                             (kvCallStats.second == GCST_OFFERED) || (kvCallStats.second == GCST_ACCEPTED) || (kvCallStats.second == GCST_CONNECTED) ||
                             (kvCallStats.second == GCST_PROCEEDING) || (kvCallStats.second == GCST_ALERTING)
                             ) 
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_IDLE;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first);  

                            ss.str(std::string());
                            ss << boost::format("gc_ReleaseCallEx(release_crn = %ld)") % kvCallStats.first;   
                            m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, ss.str().c_str());

                            if( gc_ReleaseCallEx( kvCallStats.first, EV_ASYNC ) != GC_SUCCESS )
                            {
							    PutError( "Error in Dialogic::Disconnect()->gc_ReleaseCallEx(): %s", gc_GetStringError() );
							    CtiLog( "%s", GetLastError() );
							    return -1;
						    } 
                        }
                    });
					
					return T_DROPCALL;

				case GCEV_DISCONNECTED:
					CtiLog("GCEV_DISCONNECTED");                    
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if ( (kvCallStats.second == GCST_DIALING) || (kvCallStats.second == GCST_OFFERED) || (kvCallStats.second == GCST_ACCEPTED) || 
                             (kvCallStats.second == GCST_CONNECTED) || (kvCallStats.second == GCST_ALERTING) ) 
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_DISCONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_DISCONNECTED", this, __FUNCTION__ , kvCallStats.first);  

                            if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                            {
							    PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
							    CtiLog( "%s", GetLastError() );
							    return -1;
						    }
                        }
                    });

					GC_ReasonDisconnect();

					// A ligacao e de saida
					if(DialOut == true){
						// Incrementa o contador de desconexao
						ContDisconnected++;
						// Se veio o evento GCEV_ALERTING, entao atendeu e desligou,
						// espera o reatendimento
						if(ContDisconnected == 1 && FlagGlobalCallAlerting){
							return -1;
						}
						else{
							// Nao terminou a troca de sinalizacao, veio sinal de congestionamento
							// ou ocupado
							if(ContDisconnected == 1 && FlagGlobalCallAlerting == false){
								return T_BUSY;
							}
							// Desligou duas vezes e veio o evento GCEV_ALERTING, entao o 
							// telefone de destino desligou mesmo
							if(ContDisconnected == 2 && FlagGlobalCallAlerting == true){
								FlagDisconnected = true;
								return T_DISCONNECT;
							}
						}
					}
					else{ // A ligacao e de entrada
						FlagDisconnected = true;
						return T_DISCONNECT;
					}
					break;

				// O evento CCEV_CALLPROGRESS pode vir no lugar do evento GCEV_ALERTING
				// caso seja feita uma ligacao para celular, caso venha este evento,
				// o evento alerting nao vira

				// Seta o flag de evento de alerta, pois se a tele enviar um sinal de 
				// congestionamento ou de ocupado, quando vier o evento de desconexao
				// nao devemos esperar por duplo atendimento
				
				case GCEV_ALERTING:	// Terminou a sinalizacao, canal de voz aberto (saida)
					CtiLog("GCEV_ALERTING");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_CALLPROGRESS:
					CtiLog("GCEV_CALLPROGRESS");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_CALLSTATUS:	// Nao atendeu (out)
					CtiLog("GCEV_CALLSTATUS");
					ChannelStatus = CHANNEL_CALLSTATUS;
					return T_NOANSWER;

				case GCEV_CONNECTED:	// Ligacao atendida (saida)
					CtiLog("GCEV_CONNECTED");
					ChannelStatus = CHANNEL_CONNECTED;
					return T_CONNECT;
				
				case GCEV_ANSWERED:		// Ligacao atendida (entrada)
					CtiLog("GCEV_ANSWERED");
                   
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if (kvCallStats.second == GCST_ACCEPTED)
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_CONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_CONNECTED", this, __FUNCTION__ , kvCallStats.first);                              
                        }
                    });

					//ChannelStatus = CHANNEL_ANSWERED;
					return T_ANSWER;

				case GCEV_OFFERED: // uma chamada acaba de chegar (entrada)
					CtiLog("GCEV_OFFERED");
					ChannelStatus = CHANNEL_OFFERED;
					memset(Dnis,			0, sizeof(Dnis));
					memset(Ani,				0, sizeof(Ani));
					memset(CallInfo,	0, sizeof(CallInfo));
					// Pega o numero de B
					if(gc_GetDNIS(crn, Dnis) != GC_SUCCESS){
					}
					// Pega o numero de A
					if(gc_GetANI(crn, Ani) != GC_SUCCESS){
					}
					// Pega a categoria do chamador
					if(gc_GetCallInfo(crn, CATEGORY_DIGIT, CallInfo) != GC_SUCCESS){
					}
					FlagDisconnected = false;
                    
                        
                    if ( mapCrnToStatus.count(crn) == 0 )
                    {
                        mapCrnToStatus[crn] = GCST_OFFERED;
                        m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to CHANNEL_OFFERED", this, __FUNCTION__ , crn);
                    }
                    

					iDirection = CH_INBOUND;
					return T_CALL;

				case GCEV_ACCEPT: // a chamada foi aceita (ENTRADA)
					CtiLog("GCEV_ACCEPT");

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if (kvCallStats.second == GCST_OFFERED)
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_ACCEPTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_ACCEPTED", this, __FUNCTION__ , kvCallStats.first);                              
                        }
                    });

					//ChannelStatus = CHANNEL_ACCEPTED;
					return T_ACCEPT;

				case GCEV_ACKCALL:	// Retorno de OK para a funcao AckCall
					CtiLog("GCEV_ACKCALL");
					return T_ACKCALL;

				case GCEV_BLINDTRANSFER: // evento de termino de uma transferencia cega
					CtiLog("GCEV_BLINDTRANSFER");
					ChannelStatus = CHANNEL_IDLE;
					// retorna T_DROPCALL porque a funcao gc_BlindTransfer() disconecta a ligacao corrente (de entrada)
					return T_DROPCALL;

				default:
					CtiLog("%d",EventType);
				break;
			}
		}
		else{ // Eventos normais de voz
			switch(EventType){

				case TDX_CST:                                                                                                                                         
					cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
					Event	= cstp->cst_event;
					Data	= cstp->cst_data;
					
					switch(Event)
					{
						case DE_TONEON:
							ToneId = (unsigned int) Data;
							CtiLog("DE_TONEON: ToneId=%d", ToneId);
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							return T_NO_EVENTS;

						case DE_DIGITS:
							CtiLog("DE_DIGITS");
							LastDigit = (char) Data; // guarda o digito recebido
							return T_DIGIT;
					}
				break;

				case TDX_PLAY:
					CtiLog("TDX_PLAY");
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_PLAY;

				case TDX_RECORD:
					CtiLog("TDX_RECORD");
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_RECORD;

				case TDX_GETDIG:
					CtiLog("TDX_GETDIG");
					return T_MAXDIG;

				case TDX_PLAYTONE:
					CtiLog("TDX_PLAYTONE");
					return T_PLAYTONE;

				case TDX_ERROR:
					CtiLog("TDX_ERROR");
					return T_ERROR;

				case T_USER_BLOCK: // Evento para bloquear o canal
					CtiLog("T_USER_BLOCK");
					FlagUserBlock = true;
					return T_USER_BLOCK;

				case T_USER_UNBLOCK: // Evento para desbloquear o canal
					CtiLog("T_USER_UNBLOCK");
					FlagUserBlock = false;
					return T_USER_UNBLOCK;

				default:
					return EventType;
			}
		}
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::ProcLSI()");
		PutError("Exception Dialogic::ProcLSI()");
		return -1;
	}
	return -1;
}


/********************************************************************************************
	NOME:	ProcGCSS7

	DESCRICAO:	Processa os eventos de GlobalCall SS7 para canal de voz e DTI

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::ProcGCSS7( int Time )
{
    std::stringstream ss;

    m_pLog4cpp->debug("[%p][%s] ", this, __FUNCTION__);

	try
	{
		unsigned int ToneId = 0;

		Devs[0] = Vox;
		Devs[1] = Dti;
		Devs[2] = ldev;
		Devs[3] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if( sr_waitevtEx( Devs, 3, TimeToWaitEvent, &EventHandle ) == -1 )
				return -1;
		}
		else
		{
			if( sr_waitevtEx( Devs, 3, Time, &EventHandle ) == -1 )
				return -1;
		}

		if( gc_GetMetaEventEx( &metaevent, EventHandle ) != GC_SUCCESS )
			return -1;

		cstp			= (DX_CST *) metaevent.evtdatap;
		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		EventType	=	metaevent.evttype;

		if( metaevent.flags & GCME_GC_EVENT )
		{
			if( gc_GetCRN( &crn, &metaevent ) != GC_SUCCESS )
			{
				PutError( "gc_GetCRN() Error Exception Dialogic::ProcGCSS7()" );
				return -1;
			}

            //auto itRange = mmapDev2Crn.equal_range(ldev);

			switch( EventType )
			{
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  LINE-RELATED EVENTS  /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				case GCEV_SETCHANSTATE:
					CtiLog("GCEV_SETCHANSTATE");
					return T_NO_EVENTS;

				case GCEV_BLOCKED:
					CtiLog("GCEV_BLOCKED");
					ChannelStatus = CHANNEL_BLOCKED;
					if( gc_SetChanState( ldev, GCLS_OUT_OF_SERVICE, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::ProcGCSS7()->gc_SetChanState(): %s", gc_GetStringError() );
						return -1;
					}
					return T_BLOCKED;

				case GCEV_UNBLOCKED:
					CtiLog("GCEV_UNBLOCKED");
					ChannelStatus = CHANNEL_IDLE;
					if( gc_SetChanState( ldev, GCLS_INSERVICE, EV_ASYNC ) !=  0 )
					{
						PutError( "Error in Dialogic::ProcGCSS7()->gc_SetChanState(): %s", gc_GetStringError() );
						return -1;
					}
					return T_IDLE;

				case GCEV_RESETLINEDEV:
					CtiLog("GCEV_RESETLINEDEV");
					return T_RESET;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  CALL-RELATED EVENTS (INBOUND/OUTBOUND)  ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				case GCEV_TASKFAIL:
					CtiLog("GCEV_TASKFAIL");
					if( gc_ResetLineDev( ldev, EV_ASYNC ) !=  0 )
					{
						PutError("Error in function gc_ResetLineDev(): %s", gc_GetStringError());
					}
					return T_TASKFAIL;

				case GCEV_RELEASECALL_FAIL:
					CtiLog("GCEV_RELEASECALL_FAIL");
					return T_DROPCALL;

				case GCEV_RELEASECALL:
					CtiLog("GCEV_RELEASECALL");

                    if ( mapCrnToStatus.count(crn) )
                    {
                        long callStatus = mapCrnToStatus[crn];
                        if (callStatus == GCST_IDLE)
                        {
                            mapCrnToStatus[crn] = GCST_NULL;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_NULL", this, __FUNCTION__ , crn);                            

                            mapCrnToStatus.erase(crn);
                            m_pLog4cpp->debug("[%p][%s]  (crn = %ld) removed from map < CRN -> CALL STATUS >", this, __FUNCTION__ , crn);
                        }
                    }

					return T_DROPCALL;

				case GCEV_DROPCALL:
					CtiLog("GCEV_DROPCALL");

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {
                        if ( (kvCallStats.second == GCST_DISCONNECTED) || (kvCallStats.second == GCST_DETECTED) || (kvCallStats.second == GCST_DIALING) || 
                             (kvCallStats.second == GCST_OFFERED) || (kvCallStats.second == GCST_ACCEPTED) || (kvCallStats.second == GCST_CONNECTED) ||
                             (kvCallStats.second == GCST_PROCEEDING) || (kvCallStats.second == GCST_ALERTING)
                             ) 
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_IDLE;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first);  

                            ss.str(std::string());
                            ss << boost::format("gc_ReleaseCallEx(release_crn = %ld)") % crn;   
                            m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, ss.str().c_str());

                            if( gc_ReleaseCallEx( kvCallStats.first , EV_ASYNC ) != GC_SUCCESS )
                            {
							    PutError( "Error in Dialogic::Disconnect()->gc_ReleaseCallEx(): %s", gc_GetStringError() );
							    CtiLog( "%s", GetLastError() );
							    return -1;
						    }
                        }
                    });


					ChannelStatus = CHANNEL_IDLE;
					return T_DROPCALL;

				case GCEV_DISCONNECTED:
					CtiLog("GCEV_DISCONNECTED");                    
                    
                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {   

                        if ( (kvCallStats.second == GCST_DIALING) || (kvCallStats.second == GCST_OFFERED) || (kvCallStats.second == GCST_ACCEPTED) || 
                             (kvCallStats.second == GCST_CONNECTED) || (kvCallStats.second == GCST_ALERTING) ) 
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_DISCONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_DISCONNECTED", this, __FUNCTION__ , kvCallStats.first);  

                            if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS )
                            {
							    PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
							    CtiLog( "%s", GetLastError() );
							    return -1;
						    }
                        }
                    });

					ChannelStatus = CHANNEL_DISCONNECTED;
					GC_ReasonDisconnect();
					FlagDisconnected = true;
					return T_DISCONNECT;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  CALL-RELATED EVENTS (OUTBOUND)  ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				// O evento CCEV_CALLPROGRESS pode vir no lugar do evento GCEV_ALERTING
				// caso seja feita uma ligacao para celular, caso venha este evento,
				// o evento alerting nao vira

				// Seta o flag de evento de alerta, pois se a tele enviar um sinal de 
				// congestionamento ou de ocupado, quando vier o evento de desconexao
				// nao devemos esperar por duplo atendimento
				
				case GCEV_DIALING:
					CtiLog("GCEV_DIALING");
					ChannelStatus = CHANNEL_DIALING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_ALERTING:							// canal alertando (ringing)
					CtiLog("GCEV_ALERTING");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_CALLPROGRESS:					// equivalente a GCEV_ALERTING
					CtiLog("GCEV_CALLPROGRESS");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_PROCEEDING:						// equivalente a GCEV_ALERTING
					CtiLog("GCEV_PROCEEDING");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_PROGRESSING:					// equivalente a GCEV_ALERTING
					CtiLog("GCEV_PROGRESSING");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_SETUP_ACK:						// equivalente a GCEV_ALERTING
					CtiLog("GCEV_SETUP_ACK");
					ChannelStatus = CHANNEL_ALERTING;
					FlagGlobalCallAlerting = true;
					return T_ALERTING;

				case GCEV_CALLSTATUS:						// nao conseguiu completar a ligacao
					CtiLog("GCEV_CALLSTATUS");
					ChannelStatus = CHANNEL_CALLSTATUS;
					return T_NOANSWER;

				case GCEV_CONNECTED:						// ligacao completada
					CtiLog("GCEV_CONNECTED");
					ChannelStatus = CHANNEL_CONNECTED;
					return T_CONNECT;
				
				case GCEV_BLINDTRANSFER:				// evento de termino de uma transferencia cega por gc_BlindTransfer()
					CtiLog("GCEV_BLINDTRANSFER");
					ChannelStatus = CHANNEL_IDLE;
					// retorna T_DROPCALL porque a funcao gc_BlindTransfer() disconecta a ligacao corrente (de entrada)
					return T_DROPCALL;

/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  CALL-RELATED EVENTS (INBOUND)  /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
				case GCEV_ANSWERED:							// ligacao atendida
					CtiLog("GCEV_ANSWERED");

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {   

                        if (kvCallStats.second == GCST_ACCEPTED)  
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_CONNECTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_CONNECTED", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });

					//ChannelStatus = CHANNEL_ANSWERED;
					return T_ANSWER;

				case GCEV_OFFERED:							// chamada sendo oferecida
					CtiLog("GCEV_OFFERED");
					ChannelStatus = CHANNEL_OFFERED;
					memset(Dnis,			0, sizeof(Dnis));
					memset(Ani,				0, sizeof(Ani));
					memset(CallInfo,	0, sizeof(CallInfo));

					
					// Pega o numero de B
					if(gc_GetDNIS(crn, Dnis) != GC_SUCCESS){
					}
					// Pega o numero de A
					if(gc_GetANI(crn, Ani) != GC_SUCCESS){
					}
					//GetCallInfoGCSS7();
					// Pega a categoria do chamador
					if(gc_GetCallInfo(crn, CATEGORY_DIGIT, CallInfo) != GC_SUCCESS){
					}
					
					/*
					memset( &BCI, 0, sizeof(BCI) );

					BCI.bci_bits.bitA = 0;	// Par BA - Indicador de tarifa
					BCI.bci_bits.bitB = 1;	// 00 s/ indicacao, 01 s/ tarifacao, 10 c/ tarifacao, 11 reserva
					BCI.bci_bits.bitC = 1;	// Par DC - Indicador de estado do chamador
					BCI.bci_bits.bitD = 0;	// 00 s/ indicacao, 01 assinante livre, 10 nao usado, 11 reserva

					switch( CallInfo[0] )
					{
						case SS7_CPC_ASSINANTE_COMUM:
							BCI.bci_bits.bitE = 1;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_ASSINANTE_ESPECIAL:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_CHAMADA_TESTES:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_TELEFONE_PUBLICO:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 1;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_TELEFONISTA:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_CHAMADA_DADOS:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						case SS7_CPC_TP_INTERURBANO:
							BCI.bci_bits.bitE = 0;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 1;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
							break;
						default:
							BCI.bci_bits.bitE = 1;	// Par FE - Indicador de categoria do chamador
							BCI.bci_bits.bitF = 0;	// 00 s/ indicacao, 01 assinante comum, 10 telefone publico, 11 reserva
					}
					
					BCI.bci_bits.bitG = 0;	// Nao utilizado
					BCI.bci_bits.bitH = 0;	// Nao utilizado
					BCI.bci_bits.bitI = 0;	// Indicador de Interfuncionamento 0 = nao encontrado, 1 = encontrado
					BCI.bci_bits.bitJ = 0;	// Nao utilizado
					BCI.bci_bits.bitK = 1;	// Indicador de uso da ISUP 0 = nao usado, 1 = usado
					BCI.bci_bits.bitL = 0;	// Indicador de retencao 0 = nao solicitada, 1 = solicitada
					BCI.bci_bits.bitM = 0;	// Indicador de acesso ISDN 0 = nao, 1 = sim
					BCI.bci_bits.bitN = 0;	// Indicador de disp. supressor de eco 0 = nao incluido, 1 = incluido
					BCI.bci_bits.bitO = 0;	// Nao utilizado
					BCI.bci_bits.bitP = 0;	// Nao utilizado
					
					ie_blk.length = 4;
					ie_blk.data[0] = CALPPN_BCI;
					ie_blk.data[1] = 0x02;
					{
						char *p = (char *) &BCI.value;
						ie_blk.data[2] = p[0];
						ie_blk.data[3] = p[1];
					}

					gc_ie_blk.gclib = NULL;
					gc_ie_blk.cclib = &ie_blk;

					gc_SetInfoElem( ldev, &gc_ie_blk );
					*/  

                    if ( mapCrnToStatus.count(crn) == 0 )
                    {
                        mapCrnToStatus[crn] = GCST_OFFERED;
                        m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_OFFERED", this, __FUNCTION__ , crn);
                    }
                    

					FlagDisconnected = false;
					iDirection = CH_INBOUND;
					return T_CALL;
                    
                    break;

				case GCEV_ACCEPT:								// chamada aceita
					CtiLog("GCEV_ACCEPT");
                    

                    for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
                    {   

                        if (kvCallStats.second == GCST_OFFERED)  
                        {
                            mapCrnToStatus[kvCallStats.first] = GCST_ACCEPTED;                            
                            m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_ACCEPTED", this, __FUNCTION__ , kvCallStats.first);  
                        }
                    });

					//ChannelStatus = CHANNEL_ACCEPTED;
					return T_ACCEPT;

				case GCEV_ACKCALL:							// evento de termino da funcao gc_CallAck()
					CtiLog("GCEV_ACKCALL");
					return T_ACKCALL;

				case GCEV_RETRIEVEACK:					// chamada resumida (ISDN)
					CtiLog("GCEV_RETRIEVEACK");
					return T_RESUME;

				case GCEV_RETRIEVECALL:					// chamada resumida (PDK)
					CtiLog("GCEV_RETRIEVECALL");
					return T_RESUME;

				case GCEV_HOLDACK:							// chamada colocada em espera (ISDN)
					CtiLog("GCEV_HOLDACK");
					return T_HOLD;

				case GCEV_HOLDCALL:							// chamada colocada em espera (PDK)
					CtiLog("GCEV_HOLDCALL");
					return T_HOLD;

				case GCEV_HOLDREJ:							// chamada n�o est� em espera (ISDN)				
					CtiLog("GCEV_HOLDREJ");
					return T_TASKFAIL;

				default:
					CtiLog( "Event not handled: %s", GCEV_MSG( EventType ) );
					break;
			}
		}
		else
		{ // Eventos normais de voz
			switch( EventType )
			{

				case TDX_CST:                                                                                                                                         
					cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
					Event	= cstp->cst_event;
					Data	= cstp->cst_data;
					
					switch( Event )
					{
						case DE_TONEON:
							ToneId = (unsigned int) Data;
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CtiLog("DE_TONEON: ToneId=%d", ToneId);
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							return T_NO_EVENTS;

						case DE_DIGITS:
							CtiLog("DE_DIGITS");
							LastDigit = (char) Data; // guarda o digito recebido
							return T_DIGIT;
					}
				break;

				case TDX_PLAY:
					CtiLog("TDX_PLAY");
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_PLAY;

				case TDX_RECORD:
					CtiLog("TDX_RECORD");
					//Limpa a lista de Prompts
					ClearPrompts();
					return T_RECORD;

				case TDX_GETDIG:
					CtiLog("TDX_GETDIG");
					return T_MAXDIG;

				case TDX_PLAYTONE:
					CtiLog("TDX_PLAYTONE");
					return T_PLAYTONE;

				case TDX_ERROR:
					CtiLog("TDX_ERROR");
					return T_ERROR;

				case T_USER_BLOCK: // Evento para bloquear o canal
					CtiLog("T_USER_BLOCK");
					FlagUserBlock = true;
					return T_USER_BLOCK;

				case T_USER_UNBLOCK: // Evento para desbloquear o canal
					CtiLog("T_USER_UNBLOCK");
					FlagUserBlock = false;
					return T_USER_UNBLOCK;

				default:
					return EventType;
			}
		}
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::ProcGCSS7()");
		PutError("Exception Dialogic::ProcGCSS7()");
		return -1;
	}
	return -1;
}

/********************************************************************************************
	NOME:	DialGCAsync

	DESCRICAO:	Disca pelo canal digital com GlobalCall assincronamente, a thread que
							chamou deve controlar os eventos

	PARAMETROS:	Nenhum

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::DialGCAsync(void)
{
	try {

		if(gc_SetCallingNum(ldev, Ani) != GC_SUCCESS) {
			CtiLog( gc_GetStringError() );
			return -1;
		}

        if(gc_MakeCall(ldev, &crn, Dnis, NULL, 0, EV_ASYNC) != GC_SUCCESS)
        {
			CtiLog( gc_GetStringError() );
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::DialGCAsync()");
		PutError("Exception Dialogic::DialGCAsync()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	SetDialOutGCSS7

	DESCRICAO:	Prepara a linha p/ o processo de discagem.

	PARAMETROS:	bDialOut		true (prepara p/ outbound call), false (prepara p/ inbound call)

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::SetDialOutGCSS7(bool bDialOut)
{
	try
	{
		if( DialOut == bDialOut )
			return 0;

		DialOut = bDialOut;

		if( gc_ResetLineDev( dtidev, EV_ASYNC ) !=  0 )
		{
			PutError( "Error in function gc_ResetLineDev(): %s", gc_GetStringError() );
			return -1;
		}
		do
		{
			Event = GetEvent();
		} while( ( Event != -1 ) && ( Event != T_RESET ) );

		if( !DialOut )
		{
			// Inbound
			if( gc_WaitCall( dtidev, NULL, NULL, 0, EV_ASYNC ) != GC_SUCCESS )
			{
				PutError( "Error in function gc_WaitCall(): %s", gc_GetStringError() );
				CtiLog( "Error in function gc_WaitCall(): %s", gc_GetStringError() );
				return -1;
			}
		}
		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::SetDialOutGCSS7()");
		PutError("Exception Dialogic::SetDialOutGCSS7()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	BuildMakeCallBlkGCSS7

	DESCRICAO:	Prepara a estrutura gc_makecall_blk para a discagem.

	PARAMETROS:	Nenhum

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::BuildMakeCallBlkGCSS7()
{
	try
	{
		//memset(&port[canal].gc_makecall_blk,		0xFF, sizeof(GC_MAKECALL_BLK));
		//memset(&port[canal].gc_s7_makecall_blk,	0xFF, sizeof(S7_MAKECALL_BLK));
		memset( &gc_makecall_blk, 0x00, sizeof(GC_MAKECALL_BLK) );
		memset( &gc_s7_makecall_blk, 0x00, sizeof(S7_MAKECALL_BLK) );
		memset( &FCI, 0x00, sizeof(FCI) );

		// gc_s7_makecall_blk
		gc_s7_makecall_blk.ss7.trans_medium_req = 0x00;
		gc_s7_makecall_blk.ss7.destination_number_type = 0x01;
		gc_s7_makecall_blk.ss7.destination_number_plan = 0x01;
		gc_s7_makecall_blk.ss7.internal_network_number = INN_ALLOWED;
		gc_s7_makecall_blk.ss7.origination_number_type = 0x01;
		gc_s7_makecall_blk.ss7.origination_number_plan = 0x01;
		::strcpy( gc_s7_makecall_blk.ss7.origination_phone_number, Ani );
		gc_s7_makecall_blk.ss7.origination_present_restrict = PRESENTATION_RESTRICTED;
		gc_s7_makecall_blk.ss7.origination_screening = 0x03;
		gc_s7_makecall_blk.ss7.calling_party_category = 0x0A;
		
		FCI.fci_bits.bitA	= 0;	// Indicador de chamada 0 = Nacional / 1 = Internacional
		FCI.fci_bits.bitB	= 0;	// Nao utilizado
		FCI.fci_bits.bitC	= 0;	// Nao utilizado
		FCI.fci_bits.bitD	= 0;	// Indicador de Interfuncionamento 0 = nao encontrado, 1 = encontrado
		FCI.fci_bits.bitE	= 0;	// Nao utilizado
		FCI.fci_bits.bitF	= 0;	// Indicador de uso da ISUP 0 = nao usado, 1 = usado
		FCI.fci_bits.bitG	= 1;	// Par HG - Indicador de preferencia de ISUP
		FCI.fci_bits.bitH	= 0;	// 00 preferida, 01 nao requerida, 10 requerida, 11 reservado
		FCI.fci_bits.bitI	= 0;	// Indicador de acesso ISDN 0 = nao, 1 = sim
		FCI.fci_bits.bitJ	= 0;	// Nao utilizado
		FCI.fci_bits.bitK	= 0;	// Nao utilizado
		FCI.fci_bits.bitL	= 0;	// Nao utilizado
		FCI.fci_bits.bitM	= 0;	// Indicador de chamada DIC/DLC 0 = normal, 1 = a cobrar

		gc_s7_makecall_blk.ss7.forward_call_indicators = FCI.value;
		
		ie_blk.length = 6;
		ie_blk.data[0] = 0x06;		//CALPPN_NOCI
		ie_blk.data[1] = 0x01;
		ie_blk.data[2] = 0x00;
		ie_blk.data[3] = 0x02;		//CALPPN_TMR
		ie_blk.data[4] = 0x01;
		ie_blk.data[5] = 0x00;

		gc_ie_blk.gclib = NULL;
		gc_ie_blk.cclib = &ie_blk;
		gc_SetInfoElem( dtidev, &gc_ie_blk );
		
		gc_makecall_blk.gclib = NULL;
		gc_makecall_blk.cclib = (MAKECALL_BLK *) &gc_s7_makecall_blk;

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::BuildMakeCallBlkGCSS7()");
		PutError("Exception Dialogic::BuildMakeCallBlkGCSS7()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	DialGCSS7Async

	DESCRICAO:	Disca pelo canal digital com GlobalCall SS7 assincronamente, a thread que
							chamou deve controlar os eventos

	PARAMETROS:	Nenhum

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::DialGCSS7Async(void)
{
	try {

		if(gc_SetCallingNum( dtidev, Ani ) != GC_SUCCESS)
		{
			CtiLog( gc_GetStringError() );
			return -2;
		}

		BuildMakeCallBlkGCSS7();

		if(gc_MakeCall( dtidev, &crn, Dnis, &gc_makecall_blk, 0, EV_ASYNC ) != GC_SUCCESS )
		{
			CtiLog( gc_GetStringError() );
			return -3;
		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::DialGCSS7Async()");
		PutError("Exception Dialogic::DialGCSS7Async()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	GetCallInfoGCSS7

	DESCRICAO:	Obtem dados adicionais de uma chamada oferecida

	PARAMETROS:	Nenhum

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::GetCallInfoGCSS7()
{
	try
	{
		char buffer[ MAX_INFO_ELEMENT_SIZE ];
		int i;
		unsigned char		*p_char;
		unsigned short	*p_short;
		unsigned long		*p_long;

		S7_SIGINFO_BLK *pS7SigInfoBlk = (S7_SIGINFO_BLK *) buffer;

		if( gc_GetSigInfo( dtidev, (char *)pS7SigInfoBlk, U_IES, &metaevent ) != GC_SUCCESS )
			return -1;

		S7_IE *pS7Ie = (S7_IE *) &pS7SigInfoBlk->data;

		for( i = 0; pS7Ie->parm; i++ )
		{
			p_char = (unsigned char *) pS7Ie;
			p_char += offsetof( S7_IE, value );

			switch( pS7Ie->length )
			{
				case sizeof(unsigned char):
					p_char = p_char;
					break;

				case sizeof(unsigned short):
					p_short = (unsigned short *) p_char;
					break;

				case sizeof(unsigned long):
					p_long = (unsigned long *) p_char;
					break;

				default:
					p_char = p_char;
			}
			
			switch( pS7Ie->parm )
			{
				case 0x07:			//CALPPN_FCI
					// Flag indicando se a ligacao e a cobrar = FCI.fci_bits.bitM
					FCI.value = *p_short;
					break;

				case 0x09:			//CALPPN_CPC
					memset( CallInfo, 0, sizeof(CallInfo) );
					CallInfo[0] = *p_char;
					break;
			}

			p_char += pS7Ie->length;

			pS7Ie = (S7_IE *) p_char;

		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::GetCallInfoGCSS7()");
		PutError("Exception Dialogic::GetCallInfoGCSS7()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	MakeDoubleAnswerGCSS7

	DESCRICAO:	Faz o duplo atendimento (double answer) p/ bloqueio de chamadas a cobrar.

	PARAMETROS:	Nenhum

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::MakeDoubleAnswerGCSS7()
{
	try
	{
		int rc;

		if( GetChannelStatus() != CHANNEL_ANSWERED )
			return -1;

		do
		{
			Event = GetEvent( 1000 );
			if( Event == T_DISCONNECT )
				return T_DISCONNECT;
		} while( ( Event != -1 ) && ( Event != T_NO_EVENTS ) );
		rc = gc_HoldCall( crn, EV_ASYNC );
		if( rc )
		{
			PutError( "gc_HoldCall() error in Dialogic::MakeDoubleAnswerGCSS7(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}
		do
		{
			Event = GetEvent( 5000 );
		} while( ( Event != -1 ) && ( Event != T_HOLD ) && ( Event != T_DISCONNECT ) );

		switch( Event )
		{
			case T_HOLD:
				break;
			case T_DISCONNECT:
				return T_DISCONNECT;
			default:
				CtiLog( "Error in MakeDoubleAnswerGCSS7(). Cannot put the call %d on hold.", crn );
				return -1;
		}

		do
		{
			Event = GetEvent( 1000 );
			if( Event == T_DISCONNECT )
				return T_DISCONNECT;
		} while( ( Event != -1 ) && ( Event != T_NO_EVENTS ) );
		rc = gc_RetrieveCall( crn, EV_ASYNC );
		if( rc )
		{
			PutError( "gc_RetrieveCall() error in Dialogic::MakeDoubleAnswerGCSS7(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}
		do
		{
			Event = GetEvent( 5000 );
		} while( ( Event != -1 ) && ( Event != T_RESUME ) && ( Event != T_DISCONNECT ) );

		switch( Event )
		{
			case T_RESUME:
				break;
			case T_DISCONNECT:
				return T_DISCONNECT;
			default:
				CtiLog( "Error in MakeDoubleAnswerGCSS7(). Cannot resume the call %d.", crn );
				return -1;
		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::MakeDoubleAnswerGCSS7()");
		PutError("Exception Dialogic::MakeDoubleAnswerGCSS7()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	BlindTransferGC

	DESCRICAO:	Faz "transferencia cega"

	PARAMETROS:	const char *lpcszNumber		Numero de destino da transferencia
							int iTime									Tempo em segundos para fazer o blindtransfer

	RETORNO:		0		sucesso
							<0	erro
********************************************************************************************/
int Dialogic::BlindTransferGC(  char *lpcszNumber, int iTime )
{
    std::stringstream ss;

	try
	{
		if( gc_BlindTransfer( crn, (char *) lpcszNumber, NULL, iTime, EV_ASYNC ) != GC_SUCCESS )
		{
			PutError( "gc_BlindTransfer() error in Dialogic::BlindTransfer(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}

        for_each( mapCrnToStatus.begin(), mapCrnToStatus.end(), [&]( pair<CRN, long> kvCallStats) 
        {   

            if (kvCallStats.second == GCST_CONNECTED)  
            {
                mapCrnToStatus[kvCallStats.first] = GCST_IDLE;                            
                m_pLog4cpp->debug("[%p][%s] Set Call (crn = %ld) status to GCST_IDLE", this, __FUNCTION__ , kvCallStats.first );  

                if( gc_DropCall( kvCallStats.first , GC_NORMAL_CLEARING, EV_ASYNC ) != GC_SUCCESS ){
				    PutError( "Error in Dialogic::Disconnect()->gc_DropCall(): %s", gc_GetStringError() );
				    CtiLog( "%s", GetLastError() );
				    return -1;
			    }
            }
        });

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::BlindTransferGC()");
		PutError("Exception Dialogic::BlindTransferGC()");
		return -1;
	}
}

/********************************************************************************************
	Funcao:			gc_getstringerror
							
	Comentario:	Retorna uma mensagem de erro para Global Call

	Parametros:	int canal				Canal dialogic
							
	Retorno:	Ponteiro para a mensagem de erro
********************************************************************************************/
char *Dialogic::gc_GetStringError(void)
{
	char *msg;
  char *lib_name;    
	int cclibid;
  int gc_error;
  long cc_error; 

	try {

		gc_last_error[0] = 0;

		gc_ErrorValue( &gc_error, &cclibid, &cc_error);
		gc_ResultMsg( LIBID_GC, (long) gc_error, &msg);
		strcpy(gc_last_error, msg);
		strcat(gc_last_error, " - ");
		gc_ResultMsg( cclibid, cc_error,&msg);
		gc_CCLibIDToName(cclibid, &lib_name);
		strcat(gc_last_error, msg);

		return (gc_last_error);
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::cg_GetStringError()");
		PutError("Exception Dialogic::cg_GetStringError()");
		return NULL;
	}
}

/********************************************************************************************
	Funcao:			GC_ReasonDisconnect

	Comentario	Coloca na variavel  global uma string com a descricao da causa
							da desconexao

	Parametros:	int canal				Canal dialogic
							int reason			razao da desconexao

	Retorno:		nenhum
********************************************************************************************/
void Dialogic::GC_ReasonDisconnect(void)
{
	try
	{
		GC_INFO gc_info;

		if( metaevent.evttype != GCEV_DISCONNECTED )
		{
			return;
		}
		
		memset( &gc_info, 0, sizeof(gc_info) );

		if( gc_ResultInfo( &metaevent, &gc_info ) != 0 )
		{
			PutError("Error in Dialogic::GC_ReasonDisconnect()->gc_ResultInfo(): %s", gc_GetStringError());
			CtiLog( "%s", GetLastError() );
			return;
		}
		CtiLog( "GCEV_DISCONNECTED Reason: gcValue=%d. gcMsg=%s. ccValue=%d. ccMsg=%s. additionalInfo=%s.", 
            gc_info.gcValue, gc_info.gcMsg, gc_info.ccValue, gc_info.ccMsg, gc_info.additionalInfo );

		DisconnectReasonCode = gc_info.ccValue;
		strcpy(sReasonDisconnect,gc_info.gcMsg);
//		switch( gc_info.ccValue )
//		{

//			case GCRV_NOANSWER:
//				DisconnectReasonCode = REASON_NOANSWER;
//			break;

//			case GCRV_NORMAL:
//				DisconnectReasonCode = REASON_NORMAL;
//			break;

//			case GCRV_BUSY:
//				DisconnectReasonCode = REASON_BUSY;
//			break;

//			default:
//				DisconnectReasonCode = REASON_CONGESTION;
//			break;
//		}

	}
	catch(...)
	{
		CtiLog("Exception Dialogic::GC_ReasonDisconnect()");
		PutError("Exception Dialogic::GC_ReasonDisconnect()");
	}
}

/********************************************************************************************
	NOME:	HoldCall

	DESCRICAO:	Coloca a chamada ativa em hold

	RETORNO:		0		sucesso
						< 0	  erro
********************************************************************************************/
int Dialogic::HoldCall()
{
	try
	{
		if( gc_HoldCall( crn, EV_ASYNC ) != GC_SUCCESS )
		{
			PutError( "gc_HoldCall() error in Dialogic::HoldCall(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		do
		{
			Event = GetEvent(30000);
		} while( ( Event != -1 ) && ( Event != T_HOLD ) && ( Event != T_TASKFAIL ) );

		if((Event == T_TASKFAIL) || (Event == -1))
			return -2;

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::HoldCall()");
		PutError("Exception Dialogic::HoldCall()");
		return -1;
	}
}

int Dialogic::HoldCallAck()
{
	try
	{
		if( gc_HoldACK( crn ) != GC_SUCCESS )
		{
			PutError( "gc_HoldACK() error in Dialogic::RetrieveCallAck(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::HoldCallAck()");
		PutError("Exception Dialogic::HoldCallAck()");
		return -1;
	}
}








/********************************************************************************************
	NOME:	RetrieveCall

	DESCRICAO:	Recupera a chamada de estado hold

	RETORNO:		0		sucesso
						< 0	  erro
********************************************************************************************/
int Dialogic::RetrieveCall()
{
	try
	{
		if( gc_RetrieveCall( crn, EV_ASYNC ) != GC_SUCCESS )
		{
			PutError( "gc_RetrieveCall() error in Dialogic::RetrieveCall(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		do
		{
			Event = GetEvent(30000);
		} while( ( Event != -1 ) && ( Event != T_RESUME ) && ( Event != T_TASKFAIL ) );

		if((Event == T_TASKFAIL) || (Event == -1))
			return -2;

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::RetrieveCall()");
		PutError("Exception Dialogic::RetrieveCall()");
		return -1;
	}
}




int Dialogic::RetrieveCallAck()
{
	try
	{
		if( gc_RetrieveAck( crn ) != GC_SUCCESS )
		{
			PutError( "gc_RetrieveAck() error in Dialogic::RetrieveCallAck(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::RetrieveCallAck()");
		PutError("Exception Dialogic::RetrieveCallAck()");
		return -1;
	}
}


//------------------------------------------------------------------------------------------------------------------------

// Processsa eventos para o protocolo R2 Digital (entrada/saida)
int Dialogic::ProcR2digital( int Time )
{
	try {
	
		Event = R2WaitEvent( Time );
		
		switch( Event ){

			case T_SEIZE:		// Um canal de entrada recebeu ocupacao
				if( ChannelStatus == CHANNEL_DIALING )
					return R2Outbound();
				else
					return R2Inbound();
			break;
		}

		return Event;
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcR2digital()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Processsa chamada de entrada
int Dialogic::R2Inbound(void)
{
	try {
	
		// Confirma a ocupacao do canal
		CasSetBit(BON);
		// Seta o estado do canal
		ChannelStatus = CHANNEL_OFFERED;
		// Cria os tons para frente
		R2_CreateForwardTones();
		// Zera as variaveis
		memset(Dnis, 0, sizeof(Dnis));
		memset(Ani, 0, sizeof(Ani));
		memset(CallInfo, 0, sizeof(CallInfo));
		CountDnis		= 0;
		CountAni		= 0;
		NumMoreDnis	=	0;
		R2_AckCall	= false;
		R2_Accept		= false;

		// Seta a Causa default da desconexao quando o sistema desliga a ligacao		
		CauseDropCall	= DROP_NORMAL;

		if( R2GetMinimumDnis() ){
			// houve desconexao
			return -1;
		}

		iDirection = CH_INBOUND;
		return T_CALL;
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Inbound()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Fica esperando o Ani (entrada)
int Dialogic::R2GetAni(void)
{
	try {
	
		char Tone;

		// Pede a categoria
		R2_PlayBackward(SIG_5);

		if( R2RecvToneOFF() == T_DISCONNECT ){
			CasSetBit(BOFF);
			return -1;
		}

#ifdef _ASR
		if( !bAsrEnabled )
			dx_stopch(Vox, EV_ASYNC);
		else
			ec_stopch(Vox, SENDING, EV_ASYNC);
#else
		dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

		for(;;){

			if( R2RecvToneON(Tone) == T_DISCONNECT ){
				CasSetBit(BOFF);
				return -1;
			}
			if( Tone >= SIG_1 && Tone <= SIG_10 ){
				// Tom valido
				Ani[CountAni++] = SigChar(Tone);
			}
			if( Tone == SIG_15 ){
				// Fim do numero de A
				break;
			}

			// Pede o proximo numero de A
			R2_PlayBackward(SIG_5);

			if( R2RecvToneOFF() == T_DISCONNECT ){
				CasSetBit(BOFF);
				return -1;
			}

#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
		}	

		CallInfo[0] = Ani[0];
		CallInfo[1] = 0;
		strcpy(&Ani[0], &Ani[1]);
		Ani[ strlen(Ani) ] = 0;

		
		// Muda para o grupo II
		R2_PlayBackward(SIG_3);

		if( R2RecvToneOFF() == T_DISCONNECT ){
			CasSetBit(BOFF);
			return -1;
		}
#ifdef _ASR
		if( !bAsrEnabled )
			dx_stopch(Vox, EV_ASYNC);
		else
			ec_stopch(Vox, SENDING, EV_ASYNC);
#else
		dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

		// Recebe a categoria novamente
		if( R2RecvToneON(Tone) == T_DISCONNECT ){
			CasSetBit(BOFF);
			return -1;
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Inbound()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Fica esperando pelo minimo numero de B (entrada)
int Dialogic::R2GetMinimumDnis(void)
{
	try {
	
		int i;
		char Tone;

		//	AR	BR	AT	BT
		//	0		0		1		1

		// Espera por mais 4 numeros de B
		for(i = 1; i <= R2DnisMinimumSize; i++){

			if( R2RecvToneON(Tone) == T_DISCONNECT ){
				CasSetBit(BOFF);
				return -1;
			}
			if( Tone >= SIG_1 && Tone <= SIG_10 ){
				// Tom valido
				Dnis[CountDnis++] = SigChar(Tone);
			}
			if( i == R2DnisMinimumSize ){
				// Retorna para a aplicacao avaliar se eh preciso pegar mais DNIS
				break;
			}
			// Pede proximo numero de B	
			R2_PlayBackward(SIG_1);
			
			if( R2RecvToneOFF() == T_DISCONNECT ){
				CasSetBit(BOFF);
				return -1;
			}
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Inbound()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Completa o Dnis (entrada)
int Dialogic::R2GetCompleteDnis(int iNumberDnis)
{
	try {
	
		int i;
		char Tone;
	
		//	AR	BR	AT	BT
		//	0		0		1		1

		if( iNumberDnis > 0 ){

			// Pede proximo numero de B	
			R2_PlayBackward(SIG_1);
				
			if( R2RecvToneOFF() == T_DISCONNECT ){
				CasSetBit(BOFF);
				return -1;
			}
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

			// Espera por mais 4 numeros de B
			for(i = 1; i <= iNumberDnis; i++){

				if( R2RecvToneON(Tone) == T_DISCONNECT ){
					CasSetBit(BOFF);
					return -1;
				}
				if( Tone >= SIG_1 && Tone <= SIG_10 ){
					// Tom valido
					Dnis[CountDnis++] = SigChar(Tone);
				}
				if( i == iNumberDnis ){
					// Vai pegar o ANI
					break;
				}
				// Pede proximo numero de B	
				R2_PlayBackward(SIG_1);
				
				if( R2RecvToneOFF() == T_DISCONNECT ){
					CasSetBit(BOFF);
					return -1;
				}
#ifdef _ASR
				if( !bAsrEnabled )
					dx_stopch(Vox, EV_ASYNC);
				else
					ec_stopch(Vox, SENDING, EV_ASYNC);
#else
				dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
			}
		}
		
		// Pega o ANI e retorna
		return R2GetAni();
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Inbound()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::R2Accept(bool bDoubleAnswer, bool bBilling)
{
	try {

		if( R2_Accept == false ){

			// Verifica se a ligacao sera cobrada ou nao

			if(bBilling){
				// Informa que a ligacao SERA COBRADA
				R2_PlayBackward(SIG_1);
			}
			else{
				// Informa que a ligacao NAO SERA COBRADA
				R2_PlayBackward(SIG_5);
			}

			if( R2RecvToneOFF() == T_DISCONNECT ){
				CasSetBit(BOFF);
				longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
			}
			
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

			// Espera o ultimo tom enviado parar de tocar
			if( R2WaitEvent() == T_DISCONNECT ){
				CasSetBit(BOFF);
				longjmp(*JumpBuffer, JUMP_DISCONNECT); // vai para o ponto de desconexao
			}

			R2_Accept = true;
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Accept()");
	}
}


//------------------------------------------------------------------------------------------------------------------------

void Dialogic::R2Answer(bool bDoubleAnswer, bool bBilling)
{
	try {

		// Aceita a ligacao
		R2Accept(bDoubleAnswer, bBilling);

		// Espera um tempo antes de atender para nao confundir a CENTRAL									
		Sleep(R2_TIME_TO_SEND_AOFF);
		
		if(bDoubleAnswer){
			CtiLog("Send DoubleAnswer");
			// Faz o duplo atendimento
			CasSetBit(AOFF);
			Sleep(1000);
			CasSetBit(AON);
			Sleep(1000);
			CasSetBit(AOFF);
		}
		else{
			// Atende a ligacao
			CtiLog("Send Answer");
			CasSetBit(AOFF);
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2Answer()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Processsa chamada de saida
int Dialogic::R2Outbound(void)
{
	try {
	
		char Tone, Sig;

		//	AR	BR	AT	BT
		//	1		1		0		0

		for(;;){

			if( !Dnis[CountDnis] ){
				// <Antonio 26/08/2002>
				// Em alguns paises existe um equivalente ao sinal SIG_15 que indica
				// o fim do numero de A para o fim do numero de B
				// R2_PlayForward( SIG_15 ); // Fim do numero de B
				// </Antonio>
				
				// Se maximo numero de B ja foi enviado, envia congestionamento
				R2_PlayForward( CharSig(Dnis[CountDnis++]) );
				R2RecvToneON(Tone);
#ifdef _ASR
				if( !bAsrEnabled )
					dx_stopch(Vox, EV_ASYNC);
				else
					ec_stopch(Vox, SENDING, EV_ASYNC);
#else
				dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
				R2RecvToneOFF();
				return T_DISCONNECT;
			}

			// Envia o primeiro numero de B
			R2_PlayForward( CharSig(Dnis[CountDnis++]) );

			if( R2RecvToneON(Tone) == T_DISCONNECT )
				return T_DISCONNECT;

#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

			if( R2RecvToneOFF() == T_DISCONNECT )
				return T_DISCONNECT;

			switch(Tone){

				case SIG_1:
					// Pediu para enviar o proximo numero de B
				continue;

				case SIG_3:
					// Muda para o grupo II
				return R2ChangeGroupII();

				case SIG_4:
					// Congestionamento
				return T_DISCONNECT;

				case SIG_5:

					Event = R2SendAni(Sig);

					if( Event == T_TONEON ){

						switch( Sig ){
							
							case SIG_1:
								// Pediu mais numero de B
							continue;

							case SIG_3:
								// Muda para o grupo II
							return R2ChangeGroupII();

							default:
								return T_DISCONNECT;
						}
					}

					if( Event == T_DISCONNECT ){
						return T_DISCONNECT;
					}
				break;
			}
		}		
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2SendDnis()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Envia o Ani (saida)
int Dialogic::R2SendAni(char &Sig)
{
	try {

		char Tone;

		for(;;){

			if( CountAni == 0 ){
				// Envia a categoria do numero de A
				R2_PlayForward( SIG_1 );
				CountAni++;
			}
			else{
				if( !Ani[ (CountAni - 1) ] ){
					// Pediu para enviar o proximo numero de A
					R2_PlayForward( SIG_15 ); // Fim do numero de A
				}
				else{
					// Envia o primeiro numero de B
					R2_PlayForward( CharSig(Ani[ (CountAni - 1) ]) );
					CountAni++;
				}
			}

			if( R2RecvToneON(Tone) == T_DISCONNECT )
				return T_DISCONNECT;

#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

			if( R2RecvToneOFF() == T_DISCONNECT )
				return T_DISCONNECT;

			switch(Tone){

				case SIG_1:
					// Pediu para enviar o proximo numero de B
					Sig = SIG_1;
				return T_TONEON;

				case SIG_3:
					// Pediu para mudar para o grupo II e terminar a sinalizacao
					Sig = SIG_3;
				return T_TONEON;

				case SIG_5:
					// Pediu para enviar o proximo numero de A
				continue;

				case SIG_4:
				default:
					// Congestionamento
				return T_DISCONNECT;
			}
		}		
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2SendAni()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Termina a sinalizacao (saida)
int Dialogic::R2ChangeGroupII(void)
{
	try {

		char Tone;

		// Muda para o grupo II

		// Envia a categoria do numero de A
		R2_PlayForward( SIG_1 );

		if( R2RecvToneON(Tone) == T_DISCONNECT )
			return T_DISCONNECT;

#ifdef _ASR
		if( !bAsrEnabled )
			dx_stopch(Vox, EV_ASYNC);
		else
			ec_stopch(Vox, SENDING, EV_ASYNC);
#else
		dx_stopch(Vox, EV_ASYNC);
#endif // _ASR

		if( R2RecvToneOFF() == T_DISCONNECT )
			return T_DISCONNECT;

		if( Tone != SIG_1 )
			return T_DISCONNECT;

		return T_ALERTING;
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2ChangeGroupII()");
		return T_BUSY;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Inicia uma chamada (saida)
int Dialogic::DialR2Async(void)
{
	try {

		// Seta o estado do canal
		ChannelStatus = CHANNEL_DIALING;
		// Cria os tons para tras
		R2_CreateBackwardTones();
		// Seta a Causa default da desconexao quando o sistema desliga a ligacao		
		CauseDropCall	= DROP_NORMAL;
		CountDnis		= 0;
		CountAni		= 0;

		//	AR	BR	AT	BT
		//	1		0		1		0

		// Ocupa o canal de saida
		CasSetBit(AOFF);
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialR2Async()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Fica esperando evento de TONEON (entrada/saida)
int Dialogic::R2RecvToneON(char &Tone)
{
	int Event;

	try {

		for(;;){

			Event = R2WaitEvent();

			switch( Event ){

//				case T_PLAYTONE:
//					Sleep(1);
//				break;
			
				case T_TONEON:
					// Guarda o tom e continua pegando eventos
					Tone = Data;
				return T_TONEON;

				case T_CONNECT:			// Um ligacao de saida foi atendida
				case T_DISCONNECT:	// Desligou
				case T_IDLE:				// Saiu do bloqueio
				case T_BLOCKED:			// Entrou no bloqueio
					return Event;
			}
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2RecvTone()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Fica esperando evento de TONEOFF (entrada/saida)
int Dialogic::R2RecvToneOFF(void)
{
	int Event;

	try {

		for(;;){

			Event = R2WaitEvent();

			switch( Event ){

//				case T_PLAYTONE:
//					Sleep(1);
//				break;
			
				case T_TONEOFF:
					return T_TONEOFF;

				case T_CONNECT:			// Um ligacao de saida foi atendida
				case T_DISCONNECT:	// Desligou
				case T_IDLE:				// Saiu do bloqueio
				case T_BLOCKED:			// Entrou no bloqueio
					return Event;
			}
		}
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2RecvTone()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::R2WaitEvent( int Time )
{
	try {
		
		Devs[0] = Vox;
		Devs[1] = Dti;
		Devs[2] = 0;

		if(FlagEndObject){
			JumpExit();
		}
		
		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 2, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 2, Time, &EventHandle) == -1)
				return -1;
		}

		datap			= (void *)sr_getevtdatap(EventHandle);
		cstp			= (DX_CST *)sr_getevtdatap(EventHandle);
		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case DTEV_SIG:                                                                                                                                        

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));                                                                                                         

				LogCasBits();

				//----------------------------------------------------------------------------------------------------------------

				if((Bit & DTMM_AOFF)	== DTMM_AOFF)
				{
					//		0				0			  1				0
					if( !AR() && !BR() && AT() && !BT() ){	// Ocupacao do canal (Entrada)
						ScConnect(FULLDUP);
						return T_SEIZE;
					}

					//		0				1			   0				0
					if( !AR() && BR() && !AT() && !BT() ){	// Atendimento (Saida)
						return T_CONNECT;
					}
				}
			
				//----------------------------------------------------------------------------------------------------------------

				if((Bit & DTMM_AON)		== DTMM_AON)
				{
					return T_DISCONNECT;
				}

				//----------------------------------------------------------------------------------------------------------------

				if((Bit & DTMM_BON)		== DTMM_BON)
				{
					//	1				0				1				0		Estado anterior (LIVRE)
					//	1				1				1				0		Estado atual (BLOQUEIO)
					//	1				1				1				1		Estado destino (BLOQUEADO)
					if( AR() && BR() && AT() && !BT() ){
						return T_BLOCKED;
					}

					//	1			   1			 	0				 0		Confirmacao de Ocupacao (saida)
					if( AR() && BR() && !AT() && !BT() ){
						ScConnect(FULLDUP);
						return T_SEIZE;
					}
				}

				//----------------------------------------------------------------------------------------------------------------

				if((Bit & DTMM_BOFF)	== DTMM_BOFF)
				{
					//	1				1				1				1		Estado anterior (BLOQUEADO)
					//	1				0				1				1		Estado atual (RETIRADA DO BLOQUEIO)
					//	1				0				1				0		Estado destino (LIVRE)
					if( AR() && !BR() && AT() && !BT() ){
						return T_IDLE;
					}
				}

				//----------------------------------------------------------------------------------------------------------------
			break;

			case TDX_CST:                                                                                                                                         

				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;

				switch(Event){

					case DE_TONEON:
						CtiLog("Recv      #%x", SigChar(Data) - 48);
					return T_TONEON;

					case DE_TONEOFF:
						//CtiLog("Recv      #%x", SigChar(Data) - 48);
					return T_TONEOFF;
				}
			break;

			case TDX_PLAYTONE:
				return T_PLAYTONE;

			case TDX_PLAY:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_PLAY;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_ERROR:
				return T_ERROR;

			case T_USER_BLOCK: // Evento para bloquear o canal
                CtiLog("T_USER_BLOCK");
				FlagUserBlock = true;
				return T_USER_BLOCK;

			case T_USER_UNBLOCK: // Evento para desbloquear o canal
				FlagUserBlock = false;
				return T_USER_UNBLOCK;

			default:
				return EventType;
		}

		return -1; // Sem eventos
	}
	catch(...)
	{
		PutError("Exception Dialogic::R2WaitEvent()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::CasBitState(int Bit)
{
	try {
	
		switch(Bit){

			case BIT_AR:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVA) ? 1 : 0 );

			case BIT_BR:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVB) ? 1 : 0 );

			case BIT_CR:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVC) ? 1 : 0 );

			case BIT_DR:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVD) ? 1 : 0 );

			case BIT_AT:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTA) ? 1 : 0 );

			case BIT_BT:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTB) ? 1 : 0 );

			case BIT_CT:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTC) ? 1 : 0 );
			
			case BIT_DT:
				return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTD) ? 1 : 0 );
		}
		return 0;
	}
  catch(...)
	{
		PutError("Exception Dialogic::CasBitState()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::CasSetBit(int bit)
{
	try {

		switch(bit){

			case AON:
				if(dt_settssigsim(Dti, DTB_AON)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case AOFF:
				if(dt_settssigsim(Dti, DTB_AOFF)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case BON:
				if(dt_settssigsim(Dti, DTB_BON)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case BOFF:
				if(dt_settssigsim(Dti, DTB_BOFF)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case CON:
				if(dt_settssigsim(Dti, DTB_CON)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case COFF:
				if(dt_settssigsim(Dti, DTB_COFF)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case DON:
				if(dt_settssigsim(Dti, DTB_DON)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
			case DOFF:
				if(dt_settssigsim(Dti, DTB_DOFF)){
					PutError(ATDV_ERRMSGP(Dti));
				}
			break;
		}

		LogCasBits();
	}
  catch(...)
	{
		PutError("Exception Dialogic::CasSetBit()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::AT(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTA) ? 1 : 0 );
	}
  catch(...){
		PutError("Exception Dialogic::AT()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::AR(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVA) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::AR()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::BT(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTB) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::BT()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::BR(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVB) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::BR()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::CT(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTC) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::CT()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::CR(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVC) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::CR()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::DT(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_XMTD) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::DT()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int	Dialogic::DR(void)
{
	try {
		return ( (ATDT_TSSGBIT(Dti) & DTSG_RCVD) ? 1 : 0 );
	}
  catch(...)
	{
		PutError("Exception Dialogic::DR()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::R2_CreateForwardTones(void)
{
	bool bEntered;
	int i;								 
	
	try {

		if(R2ForwardTones){
			// Se os tons r2 para frente foram criados, nao precisa criar de novo
			return;
		}

		EnterCriticalSection(&CritSecR2Tones);
		bEntered = true;

		// Para o canal de voz sincronamente
		if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
		}

		// Apaga tons criados
		dx_deltones(Vox);

		// Cria os tons para frente
		for(i=1; i<=15; i++){ 
	
			if(dx_blddt((i+SIG_BASE),forward_sig[i][0],R2_DEVFREQ,forward_sig[i][1],R2_DEVFREQ,TN_LEADING)){
				PutError(ATDV_ERRMSGP(Vox));
			}
			if(dx_addtone(Vox, NULL, 0)){
				PutError(ATDV_ERRMSGP(Vox));
			}
		}

		// Seta um flag indicando que os tons para frente ja foram criados
		R2ForwardTones	= true;

		// Seta um flag indicando que os tons para tras estao deletados
		R2BackwardTones	= false;

		LeaveCriticalSection(&CritSecR2Tones);
		bEntered = false;
	}
  catch(...)
	{
		if(bEntered){
			LeaveCriticalSection(&CritSecR2Tones);
		}
		PutError("Exception Dialogic::R2_CreateForwardTones()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::R2_CreateBackwardTones(void)
{
	bool bEntered;
	int i;
	
	try {

		if(R2BackwardTones){
			// Se os tons r2 para tras foram criados, nao precisa criar de novo
			return;
		}

		EnterCriticalSection(&CritSecR2Tones);
		bEntered = true;

		// Para o canal de voz sincronamente
		if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
		}

		// Apaga tons criados
		dx_deltones(Vox);

		// cria tons para TRAS
		for(i=1; i<=15; i++){ 

			if(dx_blddt((i+SIG_BASE),backward_sig[i][0],R2_DEVFREQ,backward_sig[i][1],R2_DEVFREQ,TN_LEADING)){
				PutError(ATDV_ERRMSGP(Vox));
			}
			if(dx_addtone(Vox, NULL, 0)){
				PutError(ATDV_ERRMSGP(Vox));
			}
		}

		// Seta um flag indicando que os tons para tras foram criados
		R2BackwardTones	= true;

		// Seta um flag indicando que os tons para frente foram deletados
		R2ForwardTones	= false;

		LeaveCriticalSection(&CritSecR2Tones);
		bEntered = false;
	}
  catch(...)
	{
		if(bEntered){
			LeaveCriticalSection(&CritSecR2Tones);
		}
		PutError("Exception Dialogic::R2_CreateBackwardTones()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::R2_PlayForward(int Tone)
{
	int Sig;

	try {

		Sig = Tone - SIG_BASE;

		if( Sig == 0 ){
			Sig = 10;
		}

		dx_bldtngen(&tngen,
								forward_sig[Sig][0],	// primeira frequencia
								forward_sig[Sig][1],	// segunda frequencia
								R2_AMPLITUDE,					// amplitude da primeira frequencia
								R2_AMPLITUDE,					// amplitude da segunda frequencia
								R2_TIME_TONE * 100);	// duracao do tom em unidades de 10ms

		dx_clrtpt(&dv_tpt[0], 2);

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(R2_TIME_TONE * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		if(dx_playtone(Vox, &tngen, &dv_tpt[0], EV_ASYNC)){
			PutError(ATDV_ERRMSGP(Vox));
			return -1;
		}

		CtiLog("Send #%x", Sig);

		return 0;
	}
  catch(...)
	{
		PutError("Exception Dialogic::R2_PlayForwardTone()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::R2_PlayBackward(int Tone)
{
	int Sig;

	try {

		// Para o canal de voz sincronamente
		if(ATDX_STATE(Vox) != CS_IDLE){
#ifdef _ASR
			if( !bAsrEnabled )
				dx_stopch(Vox, EV_ASYNC);
			else
				ec_stopch(Vox, SENDING, EV_ASYNC);
#else
			dx_stopch(Vox, EV_ASYNC);
#endif // _ASR
		}

		Sig = Tone - SIG_BASE;

		dx_bldtngen(&tngen,
								backward_sig[Sig][0],	// primeira frequencia
								backward_sig[Sig][1],	// segunda frequencia
								R2_AMPLITUDE,					// amplitude da primeira frequencia
								R2_AMPLITUDE,					// amplitude da segunda frequencia
								R2_TIME_TONE * 100);	// duracao do tom em unidades de 10ms

		dx_clrtpt(&dv_tpt[0], 2);

		// seta o tempo maximo para tocar o tom
		dv_tpt[0].tp_type   = (unsigned short)IO_EOT;
		dv_tpt[0].tp_termno = (unsigned short)DX_MAXTIME;
		dv_tpt[0].tp_length = (unsigned short)(R2_TIME_TONE * 10); // tempo em unidades de 100ms
		dv_tpt[0].tp_flags  = (unsigned short)TF_MAXTIME;

		Sleep(1);

		if(dx_playtone(Vox, &tngen, &dv_tpt[0], EV_ASYNC)){
			PutError(ATDV_ERRMSGP(Vox));
			return -1;
		}

		CtiLog("Send #%x", Sig);

		return 0;
	}
  catch(...)
	{
		PutError("Exception Dialogic::R2_PlayBackward()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::LogCasBits(void)
{
	try {

		// Em placas DM3 algumas funcoes da R4 API nao sao suportadas
		if( ( TipoSinalizacao == GLOBAL_CALL_DM3 ) || ( TipoSinalizacao == GLOBAL_CALL_SS7 ) || ( TipoSinalizacao == GLOBAL_CALL_IP ) )
			return;

		if( TipoProtocolo == "ISDN" )
			return;

		// loga o estado dos bits em CAS MODE
		CtiLog("AR BR AT BT");
		CtiLog("%d  %d  %d  %d",	CasBitState(BIT_AR),
															CasBitState(BIT_BR),
															CasBitState(BIT_AT),
															CasBitState(BIT_BT));

		// Coloca o status dos bits na tela
		PrintCasBits();
	}
	catch(...){
		PutError("Exception Dialogic::LogCasBits()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Dialogic::PrintCasBits(void)
{
	try {

		int AR, BR, AT, BT;

		switch(TipoSinalizacao){

			case R2:
			case R2_DIGITAL:
			case LINE_SIDE:
			case GLOBAL_CALL:

				if( TipoProtocolo == "ISDN" )
					return;

				AR = CasBitState(BIT_AR);
				BR = CasBitState(BIT_BR);
				AT = CasBitState(BIT_AT);
				BT = CasBitState(BIT_BT);

				sprintf(sCasBits, "%d%d%d%d", AR, BR, AT, BT);

				//Win::SendMessageToAllScreens( "SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&BITS=%s\r\n", iListIndex, iBoardTrunk, iBoardPort, sCasBits );

			break;
		}
	}
	catch(...){
	}
}

//------------------------------------------------------------------------------------------------------------------------


/********************************************************************************************
	NOME:	ProcLSI

	DESCRICAO:	Processa os eventos para o canal de voz associado a LSI

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h ou -1 se nao tem evento
********************************************************************************************/
int Dialogic::ProcLSI( int Time )
{
	try {

		unsigned int ToneId = 0;

		Devs[0] = Vox;
		Devs[1] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 1, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 1, Time, &EventHandle) == -1)
				return -1;
		}

		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case TDX_CST:                                                                                                                                         
				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;
				
				switch(Event){

					default:
						return EventType;

					case DE_TONEOFF:	// recebeu tom de ocupado do pabx
					case DE_TONEON:		// recebeu tom de ocupado do pabx
					case DE_LCOFF:		// recebeu loop de corrente OFF
						ToneId = (unsigned int) Data;
						CtiLog("DE_TONEON: ToneId=%d", ToneId);
						// Verifica se o tom e nota (de melodia).
						if( ToneId > 1000 )
						{
							if(ToneId == 1011)
								return T_MESSAGEINI;	
							CheckMESSAGEBOXTone(ToneId);
							if( DetectTones( TST_VOICE_MAIL ) )
								return T_MESSAGEBOX;
							return T_NO_EVENTS;
						}
						return T_DISCONNECT;

					case DE_RINGS:
						FlagDisconnected = false;
						iDirection = CH_INBOUND;
						return T_CALL;

					case DE_DIGITS:
						LastDigit = (char) Data; // guarda o digito recebido
						return T_DIGIT;
				}
			break;

			case TDX_DIAL:
				return T_DIAL;

			case TDX_CALLP:
				CpTerm = ATDX_CPTERM(Vox);

				switch(CpTerm){

					case CR_CEPT:
					case CR_FAXTONE:
					case CR_CNCT:		// atendeu
						return T_CONNECT;

					case CR_BUSY:		// linha ocupada
						return T_BUSY;

					case CR_NOANS:	// nao responde
						return T_NOANSWER;

					case CR_NODIALTONE: // sem tom de linha
						return T_NODIALTONE;

					case CR_STOPD:
					case CR_ERROR:			// erro na chamada
					case CR_NORB:				// sem ringback
						return T_ERROR;
				}
			break;

			case TDX_PLAY:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_PLAY;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_PLAYTONE:
				return T_PLAYTONE;

			case TDX_ERROR:
				return T_ERROR;

			case T_USER_BLOCK: // Evento para bloquear o canal
                CtiLog("T_USER_BLOCK");
				FlagUserBlock = true;
				return T_USER_BLOCK;

			case T_USER_UNBLOCK: // Evento para desbloquear o canal
				FlagUserBlock = false;
				return T_USER_UNBLOCK;

			default:
				return EventType;
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcLSI()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	ProcLSITrap

	DESCRICAO:	Processa os eventos para o canal de voz associado a LSI para Grampo Analogico
							(Nao atende chamadas, so ouve o canal)

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h ou -1 se nao tem evento
********************************************************************************************/
int Dialogic::ProcLSITrap( int Time )
{
	try {

		Devs[0] = Vox;
		Devs[1] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 1, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 1, Time, &EventHandle) == -1)
				return -1;
		}

		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case TDX_CST:                                                                                                                                         
				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;
				
				switch(Event){

					case DE_SILON:
						return T_SILENCE_ON;

					case DE_SILOF:
						return T_SILENCE_OFF;

					case DE_LCON:
						return T_LOOP_CURRENT_ON;

					case DE_LCOF:
						return T_LOOP_CURRENT_OFF;
				}
			break;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_ERROR:
				return T_ERROR;

			default:
				return EventType;
		}

		return -1;
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcLSITrap()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	DialLSI

	DESCRICAO:	Disca pelo canal de voz associado a LSI 

	PARAMETROS:	char *Number			- Numero para discar
							bool CallProcess		- Se true faz CallProgress, false faz discagem cega

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::DialLSI(char *Number, bool CallProcess)
{
	int Ret;

	try {

		if(CallProcess)
			Ret = dx_dial(Vox, Number, &dx_cap, DX_CALLP | EV_ASYNC);
		else
			Ret = dx_dial(Vox, Number, &dx_cap, EV_ASYNC);

		if(Ret){
			PutError("dx_dial error in function DialLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		do {
			Event = GetEvent();
		}while(Event == -1);

		return(Event);	// retorna evento para aplicacao
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialLSI()");
		return -1;
	}
}
/********************************************************************************************
	NOME:	DialLSIAsync

	DESCRICAO:	Disca pelo canal de voz associado a LSI assincronamente

	PARAMETROS:	char *Number			- Numero para discar
							bool CallProgress	- Se true faz CallProgress, false faz discagem cega

	RETORNO:		0 sucesso, -1 erro
********************************************************************************************/
int Dialogic::DialLSIAsync(char *Number, bool CallProgress)
{
	int Ret;

	try {

		if( ATDX_HOOKST(Vox) == DX_ONHOOK	){
			// Se o canal estiver no gancho, tira do gancho antes de discar
			dx_sethook(Vox, DX_OFFHOOK, EV_SYNC);
		}

		if(CallProgress)
			Ret = dx_dial(Vox, Number, &dx_cap, DX_CALLP | EV_ASYNC);
		else
			Ret = dx_dial(Vox, Number, &dx_cap, EV_ASYNC);

		if(Ret){
			PutError("dx_dial error in function DialLSI() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}
		
		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialLSIAsync()");
		return -1;
	}
}



/********************************************************************************************
	NOME:	ScListen

	DESCRICAO:	Faz um dado canal ouvir um TimeSlot de outro canal

	PARAMETROS:	long Timeslot - TimeSlot para ouvir

	RETORNO: 0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::ScListen(long Timeslot)
{
	int iRet;
	long TimeSlotToListen;

	try
	{
		TimeSlotToListen			= Timeslot;

		sc_tsinfo.sc_numts		= 1;
		sc_tsinfo.sc_tsarrayp	= &TimeSlotToListen;

		switch( TipoInterface )
		{
			case E1:	
			case IP:
			case T1:
			case DTI:
				switch( TipoSinalizacao )
				{
					case GLOBAL_CALL:
						if( ( TipoProtocolo == "isdn" ) || ( TipoProtocolo == "ISDN" ) )
						{
							iRet	=	dt_listen( Dti, (SC_TSINFO *) &sc_tsinfo );
							if( iRet )
							{
								PutError( "Error in Dialogic::ScListen()->dt_listen(): %s", ATDV_ERRMSGP( Dti ) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						else
						{
							iRet = gc_Listen( ldev, (SC_TSINFO *) &sc_tsinfo, EV_SYNC );
							if( iRet )
							{
								PutError( "Error in Dialogic::ScListen()->gc_Listen(): %s", gc_GetStringError() );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						break;
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						/*
						CtiLog( "ScListen: Line device %d is not listening.", ldev);
						if( gc_UnListen( ldev, EV_SYNC ) == -1 )
						{
							PutError( "Error in Dialogic::ScConnect()->gc_UnListen(): %s", gc_GetStringError() );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						*/
						CtiLog ("ScListen: LineDev: %d is listening timeslot: %d", ldev, TimeSlotToListen);
						iRet = gc_Listen( ldev, (SC_TSINFO *) &sc_tsinfo, EV_SYNC );
						if( iRet )
						{
							PutError( "Error in Dialogic::ScListen()->gc_Listen(): %s", gc_GetStringError() );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						else
							CtiLog( "gcListen() Ok" );
						break;

					default:
						iRet	=	dt_listen( Dti, (SC_TSINFO *) &sc_tsinfo );
						if( iRet )
						{
							PutError( "Error in Dialogic::ScListen()->dt_listen(): %s", ATDV_ERRMSGP( Dti ) );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						break;
				}
				break;
			case LSI:
				iRet = ag_listen( Vox, (SC_TSINFO *) &sc_tsinfo );
				break;
			case MSI:
				iRet = ms_unlisten( Msi );
				iRet = ms_listen( Msi, (SC_TSINFO *) &sc_tsinfo );
				break;
			case VOX:
				iRet = dx_listen( Vox, (SC_TSINFO *) &sc_tsinfo );
				break;
			case FAX:
				iRet = fx_listen( Fax, (SC_TSINFO *) &sc_tsinfo );
				break;
			default:
				iRet = -1;
				break;
		}
		return iRet;
	}
	catch(...)
	{
		PutError("Exception in Dialogic::ScListen(Timeslot)");
		return -1;
	}
}
/********************************************************************************************
	NOME:	ScConnect

	DESCRICAO:	Faz uma conexao interna entre a interface de linha e o proprio canal de voz

	PARAMETROS:

	RETORNO: 0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::ScConnect(int mode)
{
	int iRet;
	long tslot = 0;

	try {

		switch(TipoInterface){

			case E1:
			case IP:
			case T1:
			case DTI:
				switch( TipoSinalizacao ){
					case GLOBAL_CALL:
						if( ( TipoProtocolo == "isdn" ) || ( TipoProtocolo == "ISDN" ) )
						{
							// Codigo especifico p/ Globalcall ISDN por nao suportar a funcao gc_Listen().
							if(mode == FULLDUP)
							{
								iRet = nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP);
								if( iRet )
								{
									PutError( "Error in Dialogic::ScConnect()->nr_scroute(): %s", ATDV_ERRMSGP( Dti ) );
									CtiLog( "%s", GetLastError() );
									return -1;
								}
							}
							else
							{
								iRet = nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_HALFDUP);
								if( iRet )
								{
									PutError( "Error in Dialogic::ScConnect()->nr_scroute(): %s", ATDV_ERRMSGP( Dti ) );
									CtiLog( "%s", GetLastError() );
									return -1;
								}
							}
						}
						else
						{
							// Codigo especifico p/ qualquer outro protocolo usado com Globalcall.
							sc_tsinfo.sc_numts		= 1;
							sc_tsinfo.sc_tsarrayp = &tslot;

							if( dx_unlisten( Vox ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							tslot = TimeslotLine;
							if( dx_listen( Vox, &sc_tsinfo ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							if(mode == FULLDUP)
							{
								if( gc_UnListen( ldev, EV_SYNC ) == -1 )
								{
									PutError( "Error in Dialogic::ScConnect()->gc_UnListen(): %s", gc_GetStringError() );
									CtiLog( "%s", GetLastError() );
									return -1;
								}
								tslot = TimeslotVox;
								if( gc_Listen( ldev, &sc_tsinfo, EV_SYNC ) == -1 )
								{
									PutError( "Error in Dialogic::ScConnect()->gc_Listen(): %s", gc_GetStringError() );
									CtiLog( "%s", GetLastError() );
									return -1;
								}
							}
						}
						break;
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						sc_tsinfo.sc_numts		= 1;
						sc_tsinfo.sc_tsarrayp = &tslot;

						if(mode == FULLDUP)
						{
							/*
							//Faz esse recurso de rede deixar de ouvir o que estava ouvindo
							CtiLog( "Line device %d is not listening.", ldev);
							if( gc_UnListen( ldev, EV_SYNC ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->gc_UnListen(): %s", gc_GetStringError() );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							//Faz o recurso de voz ouvir o Tx do linedev
							CtiLog( "Device %d is not listening.", Vox);
							if( dx_unlisten( Vox ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							*/
							if(ldev == dtidev)
								tslot = TimeslotLine;
							else
								tslot = TimeslotLineIP;
							CtiLog( "Device %d is listening timeslot %d", Vox, tslot);
							if( dx_listen( Vox, &sc_tsinfo ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_listen(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							tslot = TimeslotVox;
							//Faz esse recurso de rede ouvir o timeslot desejado (tslot)
							CtiLog( "Linedev %d is listening timeslot %d", ldev, tslot);
							if( gc_Listen( ldev, &sc_tsinfo, EV_SYNC ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->gc_Listen(): %s", gc_GetStringError() );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						else
						{
							// HALFDUP
							//Faz o recurso de voz ouvir o Tx do linedev
							CtiLog( "Device %d is not listening.", Vox);
							if( dx_unlisten( Vox ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
							tslot = TimeslotLine;
							CtiLog( "Device %d is listening timeslot %d", Vox, tslot);
							if( dx_listen( Vox, &sc_tsinfo ) == -1 )
							{
								PutError( "Error in Dialogic::ScConnect()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						return 0;

					default:
						if(mode == FULLDUP)
						{
							iRet = nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_FULLDUP);
							if( iRet )
							{
								PutError( "Error in Dialogic::ScListen()->ScConnect(): %s", ATDV_ERRMSGP( Dti ) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						else
						{
							iRet = nr_scroute(Dti, SC_DTI, Vox, SC_VOX, SC_HALFDUP);
							if( iRet )
							{
								PutError( "Error in Dialogic::ScListen()->ScConnect(): %s", ATDV_ERRMSGP( Dti ) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						break;
				}
			break;

			case LSI:
				if(mode == FULLDUP)
					iRet = nr_scroute(Vox, SC_LSI, Vox, SC_VOX, SC_FULLDUP);
				else
					iRet = nr_scroute(Vox, SC_LSI, Vox, SC_VOX, SC_HALFDUP);
				break;

			case MSI:
				if(mode == FULLDUP)
					iRet = nr_scroute(Msi, SC_MSI, Vox, SC_VOX, SC_FULLDUP);
				else
					iRet = nr_scroute(Msi, SC_MSI, Vox, SC_VOX, SC_HALFDUP);
				break;

			default:
				iRet = -1;
				break;
		}

		return iRet;
	}
	catch(...)
	{
		PutError("Exception in Dialogic::ScConnect(int mode)");
		return -1;
	}
}
/********************************************************************************************
	NOME:	ScUnlisten

	DESCRICAO:	Faz o canal do proprio objeto parar de ouvir qualquer conexao a que ele pertencia

	PARAMETROS:

	RETORNO: 0 sucesso
						-1 erro
********************************************************************************************/
int Dialogic::ScUnlisten(void)
{
	int iRet;

	try
	{

		switch( TipoInterface )
		{
			case E1:
			case IP:
			case T1:
			case DTI:
				switch( TipoSinalizacao )
				{
					case GLOBAL_CALL:
						if( ( TipoProtocolo == "isdn" ) || ( TipoProtocolo == "ISDN" ) )
						{
							// Codigo especifico p/ Globalcall ISDN por nao suportar a funcao gc_Listen().
							iRet	=	dt_unlisten( Dti );
							if( iRet )
							{
								PutError( "Error in Dialogic::ScUnlisten()->dt_unlisten(): %s", ATDV_ERRMSGP( Dti ) );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						else
						{
							if( gc_UnListen( ldev, EV_SYNC ) == -1 )
							{
								PutError( "Error in Dialogic::ScUnlisten()->gc_UnListen(): %s", gc_GetStringError() );
								CtiLog( "%s", GetLastError() );
								return -1;
							}
						}
						break;
					case GLOBAL_CALL_IP:
					case GLOBAL_CALL_SS7:
					case GLOBAL_CALL_DM3:
						CtiLog ("ScUnListen: LineDev: %d has been unlistened", ldev);
						if( gc_UnListen( ldev, EV_SYNC ) == -1 )
						{
							PutError( "Error in Dialogic::ScUnlisten()->gc_UnListen(): %s", gc_GetStringError() );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						break;

					default:
						iRet	=	dt_unlisten( Dti );
						if( iRet )
						{
							PutError( "Error in Dialogic::ScUnlisten()->dt_unlisten(): %s", ATDV_ERRMSGP( Dti ) );
							CtiLog( "%s", GetLastError() );
							return -1;
						}
						break;
				}
				break;
			case LSI:
				iRet = ag_unlisten( Vox );
				break;
			case MSI:
				iRet = ms_unlisten( Msi );
				break;
			case VOX:
				iRet = dx_unlisten( Vox );
				break;
			case FAX:
				iRet = fx_unlisten( Fax );
				break;
			default:
				iRet = -1;
				break;
		}
		return iRet;
	}
	catch(...)
	{
		PutError("Exception in Dialogic::ScUnlisten(void)");
		return -1;
	}
}


int Dialogic::VoxScListen(long timeslot)
{
	try
    {
	    long tslot;
        sc_tsinfo.sc_numts = 1;
        sc_tsinfo.sc_tsarrayp = &tslot;

		/*
		CtiLog( "Device %d is not listening.", Vox);
    if( dx_unlisten( Vox ) == -1 )
	  {
		  PutError( "Error in Dialogic::VoxScListen()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
		  CtiLog( "%s", GetLastError() );
		  return -1;
	  }
		*/

	    tslot = timeslot;
	    CtiLog( "Device %d is listening timeslot %d", Vox, tslot);
	    if( dx_listen( Vox, &sc_tsinfo ) == -1 )
	    {
		    PutError( "Error in Dialogic::VoxScListen()->dx_unlisten(): %s", ATDV_ERRMSGP(Vox) );
		    CtiLog( "%s", GetLastError() );
		    return -1;
	    }

        return 0;
	}
	catch(...)
	{
		PutError("Exception in Dialogic::VoxScListen()");
		return -1;
	}
}

int Dialogic::ScRouteCallIP()
{
    try
    {
        long tslot;
        sc_tsinfo.sc_numts = 1;
        sc_tsinfo.sc_tsarrayp = &tslot;

        tslot = TimeslotLine;
        if( gc_Listen( ipdev, &sc_tsinfo, EV_ASYNC ) == -1 )
        {
	        PutError( "Error in Dialogic::ScRouteCallIP()->gc_listen() ip: %s", ATDV_ERRMSGP(Vox) );
	        CtiLog( "%s", GetLastError() );
	        return -1;
        }

        do
        {
	        Event = GetEvent();
        } while( Event != -1 && Event != T_LISTEN && Event != T_DISCONNECT && Event != T_TASKFAIL );

        tslot = TimeslotLineIP;
        if( gc_Listen( dtidev, &sc_tsinfo, EV_SYNC ) == -1 )
        {
	        PutError( "Error in Dialogic::ScRouteCallIP()->gc_listen() dti: %s", ATDV_ERRMSGP(Vox) );
	        CtiLog( "%s", GetLastError() );
	        return -1;
        }

        return 0;
    }
    catch(...)
    {
        PutError("Exception in Dialogic::ScRouteCallIP()");
        return -1;
    }
}



/*************************************************************************************************************************
	 ----------------------------
	|	RX			TX								 |
	|	---------------------------|
	| A | B |	A | B |	LINE STATE |
	|	---------------------------|
	| 0 | 1 |	0 | 1 | IDLE			 |
	| 0 | 0 |	0 | 1 | RING			 |
	| 0 | 0 |	1 | 1 | OFFHOOK		 |
	| 0 | 1 |	0 | 1 | ONHOOK		 |
	 ----------------------------

	A sinalizacao CAS utilizada, simula ramais analogicos nos canais E1, desta forma.

	O BitB (Recepcao) se comporta como a campainha de um telefone analogico
		BitB = 1 significa Campainha OFF
		BitB = 0 significa Campainha ON
	Desta forma quando uma chamada de entrada e recebida em um canal este bit vai
	para O durante 1s e volta para 1 por 4s, e este ciclo se repetira ate que a chamada
	seja atendido ou desligada.

	O BitA (Transmissao) se comporta como a chave de gancho (hook) de um telefone analogico

		BitA = 0 fone no gancho
		BitA = 1 fone fora do gancho

	Desta forma para atender uma chamada de entrada que foi sinalizada atraves da mudanca
	do BitB de 1 para 0, tudo que tem que ser feito e mudar o bit A de 0 para 1.

	Para a URA desconectar a chamada � so voltar o BIT A para 0 (colocar o fone no gancho)

	Se o originador desconectar a chamada (como em um telefone analogico) nenhum bit sera mudado,
	sera enviado um sinal acustico (tom de ocupado) para o canal da URA, neste caso a URA detecta
	o sinal de ocupado e libera o canal colocando o BIT A = 0.

	Para gerar uma chamada a URA tem que tirar o fone do gancho = BIT A = 1 e discar o numero 
	de destino em tons DTMF.

	Para transferir uma chamada a URA tera que executar um flash (BIT A = 0, espera 150ms e volta BIT A 1)
	discar o numero destino em DTMF, aguardar 1 segundo e desligar (BIT A = 0).

*************************************************************************************************************************/

int Dialogic::ProcLineSide( int Time )
{
	try {
		
		unsigned int ToneId = 0;
		Devs[0] = Vox;
		Devs[1] = Dti;
		Devs[2] = 0;

		if(FlagEndObject){
			JumpExit();
		}

		if( Time < 0 )
		{
			if(sr_waitevtEx(Devs, 2, TimeToWaitEvent, &EventHandle) == -1)
				return -1;
		}
		else
		{
			if(sr_waitevtEx(Devs, 2, Time, &EventHandle) == -1)
				return -1;
		}

		datap			= (void *)sr_getevtdatap(EventHandle);
		
		cstp			= (DX_CST *)sr_getevtdatap(EventHandle);

		Event			=	cstp->cst_event;
		Data			=	cstp->cst_data;
		
		EventType	= sr_getevttype(EventHandle);

		switch(EventType){

			case DTEV_SIG:                                                                                                                                        

				Bit = *((unsigned short *)sr_getevtdatap(EventHandle));                                                                                                         

				if((Bit & DTMM_BOFF)	== DTMM_BOFF){
					// Confirma a ocupacao do canal
					CasSetBit(AON);
					// Seta a Causa default da desconexao quando o sistema desliga a ligacao		
					CauseDropCall	= DROP_NORMAL;

					// Retorno normal para a aplicacao atender a ligacao
					FlagDisconnected = false;

					ChannelStatus = CHANNEL_OFFERED;

					iDirection = CH_INBOUND;

					return T_CALL;
				}

				if((Bit & DTMM_BON)		== DTMM_BON){
				}

				if((Bit & DTMM_AON)		== DTMM_AON){
				}

				if((Bit & DTMM_AOFF)	== DTMM_AOFF){
				}
			break;

			case TDX_CST:                                                                                                                                         

				cstp	= (DX_CST *)sr_getevtdatap(EventHandle);
				Event	= cstp->cst_event;
				Data	= cstp->cst_data;

				switch(Event){

					case DE_TONEON:	
						ToneId = (unsigned int) Data;
						CtiLog("DE_TONEON: ToneId=%d", ToneId);
						if(ToneId == 1)						// Detectou Hangup1 ou Hangup2 
							return T_DISCONNECT;
						else
							return T_NO_EVENTS;

					case DE_DIGITS:
						LastDigit = (char) Data; // guarda o digito recebido
						return T_DIGIT;
				}
			break;

			case TDX_PLAY:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_PLAY;

			case TDX_RECORD:
				//Limpa a lista de Prompts
				ClearPrompts();
				return T_RECORD;

			case TDX_GETDIG:
				return T_MAXDIG;

			case TDX_ERROR:
				return T_ERROR;

			case T_USER_BLOCK: // Evento para bloquear o canal
                CtiLog("T_USER_BLOCK");
				FlagUserBlock = true;
				return T_USER_BLOCK;

			case T_USER_UNBLOCK: // Evento para desbloquear o canal
				FlagUserBlock = false;
				return T_USER_UNBLOCK;

			default:
				return EventType;
		}

		return -1; // Sem eventos
	}
	catch(...)
	{
		PutError("Exception Dialogic::ProcLineSide()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::LineSideTransfer(char *sNumber)
{
	string sDialStr;

	try {

		StopCh(1000);

		// disca em tom
		sDialStr = "T";
		sDialStr += sNumber;

		// Efetua um hookflash digital
		CasSetBit(AOFF);
		// Espera 400 ms para o hookflash ter efeito
		Sleep(400);
		CasSetBit(AON);

		if( dx_dial(Vox, sDialStr.c_str(), &dx_cap, EV_SYNC) ){
			PutError("dx_dial error in function LineSideTransfer() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		// espera mais ou menos 1 segundo antes de desligar a chamada apos discar
		Sleep(1200);

		// desliga
		CasSetBit(AOFF);

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::LineSideTransfer()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Dialogic::DialLineSide(char *sNumber)
{
	string sDialStr;

	try {

		StopCh(1000);

		// disca em tom
		sDialStr = "T";
		sDialStr += sNumber;

		// Offhook
		CasSetBit(AON);

		if( dx_dial(Vox, sDialStr.c_str(), &dx_cap, EV_SYNC) ){
			PutError("dx_dial error in function LineSideTransfer() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::DialLineSide()");
		return -1;
	}
}




/********************************************************************************************
	NOME:	OpenChannelIP

	DESCRICAO:	Abre canal com o protcolo IP H323/SIP

	PARAMETROS:	int iChannel				- Canal
							int VoiceResource		- Define a abertura do device de Voz	
							char	*Protocolo		- String que define o protocolo para GlobalCall

	RETORNO:		0  se sucesso
							-1 se erro
********************************************************************************************/
int Dialogic::OpenChannelIP(bool VoiceResource, const char *Protocolo)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try {

	  int placa, ipChannel;
		char sDevName[256];
		long tslot;
		SC_TSINFO sc_tsinfo;

		if(VoiceResource)
		{
			OFFSET_DTI++;			// pula os devices de rede

		// A interface E1 tem 30 canais, entao determina em qual placa esta o canal
			if( (OFFSET_DTI % 30) == 0 )
				placa = ((OFFSET_DTI - 1) / 30) + 1;
			else
				placa = (OFFSET_DTI / 30) + 1;

			sprintf(sDevName,"dxxxB%dC%d", (OFFSET_VOX%4) ? ((OFFSET_VOX/4)+1) : (1+(OFFSET_VOX/4)-1),
																			 (OFFSET_VOX%4) ? (OFFSET_VOX%4) : 4);

			CtiLog( "Voice Resource: %s", sDevName );

			Vox = dx_open(sDevName, 0);
			if(Vox == -1){
				PutError("dx_open() Error - Channel %d Device %s: %s", Canal, sDevName, ATDV_ERRMSGP(Vox));
				return -1;
			}

			OffSetVoxIncrement( E1, GLOBAL_CALL_DM3 );

			// guarda o timeslot VOX de transmissao para uso posterior
			sc_tsinfo.sc_numts		= 1;
			sc_tsinfo.sc_tsarrayp = &tslot;

			if(dx_getxmitslot(Vox, &sc_tsinfo) == -1){
				PutError("dx_getxmitslot() error in function OpenChannelE1() Channel %d: %s", Canal, ATDV_ERRMSGP(Vox));
				return -1;
			}

			TimeslotVox = tslot;
			CtiLog( "Vox TX ts: %ld", TimeslotVox );
		}

		ipChannel = ((iBoardTrunk - 1) * 30) + iBoardPort;

		// Placa de interface IP
		if( ipChannel == 1)
			OFFSET_IPT++;

		placa = OFFSET_IPT;
		ProtocoloIP = Protocolo;


		// Abre o protocolo H323/SIP para telefonia IP sem recurso de voz
//		sprintf(sDevName, ":N_iptB%dT%d:P_%s:M_ipmB%dC%d", placa, ipChannel, Protocolo, placa, ipChannel);
		sprintf(sDevName, ":N_iptB%dT%d:P_IP:M_ipmB%dC%d", placa, ipChannel, placa, ipChannel);
		CtiLog( "LineDevice: %s", sDevName );

		if(gc_OpenEx(&ipdev, sDevName, EV_ASYNC, NULL) != GC_SUCCESS) {
			PutError("gc_OpenEx() error in function OpenChannelIP() Device %s: %s", sDevName, gc_GetStringError());
			return -1;
		}

		do
		{
			Event = GetEvent();
		} while( (Event != -1) && (Event != T_TASKFAIL) && (Event != T_OPENEX) );

		if( (Event == T_TASKFAIL) || (Event == -1) ) {
			PutError("gc_OpenEx() error in function OpenChannelIP() Device %s: %s", sDevName, gc_GetStringError());
			return -1;
		}

		return 0;
	}
	catch(...)
	{
		PutError("Exception Dialogic::OpenChannelIP()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	IPDialAsync

	DESCRICAO:	Disca pelo canal IP com GlobalCall assincronamente

	PARAMETROS:	Nenhum

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::IPDialAsync(char *Dnis, char *Ani)
{
	try 
    {
        int Event;
        GC_MAKECALL_BLK gcmkbl;
        GCLIB_MAKECALL_BLK gclib_mkbl = {0};
        gcmkbl.cclib = NULL;
        gcmkbl.gclib = &gclib_mkbl;
        GC_PARM_BLK *target_datap = NULL;

        ::strcpy(Dialogic::Dnis,	Dnis);	// Numero a ser chamado
        ::strcpy(Dialogic::Ani,		Ani);		// Numero do chamador

        IPSetCapabilities();

//		if(gc_SetCallingNum(ipdev, Ani) != GC_SUCCESS) {
//			CtiLog( gc_GetStringError() );
//			return -1;
//		}

        // set GCLIB_ADDRESS_BLK with destination string & type
        ::strcpy(gcmkbl.gclib->destination.address,Dnis);
        ::strcpy(gcmkbl.gclib->origination.address,Ani);

		if(ProtocoloIP == "SIP")
		{
		    gcmkbl.gclib->destination.address_type = GCADDRTYPE_TRANSPARENT; 
			gcmkbl.gclib->origination.address_type = GCADDRTYPE_TRANSPARENT;
			gc_util_insert_parm_val(&target_datap, IPSET_PROTOCOL, IPPARM_PROTOCOL_BITMASK, sizeof(char), IP_PROTOCOL_SIP);
		}
        else
		{
			gcmkbl.gclib->destination.address_type = GCADDRTYPE_IP; 
			gcmkbl.gclib->origination.address_type = GCADDRTYPE_IP;
			gc_util_insert_parm_val(&target_datap, IPSET_PROTOCOL, IPPARM_PROTOCOL_BITMASK, sizeof(char), IP_PROTOCOL_H323);
		}

        gclib_mkbl.ext_datap = target_datap;
        
        if(gc_MakeCall(ipdev, &crn, NULL, &gcmkbl, 0, EV_ASYNC) != GC_SUCCESS)
        {
			CtiLog( gc_GetStringError() );
			return -1;
		}

        //CrnToStatusPair pAlerting = make_pair(crn, CHANNEL_DIALING);
        //mmapDev2Crn.emplace( ipdev, pAlerting );




        gc_util_delete_parm_blk(target_datap);

		do
		{
			Event = GetEvent();
		} while( (Event == -1) || (Event == T_ALERTING) );

		return Event;
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::IPDialAsync()");
		PutError("Exception Dialogic::IPDialAsync()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	IPSetCapabilities

	DESCRICAO:	Configura informa��es do CODEC
	PARAMETROS:	Nenhum

	RETORNO:		0  se sucesso
						 -1  se erro
********************************************************************************************/
int Dialogic::IPSetCapabilities()
{
	try {
		IP_CAPABILITY	ip_capability;
		GC_PARM_BLKP	gcParmBlk = NULL;

		// Inicializa os parametros para IPLINK (Telefonia IP)
		memset(&ip_capability, 0, sizeof(IP_CAPABILITY));

		/* Set default coders for this ldev */
		ip_capability.capability = GCCAP_AUDIO_g7231_5_3k;
		ip_capability.direction = IP_CAP_DIR_LCLTRANSMIT ; //IP_CAP_DIR_LCLRECEIVE;
		ip_capability.type = GCCAPTYPE_AUDIO;
		ip_capability.payload_type	=	IP_USE_STANDARD_PAYLOADTYPE;
		ip_capability.extra.audio.frames_per_pkt = 1;
		ip_capability.extra.audio.VAD = GCPV_DISABLE;  //GCPV_ENABLE
		gc_util_insert_parm_ref(&gcParmBlk, GCSET_CHAN_CAPABILITY, IPPARM_LOCAL_CAPABILITY, sizeof(IP_CAPABILITY), &ip_capability);

		ip_capability.direction = IP_CAP_DIR_LCLRECEIVE ;
		gc_util_insert_parm_ref(&gcParmBlk, GCSET_CHAN_CAPABILITY, IPPARM_LOCAL_CAPABILITY, sizeof(IP_CAPABILITY), &ip_capability);

//		ip_capability.capability = GCCAP_AUDIO_g711Ulaw64k;
		ip_capability.capability = GCCAP_AUDIO_g711Alaw64k;
		ip_capability.direction = IP_CAP_DIR_LCLTRANSMIT;
//		ip_capability.extra.audio.frames_per_pkt = 30;
		ip_capability.extra.audio.frames_per_pkt = 20;
		gc_util_insert_parm_ref(&gcParmBlk, GCSET_CHAN_CAPABILITY, IPPARM_LOCAL_CAPABILITY, sizeof(IP_CAPABILITY), &ip_capability);

		ip_capability.direction = IP_CAP_DIR_LCLRECEIVE;
		gc_util_insert_parm_ref(&gcParmBlk, GCSET_CHAN_CAPABILITY, IPPARM_LOCAL_CAPABILITY, sizeof(IP_CAPABILITY), &ip_capability);

		//determines who sends the Call Proceeding
//		gc_util_insert_parm_val(&gcParmBlk, GCSET_CALL_CONFIG, GCPARM_CALLPROC, sizeof(unsigned char), GCCONTROL_TCCL);

		gc_util_insert_parm_val(&gcParmBlk,IPSET_DTMF,IPPARM_SUPPORT_DTMF_BITMASK, sizeof(char), IP_DTMF_TYPE_INBAND_RTP);

		gc_SetUserInfo(GCTGT_GCLIB_CHAN, ipdev, gcParmBlk, GC_SINGLECALL);
		
		gc_util_delete_parm_blk(gcParmBlk);

		return 0;

	}
	catch(...)
	{
		CtiLog("Exception Dialogic::IPSetCapabilities()");
		PutError("Exception Dialogic::IPSetCapabilities()");
		return -1;
	}
}

/********************************************************************************************
	NOME:	BlindTransferIP

	DESCRICAO:	Pedido para transfer�ncia de chamada supervisionada  

	PARAMETROS:	Nenhum

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::BlindTransferIP(char *Dnis)
{
	try {
#ifdef _HMP
		int Event;
		GC_MAKECALL_BLK gcmkbl;
		GCLIB_MAKECALL_BLK gclib_mkbl = {0};
		gcmkbl.cclib = NULL;
		gcmkbl.gclib = &gclib_mkbl;
		GC_PARM_BLK *target_datap = NULL;

		IPSetCapabilities();

    // set GCLIB_ADDRESS_BLK with destination string & type
    strcpy(gcmkbl.gclib->destination.address,Dnis);

		if(ProtocoloIP == "SIP")
		{
			gcmkbl.gclib->destination.address_type = GCADDRTYPE_TRANSPARENT; 
			gcmkbl.gclib->origination.address_type = GCADDRTYPE_TRANSPARENT;
			gc_util_insert_parm_val(&target_datap, IPSET_PROTOCOL, IPPARM_PROTOCOL_BITMASK, sizeof(char), IP_PROTOCOL_SIP);
		}
			else
		{
			gcmkbl.gclib->destination.address_type = GCADDRTYPE_IP; 
			gcmkbl.gclib->origination.address_type = GCADDRTYPE_IP;
			gc_util_insert_parm_val(&target_datap, IPSET_PROTOCOL, IPPARM_PROTOCOL_BITMASK, sizeof(char), IP_PROTOCOL_H323);
		}

    gclib_mkbl.ext_datap = target_datap;

		if(gc_InvokeXfer(crn, crn2, NULL, &gcmkbl, 0, EV_ASYNC) != GC_SUCCESS){
			PutError( "gc_InvokeXfer() error in Dialogic::BlindTransferIP(). %s.", gc_GetStringError() );
			CtiLog( gc_GetStringError() );
			return -1;
		}

    gc_util_delete_parm_blk(target_datap);

		do
		{
			Event = GetEvent();
		} while( (Event != -1) && (Event != T_TASKFAIL) && (Event != T_INVOKEXFER) );

		if( (Event == T_TASKFAIL) || (Event == -1) ) {
			PutError( "BlindTransferIP() Error in Dialogic::gc_InvokeXfer(). %s.", gc_GetStringError() );
			CtiLog( "%s", GetLastError() );
			return -1;
		}
		return 0;
#else
		return -1;
#endif // _HMP
	}
	catch(...)
	{
		CtiLog("Exception Dialogic::BlindTransferIP()");
		PutError("Exception Dialogic::BlindTransferIP()");
		return -1;
	}
}



/********************************************************************************************
	NOME:	ProcFAX

	DESCRICAO:	Processa os eventos de FAX para canal de fax

	PARAMETROS:

	RETORNO:		Evento de acordo com Dialogic.h
********************************************************************************************/
int Dialogic::ProcFAX( int Time )
{
	return 0;
}

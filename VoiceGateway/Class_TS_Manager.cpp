#include "Class_TS_Manager.h"




Class_TS_Manager::Class_TS_Manager()
{
	InitializeCriticalSection( &critTS_Manager );
	bEnterCriticalSection = false;
}

Class_TS_Manager::_TS_ALLOC_COUNTER::_TS_ALLOC_COUNTER()
{
	timeslot = -1;
	count = 0;
}

Class_TS_Manager::~Class_TS_Manager()
{
	DeleteCriticalSection( &critTS_Manager );
}

long Class_TS_Manager::AllocateGlobalTs(long LocalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;

        // Verifica se ja existe um timeslot do ring alocado para o timeslot passado
        if(GlobalMap.count(LocalTimeslot) <= 0)
        {
            if(GlobalList.empty())
            {
                // Error There aren't any Timeslot Ring available
                LeaveCriticalSection( &critTS_Manager );
                bEnterCriticalSection = false;
                return -1;
            }
            else
            {
                TimeSlotRet = GlobalList.front();
                GlobalList.pop_front();
                ts_alloc_counter.timeslot = TimeSlotRet;
                ts_alloc_counter.count = 1;
                GlobalMap[LocalTimeslot] = ts_alloc_counter;
            }
        }
        else
        {
            ts_alloc_counter = GlobalMap[LocalTimeslot];
            ts_alloc_counter.count++;
            GlobalMap[LocalTimeslot] = ts_alloc_counter;
            TimeSlotRet = ts_alloc_counter.timeslot;
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return TimeSlotRet;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::AllocateGlobalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

// Aloca um timeslot livre do Bus
long Class_TS_Manager::AllocateLocalTs(long GlobalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;

        // Verifica se ja existe um timeslot do bus alocado para o timeslot passado
        if(LocalMap.count(GlobalTimeslot) <= 0)
        {
            if(LocalList.empty())
            {
                // Error There aren't any Timeslot Bus available
                LeaveCriticalSection( &critTS_Manager );
                bEnterCriticalSection = false;

                return -1;
            }
            else
            {
                TimeSlotRet = LocalList.front();
                LocalList.pop_front();
                ts_alloc_counter.timeslot = TimeSlotRet;
                ts_alloc_counter.count = 1;
                LocalMap[GlobalTimeslot] = ts_alloc_counter;
            }
        }
        else
        {
            ts_alloc_counter = LocalMap[GlobalTimeslot];
            ts_alloc_counter.count++;
            LocalMap[GlobalTimeslot] = ts_alloc_counter;
            TimeSlotRet = ts_alloc_counter.timeslot;
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return TimeSlotRet;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::AllocateLocalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

long Class_TS_Manager::MapGlobalTs(long LocalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;

        // Verifica se ja existe um timeslot do ring alocado para o timeslot passado
        if(GlobalMap.count(LocalTimeslot) <= 0)
        {
            // Nao ha timeslot local associado a esse timeslot global.
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
            return -1;
        }
        else
        {
            ts_alloc_counter = GlobalMap[LocalTimeslot];
            TimeSlotRet = ts_alloc_counter.timeslot;
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return TimeSlotRet;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::MapGlobalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

// Aloca um timeslot livre do Bus
long Class_TS_Manager::MapLocalTs(long GlobalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;

        // Verifica se ja existe um timeslot do bus alocado para o timeslot passado
        if(LocalMap.count(GlobalTimeslot) <= 0)
        {
            // Nao ha timeslot global associado a esse timeslot local.
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
            return -1;
        }
        else
        {
            ts_alloc_counter = LocalMap[GlobalTimeslot];
            TimeSlotRet = ts_alloc_counter.timeslot;
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return TimeSlotRet;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::MapLocalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

// Desaloca um timeslot do Bus
int Class_TS_Manager::ReleaseGlobalTs(long LocalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;
        int rc = 0;

        // Verifica se timeslot do ring esta alocado
        if(GlobalMap.count(LocalTimeslot) > 0)
        {
            ts_alloc_counter = GlobalMap[LocalTimeslot];
            TimeSlotRet = ts_alloc_counter.timeslot;
            if( ts_alloc_counter.count > 1 )
            {
                ts_alloc_counter.count--;
                GlobalMap[LocalTimeslot] = ts_alloc_counter;
                rc = 0;
            }
            else
            {
                GlobalMap.erase(LocalTimeslot);
                GlobalList.push_back(TimeSlotRet);
                // Indica no retorno da funcao q trata-se da ultima instancia desse timeslot.
                rc = 1;
            }
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return rc;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::ReleaseLocalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

// Desaloca um timeslot do Ring
int Class_TS_Manager::ReleaseLocalTs(long GlobalTimeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        long TimeSlotRet = -1;
        TS_ALLOC_COUNTER ts_alloc_counter;
        int rc = 0;

        // Verifica se timeslot do bus esta alocado
        if(LocalMap.count(GlobalTimeslot) > 0)
        {
            ts_alloc_counter = LocalMap[GlobalTimeslot];
            TimeSlotRet = ts_alloc_counter.timeslot;
            if( ts_alloc_counter.count > 1 )
            {
                ts_alloc_counter.count--;
                LocalMap[GlobalTimeslot] = ts_alloc_counter;
                rc = 0;
            }
            else
            {
                LocalMap.erase(GlobalTimeslot);
                LocalList.push_back(TimeSlotRet);
                // Indica no retorno da funcao q trata-se da ultima instancia desse timeslot.
                rc = 1;
            }
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return rc;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::ReleaseGlobalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

int Class_TS_Manager::SetGlobalTsRange(long min, long max)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        if(min < 0 || max <= min || max < 0)
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;

            return -1;
        }
    
        for(long i = min; i <= max; i++)
        {
            GlobalList.push_back(i);
        }

        GlobalList.sort();

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return 0;
    }
    catch(...)
    {
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}


int Class_TS_Manager::SetLocalTsRange(long min, long max)
{
	try
	{
		EnterCriticalSection( &critTS_Manager );
		bEnterCriticalSection = true;

		if(min < 0 || max <= min || max < 0)
		{
			LeaveCriticalSection( &critTS_Manager );
			bEnterCriticalSection = false;

			return -1;
		}

		for(long i = min; i <= max; i++)
		{
			LocalList.push_back(i);
		}

		LocalList.sort();

		LeaveCriticalSection( &critTS_Manager );
		bEnterCriticalSection = false;

		return 0;
	}
	catch(...)
	{
		if( bEnterCriticalSection )
		{
			LeaveCriticalSection( &critTS_Manager );
			bEnterCriticalSection = false;
		}
		return 0xFFFFFFFF;
	}
}

// Obt�m um timeslot livre da lista de timeslots locais
long Class_TS_Manager::PopLocalTs(void)
{
    long timeslot;
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        if(LocalList.empty())		// Error There aren't any Timeslot Bus available
	        timeslot = -1;
        else
        {
            timeslot = LocalList.front();
            LocalList.pop_front();
        }

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return timeslot;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::PopLocalTs()
        if( bEnterCriticalSection )
        {
	        LeaveCriticalSection( &critTS_Manager );
	        bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

// Retorna o timeslot para a lista de timeslots locais
int Class_TS_Manager::PushLocalTs(long timeslot)
{
    try
    {
        EnterCriticalSection( &critTS_Manager );
        bEnterCriticalSection = true;
		
        LocalList.push_back(timeslot);

        LeaveCriticalSection( &critTS_Manager );
        bEnterCriticalSection = false;

        return 0;
    }
    catch(...)
    {
        // Exception Class_TS_Manager::PushLocalTs()
        if( bEnterCriticalSection )
        {
            LeaveCriticalSection( &critTS_Manager );
            bEnterCriticalSection = false;
        }
        return 0xFFFFFFFF;
    }
}

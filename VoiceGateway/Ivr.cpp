﻿#include "Ivr.h"
#include "ThrListen.h"
#include <direct.h>
#include <io.h>
#include <stdio.h>
#include <stdarg.h>
#include "LuaAPI.h"
#include <sys/timeb.h>
#include <chrono>
#include "Cdr.h"
#include "IConnection.h"
#include "ConferenceInterface.h"
#include "Class_TS_Manager.h"

Ivr::Ivr(ISwitch *p_Switch , void *param , const std::string & sBasePathLog , int iLogMaxFileSize) :    
    m_sBasePathLog(sBasePathLog) , m_iLogMaxFileSize(iLogMaxFileSize), m_stop(false)
{
    std::stringstream ss;
    TAPI_PARAM TapiParam;	
	//int Channel;

    ThreadParam = (_ThreadParam *)param;
    
    m_pSwitch = p_Switch;
    // Canal referente ao banco de dados
	Channel = ThreadParam->Port;
    FlagBlocked = false;

    ss << boost::format("Log_%03d") % Channel;
    m_pLog4cpp.reset(new Logger(m_sBasePathLog , m_iLogMaxFileSize,  ss.str(), Channel  , true ) );

    memset(Ani, 0x00, sizeof(Ani) );
    memset(Dnis, 0x00, sizeof(Dnis) );
    TxGlobalTimeSlot = -1;
    TxTimeSlot = 0;

    // Numero da placa em relacao ao banco de dados
    IdBoard = m_pSwitch->GetBoardId(Channel);
    IdTrunk = m_pSwitch->GetTrunkId(Channel);
    BoardPort = m_pSwitch->GetTrunkChannelID(Channel);	
	BoardTrunk = m_pSwitch->GetBoardTrunkID(Channel);

    //ConfigLog4cpp( m_sFullLogPathName );    
    
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    PrintIvrInfo();

    // Seta a estrutura de parametros para o construtor da classe TAPI
	TapiParam.Site								= m_pSwitch->GetSwitch();
	TapiParam.Machine							= m_pSwitch->GetMachine();

	TapiParam.iListIndex						= m_pSwitch->GetIndexScreen() + IdBoard;
	TapiParam.iBoardTrunk						= BoardTrunk;
	TapiParam.iBoardPort						= BoardPort;
		
	TapiParam.R2DnisMinimumSize			        = m_pSwitch->GetR2MinDnis();

	
    if ( !p_Switch->GetManufacturerName(Channel).compare("DIALOGIC") )
	{			
		TapiParam.Tecnologia					= DIALOGIC;
		TapiParam.Channel						= Channel;
	}	
    else if( !p_Switch->GetManufacturerName(Channel).compare("KHOMP") )
	{			
		TapiParam.Tecnologia					= KHOMP;
        int trunkID = p_Switch->GetTrunkId(Channel);

		if( !m_pSwitch->GetTrunkSignalingName(trunkID).compare("SIP") )
		{
			// Soma 30 para pular os 30 canais TDM e subtrai 1 para comecar os canais a partir de 0.
            TapiParam.Channel						= m_pSwitch->GetTrunkChannelID(Channel) + 30 - 1;
		}
		else
		{
			// Subtrai 1 para comecar os canais a partir de 0.
			TapiParam.Channel						= m_pSwitch->GetTrunkChannelID(Channel) - 1;
		}
	}

	//TapiParam.iCtiLogLevel					= LOG_CTI;
	//TapiParam.pLogInterface					= IVR->LogFile;

#ifdef _ASR
	TapiParam.bAsrEnabled						= Switch::DbChannels[Channel].AsrEnabled;
#else
	TapiParam.bAsrEnabled						= false;
#endif // _ASR

    /*
	TapiParam.ASR_GrammarPackage		=	Switch::StRegister.ASR_GrammarPackage;
	TapiParam.ASR_DatabaseType			=	Switch::StRegister.ASR_DatabaseType;
	TapiParam.ASR_DatabaseServer		=	Switch::StRegister.ASR_DatabaseServer;
	TapiParam.ASR_DatabaseName			=	Switch::StRegister.ASR_DatabaseName;
	TapiParam.ASR_DatabaseUser			=	Switch::StRegister.ASR_DatabaseUser;
	TapiParam.ASR_DatabasePassword	    =	Switch::StRegister.ASR_DatabasePassword;    
	TapiParam.bsTtsServerHost				=	Switch::StRegister.TTS_Server_Host;
	TapiParam.ASR_PollingTime				=	Switch::StRegister.ASR_PollingTime;
    */
    

	// CTI e um ponteiro para a classe base CTIBase que e membro da classe IVR,
	// ele e inicializado com um ponteiro da classe Tapi para acesso as funcoes
	// de telefonia.
	// Quando o ponteiro TAPI foi alocado para a classe Tapi ele inicializou o 
	// ponteiro Base com o objeto de telefonia desejado que passado para o seu construtor
	// Para desalocar o objeto usar delete com o Ponteiro para o objeto da Tapi
	// Ex: delete TAPI;
	// aloca memoria para ponteiro da classe tapi

    //Tapi *TAPI = new Tapi(m_pSwitch->GetBasePathLog(), &TapiParam);
    m_pTapi = new Tapi(m_pLog4cpp, &TapiParam);
    m_pTapi->InitSystem();
    m_pCti = m_pTapi->GetCTI();

    
    if ( OpenChannel() ) 
    {
        m_pLog4cpp->debug("[%p][%s] Erro ao abrir o CANAL %d", this, __FUNCTION__, Channel);
        // Erro ao abrir o canal
		// Libera a criacao da proxima thread pelo processo principal
		SetEvent(ThreadParam->EventCreate);
		//delete TAPI;
		//delete IVR;

		return;
    }

    // Tabela dos timeslots dos canais analogicos que fornecem audio    
    if ( m_pSwitch->GetTrunkSignaling(IdTrunk) == LOOP_START_TRAP)
    {
	    IdAudio = m_pSwitch->GetIdAudio();

        m_pSwitch->SetAudioTimeslots_Channel(IdAudio, Channel);
        m_pSwitch->SetAudioTimeslots_LocalTimeSlot(IdAudio, m_pCti->GetTimeSlot());
        m_pSwitch->SetAudioTimeslots_GlobalTimeSlot(IdAudio, -1);	    
    }

    memset(CallId,0x00,sizeof(CallId));
    
    Start();

    // Libera a criacao da proxima thread pelo processo principal
    SetEvent(ThreadParam->EventCreate);
    m_pLog4cpp->debug("[%p][%s] SET EventCreate ", this, __FUNCTION__);

        
}

Ivr::~Ivr(void)
{
    m_pTapi->EndSystem();
}

void Ivr::GenerateCallID()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    std::stringstream ss;
    
    tm * Time;
    _timeb TimeBuffer;

    _ftime( &TimeBuffer );
	Time = localtime( &TimeBuffer.time );

    memset(CallId,0x00,sizeof(CallId));
    ::sprintf(CallId , "%02d%02d%03d%04d%02d%02d%02d%02d%02d%03d" ,     m_pSwitch->GetSwitch(),
                                                                        m_pSwitch->GetMachine(),
                                                                        m_pCti->GetChannel(),
                                                                        (Time->tm_year + 1900),
                                                                        (Time->tm_mon + 1), 
                                                                        Time->tm_mday, 
                                                                        Time->tm_hour, 
                                                                        Time->tm_min, 
                                                                        Time->tm_sec, 
                                                                        TimeBuffer.millitm );
    
    

    ss << std::endl;
    ss << "*******************************************************************************" << std::endl;
    ss << "                                                                                  " << std::endl;
    ss << boost::format(" Call Id  : %48s \r\n") % CallId;
    ss << "                                                                               " << std::endl;
    ss << "*******************************************************************************" << std::endl;

    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__ , ss.str().c_str());
}

std::string Ivr::GetCallId()
{
    
    std::string sCallId;
    
    if ( CallId[0] == '\0')
    {
        GenerateCallID();
    }

    sCallId.assign(CallId);    
    
	return sCallId;
}


void Ivr::Start()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    m_ThrPtr.reset ( new boost::thread ( boost::bind (&Ivr::Thr, this)));    
    m_ThrPtr->detach();
}

void Ivr::Stop()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
    m_stop = true;    
    //m_ThrPtr.get()->join();
    m_ThrPtr->join();

    //m_pLog4cpp->debug("[%p][%s] Going to close channel.", this, __FUNCTION__);
    //CloseChannel();

    // Seta o estado da porta no banco de dados para nao inicializado
	//DbSetPortStatus(PORT_NOT_INIT, Channel);
    
    m_pLog4cpp->debug("[%p][%s] Ivr for Channel %d STOPPED.", this, __FUNCTION__, Channel);
}

void Ivr::Thr()
{
    int evento, iRet;

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try 
    {
		// Libera o canal para atendimento
		InitPortToAnswer();
        
        iRet = setjmp(jmp_cti);

		if( iRet == 0 ){
			m_pCti->SetJump(&jmp_cti);
		}

		if( iRet == JUMP_DISCONNECT ){
			LongJmpDisconnect();
		}
		
		if( iRet == JUMP_EXIT ){
			Log(LOG_CTI, "JUMP_EXIT");
			return;
		}

		if( iRet == JUMP_BLOCKED ){
			Log(LOG_CTI, "JUMP_BLOCKED");
			LongJmpBlocked();
		}

        m_pLog4cpp->debug("[%p][%s] Loop waiting for events.", this, __FUNCTION__);
        
        while(!m_stop)
        {
            iRet = setjmp(jmp_state);
            

            if(iRet == 2)
            {
				// Vai para o estado setado em EstadoIVR
 				(this->*FuncState[EstadoIVR])();
			}

            if(iRet == 3)
            {
                m_pLog4cpp->debug("[%p][%s] Going to terminate this thread.", this, __FUNCTION__);
				// Termina a thread                
				return;
			}

            if(iRet == 4)
            {
                m_pLog4cpp->debug("[%p][%s] Going to process Script Return", this, __FUNCTION__);
				// Processa o valor de retorno do script
				ProcessScriptReturn();
			}

            boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
            evento = m_pCti->WaitEvent();
            
			switch(evento)
            {
                
				case T_PLAYTONE: // Evento de termino do objeto
                    m_pLog4cpp->debug("[%p][%s] Event: T_PLAYTONE", this, __FUNCTION__);                    
                    Log(LOG_CTI, "Event: T_PLAYTONE");
				break;

				case T_DISCONNECT: // Usuario desligou
                    m_pLog4cpp->debug("[%p][%s] Event: T_DISCONNECT", this, __FUNCTION__);                    
					T_Disconnect();
                    
                    Log(LOG_CTI, "Event: T_DISCONNECT");
				break;
				
				case T_DROPCALL: // Desligamento completo
                    m_pLog4cpp->debug("[%p][%s] Event: T_DROPCALL", this, __FUNCTION__);
                    T_DropCall();                    
                    
                    Log(LOG_CTI, "Event: T_DROPCALL");
				break;
				
				case T_IDLE:	// Canal ficou livre
                    m_pLog4cpp->debug("[%p][%s] Event: T_IDLE", this, __FUNCTION__);
 					T_Idle();
                    Log(LOG_CTI, "Event: T_IDLE");
				break;
			    
				case T_BLOCKED:	 // Entrou no bloqueio
                    m_pLog4cpp->debug("[%p][%s] Event: T_BLOCKED", this, __FUNCTION__);
					T_Blocked();
                    Log(LOG_CTI, "Event: T_BLOCKED");
				break;
                
				case T_UNBLOCKED:	 // Entrou no desbloqueio
                    m_pLog4cpp->debug("[%p][%s] Event: T_UNBLOCKED", this, __FUNCTION__);
					//T_Unblocked(); // does nothing !
                    
                    Log(LOG_CTI, "Event: T_UNBLOCKED");
				break;
				
				case T_CALL: // Chegou uma ligacao
                    m_pLog4cpp->debug(LOG_LINE);
                    m_pLog4cpp->debug("[%p][%s] Event: T_CALL", this, __FUNCTION__);

                    Log(LOG_CTI, "Event: T_CALL");
                    T_Call();                    
				break;
				
				case T_CONNECT: // Ligacao conectada (SAIDA)
                    m_pLog4cpp->debug("[%p][%s] Event: T_CONNECT", this, __FUNCTION__);
					T_Connect();                    
                    Log(LOG_CTI, "Event: T_CONNECT");
				break;

				case T_USER_BLOCK: // Evento para bloquear o canal
                    m_pLog4cpp->debug("[%p][%s] Event: T_USER_BLOCK", this, __FUNCTION__);
					Block();
                    Log(LOG_CTI, "Event: T_USER_BLOCK");
				break;
				
				case T_USER_UNBLOCK: // Evento para desbloquear o canal
                    m_pLog4cpp->debug("[%p][%s] Event: T_USER_UNBLOCK", this, __FUNCTION__);					
					UnBlock();
                    Log(LOG_CTI, "Event: T_USER_UNBLOCK");
 				break;
				
				case T_SW_DIAL_EX:
					// Evento para o canal discar sob comando de um script
					// sendo executado em outro canal
                    m_pLog4cpp->debug("[%p][%s] Event: T_SW_DIAL_EX", this, __FUNCTION__);					
//					DialOutEx();
                    Log(LOG_CTI, "Event: T_SW_DIAL_EX");
 				break;

				case T_SW_DIAL_TELNET:
					// Evento enviado por telnet para discar
                    m_pLog4cpp->debug("[%p][%s] Event: T_SW_DIAL_TELNET", this, __FUNCTION__);					
					DialFromTelnet();
                    Log(LOG_CTI, "Event: T_SW_DIAL_TELNET");
				break;

				case T_SW_DIAL_APPL:
					// Evento enviado pela aplicação para discar
                    m_pLog4cpp->debug("[%p][%s] Event: T_SW_DIAL_APPL", this, __FUNCTION__);					
//					DialFromAppl();
                    Log(LOG_CTI, "Event: T_SW_DIAL_APPL");
				break;

                case T_SW_ALLOCATE_CHANNEL:
					// Evento enviado pela aplicação para discar
                    m_pLog4cpp->debug("[%p][%s] Event: T_SW_ALLOCATE_CHANNEL", this, __FUNCTION__);					
//					AllocateChannel();
                    Log(LOG_CTI, "Event: T_SW_ALLOCATE_CHANNEL");
				break;

                case T_SW_AUDIO_REQUEST:
                    m_pLog4cpp->debug("[%p][%s] Event: T_SW_AUDIO_REQUEST", this, __FUNCTION__);					
//					T_SW_Audio_Request( evento );
                    Log( LOG_CTI, "Event: T_SW_AUDIO_REQUEST" );
				break;
			}
            
        }
        
    }
    catch(...)
	{
        m_pLog4cpp->error("EXCEPTION: Ivr::Thr()");
		
	}    
    
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::LongJmpDisconnect(void)
{
	try {

	    m_pCti->Disconnect();
		
		Print(IVR_STATUS, STR_IDLE);
		SetIconStatus(ICON_IDLEIN);
		
		GoEvents();
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::LongJmpDisconnect()");
		GoEvents();
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::PrintIvrInfo()
{    
    
    m_pLog4cpp->debug("\tIdBoard : %d", IdBoard );
    m_pLog4cpp->debug("\tIdTrunk : %d", IdTrunk );
    m_pLog4cpp->debug("\tBoardPort : %d", BoardPort );
    m_pLog4cpp->debug("\tBoardTrunk : %d", BoardTrunk );
}

CTIBase * Ivr::GetCTI() const
{
    return m_pCti;
}

void Ivr::StopLog()
{
    m_pLog4cpp->StopLog();
}

//------------------------------------------------------------------------------------------------------------------------

int Ivr::OpenChannel()
{
	try 
    {

        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
		
        int trunkID = m_pSwitch->GetTrunkId(Channel);

        m_pLog4cpp->debug("[%p][%s] TrunkSignaling(trunkID) = %d", this, __FUNCTION__ , m_pSwitch->GetTrunkSignaling(trunkID));
        m_pLog4cpp->debug("[%p][%s] TrunkSignaling(IdTrunk) = %d", this, __FUNCTION__ , m_pSwitch->GetTrunkSignaling(IdTrunk));
        

		switch(m_pSwitch->GetTrunkSignaling(trunkID))
		{
			case LINE_SIDE: // Protocolo Lucent Definity
				m_pLog4cpp->debug("LINE_SIDE (Lucent Definity)");
				if(m_pCti->OpenChannel(E1, LINE_SIDE))
				{
                    m_pLog4cpp->error(m_pCti->GetLastError());
					return -1;
				}
			break;

			case R2: // Protocolo R2 Digital
				m_pLog4cpp->debug("R2");
				if(m_pCti->OpenChannel(E1, R2))
				{
					m_pLog4cpp->error(m_pCti->GetLastError());
					return -1;
				}
			break;

			case R2_DIGITAL: // Novo Protocolo R2 Digital
				m_pLog4cpp->debug("R2_DIGITAL");
				if(m_pCti->OpenChannel(E1, R2_DIGITAL))
				{
					m_pLog4cpp->error(m_pCti->GetLastError());
					return -1;
				}
			break;

			case GLOBAL_CALL: // API GlobalCall
			case GLOBAL_CALL_DM3: //API Global Call DM3
			case GLOBAL_CALL_SS7: // API GlobalCall SS7
			case GLOBAL_CALL_IP: //API Global Call IP
			//case KHOMP_SIP:
				
				if(m_pSwitch->GetTrunkSignaling(IdTrunk) == (int)GLOBAL_CALL)
					m_pLog4cpp->debug("GLOBAL_CALL");
				else if(m_pSwitch->GetTrunkSignaling(IdTrunk) == (int)GLOBAL_CALL_DM3)
					m_pLog4cpp->debug("GLOBAL_CALL_DM3");
				else if(m_pSwitch->GetTrunkSignaling(IdTrunk) == (int)GLOBAL_CALL_SS7)
					m_pLog4cpp->debug("GLOBAL_CALL_SS7");
				else if(m_pSwitch->GetTrunkSignaling(IdTrunk) == (int)GLOBAL_CALL_IP)
					m_pLog4cpp->debug("GLOBAL_CALL_IP");
				//else if(m_pSwitch->GetTrunkSignaling(IdTrunk) == KHOMP_SIP)
				//	m_pLog4cpp->debug("KHOMP_SIP");
				
				if(m_pCti->OpenChannel(m_pSwitch->GetTrunkType(IdTrunk) , m_pSwitch->GetTrunkSignaling(IdTrunk) , m_pSwitch->GetTrunkProtocol(IdTrunk).c_str() ))
				{
					m_pLog4cpp->debug( m_pCti->GetLastError());
					return -1;
				}

				if( m_pSwitch->GetIPResource(IdBoard) )
				{
					if(m_pCti->OpenChannelIP(false, m_pSwitch->GetIPProtocol(IdBoard).c_str() ) )
					{
						m_pLog4cpp->debug(m_pCti->GetLastError());
						return -1;
					}
				}
			break;
					 
			case ISDN: // Protocolo ISDN back to back
				m_pLog4cpp->debug("ISDN");
				if(m_pCti->OpenChannel(E1, ISDN))
				{
					m_pLog4cpp->error( m_pCti->GetLastError());
					return -1;
				}
			break;


			case LOOP_START: // Protocolo Analogico (loop start interface)

			case LOOP_START_TRAP:
				m_pLog4cpp->debug("LOOP_START");
				if( m_pCti->OpenChannel(LSI, m_pSwitch->GetTrunkSignaling(IdTrunk) ) )
				{
					m_pLog4cpp->error(m_pCti->GetLastError());
					return -1;
				}
			break;

			case RESOURCE:
				// Protocolo p/ abrir apenas o recurso de voz
				m_pLog4cpp->debug("SIG_RESOURCE" );
				if( m_pCti->OpenChannel( VOX, RESOURCE ) )
				{
					m_pLog4cpp->error(m_pCti->GetLastError() );
					return -1;
				}
			break;

			default: // Protocolo Invalido
                m_pLog4cpp->error("Invalid Protocol %d: see table TCtiPort", m_pSwitch->GetTrunkSignaling(IdTrunk) );
				return -1;
			break;
		}

#ifdef _VXML
		VXMLBrowser = new VXMLbrowser(CTI);
		VXMLBrowser->SetParam("VXML_Url_Ssml_Server", Switch::StRegister.VXML_Url_Ssml_Server);
		VXMLBrowser->Initiate();
#endif	// _VXML

		return 0;
	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::OpenChannel()");
		return -1;
	}
}

void Ivr::Print(int Col, const char *sformat, ...)
{
    va_list vaPrint;
    char szMsg[10000];

    int l_IndexScreen;

	try {
	    l_IndexScreen = m_pSwitch->GetIndexScreen();

        memset(szMsg , 0x00, sizeof(szMsg) );
                
		va_start( vaPrint, sformat );		
        vsnprintf( szMsg, 9001, sformat, vaPrint );
		va_end( vaPrint );
        //szMsg[9000] = 0;

        if ( strlen(szMsg) > 0 )
        {
            m_pSwitch->GetThrListen()->SendStringGrid( (l_IndexScreen + IdBoard), BoardTrunk, BoardPort, Col, szMsg);		
            m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, szMsg);
        }
	}
	catch(...)
    {
		m_pLog4cpp->debug("EXCEPTION: Ivr::PrintState()");
	}
}

void Ivr::Log(int iLevel, const char *sformat, ...)
{   
    va_list vaLog;
    tm * Time;
    _timeb TimeBuffer;
    int len = 0;
    char szMessage[MAX_STRING_LENGTH_LUA_LOG + 100];    
    char szMsg[MAX_STRING_LENGTH_LUA_LOG];

    try
    {
        memset(szMessage,0x00,sizeof(szMessage) );
        memset(szMsg , 0x00 , sizeof(szMsg) );

        _ftime( &TimeBuffer );
	    Time = localtime( &TimeBuffer.time );


	    va_start( vaLog, sformat );        
        vsprintf( szMsg, sformat, vaLog );        
	    va_end( vaLog );

        // Substitui enventuais caracteres | (pipe) da mensagem por #
	    for ( int i = 0; i < strlen( szMsg ); i++ )
		    if ( szMsg[i] == '|' )
			    szMsg[i] = '#';

    
        //len = sprintf( szMessage,"%04d-%02d-%02d %02d:%02d:%02d.%03d", (Time->tm_year + 1900), (Time->tm_mon + 1), Time->tm_mday, Time->tm_hour, Time->tm_min, Time->tm_sec, TimeBuffer.millitm );
        len = ::sprintf( szMessage, "|---|GTW|               |%s", szMsg  );    

        m_pSwitch->GetThrListen()->LogScreen( m_pSwitch->GetIndexScreen() + IdBoard , BoardPort , szMessage );
        m_pLog4cpp->debug( szMessage );        

    }
	catch(...)
    {
		m_pLog4cpp->debug("EXCEPTION: Ivr::Log()");
	}
}


void Ivr::InitPortToAnswer()
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try {

		// Seta para 1 segundo o intervalo de pegar eventos
		m_pCti->SetTimeToWaitEvent(1000);
		// registra objeto para alocacao por outra thread
		m_pCti->RegObj();
		// Guarda o Timeslot de transmissao
		TxTimeSlot = m_pCti->GetTimeSlot();
    // Guarda o Timeslot de Vox
		TxTimeSlotVox = m_pCti->GetTimeSlotVox();


        m_pLog4cpp->debug("[%p][%s] Waiting to start...", this, __FUNCTION__);
		m_pCti->InitChannel();
        m_pLog4cpp->debug("[%p][%s] Started.", this, __FUNCTION__);		

		// Se a porta for de recurso manda outro status
		//if( Switch::DbChannels[Channel].Reserved )
        if (m_pSwitch->GetChannelsReserverd(Channel) )
		{
            
			Print( IVR_STATUS, STR_RESERVED );
			Print( IVR_CALL_TYPE,	"" );
			Print( IVR_DNIS,			"" );
			Print( IVR_ANI,				"" );
			Print( IVR_SERVICE,		"" );
			SetIconStatus( ICON_RESERVED );
            
		}
	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::InitPortToAnswer()");	
        GoEvents();
	}
}

void Ivr::T_Disconnect()
{
	try {

        m_pLog4cpp->debug("[%p][%s] Channel %d", this , __FUNCTION__, Channel);

		if( FlagBlocked )
		{
			// Se o canal esta bloqueado ignora os eventos.
            Log( LOG_FUNCTIONS, "Channel blocked. Event ignored." );			
			return;
		}

		if(FlagHangup)
        {
            Log(LOG_FUNCTIONS, "T_Disconnect(): Call already disconnected");			
			return;
		}

		Log(LOG_FUNCTIONS, "Ivr::T_Disconnect()");

		Print(IVR_STATUS, STR_HANGUP);
		SetIconStatus(ICON_HANGUP);

		Hangup();
	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::OnDisconnect()");	
        GoEvents();
	}
}

void Ivr::GetDatetime(char *DateTime)
{
    boost::posix_time::ptime timeLocal = boost::posix_time::microsec_clock::local_time();
	long milliseconds = timeLocal.time_of_day().total_milliseconds();
	boost::posix_time::time_duration current_time_milliseconds = boost::posix_time::milliseconds(milliseconds);
	boost::posix_time::ptime current_date_milliseconds(timeLocal.date(), current_time_milliseconds);

    sprintf(DateTime, "%04d-%02d-%02d %02d:%02d:%02d.%03d",
									current_date_milliseconds.date().year(), 
                                    current_date_milliseconds.date().month().as_number(), 
                                    current_date_milliseconds.date().day(), 
                                    current_date_milliseconds.time_of_day().hours(), 
                                    current_date_milliseconds.time_of_day().minutes(), 
                                    milliseconds);
}

int Ivr::DbSetPortStatus(int Status, int Port)
{
    m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);

	try
	{
		
        if ( m_pSwitch->GetChannelsReserverd(Port) )
		{
			if( m_pSwitch->GetChannelsStatus(Port) != PORT_INACTIVE )
			{   
                m_pLog4cpp->debug("[%p][%s] SetChannelsStatus PORT_RESERVED", this , __FUNCTION__);
                m_pSwitch->SetChannelsStatus(Port,PORT_RESERVED);
			}
		}
		else if( m_pSwitch->GetChannelsStatus(Port) != PORT_INACTIVE )
		{   
            m_pSwitch->SetChannelsStatus(Port , Status );
		}

        m_pLog4cpp->debug("[%p][%s] SetChannelStatus Port = %d , Status = '%c'", this , __FUNCTION__, Port , m_pSwitch->GetChannelsStatus(Port));            

		return 0;
	}
	catch(...)
	{
        m_pLog4cpp->error("EXCEPTION: Ivr::DbSetPortStatus()");
		return -1;
	}
}

int Ivr::PlayBusyToneAsync()
{
    m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);
	char sNomeArq[256];

	try {

        m_pLog4cpp->debug("Ivr::PlayBusyToneAsync()");
		sprintf(sNomeArq, "%s\\busy.wav", m_pSwitch->GetPathPrompts().c_str());

		if(m_pCti->PlayFileAsync(sNomeArq)){
			m_pLog4cpp->error(m_pCti->GetLastError());
			return -1;
		}
		return 0;
	}
	catch(...){
		m_pLog4cpp->error("EXCEPTION: Ivr::PlayBusyToneAsync()");
		return -1;
	}
}

void Ivr::T_Call()
{
	int iNumDnis;
	char ScriptName[_MAX_PATH+1];
	std::string sServiceName;

	try {
        //ConfigLog4cpp( m_sFullLogPathName );

        m_pLog4cpp->debug("=================================================================");
        m_pLog4cpp->debug(" | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | ");
        m_pLog4cpp->debug(" v v v v v v v v v v v v v v v v v v v v v v v v v v v v v v v v ");
        m_pLog4cpp->debug("[%p][%s] Channel %d", this , __FUNCTION__, Channel);

		// Loga a cada chamada a versao do VoiceGateway.
		//Log( LOG_INFO, "Version: %s", Win::ApplicationName.c_str() );        
		

		if( FlagBlocked )
		{
			// Se o canal esta bloqueado ignora os eventos.
			m_pLog4cpp->debug("Channel blocked. Event ignored." );
			return;
		}

		// Inicializa as variaveis para uma ligacao de entrada normal
		InitInboundCall();
		// Guarda o Timeslot de transmissao
		TxTimeSlot = m_pCti->GetTimeSlot();

		// O Script sempre atende a chamada e loga os Cdrs.
		ScriptAnswer = true;
		bDoubleAnswer = true;
        
		switch ( m_pSwitch->GetTrunkSignaling(IdTrunk) )
        {

			case GLOBAL_CALL_IP:
                m_pLog4cpp->debug("[%p][%s] Channel %d - TrunkSignaling[%d] : GLOBAL_CALL_IP.", this , __FUNCTION__, Channel, IdTrunk);
				Category = 1;
				// Numero de B (IP DO CHAMADOR)
				strcpy(Dnis, m_pCti->GetDnis());
				// Numero de A (IP LOCAL + INFO)
				strcpy(Ani, m_pCti->GetAni());
			break;		

			case LOOP_START:
                m_pLog4cpp->debug("[%p][%s] Channel %d - TrunkSignaling[%d] : LOOP_START.", this , __FUNCTION__, Channel, IdTrunk);
				// Esses tipos de sinalisacao nao possuem identificacao do chamador
				// nem numero de destino chamado
				strcpy(Ani,		"0");
				strcpy(Dnis,	"0");
				Category = 1;
			break;		

			case LINE_SIDE:
				// Esses tipos de sinalisacao nao possuem identificacao do chamador
				// nem numero de destino chamado
				strcpy(Ani,		"0");
				strcpy(Dnis,	"0");
				strcpy(ServiceName,	"IVR");
				Category = 1;
			break;		

			case R2_DIGITAL:
                m_pLog4cpp->debug("[%p][%s] Channel %d - TrunkSignaling[%d] : R2_DIGITAL.", this , __FUNCTION__, Channel, IdTrunk);
				iNumDnis = 0;
                
                
				if( m_pSwitch->GetR2MinDnis() < m_pSwitch->GetR2MaxDnis() )
                {
					// Numero de B
					strcpy(Dnis, m_pCti->GetDnis());
					// Se o minimo numero de DNIS for menor que o maximo, procura
					// no arquivo Dnis.ini esse servico, para evitar de ir ao banco de dados
					ReadDnisConfig();

					if( MapDnis[Dnis] )	// O servico existe, nao pega mais Dnis, apenas completa a sinalizacao
						iNumDnis = 0;
					else	// Nao existe esse servico com numero de algarismos menor no config
						iNumDnis = m_pSwitch->GetR2MaxDnis() - m_pSwitch->GetR2MinDnis();
				}

				if( m_pCti->CallAck(iNumDnis) ){
					// Erro a chamada ja foi desconectada
					Print(IVR_STATUS, STR_IDLE);
					SetIconStatus(ICON_IDLEIN);
                    m_pLog4cpp->error("Ivr::T_Call() called Switch::DbReadService() with Error");
					return;
				}
				
				// Numero de B
				strcpy(Dnis, m_pCti->GetDnis());
				// Numero de A
				strcpy(Ani, m_pCti->GetAni());
				// Categoria do Chamador
				Category = (unsigned char) m_pCti->GetCallInfo()[0];
			break;		

			default:
                
                m_pLog4cpp->debug("[%p][%s] Channel %d - TrunkSignaling[%d] : default.", this , __FUNCTION__, Channel, IdTrunk);
				if( !m_pSwitch->GetTrunkProtocol(IdTrunk).compare("pdk_sw_e1_luls_io" ) )
				{
					strcpy(Ani,		"0");
					strcpy(Dnis,	"0");
					strcpy(ServiceName,	"IVR");
					Category = 1;
				}
				else
				{
					// Numero de B
					strcpy(Dnis, m_pCti->GetDnis());
					// Numero de A
					strcpy(Ani, m_pCti->GetAni());
					strcpy(ServiceName,	"IVR");
					// Categoria do Chamador
					Category = (unsigned char) m_pCti->GetCallInfo()[0];
				}

			break;		
		}

		m_pLog4cpp->debug("[%p][%s] Channel : %d - DoubleAnswer: %d  ScriptAnswer: %d", this, __FUNCTION__, Channel , ((bDoubleAnswer) ? (int) 1 : (int) 0), ((ScriptAnswer) ? (int) 1 : (int) 0));

		if( !ScriptAnswer )
		{            
			if( AnswerCall(bDoubleAnswer, true) )
			{
				GoEvents();
			}
		}

		// Zera a variavel do script que contem informacoes passadas na transferencia
		//string sInfo = SCRIPT_INFO_VAR_NAME;
		//ClearStaticVar(sInfo); ?


		// Se existir o diretório ...scripts\IVR, executa o script IVR.LUA. Senão
		// executa o script 'dnis'.lua, do diretório 'dnis'.

		::sprintf(ScriptName, "%s\\IVR",  m_pSwitch->GetPathScripts().c_str());
        boost::filesystem::path fsIvrPath ( ScriptName );
        if ( !boost::filesystem::exists( fsIvrPath ) ) //o diretório ../IVR não existe		
		{
			::sprintf(ScriptName, "%s\\%s\\%s.LUA", m_pSwitch->GetPathScripts().c_str() , Dnis, Dnis);
			strcpy(ServiceName,	Dnis);
		}
		else
		{
			::sprintf(ScriptName, "%s\\IVR\\IVR.LUA", m_pSwitch->GetPathScripts().c_str() );
			strcpy(ServiceName,	"IVR");
		}

		ExecLuaScript(ScriptName);


	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::T_Call()");
		GoEvents();
	}
}

ISwitch * Ivr::GetSwitch()
{
    return m_pSwitch;
}


//boost::shared_ptr<Tapi> Ivr::GetTapi()
Tapi * Ivr::GetTapi()
{
    return m_pTapi;
}

void Ivr::EndScript()
{
	try
	{
		//lua_pushstring( LuaState, "ENDSCRIPT" );
		//lua_error( LuaState ,"EndScript error.");

		//longjmp(jmp_state, 4);
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::EndScript()");
	}
}

void Ivr::ExecLuaScript(const char *ScriptName)
{
    int ret = 0;
    std::stringstream ss;
    std::string scriptPath;

    try
    {
        if( _access(ScriptName, 0) )
        {
            m_pLog4cpp->debug("[%p][%s] - Cannot open the Script %s", this , __FUNCTION__, ScriptName);
			
			// Desliga a ligacao
			Hangup();
			Print(IVR_STATUS, STR_IDLE);
			SetIconStatus(ICON_IDLEIN);
			GoEvents();
		}

        m_pLog4cpp->debug("[%p][%s] Channel %d", this , __FUNCTION__, Channel);

        scriptPath = m_pSwitch->GetPathScripts();

        ss << boost::format("%s") %  ScriptName;

        
        try
        {
            m_pLog4cpp->debug("[%p][%s] Channel %d / Going to execute LUA script %s.", this , __FUNCTION__, Channel , ss.str().c_str());
            ret = lua_ExecFile( this , ss.str().c_str() );        
        }
        catch(...)
        {
            m_pLog4cpp->error("EXCEPTION: Error executing script LUA %s." , scriptPath.c_str() );
            GoEvents();
        }
        

        // Desliga a ligacao
		Hangup();
		Print(IVR_STATUS, STR_IDLE);
		SetIconStatus(ICON_IDLEIN);
		GoEvents();
    }
    catch(...)
    {
        Log(LOG_ERROR, "EXCEPTION: Ivr::ExecLuaScript()");
		//CloseScript();
    }

}

void Ivr::InitInboundCall()
{
	try {
		m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);

		FlagAnswer			= false;
		FlagHangup			= false;
		FlagRecordUser	    = false;
		FlagTranfer			= false;

		ClearConn(Connection);
		ClearConn(Connection_local);
		ClearConn(Connection_remote);
		ClearConn(Connection_PA);
		ClearConn(Connection_OUT);
		ClearConn(Connection_IN);

		ClearCdrDetail();

		// 1 = usuario desligado pelo script
		// 2 = usuario desligado pela posicao de atendimento
		// 3 = usuario desligou
		HangupReason = 0;
		// Guarda a data de inicio da ligacao
		GetDatetime(sIniDateOfCall);
		// Guarda o tempo em segundos de inicio da ligacao
		TimeOfCall = time(NULL);
		// Limpa a fila de eventos de conexoes
        m_pSwitch->GetConnection()->Connection_Clear(Channel);

		// Seta o offset para o arquivo de musica de espera para o inicio do arquivo
		FilePosToPlayHoldMusic = 0;
		// 1 = Ligacao Interna, 2 = Ligacao externa
		IdCallFrom = 1;
		// Guarda o status do DialOut			
		iDialResult = 0;
		// Ligacao de entrada
		iCallDirection = INBOUND;
		// Seta para pegar todos os digitos
		m_pCti->SetDigMask("@");

		// Seta status da tela
		Print(IVR_STATUS, STR_OFFERED);
		SetIconStatus(ICON_OFFERED);

	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::InitInboundCall()");
		//GoEvents();
	}
}

void Ivr::ClearCdrDetail()
{
	try {

		for(int i = 0; !(QueueCdrDetail.empty()); i++){
			QueueCdrDetail.pop();
		}
	}
	catch(...){
		m_pLog4cpp->error("EXCEPTION: Ivr::ClearCdrDetail()");
	}
}

void Ivr::ClearConn(ST_CONNECTION &st_connection)
{
	try {

		st_connection.Machine		=	0;
		st_connection.Switch		=	0;
		st_connection.Channel		=	0;
		st_connection.Event			=	0;
		st_connection.Timeslot	=	0;
		st_connection.sData			= "";
	}
	catch(...)
    {
		m_pLog4cpp->error("EXCEPTION: Ivr::ClearConn()");
	}
}

void Ivr::ReadDnisConfig()
{
	try 
    {

		char path_dir[_MAX_PATH+1];
		char sIniArq[_MAX_PATH+1];
		char sValue[128];
		char sKey[128];
		int iRet;

		_getdcwd( _getdrive(), path_dir, _MAX_PATH );   

		sprintf(sIniArq, "%s\\DNIS.INI",path_dir);

		if( !CheckFileDate(sIniArq, lCreateTimeDnisConfig) )
			return;

		if( _access(sIniArq, 0) ){
			m_pLog4cpp->error("File: %s not found", sIniArq);
			return;
		}

		MapDnis.clear();

		for(int i = 1; ;i++)
        {

			sprintf(sKey, "%d", i);

			iRet = GetPrivateProfileString(_T("Dnis"), LPWSTR(sKey), _T(""), LPWSTR(sValue), 32, LPCTSTR(sIniArq) );

			if( iRet <= 0 ){
				break;
			}

			MapDnis[sValue] = true;
		}
	}
	catch(...){
		m_pLog4cpp->error("EXCEPTION: Ivr::ReadDnisConfig()");
	}
}

int Ivr::AnswerCall(bool bDoubleAnswer, bool bBilling)
{
	try 
    {        
        m_pLog4cpp->debug("[%p][%s] ", this , __FUNCTION__);
		int rc;

		if( FlagAnswer )
		{
            m_pLog4cpp->debug("AnswerCall(): Call already answered" );
			return -1;
		}

        
        if ( m_pSwitch->GetLogCDR() )
        {            
            DbReadCallId();
        }

        GenerateCallID();        

		m_pCti->SetCallId( CallId );

		// Garante a interrupcao de prompts q estivessem tocando.
		m_pCti->StopCh();

		// Se o atendimento nao sera feito pelo script atende a chamada normalmemte
		// Faz duplo atendimento.
		rc = m_pCti->Answer(bDoubleAnswer, bBilling);
		if( rc == T_DISCONNECT )
		{
			m_pLog4cpp->debug("Call disconnected" );
			return T_DISCONNECT;
		}
		else if( rc != T_ANSWER )
		{
			m_pLog4cpp->error("Error in AnswerCall(). %s", m_pCti->GetLastError() );
			return -1;
		}

		// Seta o status da porta no banco de dados para atendendo uma ligacao
		DbSetPortStatus(PORT_ANSWER_CALL, Channel);

		SetIconStatus(ICON_ANSWERED);

		Print(IVR_STATUS,			STR_ANSWERED);
		Print(IVR_CALL_TYPE,	    "IN");
		Print(IVR_DNIS,				Dnis);
		Print(IVR_ANI,				Ani);
		Print(IVR_SERVICE,		ServiceName);

		FlagAnswer = true;

		// Guarda o timedate do atendimento da chamada.
		ChannelStat::Instance().SetLastActivityTime( Channel, time(NULL) );
		// Contabiliza o numero de chamadas atendidas.
		ChannelStat::Instance().IncrementCallsAnsweredCount( Channel );
		// Contabiliza o numero de canais ocupados.
		ChannelStat::Instance().IncrementBusyChannelCount();


		return 0;

	}
	catch(...){
        m_pLog4cpp->error("EXCEPTION: Ivr::AnswerCall()");
		return -1;
	}
}


int Ivr::DbReadCallId()
{
    std::stringstream ss;
    char sQuery[256];
    std::string sCallId;

    m_pLog4cpp->debug("[%p][%s] ", this , __FUNCTION__);

	try
	{
		::sprintf(sQuery, "EXEC Sp_TSysGetCallId");

		m_pLog4cpp->debug("Query: %s", sQuery);

        sCallId = m_pSwitch->DbGetCallId(sQuery);

        ss << "*******************************************************************************" << std::endl;
        ss << "                                                                                  " << std::endl;
        ss << boost::format(" Call id READ FROM DATABASE : %30s \r\n") % sCallId.c_str();
        ss << "                                                                               " << std::endl;
        ss << "*******************************************************************************" << std::endl;

        m_pLog4cpp->debug("[%p][%s] %s", this , __FUNCTION__, ss.str().c_str());

        strcpy(CallId, sCallId.c_str() );;

		return 0;
	}
	catch(...)
	{		
		m_pLog4cpp->error( "EXCEPTION: Switch::DbReadCallId()");
		return -1;
	}
}

bool Ivr::CheckFileDate(char *sNomeArq, long &lTimeLastChange)
{
	WIN32_FIND_DATA find_data;
	SYSTEMTIME system_time;
	SYSTEMTIME system_time_aux;
	TIME_ZONE_INFORMATION time_zone_information;
	long lTime;
	struct tm tm;
	
	HANDLE h_file = INVALID_HANDLE_VALUE;

	try {

         wchar_t wsNomeArq[20];
         mbstowcs(wsNomeArq, sNomeArq, strlen(sNomeArq)+1);//Plus null
         LPWSTR ptrwsNomeArq = wsNomeArq;
		h_file = FindFirstFile(ptrwsNomeArq, &find_data);

		if(h_file == INVALID_HANDLE_VALUE){
			FindClose(h_file );
			return false;
		}

		FindClose(h_file );


		// converte time do arquivo para time do sistema
		FileTimeToSystemTime(&find_data.ftLastWriteTime, &system_time_aux);
		// obtem informacao do time local
		GetTimeZoneInformation(&time_zone_information);
		// converte time do sistema para time local
		SystemTimeToTzSpecificLocalTime(&time_zone_information, &system_time_aux, &system_time);

		tm.tm_year	=	system_time.wYear - 1900;
		tm.tm_mon		=	system_time.wMonth -	1;
		tm.tm_mday	=	system_time.wDay;
		tm.tm_hour	=	system_time.wHour;
		tm.tm_min		=	system_time.wMinute;
		tm.tm_sec		=	system_time.wSecond;
		tm.tm_isdst	=	-1;

		lTime				=	mktime(&tm);

		if( lTime != lTimeLastChange ){
			lTimeLastChange = lTime;
			return true;
		}

		return false;
	}
	catch(...){
        m_pLog4cpp->error("EXCEPTION: CheckFileDate()");
		return true;
	}	
}


//------------------------------------------------------------------------------------------------------------------------

// Essa funcao eh executada no canal que requisita a chamada (ENTRADA)
void Ivr::HangupEx(void)
{
	try {

		// Conecta a interface de linha com o proprio canal de voz
		m_pCti->ScConnect(FULLDUP);

		Connection = Connection_OUT;

		// Envia evento para o canal de destino discar
		Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_HANGUP");
		SendConnection(T_SW_DIAL_EX_HANGUP);
	}			
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::HangupEx()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::SetScriptHangupFlag(bool value)
{
	FlagScriptHangup = value;
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::SetSSConferece(bool bFlag)
{
    bSSConference = bFlag;
}

//------------------------------------------------------------------------------------------------------------------------

bool Ivr::GetSSConferece(void) const
{
    return bSSConference;
}

//------------------------------------------------------------------------------------------------------------------------

// Essa funcao eh executada no canal que requisita a chamada (ENTRADA)
int Ivr::DialEx(const char *sDestinationNumber, const char *sOriginationNumber, const char *sTimeoutDial)
{
	try {

		int Switch, Machine, Port;
		string sDnis, sAni;

		Log(LOG_FUNCTIONS, "Ivr::DialEx()");

		sDnis = sDestinationNumber;
		sAni = sOriginationNumber;

		if( DbGetDialPort(0, Switch, Machine, Port,  sDnis, sAni) )
		{
			Log(LOG_FUNCTIONS, "There is no Ports available.");
			return -1;
		}

		Connection.Switch		= Switch;
		Connection.Machine	= Machine;
		Connection.Channel	= Port;

		Connection.sData		= "DNIS=";
		Connection.sData		+= sDnis;
		Connection.sData		+= "&ANI=";
		Connection.sData		+= sAni;

		Connection.sData		+= "&TIME_OUT=";
		Connection.sData		+= sTimeoutDial;
		
		// Passa o CallID da chamada de entrada para a chamada de saida
		Connection.sData		+= "&INBOUND_ID=";
		Connection.sData		+= CallId;

		// Envia evento para o canal de destino discar
		Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX");
		SendConnection(T_SW_DIAL_EX, Connection.sData);
		
		return 0;
	}			
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::DialEx()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Essa funcao eh executada no canal que requisitou a chamada para pegar
// status da chamada de A e C (ENTRADA)
int Ivr::GetDialExStatus(bool bClearSemaphores)
{
	int iStatus;
	string sCallId;

	try {

		if( bClearSemaphores ){
			// Retira todos os semaphoros setados e retorna
			while( WaitForSingleObject(MapSemaphore[Channel], 1) == WAIT_OBJECT_0 );
			return 0;
		}
		if( FlagScriptHangup ){
			// O usuario da chamada de entrada desligou
			return STEX_LOCAL_HANGUP;
		}
		// Verifica se houve eventos por parte do canal de saida
		if(WaitForSingleObject(MapSemaphore[Channel], 1) != WAIT_OBJECT_0){
			return STEX_DIALING;
		}

		// Pega as informacoes do evento recebido
		GetInfoConnection();

		switch( Connection.Event ){
		
			case T_SW_DIAL_EX_DISCONNECTED:
				// O canal de saida desligou
				Log(LOG_CTI, "Event: T_SW_DIAL_EX_DISCONNECTED");
				// Conecta a interface de linha com o proprio canal de voz
				m_pCti->ScConnect(FULLDUP);
			return STEX_REMOTE_HANGUP;

			case T_SW_DIAL_EX_RESULT:
				// O canal de saida enviou o resultado da discagem

				// Guarda as informacoes sobre o canal de saida
				Connection_OUT = Connection;

				GetDataPacket(Connection.sData.c_str(), "STATUS", iStatus);
				GetDataPacket(Connection.sData.c_str(), "CALL_ID", sCallId);
				
				switch( iStatus ){

					case ST_DIAL_RING: // Chamando
						Log(LOG_CTI, "Status: ST_DIAL_RING");
						// Ouve a conexao do canal de destino
						ListenConnection();
					return STEX_ALERTING;

					case ST_DIAL_BUSY:				// Ocupado
						// Status Ocupado
						Log(LOG_CTI, "Status: ST_DIAL_BUSY");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_BUSY;

					case ST_DIAL_NOANSWER:		// Nao responde
						// Status nao atende
						Log(LOG_CTI, "Status: ST_DIAL_NOANSWER");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_NOANSWER;

					case ST_DIAL_CONGESTION:	// Congestionamento
						// Status congestionamento
						Log(LOG_CTI, "Status: ST_DIAL_CONGESTION");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_CONGESTION;

					case ST_DIAL_HANGUP:			// Canal de saida desligou
						// Status congestionamento
						Log(LOG_CTI, "Status: ST_DIAL_HANGUP");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_CONGESTION;

					case ST_DIAL_TIME_OUT:		// Timeout na tentiva de discar
						// Status nao atende
						Log(LOG_CTI, "Status: ST_DIAL_TIME_OUT");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_TIMEOUT;

					default:
						// Status congestionamento
						Log(LOG_CTI, "Default");
						// Conecta a interface de linha com o proprio canal de voz
						m_pCti->ScConnect(FULLDUP);
					return STEX_CONGESTION;
				}
			break;
		}

		return STEX_CONGESTION;
	}			
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::GetDialExStatus()");
		return STEX_CONGESTION;
	}
}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

// Essa funcao eh executada no canal que recebeu a requisicao da chamada (SAIDA)
void Ivr::DialOutEx(void)
{
	int Event, iTimeoutDial, iTimeEx;
	int iRecordUser = 0;
	char sStatus[20];
	string sDnis, sAni;

	try {

		Log(LOG_FUNCTIONS, "Ivr::DialOutEx()");

		// Incializa as variaveis para uma ligacao de saida
		// Se o canal for analogico, o Dnis deve estar concatenado com o mode 'P' ou  'T' (pulso ou tom)
		InitOutboundCall();
		// Pega informacoes sobre a conexao remota que pediu a solicitacao de discagem
		GetInfoConnection(T_SW_DIAL_EX);

		// Guarda as informacoes sobre o canal de entrada
		Connection_IN = Connection;
		
		GetDataPacket(Connection.sData.c_str(), "DNIS",				sDnis);
		GetDataPacket(Connection.sData.c_str(), "ANI",				sAni);
		GetDataPacket(Connection.sData.c_str(), "TIME_OUT",		iTimeoutDial);
		GetDataPacket(Connection.sData.c_str(), "INBOUND_ID",	sInboundCallId);
        		
        if( iTimeoutDial <= 0 || iTimeoutDial > m_pSwitch->GetTimeoutDial() ){
			iTimeoutDial = m_pSwitch->GetTimeoutDial();
		}

		strcpy(Ani, sAni.c_str());
		strcpy(Dnis, sDnis.c_str());

		// Conecta a interface de linha com o proprio canal de voz
		m_pCti->ScConnect(FULLDUP);

		#if defined _TELESP_CELULAR || _GRADIENTE
			// inicia o CDR de OutBound
      if(Switch::StRegister.LogCDR)
      {
			  DbOpenCdr();
      }
		#endif

		Log(LOG_FUNCTIONS, "Dial: %s", sDnis.c_str());

		// Disca assincronamente
		m_pCti->DialAsync((char *)sDnis.c_str(), (char *)sAni.c_str());

		Print(IVR_DNIS, sDnis.c_str());
		Print(IVR_ANI, sAni.c_str());

		// Marca o tempo de inicio da ligacao
		iTimeEx = time(NULL);

		for(;;){

			Event = WaitEvent();

			switch(Event){

				case T_NO_EVENTS:
					// Verifica se esgotou o tempo de esperar o evento de discagem
					if( (time(NULL) - iTimeEx) >= iTimeoutDial ){
						// Nao foi possivel discar no canal de saida
						Log(LOG_CTI, "Event: TIME OUT ON DIAL");

						// Connection_IN eh a origem requisitante
						Connection = Connection_IN;

						sprintf(sStatus, "STATUS=%d", ST_DIAL_NOANSWER);
						Connection.sData = sStatus;
						iDialResult = DIAL_OUT_NOANSWER;

						// Envia evento para o canal de destino ouvir a conexao com o resultado da discagem
						Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_RESULT");
						SendConnection(T_SW_DIAL_EX_RESULT, Connection.sData);

						// Desliga canal de saida
						Hangup();
					}
				break;

				case T_NOANSWER:
					Log(LOG_CTI, "Event: T_NOANSWER");

					// Connection_IN eh a origem requisitante
					Connection = Connection_IN;

					sprintf(sStatus, "STATUS=%d", ST_DIAL_NOANSWER);
					Connection.sData = sStatus;
					iDialResult = DIAL_OUT_NOANSWER;

					// Envia evento para o canal de destino ouvir a conexao com o resultado da discagem
					Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_RESULT");
					SendConnection(T_SW_DIAL_EX_RESULT, Connection.sData);

					// Desliga canal de saida
					Hangup();
				break;

				case T_DISCONNECT:
					// Nao foi possivel discar no canal de saida
					Log(LOG_CTI, "Event: T_DISCONNECT");

					// Connection_IN eh a origem requisitante
					Connection = Connection_IN;
					
					sprintf(sStatus, "STATUS=%d", ST_DIAL_CONGESTION);
					Connection.sData = sStatus;
					iDialResult = DIAL_OUT_CONGESTION;

					// Envia evento para o canal de destino ouvir a conexao com o resultado da discagem
					Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_RESULT");
					SendConnection(T_SW_DIAL_EX_RESULT, Connection.sData);

					// Desliga canal de saida
					Hangup();
				break;

				case T_ALERTING:
				case T_DIAL:
					Log(LOG_CTI, "Event: T_ALERTING");

					// Connection_IN eh a origem requisitante
					Connection = Connection_IN;

					// Ouve a conexao de origem que requisitou a discagem
					ListenConnection();
			
					// Envia o status da discagem e o CallId gerado para essa ligacao
					sprintf(sStatus, "STATUS=%d&CALL_ID=%s", ST_DIAL_RING, CallId);
					Connection.sData = sStatus;
					
					// Depois que atender alterar essa variavel para DIAL_OUT_CONNECTED
					iDialResult = DIAL_OUT_NOANSWER;

					// Envia evento para o canal de destino ouvir a conexao com o resultado da discagem
					Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_RESULT");
					SendConnection(T_SW_DIAL_EX_RESULT, Connection.sData);

					// Vai para o estado de canal de saida conectado
					GoState(DIAL_CONNECTED);
				break;
			}
		}
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::DialOutEx()");
//		GoEvents();
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Essa funcao eh executada no canal que executou a requisicao de chamada (SAIDA)
void Ivr::Dial_Connected(void)
{
	int Event;

	try {

		Log(LOG_FUNCTIONS, "Ivr::Dial_Connected()");

		for(;;){

			Event = WaitEvent();

			switch(Event){
	
				case T_SW_DIAL_EX_HANGUP: // Requisicao do canal de entrada para desligar a ligacao
					Log(LOG_CTI, "Event: T_SW_DIAL_EX_HANGUP");
					Hangup();
				break;

				case T_CONNECT:
					Log(LOG_CTI, "Event: T_CONNECT");
					// Ligacao de saida requisitada foi atendida, coloca o status na tela
					Print(IVR_STATUS, STR_CONNECTED);
					SetIconStatus(ICON_CONNECTED);
					// Atualiza o resultado da discagem para conectado para o CDR
					iDialResult = DIAL_OUT_CONNECTED;
				break;

				case T_NOANSWER:	// ligacao de saida requisitada nao foi atendida
					Log(LOG_CTI, "Event: T_NOANSWER");
					// Connection_IN eh a origem requisitante
					Connection = Connection_IN;
					// Envia evento para o canal de destino indicando que a chamada de saida terminou
					Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_DISCONNECTED");
					SendConnection(T_SW_DIAL_EX_DISCONNECTED, Connection.sData);
					Hangup();
				break;
				
				case T_DISCONNECT: // Canal de saida desligou
					Log(LOG_CTI, "Event: T_DISCONNECT");
					// Connection_IN eh a origem requisitante
					Connection = Connection_IN;
					// Envia evento para o canal de destino indicando que a chamada de saida terminou
					Log(LOG_INFO, "SendConnection: T_SW_DIAL_EX_DISCONNECTED");
					SendConnection(T_SW_DIAL_EX_DISCONNECTED, Connection.sData);
					Hangup();
				break;
			}
		}
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::Dial_Connected()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

int Ivr::SendFax(const char *sFile, const char *sDisplay)
{

#ifndef _FAX
	return 0;
#else

	int Event, Switch_Fax, Machine_Fax, Port_Fax, LocalSwitch, LocalMachine, LocalChannel;
	long lTimeToMergeFax, TimeOut = 0;
	bool bFlagDisconnected = false;
	string s;

	try {

		Log(LOG_FUNCTIONS, "Ivr::SendFax()");

		Port_Fax = DbGetPortFax();
		Machine_Fax = Switch::StRegister.Machine;
		Switch_Fax = Switch::StRegister.Switch;

		Log(LOG_CTI, "Switch_Fax: %d Machine_Fax: %d Channel_Fax %d", Switch_Fax, Machine_Fax, Port_Fax);

		m_pCti->ClearDigits();

		// 008 Por favor, aguarde, seu FAX esta sendo processado
		sprintf(sPrompt,"%s\\Portuguese\\General\\008.WAV", Switch::StRegister.PathPrompts);
		m_pCti->PlayFileSync(sPrompt);

		if(strlen(sDisplay) <= 0)
			s = " ";
		else
			s = sDisplay;

		sPathFileFax = sFile;

		if( strstr( _strlwr( (char *)sPathFileFax.c_str() ), ".txt") != NULL ){
			lTimeToMergeFax = GetTickCount();
			// Fax o merge dos arquivos de FAX baseado nas informacoes do arquivo com extensao TXT passado
			ExecFaxMerge(sPathFileFax.c_str(), sOutPutTiff, Switch::sFaxFont.c_str(), Switch::iFaxCharSize, Switch::iFaxLineWidth);
			lTimeToMergeFax = GetTickCount() - lTimeToMergeFax;
			Log(LOG_CTI, "Time to merge the FAX: %lu (ms)", lTimeToMergeFax);
			sPathFileFax = sOutPutTiff;
		}

		if(Port_Fax > 0){

			// Encontrou uma porta de FAX livre e ja alocou a mesma

			Connection.Switch		= Switch_Fax; 
			Connection.Machine	= Machine_Fax;
			Connection.Channel	= Port_Fax;   
			Connection.Timeslot	= 0;          

			Connection.sData		= "?ACTION=INIT_FAX";
			Connection.sData		+= "&PATH=";
			Connection.sData		+= sPathFileFax.c_str();

			// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
			if( !SendConnection(T_SW_INIT_FAX, Connection.sData) ){
				// Libera porta alocada no banco de dados
				DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
				// retorna erro para o script
				return 1;
			}

			for(;;){

				Event = WaitEvent();

				switch(Event){

					case T_NO_EVENTS:
					break;

					case T_DISCONNECT:
						// Usuario desligou enquanto esta pedindo FAX
						Log(LOG_CTI, "Event: T_DISCONNECT");
						// Libera porta alocada no banco de dados
						DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
					return -1;

					case T_SW_LISTEN:
						Log(LOG_CTI, "Event: T_SW_LISTEN");
						// Evento do controle de FAX pedindo para estabelecer a conexao SCBUS
						// Pega informacoes sobre a conexao destino					
						GetInfoConnection(Event);

						if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
							// O evento foi enviado por uma canal que nao esta mais associado com este,
							// provavelmente de uma ligacao anterior. Ignora o evento
							Log(LOG_INFO, "Event from ThreadFax is not valid");
							Log(LOG_INFO, " ");
							Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
							Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
							Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
							Log(LOG_INFO, " ");
							continue;
						}

						// Limpa a fila de digitos
						m_pCti->ClearDigits();
						// 009 Ao ouvir o sinal, aperte a tecla de inicio do FAX
						sprintf(sPrompt,"%s\\Portuguese\\General\\009.WAV", Switch::StRegister.PathPrompts);
						m_pCti->PlayFileSync(sPrompt);

						// Ouve o timeslot de transmissao
						ListenConnection();
						// Pede para a thread do FAX para enviar o FAX
						Connection.sData	= "?ACTION=SEND_FAX";
						Connection.sData	+= "&DISPLAY=";
						Connection.sData	+= s.c_str();

						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_SEND_FAX, Connection.sData) ){
							// Libera porta alocada no banco de dados
							DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
							// retorna erro para o script
							return 1;
						}

						for(;;){

							Event = WaitEvent();
							
							switch(Event){

								case T_NO_EVENTS:
									if( bFlagDisconnected ){
										if( (time(NULL) - TimeOut) >= TIME_TO_WAIT_FAX_RETURN ){
											// Se o timeout de TIME_TO_WAIT_FAX_RETURN segundos para esperar um evento de FAX
											// depois da desconexao expiro, retorna -1 (DISCONNECT) para o LUA
											Log(LOG_CTI, "Time out for wait for Fax events expired.");
											return -1;
										}
									}
								break;

								case T_DISCONNECT:
									// Usuario desligou enquanto esta pedindo FAX
									Log(LOG_CTI, "Event: T_DISCONNECT");
									Log(LOG_CTI, "Waiting %d seconds for Fax events...", TIME_TO_WAIT_FAX_RETURN);

									if( bFlagDisconnected == false){
										// Seta um flag indicando que a ligacao foi desconectada antes de receber
										// um evento de FAX, a aplicacao vai esperar 5 segundos por um evento
										// de FAX antes de retornar um DISCONNECT para o LUA, pois quando a operacao
										// de FAX termina pode ser que o evento T_DISCONNECT veio antes da
										// thread que controla os recursos de FAX enviar o evento de termino de FAX
										bFlagDisconnected = true;
										TimeOut = time(NULL);
									}
								break;

								case T_SW_FAX_OK:
									Log(LOG_CTI, "Event: T_SW_FAX_OK");
									GetInfoConnection(Event);

									if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
										// O evento foi enviado por uma canal que nao esta mais associado com este,
										// provavelmente de uma ligacao anterior. Ignora o evento
										Log(LOG_INFO, "Event from ThreadFax is not valid");
										Log(LOG_INFO, " ");
										Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
										Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
										Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
										Log(LOG_INFO, " ");
										continue;
									}
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 0;

								case T_SW_FAX_ERROR:
									Log(LOG_CTI, "Event: T_SW_FAX_ERROR");
									GetInfoConnection(Event);

									if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
										// O evento foi enviado por uma canal que nao esta mais associado com este,
										// provavelmente de uma ligacao anterior. Ignora o evento
										Log(LOG_INFO, "Event from ThreadFax is not valid");
										Log(LOG_INFO, " ");
										Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
										Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
										Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
										Log(LOG_INFO, " ");
										continue;
									}
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 1;
							}
						}
					break;
				}
			}
		}
		else{

			// Nao foi possivel achar canal de FAX disponivel coloca o canal em um fila de espera por FAX
			if( !Switch::FaxQueue.InsertFaxQueue(Channel, sPathFileFax.c_str()) ){
				// Nao foi possivel colocar o usuario na fila de FAX, fila cheia
				return 2;
			}

			// Toca musica de espera para a fila de FAX
			PlayHoldMusic();

			for(;;){

				Event = WaitEvent();

				switch(Event){

					case T_NO_EVENTS:
					break;

					case T_PLAY:
						Log(LOG_CTI, "Event: T_PLAY");
						PlayHoldMusic();
					break;

					case T_DISCONNECT:
						// Usuario desligou enquanto esta pedindo FAX
						Log(LOG_CTI, "Event: T_DISCONNECT");
						// Retira o canal da fila de FAX
						Switch::FaxQueue.DeleteFaxQueue(Channel);
					return -1;

					case T_SW_LISTEN:
						Log(LOG_CTI, "Event: T_SW_LISTEN");
						// Evento do controle de FAX pedindo para estabelecer a conexao SCBUS
						// Pega informacoes sobre a conexao destino					
						GetInfoConnection(Event);

						// Verifica a integridade do evento de FAX para assegurar que o FAX
						// que esta sendo recebido eh o mesmo dessa ligacao

						GetDataPacket(Connection.sData.c_str(), "SWITCH_USER",		LocalSwitch);
						GetDataPacket(Connection.sData.c_str(), "MACHINE_USER",		LocalMachine);
						GetDataPacket(Connection.sData.c_str(), "CHANNEL_USER",		LocalChannel);

						if(	LocalSwitch		!=	Switch::StRegister.Switch		|| 
								LocalMachine	!=	Switch::StRegister.Machine	||
								LocalChannel	!=	Channel){

							// O evento foi enviado por uma canal que nao esta mais associado com este,
							// provavelmente de uma ligacao anterior. Ignora o evento
							Log(LOG_INFO, "Event from ThreadFax is not valid");
							Log(LOG_INFO, " ");
							Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
							Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
							Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
							Log(LOG_INFO, " ");
							continue;
						}

						// Para o canal de voz assincronamente
						switch( m_pCti->StopCh(5000) ){
						
							case T_DISCONNECT:
								// Usuario desligou enquanto esta pedindo FAX
								Log(LOG_CTI, "Event: T_DISCONNECT");
								// Retira o canal da fila de FAX
								Switch::FaxQueue.DeleteFaxQueue(Channel);
							return -1;
						}

						// Limpa a fila de digitos
						m_pCti->ClearDigits();
						// 009 Ao ouvir o sinal, aperte a tecla de inicio do FAX
						sprintf(sPrompt,"%s\\Portuguese\\General\\009.WAV", Switch::StRegister.PathPrompts);
						m_pCti->PlayFileSync(sPrompt);

						// Ouve o timeslot de transmissao
						ListenConnection();
						// Pede para a thread do FAX para enviar o FAX
						Connection.sData	= "?ACTION=SEND_FAX";
						Connection.sData	+= "&DISPLAY=";
						Connection.sData	+= s.c_str();

						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_SEND_FAX, Connection.sData) ){
							// Libera porta alocada no banco de dados
							DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
							// retorna erro para o script
							return 1;
						}

						for(;;){

							Event = WaitEvent();
							
							switch(Event){

								case T_NO_EVENTS:
									if( bFlagDisconnected ){
										if( (time(NULL) - TimeOut) >= TIME_TO_WAIT_FAX_RETURN ){
											// Se o timeout de TIME_TO_WAIT_FAX_RETURN segundos para esperar um evento de FAX
											// depois da desconexao expiro, retorna -1 (DISCONNECT) para o LUA
											Log(LOG_CTI, "Time out for wait for Fax events expired.");
											return -1;
										}
									}
								break;

								case T_DISCONNECT:
									// Usuario desligou enquanto esta pedindo FAX
									Log(LOG_CTI, "Event: T_DISCONNECT");
									Log(LOG_CTI, "Waiting %d seconds for Fax events...", TIME_TO_WAIT_FAX_RETURN);

									if( bFlagDisconnected == false){
										// Seta um flag indicando que a ligacao foi desconectada antes de receber
										// um evento de FAX, a aplicacao vai esperar 5 segundos por um evento
										// de FAX antes de retornar um DISCONNECT para o LUA, pois quando a operacao
										// de FAX termina pode ser que o evento T_DISCONNECT veio antes da
										// thread que controla os recursos de FAX enviar o evento de termino de FAX
										bFlagDisconnected = true;
										TimeOut = time(NULL);
									}
								break;

								case T_SW_FAX_OK:
									Log(LOG_CTI, "Event: T_SW_FAX_OK");
									GetInfoConnection(Event);
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 0;

								case T_SW_FAX_ERROR:
									Log(LOG_CTI, "Event: T_SW_FAX_ERROR");
									GetInfoConnection(Event);
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 1;
							}
						}
					break;
				}
			}
			return 1;
		}
		return 0;		
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::SendFax()");
		return 1;
	}

#endif // _FAX
}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

int Ivr::RecvFax(const char *sFile, const char *sDisplay)
{

#ifndef _FAX
	return 0;
#else

	int Event, Switch_Fax, Machine_Fax, Port_Fax, LocalSwitch, LocalMachine, LocalChannel;
	long TimeOut = 0;
	bool bFlagDisconnected = false;
	string s;

	try {

		Log(LOG_FUNCTIONS, "Ivr::RecvFax()");

		if(strlen(sDisplay) <= 0)
			s = " ";
		else
			s = sDisplay;

		sPathFileFax = sFile;

		Port_Fax = DbGetPortFax();
		Machine_Fax = Switch::StRegister.Machine;
		Switch_Fax = Switch::StRegister.Switch;

		m_pCti->ClearDigits();

		// 008 Por favor, aguarde, seu FAX esta sendo processado
		sprintf(sPrompt,"%s\\Portuguese\\General\\008.WAV", Switch::StRegister.PathPrompts);
		m_pCti->PlayFileSync(sPrompt);

		if(Port_Fax > 0){

			// Encontrou uma porta de FAX livre e ja alocou a mesma

			Connection.Switch		= Switch_Fax; 
			Connection.Machine	= Machine_Fax;
			Connection.Channel	= Port_Fax;   
			Connection.Timeslot	= 0;          

			Connection.sData		= "?ACTION=INIT_FAX";
			Connection.sData		+= "&PATH=";
			Connection.sData		+= sPathFileFax.c_str();

			// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
			if( !SendConnection(T_SW_INIT_FAX, Connection.sData) ){
				// Libera porta alocada no banco de dados
				DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
				// retorna erro para o script
				return 1;
			}

			for(;;){

				Event = WaitEvent();

				switch(Event){

					case T_NO_EVENTS:
					break;

					case T_DISCONNECT:
						// Usuario desligou enquanto esta pedindo FAX
						Log(LOG_CTI, "Event: T_DISCONNECT");
						// Libera porta alocada no banco de dados
						DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
					return -1;

					case T_SW_LISTEN:
						Log(LOG_CTI, "Event: T_SW_LISTEN");
						// Evento do controle de FAX pedindo para estabelecer a conexao SCBUS
						// Pega informacoes sobre a conexao destino					
						GetInfoConnection(Event);

						if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
							// O evento foi enviado por uma canal que nao esta mais associado com este,
							// provavelmente de uma ligacao anterior. Ignora o evento
							Log(LOG_INFO, "Event from ThreadFax is not valid");
							Log(LOG_INFO, " ");
							Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
							Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
							Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
							Log(LOG_INFO, " ");
							continue;
						}

						// Limpa a fila de digitos
						m_pCti->ClearDigits();
						// 009 Ao ouvir o sinal, aperte a tecla de inicio do FAX
						sprintf(sPrompt,"%s\\Portuguese\\General\\009.WAV", Switch::StRegister.PathPrompts);
						m_pCti->PlayFileSync(sPrompt);

						// Ouve o timeslot de transmissao
						ListenConnection();
						// Pede para a thread do FAX para receber o FAX
						Connection.sData	= "?ACTION=RECV_FAX";
						Connection.sData	+= "&DISPLAY=";
						Connection.sData	+= s.c_str();

						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_RECV_FAX, Connection.sData) ){
							// Libera porta alocada no banco de dados
							DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
							// retorna erro para o script
							return 1;
						}

						for(;;){

							Event = WaitEvent();
							
							switch(Event){

								case T_NO_EVENTS:
									if( bFlagDisconnected ){
										if( (time(NULL) - TimeOut) >= TIME_TO_WAIT_FAX_RETURN ){
											// Se o timeout de TIME_TO_WAIT_FAX_RETURN segundos para esperar um evento de FAX
											// depois da desconexao expiro, retorna -1 (DISCONNECT) para o LUA
											Log(LOG_CTI, "Time out for wait for Fax events expired.");
											return -1;
										}
									}
								break;

								case T_DISCONNECT:
									// Usuario desligou enquanto esta pedindo FAX
									Log(LOG_CTI, "Event: T_DISCONNECT");
									Log(LOG_CTI, "Waiting %d seconds for Fax events...", TIME_TO_WAIT_FAX_RETURN);

									if( bFlagDisconnected == false){
										// Seta um flag indicando que a ligacao foi desconectada antes de receber
										// um evento de FAX, a aplicacao vai esperar 5 segundos por um evento
										// de FAX antes de retornar um DISCONNECT para o LUA, pois quando a operacao
										// de FAX termina pode ser que o evento T_DISCONNECT veio antes da
										// thread que controla os recursos de FAX enviar o evento de termino de FAX
										bFlagDisconnected = true;
										TimeOut = time(NULL);
									}
								break;

								case T_SW_FAX_OK:
									Log(LOG_CTI, "Event: T_SW_FAX_OK");
									GetInfoConnection(Event);

									if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
										// O evento foi enviado por uma canal que nao esta mais associado com este,
										// provavelmente de uma ligacao anterior. Ignora o evento
										Log(LOG_INFO, "Event from ThreadFax is not valid");
										Log(LOG_INFO, " ");
										Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
										Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
										Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
										Log(LOG_INFO, " ");
										continue;
									}
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 0;

								case T_SW_FAX_ERROR:
									Log(LOG_CTI, "Event: T_SW_FAX_ERROR");
									GetInfoConnection(Event);

									if(Connection.Switch != Switch_Fax || Connection.Machine != Machine_Fax || Connection.Channel	!= Port_Fax){
										// O evento foi enviado por uma canal que nao esta mais associado com este,
										// provavelmente de uma ligacao anterior. Ignora o evento
										Log(LOG_INFO, "Event from ThreadFax is not valid");
										Log(LOG_INFO, " ");
										Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
										Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
										Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
										Log(LOG_INFO, " ");
										continue;
									}
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 1;
							}
						}
					break;
				}
			}
		}
		else{

			// Nao foi possivel achar canal de FAX disponivel coloca o canal em um fila de espera por FAX
			if( Switch::FaxQueue.InsertFaxQueue(Channel, sPathFileFax.c_str()) ){
				// Nao foi possivel colocar o usuario na fila de FAX, fila cheia
				return 2;
			}

			// Toca musica de espera para a fila de FAX
			PlayHoldMusic();

			for(;;){

				Event = WaitEvent();

				switch(Event){

					case T_NO_EVENTS:
					break;

					case T_PLAY:
						Log(LOG_CTI, "Event: T_PLAY");
						PlayHoldMusic();
					break;

					case T_DISCONNECT:
						// Usuario desligou enquanto esta pedindo FAX
						Log(LOG_CTI, "Event: T_DISCONNECT");
						// Retira o canal da fila de FAX
						Switch::FaxQueue.DeleteFaxQueue(Channel);
					return -1;

					case T_SW_LISTEN:
						Log(LOG_CTI, "Event: T_SW_LISTEN");
						// Evento do controle de FAX pedindo para estabelecer a conexao SCBUS
						// Pega informacoes sobre a conexao destino					
						GetInfoConnection(Event);

						// Verifica a integridade do evento de FAX para assegurar que o FAX
						// que esta sendo recebido eh o mesmo dessa ligacao

						GetDataPacket(Connection.sData.c_str(), "SWITCH_USER",		LocalSwitch);
						GetDataPacket(Connection.sData.c_str(), "MACHINE_USER",		LocalMachine);
						GetDataPacket(Connection.sData.c_str(), "CHANNEL_USER",		LocalChannel);

						if(	LocalSwitch		!=	Switch::StRegister.Switch		|| 
								LocalMachine	!=	Switch::StRegister.Machine	||
								LocalChannel	!=	Channel){

							// O evento foi enviado por uma canal que nao esta mais associado com este,
							// provavelmente de uma ligacao anterior. Ignora o evento
							Log(LOG_INFO, "Event from ThreadFax is not valid");
							Log(LOG_INFO, " ");
							Log(LOG_INFO,"Connection.Switch  :%d | Switch_Fax  :%d",	Connection.Switch,	Switch_Fax);
							Log(LOG_INFO,"Connection.Machine :%d | Machine_Fax :%d",	Connection.Machine,	Machine_Fax);
							Log(LOG_INFO,"Connection.Channel :%d | Port_Fax    :%d",	Connection.Channel,	Port_Fax);
							Log(LOG_INFO, " ");
							continue;
						}

						// Para o canal de voz assincronamente
						switch( m_pCti->StopCh(5000) ){
						
							case T_DISCONNECT:
								// Usuario desligou enquanto esta pedindo FAX
								Log(LOG_CTI, "Event: T_DISCONNECT");
								// Retira o canal da fila de FAX
								Switch::FaxQueue.DeleteFaxQueue(Channel);
							return -1;
						}

						// Limpa a fila de digitos
						m_pCti->ClearDigits();
						// 009 Ao ouvir o sinal, aperte a tecla de inicio do FAX
						sprintf(sPrompt,"%s\\Portuguese\\General\\009.WAV", Switch::StRegister.PathPrompts);
						m_pCti->PlayFileSync(sPrompt);

						// Ouve o timeslot de transmissao
						ListenConnection();
						// Pede para a thread do FAX para enviar o FAX
						Connection.sData	= "?ACTION=RECV_FAX";
						Connection.sData	+= "&DISPLAY=";
						Connection.sData	+= s.c_str();

						// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
						if( !SendConnection(T_SW_RECV_FAX, Connection.sData) ){
							// Libera porta alocada no banco de dados
							DbSetPortStatus(PORT_FREE_AVAILABLE, Port_Fax);
							// retorna erro para o script
							return 1;
						}

						for(;;){

							Event = WaitEvent();
							
							switch(Event){

								case T_NO_EVENTS:
									if( bFlagDisconnected ){
										if( (time(NULL) - TimeOut) >= TIME_TO_WAIT_FAX_RETURN ){
											// Se o timeout de TIME_TO_WAIT_FAX_RETURN segundos para esperar um evento de FAX
											// depois da desconexao expiro, retorna -1 (DISCONNECT) para o LUA
											Log(LOG_CTI, "Time out for wait for Fax events expired.");
											return -1;
										}
									}
								break;

								case T_DISCONNECT:
									// Usuario desligou enquanto esta pedindo FAX
									Log(LOG_CTI, "Event: T_DISCONNECT");
									Log(LOG_CTI, "Waiting %d seconds for Fax events...", TIME_TO_WAIT_FAX_RETURN);

									if( bFlagDisconnected == false){
										// Seta um flag indicando que a ligacao foi desconectada antes de receber
										// um evento de FAX, a aplicacao vai esperar 5 segundos por um evento
										// de FAX antes de retornar um DISCONNECT para o LUA, pois quando a operacao
										// de FAX termina pode ser que o evento T_DISCONNECT veio antes da
										// thread que controla os recursos de FAX enviar o evento de termino de FAX
										bFlagDisconnected = true;
										TimeOut = time(NULL);
									}
								break;

								case T_SW_FAX_OK:
									Log(LOG_CTI, "Event: T_SW_FAX_OK");
									GetInfoConnection(Event);
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 0;

								case T_SW_FAX_ERROR:
									Log(LOG_CTI, "Event: T_SW_FAX_ERROR");
									GetInfoConnection(Event);
									// Conecta a interface de linha com o proprio canal de voz
									m_pCti->ScConnect(FULLDUP);
								return 1;
							}
						}
					break;
				}
			}
			return 1;
		}
		return 0;		
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::RecvFax()");
		return 1;
	}

#endif // _FAX
}

//---------------------------------------------------------------------------------------------

int Ivr::GetDataPacket(const char *datapacket, char *cCommand, string &sValue)
{
    string data, valor;
    int pos_ini, pos_fim;

    try
    {
		data = datapacket;
		pos_ini = data.find(cCommand, 0);

		if( pos_ini == -1 )
        {
			sValue= "";
			return -1;
		}

		pos_ini += (strlen(cCommand) + 1);
		pos_fim = data.find("&", pos_ini);
		sValue = data.substr(pos_ini, pos_fim-pos_ini).c_str();

		return 0;
	}
	catch(...)
    {
		sValue= "";
        m_pLog4cpp->debug("EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, string &sValue)");
		return -1;
	}
}

//---------------------------------------------------------------------------------------------

int Ivr::GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize)
{
    string data, valor;
    int pos_ini, pos_fim;

    try {

		data = datapacket;

		pos_ini = data.find(cCommand, 0);

		if( pos_ini == -1 )
        {
			strcpy(cValue, "");
			return -1;
		}

		pos_ini += (strlen(cCommand) + 1);

		pos_fim = data.find("&", pos_ini);

		strncpy(cValue, data.substr(pos_ini, pos_fim-pos_ini).c_str(), valuesize);

		return 0;
	}
	catch(...){
		m_pLog4cpp->debug("EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, char *cValue, int valuesize)");
		strcpy(cValue, "");
		return -1;
	}
}

//---------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------

int Ivr::GetDataPacket(const char *datapacket, char *cCommand, int &iValue)
{
    string data, valor, command;
    int pos_ini, pos_fim;

    try 
    {

		data = datapacket;
        command = cCommand;
        command += "=";

		pos_ini = data.find(command.c_str(), 0);

		if( pos_ini == -1 )
        {
			iValue = 0;
			return -1;
		}

		pos_ini += command.size();
		pos_fim = data.find("&", pos_ini);

        if( pos_fim == -1 )
        {
			pos_fim = data.size();
		}

		iValue = atoi( data.substr(pos_ini, pos_fim-pos_ini).c_str() );

		return 0;
	}
	catch(...)
    {
		iValue = 0;
		m_pLog4cpp->debug("EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, int &iValue)");
		return -1;
	}
}

//---------------------------------------------------------------------------------------------
char * Ivr::ReturnAni(void) const
{
	return (char *)&Ani[0];
}

//---------------------------------------------------------------------------------------------

char * Ivr::ReturnDnis(void) const
{
	return (char *)&Dnis[0];
}

//---------------------------------------------------------------------------------------------

int Ivr::ReturnCategory(void)
{
	return Category;
}
//------------------------------------------------------------------------------------------------------------------------

int Ivr::DbGetDialPort(int Route, int &Switch, int &Machine, int &Port, string &sDnis, string &sAni)
{
	try
	{
		int index = 1;
		
		Log(LOG_FUNCTIONS, "Ivr::DbGetDialPort()");

		//for(index = 1; index <= Switch::NumPortsInDataBase; index++)
        for(index = 1; index <= m_pSwitch->GetNumPortsInDataBase() ; index++)
		{			
            if ( ( m_pSwitch->GetChannelsDirection(index) == 'D' ) || ( m_pSwitch->GetChannelsDirection(index) == 'O' ) )
                if ( m_pSwitch->GetChannelsStatus(index) == PORT_FREE_AVAILABLE )				
				{
                    Switch = m_pSwitch->GetSwitch();
					Machine = m_pSwitch->GetMachine();
					Port = index;

                    m_pSwitch->SetChannelsStatus(index, PORT_DIALING);
                    
				}
		}

		return 0;
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::DbGetDialPort()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------
int Ivr::ListenConnection(void)
{
	int iRet;

	try {
	
		m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);

		if(	    ( m_pSwitch->GetSwitch() ==	Connection.Switch) &&
				( m_pSwitch->GetMachine() ==	Connection.Machine) )
        {

			// Conexao Local SCBUS

            m_pLog4cpp->debug("Connection.Timeslot    :%d",	Connection.Timeslot);
			
			iRet = m_pCti->ScListen(Connection.Timeslot);
			
			// faz o recurso de voz ouvir o timeslot da interface de linha
			//CTI->ScConnect(HALFDUP);

			if(iRet){
				m_pLog4cpp->error("Ivr::ListenConnection() %s", m_pCti->GetLastError());
				// Desconecta a interface de linha de qualquer conexao
				//CTI->ScUnlisten();
				m_pCti->ScConnect(FULLDUP);
				return -1;
			}

		}
		else
        {
			// Conexao remota ATM			
			m_pLog4cpp->debug("Connection.Timeslot    :%d",	Connection.Timeslot);

			iRet = m_pCti->ScListen(Connection.Timeslot);

			// faz o recurso de voz ouvir o timeslot da interface de linha
			m_pCti->ScConnect(HALFDUP);

			if(iRet){
				// Desconecta a interface de linha de qualquer conexao
				m_pCti->ScUnlisten();
				return -1;
			}
		}
		return 0;
	}
	catch(...)
    {
		m_pLog4cpp->error("EXCEPTION: Ivr::ListenConnection()");
		return -1;
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Ivr::WaitEvent(void)
{
	try {

		// Fecha arquivo de log para que os antigos sejam apagados
//		if( DayChanged() )
//        {
//			Log(LOG_ERROR, "");
//		}

		return m_pCti->WaitEvent();
	}
	catch(...){
        m_pLog4cpp->error("EXCEPTION: Ivr::SendIvrEvent()"); 
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------


// Chama a funcao longjmp com valor de 2 para ir ate o estado especificado
void Ivr::GoState(int State)
{
	try {
		EstadoIVR = State;
		longjmp(jmp_state, 2);
	}
	catch(...){
		m_pLog4cpp->error("EXCEPTION: Ivr::GoState()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------

void Ivr::InitOutboundCall(void)
{
	try 
    {
		m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);

		// Inicializa as variaveis para uma nova chamada
		FlagHangup			= false;
		FlagRecordUser	= false;
		FlagTranfer			= false;

		ClearConn(Connection_local);
		ClearConn(Connection_remote);
		ClearConn(Connection_PA);
		ClearConn(Connection_OUT);
		ClearConn(Connection_IN);

		ClearCdrDetail();

		memset(&ServiceName,			0, sizeof(ServiceName));
		memset(&Ani,							0, sizeof(Ani));
		memset(&Dnis,							0, sizeof(Dnis));

		// 1 = usuario desligado pelo script
		// 2 = usuario desligado pela posicao de atendimento
		// 3 = usuario desligou
		HangupReason = 0;
		// Guarda a data de inicio da ligacao
		GetDatetime(sIniDateOfCall);
		// Guarda o tempo em segundos de inicio da ligacao
		TimeOfCall = time(NULL);
		// Seta o offset para o arquivo de musica de espera para o inicio do arquivo
		FilePosToPlayHoldMusic = 0;
		// 1 = Ligacao Interna, 2 = Ligacao externa
		IdCallFrom = 2;
		// Guarda o status do DialOut			
		iDialResult = 0;
		// Ligacao de entrada
		iCallDirection = OUTBOUND;
		// Seta o status da porta no banco de dados para discando
		DbSetPortStatus(PORT_DIALING, Channel);
		// Seta para pegar todos os digitos
		m_pCti->SetDigMask("@");
		// Seta status da tela
		Print(IVR_STATUS, STR_DIALING);
		Print(IVR_CALL_TYPE,	"OUT");
		SetIconStatus(ICON_ALERTING);
		
        // Pega o ID da chamada do banco de dados

//        if(Switch::StRegister.LogCDR)
//		    DbReadCallId();

	}
	catch(...)
	{
		m_pLog4cpp->error("EXCEPTION: Ivr::InitOutboundCall()");
		GoEvents();
	}
}

//------------------------------------------------------------------------------------------------------------------------

// Chama a funcao longjmp com valor de 1 para ir ate o estado de pegar eventos
void Ivr::GoEvents(void)
{
	try {
		longjmp(jmp_state, 1);
	}
	catch(...){
		m_pLog4cpp->error("EXCEPTION: Ivr::GoEvents()");
	}
}

void Ivr::GoEndThread(void)
{
	try {
		longjmp(jmp_state, 3);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::GoEvents()");
	}
}

const std::string & Ivr::GetDbIndexConnection()
{
    return DbIndexConnection;
}

void Ivr::SetDbIndexConnection(const std::string & value)
{
    DbIndexConnection = value;
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::PushRecordSet(int iRow, int iCol, string &sData)
{
	try {

		MapRecordSet[iRow][iCol] = sData;
	}
	catch(...){
		m_pLog4cpp->debug("EXCEPTION: Ivr::PushRecordSet()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::PopRecordSet(int iRow, int iCol, string &sData)
{
	try {

		sData = MapRecordSet[iRow][iCol];
	}
	catch(...){
		m_pLog4cpp->debug("EXCEPTION: Ivr::PopRecordSet()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

void Ivr::ClearRecordSet(void)
{
	try {

		MapRecordSet.clear();
	}
	catch(...){
		m_pLog4cpp->debug("EXCEPTION: Ivr::ClearRecordSet()");
	}
}

//------------------------------------------------------------------------------------------------------------------------

int Ivr::RemoteAudioPlayFile( const char *play, const char *terminator, long& timeslotvox )
{
	try
	{
		int Event;
		time_t Time;
		bool bFlagDisconnect = false;
		int rc;

		Log( LOG_INFO, "RemoteAudioPlayFile( \"%s\", \"%s\" )", play, terminator );

		if( RemoteAudioPort > 0 )
		{
			// Existe uma reproducao em andamento. Manda parar.
			rc = RemoteAudioStop();
			if( rc < 0 )
			{
				return rc;
			}
			if( rc == T_DISCONNECT )
			{
				return T_DISCONNECT;
			}
		}

		// Reserva um recurso no banco de dados
		RemoteAudioPort = DbGetResource();
		if( RemoteAudioPort < 0 )
		{
			Log( LOG_ERROR, "Error in RemotePlayFile(): There is no available resources." );
			return -1;
		}

		ClearConn( Connection );
		
        Connection.Switch		= m_pSwitch->GetSwitch();
        Connection.Machine	    = m_pSwitch->GetMachine();
		Connection.Channel	= RemoteAudioPort;

		Connection.sData		=  "?ACTION=AUDIO_PLAY";
		Connection.sData		+= "&FILENAME=";
		Connection.sData		+= play;
		Connection.sData		+= "&TERMINATOR=";
		Connection.sData		+= terminator;

		// Envia evento para o canal de destino tocar um arquivo
		Log( LOG_INFO, "SendConnection: T_SW_AUDIO_REQUEST" );
		if( !SendConnection( T_SW_AUDIO_REQUEST, Connection.sData ) )
		{
			Log( LOG_ERROR, "SendConnection() error in RemotePlayFile(): T_SW_AUDIO_REQUEST" );
			return -2;
		}

		Time = time(NULL);

		// Após o envio, fica aguardando os eventos de resposta
		for(;;)
		{
			Event = WaitEvent();

			switch( Event )
			{
				case T_NO_EVENTS:
          if( ( time(NULL) - Time ) >= EVENT_TIMEOUT )
					{
						Log(LOG_CTI, "Timeout waiting event.");
					  return -3;
					}
          break;

				case T_DISCONNECT:
					// Usuario desligou
					Log(LOG_CTI, "Event: T_DISCONNECT");
					bFlagDisconnect = true;
				  break;

				case T_SW_AUDIO_OK:
					Log(LOG_CTI, "Event: T_SW_AUDIO_OK");
					GetInfoConnection( Event );
//					if( GetDataPacket( Connection.sData.c_str(), "TIMESLOTVOX", timeslotvox ) )
//					{
//						Log( LOG_ERROR, "There is no data in tag TIMESLOTVOX." );
//						return -4;
//					}
					if( bFlagDisconnect )
						return T_DISCONNECT;
					return 0;

				case T_SW_AUDIO_ERROR:
					Log(LOG_CTI, "Event: T_SW_AUDIO_ERROR");
					GetInfoConnection( Event );
					if( bFlagDisconnect )
						return T_DISCONNECT;
					return -5;
			}
		}

		return 0;
	}
	catch(...)
	{
		Log( LOG_ERROR, "Exception in RemoteAudioPlayFile()." );
		return 0xFFFFFFFF;
	}
}

int Ivr::RemoteAudioStop()
{
	try
	{
		if( RemoteAudioPort <= 0 )
			// Canal remoto ja foi parado.
			return -1;

		int Event;
		time_t Time;
		bool bFlagDisconnect = false;

		Log( LOG_INFO, "RemoteAudioStop()" );

		ClearConn( Connection );
		
        Connection.Switch		= this->m_pSwitch->GetSwitch();
        Connection.Machine	=   this->m_pSwitch->GetMachine();
		Connection.Channel	= RemoteAudioPort;

		Connection.sData		= "ACTION=AUDIO_STOP";

		// Envia evento para o canal de destino tocar um arquivo
		Log( LOG_INFO, "SendConnection: T_SW_AUDIO_REQUEST" );
		if( !SendConnection( T_SW_AUDIO_REQUEST, Connection.sData ) )
		{
			Log( LOG_ERROR, "SendConnection() error in RemotePlayFile(): T_SW_AUDIO_REQUEST" );
			return -1;
		}

		Time = time(NULL);

		// Após o envio, fica aguardando os eventos de resposta
		for(;;)
		{
			Event = WaitEvent();

			switch( Event )
			{
				case T_NO_EVENTS:
          if( ( time(NULL) - Time ) >= EVENT_TIMEOUT )
					{
						Log(LOG_CTI, "Timeout waiting event.");
					  return -2;
					}
          break;

				case T_DISCONNECT:
					// Usuario desligou
					Log(LOG_CTI, "Event: T_DISCONNECT");
					bFlagDisconnect = true;
				  break;

				case T_SW_AUDIO_OK:
					Log(LOG_CTI, "Event: T_SW_AUDIO_OK");
					GetInfoConnection( Event );
					if( DbSetPortStatus( PORT_RESERVED, RemoteAudioPort ) < 0 )
					{
						Log( LOG_ERROR, "Error freeing port." );
						return -3;
					}
					RemoteAudioPort = -1;
					if( bFlagDisconnect )
						return T_DISCONNECT;
					return 0;

				case T_SW_AUDIO_ERROR:
					Log(LOG_CTI, "Event: T_SW_AUDIO_ERROR");
					if( bFlagDisconnect )
						return T_DISCONNECT;
					return -4;
			}
		}

		return 0;
	}
	catch(...)
	{
		Log( LOG_ERROR, "Exception in RemoteAudioStop()." );
		return 0xFFFFFFFF;
	}
}

int Ivr::RecordFileWithMusic( const char *record, const char *play, int timeout, int silence_timeout, const char *terminator )
{
	try
	{
		int rc = 0;
		int RecordResult = 0;
		long timeslotvox = -1;
		
		rc = RemoteAudioPlayFile( play, "", timeslotvox );
		if( rc < 0 )
		{
			Log( LOG_ERROR, "Error in RecordFileWithMusic() using RemoteAudioPlayFile()." );
			if( rc == -1 )
			{
				// Retorna 1 quando nao houver recursos disponiveis.
				return 1;
			}
			if( RemoteAudioStop() == T_DISCONNECT )
			{
				return T_DISCONNECT;
			}
			return rc;
		}
		if( rc == T_DISCONNECT )
		{
			RemoteAudioStop();
			return T_DISCONNECT;
		}
		// Faz o recurso de rede ouvir o recurso de voz remoto.
		m_pCti->ScListen( timeslotvox );

		rc = m_pCti->RecordFileFrom2Src( record, timeslotvox, timeout, silence_timeout, false, terminator );
		if( rc < 0 )
		{
			Log( LOG_ERROR, "Error in RecordFileWithMusic() using CTI->RecordFileFrom2Src()." );
			RemoteAudioStop();
			// Restaura a conexao recurso de rede <-> recurso de voz.
            this->m_pCti->ScConnect( FULLDUP );
			return rc;
		}
		if( rc == T_DISCONNECT )
		{
			RemoteAudioStop();
			// Restaura a conexao recurso de rede <-> recurso de voz.
			m_pCti->ScConnect( FULLDUP );
			return rc;
		}

		rc = RemoteAudioStop();
		if( rc < 0 )
		{
			Log( LOG_ERROR, "Error in RecordFileWithMusic() using RemoteAudioStop()." );
			// Restaura a conexao recurso de rede <-> recurso de voz.
			m_pCti->ScConnect( FULLDUP );
			return rc;
		}
		if( rc == T_DISCONNECT )
		{
			// Restaura a conexao recurso de rede <-> recurso de voz.
			m_pCti->ScConnect( FULLDUP );
			return T_DISCONNECT;
		}
		// Restaura a conexao recurso de rede <-> recurso de voz.
		m_pCti->ScConnect( FULLDUP );
		
		return rc;
	}
	catch(...)
	{
		Log( LOG_ERROR, "Exception in RecordFileWithMusic()." );
		return 0xFFFFFFFF;
	}
}

int Ivr::DbGetResource()
{
	try
	{
		int Port = -1;
		int index = 1;		
		Log(LOG_FUNCTIONS, "Ivr::DbGetResource()");

        while ((Port == -1) && (index <= m_pSwitch->GetNumPortsInDataBase() ))
		{
			if( m_pSwitch->GetChannelsStatus(index) == PORT_RESERVED)
			{
				Port = index;				
                m_pSwitch->SetChannelsStatus(index, PORT_LOCKED );
				
				
			}
			index++;
		}
		return Port;
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Switch::Ivr::DbGetResource()");
		return -1;
	}
}

int Ivr::UnListenGlobalAudio(void)
{
    return -1;
}


int Ivr::ListenGlobalAudio(long TimeslotRing)
{
    return -1;
}


int Ivr::GetGlobalTimeSlot(void)
{
    return TxGlobalTimeSlot;
}



// Loga no banco de dados o CallDetail
int Ivr::DbCallDetail(int Evento, char *sDataInicio, char *sDataFim)
{
	try
	{
		Log(LOG_FUNCTIONS, "Ivr::DbCallDetail()");
		return 0;
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::DbCallDetail()");
		return -1;
	}
}

int Ivr::DbHoliday(int iDia, int iMes, int iAno, int &iResult)
{
	try
	{
		Log(LOG_FUNCTIONS, "Ivr::DbHoliday()");
		return 0;
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::DbHoliday()");
		return -1;
	}
}


void Ivr::PushCdrDetail(int iEvento, string sDataInicio, string sDataFim)
{
	try {

		ST_CDR_DETAIL StCdrDetail;

		StCdrDetail.iEvento			=	iEvento;
		StCdrDetail.sDataInicio	=	sDataInicio;
		StCdrDetail.sDataFim		=	sDataFim;

		QueueCdrDetail.push(StCdrDetail);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::PushCdrDetail()");
	}
}

void Ivr::LuaIniciaCdr(void)
{
	try {
	
		#if defined _TELESP_CELULAR || _GRADIENTE
			DbOpenCdr(); // Inicia o CDR
		#endif
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::LuaIniciaCdr()");
	}
}

int Ivr::GetLogCDREnabled(void)
{
    return m_pSwitch->GetLogCDR();
}



void Ivr::SetIconStatus(int Status)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    std::stringstream ss;
    int l_IndexScreen;

	try	
    {
        l_IndexScreen = m_pSwitch->GetIndexScreen();

        ss << boost::format("SS_SET_CHANNEL_STATUS?ID=%d&TRUNK=%d&CHANNEL=%d&STATUS=%s\r\n") %
             (l_IndexScreen + IdBoard) % BoardTrunk % BoardPort % m_pSwitch->StatusChannel( Status );

        m_pSwitch->GetThrListen()->SendMessageToAllScreens(ss.str());
	}
	catch(...)
	{
		m_pLog4cpp->debug("EXCEPTION: Ivr::SetIconStatus()");
	}
}

void Ivr::T_Blocked(void)
{
	try {

		FlagBlocked = true;

		Print(IVR_STATUS, STR_BLOCKED);
		Print(IVR_CALL_TYPE,	"");
		Print(IVR_DNIS,				"");
		Print(IVR_ANI,				"");
		Print(IVR_SERVICE,		"");

		SetIconStatus(ICON_BLOCKED); // amarelo

		// Guarda o timedate do inicio do bloqueio do canal.
		ChannelStat::Instance().SetLastActivityTime( Channel, time(NULL) );
		// Contabiliza o numero de canais ocupados.
		ChannelStat::Instance().DecrementBusyChannelCount();
		// Seta o status da porta no banco de dados para bloqueado
		DbSetPortStatus(PORT_BLOCKED, Channel);
	}
	catch(...)
	{
		m_pLog4cpp->debug("EXCEPTION: Ivr::T_Blocked()");
		GoEvents();
	}
}

void Ivr::T_DropCall(void)
{
	try {

		if( FlagBlocked )
		{
			// Se o canal esta bloqueado ignora os eventos.
			Log( LOG_FUNCTIONS, "Channel blocked. Event ignored." );
			return;
		}

		Print(IVR_STATUS, STR_IDLE);
			
		Print(IVR_CALL_TYPE,	"");
		Print(IVR_DNIS,				"");
		Print(IVR_ANI,				"");
		Print(IVR_SERVICE,		"");

		SetIconStatus(ICON_IDLEIN);

		// Seta o estado da porta no banco de dados para livre
		DbSetPortStatus(PORT_FREE_AVAILABLE, Channel);
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::T_DropCall()");
	}
}

void Ivr::T_Idle(void)
{
	try {

		FlagBlocked = false;

		Print(IVR_STATUS, STR_IDLE);
			
		Print(IVR_CALL_TYPE,	"");
		Print(IVR_DNIS,				"");
		Print(IVR_ANI,				"");
		Print(IVR_SERVICE,		"");

		SetIconStatus(ICON_IDLEIN);

		// Seta o estado da porta no banco de dados para livre
		// Guarda o timedate do inicio do desbloqueio do canal.
		ChannelStat::Instance().SetLastActivityTime( Channel, time(NULL) );

		DbSetPortStatus(PORT_FREE_AVAILABLE, Channel);
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::T_Idle()");
	}
}

void Ivr::T_Connect(void)
{
	try {

		Log(LOG_FUNCTIONS, "Ivr::T_Connect()");

		if( FlagBlocked )
		{
			// Se o canal esta bloqueado ignora os eventos.
			Log( LOG_FUNCTIONS, "Channel blocked. Event ignored." );
			return;
		}

		FilePosToPlayHoldMusic = 0;

		Print(IVR_STATUS, STR_OFFERED);
		SetIconStatus(ICON_OFFERED);

        if(m_pSwitch->GetLogCDR())    
        {            
		    DbReadCallId();
        }

		// Guarda o timedate do atendimento da chamada.
		ChannelStat::Instance().SetLastActivityTime( Channel, time(NULL) );
		// Contabiliza o numero de chamadas atendidas.
		ChannelStat::Instance().IncrementCallsConnectedCount( Channel );
		// Contabiliza o numero de canais ocupados.
		ChannelStat::Instance().IncrementBusyChannelCount();
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::T_Connect()");
		GoEvents();
	}
}

void Ivr::Block(void)
{
	try {

		Log(LOG_FUNCTIONS, "Ivr::Block()");

		if( m_pCti->SetChannelState(CHANNEL_OUT_OF_SERVICE) )
		{
			Log(LOG_ERROR, "%s", m_pCti->GetLastError());
			return;
		}

		FlagBlocked = true;

		T_Blocked();
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::Block()");
	}
}

void Ivr::UnBlock(void)
{
	try {
        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
		

		if( m_pCti->SetChannelState(CHANNEL_IN_SERVICE) )
		{
			Log(LOG_ERROR, "%s", m_pCti->GetLastError());
			return;
		}

		FlagBlocked = false;

		T_DropCall();
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::UnBlock()");
	}
}

void Ivr::LuaPrintStatus(const char *sStatus)
{
	try {
	
		Print(IVR_SERVICE, sStatus);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::LuaPrintStatus()");
	}
}

void Ivr::LuaPrintAni(const char *sStatus)
{
	try {
	
		Print(IVR_ANI, sStatus);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::LuaPrintStatus()");
	}
}

void Ivr::LuaPrintDnis(const char *sStatus)
{
	try {
	
		Print(IVR_DNIS, sStatus);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::LuaPrintStatus()");
	}
}

void Ivr::LuaPrintBits(const char *sStatus)
{
	try {
	
		Print(IVR_R2BITS, sStatus);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::LuaPrintStatus()");
	}
}

void Ivr::CloseChannel(void)
{
	try 
    {
        m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);		

		// Fecha o canal
		m_pCti->CloseChannel();
		// Seta o estado da porta no banco de dados para nao inicializado
		DbSetPortStatus(PORT_NOT_INIT, Channel);

#ifdef _VXML
		if(VXMLBrowser != NULL)
		{
			VXMLBrowser->Terminate();
			delete VXMLBrowser;
		}
#endif	// _VXML

	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::CloseChannel()");
	}
}

void Ivr::GetInfoConnection(void)
{
	try {

		Log(LOG_FUNCTIONS, "Ivr::GetInfoConnection(void)");

		// Pega informacoes sobre a conexao destino
        if( m_pSwitch->GetConnection()->Connection_Get(Channel, Connection) == false)
        {
			Log(LOG_CTI, "Event: There is no information for Connection");
		}

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection.Switch      :%d",	Connection.Switch);
		Log(LOG_INFO,"Connection.Machine     :%d",	Connection.Machine);
		Log(LOG_INFO,"Connection.Channel     :%d",	Connection.Channel);
		Log(LOG_INFO,"Connection.Timeslot    :%d",	Connection.Timeslot);
		Log(LOG_INFO,"Connection.sData       :%s",	Connection.sData.c_str());

		Log(LOG_INFO, " ");
	}
	catch(...)
    {
		Log(LOG_ERROR, "EXCEPTION: Ivr::GetInfoConnection(void)");
	}
}


void Ivr::GetInfoConnection(int Event)
{
	try {

		Log(LOG_FUNCTIONS, "Ivr::GetInfoConnection(int Event)");

		ClearConn(Connection);

		if( Event == -1 )
		{

			// Limpa todos as informacoes de eventos da fila.
            while( m_pSwitch->GetConnection()->Connection_Get( Channel, Connection ,Event ) )
			{
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
				//Sleep( 1 );
			}

			return;
		}
		
		// Pega informacoes sobre a conexao destino
		if( m_pSwitch->GetConnection()->Connection_Get(Channel, Connection, Event) == false)
        {
			Log(LOG_ERROR, "Event: There is no information for Connection");
			return;
		}

		
		for(;;){
			// Se o evento retornado e diferente do desejado descarta a informacao
			if(Connection.Event != Event){
				// Pega informacoes sobre a conexao destino
				if(m_pSwitch->GetConnection()->Connection_Get(Channel, Connection) == false)
                {
					Log(LOG_ERROR, "Event: There is no information for Connection");
					return;
				}
				Sleep(1);
				continue;
			}
			break;
		}
		

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection.Switch      :%d",	Connection.Switch);
		Log(LOG_INFO,"Connection.Machine     :%d",	Connection.Machine);
		Log(LOG_INFO,"Connection.Channel     :%d",	Connection.Channel);
		Log(LOG_INFO,"Connection.Timeslot    :%d",	Connection.Timeslot);
		Log(LOG_INFO,"Connection.sData       :%s",	Connection.sData.c_str());

		Log(LOG_INFO, " ");

	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::GetInfoConnection()");
	}
}


void Ivr::SendIvrEvent(int iChannel)
{
	try {

		HANDLE hSem	=	MapSemaphore[iChannel];

		ReleaseSemaphore(hSem, 1, NULL);
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::SendIvrEvent()");
	}
}

int Ivr::DbCdr( const char  *sAppName , const char * sCdrInfo)
{
    std::stringstream ss;
    std::string stdPathCdr;    
    std::string stdPathDate;

    m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);		

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::DbCdr()");

		int index, n;
		char sPathDate[32], sIndex[4];
        
		string sFilename, sFileIndex;
		FILE *IndexFile;
		struct tm *timer;
		time_t ltime;

        if ( m_pSwitch->GetCdr()->MapLogCdr.count( sAppName )  <= 0 ) 		
		{
			EnterCriticalSection( &m_pSwitch->GetCdr()->CritLogCdr );

			if ( m_pSwitch->GetCdr()->MapLogCdr.count( sAppName )  <= 0 )
			{
                ss << boost::format("%s\\CDR\\%s") % m_pSwitch->GetBasePathLog().c_str() % sAppName;
                stdPathCdr.assign( ss.str().c_str() );

                ss.str(std::string() );
                ss << boost::format("%s\\index.txt") % stdPathCdr;
                sFileIndex.assign( ss.str() );

				if(_access( sFileIndex.c_str(), 0) != 0 )		//o arquivo n�o existe
				{
                    ss.str(std::string());
                    ss << boost::format("%s\\CDR") % m_pSwitch->GetBasePathLog().c_str();
                    stdPathCdr.assign(ss.str());
					
                    if(_access(stdPathCdr.c_str(), 0) != 0)		//o diret�rio ../CDR n�o existe
                    {                        
                        if(_mkdir(stdPathCdr.c_str()))
						{							
                            Log(LOG_ERROR, "Ivr::DbCdr(): N�o conseguiu criar o diret�rio %s", stdPathCdr.c_str());
							return -1;
						}
                    }

                    ss.str(std::string());
                    ss << boost::format("%s\\CDR\\%s") % m_pSwitch->GetBasePathLog().c_str() % sAppName;
                    stdPathCdr.assign(ss.str());
					
                    if(_access(stdPathCdr.c_str(), 0) != 0)		//o diret�rio ..\CDR\nome_da_aplicacao n�o existe
                    {                        
                        if(_mkdir(stdPathCdr.c_str()))
						{							
                            Log(LOG_ERROR, "Ivr::DbCdr(): N�o conseguiu criar o diretorio %s", stdPathCdr.c_str());
							return -1;
						}
                    }
					index = 1;
				}
				else
				{					
                    IndexFile = fopen(sFileIndex.c_str(),"r+t");
					n = fread(sIndex, sizeof(sIndex), 1, IndexFile);
					index = atoi(sIndex);
					fclose(IndexFile);
					index = index + 1;
				}

				time(&ltime);
				timer = localtime(&ltime);
                ss.str(std::string());
                ss << boost::format("\\%04d-%02d-%02d") % (timer->tm_year + 1900) % (timer->tm_mon + 1) % timer->tm_mday;
                stdPathDate.assign(ss.str());

                ss.str(std::string());
                ss << boost::format("%s%s") % stdPathCdr.c_str() % stdPathDate.c_str();
                sFilename.assign(ss.str());
				
				if(_access( sFilename.c_str(), 0) != 0)		//o diret�rio n�o existe
				{
					if(_mkdir(sFilename.c_str()))
					{
						Log(LOG_ERROR, "Ivr::DbCdr(): N�o conseguiu criar o diret�rio %s", sFilename.c_str());
						return -1;
					}
					index = 1;
				}

				IndexFile = fopen(sFileIndex.c_str(),"wt");
				_itoa(index, sIndex, 10);
				n = fwrite(sIndex, sizeof(sIndex), 1, IndexFile); 
				fclose(IndexFile);

                ss.str(std::string());
                ss << boost::format("\\CDR_%04d.txt") % index;
                stdPathDate = ss.str();
                sFilename.append(stdPathDate);

                ss.str(std::string());
                ss << "\r\n";
                ss << "\t\t===================================================================================================\r\n";
                ss << "\t\t|                                                                                                  |\r\n";
                ss << "\t\t|                                          ( Ivr::DbCdr() )                                        |" << std::endl;
                ss << boost::format("\t\t|  CDR App Name   : %60s |\r\n") % sAppName;
                ss << boost::format("\t\t|  CDR Index File : %60s |\r\n") % sFileIndex.c_str();
                ss << boost::format("\t\t|  CDR File Name  : %48s |\r\n") % sFilename.c_str();
                ss << "\t\t|                                                                                                  |\r\n";
                ss << "\t\t===================================================================================================\r\n";
                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, ss.str().c_str() );                


                fflush( m_pSwitch->GetCdr()->MapLogCdr[sAppName].mp_outFile );
                fclose( m_pSwitch->GetCdr()->MapLogCdr[sAppName].mp_outFile );                        
                                                
                m_pSwitch->GetCdr()->MapLogCdr[sAppName].mp_outFile = fopen(sFilename.c_str() , "a+");
                m_pSwitch->GetCdr()->MapLogCdr[sAppName].m_filename = sFilename;
                m_pSwitch->GetCdr()->MapLogCdr[sAppName].ChangeFile = 0;
			}				

			LeaveCriticalSection( &m_pSwitch->GetCdr()->CritLogCdr );
		}

		EnterCriticalSection( &m_pSwitch->GetCdr()->CritLogCdr );

		if(m_pSwitch->GetCdr()->MapLogCdr[sAppName].ChangeFile)
			m_pSwitch->GetCdr()->MapLogCdr[sAppName].ChangeFile = 0;

        fwrite(sCdrInfo, sizeof(char), strlen(sCdrInfo) , m_pSwitch->GetCdr()->MapLogCdr[sAppName].mp_outFile );
        fflush( m_pSwitch->GetCdr()->MapLogCdr[sAppName].mp_outFile );        

        LeaveCriticalSection( &m_pSwitch->GetCdr()->CritLogCdr );
		

		return 0;
	}
	catch(...)
	{
		Log(LOG_ERROR, "EXCEPTION: Ivr::DbCdr()");
		return -1;
	}
}

bool Ivr::SendConnection(int Event)
{
	try {

		Log(LOG_FUNCTIONS, "Ivr::SendConnection(int Event)");

		// Informacoes da conexao origem (canal que esta atendendo o usuario)
        Connection_local.Switch			= m_pSwitch->GetSwitch();
		Connection_local.Machine		= m_pSwitch->GetMachine();
		Connection_local.Channel		= Channel;
		Connection_local.Timeslot		= TxTimeSlot;
		Connection_local.Event			= Event;
		Connection_local.sData.erase();

		// Informacoes da conexao destino (estacao da atendente)
		Connection_remote.Switch		= Connection.Switch;
		Connection_remote.Machine		= Connection.Machine;
		Connection_remote.Channel		= Connection.Channel;
		Connection_remote.Timeslot	=	Connection.Timeslot;
		Connection_remote.Event			= Event;
		Connection_remote.sData.erase();

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection_local.Switch      :%d",	Connection_local.Switch);
		Log(LOG_INFO,"Connection_local.Machine     :%d",	Connection_local.Machine);
		Log(LOG_INFO,"Connection_local.Channel     :%d",	Connection_local.Channel);
		Log(LOG_INFO,"Connection_local.Timeslot    :%d",	Connection_local.Timeslot);
		Log(LOG_INFO,"Connection_local.sData       :%s",	Connection_local.sData.c_str());

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection_remote.Switch     :%d",	Connection_remote.Switch);
		Log(LOG_INFO,"Connection_remote.Machine    :%d",	Connection_remote.Machine);
		Log(LOG_INFO,"Connection_remote.Channel    :%d",	Connection_remote.Channel);
		Log(LOG_INFO,"Connection_remote.Timeslot   :%d",	Connection_remote.Timeslot);
		Log(LOG_INFO,"Connection_remote.sData      :%s",	Connection_remote.sData.c_str());

		Log(LOG_INFO, " ");

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
        return m_pSwitch->GetConnection()->Connection_Send(Connection_remote, Event, Connection_local);
	}
	catch(...)
    {
		Log(LOG_ERROR, "EXCEPTION: Ivr::SendConnection(int Event)");
		return false;
	}
}

bool Ivr::SendConnection(int Event, const std::string & sData)
{
    std::stringstream ss;
    EventName<std::stringstream> EvtName;
	try {
        ss << boost::format("Ivr::SendConnection(%s,%s)") % EvtName.GetEventName(Event).str().c_str() % sData.c_str();
        Log(LOG_FUNCTIONS,  ss.str().c_str() );

		// Informacoes da conexao origem (canal que esta atendendo o usuario)
		Connection_local.Switch			= m_pSwitch->GetSwitch();
		Connection_local.Machine		= m_pSwitch->GetMachine();
		Connection_local.Channel		= Channel;
		Connection_local.Timeslot		= TxTimeSlot;
		Connection_local.Event			= Event;
		Connection_local.sData			= sData;

		// Informacoes da conexao destino (estacao da atendente)
		Connection_remote.Switch		= Connection.Switch;
		Connection_remote.Machine		= Connection.Machine;
		Connection_remote.Channel		= Connection.Channel;
		Connection_remote.Timeslot	=	Connection.Timeslot;
		Connection_remote.Event			= Event;
		Connection_remote.sData			= sData;

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection_local.Switch      :%d",	Connection_local.Switch);
		Log(LOG_INFO,"Connection_local.Machine     :%d",	Connection_local.Machine);
		Log(LOG_INFO,"Connection_local.Channel     :%d",	Connection_local.Channel);
		Log(LOG_INFO,"Connection_local.Timeslot    :%d",	Connection_local.Timeslot);
		Log(LOG_INFO,"Connection_local.sData       :%s",	Connection_local.sData.c_str());

		Log(LOG_INFO, " ");

		Log(LOG_INFO,"Connection_remote.Switch     :%d",	Connection_remote.Switch);
		Log(LOG_INFO,"Connection_remote.Machine    :%d",	Connection_remote.Machine);
		Log(LOG_INFO,"Connection_remote.Channel    :%d",	Connection_remote.Channel);
		Log(LOG_INFO,"Connection_remote.Timeslot   :%d",	Connection_remote.Timeslot);
		Log(LOG_INFO,"Connection_remote.sData      :%s",	Connection_remote.sData.c_str());

		Log(LOG_INFO, " ");

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		return m_pSwitch->GetConnection()->Connection_Send(Connection_remote, Event, Connection_local);
	}
	catch(...)
    {
		Log(LOG_ERROR, "EXCEPTION: Ivr::SendConnection(int Event, string sData)");
		return false;
	}
}

void Ivr::ProcessScriptReturn(void)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try 
    {
		// 1 = usuario desligado pelo script
		// 2 = usuario desligado pela posicao de atendimento
		// 3 = usuario desligou
		if(m_pCti->AlreadyDisconnected())
			HangupReason = 3;
		else
			HangupReason = 1;
				
        // Desliga a ligacao
		Hangup();
		Print(IVR_STATUS, STR_IDLE);
		SetIconStatus(ICON_IDLEIN);
	}
	catch(...)
    {
		Log(LOG_ERROR, "EXCEPTION: Ivr::ProcessScriptReturn()");
	}
}

void Ivr::Hangup(void)
{
	try 
    {
        m_pLog4cpp->debug("[%p][%s]", this , __FUNCTION__);		

        Log(LOG_FUNCTIONS, "Ivr::Hangup()");

		if( FlagBlocked )
		{
			// Se o canal esta bloqueado ignora os eventos.
            Log( LOG_FUNCTIONS, "Channel blocked. Event ignored." );
			return;
		}

		if(FlagHangup)
		{
            Log(LOG_FUNCTIONS, "Hangup(): Call already disconnected");
			return;
		}

#ifdef _CONFERENCE
    // Verifica se o canal esta em uma conferencia.
		if( ConferenceId != "" )
		{
			Log(LOG_ERROR, "Hangup(): Removing channel from conference id %s.", ConferenceId.c_str() );
			// Remove o canal da conferencia.
			ConferenceRemove( ConferenceId.c_str(), ConfereeType );
		}
#endif // _CONFERENCE

		// Para o canal de voz
		m_pCti->StopCh();
		// Limpa todos as informacoes de eventos da fila.
		GetInfoConnection( -1 );

		if( (HangupReason <= 0) && (m_pCti->AlreadyDisconnected()) ){
			// Se a ligacao ja foi desligada e nao existe nenhuma razao para 
			// a desconexao, seta uma.
			// 1 = usuario desligado pelo script
			// 2 = usuario desligado pela posicao de atendimento
			// 3 = usuario desligou
			HangupReason = 3;
		}

		FlagHangup = true;

		// Guarda a data de fim da ligacao
		GetDatetime(sEndDateOfCall);

		// Guarda o tempo em segundos de fim da ligacao
		TimeOfCall = time(NULL) - TimeOfCall;

		// Grava o registro de CDR        
        if ( m_pSwitch->GetLogCDR() )
        {
            DbCdr( m_pSwitch->GetAppName().c_str(), "HANGUP - CDR" );
        }

        
		SetIconStatus(ICON_DROPCALL);
		// Desliga a ligacao
		if( m_pCti->Disconnect() )
		{
			m_pLog4cpp->error( "%s", m_pCti->GetLastError());
		}
        

		Print(IVR_STATUS," ");
		Print(IVR_CALL_TYPE," ");
		Print(IVR_DNIS," ");
		Print(IVR_ANI," ");
		Print(IVR_SERVICE," ");

		Print(IVR_STATUS, STR_IDLE);
		SetIconStatus(ICON_IDLEIN);

        m_pLog4cpp->debug("[%p][%s] Going to call DbSetPortStatus as 'F'", this , __FUNCTION__);		        
		// Seta o estado da porta no banco de dados para livre
		DbSetPortStatus(PORT_FREE_AVAILABLE, Channel);
        
        /*
		if( m_pCti->GetChannelStatus() == CHANNEL_BLOCKED )
        {
			// Bloqueia o canal

			Print(IVR_STATUS,			STR_BLOCKED);
			Print(IVR_CALL_TYPE,	"");
			Print(IVR_DNIS,				"");
			Print(IVR_ANI,				"");
			Print(IVR_SERVICE,		"");

			SetIconStatus(ICON_BLOCKED); // amarelo

			// Seta o status da porta no banco de dados para bloqueado
			DbSetPortStatus(PORT_BLOCKED, Channel);
		}
		else
		{
			// Toca tom de ocupado
			PlayBusyToneAsync();
		}
        */
        PlayBusyToneAsync();

		// Guarda o timedate do desligamento da chamada.
		ChannelStat::Instance().SetLastActivityTime( Channel, time(NULL) );
		// Contabiliza o numero de canais ocupados.
		ChannelStat::Instance().DecrementBusyChannelCount();

		//GoEvents();
	}
	catch(...)
	{
        m_pLog4cpp->error("EXCEPTION: Ivr::Hangup()");
		GoEvents();
	}
}

#define TIME_OUT  3

/************************************************************************************************
	 Método: ConferenceAdd ()

	 Descr.:  Adiciona usuário em uma conferência ou cria conferência (caso não exista) e adiciona
	 Param:   - ID da conferência
	          - Número de usuários máximo da conferência
						- Tipo de conferência
						- NOTIFYTONE ou não
	 Retorno: 0 - OK																																							
*************************************************************************************************/
int Ivr::ConferenceAdd( const char* lpcszConferenceId, const int& iConferenceSize, const int& iConfereeType, const bool& bNotifyTone )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    long lTimeSlot = -1;
    long lTimeSlotRing = -1;
    long lTimeSlotVoxRing = -1;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceAdd()");

        if( lpcszConferenceId[0] == '\0' )
		{
			return 1;
		}

        /*
        if( ConferenceId != "" )
		{
			return 0;
		}
        */

		// Preenchimento da estrutura da SendConnection
		// Envia pacote com o comando e dados para a thread de processamento 
		// de eventos da placa (ex: Thread_Conference em conference_manager.cpp)
		// 
		// Estrutura com dados do destino. 
		// Neste caso, a própria máquina e canal = 0, pois vai para a conferência
        ConnectionParam(lpcszConferenceId);

        memset(pActionBuffer,0x00,sizeof(pActionBuffer));

		// Máquina Remota
        if ( m_pSwitch->GetMachine() != Connection.Machine )		
		{	

			if( iConfereeType == CP_VOX )
			{
				// Guarda o Timeslot de exportação global
				lTimeSlotVoxRing = m_pSwitch->GetTS_Manager()->AllocateGlobalTs(TxTimeSlotVox);
				Log(LOG_CTI, "Connecting MC3->CTbus. MC3 TX ts: %d / CTbus RX ts: %d", lTimeSlotVoxRing, TxTimeSlotVox);
			}
			else
			{
				lTimeSlotVoxRing = TxTimeSlotVox;
			}

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_ADD"
									"&CHANNEL=%d"
									"&CONFERENCE_ID=%s"
									"&CONFERENCE_SIZE=%d"
									"&CONFEREE_TYPE=%d"
									"&NOTIFYTONE=%d"
									"&TIMESLOTVOX=%ld"
									"&TIMESLOT=%ld",
									Channel,
									lpcszConferenceId,
									iConferenceSize,
									iConfereeType,
									(bNotifyTone) ? 1: 0,
									lTimeSlotVoxRing,
									TxGlobalTimeSlot);
		}
		else
		{
			sprintf (pActionBuffer, "?ACTION=CONFERENCE_ADD"
									"&CHANNEL=%d"
									"&CONFERENCE_ID=%s"
									"&CONFERENCE_SIZE=%d"
									"&CONFEREE_TYPE=%d"
									"&NOTIFYTONE=%d"
									"&TIMESLOTVOX=%ld"
									"&TIMESLOT=%ld",
									Channel,
									lpcszConferenceId,
									iConferenceSize,
									iConfereeType,
									(bNotifyTone) ? 1: 0,
									TxTimeSlotVox,
									TxTimeSlot);
		}	
		
        Connection.sData.assign(pActionBuffer);        

        Log(LOG_CTI,"SendConnection T_SW_CONF_REQUEST : %s", Connection.sData.c_str() );
		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
			return 0;	// retorna erro para o script


        iTimeInit = time(NULL);

		// Após o envio, fica no aguardo dos eventos de resposta
		for(;;)
		{
		
			Event = WaitEvent();

			switch(Event)
			{

				case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }
                break;
          
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
					    bFlagDisconnect = true;
				  
                    break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");
					
                    // Guarda o id da conferencia para remover no hangup.
					ConferenceId = lpcszConferenceId;
					ConfereeType = iConfereeType;
					GetInfoConnection(Event);

                    // >>> Ricardo Hayama 09/08/2003
                    // Utilizado para adicionar o timeslot de vox na conferencia e não ouvir o timeslot de retorno da conferencia
                    // Quando o tipo da conferencia for CP_VOX não deve ouvir o timeslot que retornou, mais sim o timeslot da conferencia
                    // anterior.
                    if( iConfereeType == CP_VOX )
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 1;
                    }

                    //Recebe o timeslot da conferência
                    TimeSlotConference = Connection.Timeslot;

					// Máquina Remota
                    if ( m_pSwitch->GetMachine() != Connection.Machine )							
					{
                        lTimeSlotRing = TimeSlotConference;
                        // Aloca um timeslot no bus para conectar com o timeslot do ring
                        lTimeSlot = m_pSwitch->GetTS_Manager()->AllocateLocalTs(lTimeSlotRing);
						Log(LOG_CTI, "Connecting CTbus->MC3. CTbus TX ts: %d / MC3 RX ts: %d", lTimeSlot, lTimeSlotRing);

                        //Recebe o timeslot da conferência
                        TimeSlotConference = Connection.Timeslot = lTimeSlot;
                    }
        
                    ListenConnection();

                    if( iConfereeType == CP_PERS )
                    {
			            // Inicializa o timeslot c/ valor invalido.
						lTimeSlot = -1;
						// Se o conferencista for personalidade(CP_PERS) obtem o timeslot
                        // de monitoracao retornado pela thread de conferencia.
                        if(GetDataPacket(Connection.sData.c_str(), "MONITOR_TIMESLOT", lTimeSlot))
			            {
				            Log(LOG_ERROR, "There is no information in tag MONITOR_TIMESLOT");
			            }
            
                        if( lTimeSlot < 0 )
			            {
				            Log(LOG_ERROR, "Invalid timeslot (ts=%ld) returned by MONITOR_TIMESLOT tag.", lTimeSlot);
			            }
                        else
                        {
						    // Máquina Remota
                            if ( m_pSwitch->GetMachine() != Connection.Machine )		                            
                            {
                                lTimeSlotRing = lTimeSlot;
                                // Aloca um timeslot no bus para conectar com o timeslot do ring
                                lTimeSlot = m_pSwitch->GetTS_Manager()->AllocateLocalTs(lTimeSlotRing);
							    Log(LOG_CTI, "Connecting CTbus->MC3. CTbus TX ts: %d / MC3 RX ts: %d", lTimeSlot, lTimeSlotRing);
                            }

                            // Faz o recurso de voz ouvir o timeslot de monitoracao da conferencia p/
                            // permitir a gravacao da mesma.                            
                            m_pCti->VoxScListen(lTimeSlot);
                        }
                    }
					
					if( bFlagDisconnect )
						return -1;
					else
					    return 1;

	            case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					
                    if( bFlagDisconnect )
						return -1;
					else
					    return 0;

#ifdef _APPLICATION_SOCKET
                    // Trata os eventos do monitor
                case T_SW_CONF_MONITOR_REQUEST:
                    CTI->SendEvent(Channel, Event);
					
                    break;
#endif

Log(LOG_FUNCTIONS, "Ivr::ConferenceAdd() 4 ");
		    } //switch
        } // for
	} // try
	catch (...)
	{
	    Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceAdd()");
	    return 0;
	}
}



int Ivr::GetDataPacket(const char *datapacket, char *cCommand, long &iValue)
{
  string data, valor;
  int pos_ini, pos_fim;

  try {

		data = datapacket;

		pos_ini = data.find(cCommand, 0);

		if( pos_ini == -1 ){
			iValue = 0;
			return -1;
		}

		pos_ini += (strlen(cCommand) + 1);

		pos_fim = data.find("&", pos_ini);

		iValue = atoi( data.substr(pos_ini, pos_fim-pos_ini).c_str() );

		return 0;
	}
	catch(...){
		iValue = 0;
		Log(LOG_FUNCTIONS, "EXCEPTION: GetDataPacket(const char *datapacket, char *cCommand, long &iValue)");
		return -1;
	}
}

int Ivr::ConferenceRemove( const char* lpcszConferenceId, const int& iConfereeType )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    long lTimeSlot = -1;
    long lTimeSlotRing = -1;
    long lMonitorTimeSlot = -1;
    long lMonitorTimeSlotRing = -1;
    long lTimeSlotVoxRing = -1;
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceRemove()");

		if( lpcszConferenceId[0] == '\0' )
		{
			return 1;
		}
    
        if( ConferenceId == "" )
		{
			return 0;
		}

		// Estrutura com dados do destino. Neste caso, a própria máquina
        ConnectionParam(lpcszConferenceId);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {

			if( iConfereeType == CP_VOX )
			{
				// Guarda o Timeslot de exportação global
				lTimeSlotVoxRing = m_pSwitch->GetTS_Manager()->MapGlobalTs( TxTimeSlotVox );
				
				if( m_pSwitch->GetTS_Manager()->ReleaseGlobalTs( TxTimeSlotVox ) == 1 )
				{
					Log(LOG_CTI, "Disconnecting MC3. MC3 TX ts: %d", lTimeSlotVoxRing);
				}
			}
			else
			{
				lTimeSlotVoxRing = TxTimeSlotVox;
			}

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_REMOVE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=%d"
                                    "&TIMESLOTVOX=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    iConfereeType,
                                    lTimeSlotVoxRing,
                                    TxGlobalTimeSlot);
        }
		else
        {
            sprintf (pActionBuffer, "?ACTION=CONFERENCE_REMOVE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=%d"
                                    "&TIMESLOTVOX=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    iConfereeType,
                                    TxTimeSlotVox,
                                    TxTimeSlot);
        }
		
		Connection.sData		= pActionBuffer;		

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
			return 0;	// retorna erro para o script

		iTimeInit = time(NULL);

        //Trata eventos de resposta
		for(;;)
		{		
            Event = WaitEvent();

			switch(Event)
			{
				case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
					        return -1;
                        else
					        return 0;
                    }

			        break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
					  bFlagDisconnect = true;
				  break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");
					
					GetInfoConnection(Event);
          
					// Máquina Remota
                    if( m_pSwitch->GetMachine() != Connection.Machine )
                    {
                        lTimeSlotRing = Connection.Timeslot;
                        lTimeSlot = m_pSwitch->GetTS_Manager()->MapLocalTs(lTimeSlotRing);

                        if( m_pSwitch->GetTS_Manager()->ReleaseLocalTs(lTimeSlotRing) )
						{
							Log(LOG_CTI, "Disconnecting CTbus. CTbus TX ts: %d", lTimeSlot);
						}

						if( iConfereeType == CP_PERS )
						{
							// Inicializa o timeslot c/ valor invalido.
							lTimeSlot = -1;
							// Se o conferencista for personalidade(CP_PERS) obtem o timeslot
							// de monitoracao retornado pela thread de conferencia.
							if(GetDataPacket(Connection.sData.c_str(), "MONITOR_TIMESLOT", lMonitorTimeSlotRing))
							{
								Log(LOG_ERROR, "There is no information in tag MONITOR_TIMESLOT");
							}
            
							if( lMonitorTimeSlotRing < 0 )
							{
								Log(LOG_ERROR, "Invalid timeslot (ts=%ld) returned by MONITOR_TIMESLOT tag.", lTimeSlot);
							}
							else
							{
								lMonitorTimeSlot = m_pSwitch->GetTS_Manager()->MapLocalTs(lMonitorTimeSlotRing);

								if( m_pSwitch->GetTS_Manager()->ReleaseLocalTs(lMonitorTimeSlotRing) )
								{
									Log(LOG_CTI, "Disconnecting CTbus. CTbus TX ts: %d", lMonitorTimeSlot);
								}
							}
						}
                    }

                    // >>> Ricardo Hayama Reis 11/08/2003
                    if( iConfereeType != CP_VOX )
                        m_pCti->ScConnect(FULLDUP);
                    // <<< Ricardo Hayama Reis 11/08/2003

					// Limpa o id da conferencia.
					ConferenceId = "";

					if( bFlagDisconnect )
						return -1;
					else
					    return 1;

				case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					if( bFlagDisconnect )
                        return -1;
					else
				        return 0;

#ifdef _APPLICATION_SOCKET
        // Trata os eventos do monitor
        case T_SW_CONF_MONITOR_REQUEST:
          CTI->SendEvent(Channel, Event);
					break;
#endif

            }
		}
	}
	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceRemove()");
		return 0;
	}
}

int Ivr::ConferenceChange( const char* lpcszConferenceIdSource, const char* lpcszConferenceIdDestination, const int& iConferenceSize, const int& iConfereeType, const bool& bNotifyTone )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    long lTimeSlot = -1;
    long lTimeSlotRing = -1;
    long lTimeSlotVoxRing = -1;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceChange()");

		// Preenchimento da estrutura da SendConnection
		// Envia pacote com o comando e dados para a thread de processamento 
		// de eventos da placa (ex: Thread_Conference em conference_manager.cpp)
		// 
		// Estrutura com dados do destino. 
		// Neste caso, a própria máquina e canal = 0, pois vai para a conferência
        ConnectionParam(lpcszConferenceIdSource);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {
			if( iConfereeType == CP_VOX )
			{
				// Guarda o Timeslot de exportação global
                lTimeSlotVoxRing = m_pSwitch->GetTS_Manager()->MapGlobalTs(TxTimeSlotVox);
				Log(LOG_CTI, "Connecting MC3->CTbus. MC3 TX ts: %d / CTbus RX ts: %d", lTimeSlotVoxRing, TxTimeSlotVox);
			}
			else
			{
				lTimeSlotVoxRing = TxTimeSlotVox;
			}

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_CHANGE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID_SOURCE=%s"
                                    "&CONFERENCE_ID_DESTINATION=%s"
                                    "&CONFERENCE_SIZE=%d"
                                    "&CONFEREE_TYPE=%d"
                                    "&NOTIFYTONE=%d"
                                    "&TIMESLOTVOX=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceIdSource,
                                    lpcszConferenceIdDestination,
                                    iConferenceSize,
                                    iConfereeType,
                                    (bNotifyTone) ? 1: 0,
                                    lTimeSlotVoxRing,
                                    TxGlobalTimeSlot);
		}
		else
		{
            sprintf (pActionBuffer, "?ACTION=CONFERENCE_CHANGE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID_SOURCE=%s"
                                    "&CONFERENCE_ID_DESTINATION=%s"
                                    "&CONFERENCE_SIZE=%d"
                                    "&CONFEREE_TYPE=%d"
                                    "&NOTIFYTONE=%d"
                                    "&TIMESLOTVOX=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceIdSource,
                                    lpcszConferenceIdDestination,
                                    iConferenceSize,
                                    iConfereeType,
                                    (bNotifyTone) ? 1: 0,
                                    TxTimeSlotVox,
                                    TxTimeSlot);
		}
		
		Connection.sData		= pActionBuffer;

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
		    return 0;	// retorna erro para o script

		iTimeInit = time(NULL);

        // Após o envio, fica no aguardo dos eventos de resposta
		for(;;)
		{
			Event = WaitEvent();

			switch(Event)
			{
				case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }

				    break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
					    bFlagDisconnect = true;
				    break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");

                    ConferenceId = lpcszConferenceIdDestination;
					
                    GetInfoConnection(Event);

                    // >>> Ricardo Hayama 09/08/2003
                    // Utilizado para adicionar o timeslot de vox na conferencia e não ouvir o timeslot de retorno da conferencia
                    // Quando o tipo da conferencia for CP_VOX não deve ouvir o timeslot que retornou, mais sim o timeslot da conferencia
                    // anterior.
                    if( iConfereeType == CP_VOX )
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 1;
                    }

					// Máquina Remota
                    if( m_pSwitch->GetMachine() != Connection.Machine )
                    {
                        // Desconecta o timeslot alocado da conferencia anterior.
                        if(GetDataPacket(Connection.sData.c_str(), "SOURCE_TIMESLOT", lTimeSlot))
			            {
				            Log(LOG_ERROR, "There is no information in tag SOURCE_TIMESLOT");
			            }

                        Log(LOG_CTI, "Disconnecting CTbus. CTbus TX ts: %d", TimeSlotConference);

                        lTimeSlotRing = Connection.Timeslot;
                        
                        // Aloca um timeslot no bus para conectar com o timeslot do ring
                        lTimeSlot = m_pSwitch->GetTS_Manager()->AllocateLocalTs(lTimeSlotRing);

						Log(LOG_CTI, "Connecting CTbus->MC3. CTbus TX ts: %d / MC3 RX ts: %d", lTimeSlot, lTimeSlotRing);

                        // Troca o timeslot da conferencia de global (MC3) p/ local (CTbus).
                        Connection.Timeslot = lTimeSlot;
                    }

                    //Recebe o timeslot da conferência
                    TimeSlotConference = Connection.Timeslot;

                    ListenConnection();
			
					if( bFlagDisconnect )
					    return -1;
					else
					    return 1;

                case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					
                    if( bFlagDisconnect )
						return -1;
					else
					    return 0;

#ifdef _APPLICATION_SOCKET
        // Trata os eventos do monitor
        case T_SW_CONF_MONITOR_REQUEST:
          CTI->SendEvent(Channel, Event);
					break;
#endif

            }
		}
	}	
	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceChange()");
	    return 0;
	}
}


int Ivr::ConferenceListen( const char* lpcszConferenceId )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    long lTimeSlot = 0;
    long lTimeSlotRing = 0;
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceListen()");
		
		// Estrutura com dados do destino. Neste caso, a própria máquina
        ConnectionParam(lpcszConferenceId);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {	
			//Se DCB: manda para a placa colocar o canal como MSPA_NULL (ouve e fala na conferência)
		    sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=0"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    TxGlobalTimeSlot);

        }
        else
        {
            //Se DCB: manda para a placa colocar o canal como MSPA_NULL (ouve e fala na conferência)
		    sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=0"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    TxTimeSlot);
        }

		Connection.sData		= pActionBuffer;

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
            return 0;	// retorna erro para o script

		iTimeInit = time(NULL);

        //Trata eventos de resposta
		for(;;)
		{
		
			Event = WaitEvent();

			switch(Event)
			{
				case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }

					break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
                    {
					    bFlagDisconnect = true;
					}
				    break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");
          
                    GetInfoConnection(Event);
                    Connection.Timeslot = TimeSlotConference;

					Log(LOG_CTI, "Listening timeslot %d", TimeSlotConference);
					// Conecta o usuário na sala de conferência.
					ListenConnection();
					if( bFlagDisconnect )
						return -1;
					else
					    return 1;

				case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					if( bFlagDisconnect )
						return -1;
					else
					    return 0;

#ifdef _APPLICATION_SOCKET
                // Trata os eventos do monitor
                case T_SW_CONF_MONITOR_REQUEST:
                    CTI->SendEvent(Channel, Event);
					break;
#endif

			}
		}
	}	

	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceListen()");
		return 0;
	}
}


/************************************************************************************************
   Metodo: ConferenceUnlisten (const char* lpcszConferenceId)

   Descr.: Retira o canal da conferência: 
						1 - Muda o atributo para apenas ouvir a conferência
						2 - Faz o canal parar de ouvir a conferência

   Param:   [in] Índice de uma conferência no formato string

   Retorno: 0 - Erro, -1 - Canal desligou, 1 - Ok 
************************************************************************************************/
int Ivr::ConferenceUnlisten( const char* lpcszConferenceId )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    long lTimeSlot = 0;
    long lTimeSlotRing = 0;
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceUnlisten()");
		
		// Estrutura com dados do destino. Neste caso, a própria máquina
        ConnectionParam(lpcszConferenceId);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {

			//Se DCB: manda para a placa colocar o canal como MSPA_RO (só ouvir a conferência)
		    sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=1"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    TxGlobalTimeSlot);
			
		}
		else
		{
			//Se DCB: manda para a placa colocar o canal como MSPA_RO (só ouvir a conferência)
		    sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=1"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    TxTimeSlot);
		}

		Connection.sData		= pActionBuffer;

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
			return 0;	// retorna erro para o script

		iTimeInit = time(NULL);

        //Trata eventos de resposta
		for(;;)
		{
		
			Event = WaitEvent();

			switch(Event)
			{

                case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }

					break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
                    {
					    bFlagDisconnect = true;
					}
				    break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");
					GetInfoConnection(Event);

					// Conecta a interface de linha c/ o recurso de voz
                    m_pCti->ScConnect(FULLDUP);
					
                    if( bFlagDisconnect )
						return -1;
					else
					    return 1;

				case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					if( bFlagDisconnect )
						return -1;
					else
					    return 0;

#ifdef _APPLICATION_SOCKET
                // Trata os eventos do monitor
                case T_SW_CONF_MONITOR_REQUEST:
                    CTI->SendEvent(Channel, Event);
					break;
#endif

			}
		}
	}
	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceUnlisten()");
		return 0;
	}
}


int Ivr::ConferenceGetConfereeId( const char* lpcszConferenceId, const char* lpcszANI )
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    int iValue = 0;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    long lTimeSlotRing = 0;
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceGetConfereeId()");
		
		// Estrutura com dados do destino. Neste caso, a própria máquina
        ConnectionParam(lpcszConferenceId);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {	
			sprintf (pActionBuffer, "?ACTION=CONFERENCE_GETCONFEREEID"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&ANI=%s"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    lpcszANI,
                                    TxGlobalTimeSlot);

		}
		else
		{
			sprintf (pActionBuffer, "?ACTION=CONFERENCE_GETCONFEREEID"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&ANI=%s"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    lpcszANI,
                                    TxTimeSlot);
        }
		
		Connection.sData		= pActionBuffer;

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
			return -2;	// retorna erro para o script

		iTimeInit = time(NULL);

        //Trata eventos de resposta
		for(;;)
		{
			    
			Event = WaitEvent();

			switch(Event)
			{
                case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }

					break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
                    {
					    bFlagDisconnect = true;
					}
				    break;

				case T_SW_CONF_OK:
    				Log(LOG_CTI, "Event: T_SW_CONF_OK");
					GetInfoConnection(Event);
					if(GetDataPacket(Connection.sData.c_str(), "VALUE", iValue))
					{
						Log(LOG_ERROR, "There is no information in tag VALUE");
					    if( bFlagDisconnect )
						    return -1;
					    else
						    return -2;
					}
					if( bFlagDisconnect )
						return -1;
					else
					    return iValue;

				case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					if( bFlagDisconnect )
						return -1;
					else
					    return -2;

#ifdef _APPLICATION_SOCKET
                // Trata os eventos do monitor
                case T_SW_CONF_MONITOR_REQUEST:
                    CTI->SendEvent(Channel, Event);
					break;
#endif

			}
		}
	}	

	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceGetConfereeId()");
		return -2;
	}
}


int Ivr::ConferenceChangeStatus( const char* lpcszConferenceId, const int& iConfereeType, const int& iUnListen)
{
    char pActionBuffer [MAX_ACTION_BUFFER];
    int Event;
    bool bFlagDisconnect = false;
    string LastErrorMessage = " ";
    long lTimeSlotRing = 0;
    int iTimeInit = 0;
    int iTime = 0;

	try
	{
		Log(LOG_FUNCTIONS, "Ivr::ConferenceUnlisten()");

        if( iUnListen )
		{
            // Faz o usuario parar de ouvir a conferencia			
            m_pCti->ScConnect(FULLDUP);
		}
		
		// Estrutura com dados do destino. Neste caso, a própria máquina
        ConnectionParam(lpcszConferenceId);

		// Máquina Remota
        if( m_pSwitch->GetMachine() != Connection.Machine )
        {	

			sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    iConfereeType,
                                    TxGlobalTimeSlot);

		}
		else
		{
			sprintf (pActionBuffer, "?ACTION=CONFERENCE_SETCONFEREEATTRIBUTE"
                                    "&CHANNEL=%d"
                                    "&CONFERENCE_ID=%s"
                                    "&CONFEREE_TYPE=%d"
                                    "&TIMESLOT=%d",
                                    Channel,
                                    lpcszConferenceId,
                                    iConfereeType,
                                    TxTimeSlot);
        }
		
		Connection.sData		= pActionBuffer;

		// Envia para a conexao Destino o Evento X com informacao da conexao de Origem
		if( !SendConnection(T_SW_CONF_REQUEST, Connection.sData) )
			return 0;	// retorna erro para o script

		iTimeInit = time(NULL);

        //Trata eventos de resposta
		for(;;)
		{
		
			Event = WaitEvent();

			switch(Event)
			{

                case T_NO_EVENTS:
                    iTime = time(NULL) - iTimeInit;
                    if(iTime >= TIME_OUT)
                    {
                        if( bFlagDisconnect )
						    return -1;
					    else
					        return 0;
                    }

					break;
					
				case T_DISCONNECT:
					// Usuario desligou durante a criação da conferência
					Log(LOG_CTI, "Event: T_DISCONNECT");
					if( bFlagDisconnect == false)
                    {
					    bFlagDisconnect = true;
					}
				    break;

				case T_SW_CONF_OK:
					Log(LOG_CTI, "Event: T_SW_CONF_OK");
					GetInfoConnection(Event);					
					if( bFlagDisconnect )
						return -1;
					else
					    return 1;

				case T_SW_CONF_ERROR:
					Log(LOG_CTI, "Event: T_SW_CONF_ERROR");
					GetInfoConnection(Event);
					GetDataPacket( Connection.sData.c_str(), "LAST_ERROR_MESSAGE", LastErrorMessage );
					Log(LOG_CTI, "%s", LastErrorMessage.c_str() );
					if( bFlagDisconnect )
						return -1;
					else
					    return 0;

#ifdef _APPLICATION_SOCKET
                // Trata os eventos do monitor
                case T_SW_CONF_MONITOR_REQUEST:
                    CTI->SendEvent(Channel, Event);
					break;
#endif


			}
		}
	}	
	catch (...)
	{
		Log(LOG_FUNCTIONS, "Exception: Ivr::ConferenceChangeStatus()");
		return 0;
	}
}

//------------------------------------------------------------------------------------------------------------------------

string Ivr::ConnectionParam(string sParam, char *parser)
{  
    string sRoomId;
    string sMachineId;
    string sEnd;

    int iPos1 = 0;
    int iPos2 = 0;
  
    iPos1 = sParam.find(parser,0);

    if( iPos1 < 0 )
    {
        Connection.Switch		= m_pSwitch->GetSwitch();
		Connection.Machine	= m_pSwitch->GetMachine();
		Connection.Channel	= 0;
		Connection.Timeslot	= 0;
        return "";
    }  

    sRoomId = sParam.substr(0, iPos1);

    iPos2 = sParam.find(parser,iPos1 + 1);

    if( iPos2 < 0 )
    {
		sMachineId = sParam.substr(iPos1 + 1, sParam.size());
		Connection.Machine  = atoi( sMachineId.c_str() );
		Connection.Switch   = m_pSwitch->GetSwitch();
		Connection.Channel	= 0;
		Connection.Timeslot = 0;
        return "";
    }

    sMachineId = sParam.substr(iPos1 + 1, iPos2 - 1);
    sEnd = sParam.substr(iPos2 + 1, sParam.size());

	Connection.Machine  = atoi( sMachineId.c_str() );
	Connection.Switch   = m_pSwitch->GetSwitch();;
	Connection.Channel	= 0;
	Connection.Timeslot = 0;
    return sEnd;
}


//================================================================================================================================

void Ivr::DialFromTelnet(void)
{
	bool bAlerting = false;
	int Event, iTimeoutDial, iTimeEx;
	int iRecordUser = 0;
	string sDnis, sAni;

	try {

		Log(LOG_FUNCTIONS, "Ivr::DialFromTelnet()");

		// Incializa as variaveis para uma ligacao de saida
		InitOutboundCall();
		// Pega informacoes sobre a conexao remota que pediu a solicitacao de discagem
		// Se o canal for analogico, o Dnis deve estar concatenado com o mode 'P' ou  'T' (pulso ou tom)
		GetInfoConnection(T_SW_DIAL_TELNET);

		GetDataPacket(Connection.sData.c_str(), "DNIS",	sDnis);
		GetDataPacket(Connection.sData.c_str(), "ANI",	sAni);

		iTimeoutDial = 40;

		strcpy(Ani, sAni.c_str());
		strcpy(Dnis, sDnis.c_str());

		// Conecta a interface de linha com o proprio canal de voz
		m_pCti->ScConnect(FULLDUP);

		Log(LOG_FUNCTIONS, "Dial: %s", sDnis.c_str());

		// Disca assincronamente
		m_pCti->DialAsync((char *)sDnis.c_str(), (char *)sAni.c_str());

		Print(IVR_DNIS, sDnis.c_str());
		Print(IVR_ANI, sAni.c_str());

		// Marca o tempo de inicio da ligacao
		iTimeEx = time(NULL);

		for(;;){

			Event = WaitEvent();

			switch(Event){

				case T_NO_EVENTS:
					// Verifica se esgotou o tempo de esperar o evento de discagem
					if( (time(NULL) - iTimeEx) >= iTimeoutDial ){
						// Nao foi possivel discar no canal de saida
						Log(LOG_CTI, "Event: TIME OUT ON DIAL");
						// Desliga canal de saida
						Hangup();
					}
				break;

				case T_NOANSWER:
					Log(LOG_CTI, "Event: T_NOANSWER");
					// Desliga canal de saida
					Hangup();
				break;

				case T_DISCONNECT:
					// Nao foi possivel discar no canal de saida
					Log(LOG_CTI, "Event: T_DISCONNECT");
					// Desliga canal de saida
					Hangup();
				break;

				case T_ALERTING:
				case T_DIAL:
					Log(LOG_CTI, "Event: T_ALERTING");
					// Vai para o estado de canal de saida conectado
					bAlerting = true;
				break;
			}

			if(bAlerting)
				break;
		}

		for(;;){
		
			Event = WaitEvent();

			switch(Event){
	
				case T_CONNECT:
					Log(LOG_CTI, "Event: T_CONNECT");
					// Ligacao de saida requisitada foi atendida, coloca o status na tela
					Print(IVR_STATUS, STR_CONNECTED);
					SetIconStatus(ICON_CONNECTED);
					// Atualiza o resultado da discagem para conectado para o CDR
					iDialResult = DIAL_OUT_CONNECTED;
				break;

				case T_NOANSWER:	// ligacao de saida requisitada nao foi atendida
					Log(LOG_CTI, "Event: T_NOANSWER");
					Hangup();
				break;
				
				case T_DISCONNECT: // Canal de saida desligou
					Log(LOG_CTI, "Event: T_DISCONNECT");
					Hangup();
				break;
			}
		}
	}
	catch(...){
		Log(LOG_ERROR, "EXCEPTION: Ivr::DialFromTelnet()");
	}
}

std::string Ivr::MountCDRClose(const std::string & _callId)
{
    std::stringstream ss;
    //CDR_CLOSE, CALL_ID='01040011566852502000', FINAL_DATETIME='2019-08-26 17:48:30.000', NUMBER_A='11996895993', NUMBER_B='209' 

    char szDateTime[30] = {0};    
    GetDatetime(szDateTime);
    
    std::string sNumA = GetCTI()->GetAni();
    std::string sNumB = GetCTI()->GetDnis();

    ss << boost::format("CDR_CLOSE, CALL_ID='%s', FINAL_DATETIME='%s', NUMBER_A='%s', NUMBER_B='%s'") % _callId.c_str() % szDateTime % sNumA.c_str() %sNumB.c_str();

    return ss.str();
}

void Ivr::LongJmpBlocked()
{
    std::stringstream ss;

    std::string sCallId;
    std::string sCdr;

    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

    try
    {
        FlagBlocked = true;

	    // Para o canal de voz
	    if( m_pCti->StopCh( 1000 ) == T_IDLE )
	    {
		    FlagBlocked = false;
	    }

	    // Guarda a data de fim da ligacao
	    GetDatetime(sEndDateOfCall);

	    // Guarda o tempo em segundos de fim da ligacao
	    TimeOfCall = time(NULL) - TimeOfCall;

        sCallId.assign(CallId);       
        sCdr = MountCDRClose(sCallId);

        Print(IVR_STATUS,			"");
	    Print(IVR_CALL_TYPE,	"");
	    Print(IVR_DNIS,				"");
	    Print(IVR_ANI,				"");
	    Print(IVR_SERVICE,		"");

	    Print(IVR_STATUS, STR_BLOCKED);
	    SetIconStatus(ICON_INACTIVE); // LINE_OUT amarelo

	    // Seta o status da porta no banco de dados para bloqueado
	    DbSetPortStatus(PORT_BLOCKED, Channel);

	    GoEvents();
    }
    catch(...)
	{
        m_pLog4cpp->debug("[%p][%s] EXCEPTION: Ivr::LongJmpBlocked()", this, __FUNCTION__);		
		GoEvents();
	}
    
}
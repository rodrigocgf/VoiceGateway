/*********************************************************************************************
LuaCti.cpp
22/JUL/2003 - Jairo Rosa - Nova conf privada para DMV 600A e outras                       
*********************************************************************************************/

#include <time.h>
#include <sys\timeb.h>
#include "Ivr.h"
#include "Switch.h"
#include <lua.hpp>
#include <io.h>
#include <setjmp.h>

struct lua_sclongjmpnode
{
    jmp_buf env_buffer;
    struct lua_sclongjmpnode* previous;
};

struct lua_scjmpstack
{
    lua_sclongjmpnode* top;
};


// RETORNO DAS FUNCOES DE TELEFONIA CASO O USUARIO DESLIGUE
#define R_CALL_DISCONNECTED					-1

// RETORNO DA FUNCAO DIAL
#define R_DIAL_COMPLETE							0
#define R_DIAL_DISCONNECTED					1
#define R_DIAL_CONNECT							2
#define R_DIAL_BUSY									3
#define R_DIAL_NOANSWER							4
#define R_DIAL_NODIALTONE						5
#define R_DIAL_ERROR								6

//---------------------------------------------------------------------------------------------

int LuaSetFaxParam					(lua_State *L);
int LuaAccept								(lua_State *L);
int LuaAnswer								(lua_State *L);
int LuaHangup								(lua_State *L);
int LuaDisconnect						(lua_State *L);
int LuaDial									(lua_State *L);
int LuaIPDial								(lua_State *L);
int LuaSendFax							(lua_State *L);
int LuaRecvFax							(lua_State *L);
int LuaChannelIdle					(lua_State *L);
int LuaGetDnis							(lua_State *L);
int LuaGetAni								(lua_State *L);
int LuaGetCategory					(lua_State *L);
int LuaLineSideTransfer			(lua_State *L);
int LuaPulseDetection				(lua_State *L);
int LuaHangupToneDetection	(lua_State *L);
int LuaMsgBoxBeepDetection	(lua_State *L);
int	LuaDialEx								(lua_State *L);
int LuaGetDialExStatus			(lua_State *L);
int LuaHangupEx							(lua_State *L);
int LuaBlindTransfer				(lua_State *L);
int LuaDetectTones					(lua_State *L);
int LuaGetEvents   					(lua_State *L);
int LuaSetHook							(lua_State *L);
int LuaHoldCall							(lua_State *L);
int LuaRetrieveCall					(lua_State *L);
int LuaLog					        (lua_State *L);
int LuaClearDigits                  ( lua_State* L );

//---------------------------------------------------------------------------------------------
// Registra as funcoes a serem interpretadas pelo engine do Lua

static const struct luaL_reg CtiLibFunctions[] = 
{
    {"SetFaxParam",						LuaSetFaxParam					},
    {"Accept",								LuaAccept								},
    {"Answer",								LuaAnswer								},
    {"SendFax",								LuaSendFax							},
    {"RecvFax",								LuaRecvFax							},
    {"ChannelIdle",						LuaChannelIdle					},
    {"Hangup",								LuaHangup								},
    {"Disconnect",						LuaDisconnect						},
    {"GetDnis",								LuaGetDnis							},
    {"GetAni",								LuaGetAni								},
    {"GetCategory",						LuaGetCategory					},
    {"Dial",									LuaDial									},
    {"IPDial",								LuaIPDial								},
    {"LineSideTransfer",			LuaLineSideTransfer			},
    {"PulseDetection",				LuaPulseDetection				},
    {"HangupToneDetection",		LuaHangupToneDetection	},
    {"MsgBoxBeepDetection",		LuaMsgBoxBeepDetection	},
    {"DialEx",								LuaDialEx								},
    {"GetDialExStatus",				LuaGetDialExStatus			},
    {"HangupEx",							LuaHangupEx							},
    {"BlindTransfer",					LuaBlindTransfer				},
    {"DetectTones",						LuaDetectTones					},
    {"GetEvents",						  LuaGetEvents   					},
    {"SetHook",							  LuaSetHook	   					},
    {"HoldCall",						  LuaHoldCall							},  
    {"RetrieveCall",				  LuaRetrieveCall					},
    {"Log",                           LuaLog                            }    
    
};

struct lua_sclongjmpnode* GetNodeCTI( lua_State* L )
{
    struct lua_sclongjmpnode* node;
    lua_getglobal( L, "__Node" );
    
    node = static_cast<struct lua_sclongjmpnode*>( lua_touserdata( L, -1 ) );    

    lua_remove( L, -1 );
    return node;
}


Ivr * GetIvrCTI( lua_State* L )
{
    Ivr * pIvr;
    lua_getglobal( L, "__Ivr" );

    pIvr = static_cast<Ivr *>( lua_touserdata( L, -1 ) );

    lua_remove( L, -1 );
    return pIvr;
}

//---------------------------------------------------------------------------------------------

void Lua_CtiLibOpen(lua_State *L)
{
    //luaL_openlib( L, "CTI", CtiLibFunctions, 0 );
    luaL_openl( L, CtiLibFunctions );
    
}

//---------------------------------------------------------------------------------------------

int LuaSetFaxParam(lua_State *L)
{
    int iNumArgs;
    const char *sFont;
    int iCharSize;
    int iLineWidth;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        pIvr->Log(LOG_SCRIPT, "SetFaxParam()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        switch(iNumArgs){

        case 1:
            sFont				=	luaL_check_string(L, 1);
            iCharSize		= 0;
            iLineWidth	= 0;
            break;
        case 2:
            sFont				=	luaL_check_string(L, 1);
            iCharSize		= luaL_check_int(L, 2);
            iLineWidth	= 0;
            break;
        case 3:
            sFont				=	luaL_check_string(L, 1);
            iCharSize		= luaL_check_int(L, 2);
            iLineWidth	= luaL_check_int(L, 3);
            break;
        }

        if( strlen(sFont) > 0 ){
            // Se o valor for invalido deixa o default
            pIvr->GetSwitch()->SetFaxFont( sFont );            
        }

        if( iCharSize > 0 ){
            // Se o valor for invalido deixa o default
            pIvr->GetSwitch()->SetFaxCharSize( iCharSize );            
        }

        if( iLineWidth > 0 ){
            // Se o valor for invalido deixa o default
            pIvr->GetSwitch()->SetFaxLineWidth ( iLineWidth );            
        }

        return 0;
    }
    catch(...){
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetFaxParam()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaDial(lua_State *L)
{
    int iNumArgs, iRet, iCallProgrees, iTimeout = 0;
    const char *sDnis;
    const char *sAni;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "Dial()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 3){
            return 0;
        }

        sDnis					= luaL_check_string(L, 1);
        sAni					= luaL_check_string(L, 2);
        iCallProgrees	= luaL_check_int(L, 3);

        iRet = pIvr->GetCTI()->StopCh();        

        if(iRet == T_DISCONNECT)
        {
            pIvr->Log(LOG_SCRIPT, "Call disconnected");
            pIvr->SetScriptHangupFlag(true);
            lua_pushnumber (L, R_CALL_DISCONNECTED);
            return 1;
        }

        if(iRet == -1){
            pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
            // Quando der erro retorna 1
            lua_pushnumber (L, 1);
            return 1;
        }

        if( iCallProgrees )
            iRet = pIvr->GetCTI()->DialAsync((char *)sDnis, (char *)sAni, true);
        else
            iRet = pIvr->GetCTI()->DialAsync((char *)sDnis, (char *)sAni, false);

        switch(iRet){

        case T_DISCONNECT:
            lua_pushnumber (L, R_DIAL_DISCONNECTED);
            return 1;
        case T_DIAL:
            lua_pushnumber (L, R_DIAL_COMPLETE);
            return 1;
        case T_CONNECT:
            lua_pushnumber (L, R_DIAL_CONNECT);
            return 1;
        case T_BUSY:
            lua_pushnumber (L, R_DIAL_BUSY);
            return 1;
        case T_NOANSWER:
            lua_pushnumber (L, R_DIAL_NOANSWER);
            return 1;
        case T_NODIALTONE:
            lua_pushnumber (L, R_DIAL_NODIALTONE);
            return 1;
        case T_ERROR:
            lua_pushnumber (L, R_DIAL_ERROR);
            return 1;
        }

        lua_pushnumber (L, R_DIAL_ERROR);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaDial()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaIPDial(lua_State *L)
{
    int iNumArgs, iRet, iTimeout = 0;
    const char *sDnis;
    const char *sAni;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "IPDial()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 2){
            return 0;
        }

        sDnis					= luaL_check_string(L, 1);
        sAni					= luaL_check_string(L, 2);

        iRet = pIvr->GetCTI()->StopCh();

        if(iRet == T_DISCONNECT){
            pIvr->Log(LOG_SCRIPT, "Call disconnected");
            pIvr->SetScriptHangupFlag(true);
            lua_pushnumber (L, R_CALL_DISCONNECTED);
            return 1;
        }

        if(iRet == -1){
            pIvr->Log(LOG_SCRIPT, pIvr->GetCTI()->GetLastError());
            // Quando der erro retorna 1
            lua_pushnumber (L, 1);
            return 1;
        }

        iRet = pIvr->GetCTI()->IPDialAsync((char *)sDnis, (char *)sAni);

        switch(iRet){

        case T_DISCONNECT:
            lua_pushnumber (L, R_DIAL_DISCONNECTED);
            return 1;
        case T_DIAL:
            lua_pushnumber (L, R_DIAL_COMPLETE);
            return 1;
        case T_CONNECT:
            pIvr->GetCTI()->ScRouteCallIP();
            lua_pushnumber (L, R_DIAL_CONNECT);
            return 1;
        case T_BUSY:
            lua_pushnumber (L, R_DIAL_BUSY);
            return 1;
        case T_NOANSWER:
            lua_pushnumber (L, R_DIAL_NOANSWER);
            return 1;
        case T_NODIALTONE:
            lua_pushnumber (L, R_DIAL_NODIALTONE);
            return 1;
        case T_ERROR:
            lua_pushnumber (L, R_DIAL_ERROR);
            return 1;
        }

        lua_pushnumber (L, R_DIAL_ERROR);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaIPDial()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

// Aceita a ligacao
// Entrada: Bloqueia ligacao a cobrar (default e nao bloquear
//					Cobra a ligacao (default e cobrar)

int LuaAccept(lua_State *L)
{
    int iNumArgs, iBloqueioDDC, iBilhetaLigacao;
    bool bDDC, bBil;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "Accept()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            // Se nao for passado nenhum argumento, nao bloqueia ligacoes a cobrar
            // e cobra a ligacao normalmemte
            bDDC = false;
            bBil = true;
        }

        switch(iNumArgs){

        case 1: // Apenas parametro de ligacao a cobrar
            iBloqueioDDC = luaL_check_int(L, 1);

            if(iBloqueioDDC)
                bDDC = true;
            else
                bDDC = false;

            // Cobra a ligacao
            bBil = true;
            break;

        case 2:
            iBloqueioDDC = luaL_check_int(L, 1);

            if(iBloqueioDDC)
                bDDC = true;
            else
                bDDC = false;

            iBilhetaLigacao = luaL_check_int(L, 2);

            if(iBilhetaLigacao)
                bBil = true;
            else
                bBil = false;
            break;
        }

        // Atende a ligacao
        pIvr->GetCTI()->Accept( bDDC, bBil );

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaAccept()");
        // Termina o scrit
        pIvr->EndScript();
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

// Atende a ligacao
// Entrada: Bloqueia ligacao a cobrar (default e nao bloquear
//					Cobra a ligacao (default e cobrar)

int LuaAnswer(lua_State *L)
{
    int iNumArgs, iRet;
    bool bDoubleAnswer = false, bBilling = true;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try
    {        
        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if( iNumArgs <= 0 )
        {
            // Se nao for passado nenhum argumento, nao bloqueia ligacoes a cobrar
            // e cobra a ligacao normalmemte
            bDoubleAnswer = false;
            bBilling = true;
        }

        switch( iNumArgs )
        {

        case 1:
            // Duplo atendimento
            bDoubleAnswer = ( luaL_check_int(L, 1) ) ? ( true ) : ( false );
            // Cobranca da ligacao
            bBilling = true;
            break;

        case 2:
            // Duplo atendimento
            bDoubleAnswer = ( luaL_check_int(L, 1) ) ? ( true ) : ( false );
            // Cobranca da ligacao
            bBilling = ( luaL_check_int(L, 2) ) ? ( true ) : ( false );
            break;
        }

        pIvr->Log( LOG_SCRIPT, "Answer( \"%s\", \"%s\" )", ( bDoubleAnswer ) ? ( "true" ) : ( "false" ), ( bBilling ) ? ( "true" ) : ( "false" ) );

        // Atende a ligacao
        iRet = pIvr->AnswerCall( bDoubleAnswer, bBilling );

        if(iRet == T_DISCONNECT)
        {
            pIvr->Log(LOG_SCRIPT, "Call disconnected");
            pIvr->SetScriptHangupFlag(true);
            lua_pushnumber (L, R_CALL_DISCONNECTED);
            return 1;
        }

        if(iRet != 0)
        {
            pIvr->Log( LOG_SCRIPT, "Error in Answer()." );
            // Quando der erro retorna 1
            lua_pushnumber (L, 1);
            return 1;
        }

        lua_pushnumber (L, 0);
        return 1;
    }
    catch(...)
    {        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaAnswer()");
        // Termina o scrit
        pIvr->EndScript();
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaHangup(lua_State *L)
{
    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "Hangup()");
        pIvr->Hangup();

        // Termina o scrit
        struct lua_sclongjmpnode *node = GetNodeCTI( L );	
        longjmp( node->env_buffer, 1 );

        return 0;
    }
    catch(...)
    {        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaHangup()");
        // Termina o scrit
        pIvr->EndScript();
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaDisconnect(lua_State *L)
{
    int iRet;
    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        pIvr->Log(LOG_SCRIPT, "Disconnect()");

        iRet = pIvr->GetCTI()->Disconnect2();		// desliga a segunda chamada

        if( iRet == T_DISCONNECT )			// desligou a primeira chamada
        {
            lua_pushnumber (L, R_CALL_DISCONNECTED);
            return 1;
        }

        lua_pushnumber (L, 0);
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaDisconnect()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaGetDnis(lua_State *L)
{
    char sDnis[128];

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {
        pIvr->Log(LOG_SCRIPT, "GetDnis()");

        strcpy(sDnis, pIvr->ReturnDnis());

        if(strlen(sDnis) == 0)
            return 0;

        lua_pushstring(L, sDnis);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetDnis()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaGetAni(lua_State *L)
{
    char sAni[128];

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        pIvr->Log(LOG_SCRIPT, "GetAni");

        strcpy(sAni, pIvr->ReturnAni());

        if(strlen(sAni) == 0)
            return 0;

        lua_pushstring(L, sAni);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetAni()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaGetCategory(lua_State *L)
{
    int iCategoria;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {
        pIvr->Log(LOG_SCRIPT, "GetCategory()");

        iCategoria = pIvr->ReturnCategory();

        lua_pushnumber (L, iCategoria);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetCategory()");
        return 0;
    }
}

//--------------------------------------------------------------------------------------------------------------------

int LuaChannelIdle(lua_State *L)
{
    int iState;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        pIvr->Log(LOG_SCRIPT, "ChannelIdle()");
        iState = pIvr->GetCTI()->VoxState();

        if(iState == S_IDLE)
            lua_pushnumber (L, 1);
        else
            lua_pushnumber (L, 0);

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaChannelIdle()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

// int LuaGetEvents()

int LuaGetEvents(lua_State *L)
{
    int iRet, iNumArgs, iTimerWait, iTimerInit, iTimer;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "GetEvent()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs == 1){
            // Tempo maximo de espera do evento
            iTimerWait	= luaL_check_int(L, 1);
            iTimerInit = time(NULL);
        }
        else
            iTimerWait = 0;

        for(;;) {

            // Espera eventos
            iRet = pIvr->GetCTI()->WaitEvent();

            if(iRet == T_DISCONNECT2)
            {
                lua_pushnumber (L, R_DIAL_DISCONNECTED);
                return 1;
            }

            if(iRet == T_DISCONNECT){
                pIvr->Log(LOG_SCRIPT, "Call disconnected");
                if( pIvr->GetSSConferece() )
                {
                    pIvr->SetSSConferece(false);
                    continue;
                }
                else
                    pIvr->SetScriptHangupFlag(true);

                lua_pushnumber (L, R_CALL_DISCONNECTED);
                return 1;
            }

            // Trata evento do monitor
#ifdef _APPLICATION_SOCKET
            if(iRet == T_SW_CONF_MONITOR_REQUEST)
            {
                string sMessage;
                iRet = Lua_UserData->IVR->EventMonitor(iRet, sMessage);
                if( iRet <= 0 )
                {
                    lua_pushnumber (L, iRet);
                    lua_pushstring(L, sMessage.c_str());
                    return 2;
                }
                continue;
            }
#endif // _APPLICATION_SOCKET

            if(iTimerWait)
            {
                iTimer = time(NULL) - iTimerInit;
                if(iTimer >= iTimerWait)
                {
                    lua_pushnumber (L, -2);
                    return 1;
                }
            }

        }

    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: WaitEvent()");
        return 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------------

int LuaSendFax (lua_State *L)
{
    int iNumArgs, iRet;
    const char *sPathFile;
    const char *sDisplay;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "SendFax()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        sPathFile = luaL_check_string(L, 1);

        if( _access(sPathFile, 0) ){
            // O arquivo nao existe
            lua_pushnumber (L, 2);
            return 1;
        }

        if(iNumArgs >= 2){
            sDisplay = luaL_check_string(L, 2);
            iRet = pIvr->SendFax(sPathFile, sDisplay);
        }
        else{
            iRet = pIvr->SendFax(sPathFile, " ");
        }

        lua_pushnumber (L, iRet);
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSendFax()");
        return 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------------

int LuaRecvFax (lua_State *L)
{
    int iNumArgs, iRet;
    const char *sPathFile;
    const char *sDisplay;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {
        pIvr->Log(LOG_SCRIPT, "RecvFax()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        sPathFile = luaL_check_string(L, 1);

        if(iNumArgs >= 2){
            sDisplay = luaL_check_string(L, 2);
            iRet = pIvr->RecvFax(sPathFile, sDisplay);
        }
        else{
            iRet = pIvr->RecvFax(sPathFile, " ");
        }

        lua_pushnumber (L, iRet);
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaRecvFax()");
        return 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------------

int LuaLineSideTransfer(lua_State *L)
{
    int iNumArgs;
    const char *sDestination;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1){
            return 0;
        }

        sDestination = luaL_check_string(L, 1);

        pIvr->Log(LOG_SCRIPT, "LineSideTransfer(%s)", sDestination);

        pIvr->GetCTI()->BlindTransfer((char *)sDestination);

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaLineSideTransfer()");
        return 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------------

int LuaBlindTransfer(lua_State *L)
{
    int iNumArgs;
    const char *sDestination = NULL;
    int rc, iTime;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try
    {        
        // Pega o numero de argumentos
        iNumArgs = lua_gettop( L );

        if(iNumArgs < 1){
            return 0;
        }

        sDestination = luaL_check_string( L, 1 );

        if(iNumArgs == 2)
            iTime = luaL_check_int( L, 2 );
        else
            iTime = 10;

        pIvr->Log( LOG_SCRIPT, "LuaBlindTransfer(%s)", sDestination );

        if( sDestination == NULL )
            return 0;

        rc = pIvr->GetCTI()->BlindTransfer( sDestination, iTime);

        if( rc != 0 )
        {
            lua_pushnumber( L, R_DIAL_ERROR );
            return 1;
        }
        else
        {
            lua_pushnumber( L, 0 );
            return 1;
        }
    }
    catch(...)
    {        
        pIvr->Log( LOG_ERROR, "EXCEPTION: LuaBlindTransfer()" );
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaPulseDetection(lua_State *L)
{
    int iNumArgs, iFlag;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        iFlag = luaL_check_int(L, 1);

        if( iFlag )
            pIvr->GetCTI()->PulseDetection(true);
        else
            pIvr->GetCTI()->PulseDetection(false);

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaPulseDetection()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaHangupToneDetection(lua_State *L)
{
    int iNumArgs, iFlag;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        iFlag = luaL_check_int(L, 1);

        if( iFlag )
            pIvr->GetCTI()->HangupToneDetetion(true);
        else
            pIvr->GetCTI()->HangupToneDetetion(false);

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaHangupToneDetection()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------
int LuaMsgBoxBeepDetection(lua_State *L)
{
    int iNumArgs, iFlag;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            return 0;
        }

        iFlag = luaL_check_int(L, 1);

        if( iFlag )
            pIvr->GetCTI()->MsgBoxBeepDetection(true);
        else
            pIvr->GetCTI()->MsgBoxBeepDetection(false);

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaMsgBoxBeepDetection()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

/* Fun��o LuaAddFile - Adiciona um arquivo do tipo wav na lista de prompts */
int LuaAddFile (lua_State *L)
{
    int iNumArgs, status;
    char szFilename[_MAX_PATH+1];

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            pIvr->Log(LOG_SCRIPT, "LuaAddFile()");
            return 0;
        }

        // monta o caminho para o arquivo de voz a ser reproduzido
        sprintf( szFilename, "%s\\%s", pIvr->GetSwitch()->GetPathScripts().c_str() , luaL_check_string(L, 1) );

        pIvr->Log(LOG_SCRIPT, "LuaAddFile( %s )", szFilename);

        status = pIvr->GetCTI()->AddFile( szFilename );

        // 0 - SUCESSO, diferente de zero erro
        lua_pushnumber	(L, status);
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaAddFile ()");
        return 0;
    }
}


//---------------------------------------------------------------------------------------------

// Fun��o LuaAddRemoteFile - Adiciona um arquivo do tipo wav na lista de prompts
// sem pertencer ao diretorio padra do script

int LuaAddRemoteFile (lua_State *L)
{
    int iNumArgs, status;
    char szFilename[_MAX_PATH+1];

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {
        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs <= 0){
            pIvr->Log(LOG_SCRIPT, "AddRemoteFile()");
            return 0;
        }

        // monta o caminho para o arquivo de voz a ser reproduzido
        sprintf( szFilename, "%s", luaL_check_string(L, 1) );

        pIvr->Log(LOG_SCRIPT, "AddRemoteFile( %s )", szFilename);

        status = pIvr->GetCTI()->AddFile( szFilename );

        // 0 - SUCESSO, diferente de zero erro
        lua_pushnumber	(L, status);
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaAddRemoteFile ()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaDialEx(lua_State *L)
{
    int iNumArgs, iRet;
    const char *sDnis	=	NULL;
    const char *sAni	=	NULL;
    const char *sTime	=	NULL;
    string sTimeout;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        pIvr->Log(LOG_SCRIPT, "DialEx()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs < 1){
            return 0;
        }

        switch(iNumArgs)
        {
        case 1:
            sDnis	=	luaL_check_string	(L, 1);
            break;
        case 2:
            sDnis	=	luaL_check_string	(L, 1);
            sAni	=	luaL_check_string	(L, 2);
            break;
        case 3:
            sDnis	=	luaL_check_string	(L, 1);
            sAni	=	luaL_check_string	(L, 2);
            sTime	=	luaL_check_string	(L, 3);

            if( atoi(sTime) <= 20 )
                sTimeout = "40";
            else
                sTimeout = sTime;
            break;
        }

        // Esvazia a fial de eventos antes de discar
        pIvr->GetDialExStatus(true);

        // Disca
        iRet = pIvr->DialEx(sDnis, sAni, sTimeout.c_str());

        if( iRet )
            lua_pushnumber (L, 1); // Erro
        else
            lua_pushnumber (L, 0); // Iniciou a discagem

        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaDialEx()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaGetDialExStatus(lua_State *L)
{
    int iStatus, iNumArgs, iModoBloqueado;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {
        pIvr->Log(LOG_SCRIPT, "GetDialExStatus()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs == 1)
            iModoBloqueado = luaL_check_int(L, 1);
        else
            iModoBloqueado = 0;

        if( iModoBloqueado ){

            // Fica pegando eventos ate um resultado
            for(;;){

                // Verifica se o canal de entrada desligou (SOMENTE EM BLOCKING MODE)
                if( pIvr->GetCTI()->WaitForDisconnect(1000) ){
                    pIvr->Log(LOG_SCRIPT, "Call disconnected");
                    lua_pushnumber (L, STEX_LOCAL_HANGUP); 
                    return 1;
                }

                // Verifica o status do canal de saida
                iStatus = pIvr->GetDialExStatus();

                if( iStatus != STEX_DIALING ){
                    lua_pushnumber (L, iStatus); 
                    return 1;
                }
            }
        }

        // Verifica o status do canal de saida
        iStatus = pIvr->GetDialExStatus();
        lua_pushnumber (L, iStatus);

        return 1;
    }
    catch(...) {        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaGetDialExStatus()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaHangupEx(lua_State *L)
{
    int iNumArgs, iLocalHangup;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        pIvr->Log(LOG_SCRIPT, "HangupEx()");

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs == 1){
            iLocalHangup = luaL_check_int(L, 1);
        }
        else{
            iLocalHangup = 0;
        }

        // Desliga o canal remoto
        pIvr->HangupEx();

        if( iLocalHangup ){
            // Termina o scrit
            //pIvr->EndScript(); NOT USED
        }

        return 0;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaHangupEx()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaDetectTones(lua_State *L)
{
    int iNumArgs, iToneSetType;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs != 1)
        {
            pIvr->Log(LOG_SCRIPT, "DetectTones()");
            return 0;
        }

        iToneSetType = luaL_check_int(L, 1);
        pIvr->Log(LOG_SCRIPT, "DetectTones(%d)", iToneSetType);

        lua_pushnumber (L, pIvr->GetCTI()->DetectTones( iToneSetType )); 
        return 1;
    }
    catch(...){        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaDetectTones()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------
int LuaSetHook(lua_State *L)
{
    int iNumArgs, iTxBitA, iRet;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try {        

        // Pega o numero de argumentos
        iNumArgs = lua_gettop(L);

        if(iNumArgs != 1)
        {
            return 0;
        }

        iTxBitA = luaL_check_int(L, 1);
        pIvr->Log(LOG_SCRIPT, "LuaSetHook(%d)", iTxBitA);
        if(iTxBitA)
            iRet = pIvr->GetCTI()->OffHook();
        else iRet = pIvr->GetCTI()->OnHook();
        if(iRet == 0)
            lua_pushnumber (L, 1);
        else lua_pushnumber (L, 0);
        return 1;
    }
    catch(...) {        
        pIvr->Log(LOG_ERROR, "EXCEPTION: LuaSetHook()");
        return 0;
    }
}

//---------------------------------------------------------------------------------------------

int LuaHoldCall(lua_State *L)
{
    int rc;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try
    {
        pIvr->Log( LOG_SCRIPT, "LuaHoldCall()" );

        rc = pIvr->GetCTI()->HoldCall();

        if( rc < 0 )
            lua_pushnumber(L, 0);
        else
            lua_pushnumber(L, 1);

        return 1;
    }
    catch(...)
    {        
        pIvr->Log( LOG_ERROR, "EXCEPTION: LuaHoldCall()" );
        return 0;
    }
}

int LuaLog( lua_State* L )
{
    Ivr * pIvr = NULL;
    char const* message = lua_tostring(L,1);
    pIvr = GetIvrCTI(L);
    size_t length = lua_strlen( L , 1);

    if ( pIvr != nullptr )
    {
        if ( length > MAX_STRING_LENGTH_LUA_LOG )
        {
            pIvr->Log(LOG_INFO,"ERROR: String length is greater than allocated buffer");
            lua_pushnumber(L, 0);
            return 0;

        }
        pIvr->Log( LOG_INFO, message);
    }            

    lua_pushnumber(L, 1);
    return 0;
}

//-----------------------------------------------------------------------------------------------------------------------

int LuaRetrieveCall(lua_State *L)
{
    int rc;

    Ivr * pIvr = NULL;
    pIvr = GetIvrCTI(L);

    try
    {
        pIvr->Log( LOG_SCRIPT, "LuaRetrieveCall()" );

        rc = pIvr->GetCTI()->RetrieveCall();

        if( rc < 0 )
            lua_pushnumber(L, 0);
        else
            lua_pushnumber(L, 1);

        return 1;
    }
    catch(...)
    {        
        pIvr->Log( LOG_ERROR, "EXCEPTION: LuaRetrieveCall()" );
        return 0;
    }
}

//-------------------------------------------------------------------------------------------------------------------------

/************************************************************************************************/
/* Support Comm Teleinformatica S/A                                                             */
/*                                                                                              */
/* Modulo: DCB.cpp																																							*/
/* Funcao: Funcoes de baixo n�vel para uso da placa DCB	(implementa��o da interface)						*/
/*                                                                                              */
/* Updates:                                                                                     */
/*                                                                                              */
/* 15/OUT/2002 - Ricardo Sato - Criacao do Modulo                                               */
/* 28/ABR/2003 - Jairo Rosa - Suporte para DMV 600A                                             */
/*                                                                                              */
/************************************************************************************************/
// para STL
#ifdef _DEBUG
	#pragma warning(disable:4786) 
#endif

#include "DCB.h"

//Inicializa�ao dos membros das estruturas
DCBClass::_conferee_info::_conferee_info() : Timeslot(0), ConferenceTimeslot(0)
{
}

DCBClass::_conference_info::_conference_info() : Id(0), BoardId(0), DSPDeviceHandle(-1), NumberOfConferees(0), NotifyTone(false)
{
}

DCBClass::_private_conference_info::_private_conference_info() : Id(0), BoardId(0), DSPDeviceHandle(-1), NumberOfConferees(0), NotifyTone(false)
{
}

DCBClass::_resource_info::_resource_info() : BoardId(0), DSPHandle(0)
{
}

DCBClass::_dsp_info::_dsp_info() : DeviceHandle(-1), ResourcesCount(0)
{
}

DCBClass::_chair_person::_chair_person() : Timeslot(-1), Contador(0)
{
}

int DCBClass::ChooseBestDSP(resource_info &info)
{
	try
	{
		int DcbBoardCount = 1;
		int DcbDspCount = 1;
		int DcbDspResourceCount = 0;
		int DcbDspLessResourceUsage = 1;
		int DcbBoardLessResourceUsage = 1;

		//Busca qual dupla placa/DSP que tem mais recursos livres
		DcbDspResourceCount = Boards[ DcbBoardCount ][ DcbDspCount ].ResourcesCount;

		for( DcbBoardCount = 1; DcbBoardCount <= Boards.size(); DcbBoardCount++ )
		{
			for( DcbDspCount = 1; DcbDspCount <= Boards[ DcbBoardCount ].size(); DcbDspCount++ )
			{
				if( Boards[ DcbBoardCount ][ DcbDspCount ].ResourcesCount < DcbDspResourceCount )
				{
					DcbDspResourceCount = Boards[ DcbBoardCount ][ DcbDspCount ].ResourcesCount;
					DcbDspLessResourceUsage		= DcbDspCount;
					DcbBoardLessResourceUsage	= DcbBoardCount;
				}
			}
		}

		info.DSPHandle	= Boards[ DcbBoardLessResourceUsage ][ DcbDspLessResourceUsage ].DeviceHandle;
		info.BoardId		= DcbBoardLessResourceUsage;
		info.DspId	  	= DcbDspLessResourceUsage;

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::ChooseBestDSP()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

void DCBClass::ClearLastErrorMessage()
{
	try
	{
		// Atualiza o ponteiro do stream p/ o inicio do buffer.
		LastErrorMessage.seekp(0);
		// "Descongela" o buffer liberando seu conteudo.
		LastErrorMessage.rdbuf()->freeze( false );
	}
	catch(...)
	{
		return;
	}
}

DCBClass::DCBClass(boost::shared_ptr<Logger> pLog4cpp) : ReturnCode(0) , m_pLog4cpp(pLog4cpp)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);
	memset( &MsCdt[0], 0, sizeof(MsCdt[0]) );
}

DCBClass::~DCBClass()
{
}

int DCBClass::OpenBoard( const int& iBoardId, const char* sBoardName )
{
    m_pLog4cpp->debug("[%p][%s] BOARD %s", this, __FUNCTION__, sBoardName);

	try
	{
		char DcbDeviceName[256];
		int DcbDspHandle = -1;
		int DcbDspCount = 0;
		int DspNumber;

		if (strcmp(sBoardName, "DCB320SC") == 0)
			DspNumber = 1;			// 1 DSP
		else
		if (strcmp(sBoardName, "DCB640SC") == 0)
			DspNumber = 2;			// 2 DSP
		else
		if (strcmp(sBoardName, "DCB960SC") == 0)
			DspNumber = 3;			// 3 DSP
		else
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::OpenBoard(). Invalid board type." << std::ends;
			return CONFERENCE_ERROR;
		}

		for( DcbDspCount = 1; DcbDspCount <= DspNumber; DcbDspCount++ )
		{
			sprintf( DcbDeviceName, "dcbB%dD%d", iBoardId, DcbDspCount );
			DcbDspHandle = dcb_open( DcbDeviceName, 0 );
			if( DcbDspHandle == -1 )
			{
				CloseBoard();
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::OpenBoard(). Cannot open the device " << DcbDeviceName << "." << std::ends;
				return CONFERENCE_ERROR;
			}
			Boards[ iBoardId ][ DcbDspCount ].DeviceHandle = DcbDspHandle;
			Boards[ iBoardId ][ DcbDspCount ].BoardName = sBoardName;
			Boards[ iBoardId ][ DcbDspCount ].ResourcesCount = 0;


			int i, j, iConfereeCount = 0;
			
			//Remove as salas criadas na placa
			for( i = 1; i <= 32; i++ )
			{
				if( dcb_getcnflist( DcbDspHandle, i, &iConfereeCount, &MsCdt[0] ) == 0 )
				{
					for( j = 0; iConfereeCount >= 1; j++, iConfereeCount-- )
					{
						dcb_remfromconf( DcbDspHandle, i, &MsCdt[j] );
					}
					dcb_delconf( DcbDspHandle, i );
				}
			}
		}

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::OpenBoard()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::CloseBoard( const int& iBoardId )
{
	try
	{
		int DcbDspHandle = -1;
		int DcbBoardCount = 1;
		int DcbDspCount = 1;
		int ErrorCode = 0;

		if( iBoardId <= 0 )
		{
			// Fecha todas as placas.
			for( DcbBoardCount = 1; DcbBoardCount <= Boards.size(); DcbBoardCount++ )
			{
				for( DcbDspCount = 1; DcbDspCount <= Boards[ DcbBoardCount ].size(); DcbDspCount++ )
				{
					DcbDspHandle = Boards[ DcbBoardCount ][ DcbDspCount ].DeviceHandle;
					if( DcbDspHandle > 0 )
					{
						ReturnCode = dcb_close( DcbDspHandle );
						if( ReturnCode == -1 )
						{
							// Tolera falhas no fechamento de um device p/ tentar fechar os outros.
							ClearLastErrorMessage();
							LastErrorMessage << "Error in DCBClass::CloseBoard(). Cannot close the device " << ATDV_NAMEP( DcbDspHandle ) << ". " << ATDV_ERRMSGP( DcbDspHandle ) << "." << std::ends;
							ErrorCode = -1;
						}
					}
					Boards[ DcbBoardCount ].erase( DcbDspCount );
				}
				Boards.erase( DcbBoardCount );
			}
		}
		else
		{
			// Fecha uma placa especifica.
			for( DcbDspCount = 1; DcbDspCount <= Boards[ iBoardId ].size(); DcbDspCount++ )
			{
				DcbDspHandle = Boards[ iBoardId ][ DcbDspCount ].DeviceHandle;
				if( DcbDspHandle > 0 )
				{
					ReturnCode = dcb_close( DcbDspHandle );
					if( ReturnCode == -1 )
					{
						// Tolera falhas no fechamento de um device p/ tentar fechar os outros.
						ClearLastErrorMessage();
						LastErrorMessage << "Error in DCBClass::CloseBoard(). Cannot close the device " << ATDV_NAMEP( DcbDspHandle ) << ". " << ATDV_ERRMSGP( DcbDspHandle ) << "." << std::ends;
						ErrorCode = -1;
					}
				}
				Boards[ iBoardId ].erase( DcbDspCount );
			}
			Boards.erase( iBoardId );
		}
		return ErrorCode;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::CloseBoard()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::Create( const char* lpcszConferenceId, const bool& bNotifyTone )
{
	try
	{
        m_RoomId = lpcszConferenceId;

        //Verifica se j� foi criada
        if( map_Conferences.count( m_RoomId ) > 0 )
	        return CONFERENCE_WARNING;

	    conference_info ConferenceInfo;
	    ConferenceInfo.Id = -1;
        ConferenceInfo.NumberOfConferees = 0;
	    ConferenceInfo.NotifyTone = bNotifyTone;
        ConferenceInfo.ChairPersonTimeslot = -1;
		map_Conferences[ m_RoomId ] = ConferenceInfo;

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Create()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::Destroy( const char* lpcszConferenceId )
{
	try
	{
		m_RoomId = lpcszConferenceId;

        //Verifica se existe a conferencia
		if( map_Conferences.count( m_RoomId ) <= 0 )
		    return CONFERENCE_WARNING;

		conference_info& ConferenceInfo = map_Conferences[ m_RoomId ];
    
        Boards[ ConferenceInfo.BoardId ][ ConferenceInfo.DspId ].ResourcesCount -= ConferenceInfo.NumberOfConferees;
    
		ReturnCode = dcb_delconf( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id );
		if( ReturnCode == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::Destroy(). Cannot destroy the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
			return CONFERENCE_ERROR;
		}

		map_Conferences.erase( m_RoomId );

		// debug

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Destroy()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::Add( const char* lpcszConferenceId, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot)
{
    m_pLog4cpp->debug("[%p][%s]", this, __FUNCTION__);

	try
	{
		int iConfSize = iConferenceSize;
		conference_info ConferenceInfo;
		conferee_info ConfereeInfo;
		resource_info	info;
		chair_person ChairPersonInfo;

		m_RoomId = lpcszConferenceId;
    
		// Verifica se a chamada anterior n�o foi removida da confer�ncia antes de adicion�-la
		VerifyConfereeExistAndRemove( lTimeSlot );

		//Verifica se existe a conferencia
		if( map_Conferences.count(m_RoomId) <= 0 )
		{			
			ReturnCode = Create( lpcszConferenceId );      
			if( ( ReturnCode == CONFERENCE_ERROR ) || ( ReturnCode == CONFERENCE_EXCEPTION ) )
				return ReturnCode;
		}

		ConferenceInfo = map_Conferences[ m_RoomId ];	
    
        // Verifica se o tipo � conferencista e apenas retorna o timeslot
        if(cctConfereeType == CP_CONF)
        {
            //Atualiza valor do timeslot para retornar ao IVR
		    lTimeSlot = lMonitorTimeSlot = ConferenceInfo.ChairPersonTimeslot;
			if( ChairPerson.count(m_RoomId) <= 0 )
			{
				ChairPersonInfo.Timeslot = lTimeSlot;
				ChairPersonInfo.Contador = 1;
				ChairPerson[ m_RoomId ] = ChairPersonInfo;
			}
			else
			{
				ChairPersonInfo = ChairPerson[ m_RoomId ];
				ChairPersonInfo.Contador += 1;
				ChairPerson[ m_RoomId ] = ChairPersonInfo;
			}
            return CONFERENCE_OK;
        }

		// Guarda ID do conferencista (timeslot de transmiss�o).
		ConfereeInfo.Timeslot		= lTimeSlot;
		// Guarda o flag indicando se o conferencista e supervisor.
		ConferenceInfo.NotifyTone = bNotifyTone;

		//Preenche estrutura Dialogic
		MsCdt[0].chan_num = ConfereeInfo.Timeslot;
		MsCdt[0].chan_sel = MSPN_TS;

		switch( cctConfereeType )
		{
			case DUPLEX:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
			case MONITOR:
			case CP_GHOST:
				MsCdt[0].chan_attr = MSPA_RO;
				break;
			case COACH:
				MsCdt[0].chan_attr = MSPA_COACH;
				break;
			case PUPIL:
				MsCdt[0].chan_attr = MSPA_PUPIL;
				break;
			case PRIVATE:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
			case CP_MONI:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
			case CP_PERS:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
			default:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
		}
		
		if( ConferenceInfo.Conferees.size() <= 0 )
		{
			ChooseBestDSP(info);
			
			ConferenceInfo.DSPDeviceHandle	 = info.DSPHandle;
			ConferenceInfo.BoardId					 = info.BoardId;
			ConferenceInfo.DspId				  	 = info.DspId;
			ConferenceInfo.NumberOfConferees = iConferenceSize;
        
			//Primeiro conferencista. A conferencia deve ser criada.
			//MsCdt[0] vai com o timeslot TX (MsCdt[0].cham_num) e retorna com o timeslot RX (MsCdt[0].cham_attr - alias cham_lts)
			//( MSCA_ND | MSCA_NN ) = Ativa a notificacao por tom da entrada/saida de usuarios da conferencia.
			if( bNotifyTone )
				ReturnCode = dcb_estconf( ConferenceInfo.DSPDeviceHandle, &MsCdt[0], 1, MSCA_ND | MSCA_NN, &ConferenceInfo.Id );
			else
				ReturnCode = dcb_estconf( ConferenceInfo.DSPDeviceHandle, &MsCdt[0], 1, MSCA_NN, &ConferenceInfo.Id );
        
			if( ReturnCode == -1 )
			{																				
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Add(). Cannot create the conference and add the conferee (ID:" << ConfereeInfo.Timeslot << ") to this conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
				ConferenceInfo.DSPDeviceHandle = -1;
				return CONFERENCE_ERROR;
			}

			if( cctConfereeType == CP_GHOST )
			{
				//Fun��o n�o suportada nas DM3
				//Implementar adi��o de usu�rio como Read Only
				ReturnCode = dcb_monconf(ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, &lMonitorTimeSlot);

				// debug

				if( ReturnCode == -1 )
				{
					ClearLastErrorMessage();
					LastErrorMessage << "Error in DCBClass::Add(). Cannot add the conferee (ID:" << ConfereeInfo.Timeslot << ") to the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
                    m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
					return CONFERENCE_ERROR;
				}

				// Incrementa mais um para contabilizar o TimeSlot Ghost, serve para os participantes poderem ouvir
				iConfSize++;
				ConferenceInfo.ChairPersonTimeslot = lMonitorTimeSlot;
				ConferenceInfo.GhostTimeslot = ConfereeInfo.Timeslot;
			}
		    //Incrementa contador de recursos da placa em uso e DSP Alocando o numero maximo de conferencistas
			Boards[ ConferenceInfo.BoardId ][ ConferenceInfo.DspId ].ResourcesCount += iConfSize;
		}
		else
		{
			// A conferencia ja esta criada. Adiciona conferencista na conferencia.
			//Verifica se h� DSP vinculado
			if( ConferenceInfo.DSPDeviceHandle == -1 )
			{
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Add(). DSP device handle not found in the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") using the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
				return CONFERENCE_ERROR;
			}

			if( VerifyConfereePresence( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, lTimeSlot ) )
			{
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Add(). Cannot add the conferee (ID:" << ConfereeInfo.Timeslot << ") to the conference (ID:" << ConferenceInfo.Id << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". Conferee already in the conference." << std::ends;
                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
				return CONFERENCE_ERROR;
			}

			//MsCdt[0] vai com o timeslot TX (MsCdt[0].cham_num) e retorna com o timeslot RX (MsCdt[0].cham_attr - alias cham_lts)
			ReturnCode = dcb_addtoconf( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, &MsCdt[0] );
			if( ReturnCode == -1 )
			{
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Add(). Cannot add the conferee (ID:" << ConfereeInfo.Timeslot << ") to the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
                m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
				return CONFERENCE_ERROR;
			}      
      
			// Retorna o timeslot a ser ouvido pelo ChairPerson
			lMonitorTimeSlot = ConferenceInfo.ChairPersonTimeslot;
		}
		
		// Guarda timeslot da conferencia.
		ConfereeInfo.ConferenceTimeslot = MsCdt[0].chan_lts;

		//Incrementa contador de recursos da placa em uso e DSP
		//Boards[ ConferenceInfo.BoardId ][ ConferenceInfo.DspId ].ResourcesCount++;

		//Cadastra os dados referentes a este participante ....
		ConferenceInfo.Conferees[ ConfereeInfo.Timeslot ] = ConfereeInfo;

		//... nesta confer�ncia
		map_Conferences[ m_RoomId ] = ConferenceInfo;
		
		//Atualiza valor do timeslot para retornar ao IVR
		lTimeSlot = MsCdt[0].chan_lts;

		// debug

        m_pLog4cpp->debug("[%p][%s] CONFERENCE_OK", this, __FUNCTION__ );
		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Add()." << std::ends;
        m_pLog4cpp->debug("[%p][%s] %s", this, __FUNCTION__, LastErrorMessage.str() );
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::Remove( const char* lpcszConferenceId, long& lTimeSlot, conference_conferee_type cctConfereeType)
{
	try
	{
		m_RoomId = lpcszConferenceId;
		chair_person ChairPersonInfo;

        // Verifica se o tipo � conferencista e decrementa o contador de participantes
        if(cctConfereeType == CP_CONF)
        {
            //Decrementa 1 do contador de participantes do chat com personalidade
			ChairPersonInfo = ChairPerson[ m_RoomId ];
			if(ChairPersonInfo.Contador == 1)
				ChairPerson.erase( m_RoomId );
			else
			{
				ChairPersonInfo.Contador -= 1;
				ChairPerson[ m_RoomId ] = ChairPersonInfo;
			}
            return CONFERENCE_OK;
        }

		//Verifica se existe a conferencia
		if( map_Conferences.count( m_RoomId ) <= 0 )
		    return CONFERENCE_ERROR;

		conference_info ConferenceInfo = map_Conferences[ m_RoomId ];

		if( ConferenceInfo.Id == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::Remove(). Conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << " not found." << std::ends;
			return CONFERENCE_ERROR;
		}

		if(cctConfereeType == CP_GHOST)
		{
			lTimeSlot = ConferenceInfo.GhostTimeslot;
		}

		if( ConferenceInfo.Conferees.count( lTimeSlot ) <= 0 )
			return CONFERENCE_WARNING;

		conferee_info ConfereeInfo = map_Conferences[ m_RoomId ].Conferees[ lTimeSlot ];
		
		if( ConferenceInfo.Conferees.size() == 1 )
		{
			// Destroi a conferencia quando so resta um usuario ao inves de remove-lo.
			return Destroy( lpcszConferenceId );
		}
		else
		{
			// Remove o usuario da conferencia.
			MsCdt[0].chan_num	= lTimeSlot;
			MsCdt[0].chan_sel	= MSPN_TS;
			MsCdt[0].chan_attr = MSPA_NULL;		// Esse membro e ignorado pela dialogic na funcao dcb_remfromconf().

			ReturnCode = dcb_remfromconf( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, &MsCdt[0] );
			if( ReturnCode == -1 )
			{
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Remove(). Cannot remove the conferee (ID:" << lTimeSlot << ") from the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
				return CONFERENCE_ERROR;
			}

			if( VerifyConfereePresence( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, lTimeSlot ) )
			{
				ClearLastErrorMessage();
				LastErrorMessage << "Error in DCBClass::Remove(). Cannot remove the conferee (ID:" << lTimeSlot << ") from the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". Conferee already in the conference." << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
				return CONFERENCE_ERROR;
			}
		}

		//Boards[ ConferenceInfo.BoardId ][ ConferenceInfo.DspId ].ResourcesCount--;
		ConferenceInfo.Conferees.erase( lTimeSlot );

		map_Conferences[ m_RoomId ] = ConferenceInfo;		

		// debug

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Remove()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::Change( const char * lpcszConferenceIdSource, const char * lpcszConferenceIdDestination, long& lTimeSlot, const int& iConferenceSize, conference_conferee_type cctConfereeType, const bool& bNotifyTone, long& lMonitorTimeSlot )
{
	try
	{
		if( !lpcszConferenceIdSource || !lpcszConferenceIdDestination )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::Change(). Invalid parameters. Null pointer parameter." << std::ends;
			return CONFERENCE_ERROR;
		}

		if( !stricmp( lpcszConferenceIdSource, lpcszConferenceIdDestination ) )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::Change(). Invalid parameters. Conference source is the same as conference destination." << std::ends;
			return CONFERENCE_ERROR;
		}

		ReturnCode = Remove( lpcszConferenceIdSource, lTimeSlot, cctConfereeType );
		if( ReturnCode != CONFERENCE_OK )
		{
			return ReturnCode;
		}

		ReturnCode = Add( lpcszConferenceIdDestination, lTimeSlot, iConferenceSize, cctConfereeType, bNotifyTone, lMonitorTimeSlot );
		if( ReturnCode != CONFERENCE_OK )
		{
			return ReturnCode;
		}
		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Change()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::GetNumberOfConferees( const char* lpcszConferenceId )
{
	try
	{
        m_RoomId = lpcszConferenceId;

		//Verifica se existe a conferencia
		if( map_Conferences.count( m_RoomId ) <= 0 )
			return -1;

		conference_info& ConferenceInfo = map_Conferences[ m_RoomId ];

		return ConferenceInfo.Conferees.size();
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::GetNumberOfConferees()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::SetConfereeAttribute( const char* lpcszConferenceId, const long& lTimeSlot, conference_conferee_type cctConfereeType )
{
	try
	{
        m_RoomId = lpcszConferenceId;

		//Verifica se existe a conferencia
		if( map_Conferences.count( m_RoomId ) <= 0 )
		    return CONFERENCE_WARNING;

		//Verifica se o usuario existe na conferencia
		if( map_Conferences[ m_RoomId ].Conferees.count( lTimeSlot ) <= 0 )
			return CONFERENCE_WARNING;

		conference_info ConferenceInfo = map_Conferences[ m_RoomId ];
		conferee_info ConfereeInfo = map_Conferences[ m_RoomId ].Conferees[ lTimeSlot ];

		// Faz o usuario somente ouvir a conferencia.
		MsCdt[0].chan_num	= lTimeSlot;
		MsCdt[0].chan_sel	= MSPN_TS;

		switch( cctConfereeType )
		{
			case DUPLEX:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
			case MONITOR:
				MsCdt[0].chan_attr = MSPA_RO;
				break;
			case COACH:
				MsCdt[0].chan_attr = MSPA_COACH;
				break;
			case PUPIL:
				MsCdt[0].chan_attr = MSPA_PUPIL;
				break;
			case PRIVATE:
				MsCdt[0].chan_attr = MSPA_RO;
				break;
			default:
				MsCdt[0].chan_attr = MSPA_NULL;
				break;
		}

		ReturnCode = dcb_setcde( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, &MsCdt[0] );
		if( ReturnCode == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::SetConfereeAttribute(). Cannot set the conferee attribute (ID:" << lTimeSlot << ") in the conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << ". " << ATDV_ERRMSGP( ConferenceInfo.DSPDeviceHandle ) << "." << std::ends;
			return CONFERENCE_ERROR;
		}
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::SetConfereeAttribute()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}

	return CONFERENCE_OK;
}


int DCBClass::VerifyConfereeExistAndRemove( const long& lTimeSlot )
{
	try
	{
		// Pesquisa em todas as conferencias.
        std::string sConferenceId;
		std::map < std::string, conference_info >::iterator ConferencesIterator;
		std::map < long, conferee_info >::iterator ConfereesIterator;
		
		for( ConferencesIterator = map_Conferences.begin(); ConferencesIterator != map_Conferences.end(); ConferencesIterator++ )
		{
			conference_info& ConferenceInfo = (*ConferencesIterator).second;

		    for( ConfereesIterator = ConferenceInfo.Conferees.begin(); ConfereesIterator != ConferenceInfo.Conferees.end(); ConfereesIterator++ )
			{
		        conferee_info ConfereeInfo = (*ConfereesIterator).second;
				if ( ConfereeInfo.Timeslot == lTimeSlot)
				{
					sConferenceId = (*ConferencesIterator).first;
					if( ConferenceInfo.Conferees.size() == 1 )
					{
						// Destroi a conferencia quando so resta um usuario ao inves de remove-lo.
                        Boards[ ConferenceInfo.BoardId ][ ConferenceInfo.DspId ].ResourcesCount -= ConferenceInfo.NumberOfConferees;
						ReturnCode = dcb_delconf( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id );
						map_Conferences.erase( sConferenceId );
					}
					else
					{
						// Remove o usuario da conferencia.
						MsCdt[0].chan_num	= lTimeSlot;
						MsCdt[0].chan_sel	= MSPN_TS;
						MsCdt[0].chan_attr = MSPA_NULL;		// Esse membro e ignorado pela dialogic na funcao dcb_remfromconf().

						ReturnCode = dcb_remfromconf( ConferenceInfo.DSPDeviceHandle, ConferenceInfo.Id, &MsCdt[0] );
						ConferenceInfo.Conferees.erase( lTimeSlot );
						map_Conferences[ sConferenceId ] = ConferenceInfo;
					}
					return CONFERENCE_OK;
				}
			}
        }
		
	    return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::VerifyConfereeExistAndRemove()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}


bool DCBClass::VerifyConfereePresence( const int& iDSPHandle, const int& iConferenceId, long& lTimeSlot )
{
	try
	{
		int iConfereeCount = 0;
		int i = 0;
		MS_CDT ms_cdt[32];
		
		ReturnCode = dcb_getcnflist( iDSPHandle, iConferenceId, &iConfereeCount, &ms_cdt[0] );
		if( ReturnCode == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::VerifyConfereePresence(). Cannot verify the conferee presence (ID:" << lTimeSlot << ") in the conference (ID:" << iConferenceId << ") in the device " << ATDV_NAMEP( iDSPHandle ) << ". " << ATDV_ERRMSGP( iDSPHandle ) << "." << std::ends;
			return false;
		}

		if( iConfereeCount <= 0 )
			// Conferencia vazia.
			return false;

		for( i = 0; i < iConfereeCount; i++ )
		{
			if( ms_cdt[ i ].chan_num == lTimeSlot )
				// Encontrou o usuario na conferencia.
				return true;
		}
		
		// Conferencia varrida e usuario nao encontrado.
		return false;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::VerifyConfereePresence()." << std::ends;
		return false;
	}
}


void DCBClass::Debug()
{
	try
	{
		int DcbBoardCount = 0;
		int DcbDspCount = 0;
		int iConferenceId = 0;
		int iConfereeCount = 0;
		int i = 0;
		MS_CDT ms_cdt[32];
		conference_info ConferenceInfo;
		std::map < std::string, conference_info >::iterator itConferences;
		std::string bsConferenceId;
		struct tm *Time;
		struct _timeb TimeBuffer;
		char lpcszBuffer[256];
		
		std::ofstream debufFile( "dcb.txt", std::ios::app );

		debufFile << "---------------------------------\n";

		_ftime( &TimeBuffer );
		Time = localtime( &TimeBuffer.time );

		sprintf( lpcszBuffer,"DateTime: %04d-%02d-%02d %02d:%02d:%02d.%03d", Time->tm_year+1900, Time->tm_mon + 1, Time->tm_mday, Time->tm_hour, Time->tm_min, Time->tm_sec, TimeBuffer.millitm );

		debufFile << lpcszBuffer << "\n";

		for( DcbBoardCount = 1; DcbBoardCount <= Boards.size(); DcbBoardCount++ )
		{
			for( DcbDspCount = 1; DcbDspCount <= Boards[ DcbBoardCount ].size(); DcbDspCount++ )
			{
				for( iConferenceId = 1; iConferenceId <= 32; iConferenceId++ )
				{
					if( dcb_getcnflist( Boards[ DcbBoardCount ][ DcbDspCount ].DeviceHandle, iConferenceId, &iConfereeCount, &ms_cdt[0] ) == 0 )
					{
						for( itConferences = map_Conferences.begin(); itConferences != map_Conferences.end(); itConferences++ )
						{
							if( ( (*itConferences).second.Id == iConferenceId ) && ( (*itConferences).second.DSPDeviceHandle == Boards[ DcbBoardCount ][ DcbDspCount ].DeviceHandle ) )
							{
								bsConferenceId = (*itConferences).first;
								break;
							}
						}
						if( itConferences == map_Conferences.end() )
						{
							// Conferencia nao encontrada no map.
							debufFile << "Conference ID not found. Conference ID: " << iConferenceId << ", DeviceHandle: " << Boards[ DcbBoardCount ][ DcbDspCount ].DeviceHandle << "\n";
						}
						else
						{
							// Loga os dados da conferencia encontrada no map.
							debufFile << "Board ID      : " << DcbBoardCount << "\n";
							debufFile << "DSP ID        : " << DcbDspCount << "\n";
							debufFile << "Conference ID : " << bsConferenceId.c_str() << "/" << iConferenceId << "\n";
							for( i = 0; i < iConfereeCount; i++ )
							{
								debufFile << "TxTs          : " << ms_cdt[ i ].chan_num << "\n";
							}
							debufFile << "\n";
						}
					}
				}
			}
		}
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::Debug()." << std::ends;
		return;
	}
}

int DCBClass::GetConferenceTimeSlot( const char * lpcszConferenceId, long& lTimeSlot )
{
  try
	{
		m_RoomId = lpcszConferenceId;

		//Verifica se existe a conferencia
		if( map_Conferences.count( m_RoomId ) <= 0 )
			return CONFERENCE_ERROR;

		conference_info ConferenceInfo = map_Conferences[ m_RoomId ];		

		if( ConferenceInfo.Id == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::GetConferenceTimeSlot(). Conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << " not found." << std::ends;
			return CONFERENCE_ERROR;
		}

		if( ConferenceInfo.Conferees.count( lTimeSlot ) <= 0 )
			return CONFERENCE_ERROR;

        conferee_info ConfereeInfo = map_Conferences[ m_RoomId ].Conferees[ lTimeSlot ];

        lTimeSlot = ConfereeInfo.ConferenceTimeslot;

        return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::GetConferenceTimeSlot()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

int DCBClass::GetChairPersonTimeSlot( const char * lpcszConferenceId, long& lChairPersonTimeSlot, conference_conferee_type cctConfereeType )
{
  try
	{
		m_RoomId = lpcszConferenceId;

		//Verifica se existe a conferencia. Sen�o, procura em ChairPerson
		if( map_Conferences.count( m_RoomId ) <= 0 )
		{
			if( ChairPerson.count(m_RoomId) <= 0 )
				return CONFERENCE_ERROR;

			chair_person ChairPersonInfo = ChairPerson[ m_RoomId ];
			lChairPersonTimeSlot = ChairPersonInfo.Timeslot;
			return CONFERENCE_OK;
		}

		conference_info ConferenceInfo = map_Conferences[ m_RoomId ];		

		if( ConferenceInfo.Id == -1 )
		{
			ClearLastErrorMessage();
			LastErrorMessage << "Error in DCBClass::GetChairPersonTimeSlot(). Conference (ID:" << lpcszConferenceId << ",id:" << ConferenceInfo.Id << ") in the device " << ATDV_NAMEP( ConferenceInfo.DSPDeviceHandle ) << " not found." << std::ends;
			return CONFERENCE_ERROR;
		}

		lChairPersonTimeSlot = ConferenceInfo.ChairPersonTimeslot;

		return CONFERENCE_OK;
	}
	catch(...)
	{
		ClearLastErrorMessage();
		LastErrorMessage << "Exception in DCBClass::GetChairPersonTimeSlot()." << std::ends;
		return CONFERENCE_EXCEPTION;
	}
}

const char* DCBClass::GetLastError()
{
	try
	{
		return LastErrorMessage.str();
	}
	catch(...)
	{
		return "Exception in DCBClass::GetLastError().";
	}
}

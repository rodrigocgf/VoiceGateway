/*
** $Id: luasql.c,v 1.28 2009/02/11 12:08:50 tomas Exp $
** See Copyright Notice in license.html
*/

//#include <sqlext.h>


#include <string.h>

#include "lua.h"
#include "lauxlib.h"


#include "luasql.h"


#if !defined(lua_pushliteral)
#define lua_pushliteral(L, s) \
	lua_pushstring(L, "" s ) //, (sizeof(s)/sizeof(char))-1)                    // changed from Lua 5
#endif

static int ODBCEnvironmentTag = LUA_NOTAG;
static int ODBCConnectionTag  = LUA_NOTAG;
static int ODBCCursorTag      = LUA_NOTAG;


/*
** Typical database error situation
*/
LUASQL_API int luasql_faildirect(lua_State *L, const char *err) {
	lua_pushnil(L);
	lua_pushliteral(L, LUASQL_PREFIX);
	lua_pushstring(L, err);
	lua_concat(L, 2);
	return 2;
}


/*
** Database error with LuaSQL message
** @param err LuaSQL error message.
** @param m Driver error message.
*/
LUASQL_API int luasql_failmsg(lua_State *L, const char *err, const char *m) {
	lua_pushnil(L);
	lua_pushliteral(L, LUASQL_PREFIX);
	lua_pushstring(L, err);
	lua_pushstring(L, m);
	lua_concat(L, 3);
	return 2;
}


typedef struct { short  closed; } pseudo_data;

/*
** Return the name of the object's metatable.
** This function is used by `tostring'.
*/
static int luasql_tostring (lua_State *L) {
	/*char buff[100];
	pseudo_data *obj = (pseudo_data *)lua_touserdata (L, 1);
	if (obj->closed)
		strcpy (buff, "closed");
	else
		sprintf (buff, "%p", (void *)obj);
    char sqlstr[512];
    _snprintf( sqlstr, sizeof( sqlstr ), "%s (%s)", lua_tostring(L,lua_upvalueindex(1)), buff );
	lua_pushstring (L, sqlstr);*/
    lua_pushstring( L, "luasql_tostring n�o implementado" );
	return 1;
}


#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
/*
** Adapted from Lua 5.2.0
*/
void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup) {
	luaL_checkstack(L, nup, "too many upvalues");
	for (; l->name != NULL; l++) {	/* fill the table with given functions */
		int i;
		for (i = 0; i < nup; i++)	/* copy upvalues to the top */
			lua_pushvalue(L, -nup);
		lua_pushstring(L, l->name);
		lua_pushcclosure(L, l->func, nup);	/* closure with those upvalues */
		lua_settable(L, -(nup + 3));
	}
	lua_pop(L, nup);	/* remove upvalues */
}
#endif

/* added for compatibility with lua 4 */
LUASQL_API int luasql_newmetatable( lua_State* L, char const* name )
{                                                       // stack: empty
    lua_getref( L, SQLRegistryRef );                    // stack: registryTable
    lua_pushstring( L, name );                          // stack: registryTable  metaname
    lua_rawget( L, -2 );                                // stack: registryTable  table?
    //checking if the table already exists
    if( !lua_isnil( L, -1 ) )
        return 0;

    lua_pop( L, 1 );                                    // stack: registryTable

    lua_newtable( L );                                  // stack: registryTable newTable
    lua_pushstring( L, name );                          // stack: registryTable newTable name
    lua_pushvalue( L, -2 );                             // stack: registryTable newTable name newTable
    lua_rawset( L, -4 );                                // stack: registryTable newTable
    lua_remove( L, -2 );                                // stack: newTable

    return 1;
}

/* added for compatibility with lua 4 */
LUASQL_API int luasql_getmetatable( lua_State* L, char const* name )
{
    lua_getref( L, SQLRegistryRef );                    // stack: empty
    lua_pushstring( L, name );                          // stack: registryTable
    lua_rawget( L, -2 );                                // stack: registryTable name
                                                        // stack: registryTable metatable?
    if( lua_isnil( L, -1 ) )
    {
        lua_pop( L, 2 );                                // stack: registryTable nil
        return 0;                                       // stack: empty
    }

    lua_remove( L, -2 );                                // stack: registryTable metatable
    return 1;                                           // stack: metatable
}

/* added for compatibility with lua 4 */
LUASQL_API int luasql_setmetatable( lua_State* L, int index )
{
    int metatableTag;
    if( lua_isnil( L, index ) )
        return 0;
    
    metatableTag = lua_tag( L, -1 );                // stack: table metatable
    lua_pop( L, 1 );                                    // stack: table
    lua_settag( L, metatableTag );
    return 1;
}

/* added for compatibility with lua 4 */
LUASQL_API int luasql_gettabletagmethod( lua_State* L )
{
    char const* tableName;
    int objectTag = lua_tag( L, 1 );                    // stack: userdata index

    if( objectTag == LUA_NOTAG )
    {
        return 0;
    }
    
    if( objectTag == ODBCConnectionTag )
        tableName = LUASQL_CONNECTION_ODBC;
    else if( objectTag == ODBCEnvironmentTag )
        tableName = LUASQL_ENVIRONMENT_ODBC;
    else if( objectTag == ODBCCursorTag )
        tableName = LUASQL_CURSOR_ODBC;
    else
        return 0;

    luasql_getmetatable( L, tableName );                // stack: userdata index metatable
    lua_pushvalue( L, -2 );                             // stack: userdata index metatable index
    lua_rawget( L, -2 );                              // stack: userdata index value

    return 1;
}


/*
** Create a metatable and leave it on top of the stack.
*/
LUASQL_API int luasql_createmeta (lua_State *L, const char *name, const luaL_Reg *methods, lua_CFunction garbageCollector ) {
	/*
    if (!luaL_newmetatable (L, name))
		return 0;

	/* define methods * /
	luaL_setfuncs (L, methods, 0);

	/* define metamethods * /
	lua_pushliteral (L, "__index");
	lua_pushvalue (L, -2);
	lua_settable (L, -3);

	lua_pushliteral (L, "__tostring");
	lua_pushstring (L, name);
	lua_pushcclosure (L, luasql_tostring, 1);
	lua_settable (L, -3);

	lua_pushliteral (L, "__metatable");
	lua_pushliteral (L, LUASQL_PREFIX"you're not allowed to get this metatable");
	lua_settable (L, -3);
    */
    int newTag;
    int maxLength;
    if( !luasql_newmetatable( L, name ) )
        return 0;
                                                        // stack: metatable
    luaL_setfuncs( L, methods, 0 );

    newTag = lua_newtag( L );                           // stack: metatable
    lua_pushcfunction( L, luasql_gettabletagmethod );   // stack: metatable cfunction
    lua_settagmethod( L, newTag, "gettable" );          // stack: metatable
	lua_pushcfunction( L, garbageCollector );
	lua_settagmethod( L, newTag, "gc" );

    
    lua_settag( L, newTag );

    maxLength = strlen( name );
    if( strncmp( name, LUASQL_ENVIRONMENT_ODBC, maxLength ) == 0 )
        ODBCEnvironmentTag = newTag;
    else if( strncmp( name, LUASQL_CONNECTION_ODBC, maxLength ) == 0 )
        ODBCConnectionTag = newTag;
    else if( strncmp( name, LUASQL_CURSOR_ODBC, maxLength ) == 0 )
        ODBCCursorTag = newTag;

	return 1;
}


/*
** Define the metatable for the object on top of the stack
*/
LUASQL_API void luasql_setmeta (lua_State *L, const char *name) {
	//luaL_getmetatable (L, name);                                              // changed from Lua 5
    luasql_getmetatable( L, name );
	//lua_setmetatable (L, -2);                                                 // changed from Lua 5
    luasql_setmetatable( L, -2 );
}


/*
** Assumes the table is on top of the stack.
*/
LUASQL_API void luasql_set_info (lua_State *L) {
	lua_pushliteral (L, "_COPYRIGHT");
	lua_pushliteral (L, "Copyright (C) 2003-2012 Kepler Project");
	lua_settable (L, -3);
	lua_pushliteral (L, "_DESCRIPTION");
	lua_pushliteral (L, "LuaSQL is a simple interface from Lua to a DBMS");
	lua_settable (L, -3);
	lua_pushliteral (L, "_VERSION");
	lua_pushliteral (L, "LuaSQL 2.3.0 for Lua 4");
	lua_settable (L, -3);
}

LUASQL_API void luasql_init( lua_State* L )
{
    lua_newtable( L );                          // create a table to serve as registry
    SQLRegistryRef = lua_ref( L, 1 );           // create a reference to it and don't allow it to be garbage collected
}
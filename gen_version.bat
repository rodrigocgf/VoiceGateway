@echo off
setlocal enabledelayedexpansion
set filename=version.h

for /f "tokens=1-3 delims= " %%A in (version.h) do (
 if "%%B"=="VERSION_MAJOR_NUMBER" (
  set num=%%C
  set /a num+=1
  echo READ:  %%A %%B            %%C
  echo WRITE: %%A %%B            !num!
  echo.
  echo %%A %%B            !num!>>"%filename%.temp" 
  )
 if "%%B"=="VERSION_MINOR_NUMBER" (
  set num=%%C
  set /a num+=1
  echo READ:  %%A %%B            %%C
  echo WRITE: %%A %%B            !num!
  echo.
  echo %%A %%B            !num!>>"%filename%.temp" 
  ) 
 if "%%B"=="VERSION_BUILD_NUMBER" (
  echo READ:  %%A %%B            %%C
  echo WRITE: %%A %%B            %%C
  echo.
  echo %%A %%B            %%C>>"%filename%.temp" 
  ) 
 )
